/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.places

import java.io.File

/**
 * The data used by the PlacesDock in subproject glok-dock.
 *
 * This isn't part of glok-dock so that we can use [Place] without depending on the
 * Glok's GUI components.
 */
class Place(val file: File, val alias: String = file.name) {

    override fun hashCode(): Int {
        var result = file.hashCode()
        result = 31 * result + alias.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        return other is Place && file == other.file && alias == other.alias
    }

    override fun toString() = "$alias : $file"

}

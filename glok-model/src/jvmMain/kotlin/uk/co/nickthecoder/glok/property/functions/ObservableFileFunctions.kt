/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.util.Converter
import java.io.File

fun ObservableFile.exists() = BooleanUnaryFunction(this) { it.exists() }
fun ObservableFile.parentFile() = FileUnaryFunction(this) { it.parentFile }
fun ObservableFile.name() = StringUnaryFunction(this) { it.name }
fun ObservableFile.nameWithoutExtension() = StringUnaryFunction(this) { it.nameWithoutExtension }
fun ObservableFile.extension() = StringUnaryFunction(this) { it.extension }
fun ObservableFile.absoluteFile() = FileUnaryFunction(this) { it.absoluteFile }
fun ObservableFile.isDirectory() = BooleanUnaryFunction(this) { it.isDirectory }
fun ObservableFile.isFile() = BooleanUnaryFunction(this) { it.isFile }
fun ObservableFile.lastModified() = LongUnaryFunction(this) { it.lastModified() }
fun ObservableFile.path() = StringUnaryFunction(this) { it.path }


object StringToFile : Converter<String, File> {
    override fun forwards(value: String) = File(value)
    override fun backwards(value: File) = value.toString()
}

object StringToOptionalFile : Converter<String, File?> {
    override fun forwards(value: String) = if (value.isEmpty()) null else File(value)
    override fun backwards(value: File?) = value?.toString() ?: ""
}

object FileToString : Converter<File, String> {
    override fun forwards(value: File) = value.toString()
    override fun backwards(value: String) = File(value)
}

object OptionalFileToString : Converter<File?, String> {
    override fun forwards(value: File?) = value?.toString() ?: ""
    override fun backwards(value: String) = if (value.isEmpty()) null else File(value)
}

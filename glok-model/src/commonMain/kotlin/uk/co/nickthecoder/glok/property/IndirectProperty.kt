/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.property.boilerplate.*

open class IndirectProperty<P : Any, V>(

    val parentProperty: ObservableValue<P>,

    /**
     * A lambda to get the [Property] we are interested in tracking, from the value of [parentProperty].
     */
    val getProperty: (P) -> Property<V>

) : PropertyBase<V>() {

    private var oldValue: V = value

    private val listener = invalidationListener {
        val oldValue = oldValue
        val newValue = value
        if (oldValue != newValue) {
            this.fire(oldValue, value)
        }
    }

    @Suppress("unused")
    private val parentPropertyListener = parentProperty.addWeakChangeListener { _, oldParentValue, parentValue ->
        getProperty(oldParentValue).removeListener(listener)
        getProperty(parentValue).addListener(listener)
    }

    init {
        parentProperty.addListener(listener)
        getProperty(parentProperty.value).addListener(listener)
    }

    final override var value: V
        get() {
            val parentValue = parentProperty.value
            oldValue = getProperty(parentValue).value
            return oldValue
        }
        set(v) {
            val parentValue = parentProperty.value
            getProperty(parentValue).value = v
        }

}


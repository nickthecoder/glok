/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener

/**
 * For the rare cases where an immutable list must have the API of an ObservableList.
 * Adding/removing listeners is a no-op because no events will ever be fired!
 * (because the list is immutable).
 */
internal class ObservableListWrapper<E>(private val actual: List<E>) : ObservableList<E> {

    override fun addChangeListener(listener: ListChangeListener<E>) {}

    override fun removeChangeListener(listener: ListChangeListener<E>) {}

    override fun addListener(listener: InvalidationListener) {}

    override fun removeListener(listener: InvalidationListener) {}

    override val size get() = actual.size

    override fun get(index: Int) = actual.get(index)

    override fun isEmpty() = actual.isEmpty()

    override fun iterator() = actual.iterator()

    override fun listIterator() = actual.listIterator()

    override fun listIterator(index: Int) = actual.listIterator(index)

    override fun subList(fromIndex: Int, toIndex: Int) = actual.subList(fromIndex, toIndex)

    override fun lastIndexOf(element: E) = actual.lastIndexOf(element)

    override fun indexOf(element: E) = actual.indexOf(element)

    override fun containsAll(elements: Collection<E>) = actual.containsAll(elements)

    override fun contains(element: E) = actual.contains(element)
}


/**
 * Wraps a _regular_ list, creating a [MutableObservableList].
 *
 * Any changes made to the underlying list will NOT cause change events to be fired.
 * Therefore, it is not advisable to keep references to the underlying list.
 */
fun <E> List<E>.asObservableList(): ObservableList<E> = ObservableListWrapper(this)

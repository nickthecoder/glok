package uk.co.nickthecoder.glok.collections

/**
 * The data describing changes to an [ObservableSet].
 *
 * A typical listener has the following structure :
 *
 *     val myListener = myObservableSet.addChangeListener { _, change ->
 *         if ( change.removed.isNotEmpty() ) {
 *             // Act on removed items
 *         }
 *         if ( change.added.isNotEmpty() ) {
 *             // Act on data added.
 *         }
 *     }
 *
 */
interface SetChange<E> {
    val added: Iterable<E>
    val removed: Iterable<E>
    fun isRemoval(): Boolean
    fun isAddition(): Boolean

    companion object {
        /**
         * Creates a [SetChange] describing items removed from an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         */
        fun <E> removal(removed: Set<E>): SetChange<E> = SetChangeImpl(emptySet(), removed)

        /**
         * Creates a [SetChange] describing items added to an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         */
        fun <E> addition(added: Set<E>): SetChange<E> = SetChangeImpl(added, emptySet())
    }

}

internal class SetChangeImpl<E>(

    override val added: Set<E>,
    override val removed: Set<E>

) : SetChange<E> {

    override fun isRemoval() = removed.isNotEmpty() && added.isEmpty()
    override fun isAddition() = added.isNotEmpty() && removed.isEmpty()

}

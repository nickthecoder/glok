/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*

operator fun ObservableLong.unaryMinus(): ObservableLong = LongUnaryFunction(this) { a -> -a }

operator fun ObservableLong.plus(constant: Long): ObservableLong = LongUnaryFunction(this) { a -> a + constant }
operator fun ObservableLong.minus(constant: Long): ObservableLong = LongUnaryFunction(this) { a -> a - constant }
operator fun ObservableLong.times(constant: Long): ObservableLong = LongUnaryFunction(this) { a -> a * constant }
operator fun ObservableLong.div(constant: Long): ObservableLong = LongUnaryFunction(this) { a -> a / constant }
operator fun ObservableLong.rem(constant: Long): ObservableLong = LongUnaryFunction(this) { a -> a % constant }
fun min(oa: ObservableLong, b: Long): ObservableLong = LongUnaryFunction(oa) { a -> if (a < b) a else b }
fun max(oa: ObservableLong, b: Long): ObservableLong = LongUnaryFunction(oa) { a -> if (a > b) a else b }

operator fun ObservableLong.plus(other: ObservableLong): ObservableLong = LongBinaryFunction(this, other) { a, b -> a + b }
operator fun ObservableLong.minus(other: ObservableLong): ObservableLong = LongBinaryFunction(this, other) { a, b -> a - b }
operator fun ObservableLong.times(other: ObservableLong): ObservableLong = LongBinaryFunction(this, other) { a, b -> a * b }
operator fun ObservableLong.div(other: ObservableLong): ObservableLong = LongBinaryFunction(this, other) { a, b -> a / b }
operator fun ObservableLong.rem(other: ObservableLong): ObservableLong = LongBinaryFunction(this, other) { a, b -> a % b }
fun min(oa: ObservableLong, ob: ObservableLong): ObservableLong = LongBinaryFunction(oa, ob) { a, b -> if (a < b) a else b }
fun max(oa: ObservableLong, ob: ObservableLong): ObservableLong = LongBinaryFunction(oa, ob) { a, b -> if (a > b) a else b }

fun ObservableLong.toFloat(): ObservableFloat = FloatUnaryFunction(this) { a -> a.toFloat() }
fun ObservableLong.toDouble(): ObservableDouble = DoubleUnaryFunction(this) { a -> a.toDouble() }

fun ObservableLong.greaterThan(value: Long) = BooleanUnaryFunction(this) { it > value }
fun ObservableLong.lessThan(value: Long) = BooleanUnaryFunction(this) { it < value }
fun ObservableLong.greaterThanOrEqual(value: Long) = BooleanUnaryFunction(this) { it >= value }
fun ObservableLong.lessThanOrEqual(value: Long) = BooleanUnaryFunction(this) { it <= value }

fun ObservableLong.greaterThan(other: ObservableLong) = BooleanBinaryFunction(this, other) { a, b -> a > b }
fun ObservableLong.lessThan(other: ObservableLong) = BooleanBinaryFunction(this, other) { a, b -> a < b }
fun ObservableLong.greaterThanOrEqual(other: ObservableLong) = BooleanBinaryFunction(this, other) { a, b -> a >= b }
fun ObservableLong.lessThanOrEqual(other: ObservableLong) = BooleanBinaryFunction(this, other) { a, b -> a <= b }

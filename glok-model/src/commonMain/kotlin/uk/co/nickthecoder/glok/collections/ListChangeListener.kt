package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.util.Weak

/**
 * When programming in the typical Kotlin style, this isn't used, because you'll use a `lambda` instead.
 *
 * See [ObservableList.addChangeListener].
 */
interface ListChangeListener<out E> {
    fun changed(list: ObservableList<@UnsafeVariance E>, change: ListChange<@UnsafeVariance E>)
}


fun <E> listChangeListener(lambda: (list: ObservableList<E>, change: ListChange<E>) -> Unit): ListChangeListener<E> =
    object : ListChangeListener<E> {
        override fun changed(
            list: ObservableList<@UnsafeVariance E>, change: ListChange<@UnsafeVariance E>
        ) {
            lambda(list, change)
        }
    }

/**
 * Using weak listeners can help prevent memory leaks.
 *
 * A Weak Listener is a `wrapper` around an `inner` listener.
 * The `wrapper` contains a _weak_ reference to the `inner`
 *
 * The `wrapper` should be added to the list of listeners (via [ObservableList.addChangeListener]`)
 * *
 * You have to keep a reference to `inner`, otherwise it will be garbage collected,
 * and the listener will stop working.
 *
 * When your class (which holds the returned reference to `inner`) is garbage collected,
 * there will be no strong references to `inner`, so it too will be garbage collected,
 * and the listener will stop.
 *
 * To remove a weak listener, pass `inner` or `this` to [ObservableList.removeChangeListener].
 * ([ObservableList.removeChangeListener] has special code which removes the `wrapper` (this),
 * even when the `inner` is passed to it).
 */
class WeakListChangeListener<out E>(
    wraps: ListChangeListener<E>
) : ListChangeListener<E> {

    private val weak = Weak(wraps)

    fun actual() = weak.get()

    override fun changed(list: ObservableList<@UnsafeVariance E>, change: ListChange<@UnsafeVariance E>) {
        weak.get()?.changed(list, change)
    }
}

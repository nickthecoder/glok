
// **** Autogenerated. Do NOT edit. ****

package uk.co.nickthecoder.glok.property.boilerplate

import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.documentation.Boilerplate
import uk.co.nickthecoder.glok.documentation.PropertyFunctions 


// region ==== Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ObservableValue<Byte>`, use `ObservableByte`.
 */
interface ObservableByte: ObservableValue<Byte> {}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Byte>`, use `ReadOnlyByteProperty`.
 */
interface ReadOnlyByteProperty : ObservableByte, ReadOnlyProperty<Byte>

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `Property<Byte>`, use `ByteProperty`.
 */
interface ByteProperty : Property<Byte>, ReadOnlyByteProperty {

    /**
     * Returns a read-only view of this mutable ByteProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by ByteProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyByteProperty = ReadOnlyBytePropertyWrapper( this )
}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `SimpleProperty<Byte>`, we can use `SimpleByteProperty`.
 */
open class SimpleByteProperty(initialValue: Byte, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Byte>(initialValue, bean, beanName), ByteProperty

/**
 * Never use this class directly.
 * Use [ByteProperty.asReadOnly] to obtain a read-only version of a mutable [ByteProperty].
 *
 * [Boilerplate] which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Byte, Property<Byte>>`, use `ReadOnlyBytePropertyWrapper`.
 */
class ReadOnlyBytePropertyWrapper(wraps: ByteProperty) :
    ReadOnlyPropertyWrapper<Byte, Property<Byte>>(wraps), ReadOnlyByteProperty


/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ValidatedProperty<Byte>`, we can use `ValidatedByteProperty`.
 */
open class ValidatedByteProperty(initialValue: Byte, bean: Any? = null, beanName: String? = null, validation : (ValidatedProperty<Byte>,Byte,Byte)->Byte ) :
    ValidatedProperty<Byte>(initialValue, bean, beanName, validation), ByteProperty
    

// endregion Basic Features

// region ==== Optional Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ObservableByte], but the [value] can also be `null`.
 */
interface ObservableOptionalByte : ObservableValue<Byte?> {
    fun defaultOf( defaultValue : Byte ) : ObservableByte = ByteUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyByteProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalByteProperty : ObservableOptionalByte, ReadOnlyProperty<Byte?>

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ByteProperty], but the [value] can also be `null`.
 */
interface OptionalByteProperty : Property<Byte?>, ReadOnlyOptionalByteProperty {

    /**
     * Returns a read-only view of this mutable OptionalByteProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by optionalByteProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalByteProperty = ReadOnlyOptionalBytePropertyWrapper( this )

}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [SimpleByteProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalByteProperty(initialValue: Byte?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Byte?>(initialValue, bean, beanName), OptionalByteProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ValidatedByteProperty], but the [value] can also be `null`.
 */
open class OptionalValidatedByteProperty(initialValue: Byte?, bean: Any? = null, beanName: String? = null, validation: (ValidatedProperty<Byte?>,Byte?,Byte?)->Byte?) :
    ValidatedProperty<Byte?>(initialValue, bean, beanName, validation), OptionalByteProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyBytePropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalBytePropertyWrapper(wraps: OptionalByteProperty) :
    ReadOnlyPropertyWrapper<Byte?, Property<Byte?>>(wraps), ReadOnlyOptionalByteProperty

// endregion optional basic features

// region ==== Delegate ====

/**
 * A Kotlin `delegate` to create a [ByteProperty] (the implementation will be a [SimpleByteProperty].
 * Typical usage :
 *
 *     val fooProperty by byteProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun byteProperty(initialValue: Byte) = PropertyDelegate<Byte,ByteProperty>(initialValue) { bean, name, value ->
    SimpleByteProperty(value, bean, name)
}

    
/**
 * A Kotlin `delegate` to create a [ValidatedByteProperty].
 * Typical usage :
 *
 *     val fooProperty by validatedByteProperty( initialValue ) { value -> ... }
 *     var foo by fooProperty
 *
 */
fun validatedByteProperty(initialValue: Byte, validation : (ValidatedProperty<Byte>,Byte,Byte) -> Byte) = PropertyDelegate<Byte,ByteProperty>(initialValue) { bean, name, value ->
    ValidatedByteProperty(value, bean, name, validation)
}

//endregion Delegate

// region ==== Optional Delegate ====
/**
 * A Kotlin `delegate` to create an [OptionalByteProperty] (the implementation will be a [SimpleOptionalByteProperty].
 * Typical usage :
 *
 *     val fooProperty by optionalByteProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun optionalByteProperty(initialValue: Byte?) = PropertyDelegate<Byte?,OptionalByteProperty>(initialValue) { bean, name, value ->
    SimpleOptionalByteProperty(value, bean, name)
}

/**
 * A Kotlin `delegate` to create an [OptionalValidatedByteProperty].
 * Typical usage :
 *
 *     val fooProperty by optionalValidatedByteProperty( initialValue ) { value -> ... }
 *     var foo by fooProperty
 *
 */
fun optionalValidatedByteProperty(initialValue: Byte?, validation:(ValidatedProperty<Byte?>,Byte?,Byte?)->Byte?) = PropertyDelegate<Byte?,OptionalByteProperty>(initialValue) { bean, name, value ->
    OptionalValidatedByteProperty(value, bean, name, validation)
}

// endregion Optional Delegate

// region ==== Functions ====

/**
 * [Boilerplate] which avoids having to use generics.
 * An [ObservableByte] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction] ([PropertyFunctions]).
 */
class ByteUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Byte) :
    UnaryFunction<Byte, A, OA>(argA, lambda), ObservableByte

/**
 * [Boilerplate] which avoids having to use generics.
 * An [ObservableByte] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction] ([PropertyFunctions]).
 */
class ByteBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Byte) :
    BinaryFunction<Byte, A, OA, B, OB>(argA, argB, lambda), ObservableByte

/**
 * [Boilerplate] which avoids having to use generics.
 * An [ObservableByte] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction] ([PropertyFunctions]).
 */
class ByteTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Byte
) : TernaryFunction<Byte, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableByte

// endregion Functions

// region ==== Optional Functions ====

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ByteUnaryFunction], but the [value] can also be `null`.
 */
class OptionalByteUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Byte?) :
    UnaryFunction<Byte?, A, OA>(argA, lambda), ObservableOptionalByte

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ByteBinaryFunction], but the [value] can also be `null`.
 */
class OptionalByteBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Byte?) :
    BinaryFunction<Byte?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalByte

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ByteTernaryFunction], but the [value] can also be `null`.
 */
class OptionalByteTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Byte?
) : TernaryFunction<Byte?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalByte

// endregion Optional Functions

// region ==== Indirect ====

/**
 * [Boilerplate] for a non-generic version of [IndirectProperty].
 */
class IndirectByteProperty<P : Any>(
    parentProperty: ObservableValue<P>,
    getProperty: (P) -> Property<Byte>
) : IndirectProperty<P, Byte>(parentProperty, getProperty), ByteProperty


/**
 * [Boilerplate] for a non-generic version of [IndirectProperty].
 */
class DefaultIndirectByteProperty<P : Any>(
    parentProperty: ObservableValue<P?>,
    defaultProperty : Property<Byte>,
    getProperty: (P) -> Property<Byte>
) : DefaultIndirectProperty<P, Byte>(parentProperty, defaultProperty, getProperty), ByteProperty


/**
 * [Boilerplate] for a non-generic version of [IndirectObservableValue].
 */
class IndirectObservableByte<P : Any>(
    parentProperty: ObservableValue<P>,
    getObservable: (P) -> ObservableValue<Byte>
) : IndirectObservableValue<P, Byte>(parentProperty, getObservable), ObservableByte

/**
 * [Boilerplate] for a non-generic version of [DefaultIndirectObservableValue].
 */
class DefaultIndirectObservableByte<P : Any>(
    parentProperty: ObservableValue<P?>,
    defaultValue: Byte,
    getObservable: (P) -> ObservableValue<Byte>
) : DefaultIndirectObservableValue<P, Byte>(parentProperty, defaultValue, getObservable), ObservableByte

// endregion Indirect

// region ==== Optional Indirect ====

/**
 * [Boilerplate] for a non-generic version of [IndirectProperty].
 */
class IndirectOptionalByteProperty<P : Any>(
    parentProperty: Property<P>,
    getProperty: (P) -> Property<Byte?>
) : IndirectProperty<P, Byte?>(parentProperty, getProperty), OptionalByteProperty


/**
 * [Boilerplate] for a non-generic version of [IndirectProperty].
 */
class DefaultIndirectOptionalByteProperty<P : Any>(
    parentProperty: ObservableValue<P?>,
    defaultProperty : Property<Byte?>,
    getProperty: (P) -> Property<Byte?>
) : DefaultIndirectProperty<P, Byte?>(parentProperty, defaultProperty, getProperty), OptionalByteProperty

/**
 * [Boilerplate] for a non-generic version of [IndirectObservableValue].
 */
class IndirectObservableOptionalByte<P : Any>(
    parentProperty: ObservableValue<P>,
    getObservable: (P) -> ObservableValue<Byte?>
) : IndirectObservableValue<P, Byte?>(parentProperty, getObservable), ObservableOptionalByte

/**
 * [Boilerplate] for a non-generic version of [IndirectObservableValue].
 */
class DefaultIndirectObservableOptionalByte<P : Any>(
    parentProperty: ObservableValue<P?>,
    defaultValue: Byte?,
    getObservable: (P) -> ObservableValue<Byte?>
) : DefaultIndirectObservableValue<P, Byte?>(parentProperty, defaultValue, getObservable), ObservableOptionalByte

// endregion Optional Indirect



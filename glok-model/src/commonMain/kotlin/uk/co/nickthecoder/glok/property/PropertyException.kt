package uk.co.nickthecoder.glok.property

class PropertyException(val property: Property<*>, message: String) : Exception(message)

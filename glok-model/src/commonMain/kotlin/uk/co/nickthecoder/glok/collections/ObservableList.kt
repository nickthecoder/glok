/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.Observable
import uk.co.nickthecoder.glok.property.WeakChangeListener
import uk.co.nickthecoder.glok.property.changeListener

/**
 * A list, which can be observed by [ListChangeListener]s and [InvalidationListener]s.
 *
 * Note, the collection names in Glok use Kotlin's naming convention.
 * So an [ObservableList] is not mutable, whereas [MutableObservableList] is mutable.
 *
 * Comparing Glok with JavaFX :
 * * Glok : ObservableList -> JavaFX : ReadOnlyObservableList
 * * Glok : MutableObservableList -> JavaFX : ObservableList
 *
 * ## Class Diagram
 *
 *
 *                                                 ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                            ╭╌╌╌╌╌╌╌╌╌╌╌╮        ┆    Observable    ┆           ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *     ┌─────────────────────▶┆   List    ┆        ┆ addListener()    ┆           ┆ObservableInt┆
 *     │                      ╰╌╌╌╌╌╌╌╌╌╌╌╯        ┆ removeListener() ┆           ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *     │                            △              ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯                  △
 *     │                            │                        △                           │
 *     │                ┌───────────┴──────────────────┐     │                           │
 *     │                │                       ╭╌╌╌╌╌╌┴╌╌╌╌╌┴╌╌╌╌╌╌╌╌╌╌╮       ┏━━━━━━━━┷━━━━━━━━━┓
 *     │           ╭╌╌╌╌┴╌╌╌╌╌╌╮                ┆    ObservableList     ┆       ┃ObservableListSize┃
 *     │  ┌───────▶┆MutableList┆                ┆ addChangeListener()   ┆◀──────┨                  ┃
 *     │  │        ╰╌╌╌╌╌╌╌╌╌╌╌╯                ┆ removeChangeListener()┆       ┃                  ┃
 *     │  │             △                       ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯       ┗━━━━━━━━━━━━━━━━━━┛
 *     │  │             │                                    △
 *     │  │             │                                    │
 *     │  │             │      ┌─────────────────────────────┴──┬─────────────────────────────┐
 *     │  │             │      │                                │                             │
 *     │  │  ╭╌╌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌┴╌╌╌╮            ┏━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━┓     ┏━━━━━━━━━┷━━━━━━━━━┓
 *     │  │  ┆MutableObservableList┆            ┃ReadOnlyObservableListWrapper┃     ┃EmptyObservableList┃
 *     │  │  ┆                     ┆            ┃  actual : List              ┃     ┃                   ┃
 *     │  │  ┆                     ┆            ┃                             ┃     ┗━━━━━━━━━━━━━━━━━━━┛
 *     │  │  ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯            ┃                             ┠────────────────────┐
 *     │  │             △                       ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛                    │
 *     │  │             │                                                                          │
 *     │  │             │                                                                          │
 *     │  │ ┏━━━━━━━━━━━┷━━━━━━━━━━━━━━━━┓   ┌──────────────────────────┐ ┌───────────────────┐    │
 *     │  │ ┃MutableObservableListWrapper┃   │    ListChangeListener    │ │    ListChange     │    │
 *     │  └─┨  actual : MutableList      ┃◇──┤                          │ │ from : Int        │    │
 *     │    ┃                            ┃   │ changed(List<ListChange>)│ │ added : List      │    │
 *     │    ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛   └──────────────────────────┘ │ removed : List    │    │
 *     │                                                                  │ isAddition()      │    │
 *     │                                                                  │ isRemoval()       │    │
 *     │                                                                  │ isReplacement()   │    │
 *     │                                                                  └───────────────────┘    │
 *     │                                                                                           │
 *     └───────────────────────────────────────────────────────────────────────────────────────────┘
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
 */
interface ObservableList<out E> : List<E>, Observable {

    fun addChangeListener(listener: ListChangeListener<@UnsafeVariance E>)
    fun removeChangeListener(listener: ListChangeListener<@UnsafeVariance E>)

    fun addChangeListener(
        lambda: (list: ObservableList<E>, change: ListChange<E>) -> Unit
    ): ListChangeListener<E> {
        val lcl = listChangeListener(lambda)
        addChangeListener(lcl)
        return lcl
    }

    fun addWeakChangeListener(
        lambda: (list: ObservableList<E>, change: ListChange<E>) -> Unit
    ): ListChangeListener<E> {
        val inner = listChangeListener(lambda)
        val wrapper = WeakListChangeListener(inner)
        addChangeListener(wrapper)
        return inner
    }
}

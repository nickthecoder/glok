/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import kotlin.reflect.KProperty

/**
 * Creates a [SimpleProperty], where the `bean` and `beanName` are automatically set.
 *
 * [V] is the _value type_ (`Double` in the example above).
 *
 * [O] is the _property type_ (`DoubleProperty`) in the example above.
 *
 * Example usage :
 *
 *     val widthProperty by doubleProperty(1.0)
 *
 * [Property] is itself a delegate, which returns the property value. So...
 *
 *     val widthProperty by doubleProperty(1.0)
 *     var width by widthProperty
 *
 * NOTE. The value of `beanName` is assumed to be the name of the `Property`, without the `Property` suffix.
 * In the example above `by doubleProperty(1.0)` sees the name `widthProperty`, and reduces it to just `width`.
 * This is a problem when you have code such as :
 *
 *     private val privateWidthProperty by doubleProperty(1.0)
 *     val widthProperty = privateWidthProperty.asReadOnly()
 *     var width by privateWidthProperty
 *
 * In this case, the bean name would be `privateWidth`, which is wrong.
 * So there are 3 common name prefixes that are tested for :
 *
 * 1. _ (underscore)
 * 2. private
 * 3. mutable
 *
 * A warning message is issued to Glok's [log] if the bean is not found.
 *
 * For any other naming conventions, you'll have to set the bean name manually i.e.
 *
 *     val funnyWidthProperty = SimpleDoubleProperty(1.0, this, "width")
 *     var width by widthProperty
 *
 *
 * A common mistake is to use PropertyDelegate with `non-public` fields.
 * Beans must be public. So this is wrong :
 *
 *     private val widthProperty by doubleProperty(1.0)
 *     private var width by widthProperty
 *
 * Use this instead (without using a PropertyDelegate, and therefore `bean` and `beanName` are not set) :
 *
 *     private val widthProperty = SimpleDoubleProperty(1.0)
 *     private var width by widthProperty
 *
 * Bean name will be blank.
 */
class PropertyDelegate<V, O : Property<V>>(
    private val initialValue: V,
    private val propertyFactory: (Any, String, V) -> O
) {

    private var property: O? = null

    operator fun getValue(thisRef: Any, kProperty: KProperty<*>): O {


        fun createProperty(): O {
            val propName = kProperty.name
            if (propName.endsWith("Property")) {
                var beanName = propName.substring(0, propName.length - 8)

                if (! beanExists(thisRef, beanName)) {

                    if (beanName.startsWith("_")) {

                        val alternate = beanName.substring(1)
                        if (beanExists(thisRef, alternate)) {
                            beanName = alternate
                        }

                    } else if (beanName.startsWith("mutable")) {

                        val alternate = beanName.substring(7, 8).lowercase() + beanName.substring(8)
                        if (beanExists(thisRef, alternate)) {
                            beanName = alternate
                        }

                    } else if (beanName.startsWith("private")) {

                        val alternate = beanName.substring(7, 8).lowercase() + beanName.substring(8)
                        if (beanExists(thisRef, alternate)) {
                            beanName = alternate
                        }

                    } else {
                        // log.warn("Bean ${this::class.simpleName}.$beanName not found. See PropertyDelegate for help.")
                    }
                }

                val result = propertyFactory(thisRef, beanName, initialValue)
                property = result
                return result
            } else {
                throw Exception("Properties must end with `Property`")
            }
        }

        return property ?: createProperty()
    }
}

internal expect fun beanExists(thisRef: Any, beanName: String): Boolean

fun <V> simpleProperty(initialValue: V) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleProperty(value, bean, name)
}

package uk.co.nickthecoder.glok.property

/**
 *  ([PropertyFunction])
 */
open class UnaryFunction<V, A, OA : ObservableValue<A>>(
    val argA: OA,
    val lambda: (A) -> V
) : LazyObservableValue<V>(argA) {

    final override fun eval() = lambda(argA.value)

}

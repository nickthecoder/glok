/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.property.boilerplate.*


/**
 * Tracks a value of a property where we don't have the property itself, but only another property,
 * ([parentProperty]) from which we can access the required property.
 *
 * Imagine we have a class `Person`, with : `val nameProperty : StringProperty`
 *
 * Now suppose we have :
 *
 *     val personProperty : Property<Person?>
 *
 * Note that `personProperty.value` can be `null`.
 *
 * The following will give us an [ObservableValue] which changes whenever `personProperty` changes,
 * and also changes whenever that person's name changes.
 *
 *     val personNameProperty : ObservableValue<String> = IndirectObservableValue( personProperty, "<None>" ) {
 *         person -> person.nameProperty.value
 *     }
 *
 * When `personProperty.value == null`, then `personNameProperty.value == "<None>"`, not `null`.
 *
 * Alas, `personNameProperty` is not of type [ObservableString].
 * To work around the JVM's limited type system, consider creating a boilerplate class :
 *
 *     class IndirectObservableString<P : Any>(
 *         parentProperty: ObservableValue<P?>,
 *         defaultValue: String,
 *         getObservable: (P) -> ObservableValue<String>
 *     ) : IndirectObservableValue<P, String>(parentProperty, defaultValue, getObservable), ObservableString
 *
 */
open class DefaultIndirectObservableValue<P : Any, V>(

    val parentProperty: ObservableValue<P?>,

    /**
     * The default value to use when [parentProperty].value == `null`.
     */
    val defaultValue: V,

    /**
     * A lambda to get the [ObservableValue] we are interested in tracking, from the value of [parentProperty].
     */
    val getObservable: (P) -> ObservableValue<V>

) : LazyObservableValue<V>() {

    @Suppress("unused")
    private val parentPropertyListener = parentProperty.addChangeListener { _, oldParentValue, parentValue ->
        if (oldParentValue != null) {
            removeDependent(getObservable(oldParentValue))
        }

        if (parentValue != null) {
            addDependent(getObservable(parentValue))
        }
        invalidate()
    }

    override fun eval(): V {
        val parentValue = parentProperty.value
        return if (parentValue == null) {
            defaultValue
        } else {
            getObservable(parentValue).value
        }
    }

}

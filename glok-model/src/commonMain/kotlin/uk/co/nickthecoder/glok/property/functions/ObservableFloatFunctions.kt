/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*

operator fun ObservableFloat.unaryMinus() : ObservableFloat = FloatUnaryFunction(this) { a -> -a }

operator fun ObservableFloat.plus(constant: Float): ObservableFloat = FloatUnaryFunction(this) { a -> a + constant }
operator fun ObservableFloat.minus(constant: Float): ObservableFloat = FloatUnaryFunction(this) { a -> a - constant }
operator fun ObservableFloat.times(constant: Float): ObservableFloat = FloatUnaryFunction(this) { a -> a * constant }
operator fun ObservableFloat.div(constant: Float): ObservableFloat = FloatUnaryFunction(this) { a -> a / constant }
operator fun ObservableFloat.rem(constant: Float): ObservableFloat = FloatUnaryFunction(this) { a -> a % constant }
fun min( oa : ObservableFloat, b: Float) : ObservableFloat = FloatUnaryFunction(oa) { a -> if (a < b) a else b }
fun max( oa : ObservableFloat, b: Float) : ObservableFloat = FloatUnaryFunction(oa) { a -> if (a > b) a else b }

operator fun ObservableFloat.plus(other: ObservableFloat) : ObservableFloat = FloatBinaryFunction(this, other) { a, b -> a + b }
operator fun ObservableFloat.minus(other: ObservableFloat): ObservableFloat = FloatBinaryFunction(this, other) { a, b -> a - b }
operator fun ObservableFloat.times(other: ObservableFloat): ObservableFloat = FloatBinaryFunction(this, other) { a, b -> a * b }
operator fun ObservableFloat.div(other: ObservableFloat): ObservableFloat = FloatBinaryFunction(this, other) { a, b -> a / b }
operator fun ObservableFloat.rem(other: ObservableFloat): ObservableFloat = FloatBinaryFunction(this, other) { a, b -> a % b }
fun min(oa: ObservableFloat, ob: ObservableFloat): ObservableFloat = FloatBinaryFunction(oa, ob) { a, b -> if (a < b) a else b }
fun max(oa: ObservableFloat, ob: ObservableFloat): ObservableFloat = FloatBinaryFunction(oa, ob) { a, b -> if (a > b) a else b }

fun ObservableFloat.toInt(): ObservableInt = IntUnaryFunction(this) { a -> a.toInt() }
fun ObservableFloat.toDouble(): ObservableDouble = DoubleUnaryFunction(this) { a -> a.toDouble() }

operator fun ObservableOptionalFloat.times(constant: Float): ObservableOptionalFloat =
    OptionalFloatUnaryFunction(this) { a -> if (a == null) null else a * constant }


fun ObservableFloat.greaterThan(value: Float) = BooleanUnaryFunction(this) { it > value }
fun ObservableFloat.lessThan(value: Float) = BooleanUnaryFunction(this) { it < value }
fun ObservableFloat.greaterThanOrEqual(value: Float) = BooleanUnaryFunction(this) { it >= value }
fun ObservableFloat.lessThanOrEqual(value: Float) = BooleanUnaryFunction(this) { it <= value }

fun ObservableFloat.greaterThan(other: ObservableFloat) = BooleanBinaryFunction(this, other) { a, b -> a > b }
fun ObservableFloat.lessThan(other: ObservableFloat) = BooleanBinaryFunction(this, other) { a, b -> a < b }
fun ObservableFloat.greaterThanOrEqual(other: ObservableFloat) = BooleanBinaryFunction(this, other) { a, b -> a >= b }
fun ObservableFloat.lessThanOrEqual(other: ObservableFloat) = BooleanBinaryFunction(this, other) { a, b -> a <= b }

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*

operator fun ObservableDouble.unaryMinus() : ObservableDouble = DoubleUnaryFunction(this) { a -> -a }

operator fun ObservableDouble.plus(constant: Double): ObservableDouble = DoubleUnaryFunction(this) { a -> a + constant }
operator fun ObservableDouble.minus(constant: Double): ObservableDouble = DoubleUnaryFunction(this) { a -> a - constant }
operator fun ObservableDouble.times(constant: Double): ObservableDouble = DoubleUnaryFunction(this) { a -> a * constant }
operator fun ObservableDouble.div(constant: Double): ObservableDouble = DoubleUnaryFunction(this) { a -> a / constant }
operator fun ObservableDouble.rem(constant: Double): ObservableDouble = DoubleUnaryFunction(this) { a -> a % constant }
fun min( oa : ObservableDouble, b: Double) : ObservableDouble = DoubleUnaryFunction(oa) { a -> if (a < b) a else b }
fun max( oa : ObservableDouble, b: Double) : ObservableDouble = DoubleUnaryFunction(oa) { a -> if (a > b) a else b }

operator fun ObservableDouble.plus(other: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(this, other) { a, b -> a + b }
operator fun ObservableDouble.minus(other: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(this, other) { a, b -> a - b }
operator fun ObservableDouble.times(other: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(this, other) { a, b -> a * b }
operator fun ObservableDouble.div(other: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(this, other) { a, b -> a / b }
operator fun ObservableDouble.rem(other: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(this, other) { a, b -> a % b }
fun min( oa : ObservableDouble, ob: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(oa, ob) { a, b -> if (a < b) a else b }
fun max( oa : ObservableDouble, ob: ObservableDouble) : ObservableDouble = DoubleBinaryFunction(oa, ob) { a, b -> if (a > b) a else b }

fun ObservableDouble.toInt() : ObservableInt = IntUnaryFunction(this) { a -> a.toInt() }
fun ObservableDouble.toFloat() : ObservableFloat = FloatUnaryFunction(this) { a -> a.toFloat() }


fun ObservableDouble.greaterThan(value: Double) = BooleanUnaryFunction(this) { it > value }
fun ObservableDouble.lessThan(value: Double) = BooleanUnaryFunction(this) { it < value }
fun ObservableDouble.greaterThanOrEqual(value: Double) = BooleanUnaryFunction(this) { it >= value }
fun ObservableDouble.lessThanOrEqual(value: Double) = BooleanUnaryFunction(this) { it <= value }

fun ObservableDouble.greaterThan(other: ObservableDouble) = BooleanBinaryFunction(this, other) { a, b -> a > b }
fun ObservableDouble.lessThan(other: ObservableDouble) = BooleanBinaryFunction(this, other) { a, b -> a < b }
fun ObservableDouble.greaterThanOrEqual(other: ObservableDouble) = BooleanBinaryFunction(this, other) { a, b -> a >= b }
fun ObservableDouble.lessThanOrEqual(other: ObservableDouble) = BooleanBinaryFunction(this, other) { a, b -> a <= b }

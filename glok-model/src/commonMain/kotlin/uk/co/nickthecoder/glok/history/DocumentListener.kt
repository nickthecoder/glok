/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.history

import uk.co.nickthecoder.glok.util.Weak

interface DocumentListener {
    fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean)
}

fun documentListener(lambda: (document: HistoryDocument, change: Change, isUndo: Boolean) -> Unit) =
    object : DocumentListener {
        override fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean) {
            lambda(document, change, isUndo)
        }
    }

/**
 * Using weak listeners can help prevent memory leaks.
 *
 * A Weak Listener is a `wrapper` around an `inner` listener.
 * The `wrapper` contains a _weak_ reference to the `inner`
 *
 * The `wrapper` should be added to the list of listeners (via [HistoryDocument.addListener]`)
 * *
 * You have to keep a reference to `inner`, otherwise it will be garbage collected,
 * and the listener will stop working.
 *
 * When your class (which holds the returned reference to `inner`) is garbage collected,
 * there will be no strong references to `inner`, so it too will be garbage collected,
 * and the listener will stop.
 *
 * To remove a weak listener, pass `inner` or `this` to [HistoryDocument.removeListener].
 * ([HistoryDocument.removeListener] has special code which removes the `wrapper` (this),
 * even when the `inner` is passed to it).
 */
class WeakDocumentListener(inner: DocumentListener) : DocumentListener {
    private val weak = Weak(inner)

    fun actual() = weak.get()

    override fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean) {
        weak.get()?.documentChanged(document, change, isUndo)
    }
}

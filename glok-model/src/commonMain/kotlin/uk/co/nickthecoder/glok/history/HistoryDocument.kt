/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.history

/**
 * A document with a [History], allowing `undo` and `redo` actions.
 *
 * Implementing classes must ensure that they only mutate via the [History]
 * mechanism.
 *
 * If you were to move, delete or add to the document directly (without using [history]),
 * then undo / redo will break.
 */
interface HistoryDocument {

    val history: History

    fun addListener(listener: DocumentListener): DocumentListener
    fun removeListener(listener: DocumentListener)
    fun fireChange(change: Change, isUndo: Boolean)

    fun addListener(lambda: (HistoryDocument, Change, Boolean) -> Unit): DocumentListener =
        addListener(documentListener(lambda))


    fun addWeakListener(lambda: (HistoryDocument, Change, Boolean) -> Unit): DocumentListener {
        val inner = documentListener(lambda)
        val wrapper = WeakDocumentListener(inner)
        addListener(wrapper)
        return inner
    }
}

abstract class HistoryDocumentBase : HistoryDocument {

    final override val history = History(this)

    private val listeners = mutableListOf<DocumentListener>()

    override fun addListener(listener: DocumentListener): DocumentListener {
        listeners.add(listener)
        return listener
    }

    override fun removeListener(listener: DocumentListener) {
        listeners.removeAll {
            it == listener ||
                (it is WeakDocumentListener && (it.actual() == null || it.actual() == listener))
        }
    }

    override fun fireChange(change: Change, isUndo: Boolean) {
        for (l in listeners) {
            l.documentChanged(this, change, isUndo)
        }
    }
}

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener

/**
 * Wraps an existing [MutableObservableList], and prevents write access to it.
 */
internal class ReadOnlyObservableListWrapper<E>(private val actual: MutableObservableList<E>) : ObservableList<E> {

    private val readOnly = (actual as List<E>).asReadOnly()

    override fun addListener(listener: InvalidationListener) {
        actual.addListener(listener)
    }

    override fun removeListener(listener: InvalidationListener) {
        actual.removeListener(listener)
    }

    override fun addChangeListener(listener: ListChangeListener<E>) {
        actual.addChangeListener(listener)
    }

    override fun removeChangeListener(listener: ListChangeListener<E>) {
        actual.removeChangeListener(listener)
    }

    override val size get() = readOnly.size
    override fun get(index: Int) = readOnly.get(index)
    override fun isEmpty() = readOnly.isEmpty()
    override fun iterator() = readOnly.iterator()
    override fun listIterator() = readOnly.listIterator()
    override fun listIterator(index: Int) = readOnly.listIterator(index)
    override fun subList(fromIndex: Int, toIndex: Int) = readOnly.subList(fromIndex, toIndex)
    override fun lastIndexOf(element: E) = readOnly.lastIndexOf(element)
    override fun indexOf(element: E) = readOnly.indexOf(element)
    override fun containsAll(elements: Collection<E>) = readOnly.containsAll(elements)
    override fun contains(element: E) = readOnly.contains(element)
    override fun toString() = readOnly.toString()
}

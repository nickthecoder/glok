/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

/**
 * Converts values from one type to another bi-directionally.
 * The type parameters [F] and [T] can be read as `FROM` and `TO`,
 * as the [forwards] conversion converts [F] to [T].
 *
 * Most commonly used to convert a value to a String (e.g. [SpinnerBase.converter]).
 * In which case [T] is `String`.
 *
 * It can also be used to convert values to/from the same type.
 * e.g. You could have a `UnaryMinus` converter, which reverses the sign of a number.
 *
 */
interface Converter<F, T> {
    fun forwards(value: F): T
    fun backwards(value: T): F
}

interface StringConverter<V> : Converter<String, V> {
    fun toString(value: V): String
    fun fromString(str: String): V
    override fun backwards(value: V) = toString(value)
    override fun forwards(value: String) = fromString(value)
}

/**
 * Converts between an `Optional` property and a `non-Optional` property,
 * using a default value when the Optional property's value is null.
 */
class DefaultValueConverter<V : Any>(val defaultValue: V) : Converter<V?, V> {
    override fun backwards(value: V) = value
    override fun forwards(value: V?) = value ?: defaultValue
}

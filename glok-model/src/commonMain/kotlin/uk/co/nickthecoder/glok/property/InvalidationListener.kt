package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.util.Weak

interface InvalidationListener {
    fun invalidated(observable: Observable)
}

fun invalidationListener(lambda: (Observable) -> Unit) = object : InvalidationListener {
    override fun invalidated(observable: Observable) {
        lambda(observable)
    }
}


/**
 * Using weak listeners can help prevent memory leaks.
 *
 * A Weak Listener is a `wrapper` around an `inner` listener.
 * The `wrapper` contains a _weak_ reference to the `inner`
 *
 * The `wrapper` should be added to the list of listeners (via [Observable.addListener]`)
 * *
 * You have to keep a reference to `inner`, otherwise it will be garbage collected,
 * and the listener will stop working.
 *
 * When your class (which holds the returned reference to `inner`) is garbage collected,
 * there will be no strong references to `inner`, so it too will be garbage collected,
 * and the listener will stop.
 *
 * To remove a weak listener, pass `inner` or `this` to [Observable.removeListener].
 * ([Observable.removeListener] has special code which removes the `wrapper` (this),
 * even when the `inner` is passed to it).
 */
class WeakInvalidationListener(
    wraps: InvalidationListener
) : InvalidationListener {

    private val weak = Weak(wraps)

    fun actual() = weak.get()

    override fun invalidated(observable: Observable) {
        weak.get()?.invalidated(observable)
    }
}

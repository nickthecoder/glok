/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.util.Converter
import kotlin.reflect.KProperty

/**
 *
 */
interface Property<V> : ReadOnlyProperty<V> {

    /**
     * @throws [PropertyException] when setting the value while this property is bound using [bindTo] or [bindCastTo].
     */
    override var value: V

    fun isBound(): Boolean

    /**
     * The value of this property is bound to the value of property [to].
     *
     * __WARNING__. These binds use a _strong_ reference to the [to] property.
     * So if this binding is the last reference, it will prevent [to] from being garbage collected.
     * As Properties have a reference to their owning class, this could lead to a memory leak of a large
     * number of objects (not just Property [to]).
     *
     * If you attempt to set [value] when a property is bound, a [PropertyException] is thrown.
     */
    fun bindTo(to: ObservableValue<V>)

    /**
     * This is the same as [bindTo], but casting `this` to `Property<T>`,
     * suppressing the unchecked cast. I _believe_ this is fine in all cases.
     *
     * This lets us, for example bind a `Property<Any>` to a [StringProperty],
     * or an [OptionalStringProperty] to a [StringProperty].
     *
     * [T] : The value type of the `Source`.
     */
    fun <T : V> bindCastTo(bindTo: ObservableValue<T>) {
        /*
         * This UNCHECKED_CAST isn't strictly correct, but AFAIK, it works.
         * For example, when binding a Property<Any> to a Property<String> ( D=Any , S=String ),
         * we are casting Property<Any> to Property<String>, which is naughty!
         *
         * Ideally, we'd use the correct type projections throughout this package, so that the cast isn't needed.
         * Quite frankly though, the cure might be worse than the disease, because rigorous code
         * can very easily become an unreadable mess IMHO.
         * So for now at least, I'm not going to try to "fix" the design.
         * AFAIK, this works fine, and is MUCH simpler than a version without the UNCHECKED_CAST.
         */
        @Suppress("UNCHECKED_CAST")
        (this as Property<T>).bindTo(bindTo)
    }

    fun unbind()

    /**
     * This property's value is set to [other]'s value, and then both values are bound to each other.
     * i.e. if either property changes, the other will also change.
     *
     * NOTE. Unlike [bindTo], a bidirectional bind only uses _weak_ references, and therefore if this
     * bind is the last reference to Property [other], then other will be garbage collected,
     * so bidirectional binds cannot lead to memory leaks.
     *
     * @throws PropertyException If a bidirectional bind already exists between the two properties.
     */
    fun bidirectionalBind(other: Property<V>): BidirectionalBind

    /**
     * This property's value is set to [other]'s value, and then both values are bound to each other.
     * i.e. if either property changes, the other will also change.
     *
     * [converter] is used to convert this Property's value to a value suitable for property [other]
     * and vice-versa.
     * For example, if this property is a ratio in the range 0..1, then we could bind it to
     * a percentage property.
     * The `forward` direction of the [converter] would multiply by 100, and the `backward` direction
     * would divide by 100.
     *
     * The types of this Property's value and [other] Property's value do not have to be the same.
     * e.g. we could use a converter which uses `Int.toString()` in the forward direction, and
     * `String.parseInt()` in the backward direction.
     *
     * NOTE. Unlike [bindTo], a bidirectional bind only uses _weak_ references, and therefore if this
     * bind is the last reference to Property [other], then [other] will be garbage collected.
     * Bidirectional binds cannot lead to memory leaks.
     *
     * @throws [PropertyException] If a bidirectional bind already exists between the two properties.
     */
    fun <B> bidirectionalBind(other: Property<B>, converter: Converter<V, B>): BidirectionalBind =
        ConvertedBidirectionalBind(this, other, converter)

    /**
     * Removes a bidirectional bind created using [bidirectionalBind].
     *
     * Note, `a.bidirectionalUnbind( b )` is identical to `b.bidirectionalUnbind(a)`,
     * both work equally well.
     *
     * If there is no bidirectional bind between the two Properties, this is a no-op
     * and no exception is thrown.
     */
    fun bidirectionalUnbind(other: Property<*>?)

    /**
     * Lets us declare a `var` for the value of this property.
     * e.g.
     *
     *     val testProperty : ObservableValue<String> = ...
     *     val test by testProperty
     *
     * Note. [ObservableValue] has the corresponding [getValue] delegate operator.
     */
    operator fun setValue(thisRef: Any, kProperty: KProperty<*>, newValue: V) {
        value = newValue
    }
}

fun <V> property(initialValue: V) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleProperty(value, bean, name)
}

fun <V> Property<V>.asReadOnly() = ReadOnlyPropertyWrapper(this)

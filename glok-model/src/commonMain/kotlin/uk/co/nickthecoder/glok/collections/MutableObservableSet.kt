package uk.co.nickthecoder.glok.collections

/**
 * Any changes made to the underlying set will NOT cause change events to be fired.
 * Therefore, it is not advisable to keep references to the underlying set.
 */
interface MutableObservableSet<E> : MutableSet<E>, ObservableSet<E> {
    fun addAll(vararg elements: E) = addAll(elements.asList())
    fun removeAll(vararg elements: E) = removeAll(elements.toSet())
}


fun <E> MutableSet<E>.asMutableObservableSet(): MutableObservableSet<E> = MutableObservableSetWrapper(this)

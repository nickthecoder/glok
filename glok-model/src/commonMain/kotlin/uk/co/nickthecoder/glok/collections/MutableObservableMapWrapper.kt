/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener
import uk.co.nickthecoder.glok.property.WeakInvalidationListener

internal class MutableObservableMapWrapper<K : Any, V>(private val actual: MutableMap<K, V>) :
    MutableObservableMap<K, V> {

    private val listeners = mutableListOf<InvalidationListener>()

    private val changeListeners = mutableListOf<MapChangeListener<K, V>>()

    override fun addListener(listener: InvalidationListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: InvalidationListener) {
        listeners.remove(listener)
        if (listener is WeakInvalidationListener) {
            listener.actual()?.let { listeners.remove(it) }
        }
    }

    override fun addChangeListener(listener: MapChangeListener<K, V>) {
        changeListeners.add(listener)
    }

    override fun removeChangeListener(listener: MapChangeListener<K, V>) {
        changeListeners.remove(listener)
        if (listener is WeakMapChangeListener<K, V>) {
            listener.actual()?.let { changeListeners.remove(it) }
        }
    }

    private fun invalidate(changes: List<MapChange<K, V>>) {
        for (l in listeners) {
            l.invalidated(this)
        }
        for (l in changeListeners) {
            l.changed(this, changes)
        }
    }

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get() = EntriesSet()

    override val keys: MutableSet<K>
        get() = KeySet()

    override val values: MutableCollection<V>
        get() = actual.values

    override val size: Int
        get() = actual.size

    override fun isEmpty() = actual.isEmpty()

    override fun clear() {
        val changes = actual.entries.map { MapChange.removal(it.key, it.value) }
        actual.clear()
        invalidate(changes)
    }

    override fun remove(key: K): V? {
        return if (actual.keys.contains(key)) {
            val removedValue = actual.remove(key)

            @Suppress("UNCHECKED_CAST")
            val change = MapChange.removal(key, removedValue as V)
            invalidate(listOf(change))
            removedValue
        } else {
            null
        }
    }

    override fun putAll(from: Map<out K, V>) {
        val changes = from.map { MapChange.addition(this, it.key, it.value) }
        actual.putAll(from)
        invalidate(changes)
    }

    override fun put(key: K, value: V): V? {
        val change = MapChange.addition(this, key, value)
        val result = actual.put(key, value)
        invalidate(listOf(change))
        return result
    }

    override fun get(key: K): V? = actual[key]

    override fun containsValue(value: V) = actual.containsValue(value)

    override fun containsKey(key: K) = actual.containsKey(key)

    // region == KeySet ==
    /**
     * The set for [keys].
     * If keys are removed, we need to [invalidate] as if they were removed from the Map.
     * If keys are added, then throw an [UnsupportedOperationException].
     */
    private inner class KeySet : MutableSet<K> by actual.keys {

        override fun add(element: K): Boolean {
            throw UnsupportedOperationException()
        }

        override fun addAll(elements: Collection<K>): Boolean {
            throw UnsupportedOperationException()
        }

        override fun clear() {
            this@MutableObservableMapWrapper.clear()
        }

        override fun remove(element: K): Boolean {
            return if (contains(element)) {
                this@MutableObservableMapWrapper.remove(element)
                true
            } else {
                false
            }
        }

        override fun removeAll(elements: Collection<K>): Boolean {
            val changes = elements.filter { contains(it) }.map {
                @Suppress("UNCHECKED_CAST")
                MapChange.removal(it, get(it) as V)
            }
            actual.keys.removeAll(elements.toSet())
            invalidate(changes)
            return changes.isNotEmpty()
        }

        override fun retainAll(elements: Collection<K>): Boolean {
            val toRemove = keys.filter { elements.contains(it) }
            return removeAll(toRemove)
        }

        override fun iterator(): MutableIterator<K> = KeysIterator()
    }

    // endregion KeySet

    // region == KeysIterator ==

    private inner class KeysIterator : MutableIterator<K> {

        private val wrapped = actual.keys.iterator()
        private lateinit var current: K

        override fun hasNext() = wrapped.hasNext()

        override fun next(): K {
            val result = wrapped.next()
            current = result
            return result
        }

        override fun remove() {
            @Suppress("UNCHECKED_CAST")
            val value = get(current) as V
            invalidate(listOf(MapChange.removal(current, value)))
            wrapped.remove()
        }
    }

    // endregion

    // region == EntriesSet ==
    private inner class EntriesSet : MutableSet<MutableMap.MutableEntry<K, V>> {

        override fun add(element: MutableMap.MutableEntry<K, V>): Boolean {
            val result = ! containsKey(element.key)
            put(element.key, element.value)
            return result
        }

        override fun addAll(elements: Collection<MutableMap.MutableEntry<K, V>>): Boolean {
            putAll(elements.map { Pair(it.key, it.value) })
            return true
        }

        override val size: Int
            get() = this@MutableObservableMapWrapper.size

        override fun clear() {
            this@MutableObservableMapWrapper.clear()
        }

        override fun isEmpty() = this@MutableObservableMapWrapper.isEmpty()

        override fun containsAll(elements: Collection<MutableMap.MutableEntry<K, V>>): Boolean {
            return actual.entries.containsAll(elements)
        }

        override fun contains(element: MutableMap.MutableEntry<K, V>): Boolean {
            return actual.entries.contains(element)
        }

        override fun iterator(): MutableIterator<MutableMap.MutableEntry<K, V>> {
            return EntriesIterator()
        }

        override fun retainAll(elements: Collection<MutableMap.MutableEntry<K, V>>): Boolean {
            val toRemove = entries.filter { ! elements.contains(it) }
            return removeAll(toRemove)
        }

        override fun removeAll(elements: Collection<MutableMap.MutableEntry<K, V>>): Boolean {
            val toRemove = elements.filter { get(it.key) == it.value }
            var changed = false
            for (element in toRemove) {
                this@MutableObservableMapWrapper.remove(element.key)
                changed = true
            }
            return changed
        }

        override fun remove(element: MutableMap.MutableEntry<K, V>): Boolean {
            return if (contains(element)) {
                this@MutableObservableMapWrapper.remove(element.key)
                true
            } else {
                false
            }
        }
    }

    // endregion EntriesMap

    // region == EntriesIterator ==

    private inner class EntriesIterator : MutableIterator<MutableMap.MutableEntry<K, V>> {

        private val wrapped = actual.entries.iterator()
        private lateinit var current: MutableMap.MutableEntry<K, V>

        override fun hasNext() = wrapped.hasNext()

        override fun next(): MutableMap.MutableEntry<K, V> {
            current = EntriesEntry(wrapped.next())
            return current
        }

        override fun remove() {
            invalidate(listOf(MapChange.removal(current.key, current.value)))
            wrapped.remove()
        }
    }

    private inner class EntriesEntry(val wrapped: MutableMap.MutableEntry<K, V>) : MutableMap.MutableEntry<K, V> {
        override val key: K
            get() = wrapped.key
        override val value: V
            get() = wrapped.value

        override fun setValue(newValue: V): V {
            val oldValue = value
            invalidate(listOf(MapChange.addition(this@MutableObservableMapWrapper, key, newValue)))
            wrapped.setValue(newValue)
            return oldValue
        }
    }

    // endregion EntriesIterator
}

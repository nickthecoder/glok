/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

interface MapChange<out K, out V> {

    val isRemoval: Boolean
    val key: K
    val value: V
    val oldValue: V?

    companion object {
        /**
         * Creates a [SetChange] describing items removed from an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         */
        fun <K, V> removal(key: K, value: V): MapChange<K, V> = MapChangeImpl(true, key, value, null)

        /**
         * Creates a [SetChange] describing items added to an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         */
        fun <K, V> addition(map: Map<K, V>, key: K, value: V): MapChange<K, V> =
            MapChangeImpl(false, key, value, map[key])
    }
}


internal class MapChangeImpl<K, V>(

    override val isRemoval: Boolean,
    override val key: K,
    override val value: V,
    override val oldValue: V?

) : MapChange<K, V> {

    override fun toString() = "Map Change"
}

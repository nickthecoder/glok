package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener
import uk.co.nickthecoder.glok.property.WeakInvalidationListener

/**
 * Wraps an existing [MutableList].
 * Note, and changes made to the [actual] list will not be noticed by listeners.
 */
internal open class MutableObservableSetWrapper<E>(private val actual: MutableSet<E>) : MutableObservableSet<E> {

    private val listeners = mutableListOf<InvalidationListener>()

    private val changeListeners = mutableListOf<SetChangeListener<E>>()

    override fun addListener(listener: InvalidationListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: InvalidationListener) {
        listeners.remove(listener)
        if (listener is WeakInvalidationListener) {
            listener.actual()?.let { listeners.remove(it) }
        }
    }

    override fun addChangeListener(listener: SetChangeListener<E>) {
        changeListeners.add(listener)
    }

    override fun removeChangeListener(listener: SetChangeListener<E>) {
        changeListeners.remove(listener)
        if (listener is WeakSetChangeListener<E>) {
            listener.actual()?.let { changeListeners.remove(it) }
        }
    }


    override val size: Int
        get():Int {
            return actual.size
        }

    override fun isEmpty(): Boolean {
        return actual.isEmpty()
    }

    override fun iterator(): MutableIterator<E> {
        return ObservableMutableIterator(actual.iterator())
    }

    override fun containsAll(elements: Collection<E>): Boolean {
        return actual.containsAll(elements)
    }

    override fun contains(element: E): Boolean {
        return actual.contains(element)
    }

    private fun invalidate(change: SetChange<E>) {
        for (l in listeners) {
            l.invalidated(this)
        }
        for (l in changeListeners) {
            l.changed(this, change)
        }
    }


    override fun clear() {
        val change = SetChange.removal(actual.toSet())
        actual.clear()
        invalidate(change)
    }

    override fun addAll(elements: Collection<E>): Boolean {
        val change = SetChange.addition(elements.filter { !actual.contains(it) }.toSet())
        return if (actual.addAll(elements)) {
            invalidate(change)
            true
        } else {
            false
        }
    }

    override fun add(element: E): Boolean {
        return if (actual.add(element)) {
            val change = SetChange.addition(setOf(element))
            invalidate(change)
            true
        } else {
            false
        }
    }

    override fun retainAll(elements: Collection<E>): Boolean {
        val toRemove = actual.filter { !elements.contains(it) }
        return removeAll(toRemove)
    }

    override fun removeAll(elements: Collection<E>): Boolean {
        val willRemove = elements.filter { actual.contains(it) }.toSet()
        return if (willRemove.isEmpty()) {
            false
        } else {
            actual.removeAll(willRemove)
            invalidate(SetChange.removal(willRemove))
            true
        }
    }

    override fun remove(element: E): Boolean {
        return if (actual.remove(element)) {
            invalidate(SetChange.removal(setOf(element)))
            true
        } else {
            false
        }
    }

    override fun toString() = actual.toString()


    private inner class ObservableMutableIterator(val inner: MutableIterator<E>) : MutableIterator<E> {
        private var lastItem: E? = null

        override fun hasNext() = inner.hasNext()

        override fun next(): E {
            val result = inner.next()
            lastItem = result
            return result
        }

        override fun remove() {
            inner.remove()
            lastItem?.let { invalidate(SetChange.addition(setOf(it))) }
        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.history

import uk.co.nickthecoder.glok.collections.ObservableListSize
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.BooleanBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.SimpleIntProperty
import uk.co.nickthecoder.glok.property.boilerplate.doubleProperty
import uk.co.nickthecoder.glok.property.functions.greaterThan
import uk.co.nickthecoder.glok.property.functions.lessThan
import uk.co.nickthecoder.glok.util.currentTimeMillis

/**
 * The mechanism underlying `undo` and `redo`.
 * It is called `History`, because I think of `undo` as going back in time, and `redo` as going forward in time.
 *
 * You can think of History being a list of changes made to a document.
 * Internally, History keeps an `index` into this list. If the `index` is 0, then we cannot undo.
 * If the `index` is at the end of the list, then we cannot redo.
 *
 * To make a change to a document, we create a [Change] object, add it to the history, and then apply the [Change].
 *
 * Any [Change]s after the old `index` are lost. For example :
 *     * Start with an empty document (`index` is 0)
 *     * Add the word "Hello" (`index` is 1)
 *     * Undo (so we are back to an empty document) (`index` is 0 again)
 *     * Add the word "Goodbye" (`index` is 1 again)
 *
 * The [Change] which added the word "Hello" is lost forever.
 *
 * Up until now, I've implied that the History is a list of changes, but that's not quite correct.
 * History is a list of [Batch]es, where a [Batch] contains one or more [Change].
 * This is because [Change]s are very simplistic, and complicated changes are implemented as a [Batch] of simple
 * [Change]s. For example, to replace the word "Hi" with the word "Hello" is a deletion followed by an insertion.
 * There is no [Change] for a `replacement`.
 *
 * Undo and redo works on [Batch]es, not individual [Change]s.
 *
 * Every [Change] instance must have enough information to `undo` and `redo` that change.
 *
 * ## Merging Changes
 *
 * Consider what happens when you type the word "Hello", and then press the `Undo` button.
 * Each individual key-press creates an `insert` [Change]. But for efficiency (in both memory and speed),
 * these changes can be merged together. So we end up with a single [Batch] containing a single `insert` [Change]
 * containing the word "Hello".
 *
 * Merging is only allowed on the last [Change] of the current [Batch].
 *
 * Merging is only permitted within a short time-window. For example, if you type "Hello", then have some coffee,
 * then type " World", the result is two Batches, and you can undo/redo each individually.
 * [mergeTimeThreshold] defaults to 10,000 milliseconds (10 seconds).
 * As this is an arbitrary value, `TextDocument`s bind this to `GlokSettings.historyMergeTimeThresholdProperty`.
 *
 * ## Change Implementations
 *
 * Each concrete implementation of [HistoryDocument] must be able to create [Change] objects,
 * but the public API must NOT expose the [ChangeImpl] interface.
 * Only [History] should have access to the  change's `undo()` and `redo()` methods.
 * Calling these from anywhere else will break the undo/redo mechanism.
 *
 * ## Class Diagram
 *
 *        ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐           ┏━━━━━━━━━━━━━━━━━━━━━┓
 *        ┆ HistoryDocument   ┆           ┃       History       ┃
 *        ┆                   ┆           ┃                     ┃
 *        ┆ history           ├───────────┨ document            ┃         ┏━━━━━━━━━━━━━━━━━━━━━┓
 *    ┌──▶┆                   ┆           ┃ undoable : Boolean  ┃         ┃        Batch        ┃
 *    │   ┆                   ┆           ┃ redoable : Boolean  ┃◆────────┨                     ┃
 *    │   └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘           ┃ isSaved : Boolean   ┃         ┃                     ┃
 *    │                                   ┃                     ┃         ┃                     ┃
 *    │                                   ┃ undo()              ┃    ┌───◆┃                     ┃
 *    │                                   ┃ redo()              ┃    │    ┗━━━━━━━━━━━━━━━━━━━━━┛
 *    │                                   ┃ saved()             ┃    │
 *    │  ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐           ┃                     ┃    │
 *    │  ┊       Change       ┊           ┗━━━━━━━━━━━━━━━━━━━━━┛    │
 *    │  ┊                    ┊                                      │
 *    └──┤ document           ├──────────────────────────────────────┘
 *       ┊                    ┊
 *       ┊                    ┊
 *       └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘
 *                  △
 *                  │
 *        ┌┄┄┄┄┄┄┄┄┄┴┄┄┄┄┄┄┄┄┄┄┐
 *        ┊     ChangeImpl     ┊
 *        ┊                    ┊
 *        ┊                    ┊
 *        ┊ undo()             ┊
 *        ┊ redo()             ┊
 *        └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘
 *
 *
 * ## Notes
 *
 * [History] is used by Glok's `TextArea` and `StyledTextArea`, but it can also application developers for their
 * own documents.
 * It is intended to be general purpose, and includes features which `glok-core` doesn't use
 * (such as [SkippableChange]).
 *
 * [History] is part of the `glok-model` sub-project (not `glok-core`), so that application developers
 * can use this history mechanism within their model, with no dependency on `glok-core`.
 * For example, suppose I write a diagram editor, with classes `Diagram` and `DiagramView`.
 * The `Diagram` class has no GUI component, and therefore shouldn't depend on `glok-core`.
 */
class History(val document: HistoryDocument) {

    var maxBatches: Int = 30

    private val batches = mutableListOf<BatchImpl>().asMutableObservableList()

    private val currentIndexProperty = SimpleIntProperty(0)

    /**
     * The time of the last batch that was added.
     * Reset to 0 if undo/redo is called.
     */
    private var timestamp: Double = currentTimeMillis()

    /**
     * The time between changes in milliseconds, where changes can be merged.
     * The default is 10,000 milliseconds (10 seconds).
     * i.e. if we type with less than 10 seconds between keystrokes, the changes will be merged to form
     * a single batch, so undo/redo will not operate one character at a time.
     *
     * See `GlokSettings.historyMergeTimeThresholdProperty`
     */
    val mergeTimeThresholdProperty by doubleProperty(10_000.0)
    var mergeTimeThreshold by mergeTimeThresholdProperty

    /**
     * The index into [batches] where new [Batch]es will be added.
     * If the index == 0, then there is no more to undo.
     * If `index == history.size`, then there is nothing to redo.
     */
    private var currentIndex by currentIndexProperty


    /**
     * [currentIndexProperty] value when [saved] was last called, or -1 if the document hasn't been saved.
     */
    private val savedIndexProperty = SimpleIntProperty(- 1)
    private var savedIndex by savedIndexProperty

    /**
     * `true` if the document was last [saved] at the current point in the history.
     * i.e. it returns `true` immediately after [saved] is called, but also returns `true` if changes
     * are made, and then `undo` is performed to get back to the point that [saved] was called.
     */
    val isSavedProperty: ObservableBoolean =
        BooleanBinaryFunction(currentIndexProperty, savedIndexProperty) { current, saved -> current == saved }
    val isSaved by isSavedProperty

    val undoableProperty: ObservableBoolean = currentIndexProperty.greaterThan(0)
    val undoable by undoableProperty

    val redoableProperty: ObservableBoolean = currentIndexProperty.lessThan(ObservableListSize(batches))
    val redoable by redoableProperty


    private var currentBatch: BatchImpl? = null

    /**
     * Updates [isSavedProperty].
     */
    fun saved() {
        savedIndexProperty.value = currentIndexProperty.value
    }

    fun clear() {
        batches.clear()
        currentIndex = 0
    }

    fun currentBatch(): Batch? = currentBatch

    fun isBatchStarted() = currentBatch != null

    fun beginBatch(label: String = ""): Batch {
        if (currentBatch != null) {
            throw IllegalStateException("History is already within a batch")
        }

        val batch = BatchImpl(label)
        currentBatch = batch
        return batch
    }

    fun endBatch() {

        try {
            val currentBatch = currentBatch ?: return
            if (currentBatch.changes.isNotEmpty()) {
                val now = currentTimeMillis()
                if (savedIndex > currentIndex) {
                    savedIndex = - 1
                }

                while (batches.size > currentIndex) {
                    batches.removeAt(batches.size - 1)
                }

                val firstChange = (currentBatch.changes.first() as ChangeImpl)
                val previousBatch = batches.lastOrNull()
                if (previousBatch != null &&
                    currentBatch.changes.size == 1 && previousBatch.changes.size == 1 &&
                    now < timestamp + mergeTimeThreshold &&
                    firstChange.canMergeWith(previousBatch.changes.first())
                ) {
                    firstChange.mergeWith(previousBatch.changes.first())
                } else {
                    if (maxBatches > 0) {
                        batches.add(currentBatch)
                        if (batches.size >= maxBatches) {
                            batches.removeFirst()
                        } else {
                            currentIndex ++
                        }
                    }
                }

                timestamp = now
            }
        } finally {
            currentBatch = null
        }
    }

    fun abortBatch() {
        currentBatch?.let {
            it.undo()
            this.currentBatch = null
        }
    }

    /**
     * Begins a new [Batch], runs the [block], and then closes the [Batch].
     */
    fun batch(label: String = "", block: Batch.() -> Unit) {
        val batch = beginBatch(label)
        try {
            batch.block()
        } finally {
            endBatch()
        }
    }

    fun batch(block: Batch.() -> Unit) = batch("", block)

    fun undo(skipSelectionChanges: Boolean = false): Change? {
        var result: Change? = null
        timestamp = 0.0

        if (currentBatch == null) {
            while (undoable) {
                currentIndex --
                val batch = batches[currentIndex]
                result = batch.undo()
                if (! skipSelectionChanges || ! batch.isSkippable()) {
                    break
                }
            }
        } else {
            throw IllegalStateException("Cannot undo while in the middle of a batch")
        }
        return result
    }

    fun redo(skipSelectionChanges: Boolean = false): Change? {
        var result: Change? = null
        timestamp = 0.0

        if (currentBatch == null) {
            while (redoable) {
                val batch = batches[currentIndex]
                currentIndex ++
                result = batch.redo()
                if (! skipSelectionChanges || ! batch.isSkippable()) {
                    break
                }
            }
        } else {
            throw IllegalStateException("Cannot undo while in the middle of a batch")
        }
        return result
    }

    inner class BatchImpl(label: String) : Batch {

        override var label: String = label
            get() = field.ifEmpty {
                when (changes.size) {
                    0 -> "Nothing"
                    1 -> changes.first().label
                    else -> changes.first().label + " ..."
                }
            }

        val changes = mutableListOf<Change>()

        override fun isSkippable(): Boolean {
            for (change in changes) {
                if (change !is SkippableChange) {
                    return false
                }
            }
            return true
        }

        fun undo(): Change? {
            changes.reversed().forEach { change ->
                (change as? ChangeImpl)?.undo()
                document.fireChange(change, isUndo = true)
            }
            return changes.firstOrNull()
        }

        fun redo(): Change? {
            changes.forEach { change ->
                (change as? ChangeImpl)?.redo()
                document.fireChange(change, isUndo = false)
            }
            return changes.lastOrNull()
        }

        override fun addChange(change: Change?) {
            if (change !is ChangeImpl) return
            if (change.document !== document) throw IllegalArgumentException("Documents do not match")

            val prevChange = changes.lastOrNull()

            change.redo()
            document.fireChange(change, isUndo = false)
            if (prevChange !== null && change.canMergeWith(prevChange)) {
                change.mergeWith(prevChange)
                return
            }
            changes.add(change)
        }

        override fun toString(): String {
            return "Batch $label {\n    ${changes.joinToString(separator = "\n    ") { it.label }}\n}"
        }

    }

}

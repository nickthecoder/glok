/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

open class ReadOnlyPropertyWrapper<V, P : Property<V>>(private val wraps: Property<V>) : ReadOnlyProperty<V> {

    override val bean: Any? get() = wraps.bean

    override val beanName: String? get() = wraps.beanName

    override val value: V get() = wraps.value

    internal fun listeners() = if (wraps is ObservableValueBase<*>) wraps.listeners() else emptyList<Any>()
    internal fun changeListeners() = if (wraps is ObservableValueBase<*>) wraps.changeListeners() else emptyList<Any>()

    override fun addListener(listener: InvalidationListener) {
        wraps.addListener(listener)
    }

    override fun addBindListener(listener: InvalidationListener) {
        wraps.addBindListener(listener)
    }

    override fun removeListener(listener: InvalidationListener) {
        wraps.removeListener(listener)
    }

    override fun addChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        wraps.addChangeListener(listener)
    }

    override fun addBindChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        wraps.addBindChangeListener(listener)
    }

    override fun removeChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        wraps.removeChangeListener(listener)
    }

    override fun toString(): String {
        val bean = bean
        val beanName = beanName
        return if (bean == null || beanName == null) {
            "ReadOnlyWrapper of anonymous property = $value"
        } else {
            "ReadOnlyWrapper: ${bean::class.simpleName} $beanName = $value"
        }
    }

}

package uk.co.nickthecoder.glok.property

/**
 * An [ObservableValue]
 */
abstract class LazyObservableValue<V>(vararg dependsOn: ObservableValue<*>) : ObservableValueBase<V>() {

    private class CachedValue<V>(val value: V)

    /**
     * V May or may not be a nullable type. So if we stored the cached value as simply V?, then we wouldn't
     * be able to distinguish between an un-cached value (null) and a cached value which happens to have the
     * value null. So we wrap the cached value in a class. A cached null value will be :
     * `cache = CachedValue(null)` and an un-cached value is `cache = null`.
     */
    private var cache: CachedValue<V>? = null

    private val strongListener = invalidationListener {
        invalidate()
    }
    private val weakListener = WeakInvalidationListener(strongListener)

    protected fun invalidate() {
        val oldCache = cache
        val newValue = eval()
        cache = CachedValue(newValue)

        if (oldCache != null) {
            fire(oldCache.value, newValue)
        }
    }

    override val value: V
        get() {
            val cached = cache
            return if (cached != null) {
                cached.value
            } else {
                val result = eval()
                cache = CachedValue(result)
                result
            }
        }


    init {
        for (d in dependsOn) {
            d.addBindListener(weakListener)
        }
    }

    override fun addChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        // For change listeners to work correctly, we must know the value NOW,
        // so that we can supply the old value when the value changes.
        // Without this, ChangeListeners won't fire
        value

        super.addChangeListener(listener)
    }

    override fun addBindChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        value
        super.addBindChangeListener(listener)
    }

    fun addDependent(d: ObservableValue<*>) {
        d.addBindListener(weakListener)
    }

    fun removeDependent(d: ObservableValue<*>) {
        d.removeListener(weakListener)
    }

    abstract fun eval(): V

}

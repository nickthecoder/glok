package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener


/**
 * A mutable list, which can be observed by [ListChangeListener]s and [InvalidationListener]s.
 *
 * Note, the collection names in Glok use Kotlin's naming convention.
 * So an [ObservableList] is not mutable, whereas [MutableObservableList] is.
 *
 * Comparing Glok with JavaFX :
 * * Glok : ObservableList <-> JavaFX : ReadOnlyObservableList
 * * Glok : MutableObservableList <-> JavaFX : ObservableList
 *
 * The easiest way to create a MutableObservableList is to wrap an existing (non-observable) list using [asMutableObservableList] :
 *
 *     val myObservableList = myMutableList.asObservableList()
 *
 * Afterwards, you should never modify `myMutableList` directly, as those changes will not
 * be heard by any [ListChangeListener]s, or [InvalidationListener]s.
 *
 * See [ObservableList] for a class diagram.
 */
interface MutableObservableList<E> : MutableList<E>, ObservableList<E> {

    fun addAll(vararg items: E) = addAll(items.asList())

    fun removeBetween(indices: IntRange)

    fun asReadOnly(): ObservableList<E> = ReadOnlyObservableListWrapper(this)
}

/**
 * Wraps a _regular_ list, creating a [MutableObservableList].
 *
 * Any changes made to the underlying list will NOT cause change events to be fired.
 * Therefore, it is not advisable to keep references to the underlying list.
 */
fun <E> MutableList<E>.asMutableObservableList(): MutableObservableList<E> = MutableObservableListWrapper(this)

/**
 * Sync `this` mutable list with [other].
 * Returns a [ListChangeListener] for `this` list.
 * To stop syncing, remove the listener using [MutableObservableList.removeListener].
 * As Glok uses weak references for listeners, you must keep a reference to this listener, otherwise the
 * garbage collector will end the sync.
 *
 * While a list is synced, you must NOT update this list, only [other] can be changed.
 * If you do mutate the list, you will likely get [IndexOutOfBoundsException] or [IllegalStateException] thrown.
 */
fun <E> MutableObservableList<E>.syncWith(other: MutableObservableList<out E>): ListChangeListener<E> {
    clear()
    addAll(other)
    return other.addChangeListener { _, change ->
        if (change.isReplacement()) {
            for (i in change.added.indices) {
                set(change.from + i, other[change.from + i])
            }
        } else {
            if (change.removed.isNotEmpty()) {
                for (i in change.removed.size - 1 downTo 0) {
                    val item = change.removed[i]
                    if (removeAt(change.from + i) !== item) {
                        throw IllegalStateException("Out of sync. Did you mutable this list?")
                    }
                }
            }
            if (change.added.isNotEmpty()) {
                addAll(change.from, change.added)
            }
        }
    }
}

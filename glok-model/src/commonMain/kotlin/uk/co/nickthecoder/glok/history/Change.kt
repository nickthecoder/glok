/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.history

/**
 * See [History] and [Batch].
 */
interface Change {

    val document: HistoryDocument

    val label: String

    /**
     * Adds this [Change] to the current [Batch].
     *
     * @throw Exception if we are not within a [Batch].
     */
    fun addToCurrentBatch() {
        val batch = document.history.currentBatch() ?: throw Exception("Not within a Batch")
        batch.addChange(this)
    }

    /**
     * Begins a new [Batch], applies this [Change], and ends the [Batch].
     *
     * @throw Exception if we are already within a [Batch].
     */
    fun now() {
        document.history.batch {
            addChange(this@Change)
        }
    }

    /**
     * Begins a new [Batch] with the given [label], applies this [Change], and ends the [Batch].
     *
     * @throw Exception if we are already within a [Batch].
     */
    fun now(label: String) {
        document.history.batch {
            addChange(this@Change)
        }
    }
}

/**
 * All concrete implementations of [Change] must also implement this.
 * However, the public API must never expose this implementation, because [undo] and [redo]
 * must only be called by [History].
 */
interface ChangeImpl : Change {

    fun undo()

    fun redo()

    /**
     * When typing, lots of [Change] objects will be created.
     * Undoing / redoing each character isn't what the user expects.
     * So instead, [Change]s have the option to merge.
     *
     * If this returns `true`, then [mergeWith] must succeed.
     */
    fun canMergeWith(previous: Change): Boolean = false

    /**
     * Mutates [previous], so that it also encompasses the changes in `this`.
     *
     * Typically, this method will throw an exception if [previous] cannot be merged.
     * It is vital that [canMergeWith] returned `false`, so that [mergeWith] is not called, and therefore
     * the exception is never thrown.
     */
    fun mergeWith(previous: Change) {}

}

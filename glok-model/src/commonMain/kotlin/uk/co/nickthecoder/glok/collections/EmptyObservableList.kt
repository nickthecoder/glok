package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener

private class EmptyObservableList<E>() : ObservableList<E> {

    private val actual = emptyList<E>()


    override fun addListener(listener: InvalidationListener) {}

    override fun removeListener(listener: InvalidationListener) {}

    override fun addChangeListener(listener: ListChangeListener<E>) {}

    override fun removeChangeListener(listener: ListChangeListener<E>) {}


    override val size: Int get() = 0

    override fun get(index: Int) = actual.get(index)

    override fun isEmpty() = true

    override fun iterator() = actual.iterator()

    override fun listIterator() = actual.listIterator()

    override fun listIterator(index: Int) = actual.listIterator(index)

    override fun subList(fromIndex: Int, toIndex: Int) = actual.subList(fromIndex, toIndex)

    override fun lastIndexOf(element: E) = actual.lastIndexOf(element)

    override fun indexOf(element: E) = actual.indexOf(element)

    override fun containsAll(elements: Collection<E>) = actual.containsAll(elements)

    override fun contains(element: E) = actual.contains(element)

}

private val emptyObservableList = EmptyObservableList<Any>()

/*
 * We are casting an ObservableList<Any?>> to ObservableSet<E>, which is usually WRONG.
 * But as the set is empty, and cannot be added to, then we should never run into
 * type issues, despite the element types being different.
 */
@Suppress("UNCHECKED_CAST")
fun <E> emptyObservableList() : ObservableList<E> = emptyObservableList as ObservableList<E>

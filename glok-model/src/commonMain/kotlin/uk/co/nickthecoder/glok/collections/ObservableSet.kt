/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener
import uk.co.nickthecoder.glok.property.Observable

/**
 * A set, which can be observed by [SetChangeListener]s and [InvalidationListener]s.
 *
 * Note, the collection names in Glok use Kotlin's naming convention.
 * So an ObservableSet is not mutable, whereas [MutableObservableSet] is.
 *
 * Comparing Glok with JavaFX :
 * * Glok : ObservableSet <-> JavaFX : ReadOnlyObservableSet
 * * Glok : MutableObservableSet <-> JavaFX : ObservableSet
 *
 * The easiest way to create a MutableObservableSet is to wrap an existing (non-observable) set using [asMutableObservableList] :
 *
 *     val myObservableSet = myMutableSet.asObservableSet()
 *
 * Afterwards, you should never modify `myMutableSet` directly, as those changes will not
 * be heard by any [SetChangeListener]s, or [InvalidationListener]s.
 *
 * For a class diagram, see [ObservableList], and replace `List` with `Set`. The structure is identical.
 */
interface ObservableSet<E> : Set<E>, Observable {
    fun addChangeListener(listener: SetChangeListener<E>)
    fun removeChangeListener(listener: SetChangeListener<E>)

    fun addChangeListener(
        lambda: (set: ObservableSet<E>, change: SetChange<E>) -> Unit
    ): SetChangeListener<E> {
        val listener = object : SetChangeListener<E> {
            override fun changed(
                set: ObservableSet<@UnsafeVariance E>, change: SetChange<@UnsafeVariance E>
            ) {
                lambda(this@ObservableSet, change)
            }
        }
        addChangeListener(listener)
        return listener
    }

    fun addWeakChangeListener(
        lambda: (set: ObservableSet<E>, change: SetChange<E>) -> Unit
    ): SetChangeListener<E> {
        val inner = setChangeListener(lambda)
        val wrapper = WeakSetChangeListener(inner)
        addChangeListener(wrapper)
        return inner
    }
}

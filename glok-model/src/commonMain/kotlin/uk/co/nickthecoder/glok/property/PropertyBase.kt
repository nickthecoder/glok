/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.util.Converter

abstract class PropertyBase<V>(

    override val bean: Any? = null,
    override val beanName: String? = null

) : ObservableValueBase<V>(), Property<V> {

    protected var boundTo: ObservableValue<V>? = null
    private var boundToListener: ChangeListener<V, ObservableValue<V>>? = null

    /**
     * A horrible bodge...
     * While a property is bound to another property, [value]'s getter is taken from the bound property,
     * not `this` property's value. When we [unbind], this property's value must NOT appear to change.
     * i.e. its value must not change, and no [InvalidationListener]s nor [ChangeListener]s must fire.
     * So we need to SET this property's value to the old bound property's value, without firing listeners.
     * We do this by setting [unbinding] to true, change the value of this property's value to the old bound property's
     * value, and then set [unbinding] back to false.
     * [changedValue] will not fire events when [unbinding] == true.
     *
     * If you can see a better solution, (without breaking backwards compatibility) please implement it! ;-)
     */
    private var unbinding = false

    private val bidirectionalBinds = mutableListOf<BidirectionalBind>()

    protected fun changedValue(block: () -> V) {
        if (unbinding) {
            block()
            return
        }
        if (boundTo != null) {
            throw PropertyException(this, "Cannot set a bound property : $this")
        }
        val oldValue = value
        val newValue = block()
        if (oldValue != newValue) {
            fire(oldValue, newValue)
        }
    }

    /**
     * Called when the bound value changes.
     */
    protected open fun boundValueChanged(old: V, new: V) {
        fire(old, new)
    }

    override fun isBound() = boundTo != null

    override fun bindTo(to: ObservableValue<V>) {
        if (isBound()) {
            // Hmm, should we silently unbind, or throw an exception if the property is already bound?
            //unbind()
            throw PropertyException(this, "Property is already bound : $this")
        }

        val oldValue = value
        val newValue = to.value
        boundTo = to

        val listener = boundToListener
        if (listener == null) {
            boundToListener = to.addBindChangeListener { _, old, new ->
                boundValueChanged(old, new)
            }
        } else {
            to.addBindChangeListener(listener)
        }
        if (newValue != oldValue) {
            boundValueChanged(oldValue, newValue)
        }
    }

    override fun unbind() {
        boundTo?.let { oldBinding ->
            unbinding = true
            try {
                boundTo = null
                boundToListener?.let { oldBinding.removeChangeListener(it) }
                if (oldBinding.value != value) {
                    value = oldBinding.value
                }
            } finally {
                unbinding = false
            }
        }
    }


    override fun bidirectionalBind(other: Property<V>): BidirectionalBind {
        bidirectionalBinds.firstOrNull {
            it.propertyA === this && it.propertyB === other ||
                it.propertyB === this && it.propertyA === other
        }?.let {
            throw PropertyException(this, "A bidirectional bind already exists between these two properties")
        }
        value = other.value
        val bind = SimpleBidirectionalBind(this, other)
        bidirectionalBinds.add(bind)
        return bind
    }

    override fun <B> bidirectionalBind(other: Property<B>, converter: Converter<V, B>): BidirectionalBind {
        bidirectionalBinds.firstOrNull {
            it.propertyA === this && it.propertyB === other ||
                it.propertyB === this && it.propertyA === other
        }?.let {
            throw PropertyException(this, "A bidirectional bind already exists between these two properties")
        }
        value = converter.backwards(other.value)
        val bind = ConvertedBidirectionalBind(this, other, converter)
        bidirectionalBinds.add(bind)
        return bind
    }

    override fun bidirectionalUnbind(other: Property<*>?) {
        // remove any binds whose properties have been garbage collected.
        bidirectionalBinds.removeAll {
            it.propertyA == null || it.propertyB == null
        }

        bidirectionalBinds.filter {
            val propA = it.propertyA
            val propB = it.propertyB
            propA == null ||
                propB == null ||
                (propA === this && propB === other) ||
                (propB === this && propA === other)
        }.forEach {
            bidirectionalBinds.remove(it)
            it.tidyUp()
        }
    }

    override fun toString(): String {
        val bean = bean
        val beanName = beanName
        return if (bean == null || beanName == null) {
            "${this::class.simpleName} = $value"
        } else {
            "${this::class.simpleName} ${bean::class.simpleName}.$beanName = $value"
        }
    }
}

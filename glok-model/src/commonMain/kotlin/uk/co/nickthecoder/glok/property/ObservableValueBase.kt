package uk.co.nickthecoder.glok.property

abstract class ObservableValueBase<V> : ObservableValue<V> {

    private val bindListeners = mutableListOf<InvalidationListener>()
    private val bindChangeListeners = mutableListOf<ChangeListener<V, ObservableValue<V>>>()

    private val listeners = mutableListOf<InvalidationListener>()
    private val changeListeners = mutableListOf<ChangeListener<V, ObservableValue<V>>>()

    internal fun listeners(): List<InvalidationListener> = listeners
    internal fun changeListeners(): List<ChangeListener<V, ObservableValue<V>>> = changeListeners

    override fun addListener(listener: InvalidationListener) {
        listeners.add(listener)
    }

    override fun addBindListener(listener: InvalidationListener) {
        bindListeners.add(listener)
    }

    override fun removeListener(listener: InvalidationListener) {
        listeners.remove(listener)
        bindListeners.remove(listener)
        if (listener is WeakInvalidationListener) {
            listener.actual()?.let {
                listeners.remove(it)
                bindListeners.remove(it)
            }
        }
    }

    override fun addChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        changeListeners.add(listener)
    }

    override fun addBindChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        bindChangeListeners.add(listener)
    }

    override fun removeChangeListener(listener: ChangeListener<V, ObservableValue<V>>) {
        changeListeners.remove(listener)
        bindChangeListeners.remove(listener)
        if (listener is WeakChangeListener<V, ObservableValue<V>>) {
            listener.actual()?.let {
                changeListeners.remove(it)
                bindChangeListeners.remove(it)
            }
        }
    }

    protected fun fire(old: V, new: V) {
        // It is important that ChangeListeners fire before InvalidationListeners to help two properties
        // appearing to change atomically.
        // For example, SingleSelectionModel binds its selectedItem and selectedIndex.
        // If the order of these for loops were reversed, then an InvalidationListener could fire
        // while the two properties are inconsistent with each other.
        if (bindChangeListeners.isNotEmpty()) {
            for (l in bindChangeListeners.toList()) {
                l.changed(this, old, new)
            }
        }
        if (bindListeners.isNotEmpty()) {
            for (l in bindListeners.toList()) {
                l.invalidated(this)
            }
        }

        if (changeListeners.isNotEmpty()) {
            for (l in changeListeners.toList()) {
                l.changed(this, old, new)
            }
        }
        if (listeners.isNotEmpty()) {
            for (l in listeners.toList()) {
                l.invalidated(this)
            }
        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*

operator fun ObservableInt.unaryMinus(): ObservableInt = IntUnaryFunction(this) { a -> - a }

operator fun ObservableInt.plus(constant: Int): ObservableInt = IntUnaryFunction(this) { a -> a + constant }
operator fun ObservableInt.minus(constant: Int): ObservableInt = IntUnaryFunction(this) { a -> a - constant }
operator fun ObservableInt.times(constant: Int): ObservableInt = IntUnaryFunction(this) { a -> a * constant }
operator fun ObservableInt.div(constant: Int): ObservableInt = IntUnaryFunction(this) { a -> a / constant }
operator fun ObservableInt.rem(constant: Int): ObservableInt = IntUnaryFunction(this) { a -> a % constant }
fun min(oa: ObservableInt, b: Int): ObservableInt = IntUnaryFunction(oa) { a -> if (a < b) a else b }
fun max(oa: ObservableInt, b: Int): ObservableInt = IntUnaryFunction(oa) { a -> if (a > b) a else b }

operator fun ObservableInt.plus(other: ObservableInt): ObservableInt = IntBinaryFunction(this, other) { a, b -> a + b }
operator fun ObservableInt.minus(other: ObservableInt): ObservableInt = IntBinaryFunction(this, other) { a, b -> a - b }
operator fun ObservableInt.times(other: ObservableInt): ObservableInt = IntBinaryFunction(this, other) { a, b -> a * b }
operator fun ObservableInt.div(other: ObservableInt): ObservableInt = IntBinaryFunction(this, other) { a, b -> a / b }
operator fun ObservableInt.rem(other: ObservableInt): ObservableInt = IntBinaryFunction(this, other) { a, b -> a % b }
fun min(oa: ObservableInt, ob: ObservableInt): ObservableInt = IntBinaryFunction(oa, ob) { a, b -> if (a < b) a else b }
fun max(oa: ObservableInt, ob: ObservableInt): ObservableInt = IntBinaryFunction(oa, ob) { a, b -> if (a > b) a else b }

fun ObservableInt.toFloat(): ObservableFloat = FloatUnaryFunction(this) { a -> a.toFloat() }
fun ObservableInt.toDouble(): ObservableDouble = DoubleUnaryFunction(this) { a -> a.toDouble() }
//fun ObservableInt.format(format: NumberFormat) = StringUnaryFunction(this) { a -> format.format(a) }

fun ObservableInt.greaterThan(value: Int) = BooleanUnaryFunction(this) { it > value }
fun ObservableInt.lessThan(value: Int) = BooleanUnaryFunction(this) { it < value }
fun ObservableInt.greaterThanOrEqual(value: Int) = BooleanUnaryFunction(this) { it >= value }
fun ObservableInt.lessThanOrEqual(value: Int) = BooleanUnaryFunction(this) { it <= value }

fun ObservableInt.greaterThan(other: ObservableInt) = BooleanBinaryFunction(this, other) { a, b -> a > b }
fun ObservableInt.lessThan(other: ObservableInt) = BooleanBinaryFunction(this, other) { a, b -> a < b }
fun ObservableInt.greaterThanOrEqual(other: ObservableInt) = BooleanBinaryFunction(this, other) { a, b -> a >= b }
fun ObservableInt.lessThanOrEqual(other: ObservableInt) = BooleanBinaryFunction(this, other) { a, b -> a <= b }

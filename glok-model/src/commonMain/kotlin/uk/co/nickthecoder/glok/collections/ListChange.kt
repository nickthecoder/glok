package uk.co.nickthecoder.glok.collections

/**
 * The data describing changes to an [ObservableList].
 *
 * See [ObservableList.addChangeListener]
 *
 * A typical listener has the following structure :
 *
 *     val myListener = myObservableList.addChangeListener { _, change ->
 *         if ( change.removed.isNotEmpty() ) {
 *             // Act on removed items
 *         }
 *         if ( change.added.isNotEmpty() ) {
 *             // Act on data added.
 *         }
 *     }
 *
 */
interface ListChange<out E> {
    /**
     * The index within the [ObservableList] where this change occurred.
     */
    val from: Int

    /**
     * The items added to the [ObservableList].
     */
    val added: List<E>

    /**
     * The items removed from the [ObservableList].
     */
    val removed: List<E>

    /**
     * Returns true iff the same number of items were added and removed.
     */
    fun isReplacement(): Boolean

    /**
     * Return true iff no items were added, and some were removed.
     */
    fun isRemoval(): Boolean

    /**
     * Return true iff no items were removed, but some were added.
     */
    fun isAddition(): Boolean

    companion object {
        /**
         * Creates a [SetChange] describing items removed from an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         */
        fun <E> removal(from: Int, removed: List<E>): ListChange<E> = ListChangeImpl(from, emptyList(), removed)

        /**
         * Creates a [SetChange] describing items added to an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         */
        fun <E> addition(from: Int, added: List<E>): ListChange<E> = ListChangeImpl(from, added, emptyList())

        /**
         * Creates a [SetChange] describing items replaced within an [ObservableSet].
         * You only need to use this when creating your own [ObservableSet] implementation.
         * Used when an item in the list is changed such as : `myObservableList[0] = newItem`
         */
        fun <E> replacement(from: Int, added: List<E>, removed: List<E>):
            ListChange<E> = ListChangeImpl(from, added, removed)
    }

}

internal class ListChangeImpl<E>(

    override val from: Int,
    override val added: List<E>,
    override val removed: List<E>

) : ListChange<E> {

    override fun isRemoval() = removed.isNotEmpty() && added.isEmpty()
    override fun isAddition() = added.isNotEmpty() && removed.isEmpty()
    override fun isReplacement() = added.isNotEmpty() && removed.isNotEmpty() && added.size == removed.size

    override fun toString() = "List Change from $from : Added ${added.size} Removed ${removed.size}"
}

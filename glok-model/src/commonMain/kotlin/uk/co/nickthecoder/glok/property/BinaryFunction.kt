package uk.co.nickthecoder.glok.property

open class BinaryFunction<V, A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(
    val argA: OA,
    val argB: OB,
    val lambda: (A, B) -> V
) : LazyObservableValue<V>(argA, argB) {

    final override fun eval() = lambda(argA.value, argB.value)

}

open class TernaryFunction<V, A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    val argA: OA,
    val argB: OB,
    val argC: OC,
    val lambda: (A, B, C) -> V
) : LazyObservableValue<V>(argA, argB, argC) {

    final override fun eval() = lambda(argA.value, argB.value, argC.value)

    override fun toString() = "TernaryFunction value=$value eval=${eval()}"
}

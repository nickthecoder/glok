/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.LazyObservableValue
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.*

/**
 * An ObservableBoolean, whose value is guaranteed to be false.
 */
val observableFalse: ObservableBoolean = object : LazyObservableValue<Boolean>(), ObservableBoolean {
    override fun eval() = false
}

/**
 * An ObservableBoolean, whose value is guaranteed to be true.
 */
val observableTrue: ObservableBoolean = object : LazyObservableValue<Boolean>(), ObservableBoolean {
    override fun eval() = true
}

operator fun ObservableBoolean.not(): ObservableBoolean = BooleanUnaryFunction(this) { a -> !a }

infix fun ObservableBoolean.or(other: ObservableBoolean): ObservableBoolean = BooleanBinaryFunction(this, other) { a, b -> a || b }
infix fun ObservableBoolean.and(other: ObservableBoolean): ObservableBoolean = BooleanBinaryFunction(this, other) { a, b -> a && b }
infix fun ObservableBoolean.xor(other: ObservableBoolean): ObservableBoolean = BooleanBinaryFunction(this, other) { a, b -> a xor b }

fun <A> ObservableBoolean.ifElse(trueValue: A, falseValue: A) = UnaryFunction(this) { if (it) trueValue else falseValue }

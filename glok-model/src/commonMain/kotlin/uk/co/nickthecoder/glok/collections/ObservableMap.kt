/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.Observable

interface ObservableMap<K : Any, out V> : Map<K, V>, Observable {

    fun addChangeListener(listener: MapChangeListener<@UnsafeVariance K, @UnsafeVariance V>)
    fun removeChangeListener(listener: MapChangeListener<@UnsafeVariance K, @UnsafeVariance V>)

    fun addChangeListener(
        lambda: (map: ObservableMap<K, V>, changes: List<MapChange<K, V>>) -> Unit
    ): MapChangeListener<K, V> {
        val mcl = mapChangeListener(lambda)
        addChangeListener(mcl)
        return mcl
    }

    fun addWeakChangeListener(
        lambda: (list: ObservableMap<K, V>, change: List<MapChange<K, V>>) -> Unit
    ): MapChangeListener<K, V> {
        val inner = mapChangeListener(lambda)
        val wrapper = WeakMapChangeListener(inner)
        addChangeListener(wrapper)
        return inner
    }
}

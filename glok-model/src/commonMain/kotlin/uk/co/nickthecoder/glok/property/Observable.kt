package uk.co.nickthecoder.glok.property

interface Observable {
    fun addListener(listener: InvalidationListener)
    fun removeListener(listener: InvalidationListener)

    fun addListener(lambda: (Observable) -> Unit) = invalidationListener(lambda).also { addListener(it) }

    fun addWeakListener(lambda: (Observable) -> Unit) : InvalidationListener {
        val inner = invalidationListener(lambda)
        val wrapper = WeakInvalidationListener(inner)
        addListener(wrapper)
        return inner
    }
}

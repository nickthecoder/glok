package uk.co.nickthecoder.glok.documentation

/**
 * This interface and all sub-interfaces are only here for _Documentation Purposes_.
 * I use them like a Wiki.
 *
 *
 */
interface Documentation

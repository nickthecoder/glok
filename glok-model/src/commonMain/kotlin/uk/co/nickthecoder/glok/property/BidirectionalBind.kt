/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.property.boilerplate.DoubleProperty
import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.util.Weak

interface BidirectionalBind {

    /**
     * One of the properties forming the bidirectional bind.
     * We only keep `weak` references, so if the property is garbage collected, then this may return null.
     */
    val propertyA: Property<*>?

    /**
     * One of the properties forming the bidirectional bind.
     * We only keep `weak` references, so if the property is garbage collected, then this may return null.
     */
    val propertyB: Property<*>?

    fun unbind()

    /**
     * Called from [Property.bidirectionalUnbind].
     */
    fun tidyUp()
}

/**
 * Uses `Strong` listeners, but `Weak` references to the properties themselves.
 * Therefore, even if you keep a reference to this [BidirectionalBind], either Property may be garbage collected.
 * If either property is garbage collected
 */
internal abstract class AbstractBidirectionalBind<A, B>(

    propertyA: Property<A>,
    propertyB: Property<B>

) : BidirectionalBind {

    private val weakPropertyA = Weak(propertyA)
    private val weakPropertyB = Weak(propertyB)

    final override val propertyA: Property<*>?
        get() = weakPropertyA.get()

    final override val propertyB: Property<*>?
        get() = weakPropertyB.get()

    protected val guard = BidirectionalGuard()

    abstract fun aToB(a: A): B
    abstract fun bToA(b: B): A

    private val aListener = propertyA.addBindChangeListener { _, _, aValue ->
        guard {
            val propB = weakPropertyB.get()
            if (propB == null) {
                unbind()
            } else {
                propB.value = aToB(aValue)
            }
        }
    }

    private val bListener = propertyB.addBindChangeListener { _, _, bValue ->
        guard {
            val propA = weakPropertyA.get()
            if (propA == null) {
                unbind()
            } else {
                propA.value = bToA(bValue)
            }
        }
    }

    final override fun unbind() {
        weakPropertyA.get()?.let {
            it.bidirectionalUnbind(weakPropertyB.get())
            return
        }
        weakPropertyB.get()?.bidirectionalUnbind(weakPropertyA.get())
    }

    override fun tidyUp() {
        weakPropertyA.get()?.removeChangeListener(aListener)
        weakPropertyB.get()?.removeChangeListener(bListener)
    }

}

internal class SimpleBidirectionalBind<V>(
    propertyA: Property<V>,
    propertyB: Property<V>

) : AbstractBidirectionalBind<V, V>(propertyA, propertyB) {

    override fun aToB(a: V): V = a
    override fun bToA(b: V): V = b

}

/**
 * Forms a [BidirectionalBind] between two properties, where a conversion is needed.
 * The value types do _not_ need to be the same.
 *
 * For example, this would allow you to bind a [FloatProperty] to a [DoubleProperty],
 * there the [converter] uses `toFloat()` and `toDouble()`.
 *
 * A common use case : A `FloatSlider`, with a `TextField` (or a spinner) next to it.
 * Bind the slider's value to the textField's text. The user can them adjust the value using
 * either the slider, or the textField.
 * You should catch exceptions in the [converter] when the string cannot be converted to a number,
 * and add a visual clue. e.g. change the color of the textField.
 * In this example, you could use the property's [Property.bean], cast it to a `Node`, and add/remove
 * `:error` to `Node.pseudoStyles`.
 *
 * When the binding is created, [propertyA] is set to [propertyB]'s value.
 *
 */
internal class ConvertedBidirectionalBind<A, B>(
    propertyA: Property<A>,
    propertyB: Property<B>,
    val converter: Converter<A, B>
) : AbstractBidirectionalBind<A, B>(propertyA, propertyB) {

    override fun aToB(a: A): B = converter.forwards(a)
    override fun bToA(b: B): A = converter.backwards(b)
}

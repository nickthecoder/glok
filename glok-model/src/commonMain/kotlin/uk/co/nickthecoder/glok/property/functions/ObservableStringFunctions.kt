package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*

operator fun ObservableString.plus(other: ObservableString): ObservableString =
    StringBinaryFunction(this, other) { a, b -> a + b }

// The following use `simple types`, not `Properties` for their arguments.

operator fun ObservableString.plus(b: String): ObservableString =
    StringUnaryFunction(this) { a -> a + b }

// Note, this cannot be an `operator`, because it gets showed by String.plus( Any? ) : String
infix fun String.plus(other: ObservableString): ObservableString =
    StringUnaryFunction(other) { a -> this + a }

fun ObservableString.prefixWith(b: String): ObservableString =
    StringUnaryFunction(this) { a -> b + a }

fun ObservableString.substring(range: IntRange): ObservableString =
    StringUnaryFunction(this) { a -> a.substring(range) }

fun ObservableString.substring(startIndex: Int): ObservableString =
    StringUnaryFunction(this) { a -> a.substring(startIndex) }

fun ObservableString.startsWith(prefix: String): ObservableBoolean =
    BooleanUnaryFunction(this) { a -> a.startsWith(prefix) }

fun ObservableString.isBlank(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a.isBlank() }
fun ObservableString.isNotBlank(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a.isNotBlank() }
fun ObservableString.isEmpty(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a.isEmpty() }
fun ObservableString.isNotEmpty(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a.isNotEmpty() }

fun ObservableOptionalString.isNullOrBlank(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a.isNullOrBlank() }
fun ObservableOptionalString.isNullOrEmpty(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a.isNullOrEmpty() }

fun ObservableString.length() = IntUnaryFunction(this) { str -> str.length }

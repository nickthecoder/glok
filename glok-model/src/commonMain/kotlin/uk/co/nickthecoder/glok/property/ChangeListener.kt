package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.util.Weak

interface AnyChangeListener {
}

interface ChangeListener<V, O : ObservableValue<V>> : AnyChangeListener {
    fun changed(observable: O, oldValue: V, newValue: V)

}

fun <V, O : ObservableValue<V>> changeListener(lambda: (O, V, V) -> Unit) =
    object : ChangeListener<V, O> {
        override fun changed(observable: O, oldValue: V, newValue: V) {
            lambda(observable, oldValue, newValue)
        }
    }


/**
 * Using weak listeners can help prevent memory leaks.
 *
 * A Weak Listener is a `wrapper` around an `inner` listener.
 * The `wrapper` contains a _weak_ reference to the `inner`
 *
 * The `wrapper` should be added to the list of listeners (via [ObservableValue.addChangeListener]`)
 * *
 * You have to keep a reference to `inner`, otherwise it will be garbage collected,
 * and the listener will stop working.
 *
 * When your class (which holds the returned reference to `inner`) is garbage collected,
 * there will be no strong references to `inner`, so it too will be garbage collected,
 * and the listener will stop.
 *
 * To remove a weak listener, pass `inner` or `this` to [ObservableValue.removeChangeListener].
 * ([ObservableValue.removeChangeListener] has special code which removes the `wrapper` (this),
 * even when the `inner` is passed to it).
 */
class WeakChangeListener<V, O : ObservableValue<V>>(
    wraps: ChangeListener<V, O>
) : ChangeListener<V, O> {

    private val weak = Weak(wraps)

    fun actual() = weak.get()

    override fun changed(observable: O, oldValue: V, newValue: V) {
        weak.get()?.changed(observable, oldValue, newValue)
    }
}

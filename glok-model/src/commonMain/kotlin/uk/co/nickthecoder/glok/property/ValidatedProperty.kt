/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

/**
 * A [Property] whose value is validated before being set.
 * Whenever [value] is changed, [validation] is called with the `old` and `new` values.
 * [validation] returns a _validated_ value.
 *
 * [validation] takes 3 arguments (similar to [ChangeListener] :
 *
 * 1. `this` property
 * 2. oldValue
 * 3. newValue
 *
 * For example, to reject a value, keeping the old value :
 *
 *     val myStringProperty = ValidatedProperty("Hello") { _, old, new ->
 *         if (old.isBlank()) old else new
 *     }
 *
 */
open class ValidatedProperty<V>(
    initialValue: V,
    bean: Any? = null,
    beanName: String? = null,
    /**
     * The validation. Args :
     *
     * * this Property
     * * old value
     * * new value
     *
     * Return : The validated value.
     */
    val validation: (ValidatedProperty<V>, V, V) -> V
) : PropertyBase<V>(bean, beanName) {

    override var value: V = initialValue
        get() = boundTo?.value ?: field
        set(v) {
            val actual = validation(this, value, v)
            changedValue {
                field = actual
                field
            }
        }
}

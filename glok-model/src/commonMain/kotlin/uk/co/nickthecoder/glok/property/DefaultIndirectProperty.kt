/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

open class DefaultIndirectProperty<P : Any, V>(

    val parentProperty: ObservableValue<P?>,

    /**
     * The default value to use when [parentProperty].value == `null`.
     */
    val defaultProperty: Property<V>,

    /**
     * A lambda to get the [Property] we are interested in tracking, from the value of [parentProperty].
     */
    val getProperty: (P) -> Property<V>

) : PropertyBase<V>() {

    private var oldValue: V = value

    /**
     * Fired when either the [parentProperty] or the indirect property changes.
     */
    private val listener = invalidationListener {
        val oldValue = oldValue
        val newValue = value
        if (oldValue != newValue) {
            this.fire(oldValue, value)
        }
    }

    @Suppress("unused")
    private val parentPropertyListener = parentProperty.addWeakChangeListener { _, oldParentValue, newParentValue ->
        if (oldParentValue != null) {
            getProperty(oldParentValue).removeListener(listener)
        }
        if (newParentValue != null) {
            getProperty(newParentValue).addListener(listener)
        }
    }

    init {
        parentProperty.addListener(listener)
        parentProperty.value?.let { getProperty(it).addListener(listener) }
    }

    final override var value: V
        get() {
            val parentValue = parentProperty.value
            oldValue = if (parentValue == null) {
                defaultProperty.value
            } else {
                getProperty(parentValue).value
            }
            return oldValue
        }
        set(v) {
            val parentValue = parentProperty.value
            if (parentValue == null) {
                defaultProperty.value = v
            } else {
                getProperty(parentValue).value = v
            }
        }

}

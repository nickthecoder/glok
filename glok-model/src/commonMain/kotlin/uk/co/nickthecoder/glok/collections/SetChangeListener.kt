package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.util.Weak


/**
 * When programming in the typical Kotlin style, this isn't used, because you'll use a `lambda` instead.
 *
 * See [ObservableSet.addChangeListener].
 */
interface SetChangeListener<E> {
    fun changed(set: ObservableSet<E>, change: SetChange<E>)
}


fun <E> setChangeListener(lambda: (list: ObservableSet<E>, change: SetChange<E>) -> Unit): SetChangeListener<E> =
    object : SetChangeListener<E> {
        override fun changed(
            set: ObservableSet<@UnsafeVariance E>, change: SetChange<@UnsafeVariance E>
        ) {
            lambda(set, change)
        }
    }

/**
 * Using weak listeners can help prevent memory leaks.
 *
 * A Weak Listener is a `wrapper` around an `inner` listener.
 * The `wrapper` contains a _weak_ reference to the `inner`
 *
 * The `wrapper` should be added to the list of listeners (via [ObservableSet.addChangeListener]`)
 * *
 * You have to keep a reference to `inner`, otherwise it will be garbage collected,
 * and the listener will stop working.
 *
 * When your class (which holds the returned reference to `inner`) is garbage collected,
 * there will be no strong references to `inner`, so it too will be garbage collected,
 * and the listener will stop.
 *
 * To remove a weak listener, pass `inner` or `this` to [ObservableSet.removeChangeListener].
 * ([ObservableSet.removeChangeListener] has special code which removes the `wrapper` (this),
 * even when the `inner` is passed to it).
 */
class WeakSetChangeListener<E>(wraps: SetChangeListener<E>) : SetChangeListener<E> {
    private val weak = Weak(wraps)

    fun actual() = weak.get()

    override fun changed(set: ObservableSet<E>, change: SetChange<E>) {
        weak.get()?.changed(set, change)
    }
}

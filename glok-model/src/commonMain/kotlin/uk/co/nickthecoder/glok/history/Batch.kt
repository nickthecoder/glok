/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.history


/**
 * A [Batch] is part of a [History] and is made up of a list of [Change]s.
 * Undo and redo work on batches, not on individual changes.
 *
 * See [History].
 */
interface Batch {

    /**
     * A human-readable short description of the changes.
     *
     * These labels could be used to give hints to the user what will happen when they undo or redo this change.
     *
     * The label could be part of the undo/redo buttons' `ToolTip`,
     * and also text within a `ListView` which shows the entire [History] of a document.
     */
    var label: String

    /**
     * Adds a [Change] to this batch using the unary plus operator. e.g.
     *
     * history.batch( "Insert Text" ) {
     *     + insertTextChange( "Hello", position )
     * }
     *
     */
    operator fun Change?.unaryPlus() {
        addChange(this)
    }

    fun addChange(change: Change?)

    /**
     * Does this batch only contain [SkippableChange]s? These are selection-only changes.
     * This gives us the choice to automatically jump over selection-only batches when performing
     * undo/redo.
     *
     * Note, at the time of writing, Glok doesn't have any [SkippableChange] implementations.
     */
    fun isSkippable(): Boolean
}

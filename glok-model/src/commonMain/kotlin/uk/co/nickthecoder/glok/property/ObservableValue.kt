package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.property.boilerplate.*
import kotlin.reflect.KProperty

/**
 * Holds a [value] of type [V], which can be observed using [InvalidationListener]s.
 * When the [value] changes, the [InvalidationListener]s are informed via [InvalidationListener.invalidated].
 *
 * [Property] is a notable sub-interface, whose value is mutable.
 * They have [ChangeListener]s (similar to [InvalidationListener], but are also sent the `old value` and `newValue`.
 *
 * Note. It is common to name an [ObservableValue] `xxxProperty`, even though it isn't really a [Property].
 * For example, suppose we have a Rectangle class, with `left`, `right` and `width` [ObservableValue]s.
 * We could choose to store `left` and `right` as `Properties`, with `width` as a [BinaryFunction].
 * Or we could store `left` and `width` as `Properties`, with `right` as a [BinaryFunction].
 * No matter which we choose, it is acceptable to name them `leftProperty`, `rightProperty` and `widthProperty`,
 * despite one of them not inheriting from [Property].
 *
 * ## Atomic Updates
 *
 * An atomic update, guarantees that two or more related properties are never in an inconsistent state,
 * where the value of one property is out-of-date, because it hasn't been updated yet.
 *
 * Consider `SingleSelectionModel` used by `ListView`, `TreeView` etc.
 * It has two properties `selectedIndex` and `selectedItem`. When we change one of these properties,
 * the other property must change before any of your listeners fire, your listener will
 * see an inconsistent state.
 *
 * Glok doesn't support atomic updates, but does offer some help, by having two sets of listeners.
 * Those added via [addBindListener] and [addBindChangeListener] fire before those added by
 * [addListener] and [addChangeListener].
 *
 * Bound properties, and [LazyObservableValue] always use [addBindListener] and [addBindChangeListener].
 * In general, your code should use [addListener] and [addChangeListener], and therefore
 * the property values will be consistent.
 *
 * However, if your (_regular_) listeners change another property, beware that other listeners
 * of the original property can see inconsistent values. It will see the new value of the original property,
 * but the value of the other property will depend on the order the listeners were added.
 * If it were added before, it will see the old (inconsistent) value.
 *
 * In my experience, the lack of atomic updates isn't an issue. YMMV.
 * None of my applications directly use [addBindListener] nor [addBindChangeListener], but they do make extensive
 * use of bound properties and [LazyObservableValue]s (such as [UnaryFunction], [BinaryFunction] etc).
 *
 * ## Class Diagram
 *
 *                                                    ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                                                    ┆     Observable    ┆
 *                                                    ┆ addListener()     ┆
 *                                                    ┆ removeListener()  ┆
 *                                                    ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                                                              △
 *                                     ┌────────────────────────┴──┬────────────────┐
 *                        ╭╌╌╌╌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╌╌╌╌╮     ╭╌╌╌╌╌╌╌╌┴╌╌╌╌╌╮   ╭╌╌╌╌╌╌┴╌╌╌╌╌╌╮
 *                        ┆     ObservableValue<V>  ┆     ┆ObservableList┆   ┆ObservableSet┆
 *                        ┆ val value : V           ┆     ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯   ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                        ┆ addChangeListener()     ┆
 *                        ┆ removeChangeListener()  ┆
 *                        ┆                         ┆
 *                        ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                                     △                                                          ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                                     ├─────────────────────────────────────────┐                ┆InvalidationListener┆
 *                           ╭╌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╌╌╮                   ┌──────────┴──────────┐     ┆  invalidated()     ┆
 *                           ┆   ReadOnlyProperty ┆                   │ ObservableValueBase │◇────┤                    ┆
 *                           ┆ bean : Any?        ┆                   │  listeners          │     ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                           ┆ beanName : String  ┆                   │  changeListeners    │     ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                           ┆                    ┆                   │                     │◇────┤ChangeListener┆
 *                           ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯                   └──────────┬──────────┘     ┆   changed()  ┆
 *                                     △                                         │                ┆              ┆
 *                  ┌──────────────────┴────┐                                    │                ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *      ┏━━━━━━━━━━━┷━━━━━━━━━━━━━┓ ╭╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╮    ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮  │
 *      ┃ ReadOnlyPropertyWrapper ┃ ┆    Property    ┆    ┆ BidirectionalBind ┆  │
 *      ┃   listeners             ┃ ┆  var value : V ┆◀───┤  propertyA        ┆  │
 *      ┃   changeListeners       ┃ ┆                ┆◀───┤  propertyB        ┆  │
 *      ┃                         ┃ ┆                ┆    ┆  unbind()         ┆  │
 *      ┗━━━━━━━━━━━━━━━━━━━━━━━━━┛ ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯    ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯  │
 *                                           △                                   │
 *                                           │                                   │
 *                                           │                                   │
 *              ┌────────────────────────┬───┴───────────────────────────────────┴───┐
 *      ╭╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╌╌╮     ┌───────┴────────┐                      ┌───────────┴──────────┐
 *      ┆ StylableProperty ┆     │  PropertyBase  │                      │ LazyObservableValue  │
 *      ┆   kClass()       ┆     │                │                      │  invalidate()        │
 *      ┆   style()        ┆     │                │                      │  eval()              │
 *      ┆   revert()       ┆     └────────────────┘                      │  addDependant()      │
 *      ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯             △                               │  removeDependant()   │
 *              △ (Used by Themes)       │                               └──────────────────────┘
 *              │                        │                                           △
 *              │                        │                         ┌─────────────────┴────────────────────┐
 *              │               ┏━━━━━━━━┷━━━━━━━━┓       ┏━━━━━━━━┷━━━━━━┓ ┏━━━━━━━━━━━━━━━━┓ ┏━━━━━━━━━━┷━━━━━━┓
 *              │               ┃  SimpleProperty ┃       ┃ UnaryFunction ┃ ┃ BinaryFunction ┃ ┃ TernaryFunction ┃
 *              │               ┃                 ┃       ┃  argA         ┃ ┃  argA          ┃ ┃  argA           ┃
 *              │               ┃                 ┃       ┃  lambda       ┃ ┃  argB          ┃ ┃  argB           ┃
 *              │               ┗━━━━━━━━━━━━━━━━━┛       ┗━━━━━━━━━━━━━━━┛ ┃  lambda        ┃ ┃  argC           ┃
 *              │                        △                                  ┗━━━━━━━━━━━━━━━━┛ ┃  lambda         ┃
 *              │     ┌──────────────────┘                                                     ┗━━━━━━━━━━━━━━━━━┛
 *      ┏━━━━━━━┷━━━━━┷━━━━━━━━┓
 *      ┃SimpleStylableProperty┃
 *      ┃                      ┃
 *      ┃                      ┃
 *      ┗━━━━━━━━━━━━━━━━━━━━━━┛
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
 */
interface ObservableValue<V> : Observable {

    val value: V

    fun addChangeListener(listener: ChangeListener<V, ObservableValue<V>>)
    fun removeChangeListener(listener: ChangeListener<V, ObservableValue<V>>)

    /**
     * Identical to [addListener], but these listeners are guaranteed to fire BEFORE _regular_ listeners.
     * These should be used for the sole purpose of updating other [ObservableValue]s which are dependent
     * on this [Observable].
     *
     * Note. Use [removeListener] to remove listeners regardless of whether they were added using
     * [addListener] or [addBindListener].
     */
    fun addBindListener(listener: InvalidationListener)
    fun addBindListener(lambda: (Observable) -> Unit) = invalidationListener(lambda).also { addBindListener(it) }

    fun addChangeListener(lambda: (ObservableValue<V>, V, V) -> Unit) =
        changeListener(lambda).also { addChangeListener(it) }

    /**
     * Identical to [addChangeListener], but these listeners are guaranteed to fire BEFORE other listeners.
     * These should be used for the sole purpose of updating single [ObservableValue]s which are dependent
     * on this [ObservableValue]. This helps (but doesn't guarantee) that properties change atomically.
     * i.e. when one property changes, a related property also changes before other (regular) listeners
     * fire. Therefore, the (regular) listeners cannot read inconsistent values.
     *
     * Note. Use [removeChangeListener] to remove listeners regardless of whether they were added using
     * [addChangeListener] or [addBindChangeListener].
     */
    fun addBindChangeListener(listener: ChangeListener<V, ObservableValue<V>>)
    fun addBindChangeListener(lambda: (ObservableValue<V>, V, V) -> Unit) =
        changeListener(lambda).also { addBindChangeListener(it) }

    fun addWeakChangeListener(lambda: (ObservableValue<V>, V, V) -> Unit): ChangeListener<V, ObservableValue<V>> {
        val inner = changeListener(lambda)
        val wrapper = WeakChangeListener(inner)
        addChangeListener(wrapper)
        return inner
    }

    /**
     * Lets us declare a `val` using a for the value of this property, using this as a delegate.
     * e.g.
     *
     *     val testProperty : ObservableValue<String> = ...
     *     val test by testProperty
     *
     * [Property] has a similar delegate operator `setValue`, which allow the `val` to be a `var` instead.
     */
    operator fun getValue(thisRef: Any, kProperty: KProperty<*>): V {
        return value
    }

    fun toObservableString(): ObservableString = StringUnaryFunction(this) { a: Any? -> a.toString() }
    fun toObservableString(nullString: String): ObservableString =
        StringUnaryFunction(this) { a: Any? -> a?.toString() ?: nullString }

    fun equalTo(other: ObservableValue<V>): ObservableBoolean = BooleanBinaryFunction(this, other) { a, b -> a == b }
    fun equalTo(other: V): ObservableBoolean = BooleanUnaryFunction(this) { a -> a == other }
    fun notEqualTo(other: ObservableValue<V>): ObservableBoolean = BooleanBinaryFunction(this, other) { a, b -> a != b }
    fun notEqualTo(other: V): ObservableBoolean = BooleanUnaryFunction(this) { a -> a != other }

    fun sameInstance(other: ObservableValue<V>): ObservableBoolean =
        BooleanBinaryFunction(this, other) { a, b -> a === b }

    fun sameInstance(other: V): ObservableBoolean = BooleanUnaryFunction(this) { a -> a === other }
    fun notSameInstance(other: ObservableValue<V>): ObservableBoolean =
        BooleanBinaryFunction(this, other) { a, b -> a !== b }

    fun notSameInstance(other: V): ObservableBoolean = BooleanUnaryFunction(this) { a -> a !== other }

    fun isNull(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a == null }
    fun isNotNull(): ObservableBoolean = BooleanUnaryFunction(this) { a -> a != null }
}

/**
 * Converts an [ObservableValue], with a nullable to value, to an [ObservableValue] whose value is NOT
 * nullable, by supplying a [defaultValue].
 *
 * See [asNullable] for the opposite operation.
 */
fun <T : Any> ObservableValue<T?>.replaceNullWith(defaultValue: T): ObservableValue<T> =
    UnaryFunction(this) { a -> a ?: defaultValue }

/**
 * Converts an [ObservableValue], with a non-nullable to value, to an [ObservableValue] whose value IS
 * nullable.
 *
 * See [replaceNullWith] for the opposite operation.
 */
fun <T : Any> ObservableValue<T>.asNullable(): ObservableValue<T?> =
    UnaryFunction(this) { a -> a }

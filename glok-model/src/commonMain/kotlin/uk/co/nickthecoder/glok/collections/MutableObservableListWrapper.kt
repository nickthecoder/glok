package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener
import uk.co.nickthecoder.glok.property.WeakInvalidationListener

/**
 * Wraps an existing [MutableList].
 * Note, and changes made to the [actual] list will not be noticed by listeners.
 */
internal class MutableObservableListWrapper<E>(private val actual: MutableList<E>) : MutableObservableList<E> {

    internal val rememberListeners = mutableListOf<Any>()

    private val listeners = mutableListOf<InvalidationListener>()

    private val changeListeners = mutableListOf<ListChangeListener<E>>()

    override fun addListener(listener: InvalidationListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: InvalidationListener) {
        listeners.remove(listener)
        if (listener is WeakInvalidationListener) {
            listener.actual()?.let { listeners.remove(it) }
        }
    }

    override fun addChangeListener(listener: ListChangeListener<E>) {
        changeListeners.add(listener)
    }

    override fun removeChangeListener(listener: ListChangeListener<E>) {
        changeListeners.remove(listener)
        if (listener is WeakListChangeListener<E>) {
            listener.actual()?.let { changeListeners.remove(it) }
        }
    }


    override val size: Int
        get():Int {
            return actual.size
        }

    override fun get(index: Int): E {
        return actual.get(index)
    }

    override fun isEmpty(): Boolean {
        return actual.isEmpty()
    }

    override fun iterator(): MutableIterator<E> {
        return ObservableMutableListIterator(actual.listIterator())
    }

    override fun listIterator(): MutableListIterator<E> {
        return ObservableMutableListIterator(actual.listIterator())
    }

    override fun listIterator(index: Int): MutableListIterator<E> {
        return ObservableMutableListIterator(actual.listIterator(index))
    }

    override fun subList(fromIndex: Int, toIndex: Int): MutableList<E> {
        val observableSubList = actual.subList(fromIndex, toIndex).asMutableObservableList()
        val listener = observableSubList.addChangeListener { _, change ->
            for (listener in listeners) {
                listener.invalidated(this)
            }
            val mappedChange = ListChangeImpl(change.from + fromIndex, change.added, change.removed)
            for (listener in changeListeners) {
                listener.changed(this, mappedChange)
            }
        }
        // We need this listener to "belong" to the observableSubList, so it doesn't get GC'd.
        (observableSubList as MutableObservableListWrapper<*>).rememberListeners.add(listener)

        return observableSubList
    }

    override fun lastIndexOf(element: E): Int {
        return actual.lastIndexOf(element)
    }

    override fun indexOf(element: E): Int {
        return actual.indexOf(element)
    }

    override fun containsAll(elements: Collection<E>): Boolean {
        return actual.containsAll(elements)
    }

    override fun contains(element: E): Boolean {
        return actual.contains(element)
    }


    private fun invalidate(change: ListChange<E>) {
        for (l in listeners) {
            l.invalidated(this)
        }
        for (l in changeListeners) {
            l.changed(this, change)
        }
    }


    override fun clear() {
        val change = ListChange.removal(0, actual.toList())
        actual.clear()
        invalidate(change)
    }

    override fun addAll(elements: Collection<E>): Boolean {
        val change = ListChange.addition(actual.size, elements.toList())
        return if (actual.addAll(elements)) {
            invalidate(change)
            true
        } else {
            false
        }
    }

    override fun addAll(index: Int, elements: Collection<E>): Boolean {
        val change = ListChange.addition(index, elements.toList())
        return if (actual.addAll(index, elements)) {
            invalidate(change)
            true
        } else {
            false
        }
    }

    override fun add(index: Int, element: E) {
        val change = ListChange.addition(index, listOf(element))
        actual.add(index, element)
        invalidate(change)
    }

    override fun add(element: E): Boolean {
        val change = ListChange.addition(actual.size, listOf(element))
        return if (actual.add(element)) {
            invalidate(change)
            true
        } else {
            false
        }
    }

    override fun removeAt(index: Int): E {
        val result = actual.removeAt(index)
        invalidate(ListChange.removal(index, listOf(result)))
        return result
    }

    override fun removeBetween(indices: IntRange) {
        val removed = actual.slice(indices).toList()
        val removeAt = indices.first
        for (i in indices) {
            actual.removeAt(removeAt)
        }
        invalidate(ListChange.removal(indices.first, removed))
    }

    override fun set(index: Int, element: E): E {
        val oldElement = actual.set(index, element)
        val change = ListChange.replacement(index, listOf(element), listOf(oldElement))
        invalidate(change)
        return oldElement
    }

    override fun retainAll(elements: Collection<E>): Boolean {
        // NOTE. This is inefficient, it can fire a LOT of changes.
        // It could be optimised somewhat, by finding which removals are sequential,
        // and placing all of them into a single Change.
        var result = false
        for (e in actual) {
            if (!elements.contains(e)) {
                remove(e)
                result = true
            }
        }
        return result
    }

    override fun removeAll(elements: Collection<E>): Boolean {
        // This is inefficient, it can fire a LOT of changes.
        // It could be optimised somewhat, by finding which removals are sequential,
        // and placing all of them into a single Change.
        var result = false
        for (element in elements) {
            if (remove(element)) {
                result = true
            }
        }
        return result
    }

    override fun remove(element: E): Boolean {
        val index = actual.indexOf(element)
        return if (actual.remove(element)) {
            invalidate(ListChange.removal(index, listOf(element)))
            true
        } else {
            false
        }
    }

    override fun toString() = actual.toString()

    private inner class ObservableMutableListIterator(val inner: MutableListIterator<E>) : MutableListIterator<E> {

        override fun hasNext() = inner.hasNext()

        override fun hasPrevious() = inner.hasPrevious()

        override fun next() = inner.next()

        override fun nextIndex() = inner.nextIndex()

        override fun previous() = inner.previous()

        override fun previousIndex() = inner.previousIndex()

        // Mutable parts...

        override fun add(element: E) {
            val index = nextIndex()
            inner.add(element)
            val change = ListChange.addition(index, listOf(element))
            invalidate(change)
        }

        override fun remove() {
            val lastRet = nextIndex() - 1
            val removed = listOf(this@MutableObservableListWrapper.actual[lastRet])
            inner.remove()
            val change = ListChange.removal(lastRet, removed)
            invalidate(change)
        }

        override fun set(element: E) {
            val lastRet = nextIndex() - 1
            val removed = listOf(this@MutableObservableListWrapper.actual[lastRet])
            inner.set(element)
            val change = ListChange.replacement(lastRet, listOf(element), removed)
            invalidate(change)
        }
    }
}

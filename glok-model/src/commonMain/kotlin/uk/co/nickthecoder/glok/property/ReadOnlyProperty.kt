package uk.co.nickthecoder.glok.property

/**
 * A [ReadOnlyProperty] is an [ObservableValue], which is aware of its owner ([bean]), and the [beanName].
 *
 * Unlike JavaFX, a [ReadOnlyProperty] _must_ have a [bean] and [beanName]. Use [ObservableValue] instead if you
 * want it to be anonymous.
 *
 */
interface ReadOnlyProperty<V> : ObservableValue<V> {
    val bean: Any?
    val beanName: String?

    /*
        /**
         * Returns the `getter` method for this property, or null if there is none
         */
        fun getter(): Method? {
            val bean = bean ?: return null
            val getterName = getterName() ?: return null
            return try {
                bean.javaClass.getMethod(getterName)
            } catch (e: NoSuchMethodException) {
                null
            }
        }

        fun setter() : Method? {
            val bean = bean ?: return null
            val beanName = beanName ?: return null
            val setterName = "set" + beanName.substring(0, 1).uppercase() + beanName.substring(1)
            return try {
                bean.javaClass.getMethod(setterName, type())
            } catch (e: NoSuchMethodException) {
                null
            }
        }
    */

}

/**
 * The name of the "getter" method based on [ReadOnlyProperty.beanName]
 */
internal fun ReadOnlyProperty<*>.getterName(): String? {
    val beanName = beanName ?: return null
    return "get" + beanName.substring(0, 1).uppercase() + beanName.substring(1)
}


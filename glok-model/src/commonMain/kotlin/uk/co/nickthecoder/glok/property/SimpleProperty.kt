package uk.co.nickthecoder.glok.property

open class SimpleProperty<V>(
    initialValue: V,
    bean: Any? = null,
    beanName: String? = null,
) : PropertyBase<V>(bean, beanName) {

    override var value: V = initialValue
        get() = boundTo?.value ?: field
        set(v) {
            changedValue {
                field = v
                field
            }
        }
}

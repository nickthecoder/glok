/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.ObservableValueBase
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.ObservableInt

/**
 * An [ObservableInt] whose value is the size of an [ObservableSet].
 *
 * As example of where this might be useful. Suppose we want to disable a button when a set is empty.
 * Simply bind its `disabledProperty` to :
 *
 *     ObservableSetSize(mySet).equalTo(0)
 *
 * Alternatively, we might want a label to contain a list's size, in which case,
 * bind its `textProperty` to :
 *
 *     ObservableSetSize(mySet).toObservableString()
 */
class ObservableSetSize(val list: ObservableSet<*>) : ObservableInt, ObservableValueBase<Int>() {

    private var cachedValue: Int = list.size

    @Suppress("unused")
    private val listener = list.addWeakListener {
        val actualSize = list.size
        if (cachedValue != actualSize) {
            try {
                fire(cachedValue, actualSize)
            } finally {
                cachedValue = actualSize
            }
        }
    }

    override val value: Int
        get() = list.size
}

fun ObservableSet<*>.sizeProperty(): ObservableInt = ObservableSetSize(this)
fun ObservableSet<*>.isEmptyProperty(): ObservableBoolean = ObservableSetSize(this).equalTo(0)
fun ObservableSet<*>.isNotEmptyProperty(): ObservableBoolean = ObservableSetSize(this).notEqualTo(0)

package uk.co.nickthecoder.glok.collections

import uk.co.nickthecoder.glok.property.InvalidationListener

private class EmptyObservableSet<E>() : ObservableSet<E> {

    private val actual = emptyList<E>()


    override fun addListener(listener: InvalidationListener) {}

    override fun removeListener(listener: InvalidationListener) {}

    override fun addChangeListener(listener: SetChangeListener<E>) {}

    override fun removeChangeListener(listener: SetChangeListener<E>) {}


    override val size: Int get() = 0


    override fun isEmpty() = true

    override fun iterator() = actual.iterator()

    override fun containsAll(elements: Collection<E>) = actual.containsAll(elements)

    override fun contains(element: E) = actual.contains(element)

}

private val emptyObservableSet = EmptyObservableSet<Any?>()

/*
 * We are casting an ObservableSet<Any>> to ObservableSet<E>, which is usually WRONG.
 * But as the set is empty, and cannot be added to, then we should never run into
 * type issues, despite the element types being different.
 */
@Suppress("UNCHECKED_CAST")
fun <E> emptyObservableSet(): ObservableSet<E> = emptyObservableSet as ObservableSet<E>

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demo.server

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.html.*
import io.ktor.server.http.content.*
import io.ktor.server.netty.*
import io.ktor.server.routing.*
import kotlinx.html.*
import java.io.File

/**
 *
 * NOTE. If you use the gradle task `glok-demo-server:run`, this will _NOT_
 * rebuild `glok-demo-client`. So either alter the gradle build script, so that it does,
 * or use `gradlew glok-demo-client:build` too.
 *
 * `gradlew build` will build both parts ;-)
 */
object DemoServer {

    /**
     * jsPath is the directory containing `glok-demo-client.js` and its associated `.map` file.
     */
    private fun startServer(port: Int, jsPath: File) {
        embeddedServer(Netty, port = port) {
            routing {
                staticFiles("/js", jsPath)
                get("/") { index(call) } // A plain HTML page (not glok)
                get("/demo") { demo(call) } // Runs one of the demos
            }
        }.start(wait = true)
    }

    /**
     * A plain HTML index page, with links to demo applications.
     */
    private suspend fun index(call: ApplicationCall) {
        call.respondHtml {
            head {
                title("Glok Demos")
            }
            body {
                h1 {
                    + "Glok Demos"
                }
                for (name in demoNames) {
                    a("demo?demo=$name") {
                        + name
                    }
                    br()
                }
            }
        }
    }

    /**
     *
     */
    private suspend fun demo(call: ApplicationCall) {
        // Which demo to run is determined by a "get" parameter ?demo=xxx
        // The js main() entry point looks for this parameter, and launches the appropriate glok application.
        val demoName = call.parameters["demo"]
        if (demoName == null) {
            // A more complete solution would set an error status. 404 Not found???
            call.respondHtml {
                head {
                    title("Error")
                }
                body {
                    + "Missing demo name"
                }
            }
        } else {
            // Builds the HTML page for the application.
            call.respondHtml {
                head {
                    title(demoName)
                    // `js` is staticFiles from "jsPath", which is : glok-demo-client/build/dist/js/productionExecutable
                    script(src = "/js/glok-demo-client.js") {}
                }
                body {
                    // When the application starts, this div is removed, and replaced by a WebGL canvas.
                    // In a "production" setting, consider replacing this with a fancy swirling graphic ;-)
                    div {
                        id = "loading"
                        + "Loading"
                    }
                }
            }
        }
    }

    /**
     *
     */
    @JvmStatic
    fun main(vararg args: String) {
        var port = 8888
        if (args.size == 1 && (args[0] == "--help" || args[0] == "-h")) {
            println(USAGE)
            return
        }

        if (args.size > 1 && args[0] == "--port") {
            args[1].toIntOrNull()?.let { port = it }
        }

        val jsPath = File("glok-demo-client/build/dist/js/productionExecutable")
        // This assumes that "resources" is a subdirectory in the current directory, holding the
        // compiled .js file, as well as any other static files, such as images etc. required by the app.
        println("Starting server http://localhost:8888    jsPath : ${jsPath.canonicalFile}")
        startServer(port, jsPath)
    }

    private val USAGE = """
        USAGE
        
            DemoServer
                Starts the server using the default port 8888
            
            DemoServer --port PORT_NUMBER
                Start the server using a custom port number.
            
            DemoServer -h
            DemoServer --help
                Display this message
        """.trimIndent()

    private val demoNames = listOf("DemoSplitPane")
}

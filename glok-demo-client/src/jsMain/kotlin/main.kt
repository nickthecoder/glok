import kotlinx.browser.window
import kotlinx.dom.createElement
import org.w3c.dom.HTMLCanvasElement
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demo.client.DemoSplitPane
import uk.co.nickthecoder.glok.util.log

/**
 * The entry point for the browser based (javascript) client-side demos.
 * Note, most of the demos in the subproject `glok-demos` would work fine as long as you exclude the
 * `@JvmStatic fun main(...)`.
 * Some of them use jvm specific features though (e.g. `DemoFileDialogs`)
 *
 * I chose not to place them in `commonMain` for simplicity.
 * (Most developers only require jvm, and I didn't want to confuse them).
 */
fun main() {
    window.onload = {
        log.info("Document Loaded")
        val search = window.location.search
        val loadingDiv = window.document.getElementById("loading")
        if (loadingDiv == null) {
            log.error("Error. loading div not found")
        } else {
            if (search.startsWith("?demo=")) {

                val appClass = when (search.substring(6)) {
                    "DemoSplitPane" -> DemoSplitPane::class
                    else -> null
                }

                if (appClass == null) {
                    // Demo not found...
                    log.error("Demo not found : $search")
                    loadingDiv.innerHTML = "Demo not found : $search"

                } else {
                    // Glok does NOT create a `canvas`, we need to do that ourselves.
                    // This gives us full control over the size of the canvas, and also
                    // lets us include other HTML elements.
                    // For simplicity, this demo uses a fixed size canvas with no other HTML elements.
                    val canvas = window.document.createElement("canvas") {
                        id = "canvas"
                    } as HTMLCanvasElement
                    canvas.width = 800
                    canvas.height = 600
                    // NOTE, the order is the new, existing, which is, IMHO the very counterintuitive.
                    loadingDiv.parentNode !!.replaceChild(canvas, loadingDiv)

                    // Launches the Application.
                    // From here on, there is no difference between using jvm/js.
                    launch(appClass)
                }
            } else {
                window.document.body?.innerHTML = "Expected ?demo=XXX"
            }
        }
    }
}

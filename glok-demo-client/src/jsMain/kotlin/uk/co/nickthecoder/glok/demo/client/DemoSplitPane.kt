/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demo.client

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.pane
import uk.co.nickthecoder.glok.scene.dsl.plainBackground
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.splitPane

class DemoSplitPane : Application() {
    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            scene = scene {
                root = splitPane {
                    + pane { plainBackground(Color["#ff0000"]) }
                    + pane { plainBackground(Color.WHITE) }
                    + pane { plainBackground(Color.BLUE) }
                }
            }
            show()
        }
    }
}

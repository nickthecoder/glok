# Version 0.6

### Miscellaneous

New methods : RegularStage.moveTo(x,y) and RegularStage.position()

### Commands and Actions

`Actions` have NamedImages, previously, `Commands` had them.

Building nodes from `Commands` has changed. Instead of :

    with( commands ) {
        ...
    }

We now use :

    commands.build( iconSizeProperty ) {
        ...
    }

### Node.background / Node.border / Node.focusXXX properties

Previously implementations of Background and Border included a Color.
Now they don't. Instead, Region has backgroundColor and borderColor properties.
Some Background/Border implementations (such as NinePatchBackground)
ignore the new `color` parameter passed to `fun draw(...)` 

The same is true for the additional background/borders which indicate "sections"
using Region's properties focusBorder, focusHighlight, focusBorderColor, focusHighlightColor.

Affects custom themes and code which uses Background / Border.

### Dock

Dock.icon : Image replaced by `icons : NamedImages` and `iconName : String`.

The old way didn't allow icons to be resized dynamically.

### FormGrid
To match all other parts of the DSL, these return a FormRow, which you must
add to the FormGrid.
To upgrade from version < 0.6, replace :

    row { ... }
    row( label ) { ... }

with

    + row { ... }
    + row( label ) { ... }

### RadioButton

Has two different styles.
Replace all calls to radioButton() with radioButton2(), because the default style
is the traditional looking radio-button, byt radioButton2() renders it
similar to a toggle-button.

## Other Changes

New default styles for FormGrid. 

# Version 0.5.2

Tantalum theme : A better handling of multiple color schemes.

Theme rules now have an `importance` level. Previous two or more matching rules
obeyed a `last rule wins`. Now, the rule with the highest importance wins,
and `last rule wins` is only used in the event of a tie.
This change is very important when combining themes.

Added Change implementations for adding/removing styles to `StyledTextDocument`.

Better handling of Stage's `focus`.

`Menus` can be navigated using the arrow keys.
There is still no keyboard shortcut to open menus in a MenuBar
(e.g. Alt+F to open the File menu).

Renamed `StandardCursor` to `MousePointer`.

OverlayStage support its `resizable` property.
i.e. it can be resized by dragging the edge of the (fake) window.

Added `ProgressBar`.

Renamed `Spinner`'s actions.

Added more properties to `GlokSettings`, and added `GlokSettingsDialog`

Added a `Switch` control, which is a `CheckBox` which looks like a sliding switch.

Buttons in a `ToolBar` which are text only now have a background.

Many minor bug fixes (too many to list here).
Search for "Bug Fix" in `git log`.
 
# Version 0.5.1

Renamed ScrollableBar to ScrollPaneWithButtons.

PopupMenu displays buttons to scroll the menu if it longer than the window height.

Oops, I stopped keeping track of non-trivial changes.
    
# Version 0.4

Listeners no longer use WeakReferences by default.
This has a potential large impact, so beware when upgrading!
After using Glok for a while, I realised that `weak` listeners were a PITA.
For the same behaviour as before replace all addListener and addChangeListener
with addWeakListener and addWeakChangeListener.

CustomisableTheme now supports multiple color schemes, each with their own set of color properties.
Tantalum has a dark and light color scheme.

IndirectProperty renamed to OptionalIndirectProperty.
The new IndirectProperty is for parent properties which are not nullable.

Added FlowPane.

Renamed various methods of Commands related to `Propery` based commands.

Changed the way property-boilerplate is generated, with finer grained control.
e.g. Previously all types included `stylable` boilerplate, despite very few types needing it.

# Version 0.3

### Toggle Buttons / Menu Items

`ChoiceButton` and `ChoiceMenuItem` no longer uses `ValueToggleGroup` (deleted).
Instead, they have a reference to a property.

`ChoiceRadioButton` and `ChoiceRadioButton` has been removed,
as `ChoiceButton`/`ChoiceMenuItem` fulfils those roles.

This affects `Commands` too.

### Key (enum)

Now has a `label`, so that `ActionTooltip` looks nicer.

# Version 0.2.x

I wasn't keeping track of the changes back then. Please don't use this version!

# Version 0.1

The first working version. Please don't use this version!

Back to [Contents](Contents.md)

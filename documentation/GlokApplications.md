# Glok Applications


## [Blokart](https://gitlab.com/nickthecoder/blokart)

Draw simple diagrams using plain-text. I mainly use it for
[class diagrams](https://gitlab.com/nickthecoder/glok/-/raw/main/glok-model/src/commonMain/kotlin/uk/co/nickthecoder/glok/property/ObservableValue.kt?ref_type=heads)
embedded inside dokka comments.

This was the first application writing from scratch using `Glok`.
If you want to study a complete Glok application, this is a good place to start, as it is quite small,
and has no other external dependencies.

## [FooCAD](https://gitlab.com/nickthecoder/foocad)

Create 3D models using a scripting language. Slice, and print them with a single button press.

Inspired by [OpenSCAD](https://openscad.org/), which, IMHO, is a great idea poorly implemented.
Scad scripts are impossible to read/maintain, and even slightly complicated models can be
incredibly hard/impossible to code directly in OpenSCAD.
This is ironic, given that `FooCAD` uses `OpenSCAD` as an intermediary step to build the `.stl` files.

All of my [3D printing models](http://nickthecoder.co.uk/3d) use FooCAD.

## [Vectorial](https://gitlab.com/nickthecoder/vectorial)

A 2D Vector Drawing program for designers, not artists. (work in progress).

With aspects of Inkscape, Visio and CAD programs.
Every point and measurement is an _expression_, not a simple number.

Vectorial was ported from JavaFX to Glok due to performance problems. It renders diagrams using OpenGL,
and the only cross-platform way to see the results in JavaFX is to copy the textures from the GPU
to main (CPU) memory and then back to the GPU again - way too slow.

This is why Glok was originally conceived. However, Glok turned out to be much better than I expected,
and I doubt I'll ever use JavaFX again for my personal projects.

## [Sandr](https://gitlab.com/nickthecoder/sandr)

Short for `Search and Replace` (Pronounced Sander)

Perform search and replace across multiple files.

Back to [Contents](Contents.md)

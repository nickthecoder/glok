// Autogenerated by MetaBuild

plugins {
    id("org.jetbrains.dokka") version "2.0.0"
}
repositories {
    mavenCentral()
}
defaultTasks( "jar", "installDist" )
tasks.dokkaHtmlMultiModule {
    moduleName.set("glok")
}
task<Exec>("ntc") {
    dependsOn( "glok-demos:installDist", "dokkaHtmlMultiModule" )
    commandLine( "publishToNTC.feather","--","0.6alpha8")
}

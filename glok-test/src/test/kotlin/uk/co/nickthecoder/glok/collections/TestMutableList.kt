/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import junit.framework.TestCase

class TestMutableList : TestCase() {

    /**
     * Tests MutableObservableListWrapper.subList()
     *
     * The changes made to the sub list, should fire changes to the original list.
     */
    fun testSubList() {
        val list = mutableListOf("Hello", "World").asMutableObservableList()
        var from = - 1
        var was = ""
        var now = ""
        val listener = list.addChangeListener { _, change ->
            from = change.from
            was = change.removed.first()
            now = change.added.first()
        }
        val hello = list.subList(1, 2)
        hello[0] = "Nick"
        assertEquals(1, from)
        assertEquals("World", was)
        assertEquals("Nick", now)
    }


}

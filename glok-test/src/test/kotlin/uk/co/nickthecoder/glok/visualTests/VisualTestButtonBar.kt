/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.boilerplate.EdgesBinaryFunction
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

class VisualTestButtonBar : Application() {

    override fun start(primaryStage: Stage) {
        lateinit var leftRightPadding: FloatSpinner
        lateinit var topBottomPadding: FloatSpinner
        with(primaryStage) {
            title = "Primary Window"
            scene {
                root = vBox {
                    + toolBar {
                        + spinner(Tantalum.buttonPadding.left) {
                            leftRightPadding = this
                            editor.prefColumnCount = 4
                        }
                        + Label("Y")
                        spinner(Tantalum.buttonPadding.top) {
                            topBottomPadding = this
                            editor.prefColumnCount = 4
                        }
                        Tantalum.buttonPaddingProperty.bindTo(
                            EdgesBinaryFunction(topBottomPadding.valueProperty, leftRightPadding.valueProperty) { y, x ->
                                Edges(y, x)
                            }
                        )
                    }

                    + PaletteColorPicker(Color.WHITE)

                    + buttonBar {
                        meaning(ButtonMeaning.LEFT) {
                            button("Custom Color...") { onAction { scene?.dump(size = true, borders = false) } }
                        }
                        meaning(ButtonMeaning.OK) { Button("OK") }
                        meaning(ButtonMeaning.CANCEL) { Button("Cancel") }
                    }
                }
            }
            show()
        }
    }


    companion object {
        fun main(vararg args: String) {
            launch(VisualTestButtonBar::class)
        }
    }

}

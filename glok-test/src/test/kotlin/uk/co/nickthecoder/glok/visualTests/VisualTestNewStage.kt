/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.RegularStage
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.scene

class VisualTestNewStage : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "Primary Window"
            scene(200, 100) {
                root = button("New Window") { onAction { newWindow() } }
            }
            show()
        }
    }

    private fun newWindow() {
        RegularStage().apply {
            scene {
                button("Say Hello") { onAction { hello() } }
            }
            title = "New Window"
            show()
        }
    }

    private fun hello() {
        RegularStage().apply {
            scene(200, 100) {
                root = label("Hello") { alignment = Alignment.CENTER_CENTER }
            }
            title = "Hello"
            show()
        }
    }

    companion object {
        fun main(vararg args: String) {
            launch(VisualTestNewStage::class)
        }
    }

}

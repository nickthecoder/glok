package uk.co.nickthecoder.glok.util

import junit.framework.TestCase

class TestInMemoryLog : TestCase() {

    fun assertThrows(expectedException: Class<*>, block: () -> Unit) {
        try {
            block()
            fail("No exception thrown. Expected ${expectedException.name}")
        } catch (e: Exception) {
            if (!expectedException.isAssignableFrom(e.javaClass)) {
                fail("Expected ${expectedException.name}, but found ${e.javaClass.name}")
            }
            Unit // Prevent IDE nagging!
        }
    }

    fun testPositive() {
        val log = InMemoryLog(2)
        assertThrows(IndexOutOfBoundsException::class.java) { log[0] }

        log.add(3,"A")
        assertEquals(1, log.size)
        assertFalse("isEmpty", log.isEmpty())
        assertTrue("isNoEmpty", log.isNotEmpty())
        assertFalse("isFull", log.isFull())
        assertEquals("A", log[0])
        assertThrows(IndexOutOfBoundsException::class.java) { log[1] }

        log.add(3,"B")
        assertEquals(2, log.size)
        assertFalse("isEmpty", log.isEmpty())
        assertTrue("isNoEmpty", log.isNotEmpty())
        assertFalse("isFull", log.isFull())
        assertEquals("A", log[0])
        assertEquals("B", log[1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[2] }

        log.add(3,"C")
        assertEquals(3, log.size)
        assertFalse("isEmpty", log.isEmpty())
        assertTrue("isNoEmpty", log.isNotEmpty())
        assertFalse("isFull", log.isFull())
        assertEquals("A", log[0])
        assertEquals("B", log[1])
        assertEquals("C", log[2])
        assertThrows(IndexOutOfBoundsException::class.java) { log[3] }

        log.add(3,"D")
        assertEquals(5, log.size) // NOTE The extra warning message to the log!
        assertFalse("isEmpty", log.isEmpty())
        assertTrue("isNoEmpty", log.isNotEmpty())
        assertTrue("isFull", log.isFull())
        assertEquals("A", log[0])
        assertEquals("B", log[1])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[2])
        assertEquals("C", log[3])
        assertEquals("D", log[4])
        assertThrows(IndexOutOfBoundsException::class.java) { log[5] }

        log.add(3,"E")
        assertEquals(5, log.size) // NOTE No change in size
        assertFalse("isEmpty", log.isEmpty())
        assertTrue("isNoEmpty", log.isNotEmpty())
        assertTrue("isFull", log.isFull())
        assertEquals("A", log[0])
        assertEquals("B", log[1])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[2])
        // C has been lost.
        assertEquals("D", log[3])
        assertEquals("E", log[4])
        assertThrows(IndexOutOfBoundsException::class.java) { log[5] }

        log.add(3,"F")
        assertEquals(5, log.size) // NOTE No change in size
        assertEquals("A", log[0])
        assertEquals("B", log[1])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[2])
        // D has been lost.
        assertEquals("E", log[3])
        assertEquals("F", log[4])
        assertThrows(IndexOutOfBoundsException::class.java) { log[5] }

        log.add(3,"G")
        assertEquals(5, log.size) // NOTE No change in size
        assertEquals("A", log[0])
        assertEquals("B", log[1])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[2])
        // E has been lost.
        assertEquals("F", log[3])
        assertEquals("G", log[4])
        assertThrows(IndexOutOfBoundsException::class.java) { log[5] }
    }

    fun testNegative() {
        val log = InMemoryLog(2)
        assertThrows(IndexOutOfBoundsException::class.java) { log[-1] }

        log.add(3,"A")
        assertEquals("A", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-2] }

        log.add(3,"B")
        assertEquals(2, log.size)
        assertEquals("A", log[-2])
        assertEquals("B", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-3] }

        log.add(3,"C")
        assertEquals(3, log.size)
        assertEquals("A", log[-3])
        assertEquals("B", log[-2])
        assertEquals("C", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-4] }

        log.add(3,"D")
        assertEquals(5, log.size) // NOTE The extra warning message to the log!
        assertEquals("A", log[-5])
        assertEquals("B", log[-4])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[-3])
        assertEquals("C", log[-2])
        assertEquals("D", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-6] }


        log.add(3,"E")
        assertEquals(5, log.size) // NOTE No change in size
        assertEquals("A", log[-5])
        assertEquals("B", log[-4])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[-3])
        // C has been lost.
        assertEquals("D", log[-2])
        assertEquals("E", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-6] }

        log.add(3,"F")
        assertEquals(5, log.size) // NOTE No change in size
        assertEquals("A", log[-5])
        assertEquals("B", log[-4])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[-3])
        // D has been lost.
        assertEquals("E", log[-2])
        assertEquals("F", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-6] }

        log.add(3,"G")
        assertEquals(5, log.size) // NOTE No change in size
        assertEquals("A", log[-5])
        assertEquals("B", log[-4])
        assertEquals("LOG FULL. Some entries after this have been lost...", log[-3])
        // E has been lost.
        assertEquals("F", log[-2])
        assertEquals("G", log[-1])
        assertThrows(IndexOutOfBoundsException::class.java) { log[-6] }

    }


}

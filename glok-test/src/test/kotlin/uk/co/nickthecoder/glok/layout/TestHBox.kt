package uk.co.nickthecoder.glok.layout

import junit.framework.TestCase
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.Pane
import uk.co.nickthecoder.glok.internal.DangerousUnitTestShims
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Scene

class TestHBox : TestCase() {

    /**
     * For each property, we test the initial value via the `var` and the `property.value`.
     * Then change it via the `var`, and test again.
     * Finally, we change it via `property.value` and test again.
     */
    fun testProperties() {
        HBox().apply {
            fillHeight = true
            assertEquals(0f, spacing)
            assertEquals(0f, spacingProperty.value)
            spacing = 3f
            assertEquals(3f, spacing)
            assertEquals(3f, spacingProperty.value)
            spacingProperty.value = 2f
            assertEquals(2f, spacing)
            assertEquals(2f, spacingProperty.value)

            assertEquals(Edges(), padding)
            assertEquals(Edges(), paddingProperty.value)
            padding = Edges(1f, 2f, 3f, 4f)
            assertEquals(Edges(1f, 2f, 3f, 4f), padding)
            assertEquals(Edges(1f, 2f, 3f, 4f), paddingProperty.value)
            paddingProperty.value = Edges(2f, 3f)
            assertEquals(Edges(2f, 3f, 2f, 3f), padding)
            assertEquals(Edges(2f, 3f), paddingProperty.value)

            assertEquals(true, fillHeight)
            assertEquals(true, fillHeightProperty.value)
            fillHeight = false
            assertEquals(false, fillHeight)
            assertEquals(false, fillHeightProperty.value)
            fillHeight = true
            assertEquals(true, fillHeight)
            assertEquals(true, fillHeightProperty.value)

        }
    }

    fun testSingleSmall() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 24f
        }
        val box = HBox().apply {
            children.add(pane1)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, box.localX)
        assertEquals(0f, box.localY)
        assertEquals(100f, box.width)
        assertEquals(50f, box.height)

        assertEquals(0f, pane1.localX)
        assertEquals(0f, pane1.localY)
        assertEquals(16f, pane1.width)
        assertEquals(24f, pane1.height) // Uses its prefHeight

        box.fillHeight = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.height) // Filled

    }

    fun testSingleSmallWithMargin() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 2f
        }
        val box = HBox().apply {
            children.add(pane1)
            padding = Edges(10f, 20f, 30f, 40f)
            growPriority = 0f
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(40f, pane1.localX)
        assertEquals(10f, pane1.localY)
        assertEquals(16f, pane1.width)
        assertEquals(2f, pane1.height)

        box.fillHeight = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(10f, pane1.height)
    }


    fun testGrowSingleSmall() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 24f
            growPriority = 1f
        }
        val box = HBox().apply {
            children.add(pane1)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localX)
        assertEquals(0f, pane1.localY)
        assertEquals(100f, pane1.width)
        assertEquals(24f, pane1.height)

        box.fillHeight = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.height)

    }

    fun testTwoBothGrow() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 24f
            growPriority = 1f
        }
        val pane2 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 20f
            growPriority = 1f
        }
        val box = HBox().apply {
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localX)
        assertEquals(0f, pane1.localY)
        assertEquals(45f, pane1.width)
        assertEquals(24f, pane1.height)
        assertEquals(55f, pane2.localX)
        assertEquals(0f, pane2.localY)
        assertEquals(45f, pane2.width)
        assertEquals(20f, pane2.height)

        box.fillHeight = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.height)
        assertEquals(50f, pane2.height)

    }


    fun testTwoBothShrink() {
        val pane1 = Pane().apply {
            overridePrefWidth = 100f
            overridePrefHeight = 24f
            shrinkPriority = 1f
        }
        val pane2 = Pane().apply {
            overridePrefWidth = 100f
            overridePrefHeight = 20f
            shrinkPriority = 1f
        }
        val box = HBox().apply {
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localX)
        assertEquals(0f, pane1.localY)
        assertEquals(45f, pane1.width)
        assertEquals(24f, pane1.height)

        assertEquals(55f, pane2.localX)
        assertEquals(0f, pane2.localY)
        assertEquals(45f, pane2.width)
        assertEquals(20f, pane2.height)

        box.fillHeight
        DangerousUnitTestShims.layout(scene)

        assertEquals(24f, pane1.height)
        assertEquals(20f, pane2.height)

    }

    /**
     * As [testTwoBothGrow], but with growPriorities of 0, so no growing
     */
    fun testTwoBothStay() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 24f
            growPriority = 0f
        }
        val pane2 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 20f
            growPriority = 0f
        }
        val box = HBox().apply {
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localX)
        assertEquals(0f, pane1.localY)
        assertEquals(16f, pane1.width)
        assertEquals(24f, pane1.height)

        assertEquals(26f, pane2.localX)
        assertEquals(0f, pane2.localY)
        assertEquals(16f, pane2.width)
        assertEquals(20f, pane2.height)

        box.fillHeight = true
        DangerousUnitTestShims.layout(scene)

        assertEquals(50f, pane1.height)
        assertEquals(50f, pane2.height)

    }


    /**
     * There is a 3rd code path, then the children's prefWidths are exactly the right size
     * for the parent's width.
     */
    fun testTwoExactSize() {
        val pane1 = Pane().apply {
            overridePrefWidth = 45f
            overridePrefHeight = 24f
            growPriority = 1f
        }
        val pane2 = Pane().apply {
            overridePrefWidth = 45f
            overridePrefHeight = 20f
            growPriority = 1f
        }
        val box = HBox().apply {
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localX)
        assertEquals(0f, pane1.localY)
        assertEquals(45f, pane1.width)
        assertEquals(24f, pane1.height)

        assertEquals(55f, pane2.localX)
        assertEquals(0f, pane2.localY)
        assertEquals(45f, pane2.width)
        assertEquals(20f, pane2.height)

        box.fillHeight = true
        DangerousUnitTestShims.layout(scene)

        assertEquals(50f, pane1.height)
        assertEquals(50f, pane2.height)

    }

}

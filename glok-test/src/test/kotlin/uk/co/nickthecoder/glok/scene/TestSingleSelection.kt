/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import junit.framework.TestCase
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.SingleSelectionModel

class TestSingleSelection : TestCase() {

    /**
     * Tests that [SingleSelectionModel.selectedIndex] and [SingleSelectionModel.selectedItem]
     * appear to change atomically with selectedItem.
     *
     * Note, the change isn't actually atomic, but the binding are the first change listeners, so
     * when we change either property, the other property changes before any other listeners fire.
     * Therefore, from the outside, they _appear_ to change atomically.
     */
    fun testAtomic1() {
        val list = mutableListOf("Hello", "World").asMutableObservableList()
        val selection = SingleSelectionModel(list)
        selection.selectedIndex = 0
        assertEquals(0, selection.selectedIndex)
        assertEquals("Hello", selection.selectedItem)

        var oldSelectedIndex = - 1
        var newSelectedIndex = - 1
        var newSelectedIndex2 = - 1
        var newSelectedIndex3 = - 1
        var newSelectedIndex4 = - 1
        var newSelectedIndex5 = - 1

        var oldSelectedItem: String? = "?"
        var newSelectedItem: String? = "?"
        var newSelectedItem2: String? = "?"
        var newSelectedItem3: String? = "?"
        var newSelectedItem4: String? = "?"
        var newSelectedItem5: String? = "?"


        selection.selectedIndexProperty.addChangeListener { _, old, new ->
            oldSelectedIndex = old
            newSelectedIndex = new

            newSelectedIndex2 = selection.selectedIndex
            newSelectedItem2 = selection.selectedItem
        }

        selection.selectedIndexProperty.addListener {
            newSelectedIndex3 = selection.selectedIndex
            newSelectedItem3 = selection.selectedItem
        }

        selection.selectedItemProperty.addChangeListener { _, old, new ->
            oldSelectedItem = old
            newSelectedItem = new

            newSelectedItem4 = selection.selectedItem
            newSelectedIndex4 = selection.selectedIndex
        }

        selection.selectedItemProperty.addListener {
            newSelectedItem5 = selection.selectedItem
            newSelectedIndex5 = selection.selectedIndex

        }
        // End of setup

        selection.selectedItem = "World"

        // Tests
        assertEquals(0, oldSelectedIndex)
        assertEquals(1, newSelectedIndex)
        assertEquals(1, newSelectedIndex2)
        assertEquals(1, newSelectedIndex3)
        assertEquals(1, newSelectedIndex4)
        assertEquals(1, newSelectedIndex5) // Trickiest!

        assertEquals("Hello", oldSelectedItem)
        assertEquals("World", newSelectedItem)
        assertEquals("World", newSelectedItem2)
        assertEquals("World", newSelectedItem3)
        assertEquals("World", newSelectedItem4)
        assertEquals("World", newSelectedItem5)
    }

    /**
     * Similar to [testAtomic1], but changing the selection via the selection's index,
     * rather than via the selection's item.
     */
    fun testAtomic2() {
        val list = mutableListOf("Hello", "World").asMutableObservableList()
        val selection = SingleSelectionModel(list)
        selection.selectedIndex = 0
        assertEquals(0, selection.selectedIndex)
        assertEquals("Hello", selection.selectedItem)

        var oldSelectedIndex = - 1
        var newSelectedIndex = - 1
        var newSelectedIndex2 = - 1
        var newSelectedIndex3 = - 1
        var newSelectedIndex4 = - 1
        var newSelectedIndex5 = - 1

        var oldSelectedItem: String? = "?"
        var newSelectedItem: String? = "?"
        var newSelectedItem2: String? = "?"
        var newSelectedItem3: String? = "?"
        var newSelectedItem4: String? = "?"
        var newSelectedItem5: String? = "?"


        selection.selectedIndexProperty.addChangeListener { _, old, new ->
            oldSelectedIndex = old
            newSelectedIndex = new

            newSelectedIndex2 = selection.selectedIndex
            newSelectedItem2 = selection.selectedItem
        }

        selection.selectedIndexProperty.addListener {
            newSelectedIndex3 = selection.selectedIndex
            newSelectedItem3 = selection.selectedItem
        }

        selection.selectedItemProperty.addChangeListener { _, old, new ->
            oldSelectedItem = old
            newSelectedItem = new

            newSelectedItem4 = selection.selectedItem
            newSelectedIndex4 = selection.selectedIndex
        }

        selection.selectedItemProperty.addListener {
            newSelectedItem5 = selection.selectedItem
            newSelectedIndex5 = selection.selectedIndex

        }
        // End of setup

        selection.selectedIndex = 1

        // Tests
        assertEquals(0, oldSelectedIndex)
        assertEquals(1, newSelectedIndex)
        assertEquals(1, newSelectedIndex2)
        assertEquals(1, newSelectedIndex3)
        assertEquals(1, newSelectedIndex4)
        assertEquals(1, newSelectedIndex5)

        assertEquals("Hello", oldSelectedItem)
        assertEquals("World", newSelectedItem)
        assertEquals("World", newSelectedItem2)
        assertEquals("World", newSelectedItem3) // Trickiest!
        assertEquals("World", newSelectedItem4)
        assertEquals("World", newSelectedItem5)
    }
}

package uk.co.nickthecoder.glok.scene.text

import junit.framework.TestCase
import uk.co.nickthecoder.glok.backend.Backend
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.internal.DangerousUnitTestShims
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.text.FontIdentifier
import uk.co.nickthecoder.glok.text.TextVAlignment
import uk.co.nickthecoder.glok.util.InMemoryLog

/**
 * Tests creating a BitmapFont from AWT.
 * I'm not sure if this test will work across different platforms (or time), because if the
 * font metrics change slightly, then the width/height of the Texture will change, as well as the
 * position of the glyphs within it.
 *
 * Also note, this is really only a regression test, because I copy/pasted some expected values
 * from the first run of the test!
 */
class TestCreateFontFromAWT : TestCase() {

    private val color = Color.WHITE

    private lateinit var log: InMemoryLog

    override fun setUp() {
        backend = DummyBackend()
        log = InMemoryLog(50)
        Backend.beginLogging(log)
    }

    override fun tearDown() {
        Backend.endLogging()
    }

    fun testCreateVerdana() {
        // NOTE, When I first created these tests, I omitted this line, but now I realise that if heuristic
        // changes (which chooses a maxTextureWidth if we don't specify one),
        // then our tests would need be to be edited. So instead, I've hard-coded what that heuristic picked.
        val font = DangerousUnitTestShims.createBitmapFont(FontIdentifier("Verdana", 18f), 2, 2, 264)
        assertEquals(1, log.size)
        assertEquals("createTexture( font_Verdana 265 x 302 )", log[0])
        log.clear()

        font.draw("a", color, 0f, 0f)
        font.draw("a", color, 0f, 0f, HAlignment.LEFT)
        font.draw("a", color, 0f, 0f, HAlignment.CENTER)
        font.draw("a", color, 0f, 0f, HAlignment.RIGHT)
        //assertEquals(4, log.size)

        // println("Font metrics : ${font.height}, ${font.ascent} ${font.descent} ${font.top} ${font.bottom}")
        // Font Metrics : Height = 23 Ascent = 19 descent = 4 top = 19 bottom = 4
        // Glyph 'a' has a width and advance of 11
        // If these metrics are different on your machine, these tests will fail.
        // Note 237 , 83 is the left/baseline of the `a` glyph in the generated font's Texture.

        assertEquals("using font_Verdana tint #ffffff", log[0])
        assertEquals("draw( 237 , 77 size 11 , 23 to 0 , -19 )", log[1])
        assertEquals("using font_Verdana tint #ffffff", log[2])
        assertEquals("draw( 237 , 77 size 11 , 23 to 0 , -19 )", log[3])
        assertEquals("using font_Verdana tint #ffffff", log[4])
        // Text is now rendered on pixel boundaries, so this is now -5, was -5.5
        assertEquals("draw( 237 , 77 size 11 , 23 to -5 , -19 )", log[5])
        assertEquals("using font_Verdana tint #ffffff", log[6])
        assertEquals("draw( 237 , 77 size 11 , 23 to -11 , -19 )", log[7])

        log.clear()

        font.draw("a", color, 0f, 0f, HAlignment.LEFT)
        font.draw("a", color, 0f, 0f, HAlignment.LEFT, TextVAlignment.BASELINE)
        font.draw("a", color, 0f, 0f, HAlignment.RIGHT, TextVAlignment.TOP)
        font.draw("a", color, 0f, 0f, HAlignment.CENTER, TextVAlignment.CENTER)
        font.draw("a", color, 0f, 0f, HAlignment.RIGHT, TextVAlignment.BOTTOM)
        //assertEquals(5, log.size)

        assertEquals("using font_Verdana tint #ffffff", log[0])
        assertEquals("draw( 237 , 77 size 11 , 23 to 0 , -19 )", log[1]) // BASE
        assertEquals("using font_Verdana tint #ffffff", log[2])
        assertEquals("draw( 237 , 77 size 11 , 23 to 0 , -19 )", log[3]) // BASE
        assertEquals("using font_Verdana tint #ffffff", log[4])
        assertEquals("draw( 237 , 77 size 11 , 23 to -11 , 0 )", log[5]) // TOP
        assertEquals("using font_Verdana tint #ffffff", log[6])
        assertEquals("draw( 237 , 77 size 11 , 23 to -5 , -11 )", log[7]) // CENTER
        assertEquals("using font_Verdana tint #ffffff", log[8])
        assertEquals("draw( 237 , 77 size 11 , 23 to -11 , -23 )", log[9]) // BOT

    }

}

package uk.co.nickthecoder.glok.application

import junit.framework.TestCase

class TestPosixArguments : TestCase() {

    fun testFlags() {
        val args = posixArguments(
            flags = listOf('a', 'b', 'c', 'd'),
            longFlags = emptyList(),
            parameters = emptyList(),
            args = arrayOf("-a", "-bc")
        )

        assertEquals(true, args.flag('a'))
        assertEquals(true, args.flag('b'))
        assertEquals(true, args.flag('c'))

    }

    fun testFlags2() {
        val args = posixArguments(
            flags = listOf('a', 'd'),
            longFlags = listOf("bc"),
            parameters = emptyList(),
            args = arrayOf("-a", "-bc", "result.txt")
        )

        assertEquals(true, args.flag('a'))
        assertEquals(false, args.flag('b'))
        assertEquals(false, args.flag('c'))
        assertEquals(true, args.flag("bc"))

        assertEquals("result.txt", args.remainder()[0])
    }


    fun testValue() {
        val args = posixArguments(
            flags = listOf('a', 'd'),
            longFlags = emptyList(),
            parameters = listOf("bc"),
            args = arrayOf("-a", "-bc", "result.txt")
        )

        assertEquals(true, args.flag('a'))
        assertEquals(false, args.flag('b'))
        assertEquals(false, args.flag('c'))
        assertEquals(false, args.flag("bc"))

        assertEquals("result.txt", args.value("bc"))

        assertEquals(0, args.remainder().size)
    }

    fun testMissing() {
        var exception: Exception? = null
        try {
            val args = posixArguments(
                flags = listOf('a', 'd'),
                longFlags = emptyList(),
                parameters = emptyList(),
                args = arrayOf("-a", "-bc", "result.txt")
            )
        } catch (e: Exception) {
            exception = e
        }

        assertEquals("Error parsing argument: -bc", exception?.message)
    }

}

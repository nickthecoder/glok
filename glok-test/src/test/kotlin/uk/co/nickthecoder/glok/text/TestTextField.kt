/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import junit.framework.TestCase
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.util.log

class TestTextField : TestCase() {

    val start = 0
    val initialText = "Hello World!"

    val afterH = 1
    val beforeWorld = 6
    val afterWorld = 11
    val beforeO = 7
    val afterO = 8

    lateinit var textField: TextField

    override fun setUp() {
        log.level = 0
        backend = DummyBackend()
        textField = TextField()
        assertEquals(false, textField.document.history.undoable)
        textField.text = initialText
    }

    fun testSetText() {

        with(textField) {
            assertEquals(false, document.history.undoable)

            assertEquals(initialText, textField.text)
            assertEquals(initialText, textField.document.text)
            assertEquals(0, textField.caretIndex)
            assertEquals(0, textField.anchorIndex)

            caretIndex = beforeWorld
            anchorIndex = afterWorld

        }
    }

    /**
     * Checks that InvalidationListeners are called the correct number of times.
     */
    fun testTextPropertyListener() {

        var changeCount = 0

        with(textField) {
            text = ""
            val listener = textField.textProperty.addListener {
                changeCount ++
            }

            assertEquals(0, changeCount)
            text = "Hello"
            assertEquals(1, changeCount)
            document.clear()
            assertEquals(2, changeCount)
        }
    }

    /**
     * Checks that changeListeners are called the correct number of times, and old/new values are correct.
     */
    fun testTextPropertyChangeListener() {

        var oldText = "?"
        var newText = "?"
        var changeCount = 0

        with(textField) {
            text = ""
            val listener = textField.textProperty.addChangeListener { _, old, new ->
                oldText = old
                newText = new
                changeCount ++
            }

            text = "Hello"
            assertEquals(1, changeCount)
            assertEquals("", oldText)
            assertEquals("Hello", newText)
            document.clear()
            assertEquals(2, changeCount)
            assertEquals("Hello", oldText)
            assertEquals("", newText)
        }
    }

    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Before the deletion, so no changes expected.
     */
    fun testRepositionAfterDelete1() {
        with(textField) {
            text = initialText

            caretIndex = afterH
            anchorIndex = afterH

            delete(beforeO, afterO) // Delete `o` from World

            assertEquals(afterH, caretIndex)
            assertEquals(afterH, anchorIndex)

            // Undo redo should affect it
            undo()
            assertEquals(afterH, caretIndex)
            assertEquals(afterH, anchorIndex)
            redo()
            assertEquals(afterH, caretIndex)
            assertEquals(afterH, anchorIndex)
        }
    }

    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Anchor beyond the deletion, subtract 1
     */
    fun testRepositionAfterDelete2() {
        with(textField) {

            // Select "World"
            caretIndex = beforeWorld
            anchorIndex = afterWorld

            // Delete "o"
            delete(beforeO, afterO)

            assertEquals(beforeWorld, caretIndex)
            assertEquals(afterWorld - 1, anchorIndex)

            undo()
            assertEquals(beforeWorld, caretIndex)
            assertEquals(afterWorld, anchorIndex)
            redo()
            assertEquals(beforeWorld, caretIndex)
            assertEquals(afterWorld - 1, anchorIndex)

        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * When a default button is disabled, it should use the standard color, not the accent color.
 *
 * FYI. Neither buttons do anything!
 */
class TestDisabledDefaultButton : Application() {

    private val disableButtonProperty by booleanProperty(false)

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "Disabled Default Button"
            scene(600, 400) {
                root = borderPane {
                    center = checkBox("Disable") {
                        selectedProperty.bidirectionalBind(disableButtonProperty)
                    }
                    bottom = buttonBar {
                        add(ButtonMeaning.OK) {
                            button("OK") {
                                defaultButton = true
                                disabledProperty.bindTo(disableButtonProperty)
                            }
                        }
                        add(ButtonMeaning.CANCEL) {
                            button("Cancel") {
                                cancelButton = true
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            launch(TestDisabledDefaultButton::class.java)
        }
    }

}

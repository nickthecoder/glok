/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import junit.framework.TestCase
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.util.log

class TestTextArea : TestCase() {

    val start = TextPosition(0, 0)
    val initialText = "Hello World !!\nLine in the middle\nThe last line"

    val afterH = TextPosition(0, 1)
    val beforeWorld = TextPosition(0, 6)
    val afterWorld = TextPosition(0, 11)
    val beforeO = TextPosition(0, 7)
    val afterO = TextPosition(0, 8)
    val afterLine = TextPosition(1, 4)

    lateinit var textArea: TextArea

    override fun setUp() {
        log.level = 0
        backend = DummyBackend()
        textArea = TextArea()
        assertEquals(false, textArea.document.history.undoable)
        textArea.text = initialText
    }

    operator fun TextPosition.plus(columns: Int) = TextPosition(row, column + columns)
    operator fun TextPosition.minus(columns: Int) = TextPosition(row, column - columns)

    fun testSetText() {

        with(textArea) {
            // Setting text now clears the history, so this is now false.
            assertEquals(false, document.history.undoable)

            assertEquals(initialText, textArea.text)
            assertEquals(start, textArea.caretPosition)
            assertEquals(start, textArea.anchorPosition)
        }
    }

    /**
     * Checks that InvalidationListeners are called the correct number of times.
     */
    fun testTextPropertyListener() {

        var changeCount = 0

        with(textArea) {
            text = ""
            textArea.textProperty.addListener {
                changeCount ++
            }
            assertEquals(0, changeCount)
            text = "Hello"
            // Hmm, when we set `text`, it is implemented using
            assertEquals(1, changeCount)

            document.clear()
            assertEquals(2, changeCount)
        }
    }

    /**
     * Checks that changeListeners are called the correct number of times, and old/new values are correct.
     */
    fun testTextPropertyChangeListener() {

        var oldText = "?"
        var newText = "?"
        var changeCount = 0

        with(textArea) {
            text = ""
            textArea.textProperty.addChangeListener { _, old, new ->
                oldText = old
                newText = new
                changeCount ++
            }

            text = "Hello"
            assertEquals(1, changeCount)
            assertEquals("", oldText)
            assertEquals("Hello", newText)
            document.clear()
            assertEquals(2, changeCount)
            assertEquals("Hello", oldText)
            assertEquals("", newText)
        }
    }

    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Before the deletion, so no changes expected.
     */
    fun testRepositionAfterDelete1() {
        with(textArea) {
            text = initialText

            caretPosition = afterH
            anchorPosition = afterH

            delete(beforeO, afterO) // Delete `o` from World

            assertEquals(afterH, caretPosition)
            assertEquals(afterH, anchorPosition)

            // Undo redo should affect it
            undo()
            assertEquals(afterO, caretPosition)
            assertEquals(afterO, anchorPosition)
            redo()
            assertEquals(beforeO, caretPosition)
            assertEquals(beforeO, anchorPosition)
        }
    }

    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Anchor beyond the deletion, subtract 1
     */
    fun testRepositionAfterDelete2() {
        with(textArea) {

            // Select "World"
            caretPosition = beforeWorld
            anchorPosition = afterWorld

            // Delete "o"
            delete(beforeO, afterO)

            assertEquals(beforeWorld, caretPosition)
            assertEquals(afterWorld - 1, anchorPosition)

            undo()
            assertEquals(afterO, caretPosition)
            assertEquals(afterO, anchorPosition)
            redo()
            assertEquals(beforeO, caretPosition)
            assertEquals(beforeO, anchorPosition)

        }
    }

    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Similar to 2, but anchor was on the next line.
     */
    fun testRepositionAfterDelete2b() {
        with(textArea) {

            // Select "World\nLine"
            caretPosition = beforeWorld
            anchorPosition = afterLine

            // Delete "o"
            delete(beforeO, beforeO + 1)
            val expectedAnchor = afterLine // No rows nor columns changed!

            assertEquals(beforeWorld, caretPosition)
            assertEquals(expectedAnchor, anchorPosition)

            undo()
            assertEquals(afterO, caretPosition)
            assertEquals(afterO, anchorPosition)
            redo()
            assertEquals(beforeO, caretPosition)
            assertEquals(beforeO, anchorPosition)
        }
    }


    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Select World, and delete the o
     */
    fun testRepositionAfterDelete3() {
        with(textArea) {

            // Select "o"
            caretPosition = beforeO
            anchorPosition = afterO

            // Delete "World...Line"
            delete(beforeWorld, afterLine)

            assertEquals(beforeWorld, caretPosition)
            assertEquals(beforeWorld, anchorPosition)

            undo()
            assertEquals(afterLine, caretPosition)
            assertEquals(afterLine, anchorPosition)
            redo()
            assertEquals(beforeWorld, caretPosition)
            assertEquals(beforeWorld, anchorPosition)
        }
    }

    /**
     * When we delete text, are caretPosition and anchorPosition updated correctly?
     * Exactly the same a 3, but deleting part of the next line too.
     */
    fun testRepositionAfterDelete3b() {
        with(textArea) {

            // Select "o"
            caretPosition = beforeWorld
            anchorPosition = afterO

            // Delete "World..\nLine"
            delete(beforeWorld, afterWorld)

            assertEquals(beforeWorld, caretPosition)
            assertEquals(beforeWorld, anchorPosition)

            undo()
            assertEquals(afterWorld, caretPosition)
            assertEquals(afterWorld, anchorPosition)
            redo()
            assertEquals(beforeWorld, caretPosition)
            assertEquals(beforeWorld, anchorPosition)
        }
    }

}

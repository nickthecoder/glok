package uk.co.nickthecoder.glok.property

import junit.framework.TestCase
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.property.functions.or
import uk.co.nickthecoder.glok.property.functions.xor

class TestBooleanFunctions : TestCase() {

    val aProperty by booleanProperty(true)
    var a by aProperty

    val bProperty by booleanProperty(false)
    var b by bProperty

    override fun setUp() {
        a = true
        b = false
    }

    fun testNot() {
        assertTrue("pre", a)
        a = false
        assertFalse("post", a)

        assertFalse("pre", b)
        b = true
        assertTrue("post", b)
    }

    fun testOr() {
        val result = aProperty or bProperty
        assertEquals(true, result.value)
        a = false
        assertEquals(false, result.value)
    }

    fun testAnd() {
        val result = aProperty and bProperty
        assertEquals(false, result.value)
        b = true
        assertEquals(true, result.value)
    }

    fun testXor() {
        val result = aProperty xor bProperty
        assertEquals(true, result.value)
        b = true
        assertEquals(false, result.value)
        a = false
        b = false
        assertEquals(false, result.value)
    }

}

package uk.co.nickthecoder.glok.property

import junit.framework.TestCase
import uk.co.nickthecoder.glok.property.boilerplate.SimpleStringProperty

class TestBind : TestCase() {

    private val helloProperty = SimpleStringProperty("Hello")
    private val textProperty = SimpleStringProperty("Text")
    private val thirdProperty = SimpleStringProperty("Third")

    override fun setUp() {
        textProperty.unbind()
        thirdProperty.unbind()
        textProperty.value = "Text"
        thirdProperty.value = "Third"
    }

    fun testBind() {
        assertEquals("Hello", helloProperty.value)
        assertEquals("Text", textProperty.value)
        textProperty.bindTo(helloProperty)
        assertEquals("Hello", helloProperty.value)
        assertEquals("Hello", textProperty.value)

        helloProperty.value = "Hi"
        assertEquals("Hi", helloProperty.value)
        assertEquals("Hi", textProperty.value)
    }

    fun testInvalidedAfterBind() {
        var helloChangeCounter = 0
        var textChangeCounter = 0
        val helloListener = helloProperty.addListener { helloChangeCounter++ }
        val textListener = textProperty.addListener { textChangeCounter++ }

        // Just to stop the compiler from nagging. Listener uses weak ref, so we should keep a reference!
        assertNotNull(helloListener)
        assertNotNull(textListener)

        assertEquals(0, helloChangeCounter)
        textProperty.bindTo(helloProperty)
        assertEquals(0, helloChangeCounter)
        assertEquals(1, textChangeCounter)

        helloProperty.value = "Hi"
        assertEquals(1, helloChangeCounter)
        assertEquals(2, textChangeCounter)
    }

    /**
     * Same as [testInvalidedAfterBind], but when we bind, the value is the same, so no invalidation event should be fired.
     */
    fun testInvalidedAfterBindSameValue() {
        // New!
        textProperty.value = helloProperty.value

        var helloChangeCounter = 0
        var textChangeCounter = 0
        val helloListener = helloProperty.addListener { helloChangeCounter++ }
        val textListener = textProperty.addListener { textChangeCounter++ }

        // Just to stop the compiler from nagging. Listener uses weak ref, so we should keep a reference!
        assertNotNull(helloListener)
        assertNotNull(textListener)

        assertEquals(0, helloChangeCounter)
        textProperty.bindTo(helloProperty)
        assertEquals(0, helloChangeCounter)
        assertEquals(0, textChangeCounter) // Was 1 in previous test case

        helloProperty.value = "Hi"
        assertEquals(1, helloChangeCounter)
        assertEquals(1, textChangeCounter) // Was 2 in previous test case
    }

    /**
     * Same as [], but this time we have a chain of 3 bound properties.
     * Is third also changed?
     */
    fun testInvalidedAfterBindTwice() {
        var helloChangeCounter = 0
        var textChangeCounter = 0
        var thirdChangeCounter = 0
        val helloListener = helloProperty.addListener { helloChangeCounter++ }
        val textListener = textProperty.addListener { textChangeCounter++ }
        val thirdListener = textProperty.addListener { thirdChangeCounter++ }

        // Just to stop the compiler from nagging. Listener uses weak ref, so we should keep a reference!
        assertNotNull(helloListener)
        assertNotNull(textListener)
        assertNotNull(thirdListener)

        assertEquals(0, helloChangeCounter)
        textProperty.bindTo(helloProperty)
        thirdProperty.bindTo(textProperty)
        assertEquals(0, helloChangeCounter)
        assertEquals(1, textChangeCounter)
        assertEquals(1, thirdChangeCounter)

        helloProperty.value = "Hi"
        assertEquals(1, helloChangeCounter)
        assertEquals(2, textChangeCounter)
        assertEquals(2, thirdChangeCounter)

        assertEquals("Hi", thirdProperty.value)
    }

}

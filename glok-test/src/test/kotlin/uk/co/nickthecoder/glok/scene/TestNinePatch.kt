package uk.co.nickthecoder.glok.scene

import junit.framework.TestCase
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.PartialTexture
import uk.co.nickthecoder.glok.backend.backend

class TestNinePatch : TestCase() {

    override fun setUp() {
        backend = DummyBackend()
    }

    fun testNinePatchMetrics() {

        val image = backend.createTexture("testImage", 20, 30, IntArray(20 * 30 * 4) { 255 })

        val ninePatch = NinePatch(image, Edges(1f, 6f, 2f, 3f))
        assertEquals(image, ninePatch.image)

        // marginTop = 1
        assertEquals(1f, ninePatch.topLeftPatch.imageHeight)
        assertEquals(1f, ninePatch.topPatch.imageHeight)
        assertEquals(1f, ninePatch.topRightPatch.imageHeight)

        // 30 - 1 - 2 = 27
        assertEquals(27f, ninePatch.leftPatch.imageHeight)
        assertEquals(27f, ninePatch.centerPatch.imageHeight)
        assertEquals(27f, ninePatch.rightPatch.imageHeight)

        // marginBottom = 2
        assertEquals(2f, ninePatch.bottomLeftPatch.imageHeight)
        assertEquals(2f, ninePatch.bottomPatch.imageHeight)
        assertEquals(2f, ninePatch.bottomRightPatch.imageHeight)

        // marginLeft = 3
        assertEquals(3f, ninePatch.topLeftPatch.imageWidth)
        assertEquals(3f, ninePatch.leftPatch.imageWidth)
        assertEquals(3f, ninePatch.bottomLeftPatch.imageWidth)

        // 20 - 6 - 3 = 11
        assertEquals(11f, ninePatch.topPatch.imageWidth)
        assertEquals(11f, ninePatch.centerPatch.imageWidth)
        assertEquals(11f, ninePatch.bottomPatch.imageWidth)

        // marginRight = 6
        assertEquals(6f, ninePatch.topRightPatch.imageWidth)
        assertEquals(6f, ninePatch.rightPatch.imageWidth)
        assertEquals(6f, ninePatch.bottomRightPatch.imageWidth)

        val topLeftPatch = ninePatch.topLeftPatch as PartialTexture
        val topPatch = ninePatch.topPatch as PartialTexture
        val topRightPatch = ninePatch.topRightPatch as PartialTexture

        val leftPatch = ninePatch.leftPatch as PartialTexture
        val centerPatch = ninePatch.centerPatch as PartialTexture
        val rightPatch = ninePatch.rightPatch as PartialTexture

        val bottomLeftPatch = ninePatch.bottomLeftPatch as PartialTexture
        val bottomPatch = ninePatch.bottomPatch as PartialTexture
        val bottomRightPatch = ninePatch.bottomRightPatch as PartialTexture

        // srcX
        assertEquals(0f, topLeftPatch.srcX)
        assertEquals(0f, leftPatch.srcX)
        assertEquals(0f, topLeftPatch.srcX)

        assertEquals(3f, topPatch.srcX)
        assertEquals(3f, centerPatch.srcX)
        assertEquals(3f, bottomPatch.srcX)

        // 20 - 6
        assertEquals(14f, topRightPatch.srcX)
        assertEquals(14f, rightPatch.srcX)
        assertEquals(14f, bottomRightPatch.srcX)

        // srcY
        assertEquals(0f, topLeftPatch.srcY)
        assertEquals(0f, topPatch.srcY)
        assertEquals(0f, topRightPatch.srcY)

        assertEquals(1f, leftPatch.srcY)
        assertEquals(1f, centerPatch.srcY)
        assertEquals(1f, rightPatch.srcY)

        // 30 - 2
        assertEquals(28f, bottomLeftPatch.srcY)
        assertEquals(28f, bottomPatch.srcY)
        assertEquals(28f, bottomRightPatch.srcY)

    }


    /**
     * The same as [testNinePatchMetrics], but instead of using a whole Texture, we use a partial image (
     * of the same size), within a lager texture.
     * NOTE. 120, 130 shouldn't appear in the expected values - this is an arbitrarily large texture!
     */
    fun testInnerNinePatchMetrics() {

        // An over-sized image
        val image = backend.createTexture("testImage", 120, 130, IntArray(120 * 130 * 4) { 255 })

        // subImage is the same size of [textNinePatchMetrics]'s image.
        val subImage = image.partialImage(1f, 2f, 20f, 30f)

        // The same margins as before
        val ninePatch = NinePatch(subImage, Edges(1f, 6f, 2f, 3f))

        assertEquals(image, (ninePatch.image as PartialTexture).texture)

        // Same as before
        assertEquals(1f, ninePatch.topLeftPatch.imageHeight)
        assertEquals(1f, ninePatch.topPatch.imageHeight)
        assertEquals(1f, ninePatch.topRightPatch.imageHeight)

        assertEquals(27f, ninePatch.leftPatch.imageHeight)
        assertEquals(27f, ninePatch.centerPatch.imageHeight)
        assertEquals(27f, ninePatch.rightPatch.imageHeight)

        assertEquals(2f, ninePatch.bottomLeftPatch.imageHeight)
        assertEquals(2f, ninePatch.bottomPatch.imageHeight)
        assertEquals(2f, ninePatch.bottomRightPatch.imageHeight)

        assertEquals(3f, ninePatch.topLeftPatch.imageWidth)
        assertEquals(3f, ninePatch.leftPatch.imageWidth)
        assertEquals(3f, ninePatch.bottomLeftPatch.imageWidth)

        assertEquals(11f, ninePatch.topPatch.imageWidth)
        assertEquals(11f, ninePatch.centerPatch.imageWidth)
        assertEquals(11f, ninePatch.bottomPatch.imageWidth)

        assertEquals(6f, ninePatch.topRightPatch.imageWidth)
        assertEquals(6f, ninePatch.rightPatch.imageWidth)
        assertEquals(6f, ninePatch.bottomRightPatch.imageWidth)

        val topLeftPatch = ninePatch.topLeftPatch as PartialTexture
        val topPatch = ninePatch.topPatch as PartialTexture
        val topRightPatch = ninePatch.topRightPatch as PartialTexture

        val leftPatch = ninePatch.leftPatch as PartialTexture
        val centerPatch = ninePatch.centerPatch as PartialTexture
        val rightPatch = ninePatch.rightPatch as PartialTexture

        val bottomLeftPatch = ninePatch.bottomLeftPatch as PartialTexture
        val bottomPatch = ninePatch.bottomPatch as PartialTexture
        val bottomRightPatch = ninePatch.bottomRightPatch as PartialTexture

        // +1 from before
        assertEquals(1f, topLeftPatch.srcX)
        assertEquals(1f, leftPatch.srcX)
        assertEquals(1f, topLeftPatch.srcX)

        assertEquals(4f, topPatch.srcX)
        assertEquals(4f, centerPatch.srcX)
        assertEquals(4f, bottomPatch.srcX)

        assertEquals(15f, topRightPatch.srcX)
        assertEquals(15f, rightPatch.srcX)
        assertEquals(15f, bottomRightPatch.srcX)

        // +2 from before
        assertEquals(2f, topLeftPatch.srcY)
        assertEquals(2f, topPatch.srcY)
        assertEquals(2f, topRightPatch.srcY)

        assertEquals(3f, leftPatch.srcY)
        assertEquals(3f, centerPatch.srcY)
        assertEquals(3f, rightPatch.srcY)

        assertEquals(30f, bottomLeftPatch.srcY)
        assertEquals(30f, bottomPatch.srcY)
        assertEquals(30f, bottomRightPatch.srcY)

    }


}

package uk.co.nickthecoder.glok.property

import junit.framework.TestCase
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.property.functions.plus
import uk.co.nickthecoder.glok.property.functions.prefixWith
import uk.co.nickthecoder.glok.property.functions.startsWith
import uk.co.nickthecoder.glok.property.functions.substring

class TestStringFunctions : TestCase() {

    val helloProperty by stringProperty("Hello ")
    var hello by helloProperty

    val personProperty by stringProperty("Nick")
    var person by personProperty

    override fun setUp() {
        hello = "Hello "
        person = "Nick"
    }

    fun testConcat() {
        val func = helloProperty + personProperty

        assertEquals("Hello Nick", func.value)
        person = "Lilian"
        assertEquals("Hello Lilian", func.value)
    }

    fun testPrefixWith() {
        val func = personProperty.prefixWith("Mr ")

        assertEquals( "Mr Nick", func.value )
        person = "Robinson"
        assertEquals( "Mr Robinson", func.value )
    }


    fun testSubstring1() {
        val func = personProperty.substring(1)

        assertEquals( "ick", func.value )
        person = "Lilian"
        assertEquals( "ilian", func.value )
    }

    fun testSubstring2() {
        val func = personProperty.substring(0 until 1)

        assertEquals( "N", func.value )
        person = "Lilian"
        assertEquals( "L", func.value )
    }

    fun testStartsWith() {
        val func = personProperty.startsWith("N")

        assertEquals( true, func.value )
        person = "Lilian"
        assertEquals( false, func.value )
    }

}

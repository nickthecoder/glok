package uk.co.nickthecoder.glok.layout

import junit.framework.TestCase
import uk.co.nickthecoder.glok.control.Pane
import uk.co.nickthecoder.glok.control.VBox
import uk.co.nickthecoder.glok.internal.DangerousUnitTestShims
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Scene

/**
 * These tests were created after [TestHBox], and are exactly the same,
 * with height switched with widths etc.
 */
class TestVBox : TestCase() {

    /**
     * For each property, we test the initial value via the `var` and the `property.value`.
     * Then change it via the `var`, and test again.
     * Finally, we change it via `property.value` and test again.
     */
    fun testProperties() {
        VBox().apply {
            assertEquals(0f, spacing)
            assertEquals(0f, spacingProperty.value)
            spacing = 3f
            assertEquals(3f, spacing)
            assertEquals(3f, spacingProperty.value)
            spacingProperty.value = 2f
            assertEquals(2f, spacing)
            assertEquals(2f, spacingProperty.value)

            assertEquals(Edges(), padding)
            assertEquals(Edges(), paddingProperty.value)
            padding = Edges(1f, 2f, 3f, 4f)
            assertEquals(Edges(1f, 2f, 3f, 4f), padding)
            assertEquals(Edges(1f, 2f, 3f, 4f), paddingProperty.value)
            paddingProperty.value = Edges(2f, 3f)
            assertEquals(Edges(2f, 3f, 2f, 3f), padding)
            assertEquals(Edges(2f, 3f), paddingProperty.value)

            assertEquals(false, fillWidth)
            assertEquals(false, fillWidthProperty.value)
            fillWidth = true
            assertEquals(true, fillWidth)
            assertEquals(true, fillWidthProperty.value)
            fillWidth = false
            assertEquals(false, fillWidth)
            assertEquals(false, fillWidthProperty.value)
        }
    }

    fun testSingleSmall() {
        val pane1 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 24f
        }
        val box = VBox().apply {
            fillWidth = false
            children.add(pane1)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, box.localY)
        assertEquals(0f, box.localX)
        assertEquals(100f, box.height)
        assertEquals(50f, box.width)

        assertEquals(0f, pane1.localY)
        assertEquals(0f, pane1.localX)
        assertEquals(16f, pane1.height)
        assertEquals(24f, pane1.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.width) // Filled

    }

    fun testSingleSmallWithMargin() {
        val pane1 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 2f
        }
        val box = VBox().apply {
            fillWidth = false
            children.add(pane1)
            padding = Edges(40f, 30f, 20f, 10f)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(40f, pane1.localY)
        assertEquals(10f, pane1.localX)
        assertEquals(16f, pane1.height)
        assertEquals(2f, pane1.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(10f, pane1.width)
    }


    fun testGrowSingleSmall() {
        val pane1 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 24f
            growPriority = 1f
        }
        val box = VBox().apply {
            fillWidth = false
            children.add(pane1)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localY)
        assertEquals(0f, pane1.localX)
        assertEquals(100f, pane1.height)
        assertEquals(24f, pane1.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.width)
    }

    fun testTwoBothGrow() {
        val pane1 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 24f
            growPriority = 1f
        }
        val pane2 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 20f
            growPriority = 1f
        }
        val box = VBox().apply {
            fillWidth = false
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localY)
        assertEquals(0f, pane1.localX)
        assertEquals(45f, pane1.height)
        assertEquals(24f, pane1.width)

        assertEquals(55f, pane2.localY)
        assertEquals(0f, pane2.localX)
        assertEquals(45f, pane2.height)
        assertEquals(20f, pane2.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.width)
        assertEquals(50f, pane2.width)

    }


    fun testTwoBothShrink() {
        val pane1 = Pane().apply {
            overridePrefHeight = 100f
            overridePrefWidth = 24f
            shrinkPriority = 1f
        }
        val pane2 = Pane().apply {
            overridePrefHeight = 100f
            overridePrefWidth = 20f
            shrinkPriority = 1f
        }
        val box = VBox().apply {
            fillWidth = false
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localY)
        assertEquals(0f, pane1.localX)
        assertEquals(45f, pane1.height)
        assertEquals(24f, pane1.width)

        assertEquals(55f, pane2.localY)
        assertEquals(0f, pane2.localX)
        assertEquals(45f, pane2.height)
        assertEquals(20f, pane2.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.width)
        assertEquals(50f, pane2.width)
    }

    /**
     * As [testTwoBothGrow], but with growPriorities of 0, so no growing
     */
    fun testTwoBothStay() {
        val pane1 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 24f
            growPriority = 0f
        }
        val pane2 = Pane().apply {
            overridePrefHeight = 16f
            overridePrefWidth = 20f
            growPriority = 0f
        }
        val box = VBox().apply {
            fillWidth = false
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localY)
        assertEquals(0f, pane1.localX)
        assertEquals(16f, pane1.height)
        assertEquals(24f, pane1.width)

        assertEquals(26f, pane2.localY)
        assertEquals(0f, pane2.localX)
        assertEquals(16f, pane2.height)
        assertEquals(20f, pane2.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.width)
        assertEquals(50f, pane2.width)
    }


    /**
     * There is a 3rd code path, then the children's prefWidths are exactly the right size
     * for the parent's width.
     */
    fun testTwoExactSize() {
        val pane1 = Pane().apply {
            overridePrefHeight = 45f
            overridePrefWidth = 24f
            growPriority = 1f
        }
        val pane2 = Pane().apply {
            overridePrefHeight = 45f
            overridePrefWidth = 20f
            growPriority = 1f
        }
        val box = VBox().apply {
            fillWidth = false
            spacing = 10f
            children.addAll(pane1, pane2)
        }
        val scene = Scene(box, 50f, 100f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, pane1.localY)
        assertEquals(0f, pane1.localX)
        assertEquals(45f, pane1.height)
        assertEquals(24f, pane1.width)

        assertEquals(55f, pane2.localY)
        assertEquals(0f, pane2.localX)
        assertEquals(45f, pane2.height)
        assertEquals(20f, pane2.width)

        box.fillWidth = true
        DangerousUnitTestShims.layout(scene)
        assertEquals(50f, pane1.width)
        assertEquals(50f, pane2.width)
    }

}

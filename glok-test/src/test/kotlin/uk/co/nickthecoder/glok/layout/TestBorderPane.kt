package uk.co.nickthecoder.glok.layout

import junit.framework.TestCase
import uk.co.nickthecoder.glok.control.BorderPane
import uk.co.nickthecoder.glok.control.Pane

class TestBorderPane : TestCase() {

    /**
     * For each property, we test the initial value via the `var` and the `property.value`.
     * Then change it via the `var`, and test again.
     * Finally, we change it via `property.value` and test again.
     */
    fun testProperties() {

        val pane = Pane()

        BorderPane().apply {

            // TOP
            assertEquals(null, top)
            assertEquals(null, topProperty.value)
            top = pane
            assertEquals(pane, top)
            assertEquals(pane, topProperty.value)
            topProperty.value = null
            assertEquals(null, top)
            assertEquals(null, topProperty.value)

            // RIGHT
            assertEquals(null, right)
            assertEquals(null, rightProperty.value)
            right = pane
            assertEquals(pane, right)
            assertEquals(pane, rightProperty.value)
            rightProperty.value = null
            assertEquals(null, right)
            assertEquals(null, rightProperty.value)


            // BOTTOM
            assertEquals(null, bottom)
            assertEquals(null, bottomProperty.value)
            bottom = pane
            assertEquals(pane, bottom)
            assertEquals(pane, bottomProperty.value)
            bottomProperty.value = null
            assertEquals(null, bottom)
            assertEquals(null, bottomProperty.value)


            // LEFT
            assertEquals(null, left)
            assertEquals(null, leftProperty.value)
            left = pane
            assertEquals(pane, left)
            assertEquals(pane, leftProperty.value)
            leftProperty.value = null
            assertEquals(null, left)
            assertEquals(null, leftProperty.value)


            // CENTER
            assertEquals(null, center)
            assertEquals(null, centerProperty.value)
            center = pane
            assertEquals(pane, center)
            assertEquals(pane, centerProperty.value)
            centerProperty.value = null
            assertEquals(null, center)
            assertEquals(null, centerProperty.value)

        }
    }

    /**
     * The children are not added/removed directly, but by listening to `top` property etc.
     * Ensure that the listener is working correctly.
     */
    fun testChildrenList() {

        val pane = Pane()

        BorderPane().apply {

            assertTrue(children.isEmpty())
            top = pane
            assertEquals(1, children.size)
            assertEquals(pane, children.first())
            top = null
            assertEquals(0, children.size)


            assertTrue(children.isEmpty())
            right = pane
            assertEquals(1, children.size)
            assertEquals(pane, children.first())
            right = null
            assertEquals(0, children.size)


            assertTrue(children.isEmpty())
            bottom = pane
            assertEquals(1, children.size)
            assertEquals(pane, children.first())
            bottom = null
            assertEquals(0, children.size)


            assertTrue(children.isEmpty())
            left = pane
            assertEquals(1, children.size)
            assertEquals(pane, children.first())
            left = null
            assertEquals(0, children.size)


            assertTrue(children.isEmpty())
            center = pane
            assertEquals(1, children.size)
            assertEquals(pane, children.first())
            center = null
            assertEquals(0, children.size)

        }
    }


}

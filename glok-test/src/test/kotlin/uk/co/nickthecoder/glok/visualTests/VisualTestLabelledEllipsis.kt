package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Labelled
import uk.co.nickthecoder.glok.property.boilerplate.alignmentProperty
import uk.co.nickthecoder.glok.property.boilerplate.contentDisplayProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.property.replaceNullWith
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * Visual test that all three values of [Labelled.ellipsisAlignment] work as expected.
 *
 * Shrink the window, and look where `…` is placed. Should be right, left and center.
 */
class VisualTestLabelledEllipsis : Application() {

    val graphicPositionProperty by contentDisplayProperty(ContentDisplay.LEFT)

    val alignmentProperty by alignmentProperty(Alignment.CENTER_CENTER)

    val myStringProperty by stringProperty("")
    var myString by myStringProperty

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "Labelled Ellipsis (Visual Test)"
            scene {
                root = borderPane {
                    top = formGrid {

                        + row("Content Display") {
                            right = choiceBox<ContentDisplay> {
                                items.addAll(ContentDisplay.entries)
                                value = ContentDisplay.LEFT
                                graphicPositionProperty.bindTo(valueProperty.replaceNullWith(ContentDisplay.TEXT_ONLY))
                            }
                        }
                        + row("Alignment") {
                            right = choiceBox<Alignment> {
                                items.addAll(Alignment.entries)
                                value = Alignment.CENTER_CENTER
                                alignmentProperty.bindTo(valueProperty.replaceNullWith(Alignment.CENTER_CENTER))
                            }
                        }
                        + row("Change Status Bar") {
                            right = hBox {
                                spacing = 10f
                                + button("Short") {
                                    onAction { myString = "Short" }
                                }
                                + button("Long") {
                                    onAction { myString = "Very ".repeat(13) + " long" }
                                }
                            }
                        }
                    }

                    center = vBox {
                        padding(20)
                        fillWidth = true

                        + testLabel(HAlignment.RIGHT)
                        + testLabel(HAlignment.LEFT)
                        + testLabel(HAlignment.CENTER)

                        + label("")

                        // To ensure that there isn't a problem with odd/even length strings,
                        // try again, with text which is longer by 1 character.

                        + testLabel(HAlignment.RIGHT, true)
                        + testLabel(HAlignment.LEFT, true)
                        + testLabel(HAlignment.CENTER, true)
                    }.withExpandWidth()

                    bottom = toolBar {

                        + label("") {
                            textProperty.bindTo(myStringProperty)
                            ellipsisAlignment = HAlignment.CENTER

                            // Adding potentially long strings to a ToolBar is tricky. It is vital to set
                            // overridePrefWidth, otherwise the ToolBar will put the label in the "overflow" popup.
                            overridePrefWidth = 100f
                            // We then make it stretchy, so that it grows WIDER than overridePrefWidth when there is room.
                            growPriority = 1f
                        }
                        + spacer()
                        + label("10:10") // A Fake text-position indicator on the right.
                    }

                }
            }
            show()
        }
    }

    fun testLabel(alignment: HAlignment, extra: Boolean = false, block: (Label.() -> Unit)? = null): Label {
        val label = label(if (extra) "$LONG_TEXT!" else LONG_TEXT) {
            graphic = Tantalum.icon("information")
            ellipsisAlignment = alignment
            overrideMinWidth = 0f
            contentDisplayProperty.bindTo(graphicPositionProperty)
            alignmentProperty.bindTo(this@VisualTestLabelledEllipsis.alignmentProperty)
        }
        block?.let {
            label.it()
        }
        return label
    }

    companion object {

        const val LONG_TEXT = "The quick brown fox jumped over the lazy dog"

        @JvmStatic
        fun main(vararg args: String) {
            launch(VisualTestLabelledEllipsis::class)
        }
    }
}

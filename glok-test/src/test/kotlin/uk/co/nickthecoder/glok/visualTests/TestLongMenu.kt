/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * Test what happens when menus are long.
 * There are 2 different edge cases :
 * 1. Taller than the window's height
 * 2. Shorter than the window's height, but the "normal" position isn't suitable. Move it up.
 */
class TestLongMenu : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "LongMenu"
            scene(600, 400) {
                root = borderPane {
                    top = menuBar {
                        + menu("File") {
                            + menuItem("Open...") {}
                        }
                        + menu("Edit") {
                            for (i in 0..8) {
                                + subMenu("Sub Menu #$i") {
                                    for (j in 0..8) {
                                        + menuItem("Menu Item #$j") {
                                        }
                                    }
                                }
                            }
                            + subMenu("Really Long") {
                                for (j in 0..20) {
                                    + menuItem("Menu Item #$j") {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            launch(TestLongMenu::class.java)
        }
    }
}

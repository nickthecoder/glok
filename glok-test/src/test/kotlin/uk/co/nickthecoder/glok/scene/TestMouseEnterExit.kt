/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import junit.framework.TestCase
import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.DummyWindow
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.VBox
import uk.co.nickthecoder.glok.internal.DangerousUnitTestShims
import uk.co.nickthecoder.glok.scene.dsl.hBox
import uk.co.nickthecoder.glok.scene.dsl.scene

class MockApplication : Application() {
    override fun start(primaryStage: Stage) {
    }
}

/**
 * Tests Mouse Entered / Exited events.
 * We log each in/out as a string. ">${id}" for entering, and "${id}" for exiting.
 */
class TestMouseEnterExit : TestCase() {

    lateinit var stage: Stage
    lateinit var scene: Scene
    lateinit var buffer: StringBuffer
    lateinit var window: DummyWindow

    private fun entered(id: String) {
        buffer.append(">").append(id).append(" ")
    }

    private fun exited(id: String) {
        buffer.append(id).append("> ")
    }

    private fun pressed(id: String) {
        buffer.append("[").append(id).append("] ")
    }

    /* 0        4        8
       ------------------- 0
       |        |        |
       |        |        |
       |   h1   |   h3   |
       |        |        |
       --  v1  -A-  v2  -- 4
       |        |        |
       |        |        |
       |   h2   |  h4    |
       |        |        |
       ------------------- 8
    */
    override fun setUp() {
        buffer = StringBuffer()
        Application.instance = MockApplication()
        backend = DummyBackend()
        GlokSettings.globalScale = 1f

        stage = RegularStage().apply {
            scene(8, 8) {
                root = hBox {
                    id = "A"
                    fillHeight = true
                    overridePrefWidth = 8f
                    overridePrefHeight = 8f
                    onMouseEntered { entered(id) }
                    onMouseExited { exited(id) }

                    + vBox("v1").apply {
                        + hBox("h1")
                        + hBox("h2")
                    }
                    + vBox("v2").apply {
                        + hBox("h3")
                        + hBox("h4")
                    }
                }
            }
            show()
        }
        scene = stage.scene !!
        DangerousUnitTestShims.layout(scene)
        //scene.dump()
        window = (backend as DummyBackend).latestWindow
    }

    private fun vBox(id: String) = VBox().apply {
        this.id = id
        overridePrefWidth = 4f
        overridePrefHeight = 8f
        onMouseEntered { entered(id) }
        onMouseExited { exited(id) }
        onMousePressed { event ->
            pressed(id)
            event.capture()
            event.consume()
        }
    }

    private fun hBox(id: String) = HBox().apply {
        this.id = id
        fillHeight = true
        overridePrefWidth = 4f
        overridePrefHeight = 4f
        onMouseEntered { entered(id) }
        onMouseExited { exited(id) }
        onMousePressed { event ->
            pressed(id)
            event.capture()
            event.consume()
        }
    }

    fun testMoveAcross() {
        window.mockMouseMove(2, 2)
        buffer.append("RIGHT ")
        window.mockMouseMove(6, 2)

        // Enter h1 v2 A
        // Leave h1 v1, Enter h3, v2
        assertEquals(">h1 >v1 >A RIGHT h1> v1> >h3 >v2 ", buffer.toString())

        buffer.append("LEFT ")
        window.mockMouseMove(2, 2)

        // Leave h3 v2, Enter h1 v1
        assertEquals(">h1 >v1 >A RIGHT h1> v1> >h3 >v2 LEFT h3> v2> >h1 >v1 ", buffer.toString())
    }

    fun testMoveDownUp() {
        window.mockMouseMove(2, 2)
        buffer.append("DOWN ")
        window.mockMouseMove(2, 6)

        // Enter h1 v1 A
        // Leave h1, Enter h2
        assertEquals(">h1 >v1 >A DOWN h1> >h2 ", buffer.toString())

        // Move back up
        buffer.append("UP ")
        window.mockMouseMove(2, 2)

        assertEquals(">h1 >v1 >A DOWN h1> >h2 UP h2> >h1 ", buffer.toString())

    }

    /**
     * Press and drag h1.
     * Imagine h1..4 are Buttons.
     * We don't expect the other buttons to get the enter/exit events
     * (we only want h1 to change its hover state)
     */
    fun testPressAndDragDownUp() {
        window.mockMouseMove(2, 2)
        window.mockPress()
        buffer.append("DOWN ")
        window.mockMouseMove(2, 6)

        // Enter h1 v1 A
        // Leave h1
        assertEquals(">h1 >v1 >A [h1] DOWN h1> ", buffer.toString())

        // Move back up
        buffer.append("UP ")
        window.mockMouseMove(2, 2)

        // Enter h1
        assertEquals(">h1 >v1 >A [h1] DOWN h1> UP >h1 ", buffer.toString())

    }

}

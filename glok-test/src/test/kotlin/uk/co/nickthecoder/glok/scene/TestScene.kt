/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import junit.framework.TestCase
import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Pane
import uk.co.nickthecoder.glok.internal.DangerousUnitTestShims
import uk.co.nickthecoder.glok.scene.dsl.scene

class TestApplication : Application() {
    override fun start(primaryStage: Stage) {
    }
}

class TestScene : TestCase() {

    override fun setUp() {
        Application.instance = TestApplication()
        backend = DummyBackend()
    }

    fun testProperties() {

        val scene = scene(300, 200) {
            root = Label("Hello World")
        }
        val root = scene.root

        scene.backgroundColor = Color.RED

        assertEquals(root, scene.root)
        assertEquals(root, scene.rootProperty.value)

        assertEquals(300f, scene.width)
        assertEquals(300f, scene.widthProperty.value)

        assertEquals(200f, scene.height)
        assertEquals(200f, scene.heightProperty.value)

        assertEquals(Color.RED, scene.backgroundColor)
        assertEquals(Color.RED, scene.backgroundColorProperty.value)

        assertEquals(scene, scene.root.scene)

        assertEquals(true, scene.requestRedraw)
        assertEquals(true, scene.requestRedrawProperty.value)
        assertEquals(true, scene.requestLayout)
        assertEquals(true, scene.requestLayoutProperty.value)
        assertEquals(true, scene.requestRestyling)
        assertEquals(true, scene.requestRestylingProperty.value)

        scene.backgroundColor = Color.GREEN

        assertEquals(Color.GREEN, scene.backgroundColor)
        assertEquals(Color.GREEN, scene.backgroundColorProperty.value)

        val stage = RegularStage()
        stage.scene = scene

        assertEquals(true, scene.requestRedraw)
        assertEquals(true, scene.requestRedrawProperty.value)
        assertEquals(true, scene.requestLayout)
        assertEquals(true, scene.requestLayoutProperty.value)
        assertEquals(true, scene.requestRestyling)
        assertEquals(true, scene.requestRestylingProperty.value)

        // Now, restyle, layout and draw. Checking that the 3 requestXXX are correct after each step.

        DangerousUnitTestShims.restyle(scene)

        assertEquals(true, scene.requestRedraw)
        assertEquals(true, scene.requestRedrawProperty.value)
        assertEquals(true, scene.requestLayout)
        assertEquals(true, scene.requestLayoutProperty.value)
        assertEquals(false, scene.requestRestyling)
        assertEquals(false, scene.requestRestylingProperty.value)

        DangerousUnitTestShims.layout(scene)

        assertEquals(true, scene.requestRedraw)
        assertEquals(true, scene.requestRedrawProperty.value)
        assertEquals(false, scene.requestLayout)
        assertEquals(false, scene.requestLayoutProperty.value)
        assertEquals(false, scene.requestRestyling)
        assertEquals(false, scene.requestRestylingProperty.value)

        stage.draw() // Shouldn't do anything, because there is no window to draw to.

        assertEquals(true, scene.requestRedraw)
        assertEquals(true, scene.requestRedrawProperty.value)
        assertEquals(false, scene.requestLayout)
        assertEquals(false, scene.requestLayoutProperty.value)
        assertEquals(false, scene.requestRestyling)
        assertEquals(false, scene.requestRestylingProperty.value)

        stage.show()
        stage.draw()

        assertEquals(false, scene.requestRedraw)
        assertEquals(false, scene.requestRedrawProperty.value)
        assertEquals(false, scene.requestLayout)
        assertEquals(false, scene.requestLayoutProperty.value)
        assertEquals(false, scene.requestRestyling)
        assertEquals(false, scene.requestRestylingProperty.value)

    }

    /*
    private fun ObservableValue<*>.listeningBy(listeners: List<Any?>): Int {
        val (invalidationListeners, changeListeners) = if (this is ReadOnlyPropertyWrapper<*, *>) {
            Pair(listeners(), changeListeners())
        } else if (this is ObservableValueBase<*>) {
            Pair(listeners(), changeListeners())
        } else {
            return -1
        }

        var count = 0
        for (l in invalidationListeners) {
            if (listeners.contains(l.get())) {
                count++
            }
        }
        for (l in changeListeners) {
            if (listeners.contains(l.get())) {
                count++
            }
        }
        return count
    }
    */

    /*
    /**
     * Checks that scene is listening to the things it's supposed to listen to.
     */
    fun testListeners() {
        val scene = scene(300, 200) {
            root = Label("Hello World")
        }

        val listeners = scene.listeners()
        assertEquals(1, scene.rootProperty.listeningBy(listeners))
        assertEquals(1, scene.focusOwnerProperty.listeningBy(listeners))
        assertEquals(1, scene.themeProperty.listeningBy(listeners))

    }
    */

    /**
     * Ensures that the root node is give the whole size of the [Scene].
     */
    fun testImpliedSize() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 24f
        }
        val box = HBox().apply {
            fillHeight = true
            children.add(pane1)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, box.localX)
        assertEquals(0f, box.localY)
        assertEquals(100f, box.width)
        assertEquals(50f, box.height)

        assertEquals(null, scene.stage)
        assertEquals(null, scene.stageProperty.value)

    }

    /**
     * If the root Node's minWidth/Height is larger than the Scene, then
     * the root node should be given its min size, so part of the scene will be invisible -
     * off the right and bottom of the window.
     */
    fun testSceneSmallerThenMin() {
        val pane1 = Pane().apply {
            overridePrefWidth = 16f
            overridePrefHeight = 24f
            overrideMinWidth = 200f
            overrideMinHeight = 150f
        }
        val box = HBox().apply {
            fillHeight = true
            children.add(pane1)
        }
        val scene = Scene(box, 100f, 50f)
        DangerousUnitTestShims.layout(scene)

        assertEquals(0f, box.localX)
        assertEquals(0f, box.localY)
        assertEquals(200f, box.width)
        assertEquals(150f, box.height)
    }

}

package uk.co.nickthecoder.glok.property

import junit.framework.TestCase
import uk.co.nickthecoder.glok.property.boilerplate.*

class TestObservableFunctions : TestCase() {

    val aProperty by booleanProperty(true)
    var a by aProperty

    val optionalProperty by optionalBooleanProperty(true)
    var optional by optionalProperty


    override fun setUp() {
        a = true
    }

    fun testToString() {
        val func = aProperty.toObservableString()
        assertEquals( "true", func.value )
        a = false
        assertEquals( "false", func.value )
    }

    fun testIsNull() {
        val func = optionalProperty.isNull()
        assertEquals( false, func.value )
        optional = null
        assertEquals( true, func.value )
    }

    fun testIsNotNull() {
        val func = optionalProperty.isNotNull()
        assertEquals( true, func.value )
        optional = null
        assertEquals( false, func.value )
    }


}

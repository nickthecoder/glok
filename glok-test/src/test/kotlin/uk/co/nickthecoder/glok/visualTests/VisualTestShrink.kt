/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.NodeParent
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * Using a SplitPane to shrink various controls within a HBox smaller than their preferred width.
 * Each HBox was two nodes of the same type.
 *
 * In the top row both have a shrinkPriority of 1, and therefore shrink at the same rate, at the same time.
 *
 * The bottom row the first of the pair has a shrinkPriority of 0, and the second a shrinkPriority of 1.
 * Therefore, as we make the width smaller, the 2nd shrinks till it reaches its minWidth, and then the
 * 1st starts to shrink too, until it also reaches its minWidth.
 * At this point no more shrinking occurs, and the nodes are clipped.
 */
class VisualTestShrink : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "Visual Test Shrink"
            scene(600, 300) {
                root = vBox {
                    test(1f)
                    test(0f)

                }
            }
            show()
        }
    }

    private fun NodeParent.test(shrink: Float = 0f) {
        + splitPane {
            + hBox {
                + spinner(99f) {
                    editor.prefColumnCount = 4
                    shrinkPriority = shrink
                }
                + spinner(99f) {
                    editor.prefColumnCount = 4
                    shrinkPriority = 1f
                }
            }
            + hBox {
                + textField("Hi") {
                    prefColumnCount = 3
                    shrinkPriority = shrink
                }
                + textField("Hi") {
                    prefColumnCount = 3
                    shrinkPriority = 1f
                }
            }

            + hBox {
                + textArea("Hi\nHi\n") {
                    prefColumnCount = 3
                    shrinkPriority = shrink
                }
                + textArea("Hi\nHi\n") {
                    prefColumnCount = 3
                    shrinkPriority = 1f
                }
            }
        }
    }

    companion object {

        fun main(vararg args: String) {
            launch(VisualTestShrink::class.java)
        }
    }
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.collections

import junit.framework.TestCase

class TestObservableMap : TestCase() {

    lateinit var allChanges: MutableList<MapChange<Int, String>>
    lateinit var map: MutableObservableMap<Int, String>

    override fun setUp() {
        allChanges = mutableListOf()
        map = mutableMapOf<Int, String>().asMutableObservableMap()
        map.addChangeListener { _, changes ->
            allChanges.addAll(changes)
        }
    }

    fun testPut() {
        assertTrue(allChanges.isEmpty())

        map[1] = "Hello"
        assertEquals(1, allChanges.size)
        assertFalse(allChanges.first().isRemoval)
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
        assertNull(allChanges.first().oldValue)

        map[3] = "World"
        assertEquals(2, allChanges.size)
        assertFalse(allChanges.last().isRemoval)
        assertEquals(3, allChanges.last().key)
        assertEquals("World", allChanges.last().value)
    }

    fun testReplace() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        map[1] = "Hi"
        assertEquals(1, allChanges.first().key)
        assertEquals("Hi", allChanges.first().value)
        assertEquals("Hello", allChanges.first().oldValue)
    }

    fun testReplaceEntry() {
        val foo = mutableMapOf(1 to "Hello", 3 to "World")
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        println(map.entries.toList())
        map.entries.first { it.key == 1 }.setValue("Hi")
        assertEquals(1, allChanges.first().key)
        assertEquals("Hi", allChanges.first().value)
        assertEquals("Hello", allChanges.first().oldValue)

        assertEquals("Hi", map[1])
    }

    fun testRemove() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        map.remove(1)
        assertTrue(allChanges.first().isRemoval)
        assertEquals(1, allChanges.size)
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
    }

    fun testRemoveEntry() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        val entry = map.entries.firstOrNull { it.key == 1 }
        map.entries.remove(entry)
        assertTrue(allChanges.first().isRemoval)
        assertEquals(1, allChanges.size)
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
    }

    fun testRemoveKey() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        map.keys.remove(1)
        assertTrue(allChanges.first().isRemoval)
        assertEquals(1, allChanges.size)
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
    }

    fun testRemoveKeyIterator() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        val iterator = map.keys.iterator()
        for (key in iterator) {
            if (key == 1) {
                iterator.remove()
            }
        }
        assertTrue(allChanges.first().isRemoval)
        assertEquals(1, allChanges.size)
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
    }

    fun testRemoveEntriesIterator() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        val iterator = map.entries.iterator()
        for (entry in iterator) {
            if (entry.key == 1) {
                iterator.remove()
            }
        }
        assertTrue(allChanges.first().isRemoval)
        assertEquals(1, allChanges.size)
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
    }

    fun testClear() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        map.clear()
        assertEquals(2, allChanges.size)
        assertTrue(allChanges.first().isRemoval)
        assertTrue(allChanges.last().isRemoval)
        // There's no guarantee of the order, so let's sort them
        allChanges.sortBy { it.key }
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
        assertEquals(3, allChanges.last().key)
        assertEquals("World", allChanges.last().value)
    }

    fun testClearEntries() {
        map[1] = "Hello"
        map[3] = "World"
        allChanges.clear()
        map.entries.clear()
        assertEquals(2, allChanges.size)
        assertTrue(allChanges.first().isRemoval)
        assertTrue(allChanges.last().isRemoval)
        // There's no guarantee of the order, so let's sort them
        allChanges.sortBy { it.key }
        assertEquals(1, allChanges.first().key)
        assertEquals("Hello", allChanges.first().value)
        assertEquals(3, allChanges.last().key)
        assertEquals("World", allChanges.last().value)
    }

}

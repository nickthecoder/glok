package uk.co.nickthecoder.glok.visualTests

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * ScrollPane wasn't calculating its minHeight nor minWidth, and always returned 0.
 */
class TestSquashedSplitPane : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {

            title = "Test Squashed Split Pane"

            scene = scene(1000, 800) {
                root = vBox {
                    spacing(30)

                    + splitPane {
                        + vBox {
                            + textArea("Hello") {
                                prefRowCount = 10
                                prefColumnCount = 20
                                overrideMinHeight = 50f
                            }
                            + label("World")
                        }
                        + label("Right")
                    }.withExpandHeight()

                    + splitPane(Orientation.VERTICAL) {
                        + hBox {
                            + textArea("Hello") {
                                prefRowCount = 10
                                prefColumnCount = 20
                                overrideMinWidth = 200f
                            }
                            + label("World")
                        }
                        + label("Below")
                    }.withExpandWidth()
                }
            }

            show()
        }
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            launch(TestSquashedSplitPane::class.java)
        }
    }

}

package uk.co.nickthecoder.glok.scene

import junit.framework.TestCase
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.PartialTexture
import uk.co.nickthecoder.glok.backend.backend

class TestImage : TestCase() {

    override fun setUp() {
        backend = DummyBackend()
    }

    fun testImageFromTexture() {
        val image = backend.createTexture("testImage", 20, 30, IntArray(20 * 30 * 4) { 255 })

        assertEquals(20, image.width)
        assertEquals(20f, image.imageWidth)
        assertEquals(30, image.height)
        assertEquals(30f, image.imageHeight)

        val subImage = image.partialImage(1f, 2f, 10f, 20f)
        val subTexture = subImage as PartialTexture
        assertEquals(10f, subTexture.imageWidth)
        assertEquals(20f, subTexture.imageHeight)

        assertEquals(1f, subTexture.srcX)
        assertEquals(2f, subTexture.srcY)

        val innerImage = subImage.partialImage(2f, 3f, 2f, 1f)
        val innerTexture = innerImage as PartialTexture
        assertEquals(3f, innerTexture.srcX)
        assertEquals(5f, innerTexture.srcY)
        assertEquals(2f, innerTexture.imageWidth)
        assertEquals(1f, innerTexture.imageHeight)

    }

}

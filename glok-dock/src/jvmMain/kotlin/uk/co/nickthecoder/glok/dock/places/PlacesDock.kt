package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.places.Place
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.util.openFileInExternalApp
import java.io.File

/**
 * A tree view of folders and files.
 *
 * The root of the tree can be any folder within the list : [places], and initially, the first [Place] in the list
 * is used as the root.
 *
 * The dock can be used to open files within your application, instead of the more traditional `File->Open` dialog.
 * To do so, override [openFile].
 *
 * The default value of [places] in the constructor are taken from [Places.sharedPlaces],
 * which means these favourite places are shared by all Glok applications.
 * If you prefer, your application can keep its own list of favourite places, which can be loaded and saved
 * using [Places.loadPlaces] and [Places.savePlaces].
 */
open class PlacesDock(

    harbour: Harbour,

    /**
     * A list of "favourite" places. Any item in this list can be the `root` node of the tree.
     */
    val places: MutableObservableList<Place> = Places.sharedPlaces,

    /**
     * Default value is [ID] ("places").
     */
    id: String = ID

) : Dock(id, harbour) {

    /**
     * The current favourite [Place] selected from [places].
     * Changing this will change the `root` of the tree.
     */
    val currentPlaceProperty by simpleProperty<Place?>(null)
    var currentPlace by currentPlaceProperty

    /**
     * Allows an application to use its own dialogs for renaming, copying and deleting files.
     * In most cases the default value ([StandardFileDialogFactory]) is sufficient.
     */
    val fileDialogFactoryProperty by simpleProperty<FileDialogFactory>(StandardFileDialogFactory)
    var fileDialogFactory by fileDialogFactoryProperty

    /**
     * Filters which files to display in the PlacesDock.
     * The default value is [EXCLUDE_HIDDEN_FILES].
     *
     * Note. Filters should NOT check [File.isFile]
     */
    val fileFilterProperty: Property<(File) -> Boolean> by simpleProperty(EXCLUDE_HIDDEN_FILES)
    var fileFilter: (File) -> Boolean by fileFilterProperty

    /**
     * Filters which folders to display in the PlacesDock.
     * The default value is [EXCLUDE_HIDDEN_FILES].
     *
     * Note. Filters should NOT check [File.isDirectory]
     */
    val folderFilterProperty: Property<(File) -> Boolean> by simpleProperty(EXCLUDE_HIDDEN_FILES)
    var folderFilter: (File) -> Boolean by folderFilterProperty


    private val watcher = object : FileWatcher() {

        override fun created(file: File) {
            findTreeItem(file.parentFile)?.let { parentItem ->
                addTreeItem(parentItem, file.name)
            }
        }

        override fun deleted(file: File) {
            findTreeItem(file)?.let { toDelete ->
                // An easy way to cancel the watcher if this is a folder.
                toDelete.expanded = false
                toDelete.parent?.children?.remove(toDelete)
            }
        }
    }

    protected val treeView = createFilesTreeView()

    private val commands = Commands().apply {
        attachTo(this@PlacesDock)

        with(PlacesDockActions) {
            EDIT_PLACES { editPlaces() }
        }
    }

    init {
        title = "Places"
        icons = Tantalum.icons
        iconName = "folder_tinted"

        allowedSides.remove(Side.TOP)
        allowedSides.remove(Side.BOTTOM)

        watcher.clear()

        with(titleButtons) {
            commands.build(harbour.iconSizeProperty) {
                with(PlacesDockActions) {
                    add(button(EDIT_PLACES))
                }
            }
        }

        content = vBox {
            fillWidth = true

            + choiceBox(places) {
                menuItemFactory = { createMenuItem(it).apply { tooltip = TextTooltip(it.file.path) } }
                converter = { place: Place? -> place?.alias ?: "<None>" }
                selection.selectedItemProperty.addChangeListener { _, _, place ->
                    if (place != null) {
                        setRoot(place.file)
                    }
                }
                selection.selectedItem = places.firstOrNull()
                currentPlaceProperty.bidirectionalBind(selection.selectedItemProperty)
                tooltipProperty.bindTo(UnaryFunction(selection.selectedItemProperty) {
                    TextTooltip(it?.file?.path ?: "<None>")
                })
            }

            + treeView.apply {
                growPriority = 1f
            }

        }

        // Refresh the list when needed.
        for (prop in listOf(fileFilterProperty, folderFilterProperty)) {
            prop.addListener {
                treeView.root?.value?.let { rootFolder ->
                    setRoot(rootFolder)
                }
            }
        }
    }

    private fun setRoot(folder: File) {
        watcher.clear()
        treeView.root = createTreeItem(folder, expand = true)
        treeView.scrollTo(0)
    }

    protected fun findTreeItem(file: File): TreeItem<File>? {
        fun findTreeItemFrom(from: TreeItem<File>): TreeItem<File>? {
            if (from.value == file) return from
            for (child in from.children) {
                if (file.path.startsWith(child.value.path)) {
                    return findTreeItemFrom(child)
                }
            }
            return null
        }

        val root = treeView.root
        return if (root == null) null else findTreeItemFrom(root)
    }

    private fun addTreeItem(parent: TreeItem<File>, filename: String) {

        val file = File(parent.value, filename)
        val isFile = file.isFile
        if ((isFile && fileFilter(file)) || (! isFile && folderFilter(file))) {

            var index = 0
            for (child in parent.children) {
                if (! isFile && child.value.isFile) break
                if (isFile && child.value.isDirectory) {
                    // Skip over all directories (which are before all the files)
                    index ++
                    continue
                }
                if (child.value.name > filename) break
                index ++
            }
            parent.children.add(index, createTreeItem(file))
        }
    }

    /**
     * Expands tree items, so that [file] is visible in the tree, and then selects it.
     *
     * If [file] is not a descendant of the root node, nothing happens. No items are expanded,
     * and the selected item isn't changed.
     *
     * Consider adding a "select opened file" button in [titleButtons], (using [Tantalum.scaledIcons] "sync_once" )
     * which calls this with the currently opened file in your application.
     */
    fun selectFile(file: File) {
        val path = file.path
        fun select(item: TreeItem<File>) {
            val matching = item.children.firstOrNull { path.startsWith(it.value.path) }
            if (matching == null) {
                treeView.selection.selectedItem = item
                // If items weren't expanded, the scrolling didn't work. Not tested with runLater ????
                Platform.runLater { treeView.scrollTo(item) }
            } else {
                matching.expanded = true
                select(matching)
            }
        }
        treeView.root?.let { select(it) }
    }

    protected fun createTreeItem(file: File, expand: Boolean = false): TreeItem<File> {

        return if (file.isDirectory) {

            TreeItem(file).apply {
                leaf = false
                expandedProperty.addChangeListener { _, _, expanded ->

                    if (expanded) {

                        val folderContents = value.listFiles() ?: emptyArray()
                        val childFolders = folderContents.filter { it.isDirectory && folderFilter(it) }
                            .sortedBy { it.name.uppercase() }
                        val childFiles = folderContents.filter { it.isFile && fileFilter(it) }
                            .sortedBy { it.name.uppercase() }

                        for (subFolder in childFolders) {
                            children.add(createTreeItem(subFolder))
                        }
                        for (childFile in childFiles) {
                            children.add(createTreeItem(childFile))
                        }
                        watcher.watch(file)
                    } else {
                        children.clear()
                        watcher.unwatch(file)
                    }
                }

                expanded = expand
            }

        } else {
            TreeItem(file).apply {
                leaf = true
            }
        }
    }

    protected open fun openFile(file: File) {
        openFileInExternalApp(file)
    }

    protected open fun editPlaces() {
        object : Dialog() {
            init {
                title = "Edit Places"
                content = Places.editPlacesNode(places).apply {
                    overridePrefWidth = 800f
                    overridePrefHeight = 500f
                }
                buttonBar.visible = false
            }

        }.createStage(scene !!.stage !!).show()
    }

    /**
     * Creates a popup menu for files (not folders).
     *
     * The default implementation has these menu items :
     * * Copy
     * * Rename
     * * Delete
     */
    protected open fun createFilePopupMenu(item: TreeItem<File>): PopupMenu? {
        return popupMenu {
            + menuItem("Copy …") {
                onAction {
                    fileDialogFactory.copyDialog(item.value).createStage(scene !!.stage !!).show()
                }
            }

            + menuItem("Rename …") {
                onAction {
                    fileDialogFactory.renameDialog(item.value).createStage(scene !!.stage !!).show()
                }
            }
            + menuItem("Delete …") {
                onAction {
                    fileDialogFactory.deleteDialog(item.value).createStage(scene !!.stage !!).show()
                }
            }
        }
    }

    /**
     * Creates a popup menu for folders (not files).
     *
     * The default implementation has these menu items :
     *
     * * Rename folder
     * * separator
     * * Browse (opens the folder in a File Explorer)
     * * Terminal Here
     * * separator
     * * Remove from Favourites (if the folder is in [places]).
     * * Add to Favourites (if the folder is not in [places]).
     */
    protected open fun createFolderPopupMenu(item: TreeItem<File>): PopupMenu? {
        return popupMenu {

            + menuItem("Rename …") {
                onAction {
                    fileDialogFactory.renameDialog(item.value).createStage(scene !!.stage !!).show()
                }
            }

            + Separator()

            fileDialogFactory.browseMenuItem(item.value)?.let { + it }
            fileDialogFactory.terminalHereMenuItem(item.value)?.let { + it }

            + Separator()

            if (places.any { it.file == item.value }) {
                + menuItem("Remove from Favourites") {
                    onAction {
                        Places.reloadSharedPreferences()
                        // IntelliJ moans when I use removeIf {...}
                        places.removeAll(places.filter { it.file == item.value })
                    }
                }
            } else {
                + menuItem("Add to Favourites") {
                    onAction {
                        Places.reloadSharedPreferences()
                        places.add(Place(item.value))
                    }
                }
            }

        }
    }

    private fun createFilesTreeView() = TreeView<File>().apply {

        cellFactory = { tree, item ->
            TextTreeCell(tree, item, item.value.name).apply {
                tooltip = TextTooltip(item.value.path)

                onMouseClicked { event ->
                    if (event.clickCount == 2 && event.isPrimary) {
                        openFile(item.value)
                    }
                }
                onPopupTrigger { event ->
                    if (item.value.isFile) {
                        createFilePopupMenu(item)?.show(event.sceneX, event.sceneY, scene !!.stage !!)
                    }

                    if (item.value.isDirectory) {
                        createFolderPopupMenu(item)?.show(event.sceneX, event.sceneY, scene !!.stage !!)
                    }
                }
            }
        }

        rootProperty.addChangeListener { _, _, new ->
            if (new != null) {
                if (new.value.isDirectory) {
                    new.expanded = true
                }
            }
        }
    }

    companion object {
        const val ID = "places"

        val ALL_FILES: (File) -> Boolean = { true }
        val EXCLUDE_HIDDEN_FILES: (File) -> Boolean = { ! it.isHidden }
    }
}

object PlacesDockActions : Actions(Tantalum.icons) {

    val EDIT_PLACES = define("edit_places", "Edit Places", null, "Edit Places") {
        iconName = "settings_tinted"
        tinted = true
    }

}

package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.textField
import java.io.File

open class DeleteFileDialog(
    originalFile: File
) : FileOperationDialog("Delete", originalFile) {

    override val okDisabledProperty by booleanProperty(false)

    var onDeleted: ((File) -> Unit)? = null

    init {
        title = "Delete File"
        content = buildContent()
    }

    override fun top(form: FormGrid) {
        with(form) {
            + row("Name") {
                right = textField(originalFile.name) {
                    readOnly = true
                }
            }

            + row("Folder") {
                right = textField(originalFile.parent) {
                    readOnly = true
                    growPriority = 1f
                    prefColumnCount = 30
                    caretIndex = text.length
                    anchorIndex = caretIndex
                }
            }
        }
    }

    override fun run(): Boolean {
        try {
            originalFile.delete()
            try {
                onDeleted?.invoke(originalFile)
            } catch (e: Exception) {
                // Do Nothing.
            }
        } catch (e: Exception) {
            message = "Delete failed (${e.javaClass.simpleName})"
            return false
        }

        return true
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.application.Platform
import java.io.File
import java.nio.file.*
import kotlin.io.path.name

/**
 * A helper class to simplify [WatchService], by creating a (private) background thread,
 * which polls the [WatchService], and forwards the messages to [created] and [deleted]
 * on the glok thread.
 *
 * Designed to work with [PlacesDock], so that the view remains current when files are created/deleted
 * for any expanded folders in the TreeView.
 *
 * ## Typical Usage
 *
 * Create a concrete subclass, implementing [created] and [deleted].
 * Watch folders using [watch] and later stop watching it using [unwatch].
 *
 * [close] ends the background thread, which polls for changes.
 */
abstract class FileWatcher {

    private val watchService = FileSystems.getDefault().newWatchService()
    private val watchKeysMap = mutableMapOf<File, WatchKey>()

    private var open = true

    private var errorReported = false
    private val thread = Thread {
        while (open) {
            val key = try {
                watchService.take()
            } catch (e: Exception) {
                Thread.sleep(1000L)
                if (! errorReported) {
                    // TODO log.error("Watcher.take failed : $e")
                    errorReported = true
                }
                null
            }
            if (open && key != null) {
                val folder = (key.watchable() as Path).toFile()
                for (event in key.pollEvents()) {
                    Platform.runLater {
                        val filename = (event.context() as Path).name
                        val file = File(folder, filename)
                        if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                            created(file)
                        } else if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
                            deleted(file)
                        }

                    }
                }
                key.reset()
            }
        }
    }.apply {
        name = FileWatcher::class.java.simpleName
        isDaemon = true
        start()
    }

    /**
     * Called on the glok thread when a file is created within a watched folder.
     */
    abstract fun created(file: File)

    /**
     * Called on the glok thread when a file is deleted from a watched folder.
     */
    abstract fun deleted(file: File)

    /**
     * Stops watching all folders.
     */
    fun clear() {
        for (watchKey in watchKeysMap.values) {
            watchKey.cancel()
        }
        watchKeysMap.clear()
    }

    fun watch(folder: File) {
        if (watchKeysMap[folder] == null) {
            val path = Paths.get(folder.path)
            watchKeysMap[folder] = path.register(
                watchService,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE
            )
        }
    }

    fun unwatch(folder: File) {
        watchKeysMap[folder]?.cancel()
        watchKeysMap.remove(folder)
    }

    fun close() {
        open = false
    }
}

package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.control.MenuItem
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.glok.util.openFileInExternalApp
import uk.co.nickthecoder.glok.util.openFolderInTerminal
import java.io.File

/**
 * The [PlacesDock] is designed to be customisable, and an application may have special requirements
 * when renaming, deleting, copying and opening files.
 *
 * ## Some reasons for creating your own dialogs :
 *
 * * If a file has associated files (such as a thumbnail image of the file), then
 * the `delete` dialog may give the option to delete these associated files too.
 *
 * * The default [CopyFileDialog] and [RenameFileDialog] do NOT allow the file extension to be changed.
 *
 * * The default [DeleteFileDialog] deletes the file permanently. It doesn't use the operating system's
 * `recycle bin`.
 *
 * Create a new implementation of [FileDialogFactory], and use it with [PlacesDock.fileDialogFactory].
 */
interface FileDialogFactory {

    fun copyDialog(file: File): Dialog = CopyFileDialog(file)
    fun renameDialog(file: File): Dialog = RenameFileDialog(file)
    fun deleteDialog(file: File): Dialog = DeleteFileDialog(file)

    fun browseMenuItem(file: File): MenuItem? {
        val directory: File = if (file.isDirectory) {
            file
        } else {
            file.absoluteFile.parentFile
        }
        return menuItem("Browse …") {
            id = "browse"
            style(TINTED)
            graphic = ImageView(Tantalum.scaledIcons["folder_tinted"])
            onAction { openFileInExternalApp(directory) }
        }
    }

    fun terminalHereMenuItem(directory: File): MenuItem? {
        return menuItem("Open Terminal Here") {
            id = "terminalHere"
            style(TINTED)
            graphic = ImageView(Tantalum.scaledIcons["terminal_tinted"])
            onAction { openFolderInTerminal(directory) }
        }
    }
}

object StandardFileDialogFactory : FileDialogFactory

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.util.GlokException
import java.util.prefs.Preferences

/**
 * Saves the layout of the Harbour to Java [Preferences] (aka the registry on Windows).
 */
fun Harbour.save(preferences: Preferences) {

    preferences.node("harbour").removeNode()

    val pHarbour = preferences.node("harbour")
    val pDocks = pHarbour.node("docks")

    for (side in Side.values()) {
        pHarbour.putFloat("divider_${side.name}", getDividerPosition(side))
        pHarbour.putFloat("split_${side.name}", getSplitPosition(side))
    }

    for (dock in docks) {
        val pDock = pDocks.node(dock.dockID)

        pDock.put("side", dock.side.name)
        pDock.putBoolean("isPrimary", dock.primary)
        pDock.putBoolean("visible", dock.visible)
        pDock.putInt("order", dock.order())
    }

    pHarbour.flush()

}

/**
 * Load the layout of the Harbour from [Preferences] (aka the registry on Windows).
 *
 * The [Harbour] must have a [DockFactory], which creates [Dock]s based on a `dockID`.
 *
 * If settings for an unknown dockID are found, they are silently ignored.
 * e.g. If you have renamed a dockID, or if a dock has been removed from your application.
 *
 * @return true iff preferences were found. Consider adding docks in default positions if the result is false.
 */
fun Harbour.load(preferences: Preferences): Boolean {

    if (!preferences.nodeExists("harbour")) {
        return false
    }

    val pHarbour = preferences.node("harbour")

    for (side in Side.values()) {
        val defaultPosition = if (side == Side.LEFT || side == Side.TOP) 0.25f else 0.75f
        setDividerPosition(side, pHarbour.getFloat("divider_${side.name}", defaultPosition))
        setSplitPosition(side, pHarbour.getFloat("split_${side.name}", 0.5f))
    }

    val pDocks = pHarbour.node("docks")
    val childrenNames = pDocks.childrenNames().sortedBy {
        val pChild = pDocks.node(it)
        pChild.getInt("order", Int.MAX_VALUE)
    }

    for (dockID in childrenNames) {
        val dock = try {
            dockFactory?.createDock(this, dockID)
        } catch (e: GlokException) {
            continue
        }
        if (dock != null) {
            val pDock = pDocks.node(dockID)
            val sideStr = pDock.get("side", Side.LEFT.name)
            val isPrimary = pDock.getBoolean("isPrimary", true)
            val visible = pDock.getBoolean("visible", false)

            val side = try {
                Side.valueOf(sideStr)
            } catch (e: Exception) {
                Side.LEFT
            }

            add(dock, side, isPrimary)
            dock.visible = visible
        }
    }
    return true
}

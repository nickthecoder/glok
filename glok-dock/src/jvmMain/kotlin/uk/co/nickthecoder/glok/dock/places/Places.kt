package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.control.ListView
import uk.co.nickthecoder.glok.control.SingleNodeListCell
import uk.co.nickthecoder.glok.dialog.promptDialog
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.places.Place
import uk.co.nickthecoder.glok.property.asNullable
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import java.io.File
import java.util.prefs.NodeChangeEvent
import java.util.prefs.NodeChangeListener
import java.util.prefs.Preferences

object Places {

    /**
     * If multiple preference nodes are changed in rapid succession, then
     * this lets us reload preferences only once.
     */
    private var reloadPending = false

    /**
     * If multiple changes to [sharedPlaces] happen within one "frame",
     * then this lets us save the preferences only once.
     */
    private var savePending = false

    /**
     * A list of [Place]s, which are shared across all Glok applications.
     * The data is stored in Java preferences.
     * If another application alters these preferences, the list is also updated
     * (by reloading from Java preferences again).
     * Whenever the list changes, the Java preferences are saved.
     *
     * The net result is that multiple Glok applications can share the same list of favourite [Place]s,
     * and any changes in one application is also reflected in the other applications.
     *
     * ## Warning
     * Java [Preferences] do NOT guarantee that node change fire when the node
     * is changed from outside the current JVM. Therefore, syncing between applications cannot be guaranteed.
     * I suspect that this feature is only implemented in Windows, but the documentation doesn't elaborate.
     * Therefore, consider calling [reloadSharedPreferences] before adding/removing places.
     * This will prevent changes in another application to be lost when favourite places are changed
     * in your application.
     *
     * Ideally, Glok would be able to use the "favourite" places supplied by your operating system,
     * but I'm not aware of a cross-platform Java library which supports this.
     * If you require this feature, you'll need to implement this yourself.
     */
    val sharedPlaces: MutableObservableList<Place> by lazy {
        val places = mutableListOf<Place>().asMutableObservableList()
        val preferences = sharedPlacesPreferences
        loadPlaces(places, preferences)
        if (places.isEmpty()) {
            places.add(Place(File(System.getProperty("user.home")), "Home"))
        }

        places.addChangeListener { _, _ ->
            //log.debug("Shared places list changed $savePending $reloadPending")
            if (! savePending && ! reloadPending) {
                savePending = true
                Platform.runLater {
                    //log.debug("Later save if $savePending")
                    if (savePending) {
                        try {
                            //log.debug("Saving shared places")
                            savePlaces(places, preferences)
                        } finally {
                            savePending = false
                        }
                    }
                }
            }
        }

        places
    }

    val sharedPlacesPreferences: Preferences by lazy {
        Preferences.userNodeForPackage(Place::class.java).apply {
            addNodeChangeListener(object : NodeChangeListener {

                private fun nodeChanged() {
                    //log.debug("Place Node changed $savePending $reloadPending")
                    if (savePending || reloadPending) return
                    reloadPending = true
                    Platform.runLater {
                        //log.debug("Later reload if $reloadPending")
                        if (reloadPending) {
                            try {
                                //log.debug("Reloading shared places")
                                loadPlaces(sharedPlaces, sharedPlacesPreferences)
                            } finally {
                                reloadPending = false
                            }
                        }
                    }
                }

                override fun childAdded(evt: NodeChangeEvent?) = nodeChanged()
                override fun childRemoved(evt: NodeChangeEvent?) = nodeChanged()
            })
        }
    }

    fun reloadSharedPreferences() {
        //log.debug("Reloading shared preferences")
        loadPlaces(sharedPlaces, sharedPlacesPreferences)
    }

    fun loadPlaces(placesList: MutableObservableList<Place>, preferences: Preferences) {
        preferences.sync()
        val previousSize = placesList.size
        preferences.childrenNames().mapNotNull { it.toIntOrNull() }.sorted().forEach {
            val subNode = preferences.node(it.toString())
            subNode.sync()

            val filename = subNode.get("file", null)
            if (filename != null) {
                val file = File(filename)
                val alias = subNode.get("alias", file.name)

                val place = Place(file, alias)
                placesList.add(place)
            }
        }
        placesList.removeBetween(0 until previousSize)
    }

    fun savePlaces(placesList: ObservableList<Place>, preferences: Preferences) {
        // We do NOT use preference.clear(), because that will delete the node
        // which is being listened to by [sharedPlaces], and therefore the listener will stop working.
        for (childName in preferences.childrenNames()) {
            val childNode = preferences.node(childName)
            childNode.clear()
        }

        for ((index, place) in placesList.withIndex()) {
            val subNode = preferences.node(index.toString())
            subNode.put("file", place.file.path)
            subNode.put("alias", place.alias)
            subNode.flush()
        }
        preferences.flush()
    }

    /**
     * Lets the use edit a list of [Place]s.
     *
     */
    fun editPlacesNode(places: MutableObservableList<Place>): Node {
        val listView = listView(places) {
            growPriority = 1f
            canReorder = true

            cellFactory = { listView, item ->
                val path = label(item.file.path).apply {
                    ellipsisAlignment = HAlignment.CENTER
                }
                val result = SingleNodeListCell(listView, item, hBox {
                    padding(6)
                    + label(item.alias) { overridePrefWidth = 200f }
                    + path
                }).apply {
                    onPopupTrigger { event ->
                        placePopupMenu(event, listView, item)
                    }
                }
                // If the path is long, this forces the ellipsis to appear.
                // Without this, path is wider than the viewport, and the node is clipped (without an ellipsis appearing)
                path.overridePrefWidthProperty.bindTo((result.widthProperty - 220f).asNullable())

                result
            }
        }

        val commands = Commands().apply {

            with(PlacesActions) {
                ADD_FOLDER {
                    FileDialog(false).apply {
                        title = "Choose Favourite Place"
                        showFolderPicker(listView.scene !!.stage !!) { file ->
                            reloadSharedPreferences()
                            if (file != null && file.isDirectory) {
                                listView.items.add(Place(file))
                                listView.selection.selectedIndex = listView.items.size - 1
                            }
                        }
                    }
                }
                REMOVE {
                    listView.selection.selectedItem?.let { item ->
                        reloadSharedPreferences()
                        listView.items.remove(item)
                    }
                }.disable(listView.selection.selectedItemProperty.isNull())
            }
        }

        return vBox {
            fillWidth = true
            + toolBar {
                commands.build(Tantalum.iconSizeProperty) {
                    with(PlacesActions) {
                        + button(ADD_FOLDER)
                        + button(REMOVE)
                    }
                }
            }
            + listView
        }
    }

    fun editPlacesForm(places: MutableObservableList<Place>) = borderPane {
        center = editPlacesNode(places)
        bottom = formGrid {

            + row("Terminal Command") {
                right = textField { textProperty.bidirectionalBind(GlokSettings.terminalCommandProperty) }

            }

        }
    }

    private fun placePopupMenu(event: MouseEvent, listView: ListView<Place>, item: Place) {
        val scene = listView.scene !!
        val stage = scene.stage !!
        val index = listView.items.indexOf(item)
        if (index < 0) return
        popupMenu {
            + menuItem("Place alias…") {
                onAction {
                    promptDialog {
                        title = "Place Alias"
                        above = "Alias for : ${item.file}"
                        text = item.alias
                    }.show(stage) { result ->
                        listView.items[index] = Place(item.file, result.ifEmpty { item.file.name })
                    }
                }
            }
        }.show(event.sceneX, event.sceneY, stage)

    }

}

object PlacesActions : Actions(Tantalum.icons) {

    val ADD_FOLDER = define("add_folder", "Add Folder") {
        iconName = "add"
    }
    val REMOVE = define("remove", "Remove") {
        iconName = "remove"
    }

}

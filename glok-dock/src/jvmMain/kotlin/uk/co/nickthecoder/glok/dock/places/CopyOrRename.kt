package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.control.addFileCompletionActions
import uk.co.nickthecoder.glok.control.withFolderButton
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.StringToFile
import uk.co.nickthecoder.glok.property.functions.isDirectory
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.functions.or
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.dsl.hBox
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.textField
import java.io.File

abstract class CopyOrRename(
    operation: String,
    file: File
) : FileOperationDialog(operation, file) {

    protected val toDirectoryProperty by fileProperty(file.absoluteFile.parentFile)
    protected var toDirectory by toDirectoryProperty

    protected val newNameProperty by stringProperty(file.nameWithoutExtension)
    protected var newName by newNameProperty

    protected val newFileProperty = FileBinaryFunction(toDirectoryProperty, newNameProperty) { dir, name ->
        if (originalFile.extension.isBlank()) {
            File(dir, name)
        } else {
            File(dir, "$name.${originalFile.extension}")
        }
    }
    val newFile by newFileProperty

    override val okDisabledProperty = (! toDirectoryProperty.isDirectory()) or newFileProperty.equalTo(originalFile)

    override fun top(form: FormGrid) {
        with(form) {
            + row("Name") {
                right = hBox {
                    alignment = Alignment.CENTER_LEFT
                    + textField {
                        textProperty.bidirectionalBind(newNameProperty)
                    }
                    + label(".${originalFile.extension}") {
                        visible = originalFile.extension.isNotBlank()
                    }
                }
            }

            + row("Folder") {
                right = textField {
                    growPriority = 1f
                    prefColumnCount = 30
                    textProperty.bidirectionalBind(toDirectoryProperty, StringToFile)
                    errors = label("Folder not found") {
                        visibleProperty.bindTo(textProperty.isDirectory())
                    }
                    caretIndex = text.length
                    anchorIndex = caretIndex
                    addFileCompletionActions()
                }.withFolderButton("$operation to Folder")
            }
        }
    }

    abstract fun fileOperation(sourceFile: File, destFile: File)


    /**
     * Renames the file, and if it is a .foocad file, then also rename other associated files,
     * such as the .md file, the -notes.md file, .png images and .custom files.
     */
    override fun run(): Boolean {
        try {
            fileOperation(originalFile, newFile)
        } catch (e: Exception) {
            message = e.message ?: "Rename failed (${e.javaClass.simpleName})"
            return false
        }
        stage.close()
        return true
    }

}

private fun StringProperty.isDirectory(): ObservableBoolean = BooleanUnaryFunction(this) { File(it).isDirectory }

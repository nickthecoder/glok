package uk.co.nickthecoder.glok.dock.places

import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.formGrid
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.row
import java.io.File

/**
 * The base class for [CopyFileDialog], [RenameFileDialog] and [DeleteFileDialog].
 */
abstract class FileOperationDialog(val operation: String, file: File) : Dialog() {

    /**
     * If the operation fails, this message will be displayed, and the dialog remains open.
     */
    val messageProperty by stringProperty("")
    var message by messageProperty

    /**
     * For [RenameFileDialog] and [CopyFileDialog], the OK button is only enabled when the new name is different
     * or the new folder is different, and the new folder exists.
     * For [DeleteFileDialog], this is always false (i.e. the OK button is always enabled).
     */
    abstract val okDisabledProperty: ObservableBoolean

    protected val originalFile: File = file.absoluteFile

    protected abstract fun run(): Boolean

    protected fun buildContent() = formGrid {

        top(this)
        center(this)
        bottom(this)

        with(buttonBar) {
            add(ButtonMeaning.CANCEL) {
                button("Cancel") {
                    cancelButton = true
                    onAction {
                        stage.close()
                    }
                }
            }
            add(ButtonMeaning.OK) {
                button("OK") {
                    defaultButton = true
                    disabledProperty.bindTo(okDisabledProperty)
                    onAction {
                        if (run()) {
                            stage.close()
                        }
                    }
                }
            }
        }

    }

    /**
     * The main content
     */
    abstract fun top(form: FormGrid)

    /**
     * Optional extra content (usually blank)
     */
    open fun center(form: FormGrid) {}

    /**
     * Adds a label containing [message].
     */
    open fun bottom(form: FormGrid) {
        with(form) {
            + row {
                below = label("") {
                    textProperty.bindTo(messageProperty)
                }
            }
        }
    }
}

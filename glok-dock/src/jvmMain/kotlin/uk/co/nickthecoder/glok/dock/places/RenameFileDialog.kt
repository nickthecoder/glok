package uk.co.nickthecoder.glok.dock.places

import java.io.File

/**
 * The default dialog used to rename files from [PlacesDock].
 * See [FileDialogFactory] to use an alternate dialog.
 *
 * This implementation does NOT allow the user to change the file extension.
 */
open class RenameFileDialog(file: File) : CopyOrRename("Rename", file) {

    init {
        title = if (file.isDirectory) "Rename Folder" else "Rename File"
        content = buildContent()
    }

    var onRenamed: ((File, File) -> Unit)? = null

    override fun fileOperation(sourceFile: File, destFile: File) {
        sourceFile.renameTo(destFile)
    }

    override fun run(): Boolean {
        return if (super.run()) {
            try {
                onRenamed?.invoke(originalFile, newFile)
            } catch (e: Exception) {
                // Do nothing
            }
            true
        } else {
            false
        }
    }

}

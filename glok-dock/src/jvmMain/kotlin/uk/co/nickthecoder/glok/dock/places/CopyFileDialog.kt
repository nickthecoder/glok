package uk.co.nickthecoder.glok.dock.places

import java.io.File

/**
 * The default dialog used to copy files from [PlacesDock].
 * See [FileDialogFactory] to use an alternate dialog.
 *
 * This implementation does NOT allow the user to choose a different file extension.
 */
open class CopyFileDialog(file: File) : CopyOrRename("Copy", file) {

    var onCopied: ((File, File) -> Unit)? = null

    init {
        title = "Copy File"
        content = buildContent()
    }

    override fun fileOperation(sourceFile: File, destFile: File) {
        sourceFile.copyTo(destFile)
    }

    override fun run(): Boolean {
        return if (super.run()) {
            try {
                onCopied?.invoke(originalFile, newFile)
            } catch (e: Exception) {
                // Do nothing
            }
            true
        } else {
            false
        }
    }

}

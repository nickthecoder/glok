/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

import uk.co.nickthecoder.glok.util.GlokException

/**
A [DockFactory], implemented by a simple [Map].

This only works when all [Dock]s are created at application start-up.
It cannot create docks on-demand.

Docks would be created, and stay in memory for the entire lifetime of the [Harbour],
even if they are never used.

On the plus side, toggling the visibility of docks (e.g. via `ToggleMenuItem`s)
is easy : Bidirectionally bind `ToggleMenuItem.selectedProperty` and [Dock.visibleProperty].
 */
class MapDockFactory(vararg docks: Dock) : DockFactory {

    private val dockMap = mutableMapOf<String, Dock>()

    init {
        for (dock in docks) {
            add(dock)
        }
    }

    fun <T : Dock> add(dock: T): T {
        dockMap[dock.dockID] = dock
        return dock
    }

    override fun createDock(harbour: Harbour, dockID: String): Dock {
        return dockMap[dockID] ?: throw GlokException("Dock $dockID not found")
    }

    override fun toString() = dockMap.toString()
}

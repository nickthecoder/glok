/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

/**
[Harbour] can load/save the position and visibility of docks.
We therefore need a way to create a dock from a unique identifier.

The identifier is [Dock.dockID], which is a `String`.

If you wish to create docks on-demand :

 *    class MyDockFactory(val harbour: Harbour) : DockFactory {
 *        override fun createDock(harbour : Harbour, dockID: String) = harbour.findDock(dockID) ?: when (dockID) {
 *            "abc" -> AbcDock(harbour)
 *            "xyz" -> XyzDock(harbour)
 *            else -> throw Exception("Unknown '$dockID'")
 *        }
 *    }

However, if you prefer to create all docks at the same time as that the [Harbour] is created, consider using [MapDockFactory].
 */
interface DockFactory {
    fun createDock(harbour: Harbour, dockID: String): Dock?

    /**
     * An optional method, which gives you the opportunity to tidy up (e.g. release resources) when a dock is closed.
     */
    fun dockClosed(harbour: Harbour, dock: Dock) {}
}

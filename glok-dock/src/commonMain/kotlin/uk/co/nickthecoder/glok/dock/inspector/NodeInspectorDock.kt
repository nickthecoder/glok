/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock.inspector

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.control.MenuButton
import uk.co.nickthecoder.glok.control.NodeInspector
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.TextListCell
import uk.co.nickthecoder.glok.dialog.dialog
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.event.EventHandler
import uk.co.nickthecoder.glok.event.EventType
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.dsl.listView
import uk.co.nickthecoder.glok.scene.dsl.padding
import uk.co.nickthecoder.glok.scene.dsl.radioChoices
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * A [Dock] containing a [NodeInspector] (in the Glok core project) which displays a TreeView of nodes.
 * Click on an item in the tree to see details of that node in the 'Details Panel' below the TreeView.
 *
 *
 */
open class NodeInspectorDock(harbour: Harbour) : Dock(ID, harbour) {

    val rootNodeProperty by optionalNodeProperty(null)
    var rootNode by rootNodeProperty

    val nodeInspector = NodeInspector().apply {
        rootNodeProperty.bindTo(this@NodeInspectorDock.rootNodeProperty)
        iconSizeProperty.bindTo(harbour.iconSizeProperty)
    }

    val showInvisibleNodesProperty by booleanProperty(false)
    var showInvisibleNodes by showInvisibleNodesProperty


    private val commands = Commands().apply {
        attachTo(this@NodeInspectorDock)

        with(NodeInspectorActions) {
            REFRESH { refresh() }
            toggle(TOGGLE_INVISIBLE_NODES, showInvisibleNodesProperty)
            NODE_PICKER { addNodePickerFilter() }
            CHOOSE_NODE { }
        }
    }

    init {
        title = "Node Inspector"
        content = nodeInspector
        icons = Tantalum.icons
        iconName = "root"
        allowedSides.removeAll(Side.TOP, Side.BOTTOM)

        nodeInspector.nodeFilterProperty.bindTo(UnaryFunction(showInvisibleNodesProperty) { show ->
            if (show) {
                { true }
            } else {
                { node: Node -> node.visible }
            }
        })

        // Add buttons to the title toolbar.
        commands.build(harbour.iconSizeProperty) {
            with(NodeInspectorActions) {
                titleButtons.addAll(
                    button(REFRESH),
                    toggleButton(TOGGLE_INVISIBLE_NODES),
                    button(NODE_PICKER),
                    menuButton(CHOOSE_NODE) { onShowing { populateNodeMenu() } }
                )
            }
        }
    }

    private fun MenuButton.populateNodeMenu() {
        items.clear()

        // We will be adding PropertyRadioMenuItems, with a tick mark next to the one
        // that is currently the root of the TreeView.
        radioChoices(rootNodeProperty) {

            // List all RegularStages, and OverlayStages
            val allStages = Application.instance.stages
            for (stage in allStages) {
                stage.scene?.root?.let { propertyRadioMenuItem(it, "Stage : ${stage.title}") }
                for (overlay in stage.overlayStages) {
                    if (overlay.stageType != StageType.POPUP_MENU) {
                        overlay.scene?.root?.let {
                            + propertyRadioMenuItem(it, "Overlay : ${overlay.title}")
                        }
                    }
                }
            }

            + Separator()

            // List node, whose ID is set (from the scene currently being inspected, but fall-back to THIS scene).
            val scene = rootNode?.scene ?: scene
            scene?.root?.let { root ->
                val nodesWithId = mutableListOf<Node>()
                root.treeVisitor { node ->
                    if (node.id.isNotEmpty()) {
                        nodesWithId.add(node)
                    }
                }
                for (node in nodesWithId.sortedBy { it.id }) {
                    + propertyRadioMenuItem(node, node.id)
                }
            }

        }
    }

    fun refresh() {
        nodeInspector.refresh()
    }

    // region ==== Node Picker ====
    private val nodePickerFilter = object : EventHandler<MouseEvent> {
        override fun handle(event: MouseEvent) {
            removeFilters()

            event.scene.findDeepestNodeAt(event.sceneX, event.sceneY)?.let { deepest ->
                chooseNode(deepest)
            }
            event.consume()
        }
    }
    private val nodePickerMovedFilter = object : EventHandler<MouseEvent> {
        override fun handle(event: MouseEvent) {
            event.consume()
        }
    }

    private fun chooseNode(deepest: Node) {
        dialog {
            title = "Choose a Node"

            content = listView<Node> {
                overridePrefWidth = 500f
                overridePrefHeight = 300f
                cellFactory = { listView, item ->
                    TextListCell(listView, item, item.toShortString()).apply {
                        padding(4, 4)
                        onMouseClicked {
                            rootNode = item
                            scene?.stage?.close()
                        }
                    }
                }
                var node: Node? = deepest
                while (node != null) {
                    items.add(node)
                    node = node.parent
                }
            }
        }.createStage(scene!!.stage!!) {
            show()
        }
    }

    private fun removeFilters() {
        for (stage in Application.instance.stages) {
            stage.mousePointer = MousePointer.ARROW
            stage.scene?.root?.removeEventFilter(EventType.MOUSE_PRESSED, nodePickerFilter)
            stage.scene?.root?.removeEventFilter(EventType.MOUSE_MOVED, nodePickerMovedFilter)
        }
    }

    private fun addNodePickerFilter() {
        for (stage in Application.instance.stages) {
            stage.mousePointer = MousePointer.CROSS_HAIR
            stage.scene?.root?.addEventFilter(EventType.MOUSE_PRESSED, nodePickerFilter)
            stage.scene?.root?.addEventFilter(EventType.MOUSE_MOVED, nodePickerMovedFilter)
        }
    }
    // endregion node picker

    companion object {
        const val ID = "NODE_INSPECTOR"
    }
}

object NodeInspectorActions : Actions(Tantalum.icons) {

    val REFRESH = define("refresh", "Refresh", Key.F5.noMods(), "Refresh") { tinted = true }

    val TOGGLE_INVISIBLE_NODES =
        define("toggle_invisible_nodes", "Toggle Invisible Nodes", null, "List invisible nodes too?") {
            iconName = "visible"
            tinted = true
        }

    val NODE_PICKER = define("node_picker", "Node Picker", null, "Pick a node by clicking within the stage") {
        iconName = "cross_hairs"
        tinted = true
    }

    val CHOOSE_NODE = define("choose_node", "Choose Node", null, "Inspect Stage root, or a node with id") {
        iconName = "root"
        tinted = true
    }

}

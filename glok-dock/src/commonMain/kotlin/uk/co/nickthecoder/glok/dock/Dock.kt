/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableSet
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.property
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

/**
Docks are panels at the edges of a stage, which can be contracted to save screen real-estate,
then shown again with a press of a button.

A Dock is part of a [Harbour]. Each Dock can be placed on any of the four sides,
and each side has a `Primary` and `Secondary` group of docks.
Only 1 dock within each group can be visible at any time. Expanding a dock will contract
the previously visible dock (if there is one).

You can hide/show a dock programmatically using the [visible] property.
Docks can be hidden/shown via a toggle button, which is part of the [HarbourSide],
not part of the [Dock] itself.

When a dock is hidden, it is removed from the scene-graph (i.e. Dock.scene == null and Dock.parent == null).

## Theme DSL
 *
 *    "dock" {
 *        child( ".container" ) { // A BorderPane
 *
 *            child( ".title_bar" ) {
 *                child( ".container" ) {
 *
 *                    child( ".hide" ) { // The 'Hide' Button (the default theme sets the graphic to a horizontal bar)
 *                    }
 *                }
 *            }
 *        }
 *    }
 */
abstract class Dock(

    /**
     * A Unique identifier. Used when loading/saving the meta-data.
     *
     * BEWARE, as Dock is a Node, it also has an [id], which is
     * unrelated to dockID.
     */
    val dockID: String,
    val harbour: Harbour,

    ) : Region(), Scrollable {

    // region ==== Properties ====

    /**
     * Appears in to ToggleButton at the side of the [Harbour], as well as in the title-bar of the dock.
     */
    val titleProperty by stringProperty("")
    var title by titleProperty

    val iconsProperty by property<NamedImages?>(null)
    var icons by iconsProperty

    /**
     * An optional icon, which is displayed to the left of the [title] in the ToggleButtons at the side of
     * the [Harbour] as well as in the title-bar of the dock.
     *
     * Glok's default Theme, `Tantalum`, applies a `fontColor` tint, so this icon should be white.
     * This allows a single monochrome icon to be used for the dark and light themes.
     */
    val iconNameProperty by stringProperty("")
    var iconName by iconNameProperty

    /**
     * Optional text which is appended (with a space) to the [title] in the ToggleButtons at the side of
     * the [Harbour]. These can be used a visual hints for a shortcut, which makes this Dock visible.
     */
    val prefixProperty by optionalStringProperty(null)
    var prefix by prefixProperty

    val buttonTextProperty: ObservableString = StringBinaryFunction(titleProperty, prefixProperty) { title, prefix ->
        if (prefix == null) title else "$prefix $title"
    }
    val buttonText by buttonTextProperty

    /**
     * The main content of the Dock.
     */
    val contentProperty by optionalNodeProperty(null)
    var content by contentProperty

    /**
     * Determines which side of the [Harbour] this Dock will be placed if not explicitly specified.
     * See [Harbour.add].
     */
    val defaultSideProperty by sideProperty(Side.LEFT)
    var defaultSide by defaultSideProperty

    /**
     * Each side of the [Harbour] has two sets of Docks, `primary` or `secondary`.
     * This decides if this Dock is placed in the `primary` set if not explicitly specified.
     * See [Harbour.add]
     */
    val defaultPrimaryProperty by booleanProperty(true)
    var defaultPrimary by defaultPrimaryProperty

    private val mutableSideProperty: SideProperty by sideProperty(Side.LEFT)
    val sideProperty = mutableSideProperty.asReadOnly()

    /**
     * The side of the [Harbour] that this dock occupies.
     * If this Dock is not within a Harbour (yet), then this value is meaningless.
     */
    var side by mutableSideProperty
        internal set

    private val mutablePrimaryProperty by booleanProperty(true)
    val primaryProperty = mutablePrimaryProperty.asReadOnly()

    /**
     * Is this dock in the `primary` or `secondary` set of docks?
     * If this Dock is not within a Harbour (yet), then this value is meaningless.
     */
    var primary by mutablePrimaryProperty
        internal set

    /**
     * Set this to `false` to prevent the user closing this Dock.
     * Closing the Dock programmatically is not affected by this.
     */
    val canCloseProperty by booleanProperty(true)
    var canClose by canCloseProperty

    // endregion

    // region ==== Fields ====

    private val commands = Commands().apply {
        attachTo(this@Dock)

        with(DockActions) {
            HIDE { visible = false }
        }
    }

    /**
     * Which sides of the harbour this Dock can be moved to by the user.
     *
     * Note, this does NOT affect where docks can be placed programmatically.
     */
    val allowedSides = mutableSetOf<Side>(
        Side.LEFT, Side.RIGHT, Side.TOP, Side.BOTTOM
    ).asMutableObservableSet()

    /**
     * Buttons (or other Nodes), which appear in the title-bar of this Dock.
     * Initially, this contains only a `close` button.
     * If you want the close button to remain on the far right, then add items at
     * the front of the list.
     *
     * I suggest these button icons are monochrome (tinted using the theme's font color),
     * so that they work for light and dark themes.
     *
     */
    val titleButtons = mutableListOf<Node>().asMutableObservableList()
    private val titleButtonsListener = titleButtons.addChangeListener { _, change ->
        for (i in change.removed.indices) {
            titleBar.items.removeAt(change.from + BUTTONS_OFFSET)
        }
        for ((i, item) in change.added.withIndex()) {
            titleBar.items.add(change.from + i + BUTTONS_OFFSET, item)
        }
    }

    private val titleBar = ToolBar()

    private val container = BorderPane().apply {
        styles.add(".container")
        top = titleBar
    }

    private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children: ObservableList<Node> = mutableChildren.asReadOnly()

    internal var owner: DockOwner? = null

    internal var harbourSideToggleButton: PropertyToggleButton<Node?, OptionalNodeProperty>? = null

    // endregion

    init {
        styles.add("dock")
        visible = false
        mutableChildren.addChangeListener(childrenListener)
        mutableChildren.add(container)

        visibleProperty.addChangeListener { _, _, visible ->
            if (visible && owner == null) {
                harbour.add(this)
            }
        }

        contentProperty.addChangeListener { _, old, new ->
            mutableChildren.remove(old)
            old?.styles?.remove(".content")
            container.center = new
            new?.styles?.add(".content")
            new?.section = true
            old?.section = false
        }

        container.top = titleBar
        container.center = content

        titleBar.apply {
            onMousePressed { event ->
                if (event.isSecondary) {
                    createContextMenu().show(titleBar)
                }
            }

            commands.build(harbour.iconSizeProperty) {
                with(DockActions) {

                    + label("") {
                        style(".title")
                        shrinkPriority = 1f
                        graphic = iconImageView()
                        textProperty.bindTo(titleProperty)
                    }
                    + spacer()

                    // The id can be used to find this button's index, so applications can
                    // add extra nodes before it.
                    + button(HIDE) { id = "hideDock" }
                }
            }
        }
    }

    // region ==== Methods ====

    fun iconImageView() = ImageView(OptionalImageTernaryFunction(
        iconsProperty, iconNameProperty, harbour.iconSizeProperty
    ) { icons, name, size ->
        icons?.get(name, size)
    })

    override fun scrollTo(descendant: Node) {
        if (this.isAncestorOf(descendant)) {
            visible = true
        }
    }

    /**
     * The position of this dock relative to its siblings.
     * -1 when the dock is not added to a harbour (i.e. when [owner] == null).
     */
    fun order() = owner?.indexOf(this) ?: - 1

    /**
     * Removes this Dock.
     *
     * If you only wish to _hide_ it, then set [visible] to false instead.
     */
    open fun close() {
        owner?.remove(this)
        owner = null
    }

    /**
     * This PopupMenu appears when the user right-clicks the Dock's ToggleButton, or the Dock's title.
     */
    open fun createContextMenu() = popupMenu {
        if (allowedSides.size > 1) {
            // Iterate over all Side values, rather than allowedSides, so we get a consistent order (allowedSides is a Set)
            for (side in Side.entries) {
                if (harbour.closedSides.contains(side)) continue
                if (! allowedSides.contains(side)) continue

                + toggleMenuItem(side.text) {
                    selectedProperty.bindTo(mutableSideProperty.equalTo(side))
                    onAction {
                        if (side != this@Dock.side) {
                            val tempVisible = this@Dock.visible
                            close()
                            harbour.add(this@Dock, side, primary)
                            this@Dock.visible = tempVisible
                        }
                    }
                }
            }
            + Separator()
        }

        + toggleMenuItem("Primary") {
            selectedProperty.bindTo(mutablePrimaryProperty)
            onAction {
                val tempVisible = this@Dock.visible
                close()
                harbour.add(this@Dock, side, ! selected)
                this@Dock.visible = tempVisible
            }
        }

        + Separator()

        + menuItem("Close") {
            visibleProperty.bindTo(canCloseProperty)
            onAction { close() }
        }
    }


    // endregion methods

    // region ==== Layout ====
    override fun nodeMaxWidth() = container.evalPrefWidth() + surroundX()
    override fun nodeMaxHeight() = container.evalPrefHeight() + surroundY()
    override fun layoutChildren() {
        setChildBounds(container, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
    // endregion

    private companion object {
        /**
         * The number of items in the [titleBar] before the [titleButtons].
         * Currently, there is a label ([titleProperty]) and a spacer, so this is 2.
         * The 3rd item in the toolbar (when [titleButtons] is empty) is the hide button, on the far right.
         *
         * Used to keep [titleBar].items in sync with [titleButtons] by [titleButtonsListener].
         */
        const val BUTTONS_OFFSET = 2

    }
}

object DockActions : Actions(Tantalum.icons) {

    val HIDE = define("hide", "Hide", null, "Hide") {
        iconName = "minimize"
        tinted = true
    }

}

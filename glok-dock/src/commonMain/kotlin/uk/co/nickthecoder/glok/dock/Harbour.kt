/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableSet
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableIntProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyle

/**
Allows [Dock]s to be placed at the sides of a scene.
A typical application places the [Harbour] in the `center` of a [BorderPane], with
a [MenuBar] and [ToolBar]s at the `top`.

The [content] of the [Harbour] is the main area of your application.

A [Harbour] has four sides (top,right,bottom,left), where [Dock]s can be placed.
Each side is split into two independent groups `primary` and `secondary`.
At most one [Dock] in each group is visible at a time.
Therefore, each side can show 0, 1 or 2 [Dock]s.

The design shamelessly mimics the docks in JetBrain's IntelliJ IDE.

## Structure

 *     ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
 *     ┃ Primary Top Buttons           Secondary Top Buttons ┃
 *     ┠───┬────────────────────────────┰────────────────┬───┨
 *     ┃ P │ Primary Top Dock           ║ Secondary      │ P ┃
 *     ┃   │                            ║ Top Dock       │   ┃
 *     ┃   │                            ║                │   ┃
 *     ┃   │                            ║                │   ┃
 *     ┃   ┝═══════╤════════════════════╧═════╤══════════┥   ┃
 *     ┃   │ Pri.  ║ `content`                ║ Pri.     │   ┃
 *     ┃   │ Left  ║                          ║ Right    │   ┃
 *     ┃   │ Dock  ║ Your applications        ║ Dock     │   ┃
 *     ┃   │       ║ main content.            ║          │   ┃
 *     ┃   ┝═══════╢                          ║          │   ┃
 *     ┃   │ Sec.  ║                          ╟══════════┥   ┃
 *     ┃   │ Left  ║                          ║ Sec.     │   ┃
 *     ┃   │ Dock  ║                          ║ Right    │   ┃
 *     ┃   │       ║                          ║ Dock     │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   ┝═══════╧══════════╤═══════════════╧══════════┥   ┃
 *     ┃   │ Pri. Bottom Dock ║         Sec. Bottom Dock │   ┃
 *     ┃ S │                  ║                          │ S ┃
 *     ┠───┴──────────────────┸──────────────────────────┴───┨
 *     ┃ Primary Bottom Buttons     Secondary Bottom Buttons ┃
 *     ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

Created with [Blokart](https://gitlab.com/nickthecoder/blokart)

The double-walled lines represent the dividers of a [SplitPane].
These can be dragged by the user to adjust the width / height of the Dock(s).

`P` and `S` labels are short for `Primary Buttons` and `Secondary Buttons`.

Each side of the harbour can have any number of [Dock]s, with a toggle button
for each. However, on each side there is at most 1 primary, and at most 1
secondary Dock visible. Therefore, there can be a maximum of 8 docks visible.

The [Harbour] uses a [BorderPane], with the primary and secondary buttons
occupying the `top`, `left`, `bottom` and `right` parts.

The `center` of the [BorderPane] contains a pair of nested [SplitPane]s.
(the horizontal SplitPane is inside the vertical SplitPane).
The application's main content lives in the center-middle of these two [SplitPane]s.
The left/right top/bottom of these split panes are where the open [Dock]s live.

When a `primary` and `secondary` dock are both open on one side of the [Harbour],
we see yet another [SplitPane].
The user can choose how much space is allocated to each by dragging the divider.

So in this diagram above, we see a total of 6 SplitPanes
(the nested pair, plus 4 more - one for each side of the Harbour).

If we hide some docks, so that only 3 docks remain visible :

 *     ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
 *     ┃ Primary Top Buttons           Secondary Top Buttons ┃
 *     ┠───┬─────────────────────────────────────────────┬───┨
 *     ┃ P │ Primary Top Dock                            │ P ┃
 *     ┃   │                                             │   ┃
 *     ┃   │                                             │   ┃
 *     ┃   │                                             │   ┃
 *     ┃   ┝═══════╤══════════════════════════╤══════════┥   ┃
 *     ┃   │ Pri.  ║ `content`                ║ Sec.     │   ┃
 *     ┃   │ Left  ║                          ║ Right    │   ┃
 *     ┃   │ Dock  ║ Your applications        ║ Dock     │   ┃
 *     ┃   │       ║ main content.            ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃   │       ║                          ║          │   ┃
 *     ┃ S │       ║                          ║          │ S ┃
 *     ┠───┴───────┸──────────────────────────┸──────────┴───┨
 *     ┃ Primary Bottom Buttons     Secondary Bottom Buttons ┃
 *     ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

## Theme DSL

 *    "harbour" {
 *    }
 *
 *    "harbour_side" {
 *        // The top/right/bottom/left nodes of the Harbour's BorderPane.
 *        // It does NOT contain the docks themselves.
 *
 *        // Tantalum does not use these.
 *        ":top" { ... }
 *        ":right" { ... }
 *        ":bottom" { ... }
 *        ":left" { ... }
 *
 *        child( ".buttons" ) { // A Box
 *            child( ".primary" ) { // A Box
 *                descendant( "toggle_button" ) {
 *                // These are the buttons which show/hide a Dock.
 *            }
 *        }
 *        child( ".secondary" ) { // A Box
 *            descendant( "toggle_button" ) {
 *                // These are the buttons which show/hide a Dock.
 *            }
 *        }
 *
 *
 *        // Set when a Dock is being dragged.
 *        ":drag_target" {
 *
 *            // Highlight ".gap" to indicate a dock can be dragged here.
 *            // There are 8 "gaps" in total. On each side, there are two "gaps" between the
 *            // primary and secondary buttons.
 *
 *            descendant(".gap") {
 *
 *                minWidth(number)    // Set minWidth and minHeight, otherwise the drag target won't be visible
 *                minHeight(number)   //  if there are docks on this side of the harbour yet.
 *
 *                ":drag_hover" {
 *                    // When a Dock is being dragged over to this gap.
 *                }
 *            }
 *        }
 *    }
 *
[Harbour] and [Dock] inherit the features of [Region].
 */

class Harbour(

    initialContent: Node? = null

) : Region() {

    // region ==== Properties ====
    /**
     * The [Node] at the center of the Harbour - the part of your application which are not [Dock]s.
     */
    val contentProperty by optionalNodeProperty(null)
    var content by contentProperty

    val iconSizeProperty by stylableIntProperty(18)
    val iconSize by iconSizeProperty

    // endregion

    // regin ==== Fields ====

    var dockFactory: DockFactory? = null

    /**
     * Prevents the user moving Docks to the specified sides.
     * This does NOT affect where docks can be placed programmatically.
     *
     * For example, if you want to prevent the top of the harbour to host [Dock]s add [Side.TOP].
     */
    val closedSides = mutableSetOf<Side>().asMutableObservableSet()

    private val contentContainer = SingleContainer(initialContent).apply {
        growPriority = 1f // Docks won't grow/shrink when the Harbour is resized.
        contentProperty.bindTo(this@Harbour.contentProperty)
    }

    internal val mutableDocks = mutableListOf<Dock>().asMutableObservableList()
    /**
     * The [Dock]s which have been added to this Harbour. This includes docks which are collapsed
     * (i.e. their content is not visible).
     */
    val docks: ObservableList<Dock> = mutableDocks.asReadOnly()

    private val left = HarbourSide(this, Side.LEFT).apply { id = "Harbour_Left" }
    private val right = HarbourSide(this, Side.RIGHT).apply { id = "Harbour_Right" }
    private val top = HarbourSide(this, Side.TOP).apply { id = "Harbour_Top" }
    private val bottom = HarbourSide(this, Side.BOTTOM).apply { id = "Harbour_Bottom" }

    private val hSplitPane = SplitPane().apply {
        id="Harbour_H_Split"
        growPriority = 1f // Docks won't grow/shrink when the Harbour is resized.
        items.addAll(left.primarySecondarySplitPane, contentContainer, right.primarySecondarySplitPane)
        dividers[0].position = 0.25f
        dividers[1].position = 0.75f
    }
    private val vSplitPane = SplitPane(Orientation.VERTICAL).apply {
        id="Harbour_V_Split"
        items.addAll(top.primarySecondarySplitPane, hSplitPane, bottom.primarySecondarySplitPane)
        dividers[0].position = 0.25f
        dividers[1].position = 0.75f
    }

    private val borderPane = BorderPane().apply {
        id="Harbour_Border"
        top = this@Harbour.top
        right = this@Harbour.right
        bottom = this@Harbour.bottom
        left = this@Harbour.left
        this.center = vSplitPane
    }

    private val harbourSides = mapOf(
        Side.LEFT to left,
        Side.RIGHT to right,
        Side.TOP to top,
        Side.BOTTOM to bottom
    )
    override val children: ObservableList<Node> = listOf(borderPane).asObservableList()

    // endregion

    init {
        styles.add("harbour")
        content = initialContent
        claimChildren()
    }

    // region ==== Methods ====

    private var draggingDock: Boolean = false

    private fun findHarbourSide(sceneX: Float, sceneY: Float, sides: Set<Side>): HarbourSide? =
        scene?.findDeepestNodeAt(sceneX, sceneY)?.firstToRoot { it is HarbourSide && sides.contains(it.side) } as? HarbourSide

    private fun findDragDestination(sceneX: Float, sceneY: Float, sides: Set<Side>): Node? {
        val node = scene?.findDeepestNodeAt(sceneX, sceneY)
            ?.firstToRoot { it.styles.contains(".gap") || it is PropertyToggleButton<*, *> }
            ?: return null
        if (node.firstToRoot { it is HarbourSide && sides.contains(it.side) } == null) return null
        return node
    }

    private var draggingOver: Node? = null

    /**
     * Begin, or continue dragging a dock's ToggleButton to another side of the Harbour,
     * or from primary to secondary halves of the same side.
     *
     * [HarbourSide] is given the pseudoStyle ":drag_target", which highlights the gaps _slightly_
     *
     */
    internal fun dragging(dock: Dock, event: MouseEvent) {
        if (!event.isPrimary) return

        draggingOver?.pseudoStyles?.remove(":drag_hover")
        draggingOver = findDragDestination(event.sceneX, event.sceneY, dock.allowedSides)
        draggingOver?.pseudoStyles?.add(":drag_hover")
        for (side in dock.allowedSides) {
            harbourSides[side]?.pseudoStyle(":drag_target")
        }
        draggingDock = true
    }

    internal fun endDragging(dock: Dock, event: MouseEvent) {
        draggingOver?.pseudoStyles?.remove(":drag_hover")
        for (harbourSide in harbourSides.values) {
            harbourSide.pseudoStyles.remove(":drag_target")
        }
        if (draggingDock) {
            draggingDock = false
            val harbourSide = findHarbourSide(event.sceneX, event.sceneY, dock.allowedSides) ?: return
            val destination = findDragDestination(event.sceneX, event.sceneY, dock.allowedSides) ?: return

            dock.close()
            if (destination is PropertyToggleButton<*, *>) {
                // We have dragged over another Dock's button.

                val primaryButtons = harbourSide.primaryButtons.children.map {
                    if (it is Rotation) it.child else it
                }.filter { it != dock.harbourSideToggleButton }

                val primaryIndex = primaryButtons.indexOf(destination)
                if (primaryIndex >= 0) {
                    harbourSide.remove(dock)
                    harbourSide.add(dock, true, primaryIndex)
                    return
                }

                val secondaryButtons = harbourSide.secondaryButtons.children.map {
                    if (it is Rotation) it.child else it
                }.filter { it != dock.harbourSideToggleButton }

                val secondaryIndex = secondaryButtons.indexOf(destination)
                if (secondaryIndex >= 0) {
                    harbourSide.remove(dock)
                    harbourSide.add(dock, false, secondaryIndex + 1)
                    return
                }

            }
            val toPrimary = harbourSide.secondaryGap !== destination
            harbourSide.add(dock, toPrimary)
        }
    }

    /**
     * Adds a dock at its preferred side of the Harbour. See [Dock.defaultSide].
     * If the dock is already present, this is a no-op.
     *
     * The dock is created with the aid if [dockFactory].
     */
    fun add(dockID: String): Dock? {
        var dock = findDock(dockID)
        if (dock == null) {
            dock = dockFactory?.createDock(this, dockID)
            if (dock != null) {
                add(dock, dock.defaultSide, dock.defaultPrimary)
            }
        }
        return dock
    }

    /**
     * Adds a dock to a particular side of the Harbour.
     * If the dock is already present (in any position), this is a no-op.
     *
     * The dock is created with the aid if [dockFactory].
     */
    fun add(dockID: String, side: Side, isPrimary: Boolean = true): Dock? {
        var dock = findDock(dockID)
        if (dock == null) {
            dock = dockFactory?.createDock(this, dockID)
            if (dock != null) {
                add(dock, side, isPrimary)
            }
        }
        return dock
    }

    /**
     * Add a dock to its preferred side of the Harbour. See [Dock.defaultSide].
     * This is rarely used because it is preferable to use [add] which takes a `dockID`.
     */
    fun add(dock: Dock) {
        add(dock, dock.defaultSide, dock.defaultPrimary)
    }

    /**
     * Adds the [dock] to this Harbour at a particular position give by [side] and [isPrimary].
     * If the dock is already present (in any position), this is a no-op.
     *
     * This is rarely used because it is preferable to use [add] which takes a `dockID`.
     */
    fun add(dock: Dock, side: Side, isPrimary: Boolean = true) {
        if (docks.contains(dock)) return

        harbourSides[side]?.add(dock, isPrimary)
    }

    fun findDock(dockID: String): Dock? = docks.firstOrNull { it.dockID == dockID }

    // endregion

    /**
     * Gets the position of a [SplitPane]'s divider (as a ratio), which determines the size of an
     * open [Dock].
     */
    fun getDividerPosition(side: Side): Float {
        return when (side) {
            Side.TOP -> vSplitPane.dividers[0].position
            Side.RIGHT -> hSplitPane.dividers[1].position
            Side.BOTTOM -> vSplitPane.dividers[1].position
            Side.LEFT -> hSplitPane.dividers[0].position
        }
    }

    /**
     * Sets the position of a [SplitPane]'s divider (as a ratio), which determines the size of an
     * open [Dock].
     * For [side] `LEFT`/`TOP` the default value is 0.25, and for `RIGHT`/`BOTTOM` is 0.75
     * i.e. open docks are allocated a quarter of the available space.
     *
     */
    fun setDividerPosition(side: Side, ratio: Float) {
        val divider = when (side) {
            Side.TOP -> vSplitPane.dividers[0]
            Side.RIGHT -> hSplitPane.dividers[1]
            Side.BOTTOM -> vSplitPane.dividers[1]
            Side.LEFT -> hSplitPane.dividers[0]
        }
        divider.position = ratio
    }

    /**
     * Gets the position of the [SplitPane]'s divider (as a ratio), which separates the
     * `Primary` and `Secondary` [Dock]s when both are visible.
     */
    fun getSplitPosition(side: Side): Float {
        return harbourSides[side]?.getSplitPosition() ?: 0.5f
    }

    /**
     * Sets the position of the [SplitPane]'s divider (as a ratio), which separates the
     * `Primary` and `Secondary` [Dock]s when both are visible.
     * The default value is 0.5, i.e. both [Dock]s are given equal space.
     */
    fun setSplitPosition(side: Side, ratio: Float) {
        harbourSides[side]?.setSplitPosition(ratio)
    }

    // region ==== Load / Save ====
    // Save/Load are extension functions. See HarbourPreferences.kt in jvmMain.
    // endregion load / save

    // region ==== Layout ====
    override fun nodePrefWidth() = borderPane.evalPrefWidth() + surroundX()
    override fun nodePrefHeight() = borderPane.evalPrefHeight() + surroundY()
    override fun layoutChildren() {
        setChildBounds(borderPane, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
    // endregion

}

/**
 * Extends Glok's DSL to support [Harbour]s.
 */
fun harbour(block: Harbour.() -> Unit) = Harbour().apply(block)

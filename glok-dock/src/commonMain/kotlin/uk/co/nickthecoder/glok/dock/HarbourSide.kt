/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.collections.isEmptyProperty
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.property.boilerplate.BooleanBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleOptionalNodeProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.propertyToggleButton
import uk.co.nickthecoder.glok.scene.dsl.spacer
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.util.GlokException

/**
 * This Node contains the toggle buttons which hide/show a [Dock].
 * It does _not_ contain the docks themselves.
 * See [Harbour] for a diagram, as well as Theme DSL.
 *
 * There are two sets of button per-side. A `Primary` group, and a `Secondary` group.
 * For [side] top or bottom, the `Primary` group is on the left, and secondary on the right, with a double-gap between them.
 * For [side] left or right, the `Primary` group is at the top, and `Secondary` at the bottom.
 *
 * There is a _double_ gap between each set, so that when a [Dock] is dragged, two gaps are highligthed,
 * which makes it is easy to place it at the end of the `Primary` group,  or the beginning of the `Secondary` group.
 * Dragging over existing Docks can place the dragged Dock in other positions.
 */
internal class HarbourSide internal constructor(
    val harbour: Harbour,
    val side: Side
) : Region(), DockOwner {

    // region ==== Fields ====

    internal val primaryButtons = Box(side.lrToVertical()).apply {
        styles.add(".primary")
        fill = true
    }
    internal val secondaryButtons = Box(side.lrToVertical()).apply {
        styles.add(".secondary")
        fill = true
    }
    internal val primaryGap = spacer().apply { style(".gap") }
    internal val secondaryGap = spacer().apply { style(".gap") }

    /**
     * Always contains exactly 4 child nodes.
     * The [primaryButtons] and [secondaryButtons] with
     * [primaryGap] and [secondaryGap] sandwiched between them.
     */
    private val buttonsBox = Box(side.lrToVertical()).apply {
        fill = true
        styles.add(".buttons")
        children.addAll(primaryButtons, primaryGap, secondaryGap, secondaryButtons)
    }

    private val primaryDockProperty = SimpleOptionalNodeProperty(null)
    private val secondaryDockProperty = SimpleOptionalNodeProperty(null)

    /**
     * The top/left part of the [primarySecondarySplitPane].
     * This is NOT a descendant of [HarbourSide] Node.
     */
    private val primaryContainer = SingleContainer().apply {
        bindVisible = true
        contentProperty.bindTo(primaryDockProperty)
    }

    /**
     * The bottom/right part of the [primarySecondarySplitPane]
     * This is NOT a descendant of [HarbourSide] Node.
     */
    private val secondaryContainer = SingleContainer().apply {
        bindVisible = true
        contentProperty.bindTo(secondaryDockProperty)
    }

    /**
     * This split pane allows the user to change the relative heights of the docks
     * when the primary and secondary docks are both visible.
     * This is NOT a descendant of [HarbourSide] Node.
     */
    internal val primarySecondarySplitPane = SplitPane().apply {
        styles.add(".docks")
        growPriority = 0f
        orientation = side.lrToVertical()
        items.addAll(primaryContainer, secondaryContainer)
        visibleProperty.bindTo(BooleanBinaryFunction(primaryDockProperty, secondaryDockProperty) { a, b ->
            a != null || b != null
        })
    }

    override val children: ObservableList<Node> = listOf(buttonsBox).asObservableList()

    // endregion Fields

    init {
        styles.add("harbour_side")
        pseudoStyles.add(side.pseudoStyle)
        sectionProperty.bindTo(! primaryButtons.children.isEmptyProperty())
        claimChildren()
    }

    // region ==== Methods ====

    internal fun add(dock: Dock, isPrimary: Boolean) =
        add(dock, isPrimary, if (isPrimary) primaryButtons.children.size else 0)

    internal fun add(dock: Dock, isPrimary: Boolean, index: Int) {
        if (dock.owner != null || harbour.mutableDocks.contains(dock)) throw GlokException("A Dock cannot be in two places")

        harbour.mutableDocks.add(dock)
        dock.owner = this
        dock.side = side
        dock.primary = isPrimary

        dock.harbourSideToggleButton?.let {
            it.textProperty.unbind()
            it.selectedProperty.bidirectionalUnbind(dock.visibleProperty)
        }

        val sideButton = propertyToggleButton<Node?, OptionalNodeProperty>(
            if (isPrimary) primaryDockProperty else secondaryDockProperty, dock, ""
        ) {
            textProperty.bindTo(dock.titleProperty)
            selectedProperty.bidirectionalBind(dock.visibleProperty)
            tooltip = TextTooltip("Click to Hide/Show\nDrag to move")
            graphic = dock.iconImageView()
            onMousePressed(HandlerCombination.BEFORE) { it.capture() }
            onMouseDragged { event ->
                harbour.dragging(dock, event)
                armed = false
            }
            onMouseReleased(HandlerCombination.BEFORE) { event ->
                harbour.endDragging(dock, event)
            }
            onPopupTrigger {
                // A bodge, because `show` gets the position wrong for transformed Nodes.
                var relativeTo: Node? = this
                if (relativeTo?.parent is Rotation) {
                    relativeTo = relativeTo.parent
                }
                if (relativeTo != null) {
                    dock.createContextMenu().show(relativeTo, side.opposite())
                }
            }
        }
        dock.harbourSideToggleButton = sideButton
        // Now add the toggleButton.
        val maybeRotated = when (side) {
            Side.LEFT -> Rotation(sideButton, - 90)
            Side.RIGHT -> Rotation(sideButton, 90)
            else -> sideButton
        }
        val half = if (isPrimary) primaryButtons else secondaryButtons
        half.children.add(index, maybeRotated)
        if (dock.visible) {
            if (isPrimary) {
                primaryDockProperty.value = dock
            } else {
                secondaryDockProperty.value = dock
            }
        }
    }

    override fun remove(dock: Dock) {
        dock.visible = false
        dock.owner = null
        harbour.mutableDocks.remove(dock)

        var toRemove: Node? = dock.harbourSideToggleButton
        if (toRemove?.parent is Rotation) toRemove = toRemove.parent

        for (half in listOf(primaryButtons, secondaryButtons)) {
            half.children.remove(toRemove)
        }
        harbour.dockFactory?.dockClosed(harbour, dock)
    }

    override fun indexOf(dock: Dock): Int {
        val button = dock.harbourSideToggleButton ?: return - 1
        val buttons = (if (dock.primary) primaryButtons else secondaryButtons)
        val actualButtons = buttons.children.map {
            if (it is Rotation) it.child else it
        }
        return actualButtons.indexOf(button)
    }

    fun getSplitPosition() = primarySecondarySplitPane.dividers[0].position
    fun setSplitPosition(ratio: Float) {
        primarySecondarySplitPane.dividers[0].position = ratio
    }

    // endregion methods

    // region ==== Layout ====
    override fun nodePrefWidth() = buttonsBox.evalPrefWidth() + surroundX()
    override fun nodePrefHeight() = buttonsBox.evalPrefHeight() + surroundY()
    override fun layoutChildren() {
        setChildBounds(buttonsBox, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
    // endregion layout

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dock

import uk.co.nickthecoder.glok.property.BidirectionalBind
import uk.co.nickthecoder.glok.property.boilerplate.BooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty

/**
Creates docks using lambdas.

Pass the constructor a map of Dock IDs mapped to a lambda which will create the dock.

[visibleProperty] returns a [BooleanProperty] which can be bound bidirectionally
with `ToggleButton.selectedProperty`, `ToggleMenuItem.selectedProperty`
or `ToggleCommand.selectedProperty`
 */
class OnDemandDockFactory(

    harbour: Harbour,
    private val map: Map<String, (Harbour) -> Dock>

) : DockFactory {

    private val visibleProperties = mutableMapOf<String, BooleanProperty>()

    private val binds = mutableMapOf<String, BidirectionalBind>()

    init {
        harbour.dockFactory = this

        for (id in map.keys) {
            visibleProperties[id] = SimpleBooleanProperty(false).apply {
                addChangeListener { _, _, newValue ->
                    if (newValue) {
                        if (harbour.findDock(id) == null) {
                            harbour.add(id)
                        }
                    }
                }
            }
        }
    }

    override fun createDock(harbour: Harbour, dockID: String): Dock? {
        val existing = harbour.findDock(dockID)
        if (existing != null) return existing

        val dock = map[dockID]?.invoke(harbour)
        if (dock != null) {
            binds[dockID] = dock.visibleProperty.bidirectionalBind(visibleProperties[dockID] !!)
        }
        return dock
    }

    override fun dockClosed(harbour: Harbour, dock: Dock) {
        binds[dock.dockID]?.unbind()
        visibleProperties[dock.dockID]?.value = false
    }

    fun visibleProperty(dockID: String) = visibleProperties[dockID]

}

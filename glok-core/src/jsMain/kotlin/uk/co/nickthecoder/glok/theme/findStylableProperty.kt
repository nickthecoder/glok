/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.property.StylableProperty
import uk.co.nickthecoder.glok.scene.Scene
import kotlin.reflect.KClass

/**
 * Looks for a method with this signature : get{PropertyName}Property() : StylableProperty<*>
 * If one is found, invokes the method, and returns the value.
 * Otherwise, return null.
 *
 * Note, [node] is of type [Any], because it _could_ also be used to style other objects
 * e.g. For [Scene], to set [Scene.backgroundColor] (which is a [StylableProperty]).
 */
actual fun findStylableProperty(
    node: Any,
    valueType: KClass<*>,
    propertyName: String,
    selector: Selector
): StylableProperty<*>? {
    val fields = js("Object.values(foo)")
    for (field in fields.iterator()) {
        if (field is StylableProperty<*>) {
            if (field.beanName == propertyName) return field
        }
    }
    return null
}

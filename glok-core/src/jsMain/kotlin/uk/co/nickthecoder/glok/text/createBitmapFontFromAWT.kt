/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.backend.DummyTexture
import uk.co.nickthecoder.glok.text.BitmapFont
import uk.co.nickthecoder.glok.text.BitmapFontBuilder
import uk.co.nickthecoder.glok.text.FontIdentifier
import uk.co.nickthecoder.glok.util.log

internal actual fun createBitmapFont(
    identifier: FontIdentifier,
    block: (BitmapFontBuilder.() -> Unit)?
): BitmapFont {
    log.warn("JS createBitmapFont not implemented")
    return BitmapFont(
        identifier, DummyTexture("dummyFont", 1, 1), emptyMap(),
        1f, 1f, 1f, 1f, 1f, 1f
    )
}

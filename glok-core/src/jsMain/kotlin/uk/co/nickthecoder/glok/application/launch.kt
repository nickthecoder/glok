/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.backend.webgl.WebGLBackend
import uk.co.nickthecoder.glok.util.*
import kotlin.reflect.KClass

actual fun launch(klass: KClass<out Application>, rawArgs: Array<out String>) {
    if (Application.running) throw GlokException("Glok has already started")
    Application.running = true
    log = ConsoleLog().apply { level = Log.TRACE }

    Platform.begin()
    backend = WebGLBackend()

    Application.launchPart2(Restart(klass, rawArgs))
}


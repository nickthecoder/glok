/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.backend.webgl

import org.khronos.webgl.WebGLRenderingContext
import org.w3c.dom.HTMLCanvasElement
import uk.co.nickthecoder.glok.backend.Window
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.WindowListener
import uk.co.nickthecoder.glok.util.log
import org.w3c.dom.Document as HTMLDocument
import org.w3c.dom.Window as HTMLWindow

class WebGLWindow(val canvas: HTMLCanvasElement) : Window {

    internal val gl = canvas.getContext("webgl") as WebGLRenderingContext

    override var windowListener: WindowListener? = null

    private val htmlWindow: HTMLWindow?
        get() = canvas.ownerDocument?.defaultView

    private val htmlDocument: HTMLDocument?
        get() = canvas.ownerDocument

    override var title: String
        get() = htmlDocument?.title ?: ""
        set(value) {
            htmlDocument?.title = value
        }

    override var resizable: Boolean
        get() = false
        set(_) {
            log.info("WebGLWindow resizable is ignored")
        }

    override var maximized: Boolean
        get() = false
        set(_) {
            log.info("WebGLWindow maximized is ignored")
        }

    override var minimized: Boolean
        get() = false
        set(_) {}

    override var shouldClose: Boolean
        get() = false
        set(_) {
        }

    override fun show() {
    }

    override fun hide() {
        log.warn("Cannot hide a WebGLWindow")
    }

    override fun setIcon(images: List<Image>, tint: Color?) {
        log.warn("setIcon not implemented")
    }

    override fun closeNow() {
        htmlWindow?.close()
    }

    override fun moveTo(x: Int, y: Int) {
        log.warn("WebGLWindows cannot be moved")
    }

    override fun resize(width: Int, height: Int) {
        log.warn("WebGLWindows cannot be resized")
    }

    override fun size(): Pair<Int, Int> {
        return Pair(htmlWindow !!.innerWidth, htmlWindow !!.innerHeight)
    }

    override fun position(): Pair<Int, Int> {
        return Pair(0, 0)
    }
}

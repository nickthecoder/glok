/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.backend.webgl

import kotlinx.browser.window
import org.khronos.webgl.WebGLRenderingContext
import org.w3c.dom.HTMLCanvasElement
import uk.co.nickthecoder.glok.backend.*
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.util.GlokException
import uk.co.nickthecoder.glok.util.Matrix
import uk.co.nickthecoder.glok.util.log

class WebGLBackend : Backend {

    private var gl: WebGLRenderingContext? = null

    private var currentWindow: WebGLWindow? = null
        set(v) {
            field = v
            if (v != null) {
                gl = v.gl
            } else {
                gl = null
            }
        }

    private fun reset() {
    }

    override fun drawFrame(window: Window, block: () -> Unit) {
        currentWindow = window as WebGLWindow
        try {
            block()
        } finally {
            currentWindow = null
        }
    }

    private var inView = false
    override fun drawView(width: Float, height: Float, scale: Float, block: () -> Unit) {
        inView = true
        try {
            block()
        } finally {
            inView = false
        }
    }

    override fun clear(color: Color) {
        gl?.clearColor(color.red, color.green, color.blue, color.alpha)
    }

    override fun processEvents(timeoutSeconds: Double) {
        log.warn("processEvents Not yet implemented")
    }

    override fun setMousePointer(window: Window, cursor: MousePointer) {
        log.warn("setMousePointer Not yet implemented")
    }

    override fun beginClipping(left: Float, top: Float, width: Float, height: Float): Boolean {
        log.warn("beginClipping Not yet implemented")
        return true
    }

    override fun endClipping() {
        log.warn("endClipping Not yet implemented")
    }

    override fun noClipping(block: () -> Unit) {
        log.warn("noClipping Not yet implemented")
    }

    override fun transform(transformation: Matrix) {
        log.warn("transform Not yet implemented")
    }

    override fun clearTransform() {
        log.warn("clearTransform Not yet implemented")
    }

    override fun transform(transformation: Matrix, block: () -> Unit) {
        log.warn("transform Not yet implemented")
        try {
            block()
        } finally {

        }
    }

    override fun fillRect(rect: GlokRect, color: Color) {
        log.warn("fillRect Not yet implemented")
    }

    override fun fillRect(left: Float, top: Float, right: Float, bottom: Float, color: Color) {
        log.warn("fillRect Not yet implemented")
    }

    override fun fillQuarterCircle(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, radius: Float) {
        log.warn("fillQuarterCircle Not yet implemented")
    }

    override fun strokeInsideRect(rect: GlokRect, thickness: Float, color: Color) {
        log.warn("strokeInsideRect Not yet implemented")
    }

    override fun strokeInsideRect(
        left: Float,
        top: Float,
        right: Float,
        bottom: Float,
        thickness: Float,
        color: Color
    ) {
        log.warn("strokeInsideRect Not yet implemented")
    }

    override fun strokeQuarterCircle(
        x1: Float,
        y1: Float,
        x2: Float,
        y2: Float,
        color: Color,
        radius: Float,
        innerRadius: Float
    ) {
        log.warn("strokeQuarterCircle Not yet implemented")
    }

    override fun drawTexture(
        texture: Texture,
        srcX: Float,
        srcY: Float,
        width: Float,
        height: Float,
        destX: Float,
        destY: Float,
        destWidth: Float,
        destHeight: Float,
        modelMatrix: Matrix?
    ) {
        log.warn("drawTexture Not yet implemented")
    }

    override fun drawTintedTexture(
        texture: Texture,
        tint: Color,
        srcX: Float,
        srcY: Float,
        width: Float,
        height: Float,
        destX: Float,
        destY: Float,
        destWidth: Float,
        destHeight: Float,
        modelMatrix: Matrix?
    ) {
        log.warn("drawTintedTexture Not yet implemented")
    }

    override fun gradient(triangles: FloatArray, colors: Array<Color>) {
        log.warn("gradient Not yet implemented")
    }

    override fun hsvGradient(triangles: FloatArray, colors: Array<FloatArray>) {
        log.warn("hsvGradient Not yet implemented")
    }

    override fun batch(texture: Texture, tint: Color?, modelMatrix: Matrix?, block: TextureBatch.() -> Unit) {
        log.warn("batch Not yet implemented")
        try {
            //block()
        } finally {

        }
    }

    override fun createWindow(width: Int, height: Int): Window {
        val untypedCanvas =
            window.document.getElementById("canvas") ?: throw GlokException("HTML element #canvas not found")
        val canvas = (untypedCanvas as? HTMLCanvasElement)
            ?: throw GlokException("Expected HTMLCanvasElement, but found $untypedCanvas")
        return WebGLWindow(canvas)
    }

    override fun createTexture(name: String, width: Int, height: Int, pixels: IntArray): Texture {
        return DummyTexture(name, width, height)
    }

    override fun resources(path: String): Resources {
        return DummyResources()
    }

    override fun fileResources(path: String): Resources {
        return DummyResources()
    }

    override fun monitorSize(): Pair<Int, Int>? {
        return Pair(window.outerWidth, window.outerHeight)
    }


}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

import uk.co.nickthecoder.glok.property.ReadOnlyProperty
import kotlin.js.Date
import kotlin.reflect.KClass

/**
 * Copy/pasted from :
 * https://discuss.kotlinlang.org/t/is-there-a-way-to-use-the-new-operator-with-arguments-on-a-dynamic-variable-in-kotlin-javascript/6126/4
 */
private fun <T : Any> JsClass<T>.newInstance(vararg args: dynamic): T {
    @Suppress("UNUSED_VARIABLE")
    val ctor = this

    @Suppress("UNUSED_VARIABLE")
    val argsArray = (listOf(null) + args).toTypedArray()

    //language=JavaScript 1.6
    return js("new (Function.prototype.bind.apply(ctor, argsArray))").unsafeCast<T>()
}

actual fun <T : Any> KClass<T>.newInstance(): T = this.js.newInstance()


internal actual fun findProperties(obj: Any): List<ReadOnlyProperty<*>> = emptyList()

internal actual fun dumpStackTrace() {}

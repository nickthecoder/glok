/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

actual object Platform {

    actual val OS: String get() = "WEB"

    private val pendingRunnable = mutableListOf<() -> Unit>()


    actual fun isWindows() = false

    actual fun isLinux() = false

    actual fun isUnix() = false

    actual fun isMacOS() = false

    actual fun isKDE() = false

    actual fun isGnome() = false

    internal actual fun begin() {
    }

    actual fun isGlokThread() = true

    actual fun runLater(runnable: () -> Unit) {
        pendingRunnable.add(runnable)
    }

    internal actual fun performPendingRunnable() {
        if (pendingRunnable.isEmpty()) {
            val toRun = pendingRunnable.toList()
            pendingRunnable.removeAll(toRun)
            for (item in toRun) {
                try {
                    item.invoke()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    actual fun getEnv(name: String): String? = null
}

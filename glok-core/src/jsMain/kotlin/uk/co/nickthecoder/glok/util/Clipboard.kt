/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

import kotlinx.browser.window
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.util.Clipboard.containsTextProperty
import uk.co.nickthecoder.glok.util.Clipboard.textContents

/**
 * Reading the HTML DOM api, I don't know if it is possible to _monitor_ the clipboard so that we can spot
 * when the clipboard does/doesn't contains text.
 * So I don't know if it is possible to implement [containsTextProperty] (cleanly).
 *
 * Also I don't know how to implement [textContents] (cleanly), as the JS API uses promises,
 * whereas this API (which was designed for the jvm) assumes that the clipboard can be
 * read immediately (not via a Promise).
 *
 * So, at the moment, [containsTextProperty] is always `false`, and the only operation that works
 * is setting [textContents].
 *
 * A dirty solution, would periodically check the clipboard, update [containsTextProperty]
 * and cache the text (if there is any).
 * This would be far from ideal.
 */
actual object Clipboard {

    private val webClipboard = window.navigator.clipboard
    private var cachedText: String? = null

    private val mutableContainsTextProperty = SimpleBooleanProperty(false)

    /**
     * Does the clipboard currently contain text.
     */
    actual val containsTextProperty: ObservableBoolean = mutableContainsTextProperty.asReadOnly()
    actual var containsText by mutableContainsTextProperty
        private set

    /**
     * The opposite of [containsTextProperty].
     * Use this as the `disabledProperty` for paste actions.
     */
    actual val noTextProperty = ! mutableContainsTextProperty

    actual var textContents: String?
        get() = null
        set(value) {
            value?.let { webClipboard.writeText(it) }
        }

    /**
     * If this is periodically called, then [containsTextProperty] and reading [textContents]
     * will work. Doing so would be a nasty bodge to get around the inability (maybe?)
     * to monitor the clipboard from javascript :-(
     */
    internal fun cacheClipboard() {
        webClipboard.readText().then { text ->
            cachedText = text
            containsText = text.isNotBlank()
        }
    }
}

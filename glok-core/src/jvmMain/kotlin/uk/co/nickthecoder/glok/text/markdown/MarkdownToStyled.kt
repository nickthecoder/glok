package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.*

fun markdownToStyled(
    lines: List<String>,
    document: StyledTextDocument,
    config: MarkdownToStyledConfig = MarkdownToStyledConfig()
) {

    val ranges = mutableListOf<HighlightRange>()

    val flow = object : Flow() {

        override val columns: Int
            get() = config.columns

        override fun append(text: String) {
            document.append(text)
        }

        override fun appendNewLine() {
            document.append("\n")
        }

        override fun endPosition() = document.endPosition()
        override fun addRange(link: String?, from: TextPosition, to: TextPosition, highlight: Highlight) {
            val range = if (link == null) {
                HighlightRange(from, to, highlight)
            } else {
                LinkHighlightRange(from, to, link, highlight)
            }
            ranges.add(range)
        }

    }

    val intermediateDocument = MarkdownToIntermediate(document, config, lines).parse()
    //intermediateDocument.dump(0)

    document.clear()
    intermediateDocument.render(flow)
    document.ranges.addAll(ranges)
    document.history.clear()

}

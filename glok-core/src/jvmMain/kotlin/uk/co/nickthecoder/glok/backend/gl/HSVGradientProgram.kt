/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL20.glEnableVertexAttribArray

internal class HSVGradientProgram : ShaderProgram(VERTEX, FRAGMENT, 6) {

    val colorLocation = getAttributeLocation("color")

    fun setup() {
        vertexAttribute(positionLocation, 2, 6 * java.lang.Float.BYTES, 0)
        OpenGL.reportError()
        vertexAttribute(colorLocation, 4, 6 * java.lang.Float.BYTES, 2L * java.lang.Float.BYTES)
        OpenGL.reportError()
        glEnableVertexAttribArray(positionLocation)
        OpenGL.reportError()
        glEnableVertexAttribArray(colorLocation)
        OpenGL.reportError()
    }

    fun gradient(triangles: FloatArray, colors: Array<FloatArray>) {
        requiredTriangles(colors.size/3)
        for (i in colors.indices) {
            val color = colors[i]
            GLBackend.floatBuffer
                .put(triangles[i * 2]).put(triangles[i * 2 + 1])
                .put(color[0]).put(color[1]).put(color[2]).put(color[3])
        }
    }


    companion object {
        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute vec4 color;

            uniform mat3 viewMatrix;
            
            varying vec4 hsva;

            void main() {
                hsva = color;

                gl_Position = vec4( viewMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val FRAGMENT = """
            #version 120
            
            vec3 HUEtoRGB(in float H)
            {
                float R = abs(H * 6 - 3) - 1;
                float G = 2 - abs(H * 6 - 2);
                float B = 2 - abs(H * 6 - 4);
                return vec3( clamp(R,0,1),clamp(G,0,1),clamp(B,0,1) );
            }
            
            vec3 HSVtoRGB(in vec3 HSV)
            {
                vec3 RGB = HUEtoRGB(HSV.x);
                return ((RGB - 1) * HSV.y + 1) * HSV.z;
            }
            
            varying vec4 hsva;

            void main() {
                gl_FragColor = vec4( HSVtoRGB( hsva.xyz ), hsva.w );
            }

            """.trimIndent()
    }
}

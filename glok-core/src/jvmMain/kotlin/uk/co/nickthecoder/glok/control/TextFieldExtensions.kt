/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.boilerplate.FileProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleFileProperty
import uk.co.nickthecoder.glok.property.functions.FileToString
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.FIELD_WITH_BUTTONS
import uk.co.nickthecoder.glok.util.homeDirectory
import java.io.File


fun folderButton(title: String, prop: FileProperty) = button("…") {
    style(".folder_button")
    onAction {
        FileDialog().apply {
            this.title = title
            initialDirectory = if (prop.value.exists()) prop.value else homeDirectory

            showFolderPicker(scene !!.stage !!) { dir ->
                dir?.let { prop.value = dir }
            }
        }
    }
}

fun fileButton(
    title: String,
    prop: FileProperty,
    extensionFilters: List<ExtensionFilter> = emptyList()

) = button("…") {
    style(".file_button")
    onAction {
        FileDialog().apply {
            this.title = title
            initialDirectory = prop.value.parentFile ?: homeDirectory
            extensions.clear()
            extensions.addAll(extensionFilters)

            showOpenDialog(scene !!.stage !!) { file ->
                file?.let { prop.value = file }
            }
        }
    }
}

fun TextField.acceptDroppedFile() {
    onFilesDropped { event ->
        if (event.paths.size == 1) {
            text = event.paths.first()
        }
    }
}

fun TextField.withFileButton(title: String, block: (HBox.() -> Unit)? = null) = hBox {
    style( FIELD_WITH_BUTTONS )
    val fileProperty = SimpleFileProperty(FileToString.backwards(text))
    fileProperty.bidirectionalBind(textProperty, FileToString)

    + this@withFileButton
    + fileButton(title, fileProperty)

    block?.invoke(this)
}

fun TextField.withFolderButton(title: String, block: (HBox.() -> Unit)? = null) = hBox {
    style( FIELD_WITH_BUTTONS )
    val fileProperty = SimpleFileProperty(FileToString.backwards(text))
    fileProperty.bidirectionalBind(textProperty, FileToString)

    + this@withFolderButton
    + folderButton(title, fileProperty)

    block?.invoke(this)
}

fun TextField.withFolderAndFileButtons(title: String, block: (HBox.() -> Unit)? = null) = hBox {
    style( FIELD_WITH_BUTTONS )
    val fileProperty = SimpleFileProperty(FileToString.backwards(text))
    fileProperty.bidirectionalBind(textProperty, FileToString)
    + this@withFolderAndFileButtons
    + fileButton(title, fileProperty)
    + folderButton(title, fileProperty)
    block?.invoke(this)
}

object FileCompletionActions : Actions(null) {
    /**
     * Down arrow key to complete a filename (similar to the `Tab` in Bash).
     *
     * Using the tab key would be impractical, because tab moves the focus to the next Node.
     */
    val COMPLETE = define("complete", "Complete", Key.DOWN.noMods())

    /**
     * Move up to the parent folder. Similar to `Escape Backspace` in Bash.
     *
     * Using `Escape Backspace` isn't possible, as Glok doesn't support multiple key presses as shortcuts.
     * Also, the application may well use `Escape` for its own purposes.
     */
    val PARENT = define("parent_folder", "Parent Folder", Key.UP.noMods())
}

/**
 * Adds additional shortcuts which perform file-completion.
 *
 * The shortcut keys are defined in [FileCompletionActions].
 */
fun TextField.addFileCompletionActions(foldersOnly: Boolean = false) {

    onKeyPressed(HandlerCombination.BEFORE) { event ->
        if (FileCompletionActions.COMPLETE.matches(event)) {
            performFileCompletion(this, foldersOnly)
            event.consume()
        }
        if (FileCompletionActions.PARENT.matches(event)) {
            fileParent(this)
            event.consume()
        }
    }

}

private fun fileParent(textField: TextField) {
    val parent = File(textField.text).parentFile ?: return
    textField.setText(parent)
}


private fun TextField.setText(file: File) {
    val path = file.path
    text = if (file.isDirectory && path != "/") {
        path + File.separatorChar
    } else {
        path
    }
    caretIndex = text.length
    anchorIndex = caretIndex
    requestFocus()
}

/**
 * Used by [performFileCompletion]. Treats the text as a file path which hasn't been fully typed yet.
 * Looks for all files/folders which match what has already been typed.
 * If there is exactly 1 match, then the text is completed with that match.
 * If there are 0 matches, then we do nothing.
 * If there are more than 1 match, then show a pop-up menu displaying the possible options.
 * Clicking one sets the text to that path.
 *
 * When the completed file is a folder, then we include a trailing / ([File.separator]).
 * This isn't usual in Java, but is allowed, and it makes much more sense!
 * (As the user can then type, or use the shortcut to choose the next completion).
 */
private fun performFileCompletion(textField: TextField, foldersOnly: Boolean) {
    val text = textField.text
    val full = File(text)
    val folder = if (text.endsWith(File.separator)) full else full.parentFile ?: full
    var folderPath = folder.path
    if (! folderPath.endsWith(File.separatorChar)) {
        folderPath += File.separator
    }
    if (! folder.exists()) return
    val prefix = if (text.endsWith(File.separator)) "" else full.name
    val includeHidden = prefix.startsWith('.')

    // Don't include hidden files, unless the user has already typed a "."
    // This isn't the convention for windows, but I don't think it matters???
    val matching = folder.listFiles { childFile ->
        val name = childFile.name
        (! childFile.isHidden || includeHidden) && name.startsWith(prefix) &&
            (! foldersOnly || childFile.isDirectory)
    } ?: return

    when (matching.size) {
        0 -> return
        1 -> textField.setText(matching.first())
        else -> {
            popupMenu {
                for (match in matching.sortedBy { it.name.uppercase() }) {
                    + menuItem(match.name) {
                        onAction {
                            textField.setText(match)
                        }
                    }
                }
            }.show(textField, Side.BOTTOM)
        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL11
import org.lwjgl.stb.STBImage
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.glok.backend.Resources
import uk.co.nickthecoder.glok.backend.Texture
import java.io.File
import java.io.IOException

/**
 * Loads textures from files.
 */
internal class GLFileResources(val directory: File) : Resources {

    constructor(path: String) : this(File(path))

    override fun loadTexture(name: String): Texture {
        val file = File(directory, name)

        MemoryStack.stackPush().use { stack ->

            val cWidth = stack.mallocInt(1)
            val cHeight = stack.mallocInt(1)
            val cChannels = stack.mallocInt(1)

            // STBImage.stbi_set_flip_vertically_on_load(true)
            val buffer = STBImage.stbi_load(file.path, cWidth, cHeight, cChannels, 4)
            buffer ?: throw IOException("Failed to load texture from ${file.absoluteFile}")
            val width = cWidth.get()
            val height = cHeight.get()
            val texture = GLTexture(file.name, width, height)
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.handle)
            GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST)
            GL11.glTexImage2D(
                GL11.GL_TEXTURE_2D,
                0,
                GL11.GL_RGBA,
                width,
                height,
                0,
                GL11.GL_RGBA,
                GL11.GL_UNSIGNED_BYTE,
                buffer
            )
            MemoryUtil.memFree(buffer)
            return texture
        }
    }
}

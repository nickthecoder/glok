package uk.co.nickthecoder.glok.text.markdown

internal class MarkdownRow(val table: MarkdownTable) {

    val cells = mutableListOf<MarkdownCell>()
    var isHeading = false

    fun addCell(str: String) {
        cells.add(MarkdownCell(this, str))
    }

    fun rowWasHeading() {
        isHeading = true
    }
}

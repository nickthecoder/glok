package uk.co.nickthecoder.glok.backend.gl

import uk.co.nickthecoder.glok.scene.Color

internal class StrokeCircleProgram : ShaderProgram(VERTEX, FRAGMENT, 4) {

    private val colorLocation = getUniformLocation("color")
    private val radiusLocation = getUniformLocation("radius")
    private val innerRadiusLocation = getUniformLocation("innerRadius")

    fun setup(color: Color, radius: Float, innerRadius: Float = 0f) {
        OpenGL.reportError()
        enablePositionUV()
        OpenGL.reportError()
        setUniform(colorLocation, color)
        OpenGL.reportError()
        setUniform(radiusLocation, radius)
        OpenGL.reportError()
        setUniform(innerRadiusLocation, innerRadius)
        OpenGL.reportError()
    }

    fun setupAndStroke(radius: Float, innerRadius: Float, color: Color, x1: Float, y1: Float, x2: Float, y2: Float) {
        setup(color, radius, innerRadius)

        requiredTriangles(2)
        with(GLBackend.floatBuffer) {
            put(x1).put(y1).put(0f).put(0f)
            put(x2).put(y1).put(radius).put(0f)
            put(x2).put(y2).put(radius).put(radius)

            put(x1).put(y1).put(0f).put(0f)
            put(x1).put(y2).put(0f).put(radius)
            put(x2).put(y2).put(radius).put(radius)
        }
        OpenGL.reportError()
    }

    companion object {

        private val VERTEX = """
            #version 120

            uniform mat3 viewMatrix;
            attribute vec2 position;
            attribute vec2 uv;
            
            varying vec2 local;
            
            void main() {
                local = uv;
                gl_Position = vec4( viewMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val FRAGMENT = """
            #version 120
            
            varying vec2 local;
            
            uniform vec4 color;
            uniform float radius;
            uniform float innerRadius;

            void main() {
                float dist2 = local.x * local.x + local.y * local.y;
                float alpha = clamp(0, 1, 1 - (dist2 - radius * radius)) *
                    clamp(0, 1, (dist2 - innerRadius * innerRadius));
                
                gl_FragColor = vec4( color.xyz, color.w * alpha );
            }
            """.trimIndent()
    }

}

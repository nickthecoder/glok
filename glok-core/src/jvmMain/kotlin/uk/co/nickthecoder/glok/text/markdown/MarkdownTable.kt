package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.StyledTextDocument

internal class MarkdownTable(

    val document: StyledTextDocument,
    val config: MarkdownToStyledConfig,
    headingsString: String

) : Element {

    val rows = mutableListOf<MarkdownRow>()

    init {
        addRow(headingsString)
    }

    fun addRow(rowString: String) {

        val cellStrings = rowString.split(Regex("\\s*\\|\\s*"))

        val newRow = MarkdownRow(this)
        rows.add(newRow)

        for (cellString in cellStrings) {
            newRow.addCell(cellString.trim())
        }
    }

    fun rowWasHeading() {
        rows.lastOrNull()?.rowWasHeading()
    }

    fun render(parentFlow: Flow) {

        /**
         * The position of this row within the document.
         * This is used when converting [MarkdownCell.ranges].
         */
        var documentRow = document.lines.size - 1

        var columnCount = 0
        for (row in rows) {
            val rowCellCount = row.cells.size
            if (rowCellCount > columnCount) columnCount = rowCellCount
        }

        // 12345678901234567890 // config.columns = 20
        // Each cell need 3 extra columns (2 for spaces and 1 for the column divider).
        // We want the columns to be 6 wide, with the last having 7.
        // | 123    | abc     |
        val defaultCellWidth = (config.columns - 1 - columnCount * 3) / columnCount
        val lastCellWidth = defaultCellWidth + config.columns - 1 - (defaultCellWidth + 3) * columnCount
        val columnsWidths = Array(columnCount) { if (it == columnCount - 1) lastCellWidth else defaultCellWidth }

        // First pass, render the cells with the default width
        for (row in rows) {
            for ((columnIndex, cell) in row.cells.withIndex()) {
                cell.render(columnsWidths[columnIndex])
            }
        }

        val newColumnsWidths = Array(columnCount) { - 1 }

        // Calculate the required widths of each column
        for (row in rows) {
            for ((columnIndex, cell) in row.cells.withIndex()) {
                if (cell.lines.size == 1) {
                    val minCellWidth = cell.lines.first().trim().length
                    if (minCellWidth > newColumnsWidths[columnIndex]) {
                        newColumnsWidths[columnIndex] = minCellWidth
                    }
                } else {
                    newColumnsWidths[columnIndex] = cell.cellWidth
                }
            }
        }

        var stretchColumnCount = 0
        var totalShrink = 0
        for (i in 0 until columnCount) {
            if (newColumnsWidths[i] != columnsWidths[i]) {
                totalShrink += columnsWidths[i] - newColumnsWidths[i]
            } else {
                stretchColumnCount ++
            }
        }
        val growBy = if (stretchColumnCount == 0) 0 else totalShrink / stretchColumnCount

        if (totalShrink > 0) {
            // We can stretch some columns, and shrink others.
            for (columnIndex in 0 until columnCount) {
                if (newColumnsWidths[columnIndex] < columnsWidths[columnIndex]) {
                    // shrink this cell
                    columnsWidths[columnIndex] = newColumnsWidths[columnIndex]
                } else {
                    // Stretch
                    columnsWidths[columnIndex] += growBy
                }
            }

            for (row in rows) {
                for ((columnIndex, cell) in row.cells.withIndex()) {
                    cell.render(columnsWidths[columnIndex])
                }
            }
        }

        // Top of the table
        StringBuilder().apply {
            append("┌─")
            for (i in 0 until columnCount - 1) {
                append("─".repeat(columnsWidths[i]))
                append("─┬─")
            }
            append("─".repeat(columnsWidths.last()))
            append("─┐\n")
            document.append(this.toString())
            documentRow ++
        }

        for (row in rows) {

            // Calculate the number of lines in this row.
            var rowLines = 0
            for (cell in row.cells) {
                val cellLines = cell.lines.size
                if (rowLines < cellLines) rowLines = cellLines
            }

            for (lineIndex in 0 until rowLines) {
                val fullLine = StringBuilder()
                fullLine.append("│")

                for (cell in row.cells) {

                    val line = if (cell.lines.size - 1 < lineIndex) "" else cell.lines[lineIndex]

                    fullLine.append(" ")
                    fullLine.append(line)

                    val extra = cell.cellWidth - line.length
                    if (extra > 0) {
                        fullLine.append(" ".repeat(extra))
                    }
                    fullLine.append(" │")
                }

                fullLine.append("\n")
                document.append(fullLine.toString())
            }

            // Convert the Cell's ranges, and add them to the document.
            var columnDelta = 2
            for (cell in row.cells) {
                for (range in cell.ranges) {
                    val from = TextPosition(range.from.row + documentRow, range.from.column + columnDelta)
                    val to = TextPosition(range.to.row + documentRow, range.to.column + columnDelta)
                    val link = (range as? LinkHighlightRange)?.link
                    parentFlow.addRange(link, from, to, range.highlight)
                    //println("Was : $range")
                    //println("Now : $correctedRange")
                }
                columnDelta += cell.cellWidth + 3 // 3 = length of " | "
            }

            if (row !== rows.last()) {
                // Line between each cell
                StringBuilder().apply {
                    append("├─")
                    for (i in 0 until columnCount - 1) {
                        append("─".repeat(columnsWidths[i]))
                        append("─┼─")
                    }
                    append("─".repeat(columnsWidths.last()))
                    append("─┤\n")
                    document.append(this.toString())
                }
                documentRow ++
            }

            documentRow += rowLines
        }

        // Bottom of the table
        StringBuilder().apply {
            append("└─")
            for (i in 0 until columnCount - 1) {
                append("─".repeat(columnsWidths[i]))
                append("─┴─")
            }
            append("─".repeat(columnsWidths.last()))
            append("─┘\n")
            document.append(this.toString())
        }

    }
}


package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.text.StyledTextDocument


internal class IntermediateDocument(

    document: StyledTextDocument,
    config: MarkdownToStyledConfig

) : Block(document, config) {

    val attributes = mutableMapOf<String, String>()

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import org.lwjgl.PointerBuffer
import org.lwjgl.util.nfd.NativeFileDialog
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.dialog.promptDialog
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.scene.stage
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.util.*
import java.io.File
import java.text.DateFormat
import java.util.*

/**
 * `File Open`, `File Save` and `Folder Picker` dialogs.
 *
 * Note. There is an option to use `native` controls, or `Glok` controls.
 * (See `native` boolean parameter of the constructor).
 *
 * Initially, only `native` controls were possible, but I added `Glok` controls,
 * and made this the default because :
 *
 * 1. The native library seems to have a critical bug when running on Linux.
 *    It very occasionally crashes the application :-(
 *    (This library is part of LWJGL - which provide all the OpenGL wrappers and other goodies).
 * 2. The native dialogs really annoy me - they ignore the [initialDirectory]
 *    I've read somewhere that this is deliberate, because users were confused
 *    by this feature. WTF? So now I have to navigate to the same folder
 *    again and again just to Save-As with a new name? No, that's madness!
 * 3. The native library's API is dreadful, and hints that it was written
 *    by a very naive programmer.
 *
 * The downsides of the Glok controls :
 *
 * 1. Looks and behaves differently from the native controls
 * 2. Doesn't include `favourite` places.
 * 3. Doesn't include file previews
 * 4. There may be other features in the native controls too.
 *
 * But has Glok's version have some advantages too :
 *
 * 1. Uses the same Theme as your application.
 * 2. Allows you to browse a folder using the file-manager (via a right-click menu).
 * 3. They match the theme of your application.
 *
 * The default value of [native] (in the primary constructor) is taken from [GlokSettings.nativeFileDialogs].
 * Therefore, you can easily switch between native and glok from a single place.
 */
class FileDialog(val native: Boolean = GlokSettings.nativeFileDialogs) {

    constructor() : this(false)

    // region ==== Properties ====

    val titleProperty by stringProperty("")
    var title by titleProperty

    val initialDirectoryProperty by fileProperty(homeDirectory)
    var initialDirectory by initialDirectoryProperty
    // endregion

    /**
     * Pairs of file type descriptions and file extensions.
     * Each pair comprises a description (String) and a spec (String) with comma separated file extensions.
     *
     * e.g. one item of this list may be :
     *
     *     Pair("C Source code", "c,cpp,h" ) )
     *
     * NOTE. Currently only the "spec" (the second part of the pair) is used, and the descriptions are ignored.
     * Version 3.3.1 of LWJGL does not support the description, but later versions do.
     * However, I've chosen not to use the later versions (yet) because the API is IMHO, badly screwed up!
     */
    val extensions = mutableListOf<ExtensionFilter>().asMutableObservableList()

    /**
     *
     * @param parentStage Not currently used, but is intended to give an "owner" to the popup.
     * However, later versions of Glok may support a non-native file open dialog as an OverlayStage within
     * a RegularStage (either parentStage, or its ancestor).
     */
    fun showOpenDialog(parentStage: Stage, callback: (File?) -> Unit) {
        if (extensions.isEmpty()) {
            extensions.add(ExtensionFilter("All Files", "*"))
        }
        if (native) {
            showNativeOpenDialog(callback)
        } else {
            showGlokFileDialog(parentStage, false, callback)
        }
    }

    /**
     *
     * @param parentStage Not currently used, but is intended to give an "owner" to the popup.
     * However, later versions of Glok may support a non-native file save dialog as an OverlayStage within
     * a RegularStage (either parentStage, or its ancestor).
     */
    fun showSaveDialog(parentStage: Stage, callback: (File?) -> Unit) {
        if (native) {
            showNativeSaveDialog(callback)
        } else {
            showGlokFileDialog(parentStage, true, callback)
        }
    }

    fun showFolderPicker(parentStage: Stage, callback: (File?) -> Unit) {
        if (native) {
            showNativeFolderPicker(callback)
        } else {
            showGlokFolderDialog(parentStage, callback)
        }
    }

    /**
     * Used for [showOpenDialog] and [showSaveDialog].
     * Note, much of the code is very similar to [showGlokFolderDialog].
     */
    private fun showGlokFileDialog(parentStage: Stage, save: Boolean, callback: (File?) -> Unit) {

        val filePicker = FilePickerPanel(save, initialDirectory, extensions)

        stage(parentStage) {
            title = this@FileDialog.title

            scene = scene(800, 600) {
                root = filePicker
            }
            onClosed {
                callback(filePicker.file)
            }
            show()
            filePicker.listView.requestFocus()
        }
    }

    /**
     * Used for [showFolderPicker].
     * Note, much of the code is very similar to [showGlokFileDialog].
     */
    private fun showGlokFolderDialog(parentStage: Stage, callback: (File?) -> Unit) {

        val folderPicker = FolderPickerPanel(initialDirectory)

        stage(parentStage) {
            title = this@FileDialog.title

            scene = scene(600, 500) {
                root = folderPicker
            }
            onClosed {
                callback(folderPicker.file)
            }
            show()
            folderPicker.listView.requestFocus()
        }
    }

    private fun showNativeOpenDialog(callback: (File?) -> Unit) {
        val outPath = PointerBuffer.allocateDirect(1)
        val filters = extensions.joinToString(separator = ";") { it.toCommaSeparatedString() }
        var file: File? = null
        if (NativeFileDialog.NFD_OpenDialog(filters, initialDirectory.path, outPath) == NativeFileDialog.NFD_OKAY) {
            file = File(outPath.stringUTF8)
            // NativeFileDialog says that outPath must be freed, but the method they supply does not accept
            // a PointBuffer !?!
            // This seems to work, but there's no documentation nor example code. grr.
            // IMHO, the NativeFileDialog API version 3.3.1 is badly screwed up!
            // Versions 3.3.2 and 3.3.3 are even worse!
            // e.g. The return value is in a PointerBuffer, but the API for PointerBuffer implies it is used to
            // store an ARRAY of pointers.
            // This explains why my solution uses outPath.get(0) - we are "pretending" to get the first element of an array.
            // This is further screwed up by NFD_OpenDialogMultiple which does NOT return a PointerBuffer !?!
            NativeFileDialog.nNFD_Free(outPath.get(0))
        }
        callback(file)
    }

    /**
     *
     * @param parentStage Not currently used, but is intended to give an "owner" to the popup.
     * However, later versions of Glok may support a non-native file save dialog as an OverlayStage within
     * a RegularStage (either parentStage, or its ancestor).
     */
    private fun showNativeSaveDialog(callback: (File?) -> Unit) {
        val outPath = PointerBuffer.allocateDirect(1)
        val filters = extensions.joinToString(separator = ";") { it.toCommaSeparatedString() }
        var file: File? = null
        if (NativeFileDialog.NFD_SaveDialog(filters, initialDirectory.path, outPath) == NativeFileDialog.NFD_OKAY) {
            file = File(outPath.stringUTF8)
            // NativeFileDialog says that outPath must be freed, but the method they supply does not accept
            // a PointBuffer !?!
            NativeFileDialog.nNFD_Free(outPath.get(0))
        }
        callback(file)
    }

    private fun showNativeFolderPicker(callback: (File?) -> Unit) {
        val outPath = PointerBuffer.allocateDirect(1)
        var file: File? = null
        if (NativeFileDialog.NFD_PickFolder(initialDirectory.path, outPath) == NativeFileDialog.NFD_OKAY) {
            file = File(outPath.stringUTF8)
            // NativeFileDialog says that outPath must be freed, but the method they supply does not accept
            // a PointBuffer !?!
            NativeFileDialog.nNFD_Free(outPath.get(0))
        }
        callback(file)

    }
}

private class FilePickerPanel(

    val save: Boolean,
    initialDirectory: File,
    val extensions: List<ExtensionFilter>

) : WrappedNode<BorderPane>(BorderPane()) {

    // region ==== Properties ====
    val fileProperty by optionalFileProperty(null)
    var file by fileProperty

    private val parentFolderProperty = SimpleFileProperty(initialDirectory)
    private val extensionFilterProperty = SimpleProperty(extensions.first())
    private val showHiddenProperty = SimpleBooleanProperty(false)
    private val parentFolderAsTextFieldProperty = SimpleBooleanProperty(false)

    // endregion Properties

    // region ==== Nodes ====
    private val filenameField = textField {
    }

    private val fileFilter = choiceBox<ExtensionFilter> {
        items.addAll(extensions)
        selection.selectedItemProperty.bidirectionalBind(
            extensionFilterProperty,
            DefaultValueConverter(extensions.first())
        )
    }

    val listView = listView<File> {
        cellFactory = { listView, item ->
            val sizeStr = if (item.isDirectory) "" else item.length().toMemorySize()
            val hBox = hBox {
                fillHeight = true
                spacing(10)
                padding(2, 4)
                + label(item.name) {
                    graphic = ImageView(Tantalum.smallIcons[if (item.isFile) "file" else "folder"])
                    growPriority = 1f
                }
                + label(sizeStr) {
                    alignment = Alignment.CENTER_RIGHT
                    overridePrefWidth = 120f
                }
                + label(item.lastModified().lastModifiedString()) {
                    alignment = Alignment.CENTER_RIGHT
                    overridePrefWidth = 120f
                }
            }
            SingleNodeListCell(listView, item, hBox).apply {
                onMouseClicked { event ->
                    if (event.clickCount == 1 && save && item.isFile) {
                        filenameField.text = item.name
                    }
                    if (event.clickCount == 2) {
                        if (item.isDirectory) {
                            parentFolderProperty.value = item
                        } else {
                            file = item
                            scene?.stage?.close()
                        }
                    }
                }
                onPopupTrigger {
                    if (item.isDirectory) {
                        popupMenu {
                            + menuItem("Browse...") {
                                onAction { openFileInExternalApp(item) }
                            }
                        }.show(this)
                    }
                }
            }
        }
    }
    // endregion Nodes

    // region ==== Listeners ====
    @Suppress("unused")
    private val filterListener = fileFilter.selection.selectedItemProperty.addListener { updateList() }

    @Suppress("unused")
    private val updateListListener = parentFolderProperty.addListener {
        updateList()
    }.apply {
        extensionFilterProperty.addListener(this)
        showHiddenProperty.addListener(this)
    }
    // endregion Listeners

    init {
        createGUI()
        updateList()

    }

    private fun createGUI() {
        inner.apply {
            onKeyPressed { event ->
                if (event.key == Key.L && event.isControlDown) {
                    parentFolderAsTextFieldProperty.value = ! parentFolderAsTextFieldProperty.value
                }
            }
            top = FolderBar(parentFolderProperty.value).apply {
                folderProperty.bidirectionalBind(parentFolderProperty)
                asTextFieldProperty.bidirectionalBind(parentFolderAsTextFieldProperty)
            }

            center = if (save) {
                vBox {
                    section = true
                    fillWidth = true
                    spacing(10)
                    + listView.apply {
                        growPriority = 1f
                    }
                    + hBox {
                        fillHeight = true
                        spacing(10)
                        + label("Name ")
                        + filenameField.apply {
                            growPriority = 1f
                        }
                    }
                }
            } else {
                listView.apply {
                    section = true
                }
            }

            bottom = vBox {
                fillWidth = true
                + buttonBar {
                    add(ButtonMeaning.RIGHT, fileFilter)
                    add(ButtonMeaning.LEFT, checkBox("Show Hidden Files") {
                        selectedProperty.bidirectionalBind(showHiddenProperty)
                    })
                }
                + buttonBar {
                    add(ButtonMeaning.OK, button(if (save) "Save" else "Open") {
                        defaultButton = true
                        onAction {
                            if (save) {
                                if (filenameField.text.isNotBlank()) {
                                    file = File(parentFolderProperty.value, filenameField.text)
                                    scene?.stage?.close()
                                }
                            } else {
                                listView.selection.selectedItem?.let {
                                    file = it
                                    scene?.stage?.close()
                                }
                            }
                        }
                    })
                    add(ButtonMeaning.CANCEL, button("Cancel") {
                        cancelButton = true
                        onAction { scene?.stage?.close() }
                    })

                    if (save) {
                        add(ButtonMeaning.LEFT, button("Create Folder") {
                            onAction {
                                promptDialog {
                                    title = "New Folder"
                                    show(scene !!.stage !!) { name ->
                                        val newFolder = File(parentFolderProperty.value, name)
                                        try {
                                            newFolder.mkdir()
                                            parentFolderProperty.value = newFolder
                                        } catch (e: Exception) {
                                            // Do nothing
                                        }
                                    }
                                }
                            }
                        })
                    }

                }
            }
        }
    }

    private fun updateList() {
        listView.items.clear()
        val all = if (showHiddenProperty.value) {
            parentFolderProperty.value.listFiles()
        } else {
            parentFolderProperty.value.listFiles { file: File -> ! file.isHidden }
        }
        val directories = all?.filter { it.isDirectory }?.sortedBy { it.name.uppercase() }
        val files =
            all?.filter { it.isFile && extensionFilterProperty.value.accept(it) }?.sortedBy { it.name.uppercase() }

        if (directories != null) listView.items.addAll(directories)
        if (files != null) listView.items.addAll(files)
    }

}

private class FolderPickerPanel(
    initialDirectory: File
) : WrappedNode<BorderPane>(BorderPane()) {

    // region ==== Properties ====
    val fileProperty by optionalFileProperty(null)
    var file by fileProperty

    private val parentFolderProperty = SimpleFileProperty(initialDirectory)
    private val showHiddenProperty = SimpleBooleanProperty(false)
    private val parentFolderAsTextFieldProperty = SimpleBooleanProperty(false)

    // endregion Properties

    // region ==== Listeners ====
    val listView = listView<File> {
        section = true
        cellFactory = { listView, item ->
            val hBox = hBox {
                fillHeight = true
                spacing(10)
                + label(item.name) {
                    graphic = ImageView(Tantalum.smallIcons[if (item.isFile) "file" else "folder"])
                    growPriority = 1f
                }
                + label(item.lastModified().lastModifiedString()) {
                    alignment = Alignment.CENTER_RIGHT
                    overridePrefWidth = 120f
                }
            }
            SingleNodeListCell(listView, item, hBox).apply {
                padding(4, 6)
                onMouseClicked { event ->
                    if (event.clickCount == 2) {
                        parentFolderProperty.value = item
                    }
                }
                onPopupTrigger {
                    if (item.isDirectory) {
                        popupMenu {
                            + menuItem("Select") {
                                onAction {
                                    file = item
                                    listView.scene?.stage?.close()
                                }
                            }
                            + menuItem("Browse...") {
                                onAction { openFileInExternalApp(item) }
                            }
                        }.show(this)
                    }
                }
            }
        }
    }

    @Suppress("unused")
    private val updateListListener = parentFolderProperty.addListener {
        updateList()
    }.apply {
        showHiddenProperty.addListener(this)
    }
    // endregion Listeners

    init {
        createGUI()
        updateList()
    }

    private fun createGUI() {
        inner.apply {
            onKeyPressed { event ->
                if (event.key == Key.L && event.isControlDown) {
                    parentFolderAsTextFieldProperty.value = ! parentFolderAsTextFieldProperty.value
                }
            }
            top = FolderBar(parentFolderProperty.value).apply {
                folderProperty.bidirectionalBind(parentFolderProperty)
                asTextFieldProperty.bidirectionalBind(parentFolderAsTextFieldProperty)
            }

            center = listView

            bottom = vBox {
                fillWidth = true
                + buttonBar {
                    add(ButtonMeaning.LEFT, checkBox("Show Hidden Folders") {
                        selectedProperty.bidirectionalBind(showHiddenProperty)
                    })
                }
                + buttonBar {
                    add(ButtonMeaning.OK, button("Select") {
                        defaultButton = true
                        onAction {
                            file = listView.selection.selectedItem
                                ?: parentFolderProperty.value
                            scene?.stage?.close()
                        }
                    })
                    add(ButtonMeaning.CANCEL, button("Cancel") {
                        cancelButton = true
                        onAction { scene?.stage?.close() }
                    })

                    add(ButtonMeaning.LEFT, button("Create Folder") {
                        onAction {
                            promptDialog {
                                title = "New Folder"
                                show(scene !!.stage !!) { name ->
                                    val newFolder = File(parentFolderProperty.value, name)
                                    try {
                                        newFolder.mkdir()
                                        parentFolderProperty.value = newFolder
                                    } catch (e: Exception) {
                                        // Do nothing
                                    }
                                }
                            }
                        }
                    })

                }
            }
        }
    }

    private fun updateList() {
        listView.items.clear()
        val directories = parentFolderProperty.value.listFiles { file: File -> file.isDirectory } ?: emptyArray()
        if (showHiddenProperty.value) {
            listView.items.addAll(directories.sorted())
        } else {
            listView.items.addAll(directories.filter { ! it.isHidden }.sorted())
        }
    }

}

internal fun Long.lastModifiedString(): String {
    val now = currentTimeMillis()
    return if (now - this < 24 * 60 * 60 * 1000L) {
        DateFormat.getTimeInstance(DateFormat.SHORT).format(Date(this))
    } else {
        DateFormat.getDateInstance(DateFormat.MEDIUM).format(Date(this))
    }
}

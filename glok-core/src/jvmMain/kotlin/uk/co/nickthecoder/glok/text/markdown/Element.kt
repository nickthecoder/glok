package uk.co.nickthecoder.glok.text.markdown

internal interface Element {
    fun dump(indent: Int) {
        println(" ".repeat(indent * 4) + toString())
    }
}


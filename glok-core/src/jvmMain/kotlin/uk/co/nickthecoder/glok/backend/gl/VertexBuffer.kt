package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL15.*
import java.nio.FloatBuffer

internal class VertexBuffer {

    val handle: Int = glGenBuffers()

    fun bind() {
        glBindBuffer(GL_ARRAY_BUFFER, handle)
    }

    fun unbind() {
        glBindBuffer(GL_ARRAY_BUFFER, 0)
    }

    fun delete() {
        glDeleteBuffers(handle)
    }

    fun uploadData(size: Long) {
        GLBackend.vertexBuffer.bind()
        glBufferData(GL_ARRAY_BUFFER, size, GL_DYNAMIC_DRAW)
        GLBackend.vertexBuffer.unbind()
    }

    fun uploadData(data: FloatBuffer) {
        glBufferData(GL_ARRAY_BUFFER, data, GL_DYNAMIC_DRAW)
    }

}

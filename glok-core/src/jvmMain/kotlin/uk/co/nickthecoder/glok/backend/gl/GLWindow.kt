package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.BufferUtils
import org.lwjgl.glfw.Callbacks
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWImage
import org.lwjgl.opengl.GL11.*
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.glok.backend.Window
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.WindowListener
import uk.co.nickthecoder.glok.util.ignoreErrors
import uk.co.nickthecoder.glok.util.log
import java.nio.ByteBuffer

internal class GLWindow(
    initialWidth: Int, initialHeight: Int, shareWith: Long
) : Window {

    internal val handle: Long
    private var closed = false

    override var windowListener: WindowListener? = null

    var width: Int = initialWidth
        private set

    var height: Int = initialHeight
        private set

    override var title: String = ""
        set(value) {
            field = value
            glfwSetWindowTitle(handle, value)
        }

    override var resizable: Boolean
        get() = glfwGetWindowAttrib(handle, GLFW_RESIZABLE) == GLFW_TRUE
        set(value) {
            glfwSetWindowAttrib(handle, GLFW_RESIZABLE, if (value) GLFW_TRUE else GLFW_FALSE)
        }

    override var maximized: Boolean
        get() = glfwGetWindowAttrib(handle, GLFW_MAXIMIZED) == GLFW_TRUE
        set(value) {
            //log.info("maximised = $value")
            if (value) {
                glfwMaximizeWindow(handle)
            } else {
                glfwRestoreWindow(handle)
            }
        }

    override var minimized: Boolean
        get() = glfwGetWindowAttrib(handle, GLFW_ICONIFIED) == GLFW_TRUE
        set(value) {
            log.info("minimised = $value")
            if (value) {
                glfwIconifyWindow(handle)
            } else {
                glfwRestoreWindow(handle)
            }
        }

    var mouseVisible: Boolean
        get() = glfwGetInputMode(handle, GLFW_CURSOR) == GLFW_CURSOR_NORMAL
        set(value) {
            log.info("mouseVisible = $value")
            glfwSetInputMode(handle, GLFW_CURSOR, if (value) GLFW_CURSOR_NORMAL else GLFW_CURSOR_HIDDEN)
        }

    override var shouldClose: Boolean
        get() = glfwWindowShouldClose(handle)
        set(value) {
            glfwSetWindowShouldClose(handle, value)
        }

    private val xBuffer = BufferUtils.createIntBuffer(1)
    private val yBuffer = BufferUtils.createIntBuffer(1)

    init {

        // Initially, the window should be hidden
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE)

        handle = glfwCreateWindow(initialWidth, initialHeight, "", MemoryUtil.NULL, shareWith)
        openWindowHandles.add(handle)

        glfwSetWindowCloseCallback(handle) {
            // log.info("Window $handle close request")
            windowListener?.close()
        }

        glfwSetWindowSizeCallback(handle) { _, _, _ ->
            MemoryStack.stackPush().use { stack ->
                val pWidth = stack.mallocInt(1)
                val pHeight = stack.mallocInt(1)
                glfwGetWindowSize(handle, pWidth, pHeight)
                width = pWidth.get()
                height = pHeight.get()

                windowListener?.resize(width, height)
            }
        }

        glfwSetWindowIconifyCallback(handle) { _, minimized ->
            windowListener?.minimize(minimized)
        }

        glfwSetWindowFocusCallback(handle) { _, focused ->
            windowListener?.focus(focused)
        }

        glfwSetKeyCallback(handle) { _, keyCode, scanCode, action, rawMods ->
            windowListener?.key(glToGlokKeyMapping[keyCode] ?: Key.UNKNOWN, action, scanCode, rawMods)
        }

        glfwSetCharCallback(handle) { _, codePoint: Int ->
            windowListener?.typed(codePoint)
        }

        glfwSetMouseButtonCallback(handle) { _, button, action, mods ->
            windowListener?.mouseButton(button, action == GLFW_PRESS, mods)
        }

        glfwSetCursorEnterCallback(handle) { _, entered ->
            windowListener?.mouseEnterExit(entered)
        }

        glfwSetCursorPosCallback(handle) { _, x, y ->
            windowListener?.mouseMove(x.toInt(), y.toInt())
        }

        glfwSetScrollCallback(handle) { _, deltaX, deltaY ->
            windowListener?.scroll(deltaX.toFloat(), deltaY.toFloat())
        }

        glfwSetDropCallback(handle) { _, count, args ->
            val paths = mutableListOf<String>()
            val pathPointers = MemoryUtil.memPointerBuffer(args, count)
            for (i in 0 until count) {
                paths.add(MemoryUtil.memUTF8(pathPointers.get(i)))
            }
            windowListener?.droppedFiles(paths)
        }
    }


    fun makeCurrent() {
        glfwMakeContextCurrent(handle)
    }

    override fun show() {
        makeCurrent()
        glfwSetWindowShouldClose(handle, false)
        glfwShowWindow(handle)
        glfwFocusWindow(handle)
        OpenGL.reportError()
    }

    override fun hide() {
        glfwHideWindow(handle)
        OpenGL.reportError()
    }

    override fun setIcon(images: List<Image>, tint: Color?) {
        val imageBuffer = GLFWImage.malloc(images.size)
        for ((index, image) in images.withIndex()) {
            val glfwImage = GLFWImage.malloc()
            val byteBuffer = (backend as GLBackend).imageToBytBufferRGBA(this, image, tint)
            glfwImage.set(image.imageWidth.toInt(), image.imageHeight.toInt(), byteBuffer)
            imageBuffer.put(index, glfwImage)
        }

        // macOS and Wayland don't support this, so we errors are EXPECTED.
        ignoreErrors {
            glfwSetWindowIcon(handle, imageBuffer)
        }

        for (glfwImage in imageBuffer) {
            // I *assume* we should free the images. i.e. I assume glfwSetWindowIcon make copies of the images.
            // The glfwSetWindowIcon docs do not help. Grr.
            glfwImage.free()
        }
        imageBuffer.free()
    }

    /**
     * According to the docs, you cannot move a window running Wayland.
     * So we ignore GL errors, as that is "expected behaviour".
     */
    override fun moveTo(x: Int, y: Int) {
        OpenGL.clearError()
        glfwSetWindowPos(handle, x, y)
        // Ignore the expected error from Wayland.
        OpenGL.clearError()
    }

    override fun resize(width: Int, height: Int) {
        OpenGL.clearError()
        glfwSetWindowSize(handle, width, height)
        OpenGL.reportError()
    }

    override fun size(): Pair<Int, Int> {
        OpenGL.clearError()
        glfwGetWindowSize(handle, xBuffer, yBuffer)
        OpenGL.reportError()
        return Pair(xBuffer.get(0), yBuffer.get(0))
    }

    override fun position(): Pair<Int, Int>? {
        OpenGL.clearError()
        glfwGetWindowPos(handle, xBuffer, yBuffer)
        OpenGL.onError {
            return null
        }
        return Pair(xBuffer.get(0), yBuffer.get(0))
    }

    fun flip() {
        OpenGL.clearError()
        glfwSwapBuffers(handle)
        OpenGL.reportError()
    }

    override fun closeNow() {
        closed = true
        openWindowHandles.remove(handle)
        Callbacks.glfwFreeCallbacks(handle)
        glfwDestroyWindow(handle)
    }


    override fun toString() = "GLWindow #$handle" + if (closed) " CLOSED" else ""

    companion object {
        /**
         * If more than one window is created, then we need to specify which windowHandle to share resources with.
         * So we keep a list of all open window handles, and pick the first.
         */
        private val openWindowHandles = mutableListOf<Long>()
    }

}

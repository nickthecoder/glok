package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.NodeInspector
import uk.co.nickthecoder.glok.control.SingleContainer
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

class MarkdownTester : Application() {

    val columnsProperty by intProperty(20)

    val markdownSource = styledTextArea(md)

    val styled = styledTextArea()
    val CONVERT = Key.S.control()

    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "Markdown Tester"
            scene = scene(1000, 800) {
                root = borderPane {
                    onKeyPressed { if (it.matches(CONVERT)) convert() }

                    top = toolBar {
                        + spacer()
                        + label("Columns : ")
                        + intSpinner {
                            editor.prefColumnCount = 3
                            valueProperty.bidirectionalBind(columnsProperty)
                        }
                        + button("Convert") {
                            onAction {
                                convert()
                            }
                        }
                        + spacer()
                        + button("Inspect") {
                            onAction { NodeInspector.showDialog(styled, primaryStage) }
                        }
                    }

                    center = splitPane {
                        + markdownSource
                        + styled
                    }
                }
            }
            convert()
            columnsProperty.addListener { convert() }
            show()
        }
    }

    private fun convert() {
        val config = MarkdownToStyledConfig().apply {
            columns = columnsProperty.value
        }
        markdownToStyled(markdownSource.document.lines, styled.document, config)
        // println("Ranges\n    " + styled.document.ranges.joinToString("\n    "))
    }

    companion object {

        val md = """

| Hello | World |
| ----- | ----- |
| abc   | 123   |

> Quoted
> word
> and more and more and this.
>> Double quoted
>> second **line**.
> And this is the last line
Back to plain text
"""

        @JvmStatic
        fun main(vararg args: String) {
            launch(MarkdownTester::class.java)
        }
    }
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL20.glEnableVertexAttribArray
import uk.co.nickthecoder.glok.scene.Color

internal class GradientProgram : ShaderProgram(VERTEX, FRAGMENT, 6) {

    val colorLocation = getAttributeLocation("color")

    fun setup() {
        vertexAttribute(positionLocation, 2, 6 * java.lang.Float.BYTES, 0)
        OpenGL.reportError()
        vertexAttribute(colorLocation, 4, 6 * java.lang.Float.BYTES, 2L * java.lang.Float.BYTES)
        OpenGL.reportError()
        glEnableVertexAttribArray(positionLocation)
        OpenGL.reportError()
        glEnableVertexAttribArray(colorLocation)
        OpenGL.reportError()
    }

    fun gradient(triangles: FloatArray, colors: Array<Color>) {
        requiredTriangles(colors.size/3)
        for (i in colors.indices) {
            val color = colors[i]
            GLBackend.floatBuffer
                .put(triangles[i * 2]).put(triangles[i * 2 + 1])
                .put(color.red).put(color.green).put(color.blue).put(color.alpha)
        }
    }


    companion object {
        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute vec4 color;

            uniform mat3 viewMatrix;
            
            varying vec4 col;

            void main() {
                col = color;

                gl_Position = vec4( viewMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val FRAGMENT = """
            #version 120
            
            varying vec4 col;

            void main() {
                gl_FragColor = col;
            }

            """.trimIndent()
    }
}

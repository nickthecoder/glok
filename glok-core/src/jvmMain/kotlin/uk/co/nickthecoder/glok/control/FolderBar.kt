/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.fileProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.FOLDER_BAR
import uk.co.nickthecoder.glok.util.openFileInExternalApp
import java.io.File

/**
 * Displays a [folder] (type [File]) as a horizontal line of [Button]s.
 * Click any of the buttons to change the value of [folder].
 *
 * By setting [asTextField] to `true`, the buttons are replaced by a [TextField],
 * where the user can type the folder name.
 * Press ENTER to update [folder] (it is NOT updated just by typing).
 *
 * This is used at the top of the [FileDialog]s.
 */
class FolderBar(initialFolder: File) : WrappedRegion<HBox>(HBox()) {

    // region ==== Properties ====
    val folderProperty by fileProperty(initialFolder)
    var folder by folderProperty

    val asTextFieldProperty by booleanProperty(false)
    var asTextField by asTextFieldProperty
    // endregion Properties

    // region ==== Nodes ====
    private val folderTextField = TextField(initialFolder.path)
    private val folderButtons = HBox()
    private val scroll = scrollPaneWithButtons {
        visibleProperty.bindTo(! asTextFieldProperty)
        content = folderButtons.apply {
            spacing(4)
        }
    }

    // endregion Nodes

    // region ==== Listeners ====
    @Suppress("unused")
    private val directoryListener = folderProperty.addChangeListener { _, _, dir ->
        folderTextField.text = dir.toString()
        folderTextField.caretIndex = folderTextField.text.length
        folderTextField.anchorIndex = folderTextField.caretIndex
    }

    @Suppress("unused")
    private val folderListener = folderProperty.addListener {
        updateButtons()
    }

    @Suppress("unused")
    private val asTextFieldListener = asTextFieldProperty.addChangeListener { _, _, asTextField ->
        if (asTextField) folderTextField.requestFocus()
    }

    @Suppress("unused")
    private val textFieldFocusListener = folderTextField.focusedProperty.addChangeListener { _, _, focused ->
        if (! focused) {
            val selectFolder = File(folderTextField.text)
            if (selectFolder.exists() && selectFolder.isDirectory) {
                folder = selectFolder
            }
        }
    }
    // endregion ==== Listeners ====


    init {
        style(FOLDER_BAR)
        section = true

        inner.apply {

            + scroll

            + hBox {
                visibleProperty.bindTo(asTextFieldProperty)
                growPriority = 1f
                spacing(8)

                + Label("Folder")
                + folderTextField.apply {
                    growPriority = 1f
                    onKeyPressed { event ->
                        if (event.key == Key.ENTER) {
                            val selectFolder = File(text)
                            if (selectFolder.exists() && selectFolder.isDirectory) {
                                folder = selectFolder
                            }
                            event.consume()
                        }
                    }
                }
            }
        }
        updateButtons()
    }

    private fun updateButtons() {
        folderButtons.children.clear()
        var oneFolder: File? = folder
        while (oneFolder != null) {
            val selectFolder = oneFolder
            folderButtons.children.add(0, button(if (oneFolder.name == "") "/" else oneFolder.name) {
                onAction { folder = selectFolder }
                onPopupTrigger {
                    popupMenu {
                        + menuItem("Browse...") {
                            onAction { openFileInExternalApp(selectFolder) }
                        }
                    }.show(this)
                }
            })
            oneFolder = oneFolder.parentFile
        }
        // Scroll o the far right. We cannot do it NOT, because the scene hasn't been laid out.
        // The runLater ensures it is done AFTER layout has been performed.
        Platform.runLater {
            folderButtons.children.lastOrNull()?.let {
                scroll.scrollToX(scroll.content !!.width)
            }
        }
    }
}

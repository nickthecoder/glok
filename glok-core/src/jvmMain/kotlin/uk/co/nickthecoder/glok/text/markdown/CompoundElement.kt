package uk.co.nickthecoder.glok.text.markdown

internal open class CompoundElement : SimpleElement {

    val elements = mutableListOf<SimpleElement>()

    fun include(line: String) {
        // The order of the pattern matching is important.
        // Links may contain characters which are also Markdown syntax, and therefore
        // links MUST be first.

        // Links
        linkPattern.ifMatches(line) {
            include(groupValues[1])
            val title = groupValues[2]
            val linkAndDescription = groupValues[3]
            var link = linkAndDescription
            var description = ""
            linkAndDescriptionPattern.ifMatches(linkAndDescription) {
                link = groupValues[1]
                description = groupValues[2]
            }
            elements.add(Link(title, link, description))
            include(groupValues[4])
            return
        }

        // Emphasis
        for ((pattern, style) in emphasisPatterns) {
            pattern.ifMatches(line) {
                include(groupValues[1])
                val body = CompoundElement().apply {
                    include(groupValues[2])
                }
                elements.add(Emphasis(body, style))
                include(groupValues[3])
                return
            }
        }

        // Line break
        lineBreakPattern.ifMatches(line) {
            include(groupValues[1])
            elements.add(LineBreak)
            include(groupValues[2])
            return
        }

        // HTML Character entities, such as &lg;
        htmlCharacterPattern.ifMatches(line) {
            val inside = groupValues[2]
            val replacement = htmlToChar[inside]
            if (replacement != null) {
                include(groupValues[1])
                elements.add(PlainText(replacement))
                include(groupValues[3])
                return
            }
        }

        elements.add(PlainText(line))
    }

    override fun dump(indent: Int) {
        println(" ".repeat(indent * 4) + toString())
        for (element in elements) {
            element.dump(indent + 1)
        }
    }

    override fun toString() = "CompoundElement with ${elements.size} parts"
}

internal class LineElement : CompoundElement()

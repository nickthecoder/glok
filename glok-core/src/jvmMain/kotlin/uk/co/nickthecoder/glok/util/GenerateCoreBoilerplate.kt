/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

import uk.co.nickthecoder.glok.application.ApplicationStatus
import uk.co.nickthecoder.glok.application.Restart
import uk.co.nickthecoder.glok.backend.Vector2
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.ActionEventHandler
import uk.co.nickthecoder.glok.event.KeyCombination
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle
import uk.co.nickthecoder.glok.text.Indentation
import uk.co.nickthecoder.glok.theme.Theme
import java.io.File
import kotlin.reflect.KClass


/**
 * Generates property boilerplate for the key classes in Glok.
 * Also generates `stylable` code for :
 *
 * Boolean, Int, Float, Double, String, File
 *
 * The non-stylable code for them is in glok-model.
 *
 * The generated code is included in glok-core's source code (in git), and therefore there is no need to
 * regenerate it as part of the gradle build script.
 * I run this (from my IDE) only when the templates have changed in some way.
 */
fun main(vararg args: String) {
    println("Generate Core Boilerplate")
    val projectDir = if (args.isEmpty()) File(".") else File(args[0])
    val baseDir = File(projectDir, "glok-core/src/commonMain/kotlin")

    fun gen(config: GenConfig, vararg classes: KClass<*>) {
        for (klass in classes) {
            val filename =
                if (config.stylableOnly) "Stylable${klass.simpleName}Boilerplate.kt" else "${klass.simpleName}Boilerplate.kt"
            generateBoilerplate(
                baseDir,
                "uk.co.nickthecoder.glok.property.boilerplate",
                klass,
                config,
                filename
            )
            println("Generated $filename")
        }
    }

    gen(
        GenConfig(stylableOnly = true, stylable = true),
        Boolean::class, Int::class, Float::class, String::class
    )

    gen(
        GenConfig(),
        ActionEvent::class, ActionEventHandler::class, ApplicationStatus::class,
        ButtonType::class
    )

    gen(
        GenConfig(stylable = true, functions = true, indirect = true),
        Color::class
    )

    gen(
        GenConfig(stylable = true, functions = true),

        Alignment::class,
        Background::class, Border::class,
        Edges::class,
        Font::class,
        HAlignment::class,
        Image::class,
        Node::class,
        Orientation::class,
        Side::class,
        VAlignment::class,
        Vector2::class
    )

    gen(
        GenConfig(stylable = true, functions = false),
        ContentDisplay::class
    )
    gen(
        GenConfig(stylable = true, functions = false, optional = false),
        ScrollBarPolicy::class,
        SpinnerArrowPositions::class,
        TabClosingPolicy::class
    )

    gen(
        GenConfig(functions = true),
        KeyCombination::class
    )

    gen(
        GenConfig(functions = true, validated = true),
        TextPosition::class
    )

    gen(
        GenConfig(functions = false),
        FixedFormat::class,
        KeyCombination::class,
        Restart::class,
        Scene::class,
        Stage::class,
        Toggle::class,
        ToggleGroup::class,
        Tooltip::class
    )

    gen(
        GenConfig(optional = false),
        ButtonMeaning::class,
        FontStyle::class
    )

    gen(
        GenConfig(optional = false, functions = true),
        Indentation::class,
        MousePointer::class,
        Theme::class
    )
}

package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.HighlightRange

class LinkHighlightRange(
    from: TextPosition,
    to: TextPosition,
    val link: String,
    highlight: Highlight
) : HighlightRange(from, to, highlight) {
    override fun toString() = "LinkHighlightRange $from to $to link='$link'"
}

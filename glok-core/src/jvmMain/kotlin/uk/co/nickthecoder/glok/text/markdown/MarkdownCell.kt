package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.HighlightRange

internal class MarkdownCell(

    val row: MarkdownRow,
    str: String

) : Block(row.table.document, row.table.config) {

    val content = CompoundElement().apply { include(str) }

    var cellWidth = 0

    val lines = mutableListOf("")

    /**
     * These ranges are relative to the CELL, not the DOCUMENT.
     * i.e. (0,0) would be at the top left of the cell.
     *
     *
     */
    internal val ranges = mutableListOf<HighlightRange>()

    init {
        elements.add(content)
    }

    fun render(cellWidth: Int) {
        this.cellWidth = cellWidth
        lines.clear()
        ranges.clear()
        lines.add("")

        val flow = object : Flow() {

            override val columns: Int get() = cellWidth

            override fun append(text: String) {
                if (text == "\n") {
                    lines.add("")
                } else {
                    lines[lines.size - 1] += text
                }
            }

            override fun appendNewLine() {
                lines.add("")
            }

            override fun endPosition() = TextPosition(lines.size - 1, lines.last().length)


            override fun addRange(link: String?, from: TextPosition, to: TextPosition, highlight: Highlight) {
                val range = if (link == null) {
                    HighlightRange(from, to, highlight)
                } else {
                    LinkHighlightRange(from, to, link, highlight)
                }
                ranges.add(range)
            }
        }
        render(flow)
    }

}

package uk.co.nickthecoder.glok.util

import org.joml.Matrix3x2f
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.property.ReadOnlyProperty
import uk.co.nickthecoder.glok.property.functions.toHashRGB
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.theme.Tantalum
import java.awt.Desktop
import java.io.File
import java.util.prefs.Preferences
import kotlin.reflect.KClass

internal val homeDirectory: File by lazy { File(System.getProperty("user.home")) }

fun Matrix.toJoml() = Matrix3x2f(m00, m01, m10, m11, m20, m21)


internal fun Matrix3x2f.times(x: Float, y: Float): Pair<Float, Float> {
    return Pair(
        m00 * x + m10 * y + m20,
        m01 * x + m11 * y + m21
    )
}

actual fun <T : Any> KClass<T>.newInstance(): T {
    return constructors.firstOrNull { it.parameters.isEmpty() }?.call() ?: throw GlokException("Failed to create $this")
}

internal actual fun findProperties(obj: Any): List<ReadOnlyProperty<*>> {
    val result = mutableListOf<ReadOnlyProperty<*>>()
    val klass = obj.javaClass
    for (method in klass.methods) {
        if (method.name.endsWith("Property")) {
            val returnType = method.returnType
            if (method.parameterCount == 0 && ReadOnlyProperty::class.java.isAssignableFrom(returnType)) {
                val property = method.invoke(obj) as ReadOnlyProperty<*>
                if (property.beanName?.isNotBlank() == false) {
                    log.warn("Property ${method.name} does not have a bean name. Ignoring it.")
                } else {
                    result.add(property)
                }
            }
        }
    }
    return result.sortedBy { it.beanName }
}


fun openFileInExternalApp(file: File) {

    if (isWindows) {
        try {
            Desktop.getDesktop().open(file)
        } catch (e: Exception) {
            // Do Nothing
        }
    } else {
        val nullFile = if (isWindows) File("NUL:") else File("/dev/null")
        ProcessBuilder("xdg-open", file.absolutePath).apply {
            redirectError(nullFile)
            redirectOutput(nullFile)
            start()
        }
    }
}

private val isWindows by lazy { System.getProperty("os.name").startsWith("Windows") }

/**
 * Redirects stdout and stderr to /dev/null or NUL (windows).
 * This prevents the process from blocking if it spews output.
 */
internal fun ProcessBuilder.nullOutputs() {
    val nullFile = File(if (isWindows) "NUL" else "/dev/null")
    redirectError(nullFile)
    redirectOutput(nullFile)
}

fun openFolderInTerminal(file: File) {
    val directory = if (file.isDirectory) file else file.parentFile
    val terminalCommand = GlokSettings.terminalCommand
    if (terminalCommand.isEmpty()) return

    ProcessBuilder().apply {
        directory(directory)
        command(terminalCommand)
        nullOutputs()
        start()
    }
}

internal actual fun dumpStackTrace() {
    Thread.dumpStack()
}

/**
 * It is recommended that your application loads and saves [GlokSettings] via
 * [load] and [save] using this [Preferences] node.
 * This allows the user to choose the settings ONCE, and they will work across
 * all `Glok` applications.
 */
fun GlokSettings.preferences() = Preferences.userNodeForPackage(GlokSettings::class.java)

/**
 * Saves [GlokSettings] using Java's [Preferences]. The default node is
 * given by [preferences], which is common to all `Glok` applications.
 *
 * However, if you wish to save them separately, you may supply a different [preferences] node.
 */
fun GlokSettings.save(preferences: Preferences = preferences()) {

    with(preferences) {

        putFloat("globalScale", globalScale)
        putBoolean("reverseScroll", reverseScroll)
        putDouble("clickTimeThreshold", clickTimeThreshold)
        putFloat("clickDistanceThreshold", clickDistanceThreshold)
        putFloat("dragDistanceThreshold", dragDistanceThreshold)
        putDouble("tooltipTimeThreshold", tooltipTimeThreshold)
        putDouble("historyMergeTimeThreshold", historyMergeTimeThreshold)

        putBoolean("indentationTabs", indentationTabs)
        putInt("indentationColumns", indentationColumns)
        putBoolean("indentationBehaveLikeTabs", indentationBehaveLikeTabs)
        put("terminalCommand", terminalCommand)

        flush()
    }

    val tantalumPreferences = preferences.node("tantalum")
    with(tantalumPreferences) {
        putBoolean("dark", Tantalum.dark)
        put("fontFamily", Tantalum.fontFamily)
        putFloat("fontSize", Tantalum.fontSize)
        putInt("iconSize", Tantalum.iconSize)
        flush()

        // Save color schemes
        for (colorScheme in listOf(Tantalum.darkColorScheme, Tantalum.lightColorScheme)) {
            val isDark = colorScheme === Tantalum.darkColorScheme
            with(tantalumPreferences.node(if (isDark) "dark" else "light")) {
                for ((name, colorProperty) in colorScheme) {
                    if (colorProperty.value.alpha == 1f) {
                        put(name, colorProperty.value.toHashRGB())
                    } else {
                        put(name, colorProperty.value.toHashRGBA())
                    }
                }
                flush()
            }
        }
    }

}

/**
 * Loads [GlokSettings] using Java's [Preferences]. The default node is
 * given by [preferences], which is common to all `Glok` applications.
 *
 * The following properties of [Tantalum] are also included :
 *
 * * [Tantalum.dark]
 * * [Tantalum.accentColor]
 * * [Tantalum.fontFamily]
 * * [Tantalum.fontSize]
 * * [Tantalum.iconSize]
 *
 * However, if you wish to load them separately, you may supply a different [preferences] node.
 */
fun GlokSettings.load(preferences: Preferences = preferences()) {

    with(preferences) {
        sync()

        globalScale = getFloat("globalScale", globalScale)
        reverseScroll = getBoolean("reverseScroll", reverseScroll)
        clickTimeThreshold = getDouble("clickTimeThreshold", clickTimeThreshold)
        clickDistanceThreshold = getFloat("clickDistanceThreshold", clickDistanceThreshold)
        dragDistanceThreshold = getFloat("dragDistanceThreshold", dragDistanceThreshold)
        tooltipTimeThreshold = getDouble("tooltipTimeThreshold", tooltipTimeThreshold)
        historyMergeTimeThreshold = getDouble("historyMergeTimeThreshold", historyMergeTimeThreshold)

        indentationTabs = getBoolean("indentationTabs", indentationTabs)
        indentationColumns = getInt("indentationColumns", indentationColumns)
        indentationBehaveLikeTabs = getBoolean("indentationBehaveLikeTabs", indentationBehaveLikeTabs)
        terminalCommand = get("terminalCommand", terminalCommand)
    }

    val tantalumPreferences = preferences.node("tantalum")
    with(tantalumPreferences) {
        Tantalum.dark = getBoolean("dark", Tantalum.dark)
        Tantalum.fontFamily = get("fontFamily", Tantalum.fontFamily)
        Tantalum.fontSize = getFloat("fontSize", Tantalum.fontSize)
        Tantalum.iconSize = getInt("iconSize", Tantalum.iconSize)

        // Load color schemes
        for (colorScheme in listOf(Tantalum.darkColorScheme, Tantalum.lightColorScheme)) {
            val isDark = colorScheme === Tantalum.darkColorScheme
            with(tantalumPreferences.node(if (isDark) "dark" else "light")) {
                for ((name, colorProperty) in colorScheme) {
                    Color.find(get(name, ""))?.let { colorProperty.value = it }
                }
            }
        }
    }
}

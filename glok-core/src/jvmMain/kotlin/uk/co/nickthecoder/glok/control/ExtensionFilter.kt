/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import java.io.File
import java.io.FileFilter

/**
 *
 */
class ExtensionFilter(

    /**
     * The human-readable text, such as "Text Files (*.txt)"
     */
    val text: String,
    /**
     * Supports `*` for wildcards, but does NOT support any other glob-like patterns.
     * Case is always ignored.
     *
     * When using `native` [FileDialog]s, each pattern must be in the form `*.XXX`,
     * because the native API that I'm using does not use file patterns, and only uses file extensions.
     */
    vararg val patterns: String

) : FileFilter {

    private val regexes =
        patterns.map { Regex(it.replace(".", "\\.").replace("*", ".*"), RegexOption.IGNORE_CASE) }

    override fun accept(file: File?): Boolean {
        val name = file?.name ?: return false
        return file.isFile && regexes.firstOrNull { it.matches(name) } != null
    }

    fun toCommaSeparatedString(): String {
        return patterns.joinToString(separator = ",") { pattern ->
            if (pattern.startsWith("*.")) {
                pattern.substring(2)
            } else {
                "*"
            }
        }
    }

    override fun toString() = text
}

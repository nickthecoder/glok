package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.control.TabPane
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.boilerplate.ColorProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleColorProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleStringProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.util.*
import java.util.prefs.Preferences


/**
 * A standalone application to edit Glok's settings.
 *
 * The settings are from [GlokSettings] and [Tantalum], and are saved as Java [Preferences]
 * (See [GlokSettings.save]).
 *
 * If another applications load these settings (using [GlokSettings.load]), then the applications
 * will share the same settings.
 */
open class GlokSettingsApplication : Application() {

    override fun start(primaryStage: Stage) {

        GlokSettings.load()

        with(primaryStage) {
            title = "Glok Settings"
            scene = createScene()
            show()
        }
    }

    open fun createScene() = scene {
        root = borderPane {
            overridePrefWidth = 1000f
            overridePrefHeight = 670f

            center = tabPane {
                addTabs(this)
            }
            bottom = buttonBar {
                add(ButtonMeaning.LEFT, button("Reset All Settings") {
                    onAction {
                        GlokSettings.reset()
                        Tantalum.reset()
                    }
                })

                add(ButtonMeaning.CANCEL, button("Cancel") {
                    cancelButton = true
                    onAction {
                        scene?.stage?.close()
                    }
                })
                add(ButtonMeaning.OK, button("OK") {
                    defaultButton = true
                    onAction {
                        save()
                        scene?.stage?.close()
                    }
                })
            }
        }
    }

    open fun addTabs(tabPane: TabPane) {
        with(tabPane) {
            side = Side.LEFT

            + tab("Appearance") {
                content = scrollPane {
                    content = Tantalum.appearanceForm()
                }
            }

            + tab("Custom Colors") {
                content = scrollPane {
                    content = Tantalum.customColorsForm()
                }
            }

            + tab("User Interface") {
                content = scrollPane {
                    content = generalSettingsForm()
                }
            }
        }
    }

    open fun save() {
        GlokSettings.save()
    }

    companion object {

        /**
         * Converts between seconds and milliseconds.
         */
        protected val millisecondsConverter = object : Converter<Double, Double> {
            override fun backwards(value: Double) = value / 1000.0

            override fun forwards(value: Double) = value * 1000.0
        }

        fun createGlokSettingsTabPane() = tabPane {
            overridePrefWidth = 1000f
            overridePrefHeight = 600f
            side = Side.LEFT

            + tab("Appearance") {
                content = scrollPane {
                    content = Tantalum.appearanceForm()
                }
            }

            + tab("Custom Colors") {
                content = scrollPane {
                    content = Tantalum.customColorsForm()
                }
            }

            + tab("General") {
                content = scrollPane {
                    content = generalSettingsForm()
                }
            }
        }

        /**
         *
         * NOTE. [GlokSettings.globalScale] is NOT included in this form, because from a user perspective,
         * it is part of `Appearances`, and therefore is part of [Tantalum.appearanceForm].
         */
        fun generalSettingsForm() = formGrid {
            style(".form")

            + rowHeading("Indentation")

            + row("Indentation Type") {
                id = "indentationType"
                right = vBox {
                    + hBox {
                        alignment = Alignment.CENTER_LEFT
                        spacing(30)

                        radioChoices(GlokSettings.indentationTabsProperty) {
                            + propertyRadioButton(true, "Tabs")
                            + propertyRadioButton(false, "Spaces")
                        }
                    }
                }
            }

            + row("Columns") {
                right = intSpinner {
                    editor.prefColumnCount = 5
                    min = 1
                    max = 8
                    valueProperty.bidirectionalBind(GlokSettings.indentationColumnsProperty)
                }
            }

            + row("Behave Like Tabs") {
                visibleProperty.bindTo(! GlokSettings.indentationTabsProperty)
                right = checkBox("") {
                    tooltip =
                        TextTooltip("Feels like tabs,\nbut the document uses spaces\nThe best of both worlds?")
                    selectedProperty.bidirectionalBind(GlokSettings.indentationBehaveLikeTabsProperty)
                }
            }

            + rowSeparator()

            + rowHeading("Mouse")

            + row("Click Time Threshold") {
                id = "clickTimeThreshold"
                right = doubleSpinner {
                    smallStep = 0.1
                    largeStep = 5.0
                    min = 0.1
                    max = 1.0
                    format = max1DecimalPlace
                    editor.prefColumnCount = 5
                    valueProperty.bidirectionalBind(GlokSettings.clickTimeThresholdProperty, millisecondsConverter)
                }.units("Seconds")
            }

            + row("Click Distance Threshold") {
                id = "clickDistanceThreshold"
                right = floatSpinner {
                    format = max1DecimalPlace
                    min = 1f
                    max = 50f
                    editor.prefColumnCount = 5
                    valueProperty.bidirectionalBind(GlokSettings.clickDistanceThresholdProperty)
                }.units("Pixels")
            }

            + row("Drag Distance Threshold") {
                id = "dragDistanceThreshold"
                right = floatSpinner {
                    format = max1DecimalPlace
                    min = 1f
                    max = 50f
                    editor.prefColumnCount = 5
                    valueProperty.bidirectionalBind(GlokSettings.dragDistanceThresholdProperty)
                }.units("Pixels")
            }

            + row("Reverse Scroll") {
                id = "reverseScroll"
                right = checkBox {
                    selectedProperty.bidirectionalBind(GlokSettings.reverseScrollProperty)
                }
            }

            + rowSeparator()

            + rowHeading("Tooltips")

            + row("Tooltip Delay") {
                id = "tooltipDelay"
                right = doubleSpinner {
                    min = 0.0
                    smallStep = 0.1
                    largeStep = 1.0
                    format = max1DecimalPlace
                    editor.prefColumnCount = 5
                    valueProperty.bidirectionalBind(
                        GlokSettings.tooltipTimeThresholdProperty,
                        millisecondsConverter
                    )
                }.units("Seconds")
            }

            + rowSeparator()

            + rowHeading("Undo / Redo")

            + row("Idle Time Threshold") {
                id = "historyMergeTimeThreshold"
                right = doubleSpinner {
                    min = 0.0
                    format = max1DecimalPlace
                    editor.prefColumnCount = 5
                    valueProperty.bidirectionalBind(
                        GlokSettings.historyMergeTimeThresholdProperty,
                        millisecondsConverter
                    )
                }.units("Seconds")
            }

        }

        @JvmStatic
        fun main(vararg args: String) {
            launch(GlokSettingsApplication::class.java)
        }
    }

}

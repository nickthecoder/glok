/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import java.util.concurrent.LinkedBlockingDeque


actual object Platform {

    actual val OS by lazy { System.getProperty("os.name") }

    private val xdgCurrentDesktop by lazy { System.getenv("XDG_CURRENT_DESKTOP")?.uppercase() }

    private val pendingRunnable = LinkedBlockingDeque<() -> Unit>()

    private var glokThread: Thread? = null

    actual fun getEnv(name: String) = System.getenv(name)

    actual fun isWindows() = OS.startsWith("Windows");
    actual fun isLinux() = OS.startsWith("Linux");

    actual fun isUnix() = OS.startsWith("Linux") || OS.startsWith("FreeBSD") || OS.startsWith("SunOS")
    actual fun isMacOS() = OS.startsWith("Mac")

    actual fun isKDE() = xdgCurrentDesktop?.contains("KDE") == true

    actual fun isGnome() = xdgCurrentDesktop?.contains("GNOME") == true

    actual internal fun begin() {
        glokThread = Thread.currentThread()
    }

    actual fun isGlokThread() = Thread.currentThread() === glokThread

    actual fun runLater(runnable: () -> Unit) {
        pendingRunnable.put(runnable)
    }

    internal actual fun performPendingRunnable() {
        while (pendingRunnable.isNotEmpty()) {
            try {
                pendingRunnable.take().invoke()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

import java.text.DecimalFormat

actual class FixedFormat actual constructor(val places: Int) {

    private val format = DecimalFormat("0.${"#".repeat(places)}")

    actual fun format(number: Number): String = format.format(number)

    actual companion object {
        actual fun places(places: Int) = FixedFormat(places)
    }
}

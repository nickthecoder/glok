package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.text.StyledTextDocument

internal interface SimpleElement : Element


internal object BlankLine : Element {
    override fun toString() = "BlankLine"
}

internal object HorizontalLine : SimpleElement {
    override fun toString() = "HorizontalLine"
}

internal object LineBreak : SimpleElement {
    override fun toString() = "LineBreak"
}

internal class PlainText(val text: String) : SimpleElement {
    override fun toString() = "PlainText $text"
}

internal class Emphasis(val content: SimpleElement, val style: String) : SimpleElement {
    override fun toString() = "Emphasis $style"

    override fun dump(indent: Int) {
        println(" ".repeat(indent * 4) + toString())
        content.dump(indent + 1)
    }
}

internal class Heading(val level: Int, val contents: SimpleElement) : SimpleElement {
    override fun toString() = "Heading $level $contents"
}

internal class Link(val text: String, val link: String, val description: String?) : SimpleElement {
    override fun toString() = "Link $text $link"
}

internal class BlockQuotes(document: StyledTextDocument, config: MarkdownToStyledConfig) : Block(document, config) {
    override fun toString() = "BlockQuotes with ${elements.size} lines"
}

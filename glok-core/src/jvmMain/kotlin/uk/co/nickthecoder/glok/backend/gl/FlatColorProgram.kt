package uk.co.nickthecoder.glok.backend.gl

import uk.co.nickthecoder.glok.backend.GlokRect
import uk.co.nickthecoder.glok.backend.Vector2
import uk.co.nickthecoder.glok.scene.Color

internal class FlatColorProgram : ShaderProgram(SG_VERTEX, SG_FRAGMENT, 2) {

    private val colorLocation = getUniformLocation("color")

    fun setup(color: Color) {
        enablePosition()
        setUniform(colorLocation, color)
    }

    fun draw(vertices: List<Vector2>) {
        requiredVertices(vertices.size)

        for (point in vertices) {
            GLBackend.floatBuffer.put(point.x).put(point.y)
        }
    }


    fun drawLine(from: Vector2, to: Vector2, thickness: Float) {
        val delta = (to - from).unit().perpendicular() * (thickness / 2f)

        requiredTriangles(2)
        with(GLBackend.floatBuffer) {

            put(from.x + delta.x).put(from.y + delta.y)
            put(from.x - delta.x).put(from.y - delta.y)
            put(to.x + delta.x).put(to.y + delta.y)

            put(to.x + delta.x).put(to.y + delta.y)
            put(to.x - delta.x).put(to.y - delta.y)
            put(from.x - delta.x).put(from.y - delta.y)
        }
    }

    fun setupAndStrokeRect(rect: GlokRect, thickness: Float, color: Color) {
        setup(color)
        strokeInsideRect(rect, thickness)
    }

    fun strokeInsideRect(rect: GlokRect, thickness: Float) {
        strokeInsideRect(rect.left, rect.top, rect.right, rect.bottom, thickness)
    }

    fun strokeInsideRect(left: Float, top: Float, right: Float, bottom: Float, thickness: Float) {

        val insideLeft = left + thickness
        val insideTop = top + thickness
        val insideRight = right - thickness
        val insideBottom = bottom - thickness

        requiredTriangles(8)
        with(GLBackend.floatBuffer) {

            // Left edge
            put(insideLeft).put(insideBottom)
            put(left).put(bottom)
            put(left).put(top)

            put(left).put(top)
            put(insideLeft).put(insideTop)
            put(insideLeft).put(insideBottom)

            // Top edge
            put(left).put(top)
            put(right).put(top)
            put(insideLeft).put(insideTop)

            put(insideLeft).put(insideTop)
            put(right).put(top)
            put(insideRight).put(insideTop)


            // Right Edge
            put(insideRight).put(insideTop)
            put(right).put(top)
            put(insideRight).put(insideBottom)

            put(insideRight).put(insideBottom)
            put(right).put(top)
            put(right).put(bottom)

            // Bottom Edge
            put(right).put(bottom)
            put(left).put(bottom)
            put(insideLeft).put(insideBottom)

            put(insideLeft).put(insideBottom)
            put(insideRight).put(insideBottom)
            put(right).put(bottom)
        }
    }

    fun setupAndFillRect(rect: GlokRect, color: Color) {
        setup(color)
        fillRect(rect)
    }

    fun fillRect(rect: GlokRect) {

        val left = rect.left
        val bottom = rect.bottom
        val right = rect.right
        val top = rect.top

        requiredTriangles(2)
        with(GLBackend.floatBuffer) {
            put(left).put(bottom)
            put(right).put(bottom)
            put(right).put(top)

            put(right).put(top)
            put(left).put(top)
            put(left).put(bottom)
        }
    }

    fun fillRect(left: Float, top: Float, right: Float, bottom: Float) {

        requiredTriangles(2)
        with(GLBackend.floatBuffer) {
            put(left).put(bottom)
            put(right).put(bottom)
            put(right).put(top)

            put(right).put(top)
            put(left).put(top)
            put(left).put(bottom)
        }
    }

    companion object {

        private val SG_VERTEX = """
            #version 120

            attribute vec2 position;
            
            uniform mat3 viewMatrix;
            
            void main() {
                gl_Position = vec4( viewMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val SG_FRAGMENT = """
            #version 120
            
            uniform vec4 color;

            void main() {
                gl_FragColor = color;
            }
            """.trimIndent()
    }

}

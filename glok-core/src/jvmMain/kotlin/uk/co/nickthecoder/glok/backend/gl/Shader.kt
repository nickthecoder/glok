package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER
import org.lwjgl.opengl.GL20.GL_VERTEX_SHADER
import java.io.File
import java.io.InputStream


internal enum class ShaderType(val value: Int) {
    VERTEX_SHADER(GL_VERTEX_SHADER),
    FRAGMENT_SHADER(GL_FRAGMENT_SHADER),
}

internal class Shader(type: ShaderType, source: CharSequence) {

    val handle = GL20.glCreateShader(type.value)

    init {
        GL20.glShaderSource(handle, source)
        GL20.glCompileShader(handle)

        val status = GL20.glGetShaderi(handle, GL20.GL_COMPILE_STATUS)
        if (status != GL20.GL_TRUE) {
            throw RuntimeException(GL20.glGetShaderInfoLog(handle))
        }
    }

    fun delete() {
        GL20.glDeleteShader(handle)
    }

    companion object {

        fun load(type: ShaderType, file: File): Shader {
            return Shader(type, file.readText())
        }

        fun load(type: ShaderType, input: InputStream): Shader {
            return Shader(type, input.bufferedReader().use { it.readText() })
        }
    }

}

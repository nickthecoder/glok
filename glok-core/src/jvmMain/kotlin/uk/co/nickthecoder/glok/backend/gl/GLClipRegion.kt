package uk.co.nickthecoder.glok.backend.gl

import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

internal data class GLClipRegion(
    val left: Int,
    val bottom: Int,
    val width: Int,
    val height: Int
) {
    val right get() = left + width
    val top get() = bottom + height

    /*
        I was having clipping issues, and thought the problem may be here, so I drew this diagram to
        make sure the code was sane.

          y            right2 = min( right, other.right )
          ▲
          │   ┌───────────────┐
          │   │         ┌─────┼───────┐╶─▶ top2 = min( top,other.top )
          │   │  this   │     │       │
          │   │         │     │       │
          │   └─────────┼─────┘ ╶╌╌╌╌╌┼╌╌╌╌bottom2 = max( bottom, other.bottom )╴
          │             │    other    │
          │             │             │
          │             └─────────────┘
          │
          │       left2 = max( bottom,other.bottom )
          └──────────────────────────────────────────────────▶ x
        (0,0)

		Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
    */
    fun intersection(other: GLClipRegion): GLClipRegion {
        val top2 = min(top, other.top)
        val right2 = min(right, other.right)
        val left2 = max(left, other.left)
        val bottom2 = max(bottom, other.bottom)
        return GLClipRegion(left2, bottom2, right2 - left2, top2 - bottom2)
    }

    override fun toString() = "GLClipRegion $left , $bottom  $width x $height"
}

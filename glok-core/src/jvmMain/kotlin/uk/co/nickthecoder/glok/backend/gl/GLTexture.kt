package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.glok.backend.Texture
import java.nio.ByteBuffer

internal class GLTexture internal constructor(

    override val name: String,
    override val width: Int,
    override val height: Int

) : Texture {

    val handle: Int = glGenTextures()

    companion object {
        fun unbind() {
            glBindTexture(GL_TEXTURE_2D, 0)
        }
    }

    override fun bind() {
        glBindTexture(GL_TEXTURE_2D, handle)
    }

    /**
     * Transfers the texture from the GPU to main memory (which is SLOW).
     * The result is a ByteArray of size width * height * 4, and the format is RGBA.
     * i.e. to get the alpha value at x,y :
     *
     *     toByteArray()[ (y * height + x) * 4 + 3 ]
     *
     * and then deal with the annoyance of java's lack of unsigned bytes. Grr :
     *
     *     .toInt() & 0xFF
     */
    fun toByteArray(): ByteArray {
        bind()
        val pixels = ByteArray(width * height * 4)
        val buffer = ByteBuffer.allocateDirect(pixels.size)
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        buffer.get(pixels)
        unbind()

        return pixels
    }

    fun toByteBufferRBGA(): ByteBuffer {
        val buffer = ByteBuffer.allocateDirect(width * height * 4)
        bind()
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        unbind()
        return buffer
    }

    /**
     * This is used for debugging only. It dumps texture, showing values in the range 0..ff
     */
    fun dump() {
        val pixels = toByteArray()
        for (y in height - 1 downTo 0) {
            for (x in 0 until width) {
                for (b in 0..3) {
                    val byte = pixels[(x + (y * width)) * 4 + b]
                    print(String.format("%02x", byte.toInt() and 0xff))
                }
                print(" ")
            }
            println()
        }
    }
}

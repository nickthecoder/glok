package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.opengl.GL11.*
import org.lwjgl.stb.STBImage
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.glok.backend.Resources
import uk.co.nickthecoder.glok.util.GlokException
import java.io.File
import java.io.IOException
import java.net.URL
import java.nio.ByteBuffer

/**
 * Loads textures from jar files using [Class.getResourceAsStream].
 */
internal class GLJarResources(val path: String) : Resources {

    override fun loadTexture(name: String): GLTexture {
        val full = "$path/$name"
        var url: URL? = javaClass.classLoader.getResource(full)
        // When launched from my IDE, it doesn't include the resources in the classpath???
        // So let's try looking for the file directly.
        if (url == null) {
            val file = File("glok-core/build/processedResources/jvm/main/$full")
            if (file.exists()) {
                url = file.toURI().toURL()
            }
        }
        if (url == null) throw GlokException("Jar resource '$full' not found")

        val inputStream = try {
            url.openStream()
        } catch (e: Exception) {
            throw GlokException("Jar resource not available: $full")
        }

        val pixels = inputStream.readAllBytes()
        val inputBuffer = ByteBuffer.allocateDirect(pixels.size)
        inputStream.use {
            for (b in pixels) {
                inputBuffer.put(b)
            }
            inputBuffer.flip()

            MemoryStack.stackPush().use { stack ->
                val cWidth = stack.mallocInt(1)
                val cHeight = stack.mallocInt(1)
                val cChannels = stack.mallocInt(1)
                val buffer = STBImage.stbi_load_from_memory(inputBuffer, cWidth, cHeight, cChannels, 4)
                OpenGL.reportError()
                buffer ?: throw IOException("Failed to load texture : $full")

                val width = cWidth.get()
                val height = cHeight.get()
                val texture = GLTexture(name, width, height)
                glBindTexture(GL_TEXTURE_2D, texture.handle)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)

                MemoryUtil.memFree(buffer)
                return texture
            }
        }
    }
}

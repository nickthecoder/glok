package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.text.StyledTextDocument

internal val horizontalLinePattern = Regex("^\\s*(---+|\\*\\*\\*+|___+)\\s*$")

internal val headingPattern = Regex("^(#+)\\s?(.*)$")

internal val attributesPattern = Regex("^---+$")

internal val heading1UnderlinePattern = Regex("^=+\\s*$")
internal val heading2UnderlinePattern = Regex("^-+\\s*$")

internal val codeBlockPattern = Regex( "^```\\s*(.*)$")
internal val plainBlockPattern = Regex( "^ {4}|\\t(.*)$")

internal val blockQuotesPattern = Regex("^>\\s*(.*)$")
internal val orderedListPattern = Regex("^(\\s*)[0-9][.)](.*)$")
internal val unorderedListPattern = Regex("^(\\s*)[-*+][.)](.*)$")
internal val indentedCodeBlockPattern = Regex("^\\s\\s\\s\\s|\t(.*)$")

internal val tableRowPattern = Regex("^\\s*\\|(.*)\\|\\s*$")
internal val tableHeadingPattern = Regex("^\\s*(\\|\\s*-+\\s*)+\\|\\s*$")

internal val lineBreakPattern = Regex("(.*?)<br>(.*)")
internal val emphasisPatterns = listOf(
    Regex("(.*)\\*\\*\\*(.+?)\\*\\*\\*(.*)") to "bold",
    Regex("(.*)___(.+?)___(.*)") to "boldItalic",
    Regex("(.*)\\*\\*(.+?)\\*\\*(.*)") to "bold",
    Regex("(.*)__(.+?)__(.*)") to "bold",
    Regex("(.*)\\*(.+?)\\*(.*)") to "italic",
    Regex("(.*)_(.+?)_(.*)") to "italic",
    Regex("(.*)`(.+?)`(.*)") to "code"
)

internal val htmlCharacterPattern = Regex("(.*?)&([a-z]*);(.*)")
internal val htmlToChar = mapOf(
    "nbsp" to " ",
    "lt" to "<", "gt" to ">", "amp" to "&", "quot" to "\"", "apos" to "'", "grave" to "`",
    "plus" to "+", "minus" to "-", "ast" to "*", "sol" to "/",
    "lowbar" to "_", "UnderBar" to "_",
    "num" to "#",
    "mid" to "|",
    "lpar" to "(", "rpar" to ")",
    "lsqb" to "[", "rsqb" to "]", "lbrack" to "[", "rbrack" to "]",
    "copy" to "©", "reg" to "®",
    "euro" to "€", "cent" to "¢", "pound" to "£", "yen" to "¥"
)

// NOTE, This does NOT include a group for the "description".
// If this is required, then take the 3rd group, and search for whitespace.
internal val linkPattern = Regex("(.*)\\[(.+?)]\\((.+?)\\)(.*)")
internal val linkAndDescriptionPattern = Regex( "(.*)\\s*(.*)" )

internal inline fun Regex.ifMatches(text: String, block: MatchResult.() -> Unit) {
    find(text)?.block()
}

internal class MarkdownToIntermediate(
    document: StyledTextDocument,
    config: MarkdownToStyledConfig,
    val lines: List<String>
) {

    val document = IntermediateDocument(document, config)

    fun parse(): IntermediateDocument {

        var firstLine = true
        var inAttributes = false
        for (line in lines) {
            if (firstLine && attributesPattern.matches(line)) {
                inAttributes = true
            } else {
                if (inAttributes) {
                    if (attributesPattern.matches(line)) {
                        inAttributes = false
                    } else {
                        // Parse attribute, such as: title: The_Title
                    }
                } else {
                    document.parseLine(line)
                }
            }
            firstLine = false
        }
        return document
    }


}

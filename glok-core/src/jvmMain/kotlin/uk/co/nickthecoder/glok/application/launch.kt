/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.backend.gl.GLBackend
import uk.co.nickthecoder.glok.backend.gl.OpenGL
import uk.co.nickthecoder.glok.util.*
import kotlin.reflect.KClass

/**
 * A convenience function for those using Java rather than Kotlin.
 * It creates a Restart using a Java [Class], rather than a Kotlin [KClass].
 */
fun createRestart(klass: Class<out Application>, rawArgs: Array<out String> = emptyArray()) =
    Restart(klass.kotlin, rawArgs)

/**
 * A convenience function for those using Java rather than Kotlin.
 * It launches an application using a Java [Class], rather than a Kotlin [KClass].
 */
fun launch(klass: Class<out Application>, rawArgs: Array<out String> = emptyArray()) = launch(klass.kotlin, rawArgs)

actual fun launch(klass: KClass<out Application>, rawArgs: Array<out String>) {
    if (Application.running) throw GlokException("Glok has already started")
    Application.running = true
    log = ConsoleLog().apply { level = Log.TRACE }

    Platform.begin()
    OpenGL.begin()
    backend = GLBackend()

    Application.launchPart2(Restart(klass, rawArgs))

    OpenGL.end()
}

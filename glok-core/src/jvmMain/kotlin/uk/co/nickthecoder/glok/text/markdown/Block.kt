package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.text.StyledTextDocument
import kotlin.math.min

internal open class Block(val document: StyledTextDocument, val config: MarkdownToStyledConfig) : Element {

    val elements = mutableListOf<Element>()

    /**
     * When we parse a simple line, we do NOT add it to the document right away,
     * because the next line may be `---` or `===` indicating that it was a heading.
     */
    var previousLine: CompoundElement? = null

    private var currentTable: MarkdownTable? = null
    private var codeBlock: CodeBlock? = null
    private var plainBlock: CodeBlock? = null

    fun parseLine(line: String) {

        val table = currentTable
        if (table != null) {
            tableHeadingPattern.ifMatches(line) {
                table.rowWasHeading()
                return
            }
            tableRowPattern.ifMatches(line) {
                table.addRow(groupValues[1])
                return
            }
            currentTable = null
        }

        codeBlock?.let { codeBlock ->
            if (codeBlockPattern.matches(line)) {
                this.codeBlock = null
            } else {
                codeBlock.lines.add(line)
            }
            return
        }

        plainBlock?.let { plainBlock ->
            plainBlockPattern.ifMatches(line) {
                plainBlock.lines.add(groupValues[1])
                return
            }
            this.plainBlock = null
        }

        // Headings using underline
        heading1UnderlinePattern.ifMatches(line) {
            previousLine?.let {
                elements.add(Heading(1, it))
                previousLine = null
                return
            }
        }
        heading2UnderlinePattern.ifMatches(line) {
            previousLine?.let {
                elements.add(Heading(2, it))
                previousLine = null
                return
            }
        }

        finishPreviousLine()

        if (line.isEmpty()) {
            elements.add(BlankLine)
            return
        }

        // Headings using prefix
        headingPattern.ifMatches(line) {
            val heading = Heading(
                groupValues[1].length,
                CompoundElement().apply { include(groupValues[2]) }
            )
            elements.add(heading)
            return
        }

        horizontalLinePattern.ifMatches(line) {
            elements.add(HorizontalLine)
            return
        }

        codeBlockPattern.ifMatches(line) {
            codeBlock = CodeBlock(false).also { elements.add(it) }
            return
        }
        plainBlockPattern.ifMatches(line) {
            plainBlock = CodeBlock(true).also { elements.add(it) }
        }

        blockQuotesPattern.ifMatches(line) {
            val remainder = groupValues[1]
            val existingBlockQuotes = elements.lastOrNull() as? BlockQuotes
            val blockQuotes = existingBlockQuotes ?: BlockQuotes(document, config).also { elements.add(it) }
            blockQuotes.parseLine(remainder)
            return
        }

        tableRowPattern.ifMatches(line) {
            val newTable = MarkdownTable(document, config, groupValues[1])
            elements.add(newTable)
            currentTable = newTable
            return
        }

        // Not a special line.
        // Don't add it yet, as the next line may be `===` or `---` indicating it is a heading.
        previousLine = LineElement().apply { include(line) }
    }

    internal fun finishPreviousLine() {
        (elements.lastOrNull() as? Block)?.finishPreviousLine()
        previousLine?.let {
            elements.add(it)
            previousLine = null
        }
    }


    fun render(flow: Flow) {
        for (element in elements) {
            renderElement(flow, element)
        }
    }

    private fun renderElement(flow: Flow, element: Element) {
        when (element) {

            is Heading -> {
                val actualLevel = min(element.level, config.headingHighlights.size) - 1
                flow.withHighlight(config.headingHighlights[actualLevel]) {
                    flow.lineBreak(1)
                    renderElement(flow, element.contents)
                    if (element.level < config.headingLevelWithoutBlankLine) {
                        flow.lineBreak(1)
                    } else {
                        flow.lineBreak(0)
                    }
                }
            }

            is CompoundElement -> {
                if (element is LineElement) {
                    flow.nextLine()
                }
                for (part in element.elements) {
                    renderElement(flow, part)
                }
            }

            is LineBreak -> {
                flow.lineBreak(0)
            }

            is BlankLine -> {
                // TODO Other contexts, such as a walled code block???
                flow.lineBreak(1)
            }

            is HorizontalLine -> {
                flow.lineBreak(0)
                flow.flow("─".repeat(config.columns))
                flow.lineBreak(0)
            }

            is Emphasis -> {
                val highlight = config.emphasisHighlights[element.style]
                flow.withHighlight(highlight) {
                    renderElement(flow, element.content)
                }
            }

            is Link -> {
                val highlight = config.emphasisHighlights["link"]
                flow.withLinkHighlight(element.link, highlight) {
                    flow.flow(element.text)
                }
            }

            is PlainText -> {
                flow.flow(element.text)
            }

            is CodeBlock -> {
                flow.startBlock()
                val highlight = if (element.plain) config.plainBlockHighlight else config.codeBlockHighlight
                flow.withHighlight(highlight) {
                    for (line in element.lines) {
                        flow.plainLine("    $line")
                        flow.lineBreak(0)
                    }
                }
                flow.lineBreak(1)
            }

            is BlockQuotes -> {
                flow.indent(config.blockQuoteIndentation.copy())
                for (line in element.elements) {
                    renderElement(flow, line)
                }
                flow.unindent()
            }

            is MarkdownTable -> {
                flow.startBlock()
                element.render(flow)
                flow.lineBreak(0)
            }
        }
    }


    override fun dump(indent: Int) {
        println(" ".repeat(indent * 4) + toString())
        for (element in elements) {
            element.dump(indent + 1)
        }
    }

    override fun toString() = "IntermediateDocument with ${elements.size} elements"
}

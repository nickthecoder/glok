/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.util.Clipboard.textContents
import java.awt.Toolkit
import java.awt.datatransfer.*
import java.awt.datatransfer.Clipboard as AWTClipboard

/**
 * Exposes the system clipboard for copy/paste operations.
 *
 * Currently only text is supported.
 *
 * Get/set the contents of text using [textContents].
 *
 */
actual object Clipboard : ClipboardOwner {

    private val awtClipboard = Toolkit.getDefaultToolkit().systemClipboard

    private val mutableContainsTextProperty = SimpleBooleanProperty(false)

    /**
     * Does the clipboard currently contain text.
     */
    actual val containsTextProperty: ObservableBoolean = mutableContainsTextProperty.asReadOnly()
    actual var containsText by mutableContainsTextProperty
        private set

    /**
     * The opposite of [containsTextProperty].
     * Use this as the `disabledProperty` for paste actions.
     */
    actual val noTextProperty = ! mutableContainsTextProperty

    private val flavorListener = FlavorListener {
        //containsText = ignoreStdErr { awtClipboard.getContents(this).isDataFlavorSupported(DataFlavor.stringFlavor) }
        containsText = awtClipboard.getContents(this).isDataFlavorSupported(DataFlavor.stringFlavor)
    }

    actual var textContents: String?
        get() {
            //val contents = ignoreStdErr { awtClipboard.getContents(this) }
            val contents = awtClipboard.getContents(this)
            if (contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    return contents.getTransferData(DataFlavor.stringFlavor) as String
                } catch (_: Exception) {
                }
            }
            return null
        }
        set(v) {
            val selection = StringSelection(v ?: "")
            awtClipboard.setContents(selection, this)
        }

    init {
        awtClipboard.addFlavorListener(flavorListener)
        containsText = awtClipboard.getContents(Clipboard).isDataFlavorSupported(DataFlavor.stringFlavor)
    }

    override fun lostOwnership(p0: AWTClipboard?, p1: Transferable?) {
    }

}
/*
I wrote this to work around the AWT bug which prints "error" messages to stderr.
But, I think the workaround may be worse than the problem it is trying to solve.
What if an application changes System.err in another Thread? It could happen while this is running!
FYI, I put "error" in quotes, because it isn't an error - it is expected behaviour for something on
another application's classpath to be absent from this application.

Here's a bug report from 2007. 17 years ago, and still causing problems.
IMHO, the fix for the bug is just to delete the error message.
https://bugs.java.com/bugdatabase/view_bug.do?bug_id=6606476
*/
/*
private val dummyStdErr = PrintStream(OutputStream.nullOutputStream())
private fun <R : Any> ignoreStdErr(block: () -> R): R {
    val oldStdErr = System.err
    System.setErr(dummyStdErr)
    return try {
        block()
    } finally {
        System.setErr(oldStdErr)
    }
}
*/

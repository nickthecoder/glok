package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.NodeInspector
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import java.io.File

class MarkdownFileViewTester : Application() {

    val showSourceProperty by booleanProperty(false)

    val sourceTextArea = styledTextArea {
        visibleProperty.bindTo(showSourceProperty)
    }

    val markdownView = MarkdownFileView()

    override fun start(primaryStage: Stage) {

        markdownView.fileProperty.addChangeListener { _, _, file ->
            sourceTextArea.text = file.readText()
        }

        markdownView.file = if (rawArguments.size == 1) {
            File(rawArguments[0])
        } else {
            File("/home/nick/projects/featureful/build/dokka/jekyll/index.md")
        }

        with(primaryStage) {
            title = "Markdown View Tester"
            scene = scene(1000, 800) {
                root = borderPane {

                    top = toolBar {
                        + button("Back") {
                            disabledProperty.bindTo(! markdownView.canGoBackProperty)
                            onAction { markdownView.back() }
                        }
                        + button("Forward") {
                            disabledProperty.bindTo(! markdownView.canGoForwardProperty)
                            onAction { markdownView.forward() }
                        }

                        + button("Save") {
                            onSave()
                        }
                        + toggleButton("Show Source") {
                            selectedProperty.bidirectionalBind(showSourceProperty)
                        }
                        + spacer()
                        + label("Columns : ")
                        + intSpinner {
                            editor.prefColumnCount = 3
                            valueProperty.bidirectionalBind(markdownView.columnsProperty)
                        }
                        + spacer()
                        + button("Inspect") {
                            onAction { NodeInspector.showDialog(markdownView, primaryStage) }
                        }
                    }

                    center = splitPane {
                        + sourceTextArea
                        + markdownView
                    }
                }
            }
            show()
        }
    }

    private fun onSave() {
        markdownView.file.writeText(sourceTextArea.text)
        markdownView.refresh()
    }

    companion object {

        @JvmStatic
        fun main(vararg args: String) {
            launch(MarkdownFileViewTester::class.java, args)
        }
    }
}

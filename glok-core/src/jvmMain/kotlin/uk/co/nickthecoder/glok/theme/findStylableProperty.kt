/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.property.StylableProperty
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Scene
import uk.co.nickthecoder.glok.util.log
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KClass

/**
 * Looks for a method with this signature : get{PropertyName}Property() : StylableProperty<*>
 * If one is found, invokes the method, and returns the value.
 * Otherwise, return null.
 *
 * Note, [node] is of type [Any], because it _could_ also be used to style other objects
 * e.g. For [Scene], to set [Scene.backgroundColor] (which is a [StylableProperty]).
 */
actual fun findStylableProperty(
    node: Any,
    valueType: KClass<*>,
    propertyName: String,
    selector: Selector
): StylableProperty<*>? {
    val nodeType = node.javaClass
    val methodName = "get${propertyName[0].uppercase() + propertyName.substring(1)}Property"
    try {
        val propertyMethod = nodeType.getMethod(methodName)
        if (StylableProperty::class.java.isAssignableFrom(propertyMethod.returnType)) {
            val property = propertyMethod.invoke(node) as StylableProperty<*>
            val foundValueType = property.kclass()
            return if (valueType == Image::class.java && foundValueType == Node::class.java) {
                // A special case for "graphic". The property is a Node, but the rule's value is an Image.
                // We allow it, even though the types don't match.
                // In StylableProperty there is also a special case, which converts the Image to an ImageView.
                property
            } else {
                if (foundValueType.java.isAssignableFrom(valueType.java)) {
                    property
                } else if (valueType.javaPrimitiveType == valueType.javaPrimitiveType) {
                    property
                } else {
                    log.warn("Expected : ${foundValueType.simpleName}, but found ${valueType.simpleName}")
                    log.warn("    Selector : $selector")
                    null
                }
            }

        } else {
            log.warn("Expected : StylableProperty, but found ${propertyMethod.returnType.simpleName}")
            log.warn("    $selector")
            return null
        }

        // Exceptions thrown by Class.getMethod and Method.invoke.
        // I don't expect any of these to be thrown unless the application programmer messed up.
    } catch (e: NoSuchMethodException) {
        log.warn("Problem with property $propertyName on $node (NoSuchMethodException)")
        log.warn("    Selector : $selector")
        return null
    } catch (e: SecurityException) {
        log.warn("Problem with property $propertyName on $node (SecurityException)")
        log.warn("    Selector : $selector")
        return null
    } catch (e: IllegalAccessException) {
        log.warn("Problem with property $propertyName on $node (IllegalAccessException)")
        log.warn("    Selector : $selector")
        return null
    } catch (e: IllegalArgumentException) {
        log.warn("Problem with property $propertyName on $node (IllegalArgumentException)")
        log.warn("    Selector : $selector")
        return null
    } catch (e: InvocationTargetException) {
        log.warn("Problem with property $propertyName on $node (InvocationTargetException)")
        log.warn("    Selector : $selector")
        return null
    }
}

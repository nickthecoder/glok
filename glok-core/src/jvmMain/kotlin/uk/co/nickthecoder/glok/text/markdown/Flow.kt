package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.Highlight
import kotlin.math.max

internal abstract class Flow {

    private val indentations = mutableListOf<MarkdownIndentation>()
    private var leftMargin = 0
    private var rightMargin = 0

    abstract val columns: Int

    private var column = 0
    private var spaceRequired = false


    protected abstract fun append(text: String)
    protected abstract fun appendNewLine()

    abstract fun endPosition(): TextPosition
    abstract fun addRange(link: String?, from: TextPosition, to: TextPosition, highlight: Highlight)

    private var isFirstLine = true
    private var newLinesBeforeNextFlow = 0

    fun startBlock() {
        while (newLinesBeforeNextFlow > 0) {
            appendNewLine()
            newLinesBeforeNextFlow --
            column = 0
            spaceRequired = false
        }
    }

    fun lineBreak(newLineCount: Int) {
        if (column == leftMargin) {
            newLinesBeforeNextFlow = max(newLineCount, newLinesBeforeNextFlow)
        } else {
            newLinesBeforeNextFlow = max(newLineCount + 1, newLinesBeforeNextFlow)
        }
    }

    fun nextLine() {
        if (column != 0) {
            spaceRequired = true
        }
    }

    fun plainLine( text : String ) {
        append( text )
        appendNewLine()
    }

    fun flow(text: String) {
        if (text.isBlank()) return

        spaceRequired = spaceRequired || text.first().isWhitespace()

        if (isFirstLine) {
            newLinesBeforeNextFlow = 0
            isFirstLine = false
        } else {
            // A heading will ask for 1 new line if the text follows immediately.
            // Or 2 new lines if there should be a blank line between the heading and plain text.
            // This is NOT used for indentations.
            while (newLinesBeforeNextFlow > 0) {
                appendNewLine()
                newLinesBeforeNextFlow --
                column = 0
                spaceRequired = false
            }
        }

        val parts = text.trim().split(Regex("\\s+"))
        for (part in parts) {
            if (column != 0 && column + part.length >= columns - rightMargin) {
                // Add a new line, because the text does not fit on the current line.
                endAllIndentations()
                appendNewLine()
                column = 0
                spaceRequired = false
            }
            if (spaceRequired) {
                append(" ")
                column ++
                spaceRequired = false
            }

            if (column == 0 && indentations.isNotEmpty()) {
                for (indentation in indentations) {
                    indentation.from = endPosition()
                    val from = endPosition()
                    append(indentation.prefix)
                    if (indentation.indentHighlight != null) {
                        val to = endPosition()
                        addRange(null, from, to, indentation.indentHighlight)
                    }
                    column += indentation.prefix.length
                }
            }

            updatePositions()
            append(part)
            spaceRequired = true
            column += part.length
        }
        spaceRequired = column != 0 && text.last().isWhitespace()
    }

    fun indent(indentation: MarkdownIndentation) {

        if (indentations.isEmpty()) {
            newLinesBeforeNextFlow = max(newLinesBeforeNextFlow, indentation.blankLinesBefore + 1)
        }

        // Blank lines for headings
        if (newLinesBeforeNextFlow > 0) {
            while (newLinesBeforeNextFlow > 0) {
                appendNewLine()
                newLinesBeforeNextFlow --
            }
            column = 0
            spaceRequired = false
        } else {

            // Do we need to start a new line before adding the indent?
            if (column != leftMargin) {
                endAllIndentations()
                appendNewLine()
                column = 0
                spaceRequired = false
            }
        }

        leftMargin += indentation.prefix.length
        rightMargin += indentation.suffix.length
        indentations.add(indentation)
    }

    fun unindent() {
        endAllIndentations()

        val old = indentations.last()
        leftMargin -= old.prefix.length
        rightMargin -= old.suffix.length

        indentations.removeLast()
        lineBreak(0)
        if (indentations.isEmpty()) {
            newLinesBeforeNextFlow = max(newLinesBeforeNextFlow, old.blankLinesAfter + 1)
        }
    }

    private fun endAllIndentations() {
        for (indentation in indentations.asReversed()) {

            if (indentation.fill) {
                val fillBy = columns - rightMargin - column
                // println("$columns - $rightMargin - $column (${endPosition().column}) = $fillBy")
                if (fillBy > 0) {
                    append(" ".repeat(fillBy))
                    column += fillBy
                }
            }
            if (indentation.suffix != "") {
                append(indentation.suffix)
                column += indentation.suffix.length
            }

            if (indentation.bodyHighlight != null) {
                val from = indentation.from
                if (from != null) {
                    val to = endPosition()
                    addRange(null, from, to, indentation.bodyHighlight)
                }
            }
        }
    }

    fun withHighlight(highlight: Highlight?, block: () -> Unit) {
        withAnyHighlight(null, highlight, block)
    }

    fun withLinkHighlight(link: String, highlight: Highlight?, block: () -> Unit) {
        withAnyHighlight(link, highlight, block)
    }

    /**
     * We want some text to be highlighted. However, due to Flow deciding when to add spaces
     * and newlines, this can be tricky!
     *
     * First, we assume that range's `from` is the end of the document.
     * But we also add to [updatePosition], so that when text is actually appended,
     * `from` is updated.
     *
     * [link] is the 'URL' when we are creating a [LinkHighlightRange],
     * and is `null` for other highlights.
     */
    private fun withAnyHighlight(link: String?, highlight: Highlight?, block: () -> Unit) {
        var from = endPosition()
        if (highlight != null) updatePosition.add { from = it }
        block()
        val to = endPosition()
        if (from != to && highlight != null) {
            addRange(link, from, to, highlight)
        }
    }

    /**
     * See [withAnyHighlight]
     */
    private val updatePosition = mutableListOf<(TextPosition) -> Unit>()

    /**
     * See [withAnyHighlight]
     */
    private fun updatePositions() {
        if (updatePosition.isNotEmpty()) {
            val end = endPosition()
            for (pos in updatePosition) {
                pos(end)
            }
            updatePosition.clear()
        }
    }

}

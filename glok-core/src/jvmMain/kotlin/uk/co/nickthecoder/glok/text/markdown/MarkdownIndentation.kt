package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.Highlight

class MarkdownIndentation(
    val prefix: String,
    val suffix: String = "",
    val fill: Boolean = false,
    val blankLinesBefore: Int = 1,
    val blankLinesAfter: Int = 1,
    val indentHighlight: Highlight?,
    val bodyHighlight: Highlight?
) {
    internal var from: TextPosition? = null

    internal fun copy() =
        MarkdownIndentation(prefix, suffix, fill, blankLinesBefore, blankLinesAfter, indentHighlight, bodyHighlight)
}

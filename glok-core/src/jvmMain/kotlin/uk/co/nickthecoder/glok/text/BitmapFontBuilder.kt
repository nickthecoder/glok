/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.util.Log
import uk.co.nickthecoder.glok.util.log
import java.awt.FontMetrics
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import kotlin.math.roundToInt
import java.awt.Font as AWTFont

/**
 * Example :
 *
 *     val verdana18 = createBitmapFontFromAWT( "Verdana", 18 ) {}
 *
 *     val verdanaBold18 = createBitmapFontFromAWT( "Verdana", 18, FontStyle.BOLD ) {
 *         padding( 4 )        // Put 4 pixels of padding around every glyph. The default is 2
 *         padding( 2, 4 )     // Alternatively, specify the x and y padding separately.
 *         includeGlyphs(  32, 256 )   // If you don't specify any ranges, it will assume 32 .. 256 (Latin1 characters)
 *         includeGlyphs( 300, 400 )   // Repeat as many times as needed.
 *     }
 */
internal actual fun createBitmapFont(
    identifier: FontIdentifier,
    block: (BitmapFontBuilder.() -> Unit)?
): BitmapFont {

    val awtFont = AWTFont(identifier.family, identifier.style.awtValue, identifier.size.toInt())

    val builder = CreateBitmapFontFromAWT(awtFont, identifier)
    if (block != null) {
        builder.block()
    }

    return builder.build()
}

/**
 * The internal workhorse behind [createBitmapFont].
 *
 * First, we get font metrics.
 *
 * Then each time [range] is called, we work out where each glyph in that range should be placed.
 * (but we don't draw anything yet).
 * This data is stored in [glyphData] (which will also be used by the [Font]).
 *
 * When [build] is called, we now know how big the texture needs to be.
 * We create a BufferedImage, and draw the glyphs using AWT.
 * We then create a Texture, and copy the BufferedImage to it. Delete the BufferedImage,
 * and return the [BitmapFont].
 */
internal class CreateBitmapFontFromAWT(

    private val awtFont: AWTFont,
    private val identifier: FontIdentifier

) : BitmapFontBuilder {

    private var xPadding: Int = 2
    private var yPadding: Int = 2
    private var maxTextureWidth: Int = 0

    private val glyphData = mutableMapOf<Char, BitmapGlyphData?>()

    private var requiredWidth: Int = 0

    private var requiredHeight: Int = 0

    /**
     * Only used to gather font metrics
     */
    private val prepareImage = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)

    private val graphics = prepareImage.createGraphics()

    private val metrics: FontMetrics

    /** The next X position to place a glyph in the texture.*/
    private var nextX: Int

    /** The next Y position to place a glyph in the texture. This is at the baseline of the glyph.*/
    private var nextY: Int

    /**
     * maxAscent + maxDescent + leading
     */
    private var maxHeight: Int

    init {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        graphics.font = this.awtFont
        metrics = graphics.fontMetrics
        requiredHeight = metrics.height
        maxHeight = Math.max(metrics.height, metrics.maxAscent + metrics.maxDescent + metrics.leading)
        nextX = xPadding
        nextY = yPadding + metrics.maxAscent
    }

    override fun padding(x: Int, y: Int) {
        nextX += x - xPadding
        nextY += y - yPadding
        xPadding = x
        yPadding = y
    }

    override fun maxTextureWidth(value: Int) {
        maxTextureWidth = value
    }

    override fun includeGlyphs(chars: Set<Char>) {
        if (maxTextureWidth == 0) {
            maxTextureWidth = (awtFont.size + xPadding * 2) * 12
        }

        // See comments below. Using these when GUESSING font metrics.
        val extraLeftForChars = "Jj"
        val extraRightForChars = "f"

        for (c in chars) {

            if (this.awtFont.canDisplay(c)) {

                val advance = metrics.charWidth(c).toFloat()
                val fixedWidth = metrics.charWidth('i') == metrics.charWidth('M')

                // This is a pure GUESS.
                // FontMetrics.getStringBounds().width is not correct. On my Linux machine, it always
                // returns the same as FontMetrics.charWidth() (the advance).
                // Therefore I have no data for the amount of pixels to the left of the `reference point`
                // that an italic `j` uses for left of its tail.
                val extraBoundsLeft =
                    if (awtFont.isItalic && ! fixedWidth && extraLeftForChars.contains(c)) metrics.descent.toFloat() else 0f
                // Similarly, I have no data for the amount of pixels beyond the advance.
                // i.e. for pixels in the top right of an italic `f`
                val extraBoundsRight =
                    if (awtFont.isItalic && ! fixedWidth && extraRightForChars.contains(c)) metrics.descent.toFloat() else 0f

                val glyphWidth = advance.toDouble() + extraBoundsLeft + extraBoundsRight

                // This proves that FontMetrics.getStringBounds() is not correct.
                // For italic fonts, the advance should be smaller than the bounds.
                // We SHOULD see message, but there are none.
                //val glyphWidth2 = metrics.getStringBounds("$c", graphics).width.toFloat()
                //if (advance != glyphWidth2) {
                //    println("advance != glyphWidth $advance vs $glyphWidth2. Font $awtFont glyph $c")
                //}

                if (nextX + glyphWidth + xPadding > maxTextureWidth) {
                    nextX = xPadding
                    nextY += maxHeight + yPadding
                    requiredHeight = nextY + metrics.descent + yPadding
                }

                glyphData[c] =
                    BitmapGlyphData(nextX.toFloat(), nextY.toFloat(), glyphWidth.toFloat(), advance, extraBoundsLeft)

                nextX += Math.ceil(glyphWidth).toInt() + xPadding * 2
                if (nextX > requiredWidth) {
                    requiredWidth = nextX
                }
            } else {
                // println("Glyph not found")
                glyphData[c] = null
            }

        }

    }

    fun build(): BitmapFont {
        // If no ranges were specified, use those specified in BitmapFont.requiredGlyphs
        if (glyphData.isEmpty()) {
            includeGlyphs(BitmapFont.requiredGlyphs)
        }

        val bufferedImage = BufferedImage(requiredWidth, requiredHeight, BufferedImage.TYPE_INT_ARGB)
        val graphics = bufferedImage.createGraphics()
        graphics.font = awtFont
        graphics.paint = java.awt.Color.WHITE

        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

        glyphData.forEach { (c, glyphData) ->
            if (glyphData != null) {
                graphics.drawString(c.toString(), glyphData.left + glyphData.extraBoundsLeft, glyphData.baseline)
            }
        }

        /* Get pixel data of image */
        val pixels = IntArray(requiredWidth * requiredHeight)
        bufferedImage.getRGB(0, 0, requiredWidth, requiredHeight, pixels, 0, requiredWidth)

        val texture = backend.createTexture("font_${awtFont.name}", requiredWidth, requiredHeight, pixels)

        return BitmapFont(
            identifier,
            texture,
            glyphData,
            height = metrics.height.toFloat(),
            ascent = metrics.maxAscent.toFloat(),
            descent = metrics.maxDescent.toFloat(),
            top = metrics.maxAscent.toFloat(),
            bottom = metrics.maxDescent.toFloat(),
            italicInverseSlope = awtFont.italicAngle
        )
    }

}

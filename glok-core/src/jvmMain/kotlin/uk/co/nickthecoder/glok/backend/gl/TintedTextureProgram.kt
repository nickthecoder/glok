package uk.co.nickthecoder.glok.backend.gl

import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.glok.scene.Color

internal class TintedTextureProgram : ShaderProgram(VERTEX, FRAGMENT, 4) {

    private val tintLocation = getUniformLocation("tint")

    fun setup(texture: Texture, tint: Color) {
        texture.bind()
        enablePositionUV()
        setUniform(tintLocation, tint)
        OpenGL.reportError()
    }

    fun draw(
        left: Float, bottom: Float, right: Float, top: Float,
        tLeft: Float, tBottom: Float, tRight: Float, tTop: Float
    ) {
        requiredTriangles(2)
        GLBackend.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)
    }

    companion object {
        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute vec2 uv;

            uniform mat3 viewMatrix;
            
            varying vec2 textureCoord;

            void main() {
                textureCoord = uv;

                gl_Position = vec4( viewMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val FRAGMENT = """
            #version 120
            
            varying vec2 textureCoord;

            uniform sampler2D texImage;
            uniform vec4 tint;

            void main() {
                gl_FragColor = tint * texture2D(texImage, textureCoord);
            }

            """.trimIndent()
    }
}
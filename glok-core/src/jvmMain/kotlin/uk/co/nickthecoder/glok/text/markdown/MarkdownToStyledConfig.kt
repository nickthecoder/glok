package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.ThemedHighlight

/**
 * Configuration options for [markdownToStyled].
 *
 * Everything apart from [columns] rarely need to be changed.
 *
 * To change the styled applied to text, there are two ways :
 *  1 Modify [headingHighlights], [emphasisHighlights]
 *    and the highlights in [blockQuoteIndentation]
 *  2 Create a new `Theme`, and redefined the styles ".heading1" etc.
 *
 * See the source code of `Tantalum` -> `STYLED_TEXT_AREA` -> `descendant(CONTAINER)` -> `child(TEXT)` { ... }
 */
class MarkdownToStyledConfig {

    /**
     * The number of columns before text wraps to the next line.
     *
     * Indentations with [MarkdownIndentation.fill] == `true` also uses this to calculate how much
     * additional whitespace needs to be added at the end of each line.
     */
    var columns: Int = 80

    /**
     * By default, heading 3 and beyond do NOT include a blank line between the heading and the following text.
     */
    var headingLevelWithoutBlankLine = 3

    /**
     * Highlights for headings.
     * If the document has a heading level greater than the size of this list, then the last entry is used.
     * e.g. the default list has 4 entries, so a level-5 heading will appear the same as a level-4 heading.
     */
    val headingHighlights: MutableList<Highlight> = mutableListOf(
        ThemedHighlight(".heading1"),
        ThemedHighlight(".heading2"),
        ThemedHighlight(".heading3"),
        ThemedHighlight(".heading4")
    )

    /**
     * Highlights for each type of emphasis. The key is one of `bold`, `boldItalic`, `italic` and `code`.
     *
     * You may remove entries, in which case the corresponding text will be un-styled.
     */
    val emphasisHighlights = mutableMapOf<String, Highlight>(
        "bold" to ThemedHighlight(".bold"),
        "boldItalic" to ThemedHighlight(".boldItalic"),
        "italic" to ThemedHighlight(".italic"),
        "code" to ThemedHighlight(".code"),
        "link" to ThemedHighlight(".link")
    )

    val codeBlockHighlight = ThemedHighlight(".blockQuoteIndent")
    val plainBlockHighlight = ThemedHighlight(".none")

    /**
     * The `indentHighlight` uses an `OverlappingBorder`, and therefore `prefix` can be just whitespace.
     * This indent uses `fill=true`, so that the entire block has a background color.
     * Without `fill=true`, the background color would have a jagged right edge.
     *
     * If you do not give the block a background color, then `fill=false` would look fine.
     */
    var blockQuoteIndentation =
        MarkdownIndentation("  ", " ", true, 1, 1, ThemedHighlight(".blockQuoteIndent"), ThemedHighlight(".blockQuote"))

}

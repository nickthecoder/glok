package uk.co.nickthecoder.glok.text.markdown

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.collections.sizeProperty
import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.fileProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.property.functions.greaterThan
import uk.co.nickthecoder.glok.property.functions.lessThan
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.styledTextArea
import java.io.File

/**
 * Displays a [File] converting the contents from Markdown into plain-text with highlights.
 */
class MarkdownFileView : Region() {

    val fileProperty by fileProperty(File(""))
    var file by fileProperty

    /**
     * The default line height is 1.0, so that lines which make up tables look solid.
     *
     * Note that [StyledTextArea]'s default is 1.1
     */
    val lineHeightProperty by stylableFloatProperty(1f)
    var lineHeight by lineHeightProperty

    val columnsProperty by intProperty(100)
    var columns by columnsProperty

    private val textArea = styledTextArea {
        lineHeightProperty.bindTo(this@MarkdownFileView.lineHeightProperty)
        readOnly = true
    }

    private val history = mutableListOf<File>().asMutableObservableList()

    private val currentHistoryIndexProperty by intProperty(- 1)
    private var currentHistoryIndex by currentHistoryIndexProperty


    val canGoBackProperty: ObservableBoolean = currentHistoryIndexProperty.greaterThan(0)
    val canGoBack by canGoBackProperty

    val canGoForwardProperty: ObservableBoolean =
        currentHistoryIndexProperty.lessThan(history.sizeProperty() - 1)
    val canGoForward by canGoForwardProperty

    override val children: ObservableList<Node> = listOf(textArea).asObservableList()

    init {
        claimChildren()

        fileProperty.addListener { fileChanged() }
        columnsProperty.addListener { load() }
        textArea.onMouseClicked { mouseClicked(it) }
    }

    fun back() {
        if (canGoBack) {
            currentHistoryIndex --
            file = history[currentHistoryIndex]
        }
    }

    fun forward() {
        if (canGoForward) {
            currentHistoryIndex ++
            file = history[currentHistoryIndex]
        }
    }

    private fun fileChanged() {
        if (currentHistoryIndex >= 0 && file != history[currentHistoryIndex]) {
            while (history.size > currentHistoryIndex + 1) {
                history.removeLast()
            }
        }
        if (currentHistoryIndex < 0 || history[currentHistoryIndex] != file) {
            history.add(file)
            currentHistoryIndex = history.size - 1
        }
        load()
    }

    fun refresh() {
        load()
    }

    private fun mouseClicked(event: MouseEvent) {
        val pos = textArea.positionForMouseEvent(event)
        val linkRange = textArea.document.ranges.firstOrNull { range ->
            pos >= range.from && pos <= range.to && range is LinkHighlightRange
        } as? LinkHighlightRange

        if (linkRange != null) {
            val link = linkRange.link
            if (link.startsWith("http:") || link.startsWith("https:")) {
                // TODO Open link in a browser.
            } else {
                val linkFile = File(link)
                if (linkFile.isAbsolute) {
                    if (linkFile.isFile) {
                        file = linkFile
                    }
                } else {
                    val newFile = File(file.parentFile, link).absoluteFile
                    if (newFile.isFile) {
                        file = newFile
                    }
                }
            }
            event.consume()
        }
    }

    override fun layoutChildren() {
        setChildBounds(textArea, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    private fun load() {
        if (file.exists() && file.isFile) {
            val config = MarkdownToStyledConfig().apply {
                columns = this@MarkdownFileView.columns
            }

            markdownToStyled(file.readLines(), textArea.document, config)
        }
    }
}

package uk.co.nickthecoder.glok.backend.gl

import org.joml.Matrix3x2f
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.EXTFramebufferObject.*
import org.lwjgl.opengl.GL.createCapabilities
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL32.GL_TEXTURE_2D_MULTISAMPLE
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.glok.backend.*
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.util.*
import java.nio.ByteBuffer
import java.nio.FloatBuffer
import java.util.*

internal class GLBackend : Backend {

    /**
     * This window is never visible. It's created to be the "main" context, which all subsequent windows share.
     * So a texture created at a later date will be available to all windows (regardless of who created it).
     *
     * NOTE, it would be purer, if each Application had its own context
     * (now that we can launch a new Application when the old one ends).
     * But to do so, would need much care. For example, fonts and icons would need to be re-created
     * in the new context.
     * So for now, there is only this one shared context.
     */
    internal var setupWindow: GLWindow = GLWindow(100, 100, MemoryUtil.NULL).apply {
        makeCurrent()
        createCapabilities()
    }

    private val viewMatrix = Matrix3x2f()

    private val textureProgram = TextureProgram()
    private val tintedTextureProgram = TintedTextureProgram()
    private val transformedTextureProgram = TransformedTextureProgram()
    private val flatColorProgram = FlatColorProgram()
    private val fillCircleProgram = FillCircleProgram()
    private val strokeCircleProgram = StrokeCircleProgram()
    private val gradientProgram = GradientProgram()
    private val HSVGradientProgram = HSVGradientProgram()

    private val subs = listOf(
        textureProgram, tintedTextureProgram, transformedTextureProgram, flatColorProgram,
        fillCircleProgram, strokeCircleProgram, gradientProgram
    )

    private var scale: Float = 1f
    private var currentWindow: GLWindow = setupWindow
        set(v) {
            field = v
            v.makeCurrent()
        }

    private val clipStack = Stack<GLClipRegion>()

    private var inView = false

    init {
        vertexBuffer = VertexBuffer()
        /* Upload null data to allocate storage for the VBO */
        val size = (floatBuffer.capacity() * java.lang.Float.BYTES).toLong()
        vertexBuffer.uploadData(size)
    }

    override fun reportError() = OpenGL.reportError()

    override fun processEvents(timeoutSeconds: Double) {
        glfwWaitEventsTimeout(timeoutSeconds)
        OpenGL.reportError()
    }

    private val cursors by lazy {
        mapOf(
            MousePointer.ARROW to glfwCreateStandardCursor(GLFW_ARROW_CURSOR),
            MousePointer.I_BEAM to glfwCreateStandardCursor(GLFW_IBEAM_CURSOR),
            MousePointer.CROSS_HAIR to glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR),
            MousePointer.POINTING_HAND to glfwCreateStandardCursor(GLFW_POINTING_HAND_CURSOR),
            MousePointer.RESIZE_EW to glfwCreateStandardCursor(GLFW_RESIZE_EW_CURSOR),
            MousePointer.RESIZE_NS to glfwCreateStandardCursor(GLFW_RESIZE_NS_CURSOR),
            MousePointer.RESIZE_NWSE to glfwCreateStandardCursor(GLFW_RESIZE_NWSE_CURSOR),
            MousePointer.RESIZE_NESW to glfwCreateStandardCursor(GLFW_RESIZE_NESW_CURSOR),
            MousePointer.RESIZE_ALL to glfwCreateStandardCursor(GLFW_RESIZE_ALL_CURSOR),
            MousePointer.NOT_ALLOWED to glfwCreateStandardCursor(GLFW_NOT_ALLOWED_CURSOR),
            MousePointer.HAND to glfwCreateStandardCursor(GLFW_HAND_CURSOR)
        )
    }

    override fun setMousePointer(window: Window, cursor: MousePointer) {
        cursors[cursor]?.let {
            glfwSetCursor((window as GLWindow).handle, it)
        }
    }

    /**
     * Called at the start and end of a view, in an attempt to ensure there are no side effects
     * which will screw up our rendering, or the application's.
     */
    private fun reset() {
        vertexBuffer.bind()
        if (clipStack.isNotEmpty()) {
            log.warn("clipStack not empty")
            clipStack.clear()
        }
        currentShader?.let {
            log.warn("currentShader = $currentShader. Expected null")
            currentShader?.unuse()
            currentShader = null
        }
        currentTexture = null
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glDisable(GL_SCISSOR_TEST)
        OpenGL.reportError()
    }

    override fun drawFrame(window: Window, block: () -> Unit) {
        currentWindow = window as GLWindow
        glViewport(0, 0, window.width, window.height)

        try {
            block()
        } finally {
            window.flip()
            currentWindow = setupWindow
        }
    }

    override fun drawView(width: Float, height: Float, scale: Float, block: () -> Unit) {
        inView = true
        currentTransformation = null

        this.scale = scale
        reset()

        viewMatrix.set(
            2f / width, 0f,
            0f, 2f / -height,
            -1f, 1f
        )
        OpenGL.reportError()

        try {
            block()
        } finally {
            reset()
            OpenGL.reportError()
            inView = false
        }
    }

    override fun saveRestoreState(block: () -> Unit) {
        if (inView) {
            vertexBuffer.unbind()
            glDisable(GL_SCISSOR_TEST)
            GLTexture.unbind()
        }

        super.saveRestoreState(block)

        if (inView) {
            val clip = clipStack.lastOrNull()
            if (clip == null) {
                glDisable(GL_SCISSOR_TEST)
            } else {
                glEnable(GL_SCISSOR_TEST)
                glScissor(clip.left, clip.bottom, clip.width, clip.height)
            }
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
            vertexBuffer.bind()
            glViewport(0, 0, currentWindow.width, currentWindow.height)
        }
    }

    // ===== Draw =====

    override fun clear(color: Color) {
        glClearColor(color.red, color.green, color.blue, color.alpha)
        glClear(GL_COLOR_BUFFER_BIT)
        OpenGL.reportError()
    }

    override fun beginClipping(left: Float, top: Float, width: Float, height: Float): Boolean {
        val window = currentWindow

        // Values left,top, width, height are in Glok's logical units.
        // Values with a "scaled" prefix are in physical pixels.

        val scaledLeft = left * scale
        val scaledWidth = width * scale
        val scaledHeight = height * scale
        val scaledBottom = window.height - top * scale - scaledHeight
        val currentTransformation : Matrix3x2f? = currentTransformation
        val requestedClipRegion = if (currentTransformation == null) {
            // This is vastly more common than the "else" branch.
           GLClipRegion(scaledLeft.toInt(), scaledBottom.toInt(), scaledWidth.toInt(), scaledHeight.toInt())
        } else {
            // There's a transformation, so the clipping has to be transformed too.
            // So let's transform each of our 4 corners into scene coordinates (still in Glok logical units),
            // with (0,0) being the top left of the window.

            val t0 = currentTransformation.times(left, top)
            val t1 = currentTransformation.times(left + width, top)
            val t2 = currentTransformation.times(left + width, top + height)
            val t3 = currentTransformation.times(left, top + height)

            // Note, the final clipping may be a LARGER area than the untransformed one if there is a rotation,
            // as clipping uses axis aligned rectangles.
            val tLeft = min(min(t0.first, t1.first), min(t2.first, t3.first))
            val tRight = max(max(t0.first, t1.first), max(t2.first, t3.first))
            val tTop = min(min(t0.second, t1.second), min(t2.second, t3.second))
            val tBottom = max(max(t0.second, t1.second), max(t2.second, t3.second))

            // Now convert them into physical pixel coordinates, with (0,0) being the bottom left.
            val tScaledLeft = tLeft * scale
            val tScaledWidth = (tRight - tLeft) * scale
            val tScaledHeight = (tBottom - tTop) * scale
            val tScaledBottom = window.height - tTop * scale - tScaledHeight

            GLClipRegion(tScaledLeft.toInt(), tScaledBottom.toInt(), tScaledWidth.toInt(), tScaledHeight.toInt())
        }


        val newClipRegion = if (clipStack.isNotEmpty()) {
            requestedClipRegion.intersection(clipStack.peek())
        } else {
            requestedClipRegion
        }
        if (newClipRegion.width <= 0 || newClipRegion.height <= 0) return false

        if (clipStack.isEmpty()) {
            glEnable(GL_SCISSOR_TEST)
        }
        clipStack.push(newClipRegion)
        glScissor(newClipRegion.left, newClipRegion.bottom, newClipRegion.width, newClipRegion.height)
        OpenGL.onError {
            log.error("Illegal clip region? $newClipRegion")
        }

        // To debug clipping, draw a HUGE rectangle, and only the clipped portion should be displayed.
        // fillRect(-10000f, -100000f, 10000f, 10000f, Color.RED.opacity(0.5f))

        return true
    }

    override fun endClipping() {
        if (clipStack.empty()) {
            throw GlokException("Unmatched beginClipping / endClipping")
        } else {
            clipStack.pop()
            if (clipStack.empty()) {
                glDisable(GL_SCISSOR_TEST)
            } else {
                val clipRegion = clipStack.peek()
                glScissor(clipRegion.left, clipRegion.bottom, clipRegion.width, clipRegion.height)
                OpenGL.onError {
                    log.error("Illegal clip region? $clipRegion")
                }
            }
        }
    }

    override fun noClipping(block: () -> Unit) {
        glDisable(GL_SCISSOR_TEST)
        block()
        if (clipStack.isNotEmpty()) {
            glEnable(GL_SCISSOR_TEST)
        }
    }

    /**
     * Transforms from Glok logical units from the perspective of the transformed nodes to
     * Glok logical units relative to the scene.
     * i.e. after the transformation is applied, we have Glok logical units, where (0,0) is the top left of
     * the window.
     */
    private var currentTransformation: Matrix3x2f? = null
    private var plainViewMatrix: Matrix3x2f? = null
    override fun transform(transformation: Matrix, block: () -> Unit) {
        val oldMatrix = Matrix3x2f(viewMatrix)
        val oldTransformation = currentTransformation
        currentTransformation =
            if (oldTransformation == null) transformation.toJoml() else Matrix3x2f(oldTransformation).mul(transformation.toJoml())
        viewMatrix.mul(transformation.toJoml())
        try {
            block()
        } finally {
            viewMatrix.set(oldMatrix)
            currentTransformation = oldTransformation
        }
    }

    override fun transform(transformation: Matrix) {
        if (currentTransformation != null) plainViewMatrix = Matrix3x2f(viewMatrix)
        val oldTransformation = currentTransformation
        currentTransformation =
            if (oldTransformation == null) transformation.toJoml() else Matrix3x2f(oldTransformation).mul(transformation.toJoml())
        viewMatrix.mul(transformation.toJoml())
    }

    override fun clearTransform() {
        currentTransformation = null
        plainViewMatrix?.let { viewMatrix.set(it) }
    }

    override fun fillRect(rect: GlokRect, color: Color) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color)
            fillRect(rect)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun fillRect(
        left: Float, top: Float, right: Float, bottom: Float,
        color: Color
    ) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color)
            fillRect(left, top, right, bottom)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun fillQuarterCircle(
        x1: Float, y1: Float, x2: Float, y2: Float,
        color: Color, radius: Float,
    ) {
        with(fillCircleProgram) {
            use(viewMatrix)
            setupAndFill(radius, color, x1, y1, x2, y2)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun strokeQuarterCircle(
        x1: Float, y1: Float, x2: Float, y2: Float,
        color: Color, radius: Float, innerRadius: Float,
    ) {
        with(strokeCircleProgram) {
            use(viewMatrix)
            setupAndStroke(radius, innerRadius, color, x1, y1, x2, y2)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun strokeInsideRect(rect: GlokRect, thickness: Float, color: Color) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color)
            strokeInsideRect(rect, thickness)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun strokeInsideRect(
        left: Float, top: Float, right: Float, bottom: Float,
        thickness: Float, color: Color
    ) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color)
            strokeInsideRect(left, top, right, bottom, thickness)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun gradient(triangles: FloatArray, colors: Array<Color>) {
        with(gradientProgram) {
            use(viewMatrix)
            setup()
            gradient(triangles, colors)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun hsvGradient(triangles: FloatArray, colors: Array<FloatArray>) {
        with(HSVGradientProgram) {
            use(viewMatrix)
            setup()
            gradient(triangles, colors)
            unuse()
        }
        OpenGL.reportError()
    }

    override fun batch(texture: Texture, tint: Color?, modelMatrix: Matrix?, block: TextureBatch.() -> Unit) {
        if (modelMatrix == null && tint == null) {
            with(textureProgram) {
                use(viewMatrix)
                setup(texture)
                val tWidth = texture.width
                val tHeight = texture.height
                object : TextureBatch {
                    override fun draw(
                        srcX: Float,
                        srcY: Float,
                        width: Float,
                        height: Float,
                        destX: Float,
                        destY: Float,
                        destWidth: Float,
                        destHeight: Float
                    ) {
                        val tLeft = srcX / tWidth
                        val tBottom = srcY / tHeight
                        val tRight = (srcX + width) / tWidth
                        val tTop = (srcY + height) / tHeight
                        this@with.draw(
                            destX,
                            destY,
                            destX + destWidth,
                            destY + destHeight,
                            tLeft,
                            tBottom,
                            tRight,
                            tTop
                        )
                    }
                }.block()
                unuse()
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(texture, tint, modelMatrix?.toJoml())
                val tWidth = texture.width
                val tHeight = texture.height
                object : TextureBatch {
                    override fun draw(srcX: Float, srcY: Float, width: Float, height: Float, destX: Float, destY: Float, destWidth: Float, destHeight: Float) {
                        val tLeft = srcX / tWidth
                        val tBottom = srcY / tHeight
                        val tRight = (srcX + width) / tWidth
                        val tTop = (srcY + height) / tHeight
                        this@with.draw(destX, destY, destX + destWidth, destY + destHeight, tLeft, tBottom, tRight, tTop)
                    }
                }.block()
                unuse()
            }

        }
    }

    override fun drawTexture(
        texture: Texture,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float, destHeight: Float,
        modelMatrix: Matrix?
    ) {
        // We need to convert `srcX`, `srcY`, `width` and `height` from pixel coordinates to GL's coordinates in the range 0..1
        val tWidth = texture.width
        val tHeight = texture.height
        val tLeft = srcX / tWidth
        val tBottom = srcY / tHeight
        val tRight = (srcX + width) / tWidth
        val tTop = (srcY + height) / tHeight

        if (modelMatrix == null) {
            with(textureProgram) {
                use(viewMatrix)
                setup(texture)
                draw(destX, destY, destX + destWidth, destY + destHeight, tLeft, tBottom, tRight, tTop)
                unuse()
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(texture, null, modelMatrix.toJoml())
                draw(destX, destY, destX + destWidth, destY + destHeight, tLeft, tBottom, tRight, tTop)
                unuse()
            }
        }
        OpenGL.reportError()
    }

    override fun drawTintedTexture(
        texture: Texture,
        tint: Color,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float, destHeight: Float,
        modelMatrix: Matrix?
    ) {
        // We need to convert `srcX`, `srcY`, `width` and `height` from pixel coordinates to GL's coordinates in the range 0..1
        val tWidth = texture.width
        val tHeight = texture.height
        val tLeft = srcX / tWidth
        val tBottom = srcY / tHeight
        val tRight = (srcX + width) / tWidth
        val tTop = (srcY + height) / tHeight

        if (modelMatrix == null) {
            with(tintedTextureProgram) {
                use(viewMatrix)
                setup(texture, tint)
                draw(destX, destY, destX + destWidth, destY + destHeight, tLeft, tBottom, tRight, tTop)
                unuse()
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(texture, tint, modelMatrix.toJoml())
                draw(destX, destY, destX + destWidth, destY + destHeight, tLeft, tBottom, tRight, tTop)
                unuse()
            }
        }
        OpenGL.reportError()
    }

    // Factory methods

    override fun createWindow(width: Int, height: Int): Window {
        return try {
            GLWindow(width, height, setupWindow.handle)
        } finally {
            OpenGL.reportError()
        }
    }

    override fun createTexture(name: String, width: Int, height: Int, pixels: IntArray): Texture {
        /* Put pixel data into a ByteBuffer, ensuring it is in RGBA order */
        val buffer = MemoryUtil.memAlloc(width * height * 4)
        for (y in 0 until height) {
            for (x in 0 until width) {
                /* Pixel format is : 0xAARRGGBB */
                val pixel = pixels[y * width + x]
                buffer.put((pixel shr 16 and 0xFF).toByte()) // R
                buffer.put((pixel shr 8 and 0xFF).toByte())  // G
                buffer.put((pixel and 0xFF).toByte())        // B
                buffer.put((pixel shr 24 and 0xFF).toByte()) // A
            }
        }
        buffer.flip()

        val texture = GLTexture(name, width, height)
        glBindTexture(GL_TEXTURE_2D, texture.handle)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        MemoryUtil.memFree(buffer)

        OpenGL.reportError()

        return texture
    }

    // ===== Other =====

    override fun monitorSize(): Pair<Int, Int>? {
        val mode = glfwGetVideoMode(glfwGetPrimaryMonitor())
        OpenGL.reportError()
        return if (mode == null) null else Pair(mode.width(), mode.height())
    }

    override fun resources(path: String) = GLJarResources(path)

    override fun fileResources(path: String) = GLFileResources(path)

    fun imageToBytBufferRGBA(window: GLWindow, image: Image, tint: Color? = null): ByteBuffer {

        if (tint == null && image is GLTexture) {
            return image.toByteBufferRBGA()
        }

        val width = image.imageWidth.toInt()
        val height = image.imageHeight.toInt()
        val texture = GLTexture("temp", width, height).apply {
            bind()
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, null as ByteBuffer?)
            GLTexture.unbind()
        }

        // Draw the image onto the NEW texture
        val glBackend = backend as GLBackend
        val outputFBO = glGenFramebuffersEXT()

        currentWindow = window
        glViewport(0, 0, width, height)

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, outputFBO)
        glFramebufferTexture2DEXT(
            GL_FRAMEBUFFER_EXT,
            GL_COLOR_ATTACHMENT0_EXT,
            GL_TEXTURE_2D,
            texture.handle,
            0
        )
        currentTransformation = null
        scale = 1f
        viewMatrix.set(
            2f / width, 0f,
            0f, 2f / height,
            - 1f, - 1f
        )
        image.draw(0f, 0f, tint)

        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
        glDeleteFramebuffersEXT(outputFBO)
        currentWindow = setupWindow

        val result = texture.toByteBufferRBGA()

        glDeleteTextures(texture.handle)
        return result
    }

    // ===== Companion Object =====

    companion object {

        lateinit var vertexBuffer: VertexBuffer

        internal var currentShader: ShaderProgram? = null
        internal var currentTexture: Texture? = null

        // We use one float buffer for all rendering.
        internal var floatBuffer: FloatBuffer = MemoryUtil.memAllocFloat(40960)

    }

}

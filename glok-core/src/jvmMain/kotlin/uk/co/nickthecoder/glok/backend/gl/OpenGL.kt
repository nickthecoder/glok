package uk.co.nickthecoder.glok.backend.gl

import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL11.glGetError
import uk.co.nickthecoder.glok.util.ignoreErrors
import uk.co.nickthecoder.glok.util.log

internal object OpenGL {

    internal fun begin() {
        log.info("Starting OpenGL")
        if (!GLFW.glfwInit()) {
            throw IllegalStateException("Unable to initialize GLFW")
        }
        GLFWErrorCallback.createPrint(System.err).set()
    }


    fun end() {
        log.info("Ending OpenGL")
        // Terminate GLFW and free the error callback
        ignoreErrors { GLFW.glfwTerminate() }
        ignoreErrors { GLFW.glfwSetErrorCallback(null)?.free() }
    }

    fun clearError() {
        glGetError()
    }

    fun reportError() : Boolean {
        val error = glGetError()
        if (error != 0) {
            log.severe("GL Error $error")
            return true
        }
        return false
    }

    inline fun onError(action: (Int) -> Unit) {
        val error = glGetError()
        if (error != 0) {
            action(error)
        }
    }
}

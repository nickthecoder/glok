/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.SELECTED
import uk.co.nickthecoder.glok.theme.styles.TOGGLE_BUTTON

/**
 * Behaves like a [ToggleButton], but instead of a [ToggleGroup] it has a [Property] backing which button is selected.
 *
 * Generic Types :
 *
 * * [V] is a simple data type, such as String, or String?.
 * * [P] is a Property whose value is of type [V], such as StringProperty or OptionalStringProperty.
 *
 * When a selected [PropertyToggleButton] is pressed, the [property] is set to null
 * no items are selected.
 *
 * In contrast, clicking a selected [PropertyRadioButton], the [property]'s value remains unchanged,
 * and the button remains selected.
 */
class PropertyToggleButton<V, P : Property<V?>>(
    val property: P,
    /**
     * This button is selected when [property].value == [value]
     */
    val value: V,
    text: String,
    graphic: Node? = null

) : SelectButtonBase(text, graphic) {

    override val selectedProperty by booleanProperty(false)
    override var selected by selectedProperty

    init {
        selected = property.value == value
        style(TOGGLE_BUTTON)
        pseudoStyleIf(selected, SELECTED)

        selectedProperty.addChangeListener { _, _, selected ->
            pseudoStyleIf(selected, SELECTED)
            if (selected) {
                property.value = value
            } else if (property.value == value) {
                property.value = null
            }
        }

        property.addChangeListener { _, _, propValue -> selected = propValue == value }

    }

    override fun toggle() {
        property.value = if (property.value == value) null else value
    }

}

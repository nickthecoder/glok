
// **** Autogenerated. Do NOT edit. ****

package uk.co.nickthecoder.glok.property.boilerplate

import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.scene.Image
import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.documentation.Boilerplate
import uk.co.nickthecoder.glok.documentation.PropertyFunctions 


// region ==== Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ObservableValue<Image>`, use `ObservableImage`.
 */
interface ObservableImage: ObservableValue<Image> {}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Image>`, use `ReadOnlyImageProperty`.
 */
interface ReadOnlyImageProperty : ObservableImage, ReadOnlyProperty<Image>

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `Property<Image>`, use `ImageProperty`.
 */
interface ImageProperty : Property<Image>, ReadOnlyImageProperty {

    /**
     * Returns a read-only view of this mutable ImageProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by ImageProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyImageProperty = ReadOnlyImagePropertyWrapper( this )
}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `SimpleProperty<Image>`, we can use `SimpleImageProperty`.
 */
open class SimpleImageProperty(initialValue: Image, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Image>(initialValue, bean, beanName), ImageProperty

/**
 * Never use this class directly.
 * Use [ImageProperty.asReadOnly] to obtain a read-only version of a mutable [ImageProperty].
 *
 * [Boilerplate] which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Image, Property<Image>>`, use `ReadOnlyImagePropertyWrapper`.
 */
class ReadOnlyImagePropertyWrapper(wraps: ImageProperty) :
    ReadOnlyPropertyWrapper<Image, Property<Image>>(wraps), ReadOnlyImageProperty


// endregion Basic Features

// region ==== Optional Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ObservableImage], but the [value] can also be `null`.
 */
interface ObservableOptionalImage : ObservableValue<Image?> {
    fun defaultOf( defaultValue : Image ) : ObservableImage = ImageUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyImageProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalImageProperty : ObservableOptionalImage, ReadOnlyProperty<Image?>

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ImageProperty], but the [value] can also be `null`.
 */
interface OptionalImageProperty : Property<Image?>, ReadOnlyOptionalImageProperty {

    /**
     * Returns a read-only view of this mutable OptionalImageProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by optionalImageProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalImageProperty = ReadOnlyOptionalImagePropertyWrapper( this )

}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [SimpleImageProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalImageProperty(initialValue: Image?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Image?>(initialValue, bean, beanName), OptionalImageProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ValidatedImageProperty], but the [value] can also be `null`.
 */
open class OptionalValidatedImageProperty(initialValue: Image?, bean: Any? = null, beanName: String? = null, validation: (ValidatedProperty<Image?>,Image?,Image?)->Image?) :
    ValidatedProperty<Image?>(initialValue, bean, beanName, validation), OptionalImageProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyImagePropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalImagePropertyWrapper(wraps: OptionalImageProperty) :
    ReadOnlyPropertyWrapper<Image?, Property<Image?>>(wraps), ReadOnlyOptionalImageProperty

// endregion optional basic features

// region ==== Delegate ====

/**
 * A Kotlin `delegate` to create a [ImageProperty] (the implementation will be a [SimpleImageProperty].
 * Typical usage :
 *
 *     val fooProperty by imageProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun imageProperty(initialValue: Image) = PropertyDelegate<Image,ImageProperty>(initialValue) { bean, name, value ->
    SimpleImageProperty(value, bean, name)
}


//endregion Delegate

// region ==== Optional Delegate ====
/**
 * A Kotlin `delegate` to create an [OptionalImageProperty] (the implementation will be a [SimpleOptionalImageProperty].
 * Typical usage :
 *
 *     val fooProperty by optionalImageProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun optionalImageProperty(initialValue: Image?) = PropertyDelegate<Image?,OptionalImageProperty>(initialValue) { bean, name, value ->
    SimpleOptionalImageProperty(value, bean, name)
}

// endregion Optional Delegate

// region ==== Functions ====

/**
 * [Boilerplate] which avoids having to use generics.
 * An [ObservableImage] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction] ([PropertyFunctions]).
 */
class ImageUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Image) :
    UnaryFunction<Image, A, OA>(argA, lambda), ObservableImage

/**
 * [Boilerplate] which avoids having to use generics.
 * An [ObservableImage] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction] ([PropertyFunctions]).
 */
class ImageBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Image) :
    BinaryFunction<Image, A, OA, B, OB>(argA, argB, lambda), ObservableImage

/**
 * [Boilerplate] which avoids having to use generics.
 * An [ObservableImage] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction] ([PropertyFunctions]).
 */
class ImageTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Image
) : TernaryFunction<Image, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableImage

// endregion Functions

// region ==== Optional Functions ====

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ImageUnaryFunction], but the [value] can also be `null`.
 */
class OptionalImageUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Image?) :
    UnaryFunction<Image?, A, OA>(argA, lambda), ObservableOptionalImage

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ImageBinaryFunction], but the [value] can also be `null`.
 */
class OptionalImageBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Image?) :
    BinaryFunction<Image?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalImage

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ImageTernaryFunction], but the [value] can also be `null`.
 */
class OptionalImageTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Image?
) : TernaryFunction<Image?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalImage

// endregion Optional Functions

    
// region ==== Stylable ====
/**
 * [Boilerplate] which avoids having to use generics.
 *
 * A subclass of [SimpleImageProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableImageProperty( initialValue: Image, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Image>(initialValue, bean, beanName), ImageProperty {
    override fun kclass() : KClass<*> = Image::class
}

fun stylableImageProperty(initialValue: Image) = PropertyDelegate<Image,StylableImageProperty>(initialValue) { bean, name, value ->
    StylableImageProperty(value, bean, name)
}
// endregion Stylable


// region ==== Optional Stylable ====
/**
 * [Boilerplate] which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalImageProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalImageProperty(initialValue: Image?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Image?>(initialValue, bean, beanName), OptionalImageProperty {
    override fun kclass() : KClass<*> = Image::class
}

fun stylableOptionalImageProperty(initialValue: Image?) = PropertyDelegate<Image?,StylableOptionalImageProperty>(initialValue) { bean, name, value ->
    StylableOptionalImageProperty(value, bean, name)
}

// endregion Optional Stylable



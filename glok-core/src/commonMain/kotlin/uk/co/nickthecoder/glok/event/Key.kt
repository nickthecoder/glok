package uk.co.nickthecoder.glok.event

/**
 * Each instance represents a physical key on a keyboard.
 * i.e. a piece of plastic with one or more symbols printed on it, and a switch underneath ;-)
 * Not be confused with a `Character` or a [KeyCombination].
 *
 * When a key has two (or more) symbols printed on it, the name here is for the lower symbol
 * (i.e. when `shift` is not pressed). e.g. There is no `PLUS`, because `plus` is
 * `Shift` + [EQUALS].
 *
 * ### Alt keys
 *
 * The two `Alt` keys are normally labelled `Alt` and `AltGr`.
 * Unlike the two `Shift` keys or the two `Control` keys, the two `Alt` keys sometimes behave differently
 * (which is why they have two different labels).
 * But here, I've ignored this, and called them [LEFT_ALT] and [RIGHT_ALT] for consistency with `Shift` and `Control`.
 *
 * Elsewhere, [LEFT_ALT] is often referred to as just `ALT`, and [RIGHT_ALT] as `ALT_GRAPH`.
 *
 * ### UK Keyboards
 *
 * The `#` key is reported as [BACKSLASH] and the `\` key is reported as [WORLD_1].
 * I think this mapping has changed; I think `#` used to be reported as [WORLD_1].
 * FYI, Glok uses whatever GLFW sends it. So any weirdness like this, is either a problem with GLFW,
 * or your keyboard layout settings.
 *
 * ### Comparison with JavaFX
 *
 * Glok's [Key] is somewhat similar to JavaFX's `KeyCode`.
 * Here's JavaFX's javadocs for KeyCode :
 *
 *     Set of key codes for KeyEvent objects
 *
 * Hmm, that's not very helpful is it!
 *
 * IMHO, `KeyCode` is badly named, and is based on the name of an Int parameter sent from the operating system
 * to represent a physical key.
 *
 * Also, I think JavaFX is trying to be clever (and failing), by abstracting away the concept of a `physical` key.
 * Glok takes the `KISS` principle (Keep it simple stupid).
 *
 * Glok's [Key] names are different names from JavaFX's `KeyCode` names.
 * e.g. JavaFX uses `DIGIT0` instead of `DIGIT_0`, and `NUMPAD0` instead of `NUMPAD_0`.
 * IMHO adding the underscore makes code much more readable.
 *
 * Number pad key names are different. e.g. [NUMPAD_PERIOD], [NUMPAD_MINUS]
 * instead of JavaFX's `DECIMAL` and `SUBTRACT`.
 * (Another JavaFX fail : Using `SUBTRACT` and `MINUS` is just asking for unintended bugs!)
 *
 * JavaFX, has _many_ more `KeyCode`s, e.g. `AMPERSAND`. I have no clue what these extras are for.
 * Are there keyboards with an actual `ampersand` key (not the `7` key)?
 * Maybe they are only relevant for virtual keyboards found on mobile devices. The API docs doesn't explain.
 *
 * If my guess is correct, IMHO, this is just _wrong_.
 * Virtual keyboards should not generate `KEY_PRESSED` nor `KEY_RELEASED` for non-standard keys.
 * They should only generate `KEY_TYPED` events. In which case, there is no [Key] instance involved.
 *
 * To prove this point, should there be a `KeyCode` for every character that could appear on a virtual keyboard,
 * including, all non-ascii letters, all character symbols, all emoji? No!
 * That's what [KeyTypedEvent.char] and [KeyTypedEvent.codePoint] are for!
 *
 * If you want to implement a virtual keyboard which generates `KEY_PRESSED` and `KEY_RELEASED` events too,
 * that's fine, but only when it is mimicking physical keys (i.e. don't introduce new KeyCode instances).
 */
enum class Key(label: String? = null) {

    SPACE("Space"), QUOTE("'"), COMMA(","), MINUS("-"), PERIOD("."), SLASH("/"),
    DIGIT_0("0"), DIGIT_1("1"), DIGIT_2("2"), DIGIT_3("3"), DIGIT_4("4"),
    DIGIT_5("5"), DIGIT_6("6"), DIGIT_7("7"), DIGIT_8("8"), DIGIT_9("9"),
    SEMICOLON(";"), EQUALS("="),

    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

    LEFT_BRACKET("("), BACKSLASH("\\"), RIGHT_BRACKET(")"), BACK_QUOTE("`"),
    WORLD_1, WORLD_2,
    ESCAPE("Esc"), ENTER("Enter"), TAB("Tab"), BACKSPACE("Backspace"),
    INSERT("Insert"), DELETE("Delete"),
    RIGHT("Right"), LEFT("Left"), DOWN("Down"), UP("Up"),
    PAGE_UP("Page Up"), PAGE_DOWN("Page Down"), HOME("Home"), END("End"),
    CAPS_LOCK("Caps Lock"), SCROLL_LOCK("Scroll Lock"), NUM_LOCK("Num Lock"),
    PRINT_SCREEN("PrtSc"), PAUSE("Pause"),

    F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24, F25,

    NUMPAD_0, NUMPAD_1, NUMPAD_2, NUMPAD_3, NUMPAD_4, NUMPAD_5, NUMPAD_6, NUMPAD_7, NUMPAD_8, NUMPAD_9,

    NUMPAD_PERIOD, NUMPAD_DIVIDE, NUMPAD_MULTIPLY, NUMPAD_MINUS, NUMPAD_PLUS, NUMPAD_ENTER, NUMPAD_EQUALS,

    LEFT_SHIFT, LEFT_CONTROL, LEFT_ALT, LEFT_SUPER, RIGHT_SHIFT, RIGHT_CONTROL, RIGHT_ALT, RIGHT_SUPER, MENU,

    UNKNOWN;

    val label: String = label ?: name

    /**
     * A Helper function for creating [KeyCombination] from a [Key].
     */
    fun noMods() = KeyCombination(this)

    /**
     * A Helper function for creating [KeyCombination] from a [Key], with `Shift` held down.
     */
    fun shift() = KeyCombination(this).shift()

    /**
     * A Helper function for creating [KeyCombination] from a [Key], with `Ctrl` held down.
     */
    fun control() = KeyCombination(this).control()

    /**
     * A Helper function for creating [KeyCombination] from a [Key], with `Alt` held down.
     */
    fun alt() = KeyCombination(this).alt()

    /**
     * A Helper function for creating [KeyCombination] from a [Key], with `Super` held down.
     * Note, the method isn't called `super`, as that is a Java keyword.
     */
    fun sup() = KeyCombination(this).sup()

    /**
     * A Helper function for creating [KeyCombination] from a [Key].
     * `Shift` may be optionally held down.
     */
    fun maybeShift() = KeyCombination(this).maybeShift()

    /**
     * A Helper function for creating [KeyCombination] from a [Key].
     * `Ctrl` may be optionally held down.
     */
    fun maybeControl() = KeyCombination(this).maybeControl()

    /**
     * A Helper function for creating [KeyCombination] from a [Key].
     * `Alt` may be optionally held down.
     */
    fun maybeAlt() = KeyCombination(this).maybeAlt()

    /**
     * A Helper function for creating [KeyCombination] from a [Key].
     * `Super` may be optionally held down.
     */
    fun maybeSuper() = KeyCombination(this).maybeSuper()

}

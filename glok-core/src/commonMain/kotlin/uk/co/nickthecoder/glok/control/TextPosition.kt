package uk.co.nickthecoder.glok.control

/**
 * Holds the [row] and [column] data for caret/anchor positions within a [TextArea].
 * Both [row] and [column] are zero based, so the start of a document is (0,0).
 *
 * NOTE, Think of a [TextPosition] representing a vertical line next to a character.
 * Do NOT think of it as an index of a particular character.
 *
 * This is important when considering the range of acceptable values.
 * If a line contains `"Hi"`, then [column] can range from 0 to 2 INCLUSIVE.
 * (the imaginary line can be after the `i`).
 *
 * It's also important to realise that it is meaningless to ask `what is the character at
 * this [TextPosition]` only _after_ or _before_ a [TextPosition].
 */
data class TextPosition(val row: Int, val column: Int) : Comparable<TextPosition> {

    override fun compareTo(other: TextPosition): Int {
        return if (row == other.row) {
            column.compareTo(other.column)
        } else {
            row.compareTo(other.row)
        }
    }

    override fun toString() = "($row,$column)"

    override fun hashCode() = row * 1000 + column

    override fun equals(other: Any?): Boolean {
        if (other !is TextPosition) return false
        return row == other.row && column == other.column
    }

    companion object {
        val START = TextPosition(0, 0)
    }
}

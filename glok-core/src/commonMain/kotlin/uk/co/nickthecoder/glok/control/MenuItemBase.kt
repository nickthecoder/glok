/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyle
import uk.co.nickthecoder.glok.theme.styles.HOVER

/**
 *
 * ## Class Diagram
 * There is a very similar class-hierarchy of buttons. See [ButtonBase].
 *
 *                 ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮     ┌─────────────┐        ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                 ┆/Actionable/     ┆     │MenuItemBase │        ┆/WithContent/┆
 *                 ┆  performAction()┆     │ onAction    │        ┆  content    ┆
 *                 ┆                 ┆     │             │        ┆             ┆
 *                 ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯     └─────────────┘        ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                           △                    △                    △
 *                           └─────────┐      ┌───┴──────────────┐     │
 *                                  ┌──┴──────┴──────┐           │     │
 *                                  │LabelledMenuItem│      ┌────┴─────┴───┐
 *                                  │ text           │      │CustomMenuItem│
 *                                  │ graphic        │      │              │
 *                                  │ ...            │      │              │
 *                                  └────────────────┘      └──────────────┘
 *                                           △
 *           ┌────────────────┬──────────────┴───────┐               ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *     ┏━━━━━┷━━━━━┓     ┏━━━━┷━━━━━━┓       ┌───────┴──────────┐    ┆/Toggle/       ┆      ┏━━━━━━━━━━━━━━━┓
 *     ┃SubMenu    ┃     ┃MenuItem   ┃       │SelectMenuItemBase│    ┆  selected     ┆      ┃ToggleGroup    ┃
 *     ┃ items     ┃     ┃           ┃       │ selected         │    ┆  toggleGroup  ├─────◇┃ selectedToggle┃
 *     ┃           ┃     ┃           ┃       │                  │    ┆  userData     ┆      ┃ userData()    ┃
 *     ┗━━━━━━━━━━━┛     ┗━━━━━━━━━━━┛       └──────────────────┘    ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯      ┗━━━━━━━━━━━━━━━┛
 *                                                   △                       △
 *                ┌───────────────────────────┬──────┴────────────┐   ┌──────┴─────────────┐
 *     ┏━━━━━━━━━━┷━━━━━━━━━━━┓   ┏━━━━━━━━━━━┷━━━━━━━━━┓    ┌────┴───┴─────────┐          ╵
 *     ┃PropertyToggleMenuItem┃   ┃PropertyRadioMenuItem┃    │ToggleMenuItemBase│   ToggleButtonBase
 *     ┃ property             ┃   ┃ property            ┃    │                  │
 *     ┃ value                ┃   ┃ value               ┃    │                  │
 *     ┗━━━━━━━━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━━━━━━━┛    └──────────────────┘
 *                                                                    △
 *                                                          ┌─────────┴──────────┐
 *                                                   ┏━━━━━━┷━━━━━━━┓   ┏━━━━━━━━┷━━━━┓
 *                                                   ┃ToggleMenuItem┃   ┃RadioMenuItem┃
 *                                                   ┃              ┃   ┃             ┃
 *                                                   ┗━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━┛
 *
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart).
 */
sealed class MenuItemBase : Region(), Actionable {

    val onActionProperty by optionalActionEventHandlerProperty(null)
    var onAction by onActionProperty

    override fun performAction() {
        pseudoStyles.remove(HOVER)
        onAction?.tryCatchHandle(ActionEvent())
        scene?.stage?.regularStage?.closePopupMenus()
    }

    init {
        onMouseClicked { performAction() }
        onMouseEntered { mouseEntered() }
        onMouseExited { mouseExited() }
        onKeyPressed { event -> keyPressed(event) }
    }

    protected open fun keyPressed(event: KeyEvent) {
        if (focused && MenuItemActions.ACTIVATE.matches(event)) {
            performAction()
            event.consume()
        }
        if (MenuItemActions.FORWARDS.matches(event)) {
            val popup = PopupMenu.findPopupMenu(this)
            if (popup != null) {
                popup.forwardsBackwards(1)
                event.consume()
            }
        }
        if (MenuItemActions.BACKWARDS.matches(event)) {
            val popup = PopupMenu.findPopupMenu(this)
            if (popup != null) {
                popup.forwardsBackwards(- 1)
                event.consume()
            }
        }
    }

    /**
     * When the mouse enters this menuItem, we tell the [PopupMenu], which adds to ":hover" [pseudoStyles],
     * and removes ":hover" from the previously hovered over node (if there was one).
     * If the previously hovered over node was a [SubMenu], then the [PopupMenu] is closed (if it was open).
     */
    protected open fun mouseEntered() {
        select()
    }

    /**
     * Removes ":hover" from [pseudoStyles].
     * NOTE. [SubMenu] overrides this (and does NOT remove ":hover"), because the exit may be caused by the
     * mouse entering the [PopupMenu] (and in that has we still want the [SubMenu] to have the ":hover" pseudo-style.
     */
    protected open fun mouseExited() {
        deselect()
    }

    internal open fun select() {
        // Ensure all siblings are not selected. This will also cause PopupMenus to close,
        // if a sibling is a SubMenu, which is open.
        PopupMenu.findPopupMenu(this)?.deselectAll()
        // Now that we've deselected, we can now select ourselves.
        requestFocus()
        pseudoStyle(HOVER)
    }

    internal open fun deselect() {
        pseudoStyles.remove(HOVER)
    }

    /**
     * Usage :
     *
     *     myMenuItem.onAction { ... }
     *
     * Equivalent to the overly verbose JavaFX style :
     *
     *     myMenuItem.onAction = EventHandler(EventType.ActionEvent) { ... }
     *
     */
    fun onAction(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onActionProperty, handlerCombination, block)
    }

}

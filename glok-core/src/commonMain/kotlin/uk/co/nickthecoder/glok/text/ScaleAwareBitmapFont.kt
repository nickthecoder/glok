/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.util.Matrix
import uk.co.nickthecoder.glok.util.log

/**
 * A [Font], which uses [BitmapFont]s, but is aware of [GlokSettings.globalScaleProperty],
 * and uses a scaled-up bitmap, which is then scaled back down to [LogicalPixels],
 * so that it appears the correct size, and hi-res on high DPI screens
 * (i.e. when [GlokSettings.globalScale] != 1.
 *
 * When [GlokSettings.globalScaleProperty] changes, the underlying [ScaledBitmapFont] changes,
 * so this font still matches. Note, this is an expensive operation, because the bitmap
 * is generated from scratch. However, this is rare.
 *
 * When text is drawn, which includes character whose glyph isn't available, then the font is rebuilt,
 * and the drawing operation is performed again.
 */
internal class ScaleAwareBitmapFont(scaledFont: ScaledBitmapFont) : Font {

    override val identifier = scaledFont.identifier

    private var cachedFont: ScaledBitmapFont? = scaledFont


    override val fixedWidth: Float

    init {
        scaledFont.unscaled.throwGlyphMissing = true
        val w = widthOfOrZero("W", 0)
        fixedWidth = if (w == widthOfOrZero(".", 0)) w else 0f
    }

    private val scaledFont: ScaledBitmapFont
        get() {
            val cached = cachedFont
            return if (cached == null) {
                val globalScale = GlokSettings.globalScale

                val requiredSize = identifier.size * globalScale
                val unscaledIdentifier = FontIdentifier(identifier.family, requiredSize, identifier.style)
                val unscaled = createBitmapFont(unscaledIdentifier)
                unscaled.throwGlyphMissing = true

                val scaled = ScaledBitmapFont(identifier, unscaled, globalScale)
                cachedFont = scaled
                scaled
            } else {
                cached
            }
        }


    /**
     * When globalScale changes, throw away [cachedFont].
     */
    private val globalScaleListener = GlokSettings.globalScaleProperty.addWeakListener {
        cachedFont = null
    }


    override val height = scaledFont.height
    override val ascent = scaledFont.ascent
    override val descent = scaledFont.descent
    override val top = scaledFont.top
    override val bottom = scaledFont.bottom
    override val italicInverseSlope = scaledFont.italicInverseSlope

    override val maxWidthOfDigit: Float by lazy {
        listOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-", ".").maxOf { widthOfOrZero(it, 0) }
    }

    override fun image() = scaledFont.image()

    override fun widthOf(text: CharSequence, indentationColumns: Int, startColumn: Int): Float {
        return try {
            scaledFont.widthOfOrZero(text, indentationColumns, startColumn)
        } catch (e: GlyphMissingException) {
            rebuild(text)
            scaledFont.widthOfOrZero(text, indentationColumns, startColumn)
        }
    }


    override fun hasGlyph(c: Char) = scaledFont.hasGlyph(c)

    override fun caretPosition(text: CharSequence, x: Float, indentationColumns: Int) =
        scaledFont.caretPosition(text, x, indentationColumns)

    private fun rebuild(text: CharSequence) {
        for (c in text) {
            if (!scaledFont.hasGlyph(c)) {
                BitmapFont.requiredGlyphs.add(c)
            }
        }
        cachedFont = null
    }

    override fun drawTopLeft(
        text: CharSequence,
        color: Color,
        x: Float,
        y: Float,
        indentationColumns: Int,
        startColumn: Int,
        modelMatrix: Matrix?
    ) {
        try {
            scaledFont.drawTopLeft(text, color, x, y, indentationColumns, startColumn, modelMatrix)
        } catch (e: GlyphMissingException) {
            rebuild(text)
            try {
                scaledFont.drawTopLeft(text, color, x, y, indentationColumns, startColumn, modelMatrix)
            } catch (e2: GlyphMissingException) {
                log.error("ScaleAwareBitmapFont unable to render missing glyph : ${e2.c}")
            }
        }
    }

    override fun toString() = "ScaleAwareBitmapFont $identifier"
}

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.control.BorderPane
import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.control.VBox
import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.event.KeyTypedEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.theme.Styled
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.util.max

/**
 * There is a 1:1 relationship between a `window` and a [Stage], and at any moment in time
 * there is also a 1:1 relationship between [Stage] and [Scene].
 * However, it is possible to replace a [Stage]'s scene. In most cases though,
 * a stage keeps the same scene.
 *
 * A Scene has a [root] node, which is the top [Node] of the `scene graph`.
 * For a typical application, this is a [BorderPane], with a `MenuBar` and `ToolBar`s at the `top`,
 * and the application's main content in the `center`.
 *
 * If the width or height of a new scene is <= 0, then when the scene is shown,
 * the scene's `prefWidth` \ `prefHeight` will be used.
 * e.g. If you want a scene 100 [LogicalPixels] high, but want the use its pref width, then
 *
 *     myScene = Scene( myRootNode, 0f, 100f )
 *
 * ## Class Diagram
 *
 *                   ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                   ┆       Stage        ┆
 *                   ┆ scene              ├────┐  ┏━━━━━━━━━━━━━━━━━━┓
 *                   ┆ resizable          ┆    │  ┃      Scene       ┃             ┌───────────────────────┐
 *                   ┆ maximized          ┆    └──┨ stage            ┃             │         Node          │
 *                   ┆ minimized          ┆       ┃ root             ┠─────────────┤ scene                 │
 *                   ┆ focused            ┆       ┃ width            ┃             │ parent : Node?        │◆─┐
 *                   ┆ title              ┆       ┃ height           ┃             │-children : List<Node> ├──┘
 *                   ┆ closing            ┆       ┃ focusOwner       ┠────────────▶│                       │
 *                   ┆ mousePointer       ┆       ┃ theme            ┠──────┐      │ localX,Y              │
 *                   ┆                    ┆       ┃                  ┃      │      │ sceneX,Y              │
 *                   ┆ show()             ┆       ┃                  ┃      │      │                       │
 *                   ┆ hide()             ┆       ┃ requestRedraw()  ┃      │      │ requestRedraw()       │
 *                   ┆ onCloseRequested   ┆       ┃ requestLayout()  ┃      │      │ requestLayout()       │
 *                   ┆ onClosed           ┆       ┃ ...              ┃      │      │ requestFocus()        │
 *                   ┆ ...                ┆       ┗━━━━━━━━━━━━━━━━━━┛      │      │                       │
 *                   ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯                                 │      │-draw()                │
 *                              △                                           │      │-drawChildren()        │
 *                              │                                           │      │-layoutChildren()      │
 *                      ┌───────┴───────┐                                   │      │                       │
 *                      │  StageBase    │                                   │      │ ...                   │
 *                      │               │                                   │      └───────────────────────┘
 *                      └───────────────┘                                   │
 *                             △                                            │
 *                  ┌──────────┴───────────┐                                │           ┌───────────────┐
 *       ┏━━━━━━━━━━┷━━━━━━━┓    ┏━━━━━━━━━┷━━━━━━━━━┓                      └──────────▶│    Theme      │
 *       ┃    OverlayStage  ┃    ┃    RegularStage   ┃                                  │               │
 *       ┃ parent : Stage   ┃    ┃ scale : Float     ┃                                  │               │
 *       ┃ stageType        ┃  ┌─┨ overlayStages     ┃                                  └───────────────┘
 *       ┃ regularStage     ┃◆─┘ ┃ window            ┃                                          △
 *       ┃                  ┃    ┃                   ┃                                          │
 *       ┗━━━━━━━━━━━━━━━━━━┛    ┗━━━━━━━━━━━━━━━━━━━┛                                  ┏━━━━━━━┷━━━━━━━┓
 *                                                                                      ┃   Tantalum    ┃
 *                                                                                      ┃               ┃
 *                                                                                      ┃               ┃
 *                                                                                      ┗━━━━━━━━━━━━━━━┛
 *
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
 */
class Scene(root: Node, width: Float = 0f, height: Float = 0f) : Styled {

    // region ==== Properties ====

    val rootProperty by nodeProperty(root)
    var root by rootProperty

    /**
     * Stage will set this when its [Stage.sceneProperty] is set.
     */
    private val mutableStageProperty by optionalStageProperty(null)
    val stageProperty = mutableStageProperty.asReadOnly()
    var stage by mutableStageProperty
        internal set

    /**
     * Initially bound to [GlokSettings.defaultThemeProperty], whose default value is [Tantalum.theme].
     *
     * If you wish to change the theme to something else for all scenes, then change [GlokSettings.defaultTheme].
     *
     * If you wish to change the theme for one scene only, then :
     *
     *     myScene.themeProperty.unbind()
     *     myScene.theme = mySpecialTheme
     */
    val themeProperty by themeProperty(Theme())
    var theme by themeProperty

    private val mutableWidthProperty by floatProperty(width)
    val widthProperty = mutableWidthProperty.asReadOnly()

    /**
     * The width of the scene in [LogicalPixels].
     * In most cases, [root]'s width == [Scene.width].
     * However, if the user shrinks the window smaller than the [root]'s minWidth, then [root] is
     * given it's minWidth, and the right hand side will not be visible.
     */
    var width by mutableWidthProperty
        internal set

    private val mutableHeightProperty by floatProperty(height)
    val heightProperty = mutableHeightProperty.asReadOnly()

    /**
     * The height of the scene in [LogicalPixels].
     * In most cases, [root]'s height == [Scene.height].
     * However, if the user shrinks the window smaller than the [root]'s minHeight, then [root] is
     * given it's minHeight, and the bottom will not be visible.
     */
    var height by mutableHeightProperty
        internal set

    val backgroundColorProperty by stylableColorProperty(Color.LIGHT_GRAY)
    var backgroundColor by backgroundColorProperty

    /**
     * Setting this to `true` will cause [layout] to be called.
     */
    val requestLayoutProperty by booleanProperty(true)
    var requestLayout by requestLayoutProperty

    /**
     * Setting this to `true` will cause the entire scene to be re-styled
     */
    val requestRestylingProperty by booleanProperty(true)
    var requestRestyling by requestRestylingProperty

    /**
     * Setting this to `true` will cause the scene to be redrawn.
     */
    val requestRedrawProperty by booleanProperty(true)
    var requestRedraw by requestRedrawProperty

    /**
     * Which node within this scene has input focus.
     *
     * Note. this value is unchanged when the stage loses focus.
     * When a stage loses focus, the [focusOwner]'s node loses focus (i.e. [Node.focused] = false).
     * When the stage gains focus again, the same node gains focus again.
     */
    private val mutableFocusOwnerProperty by optionalNodeProperty(null)
    val focusOwnerProperty = mutableFocusOwnerProperty.asReadOnly()
    var focusOwner: Node? by mutableFocusOwnerProperty
        private set

    // endregion properties

    // region ==== Listeners ====

    /**
     * This invalidation listener is attached to the focused node's sceneProperty.
     * If the value changes to null, then it means the node has been removed from the scene.
     * We don't want [focusOwner] to be this node anymore (because it isn't part of the scene),
     * so we give focus to the first focusable node in the scene
     * (or the root node, if there aren't any focusable nodes).
     */
    private val focusOwnersSceneListener = invalidationListener {
        val focusedNode = focusOwner
        if (focusedNode != null) {
            if (focusedNode.scene == null) {
                // The node with focus has been removed from the scene.
                // Let's give focus to another node.
                root.requestFocus(true)
            }
        }
    }

    private val rootListener = rootProperty.addChangeListener { _, old, new ->
        old.scene = null
        new.scene = this
    }

    private val themeListener = themeProperty.addListener { requestRestyling = true }

    /**
     * Whenever [focusOwner] changes, update [Node.focused] for the node which lost focus,
     * and the node that gained focus.
     */
    private val focusOwnerListener = mutableFocusOwnerProperty.addChangeListener { _, oldNode, newNode ->
        val stage = stage
        if (stage != null) {
            if (stage.focused) {
                if (oldNode != null) {
                    oldNode.mutableFocusedProperty.value = false
                    oldNode.sceneProperty.removeListener(focusOwnersSceneListener)
                }
                if (newNode != null) {
                    newNode.mutableFocusedProperty.value = true
                    newNode.sceneProperty.addListener(focusOwnersSceneListener)
                    newNode.scrollTo()
                }
            }
        }
    }

    /**
     * Whenever the stage is set, we need to add this listener to [Stage.focusedProperty].
     * (and stop listening when the stage is null).
     * This listener sets/reset the [focusOwner]'s [Node.focusedProperty] whenever the stage gains/loses focus.
     */
    private val stageFocusedListener = invalidationListener {
        stageFocusChanged()
    }

    // endregion

    // region ==== Fields ====

    /**
     * See [requestFocus].
     */
    private var inRequestFocus = false

    // endregion fields

    // region ==== init ====
    init {
        root.scene = this
        themeProperty.bindTo(GlokSettings.defaultThemeProperty)

        with(root) {
            localX = 0f
            localY = 0f
            sceneX = 0f
            sceneY = 0f
        }

        stageProperty.addChangeListener { _, old, new ->
            old?.focusedProperty?.removeListener(stageFocusedListener)
            new?.focusedProperty?.addListener(stageFocusedListener)
        }
    }
    // endregion init

    // region ==== Public Methods ====

    /**
     * Give the `input focus` to [node]. Key events [KeyEvent] and [KeyTypedEvent] will be directed
     * at [node]. See [Node.onKeyPressed] [Node.onKeyReleased] and [Node.onKeyTyped].
     *
     * [node]'s [Node.focusedProperty] is set to true, and scene's [focusOwner] is set to [node].
     *
     * If [useFocusTraversable] == true (which is NOT the default),
     * and [node] is not [Node.focusTraversable], then the focus moves to the next focusTraversable Node.
     * i.e. it is the same as a standard [requestFocus] followed by hitting `Tab`
     * (or calling [focusNext]).
     *
     * This is very handy if you have a container, such as [VBox], and want the focus to go to
     * the first descendant which is focusTraversable.
     *
     * @param highlight Should we set the focused [Node.focusedByKeyboard], which causes some nodes
     * to have an additional border. See [Region.focusBorder].
     */
    internal fun requestFocus(node: Node, useFocusTraversable: Boolean = false, highlight: Boolean) {
        // Don't allow a Node to be given focus if isn't part of this scene.
        if (node.scene !== this) {
            return
        }

        // If requestFocus is called again, while the focus is still changing, then reject that request.
        // This could happen if a listener of a Node's focusProperty calls requestFocus (either on a Node,
        // or here on Scene).
        if (inRequestFocus) {
            return
        }
        if (! node.mutableFocusedProperty.isBound()) {
            inRequestFocus = true
            try {
                // This may LOOK like a simple assignment, but it's backed by a Property,
                // so a heck of a lot can happen!
                focusOwner = node
            } finally {
                inRequestFocus = false
            }
            if (useFocusTraversable && ! node.focusTraversable) {
                focusNext()
            }
        }
        if (highlight) {
            focusOwner?.focusedByKeyboard = true
        }
    }

    /**
     * Equivalent to pressing the TAB key, to advance the focus to the next node.
     */
    fun focusNext() {
        val nextFocusable = focusOwner?.nextFocusableNode() ?: root.nextFocusableNode()
        nextFocusable?.requestFocus()
    }

    private fun lastVisibleNode(): Node {
        var last: Node = root
        while (last.children.isNotEmpty()) {
            val lastVisible = last.children.lastOrNull { it.visible }
            if (lastVisible == null) {
                return last
            } else {
                last = lastVisible
            }
        }
        return last
    }

    /**
     * Equivalent to pressing Shift-TAB, to move the focus to the previous node.
     */
    fun focusPrevious() {
        val prevFocusable = focusOwner?.previousFocusableNode()
        if (prevFocusable == null) {
            val lastNode = lastVisibleNode()
            if (lastNode.focusTraversable) {
                lastNode.requestFocus()
            } else {
                lastNode.previousFocusableNode()?.requestFocus()
            }
        } else {
            prevFocusable.requestFocus()
        }
    }

    /**
     * Moves the focus to the next section of the scene. A section is any Node, where [Node.section] == `true`.
     *
     * The keyboard shortcut `F6` calls this. See [StageBase.StageActions.NEXT_SECTION].
     */
    fun sectionNext() {
        val nextSection = focusOwner?.nextSection() ?: root.nextSection()
        nextSection?.let { focusOwner = it }
    }

    /**
     * The opposite of [sectionNext]
     *
     * The keyboard shortcut `Shift+F6` calls this. See [StageBase.StageActions.PREVIOUS_SECTION].
     */
    fun sectionPrevious() {
        // Hmm, this is more complex than sectionNext(), because of an asymmetry of FocusTraversableWalker.
        // Maybe this asymmetry should be removed? If we start in a deeply nested node, FocusTraversableWalker
        // does NOT climb up the ancestors first, whereas in the opposite direction, it DOES traverse
        // down through the descendants first.

        val prevSection = focusOwner?.previousSection()
        if (prevSection == null) {
            // If there is no previous section, we should start from the last node
            // i.e. loop back around from the start to the end.
            val lastNode = lastVisibleNode()
            // Is the last node, or one of its ancestors a "section"?
            val lastSection = lastNode.firstToRoot { it.section && it.visible } ?: lastNode.previousSection()
            if (lastSection != null) {
                focusOwner = lastSection
            } else {
                // The last node in the scene tree isn't part of a visible section, so
                // we can just find the previous section from the last node.
                val prevFromLast = lastNode.previousSection()
                if (prevFromLast != null) {
                    focusOwner = prevFromLast
                }
            }
        } else {
            // Simplest code path, we found a previous section from the old focused node.
            focusOwner = prevSection
        }
    }

    fun findNodeById(id: String): Node? = root.findNodeById(id)

    fun findDeepestNodeAt(sceneX: Float, sceneY: Float): Node? {
        return if (root.containsScenePoint(sceneX, sceneY)) {
            root.findDeepestNodeAt(sceneX, sceneY)
        } else {
            null
        }
    }

    fun dump(borders: Boolean = false, size: Boolean = false, filter: (Node) -> Boolean = { it.visible }) {
        println(this)
        root.dump("", borders, size, filter)
    }

    // endregion public methods

    // region ==== non-public methods ====

    /**
     * When the stage gains/loses focus, so should the state of [Node.focused] for node [focusOwner].
     */
    private fun stageFocusChanged() {
        focusOwner?.focused = stage?.focused == true
    }

    internal fun layout() {
        //log.info("Laying out scene")
        root.width = max(width, root.evalMinWidth())
        root.height = max(height, root.evalMinHeight())
        root.layout()
        requestLayout = false
        requestRedraw = true
    }

    internal fun stageResized(width: Float, height: Float) {
        this.width = width
        this.height = height
        requestLayout = true
    }

    internal fun restyle() {
        val theme = theme
        //log.info("Restyling scene $this")
        theme.applyToScene(this)
        requestRestyling = false
        requestLayout = true
    }

    internal fun stageFocused(isFocused: Boolean) {
        focusOwner?.mutableFocusedProperty?.value = isFocused
    }

    // endregion non-public methods

    // region ==== Object methods ====
    override fun toString() = "Scene size( $width x $height ) prefSize( ${root.evalPrefWidth()} x ${root.evalPrefHeight()} )"
    // endregion object methods

}

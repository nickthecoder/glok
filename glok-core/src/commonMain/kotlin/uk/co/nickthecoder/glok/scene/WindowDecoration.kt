/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.Button
import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.control.ThreeRow
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.changeListener
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.STAGE_FOCUSED
import uk.co.nickthecoder.glok.theme.styles.STAGE_RESIZABLE
import uk.co.nickthecoder.glok.theme.styles.TITLE_BAR
import uk.co.nickthecoder.glok.theme.styles.WINDOW_DECORATION
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.maxPrefWidth
import uk.co.nickthecoder.glok.util.totalMinHeight
import uk.co.nickthecoder.glok.util.totalPrefHeight

/**
 * The root node of scenes using an [OverlayStage] which draws the fake-window border, title and close button.
 * We listen to the scene's rootProperty, so that if the root node is changed, we reinsert this into the scene
 * graph, by setting [Scene.root] to `this`, and setting [clientArea] to what was briefly the scene's root.
 */
internal class WindowDecoration(

    private val overlayStage: OverlayStage

) : Region() {

    private val titleLabel = label("") {
        textProperty.bindTo(overlayStage.titleProperty)
    }

    private val closeButton = Button("X").apply {
        onAction { overlayStage.close() }
        onMouseMoved { event ->
            // As the close button is very close to the corner, we don't want the NE_SW cursor to be shown
            // which would indicate the user can resize the stage from here.
            // This is defensive programming, because the size of the hit-boxes for this button,
            // and the window border edges do not overlap (but in theory they could).
            scene?.stage?.mousePointer = MousePointer.ARROW
            event.consume()
        }
    }

    private var dragX = 0f
    private var dragY = 0f

    private val titleBar = ThreeRow().apply {

        style(TITLE_BAR)

        center = titleLabel
        right = closeButton
    }

    override val children = mutableListOf<Node>().asMutableObservableList()

    private var clientArea: Node? = null

    private val rootListener = changeListener<Node, ObservableValue<Node>> { _, _, newRoot ->
        rootChanged(newRoot)
    }

    private var draggingEdge = false
    private var nearTopEdge: Boolean = false
    private var nearBottomEdge: Boolean = false
    private var nearLeftEdge: Boolean = false
    private var nearRightEdge: Boolean = false

    init {
        pseudoStyleIf(overlayStage.resizable, STAGE_RESIZABLE)
        pseudoStyleIf(overlayStage.focused, STAGE_FOCUSED)

        overlayStage.sceneProperty.addChangeListener { _, oldScene, newScene ->
            sceneChanged(oldScene, newScene)
        }

        overlayStage.focusedProperty.addChangeListener { _, _, focused ->
            pseudoStyleIf(focused, STAGE_FOCUSED)
            requestLayout()
        }
        overlayStage.resizableProperty.addChangeListener { _, _, resizable ->
            pseudoStyleIf(resizable, STAGE_RESIZABLE)
            requestLayout()
        }

        children.addChangeListener(childrenListener)
        children.addAll(titleBar)

        sceneChanged(null, scene)

        onMousePressed { mousePressed(it) }
        onMouseReleased { mouseReleased() }
        onMouseMoved { mouseMoved(it) }
        onMouseDragged { dragBorder(it) }

        titleBar.onMousePressed { titleBarPressed(it) }
        titleBar.onMouseDragged { dragTitleBar(it) }
        titleBar.onMouseReleased { titleBarReleased() }

    }

    private fun titleBarPressed(event: MouseEvent) {
        val stage = scene?.stage ?: return
        stage.mousePointer = MousePointer.RESIZE_ALL

        dragX = event.sceneX - sceneX
        dragY = event.sceneY - sceneY
        event.capture()
    }

    private fun titleBarReleased() {
        val stage = scene?.stage ?: return
        stage.mousePointer = MousePointer.ARROW
    }

    private fun dragTitleBar(event: MouseEvent) {

        overlayStage.moveTo(
            event.sceneX - dragX,
            event.sceneY - dragY
        )
        event.consume()
    }

    private fun mousePressed(event: MouseEvent) {
        checkNearEdges(event)

        if (nearTopEdge || nearBottomEdge || nearLeftEdge || nearRightEdge) {
            draggingEdge = true
            event.capture()
        }

    }

    private fun mouseReleased() {
        draggingEdge = false
    }

    /**
     * When the stage is resizable, check if the mouse is near an edge,
     * and if so, change the mouse to indicate the direction that resizing can be applied.
     */
    private fun mouseMoved(event: MouseEvent) {
        checkNearEdges(event)
    }

    /**
     * Sets zero, one or two of [nearLeftEdge], [nearTopEdge], [nearRightEdge], [nearBottomEdge].
     *
     * If the stage is not resizable, then all values are reset.
     *
     * NOTE. It is important that we never use `this.width` or `this.height`, and instead use
     * `scene.width` and `scene.height`. See [dragBorder] for details.
     */
    private fun checkNearEdges(event: MouseEvent) {
        val scene = scene ?: return
        val stage = scene.stage ?: return

        if (! stage.resizable) {
            nearLeftEdge = false
            nearTopEdge = false
            nearRightEdge = false
            nearBottomEdge = false
            return
        }

        // The threshold for the distance the mouse is from the edge of this WindowDecoration
        // to allow window resizing.
        val borderSize = max(max(surroundTop(), surroundBottom()), max(surroundLeft(), surroundRight()))
        // When the mouse is near a corner, the threshold is larger. This is in part to make up for the rounded
        // corners, and partly  just to make the hit-box larger.
        val cornerSize = max(borderSize * 2, 12f)

        val fromTop = event.sceneY - sceneY
        val fromBottom = sceneY + scene.height - event.sceneY
        val fromLeft = event.sceneX - sceneX
        val fromRight = sceneX + scene.width - event.sceneX

        nearTopEdge =
            if (fromTop < if (fromLeft < cornerSize || fromRight < cornerSize) cornerSize else borderSize) {
                dragY = event.sceneY - sceneY
                true
            } else {
                false
            }

        nearBottomEdge =
            if (fromBottom < if (fromLeft < cornerSize || fromRight < cornerSize) cornerSize else borderSize) {
                dragY = sceneY + scene.height - event.sceneY
                true
            } else {
                false
            }

        nearLeftEdge =
            if (fromLeft < if (fromTop < cornerSize || fromBottom < cornerSize) cornerSize else borderSize) {
                dragX = event.sceneX - sceneX
                true
            } else {
                false
            }

        nearRightEdge =
            if (fromRight < if (fromTop < cornerSize || fromBottom < cornerSize) cornerSize else borderSize) {
                dragX = sceneX + scene.width - event.sceneX
                true
            } else {
                false
            }

        if (nearTopEdge) {
            if (nearLeftEdge) {
                stage.mousePointer = MousePointer.RESIZE_NWSE
            } else if (nearRightEdge) {
                stage.mousePointer = MousePointer.RESIZE_NESW
            } else {
                stage.mousePointer = MousePointer.RESIZE_NS
            }
        } else if (nearBottomEdge) {
            if (nearLeftEdge) {
                stage.mousePointer = MousePointer.RESIZE_NESW
            } else if (nearRightEdge) {
                stage.mousePointer = MousePointer.RESIZE_NWSE
            } else {
                stage.mousePointer = MousePointer.RESIZE_NS
            }
        } else if (nearLeftEdge || nearRightEdge) {
            stage.mousePointer = MousePointer.RESIZE_EW
        } else {
            stage.mousePointer = MousePointer.ARROW
        }
    }

    /**
     * NOTE. It is important that we never use `this.width` or `this.height`, and instead use
     * `scene.width` and `scene.height`, because the former values can be stale.
     *
     * `this.width` and `this.height` are only updated during a layout of a node.
     * The layout occurs at most once per frame, but we often receive multiple mouse movement events per frame.
     *
     * Failure to do so can result in the bottom right corner of the stage to move when dragging the top left corner.
     */
    private fun dragBorder(event: MouseEvent) {
        val scene = scene ?: return
        val stage = scene.stage ?: return
        val parentScene = stage.regularStage.scene ?: return

        var deltaX = 0f
        var deltaY = 0f
        var moveX = false
        var moveY = false

        if (nearTopEdge) {
            deltaY = event.sceneY - sceneY - dragY
            moveY = true
        } else if (nearBottomEdge) {
            deltaY = ((sceneY + scene.height) - event.sceneY - dragY)
        }

        if (nearLeftEdge) {
            deltaX = event.sceneX - sceneX - dragX
            moveX = true
        } else if (nearRightEdge) {
            deltaX = (sceneX + scene.width) - event.sceneX - dragX
        }

        // The provisional position of the top left of the stage relative to the parent stage.
        var newX = if (moveX) sceneX + deltaX else sceneX
        var newY = if (moveY) sceneY + deltaY else sceneY

        // Provisional new size of the stage.
        var newHeight = scene.height - deltaY
        var newWidth = scene.width - deltaX

        // Prevent edges extending beyond the parent stage's bounds
        // NOTE. Dragging the title bar doesn't have this constraint, so the window CAN be moved off the edge.
        if (newX < 0) {
            newWidth += newX
            newX = 0f
        }
        if (newY < 0) {
            newHeight += newY
            newY = 0f
        }
        val maxRight = parentScene.width
        val maxBottom = parentScene.height
        if (newX + newWidth > maxRight) {
            newWidth = maxRight - newX
        }
        if (newY + newHeight > maxBottom) {
            newHeight = maxBottom - newY
        }


        // Don't allow the scene to be shrunk smaller than the minimum width/height
        val minWidth = evalMinWidth()
        if (newWidth < minWidth) {
            if (moveX) {
                newX += newWidth - minWidth
            }
            newWidth = minWidth
        }
        val minHeight = evalMinHeight()
        if (newHeight < minHeight) {
            if (moveY) {
                newY += newHeight - minHeight
            }
            newHeight = minHeight
        }
        if (newHeight < evalMinHeight()) {
            newHeight += evalMinHeight() - newHeight
            deltaY += evalMinHeight() - newHeight
        }

        stage.resize(newWidth, newHeight)
        if (moveY || moveX) {
            overlayStage.moveTo(newX, newY)
        }

        event.consume()
    }

    /**
     * The stage's scene has been replaced with a new scene.
     */
    private fun sceneChanged(oldScene: Scene?, newScene: Scene?) {

        children.remove(clientArea)
        if (oldScene != null) {
            oldScene.rootProperty.removeChangeListener(rootListener)
            if (oldScene.root === this) {
                if (clientArea != null) {
                    oldScene.root = clientArea !!
                }
            }
        }

        if (newScene != null) {
            val clientNode = newScene.root
            clientArea = clientNode
            children.add(clientNode)
            newScene.root = this

            newScene.rootProperty.addChangeListener(rootListener)
            styles.add(WINDOW_DECORATION)

        }
    }

    /**
     * The scene's root node has been changed.
     */
    private fun rootChanged(newRoot: Node) {
        // Changed to this, so we don't need to do anything.
        if (newRoot === this) return

        val scene = newRoot.scene ?: return
        children.remove(clientArea)
        scene.root = this
        clientArea = newRoot
        children.add(newRoot)
    }

    override fun nodePrefWidth() = surroundX() + children.maxPrefWidth()
    override fun nodePrefHeight() = surroundY() + children.totalPrefHeight()
    override fun nodeMinWidth() = surroundX() + (clientArea?.evalMinWidth() ?: 0f)
    override fun nodeMinHeight() = surroundY() + children.totalMinHeight()

    override fun layoutChildren() {
        val x = surroundLeft()
        var y = surroundTop()
        val w = width - surroundX()
        val h = height - surroundY()
        val titleBarHeight = if (titleBar.visible) titleBar.evalPrefHeight() else 0f

        if (titleBar.visible) {
            setChildBounds(titleBar, x, y, w, titleBarHeight)
            y += titleBarHeight
        }

        clientArea?.let { clientArea ->
            setChildBounds(clientArea, x, y, w, h - titleBarHeight)
        }
    }

}

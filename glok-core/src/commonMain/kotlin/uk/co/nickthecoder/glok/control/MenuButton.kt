/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.combineActionEventHandlers
import uk.co.nickthecoder.glok.event.tryCatchHandle
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.WithItems
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.MENU_BUTTON
import uk.co.nickthecoder.glok.util.max

/**
 * A button, which displays [items] in a [PopupMenu] when clicked.
 * On the right hand side there is an arrow (though it could be styled differently),
 * to differentiate it from a regular [Button].
 *
 * NOTE, this is very similar to [Menu], but [Menu]s only live within [MenuBar]s,
 * and do not look like a [Button], and have no arrow.
 *
 * ### Theme DSL
 *
 *     "menu_button" {
 *         child( ".arrow" ) { // An ImageView
 *
 *             image( image : Image )
 *             image( images : NamedImages, imageName : String )
 *
 *             tint( tint : Color )
 *         }
 *     }
 *
 * MenuButton inherits all the features of [ButtonBase].
 */
class MenuButton(

    text: String,
    graphic: Node? = null

) : ButtonBase(text, graphic), WithItems {

    // region ==== Properties ====

    /**
     * An event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    val onShowingProperty by optionalActionEventHandlerProperty(null)
    var onShowing by onShowingProperty

    private val mutableShowingProperty by booleanProperty(false)

    /**
     * Set to true, after the button is clicked, and before the [PopupMenu] is displayed.
     * It is reset to false when the [PopupMenu] is closed.
     *
     * Note, it is often easier to use [onShowing] rather than listening to this property.
     *
     * Applications may listen to this, and adjust [items] as required.
     *
     * History. [onShowing] was added later than [showingProperty], but we keep this for backwards compatibility.
     * Also, [showingProperty] could be used to listen for when the menu is closed.
     */
    val showingProperty = mutableShowingProperty.asReadOnly()
    var showing by mutableShowingProperty
        private set

    // endregion properties

    // region ==== Fields ====

    override val items = mutableListOf<Node>().asMutableObservableList()

    private val arrowImageView = ImageView(null)

    // endregion

    init {
        style(MENU_BUTTON)
        arrowImageView.style(".arrow")

        mutableChildren.add(arrowImageView)

        onAction { showMenu() }
        onMousePressed(HandlerCombination.BEFORE) { event ->
            // If the user pressed this button to dismiss the PopupMenu, do NOT act as if this button were pressed.
            // The PopupMenu will be closed by ANY mouse press, so without this, the PopupMenu would show AGAIN.
            if (showing) event.consume()
        }
    }

    /**
     * Add an event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    fun onShowing(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onShowingProperty, handlerCombination, block)
    }

    private fun showMenu() {
        // If we click this MenuButton to dismiss the PopupMenu, the mouse press will cause the PopupMenu to close
        // (you could have clicked ANYWHERE to close it),
        // and when you release the mouse, this method would OPEN the menu AGAIN without this time check.
        //if (showing) return //|| closeTime + 500L > System.currentTimeMillis()) return
        onShowing?.tryCatchHandle(ActionEvent())
        showing = true
        val menu = PopupMenu()
        menu.items.addAll(items)
        menu.show(this).apply {
            onClosed {
                menu.items.clear()
                showing = false
            }
        }
    }

    // region ==== Layout ====

    override fun nodePrefWidth() = super.nodePrefWidth() + if (arrowImageView.visible) arrowImageView.evalPrefWidth() else 0f
    override fun nodePrefHeight() = max(super.nodePrefHeight(), surroundY() + if (arrowImageView.visible) arrowImageView.evalPrefHeight() else 0f)
    override fun layoutChildren() {
        val arrowWidth = if (arrowImageView.visible) arrowImageView.evalPrefWidth() else 0f
        val arrowHeight = if (arrowImageView.visible) arrowImageView.evalPrefHeight() else 0f
        layoutLabelled(width - arrowWidth, height)

        setChildBounds(
            arrowImageView,
            width - surroundRight() - arrowWidth,
            surroundTop() + (height - surroundY() - arrowHeight) / 2,
            arrowWidth,
            arrowHeight
        )
    }

    override fun drawChildren() {
        super.drawChildren()
        arrowImageView.drawAll()
    }

    // endregion layout

}

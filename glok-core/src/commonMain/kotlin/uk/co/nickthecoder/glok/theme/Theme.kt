package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.property.StylableProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Scene
import uk.co.nickthecoder.glok.theme.Theme.Rule

/**
 * A theme defines all the visual aspects of glok controls, as well as some non-visual aspects
 * (e.g. which controls accept input focus).
 *
 * It does this by changing values of [StylableProperties](StylableProperty) based on rules, similar to CSS.
 * Note, properties which are not [StylableProperty] will be ignored.
 *
 * The syntax is very different from CSS, but the premise is the same, there are [Selector]s,
 * which govern which [Node]s will be affected, and a set of property values.
 * Together these form a [Rule].
 *
 * [Theme]s are created using a DLS (domain specific language), which is Kotlin code,
 * which read more like rules, rather than executable statements.
 * The DSL is usually part of a [ThemeBuilder] object, which add support for automatically rebuilding
 * the [Theme] whenever certain properties change.
 * The [Tantalum] (which builds the default theme) can be viewed on-line here :
 * [Default Theme.kt](https://gitlab.com/nickthecoder/glok/-/blob/main/src/main/kotlin/uk/co/nickthecoder/glok/theme/DefaultTheme.kt)
 *
 * There are two basic [Selector]s : [StyleSelector] and [PseudoStyleSelector].
 * These match against Nodes using [Node.styles] and [Node.pseudoStyles].
 * (which are sets of `String`s).
 *
 * More complex [Selector]s can be created using logical operators `and` and `or`, as well as the
 * `child` and `ancestor` functions.
 *
 * If two [Rule]s both match against the same [Node], and define the same property,
 * then the value from the last [Rule] will win (i.e. lower down the theme definition).
 *
 * By convention only, `PseudoStyles` start with a colon.
 * e.g. ":focused", ":selected", ":armed" etc.
 * Regular `Styles` are either the
 * name of the control (e.g. "text_area" or "button") or the role they play within a control,
 * with a period as the prefix.
 * (e.g. ".container", ".editor", ".divider" etc. )
 * However, for each `Style` and `PseudoStyle` used by Glok, there is a const `String`.
 * e.g. TEXT_AREA = "text_area", CONTAINER = ".container" and FOCUSED = ":focuses".
 * I prefer to use these, because then there no room for typos.
 *
 * Themes can be merged, so you could take the default theme, and merge it with a (small)
 * theme of your own making. See [ThemeProperty.combineWith] and [combineWith].
 *
 * See [Scene] for a Class Diagram.
 *
 */
class Theme {

    /**
     * Note, adding new rules will NOT affect a scene until [applyToScene] is called.
     */
    val rules = mutableListOf<Rule>()

    /**
     * This font is used when converting from `em` to [LogicalPixels].
     */
    val defaultFontProperty by optionalFontProperty(null)
    var defaultFont by defaultFontProperty


    fun isEmpty() = rules.isEmpty()
    fun isNotEmpty() = rules.isNotEmpty()

    fun createRule(selector: Selector): Rule {
        val rule = Rule(selector)
        rules.add(rule)
        return rule
    }

    /**
     * Combines two [Theme]s. Note, it is often preferable to use [ThemeProperty.combineWith],
     * rather than using this method directly.
     */
    infix fun combineWith(other: Theme): Theme {
        val newTheme = Theme()
        newTheme.rules.addAll(rules)
        newTheme.rules.addAll(other.rules)
        return newTheme
    }

    /**
     * Used when restyling an entire scene.
     *
     * Walks through all nodes in the scene, and for each node,
     * we call [collectedSettings] which iterates through ever rule in the theme to see if it applicable to
     * the node.
     *
     * If the rule is applicable, then we record each property/value pair (a setting) within that rule.
     * A node may match more than one rule, in which case, any property which was previously recorded will
     * be replaced by the new setting. (i.e. last rule wins).
     * Last-rule-wins isn't a good solution though, because it is VERY difficult to override only some
     * rules when combining more than one theme.
     *
     * When we have finished walking through the entire tree of nodes, we then set each property
     * with the recorded value.
     */
    internal fun applyToScene(scene: Scene) {
        val collectedSettings = CollectedSettings()

        scene.root.forEachDescending { node ->
            node.clearCachedRules()
            collectedSettings(scene, node, collectedSettings)
        }

        collectedSettings.applySettings()
    }

    /**
     * Perform the same operations as [applyToScene], but for a single node.
     *
     * When a new node is added to the scene graph, we need to do the same process to that node
     * as we did to all the other nodes in [applyToScene].
     *
     * NOTE. When [Node.styles] or [Node.pseudoStyles] are changed, this is NOT used to re-style the node.
     * When [Node.styles] are changed, the entire scene is re-themed.
     * When [Node.pseudoStyles] are changed, [Node.restyleFromCachedRules] partially restyles the properties.
     *
     */
    internal fun applyToNewNode(newNode: Node) {

        newNode.scene?.let { scene ->
            val collectedSettings = CollectedSettings()
            newNode.forEachDescending { node ->
                collectedSettings(scene, node, collectedSettings)
            }
            collectedSettings.applySettings()
        }
    }

    /**
     * When a node is removed from the scene graph, we can dispose of the cached data,
     * for it, and all of its descendants.
     */
    internal fun removedNode(node: Node) {
        node.forEachDescending { child ->
            child.clearCachedRules()
        }
    }

    /**
     * Caches the rules for a single node.
     * This is the common code use by [applyToScene] and [applyToNewNode].
     */
    private fun collectedSettings(
        scene: Scene, node: Node, collectedSettings: CollectedSettings
    ) {
        val styleOnlyForNode = mutableMapOf<StylableProperty<*>, ValueAndImportance>()

        for ((order, rule) in rules.withIndex()) {
            val selector = rule.selector
            val importance = selector.importance()

            if (selector is RootSelector) {
                collectedSettings.addSettings(rule.settings.createSettersForNode(scene, selector, importance, order))
            }

            if (selector.couldMatch(node)) {
                val setters = rule.settings.createSettersForNode(node, selector, importance, order)

                // This rule *MIGHT* apply to this node
                if (selector.alwaysMatches(node)) {
                    // rule matches regardless of pseudo.
                    collectedSettings.addSettings(setters)
                    styleOnlyForNode.putAll(setters)
                } else {
                    // rule matches DEPENDING on pseudo.

                    if (selector.matches(node)) {
                        collectedSettings.addSettings(setters)
                    }
                    if (setters.isNotEmpty()) {
                        val ruleCache = Node.RuleCache(selector, styleOnlyForNode, setters)
                        node.cachedRuleSettings.add(ruleCache)
                    }
                }
            }
        }

    }

    /**
     * For debugging only, prints the [Theme] to the console.
     * I've tried to make the debugging information as close to the DSL as possible,
     * but there are differences, so you can't take the output of [dump] and use
     * it as actual Theme DSL code :-(
     */
    fun dump() {
        for (rule in rules) {
            println(rule)
        }
    }

    /**
     * A [Rule] is the combination of the [Selector], which determines which [Node]s will
     * be affected by the rule, and the [RuleSettings] which contain the property
     * values that will be set.
     *
     * You can think of a [Theme] applying this simple logic to every node in the scene graph :
     *
     * If node matches [selector], apply these [settings].
     */
    inner class Rule(val selector: Selector) {
        val settings = RuleSettings()

        fun createRule(selector: Selector) = this@Theme.createRule(selector)

        override fun toString() = "$selector {$settings}"
    }


    /**
     * When applying a theme, we collect all the settings, and then apply them.
     */
    private class CollectedSettings {
        private val settings = mutableMapOf<StylableProperty<*>, ValueAndImportance>()

        fun addSettings(foo: Map<StylableProperty<*>, ValueAndImportance>) {
            for ((prop: StylableProperty<*>, valueAndImportance: ValueAndImportance) in foo.entries) {
                val existing = settings[prop]
                if (existing == null || existing < valueAndImportance) {
                    settings[prop] = valueAndImportance
                }
            }
        }

        fun applySettings() {
            settings.forEach { (property, valueAndImportance) -> property.style(valueAndImportance.value) }
        }
    }

}

/**
 * An [ObservableTheme], whose value is the merge of two other [Theme]s.
 */
infix fun ObservableTheme.combineWith(other: ObservableTheme) =
    ThemeBinaryFunction(this, other) { a, b -> a.combineWith(b) }

infix fun ThemeProperty.combineWith(other: Theme) =
    ThemeUnaryFunction(this) { a -> a.combineWith(other) }

infix fun Theme.combineWith(other: ObservableTheme) =
    ThemeUnaryFunction(other) { b -> this.combineWith(b) }


/**
 * The [value] of a setting within a theme's rule, and the importance of the rule.
 * The importance is governed by [Selector.importance], and in the case of a tie, then the [order]
 * of the rules are taken into account (with the later rule winning).
 */
internal data class ValueAndImportance(
    val value: Any,
    val importance: Int,
    val order: Int
) : Comparable<ValueAndImportance> {

    override fun compareTo(other: ValueAndImportance): Int {
        val fromImportance = importance - other.importance
        return if (fromImportance == 0) {
            order - other.order
        } else {
            fromImportance
        }
    }
}

package uk.co.nickthecoder.glok.scene

enum class Side(override val pseudoStyle: String) : WithPseudoStyle {

    TOP(":top"),
    BOTTOM(":bottom"),
    LEFT(":left"),
    RIGHT(":right");

    val text: String
        get() = "${name[0]}${name.substring(1).lowercase()}"

    fun isVertical() = this == LEFT || this == RIGHT
    fun isHorizontal() = this == TOP || this == BOTTOM

    fun opposite() = when (this) {
        TOP -> BOTTOM
        BOTTOM -> TOP
        LEFT -> RIGHT
        RIGHT -> LEFT
    }

    /**
     * LEFT/RIGHT -> HORIZONTAL
     *
     * TOP/BOTTOM -> VERTICAL
     */
    fun lrToHorizontal() = if (this == LEFT || this == RIGHT) {
        Orientation.HORIZONTAL
    } else {
        Orientation.VERTICAL
    }

    /**
     * LEFT/RIGHT -> VERTICAL
     *
     * TOP/BOTTOM -> HORIZONTAL
     */
    fun lrToVertical() = if (this == LEFT || this == RIGHT) {
        Orientation.VERTICAL
    } else {
        Orientation.HORIZONTAL
    }
}

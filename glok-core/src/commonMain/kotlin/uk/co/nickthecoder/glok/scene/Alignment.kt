package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.text.TextVAlignment

enum class Alignment(val vAlignment: VAlignment, val hAlignment: HAlignment) {

    TOP_LEFT(VAlignment.TOP, HAlignment.LEFT),
    TOP_CENTER(VAlignment.TOP, HAlignment.CENTER),
    TOP_RIGHT(VAlignment.TOP, HAlignment.RIGHT),

    CENTER_LEFT(VAlignment.CENTER, HAlignment.LEFT),
    CENTER_CENTER(VAlignment.CENTER, HAlignment.CENTER),
    CENTER_RIGHT(VAlignment.CENTER, HAlignment.RIGHT),

    BOTTOM_LEFT(VAlignment.BOTTOM, HAlignment.LEFT),
    BOTTOM_CENTER(VAlignment.BOTTOM, HAlignment.CENTER),
    BOTTOM_RIGHT(VAlignment.BOTTOM, HAlignment.RIGHT);

    companion object {
        fun value(hAlignment: HAlignment, vAlignment: VAlignment) = values().first { it.hAlignment == hAlignment && it.vAlignment == vAlignment }
    }
}

enum class HAlignment(override val pseudoStyle: String) : WithPseudoStyle {
    LEFT(":left"),
    CENTER(":center"),
    RIGHT(":right")
}

enum class VAlignment(override val pseudoStyle: String, val textVAlignment: TextVAlignment) : WithPseudoStyle {
    TOP(":top", TextVAlignment.TOP),
    CENTER(":center", TextVAlignment.CENTER),
    BOTTOM(":bottom", TextVAlignment.BOTTOM);
}

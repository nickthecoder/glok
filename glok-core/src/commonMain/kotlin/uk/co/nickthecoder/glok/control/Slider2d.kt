/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.doubleProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.clamp

/**
 * A rectangular (often square) area where a point can be chosen.
 *
 * Similar to [SliderBase], except we have two independent values.
 */
class Slider2d : Region(), HasReadOnly {

    // region ==== Properties ====

    override val readOnlyProperty by booleanProperty(false)
    override var readOnly by readOnlyProperty

    // endregion


    // region ==== Fields ====
    /**
     * The selected point's Y value as a ratio of the width
     */
    val ratioXProperty by doubleProperty(0.0)
    var ratioX by ratioXProperty

    /**
     * The selected point Y value as a ratio of the height
     */
    val ratioYProperty by doubleProperty(0.0)
    var ratioY by ratioYProperty

    /**
     * The movable part
     */
    val thumb = Region().apply { styles.add(THUMB) }

    /**
     * The fixed part
     */
    val track = Region().apply { styles.add(TRACK) }

    override val children = listOf<Node>(track, thumb).asObservableList()
    // endregion

    // region ==== Commands ====
    @Suppress("unused")
    private val commands = Commands().apply {
        with(Slider2dActions) {
            LEFT { if (! readOnly) ratioX = (ratioX - 0.02).clamp(0.0, 1.0) }
            RIGHT { if (! readOnly) ratioX = (ratioX + 0.02).clamp(0.0, 1.0) }
            UP { if (! readOnly) ratioY = (ratioY + 0.02).clamp(0.0, 1.0) }
            DOWN { if (! readOnly) ratioY = (ratioY - 0.02).clamp(0.0, 1.0) }
        }
        attachTo(this@Slider2d)
    }
    // endregion

    init {
        styles.add(SLIDER_2D)

        readOnlyProperty.addChangeListener { _, _, isReadOnly ->
            if (isReadOnly) {
                pseudoStyles.add(READ_ONLY)
            } else {
                pseudoStyles.remove(READ_ONLY)
            }
        }
        claimChildren()

        // NOTE. We are setting the thumb's position in drawChildren,
        // so we do NOT need to requestLayout, only requestRedraw.
        ratioXProperty.addListener(requestRedrawListener)
        ratioYProperty.addListener(requestRedrawListener)

        onMouseEntered {
            if (! readOnly) {
                pseudoStyles.add(HOVER)
            }
        }

        onMouseExited {
            pseudoStyles.remove(HOVER)
        }

        onMousePressed { event ->
            if (!readOnly) {
                firstToRoot { it.focusAcceptable }?.requestFocus()
                // If we want clicks to move in increments
                // adjust(if (ratioForMouse(event) > ratio) 1 else -1, true)
                ratioX = ratioXForMouse(event).toDouble().clamp(0.0, 1.0)
                ratioY = ratioYForMouse(event).toDouble().clamp(0.0, 1.0)
                pseudoStyles.add(ARMED)
                event.capture()
            }
        }

        onMouseDragged { event ->
            ratioX = (ratioXForMouse(event).toDouble()).clamp(0.0, 1.0)
            ratioY = (ratioYForMouse(event).toDouble()).clamp(0.0, 1.0)
        }

        onMouseReleased {
            pseudoStyles.remove(ARMED)
        }
    }

    // region ==== Methods ====


    private fun ratioXForMouse(event: MouseEvent) =
        (event.sceneX - track.sceneX - track.borderSize.left) / (track.width - track.borderSize.x)

    private fun ratioYForMouse(event: MouseEvent) =
        1 - ((event.sceneY - track.sceneY - track.borderSize.top) / (track.height - track.borderSize.y))

    // endregion


    // region ==== Layout =====

    // Note, we are relying on the theme to set `prefWidth` or `prefHeight`
    // or the app developer setting them.
    override fun nodePrefWidth() = surroundX() + track.evalPrefWidth()

    override fun nodePrefHeight() = surroundY() + track.evalPrefHeight()

    /**
     * Note, the track takes up available space, excluding the border and padding.
     */
    override fun layoutChildren() {
        val availableX = width - surroundX()
        val availableY = height - surroundY()
        val thumbWidth = thumb.evalPrefWidth()
        val thumbHeight = thumb.evalPrefHeight()
        val x = surroundLeft()
        val y = surroundTop()

        setChildBounds(track, x, y, availableX, availableY)

        val positionX = ((track.width - track.borderSize.x) * ratioX).toFloat()
        val positionY = ((track.height - track.borderSize.y) * (1 - ratioY)).toFloat()

        setChildBounds(
            thumb,
            x + track.borderSize.left + positionX - thumbWidth / 2,
            y + track.borderSize.top + positionY - thumbHeight / 2,
            thumbWidth,
            thumbHeight
        )
    }

    /**
     * So that we don't need to ask for [requestLayout] every time a value changes,
     * we lay out the children as part of [drawChildren] too.
     */
    override fun drawChildren() {
        layoutChildren()
        super.drawChildren()
    }

    // endregion


    // region == SliderActions ==
    object Slider2dActions : Actions(null) {

        val LEFT = define("left", "Left", Key.LEFT.noMods())
        val RIGHT = define("right", "Right", Key.RIGHT.noMods())
        val UP = define("up", "Up", Key.UP.noMods())
        val DOWN = define("down", "Down", Key.DOWN.noMods())

    }
    // endregion

}

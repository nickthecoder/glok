/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.styles.DISABLED
import uk.co.nickthecoder.glok.theme.styles.GRAPHIC
import uk.co.nickthecoder.glok.util.max

abstract class LabelledMenuItem(text: String, graphic: Node? = null) : MenuItemBase(), HasDisabled {

    // region ==== Properties ====

    val textProperty by stringProperty(text)
    var text by textProperty

    val graphicProperty by stylableOptionalNodeProperty(graphic)
    var graphic by graphicProperty

    val fontProperty by stylableFontProperty(Font.defaultFont)
    var font by fontProperty

    val textColorProperty by stylableColorProperty(Color.BLACK)
    var textColor by textColorProperty

    val labelPaddingProperty by stylableEdgesProperty(Edges(0f))
    var labelPadding by labelPaddingProperty

    val graphicSizeProperty by stylableFloatProperty(0f)
    var graphicSize by graphicSizeProperty

    val graphicTextGapProperty by stylableFloatProperty(0f)
    var graphicTextGap by graphicTextGapProperty

    val textKeyCombinationGapProperty by stylableFloatProperty(0f)
    var textKeyCombinationGap by textKeyCombinationGapProperty

    final override val disabledProperty by booleanProperty(false)
    final override var disabled by disabledProperty

    val keyCombinationProperty by optionalKeyCombinationProperty(null)
    var keyCombination by keyCombinationProperty

    private val keyCombinationStringProperty: ObservableOptionalString =
        OptionalStringUnaryFunction(keyCombinationProperty) { it?.displayText }
    private val keyCombinationString by keyCombinationStringProperty

    // endregion properties

    // region ==== Fields ====

    val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children = mutableChildren.asReadOnly()

    private var textX = 0f
    private var textY = 0f
    private var keyCombinationX = 0f

    // endregion

    // region ==== init ====
    init {
        mutableChildren.addChangeListener(childrenListener)
        graphic?.let {
            it.styles.add(GRAPHIC)
            mutableChildren.add(it)
        }

        graphicProperty.addChangeListener { _, old, new ->
            mutableChildren.remove(old)
            old?.styles?.remove(GRAPHIC)
            new?.styles?.add(GRAPHIC)
            new?.let { mutableChildren.add(it) }
        }

        for (prop in listOf(
            textProperty, fontProperty, labelPaddingProperty, graphicSizeProperty, keyCombinationProperty
        )) {
            prop.addListener(requestLayoutListener)
        }

        disabledProperty.addChangeListener { _, _, isDisabled -> pseudoStyleIf(isDisabled, DISABLED) }
    }
    // endregion init

    // region ==== Methods ====

    // endregion methods

    // region ==== Layout ====

    override fun nodePrefWidth(): Float {
        val textWidth = font.widthOfOrZero(text, 1) + labelPadding.x
        val gap1 = if (graphic == null || text.isBlank()) 0f else graphicTextGap
        val combinationWidth = if (keyCombinationString == null) 0f else
            textKeyCombinationGap + font.widthOfOrZero(keyCombinationString ?: "", 1)
        return surroundX() + graphicSize + gap1 + textWidth + combinationWidth
    }

    override fun nodePrefHeight(): Float {
        val textHeight = font.height + labelPadding.y
        return surroundY() + max(textHeight, graphicSize)
    }

    override fun layoutChildren() {
        layoutMenuButtBase(width)
    }

    protected fun layoutMenuButtBase(width: Float) {
        var x = surroundLeft()
        val heightRemaining = height - surroundY()
        graphic?.let { graphic ->
            setChildBounds(graphic, x, surroundTop() + (heightRemaining - graphicSize) / 2, graphicSize, graphicSize)
        }
        x += graphicSize + graphicTextGap
        textX = x
        textY = surroundTop() + (heightRemaining - font.height - labelPadding.y) / 2 + labelPadding.top
        keyCombinationString?.let {
            keyCombinationX = width - surroundRight() - font.widthOfOrZero(it, 1)
        }
    }

    override fun draw() {
        super.draw()
        font.drawTopLeft(text, textColor, textX + sceneX, textY + sceneY, 1)
        keyCombinationString?.let {
            font.drawTopLeft(it, textColor, keyCombinationX + sceneX, textY + sceneY, 1)
        }
    }
    // endregion layout

    override fun toString() =
        super.toString() + " $text" + (if (graphic != null) " +graphic" else "") + " graphicSize=$graphicSize"

}

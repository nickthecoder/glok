package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.scene.Node

/**
 * In JavaFX, `Control`s have 3 attributes which make them different from other nodes.
 *
 * * A Skin, which can be switched out for a different one.
 * * Tooltips.
 * * A context menu.
 *
 * I'm not sure if changing skin is worth it. Lots of people style controls to make them look different,
 * but I've never seen an alternate skin being used.
 * I do like the very clear separation of the front-facing API within the Control and all the _private_ bits
 * in the Skin though.
 *
 * The other two aren't a big deal, and in fact can be achieved without subclassing Control.
 */
@Deprecated("Glok doesn't have skins, so there's currently little point in having a Control class.")
abstract class Control : Node() {
}

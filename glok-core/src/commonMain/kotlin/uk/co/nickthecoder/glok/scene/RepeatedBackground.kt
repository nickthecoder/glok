/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend

/**
 * A repeated background pattern from an [Image].
 */
class RepeatedBackground(val image: Image?, val tinted: Boolean) : Background {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        val x2 = x + size.left
        val y2 = y + size.top
        val width2 = width - size.left - size.right
        val height2 = height - size.top - size.bottom

        image?.let { image ->
            // I should probably do this in specialised OpenGL shader, but...
            val across = (width2 / image.imageWidth).toInt()
            val down = (height2 / image.imageHeight).toInt()
            backend.clip(x2, y2, width2, height2) {
                for (i in 0..across) {
                    for (j in 0..down) {
                        if (tinted) {
                            image.draw(x2 + i * image.imageWidth, y2 + j * image.imageHeight, color)
                        } else {
                            image.draw(x2 + i * image.imageWidth, y2 + j * image.imageHeight)
                        }
                    }
                }
            }
        }
    }
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON
import uk.co.nickthecoder.glok.theme.styles.SELECTED

/**
 * Behaves like a [ToggleButton], but also has a [value] associated with it.
 *
 * [V] is a simple data type, such as String?.
 * [P] is a Property whose value is of type [V], such as OptionalStringProperty.
 *
 * When a selected [PropertyRadioButton], the [property]'s value remains unchanged,
 * and the button remains selected.
 *
 * In contrast, when a selected [PropertyToggleButton] is pressed, the [property] is set to null
 * and no items are selected.
 *
 */
class PropertyRadioButton<V, P : Property<V>>(
    val property: P,
    /**
     * This button is selected when [property].value == [value]
     */
    val value: V,
    text: String,
    graphic: Node? = null

) : SelectButtonBase(text, graphic) {

    override val selectedProperty by booleanProperty(false)
    override var selected by selectedProperty

    init {
        selected = property.value == value
        style(RADIO_BUTTON)
        pseudoStyleIf(selected, SELECTED)

        selectedProperty.addChangeListener { _, _, selected ->
            pseudoStyleIf(selected, SELECTED)
            if (selected) {
                property.value = value
            }
        }
        property.addChangeListener { _, _, propValue -> selected = propValue == value }
    }

    override fun toggle() {
        property.value = value
    }

}

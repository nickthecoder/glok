package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.pixelsPerEm
import uk.co.nickthecoder.glok.property.functions.times
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.text.Font.Companion.defaultFont
import uk.co.nickthecoder.glok.theme.styles.GRAPHIC
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

/**
 * The base class for many Nodes which display a single line of text (and an optional `graphic`).
 * [Label] is the obvious subclass, but there are many others, including [Button].
 *
 * ## Anatomy
 *
 * Example 1. When [contentDisplay] = LEFT, [alignment] = BOTTOM_LEFT :
 *
 *    ◁╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌width╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌▷
 *    ◁╌╌╌╌╌╌╌╌╌╌╌╌╌prefWidth╌╌╌╌╌╌╌╌╌╌╌▷
 *                       ◁╌╌╌╌labelPadding.left╴
 *    ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓} borderSize.top
 *    ┃                                        ┃
 *    ┃                 ┌╌╌╌╌╌╌╌╌╌╌╌┐     s    ┃
 *    ┃   ┌╌╌╌╌╌╌╌┐     ┆           ┆     p    ┃ } labelPadding.top
 *    ┃   ┆       ┆     ┆   text    ┆     a    ┃
 *    ┃   ┆graphic┆     ┆           ┆     c    ┃ } labelPadding.bottom
 *    ┃   └╌╌╌╌╌╌╌┘     └╌╌╌╌╌╌╌╌╌╌╌┘     e    ┃
 *    ┃                                        ┃ } padding.bottom
 *    ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛ } borderSize.bottom
 *    ◁╌╌╌╌padding.left╴
 *                 ◁╌╌╌╌╌graphicTextGap╴
 *
 * If the width allocated to the Labelled == prefWidth, then there will be no space on the right
 * (apart from padding.right) and the horizontal part of [alignment] will make no difference.
 *
 * Example 2. When [contentDisplay] = TOP, [alignment] = TOP_CENTER :
 *    ┏━━━━━━━━━━━━━━━━━━━━┓ } borderSize.top
 *    ┃                    ┃ ╷ padding.top
 *    ┃     ┌╌╌╌╌╌╌╌┐      ┃ ╵
 *    ┃     ┆       ┆      ┃
 *    ┃     ┆graphic┆      ┃
 *    ┃     └╌╌╌╌╌╌╌┘      ┃ ╷
 *    ┃                    ┃ ┆ graphicTextGap
 *    ┃  ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐  ┃ ╵
 *    ┃  ┆              ┆  ┃ } labelPadding.top
 *    ┃  ┆     text     ┆  ┃
 *    ┃  ┆              ┆  ┃ } labelPadding.bottom
 *    ┃  └╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘  ┃
 *    ┃                    ┃ } padding.bottom
 *    ┃    empty space     ┃
 *    ┃                    ┃
 *    ┃                    ┃
 *    ┗━━━━━━━━━━━━━━━━━━━━┛ } borderSize.bottom
 *       ◁╌╌╌╌▷ labelPadding.left
 *    ◁╌▷ padding.left
 * If the height allocated to the Labelled == prefHeight, then there will be no space at the bottom
 * (apart from padding.bottom) and the vertical part of [alignment] will make no difference.
 *
 *
 * * The thick lines are the visible [border] (see super-class [Region]).
 * * The dashed inner lines are not drawn.
 * * The [graphic] has no padding of its own.
 *
 * The [graphic] which can be any [Node].
 *
 * [contentDisplay] determines where the [graphic] is compared to the text (the defaults is [ContentDisplay.LEFT]).
 *
 * When [contentDisplay] is `TOP` or `BOTTOM`, [graphicTextGap] is the Y distance, otherwise it is the
 * X distance.
 *
 * ### Theme DSL
 *
 *      ("label" or "button" or "toggleButton" ... ) {
 *
 *          alignment( value : Pos )
 *
 *          contentDisplay( value : ContentDisplay )
 *
 *          font( value : Font )
 *          font( size : Int, style : FontStyle, family : String... )
 *          font( size : Int, family : String... )
 *
 *          graphicTextGap( value : Number )
 *
 *          labelPadding( value : Edges )
 *          labelPadding( size : Number )
 *          labelPadding( topBottom : Number, leftRight : Number )
 *          labelPadding( top : Number, right : Number, bottom : Number, left : Number )
 *
 *          textColor( value : Color )
 *          textColor( value : String )
 *
 *      }
 *
 * Labelled inherits all the features of [Region].
 */
abstract class Labelled(text: String, graphic: Node? = null) : Region() {

    // ==== Properties ====

    val textProperty by stringProperty(text)
    var text by textProperty

    val graphicProperty by stylableOptionalNodeProperty(null)
    var graphic by graphicProperty

    val fontProperty by stylableFontProperty(defaultFont)
    var font by fontProperty

    val textColorProperty by stylableColorProperty(Color.BLACK)
    var textColor by textColorProperty

    val alignmentProperty by stylableAlignmentProperty(Alignment.CENTER_LEFT)
    var alignment by alignmentProperty

    val contentDisplayProperty by stylableContentDisplayProperty(ContentDisplay.LEFT)
    var contentDisplay by contentDisplayProperty

    val labelPaddingProperty by stylableEdgesProperty(Edges(0f))
    var labelPadding by labelPaddingProperty

    val graphicTextGapProperty by stylableFloatProperty(0f)
    var graphicTextGap by graphicTextGapProperty

    /**
     * When the text is longer than the available width, where should an ellipses (…) be placed,
     * and characters removed.
     * Default = [HAlignment.RIGHT].
     */
    val ellipsisAlignmentProperty by stylableHAlignmentProperty(HAlignment.RIGHT)
    var ellipsisAlignment by ellipsisAlignmentProperty

    // ==== End of Properties ====

    private var textX = 0f
    private var textY = 0f

    /**
     * Most of the time, this is the same as [text]. However, if [text] is too wide to fit,
     * then some text is replaced by an ellipsis (…)
     *
     * This is calculated within [layout], and used within [draw].
     */
    private var truncatedText: String = text

    protected val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children = mutableChildren.asReadOnly()

    // ==== End of fields ====

    init {
        mutableChildren.addChangeListener(childrenListener)

        // If any of there change, our pref size may change, so we need to perform a layout again.
        for (prop in arrayOf(
            textProperty, graphicProperty, fontProperty, alignmentProperty,
            contentDisplayProperty, labelPaddingProperty, graphicTextGapProperty,
        )) {
            prop.addListener(requestLayoutListener)
        }
        // And these properties require redraw, but not a layout.
        ellipsisAlignmentProperty.addListener(requestRedrawListener)
        textColorProperty.addListener(requestRedrawListener)

        graphicProperty.addChangeListener { _, old, new ->
            mutableChildren.remove(old)
            old?.styles?.remove(GRAPHIC)
            new?.styles?.add(GRAPHIC)
            new?.let { mutableChildren.add(it) }
        }

        graphic?.let {
            this.graphic = it
            it.styles.add(GRAPHIC)
        }

    }

    // ====End of init ====

    override fun Number.em(): ObservableOptionalFloat {
        return fontProperty.pixelsPerEm() * this.toFloat()
    }

    private fun displayedGraphic(): Node? = if (contentDisplay == ContentDisplay.TEXT_ONLY) null else graphic
    private fun displayedText(): String? {
        return if (text.isBlank() && graphic != null) {
            null
        } else {
            if (contentDisplay == ContentDisplay.GRAPHIC_ONLY && graphic != null) null else text
        }
    }

    override fun nodeMinWidth() = nodePrefWidth()
    override fun nodePrefWidth(): Float {
        val text = displayedText()
        val graphic = displayedGraphic()
        val textWidth = if (text == null) 0f else font.widthOf(text, 1) + labelPadding.x
        val graphicWidth = graphic?.evalPrefWidth() ?: 0f
        val gap = if (graphic == null || text == null) 0f else graphicTextGap
        return if (contentDisplay.isHorizontal()) {
            surroundX() + textWidth + graphicWidth + gap
        } else {
            surroundX() + max(textWidth, graphicWidth)
        }
    }

    override fun nodeMinHeight() = nodePrefHeight()
    override fun nodePrefHeight(): Float {
        val text = displayedText()
        val graphic = displayedGraphic()
        val textHeight = if (text == null) 0f else font.height + labelPadding.y
        val graphicHeight = graphic?.evalPrefHeight() ?: 0f
        val gap = if (graphic == null || text == null) 0f else graphicTextGap
        return if (contentDisplay.isVertical()) {
            surroundY() + textHeight + graphicHeight + gap
        } else {
            surroundY() + max(textHeight, graphicHeight)
        }
    }


    override fun layoutChildren() {
        layoutLabelled(width, height)
    }

    /**
     * Lays out the [text] and [graphic].
     * This is split off from [layoutChildren] so that subclasses can add additional nodes to [children],
     * which are placed to the right (or maybe below) the [Labelled] part of the control.
     */
    internal fun layoutLabelled(width: Float, height: Float) {

        val availableX = width - surroundX()
        val availableY = height - surroundY()

        val text = displayedText()
        val graphic = displayedGraphic()

        val isVertical = contentDisplay.isVertical()
        val contentDisplay = if (text == null || graphic == null) null else contentDisplay

        val textWidth = if (text == null) 0f else font.widthOfOrZero(text, 1) + labelPadding.x
        val graphicWidth = graphic?.evalPrefWidth() ?: 0f
        val textHeight = if (text == null) 0f else font.height + labelPadding.y
        val graphicHeight = graphic?.evalPrefHeight() ?: 0f
        val gap = if (graphic == null || text == null) 0f else graphicTextGap

        var graphicX: Float
        var graphicY: Float
        var textX: Float
        var textY: Float


        // Let's work out the position of the text and graphic, ignoring [alignment].
        // i.e. assuming alignment = top left.
        when (contentDisplay) {

            ContentDisplay.LEFT, null -> {
                graphicX = surroundLeft()
                graphicY = surroundTop()
                textX = graphicX + graphicWidth + gap
                textY = graphicY
            }

            ContentDisplay.RIGHT -> {
                textX = surroundLeft()
                textY = surroundTop()
                // NOTE. The following value is wrong, when the text is too wide.
                // It is corrected later (search for: special case)
                graphicX = textX + textWidth + gap
                graphicY = textY
            }

            ContentDisplay.TOP -> {
                graphicX = surroundLeft()
                graphicY = surroundTop()
                textX = graphicX
                textY = graphicY + graphicHeight + gap
            }

            ContentDisplay.BOTTOM -> {
                textX = surroundLeft()
                textY = surroundTop()
                graphicX = textX
                graphicY = textY + textHeight + labelPadding.bottom + gap
            }

            ContentDisplay.TEXT_ONLY -> {
                textX = surroundLeft()
                textY = surroundTop()
                graphicX = 0f // Not used
                graphicY = 0f // Not used
            }

            ContentDisplay.GRAPHIC_ONLY -> {
                textX = surroundLeft() // May be used, if there is no graphic
                textY = surroundTop() // May be used, if there is no graphic
                graphicX = textX
                graphicY = textY
            }
        }
        // We didn't take labelPadding into account in any of the cases above
        textX += labelPadding.left
        textY += labelPadding.top

        // Now we adjust textX,Y and graphicX,Y to take alignment into account.
        // The first step is to work out how much extra space is available.
        // When text and graphic are on the same row/column, then the extra X/Y is the
        // same for text and graphics.
        val textExtraX: Float
        val textExtraY: Float
        val graphicExtraX: Float
        val graphicExtraY: Float

        // We use max(0f, ...) because if the available width isn't enough,
        // that is taken care of in drawChildren() by replacing part of the text with '…'
        if (isVertical) {
            textExtraX = max(0f, availableX - textWidth)
            graphicExtraX = max(0f, availableX - graphicWidth)

            textExtraY = max(0f, availableY - textHeight - gap - graphicHeight)
            graphicExtraY = textExtraY
        } else {
            textExtraY = max(0f, availableY - textHeight)
            graphicExtraY = max(0f, availableY - graphicHeight)

            textExtraX = max(0f, availableX - textWidth - gap - graphicWidth)
            graphicExtraX = textExtraX
        }

        // A special case when the text is too wide, and graphic is right of the text.
        // We need to correct the value graphicX set above in : when (contentDisplay) ContentDisplay.RIGHT -> ...
        // The graphic is positioned at the far right of the control, instead of to the right of the text.
        if (contentDisplay == ContentDisplay.RIGHT) {
            graphicX = width - surroundRight() - graphicWidth
        }

        // Now we are ready to perform the alignment.
        when (alignment.hAlignment) {
            HAlignment.LEFT -> {} // Already correct

            HAlignment.CENTER -> {
                textX += textExtraX / 2
                graphicX += graphicExtraX / 2
            }

            HAlignment.RIGHT -> {
                textX += textExtraX
                graphicX += graphicExtraX
            }
        }

        when (alignment.vAlignment) {
            VAlignment.TOP -> {} // Already correct

            VAlignment.CENTER -> {
                textY += textExtraY / 2
                graphicY += graphicExtraY / 2
            }

            VAlignment.BOTTOM -> {
                textY += textExtraY
                graphicY += graphicExtraY
            }
        }

        // We now know the positions of the text and graphic.

        this.textX = textX
        this.textY = textY

        truncatedText = if (text == null) {
            ""
        } else if (width < nodePrefWidth()) {
            truncateText()
        } else {
            text
        }

        if (graphic != null) {
            setChildBounds(graphic, graphicX, graphicY, graphicWidth, graphicHeight)
        }
    }

    /**
     * When [text] is too wide, replace some text with and ellipsis (…)
     * See [truncatedText].
     */
    /*
     * I'm using the same kind of binary chop for all three ellipseAlignment options.
     * Start by removing half the characters, and then loop, removing or reinstating smaller and smaller
     * numbers of characters (given by var chop).
     *
     * During development, I used VisualTestLabelledEllipsis to check my work.
     */
    private fun truncateText(): String {
        val text = text
        // NOTE. We are using nodePrefWidth, not evalPrefWidth,
        // because we want to know the size of the text in pixels.
        val prefWidth = nodePrefWidth()
        // We subtract -1, because it is OK if the width is off by less than 1 pixel.
        // This is particularly important when using GlokSettings.globalScale which is non-integer,
        // due to rounding issues. Without this, labels become truncated when they are
        // at their preferred width, but with a tiny rounding error.
        if (width < prefWidth - 1f && text.length > 1) {
            val ellipsisAlignment = ellipsisAlignment
            val ellipsisWidth = font.widthOfOrZero("…", 0)
            // How much do we need to shrink the text by for it to fit?
            val delta = prefWidth - width + ellipsisWidth

            val length = text.length

            // We will use a binary chop, to home in the best number of character to remove.
            // chop (roughly) halves each time through the loop.
            var chop = length / 2
            // from and to are indices into [text] which will be replaced by …
            var to = when (ellipsisAlignment) {
                HAlignment.RIGHT -> length
                HAlignment.LEFT -> chop
                HAlignment.CENTER -> chop + chop / 2
            }
            var from = to - chop
            var removedWidth = font.widthOfOrZero(text.substring(from, to), 0)
            // Used to make rounding alternate between left and right
            var roundUpOrDown = 0

            while (true) {
                val fits = removedWidth >= delta

                chop = (chop + 1) / 2 // Round UP, not down when dividing by 2.
                if (fits) {
                    // Increase the amount of text to remove.
                    when (ellipsisAlignment) {
                        HAlignment.RIGHT -> {
                            if (chop > length - from) chop = length - from
                            removedWidth -= font.widthOfOrZero(text.substring(from, from + chop))
                            from += chop
                        }

                        HAlignment.LEFT -> {
                            if (chop > to) chop = to
                            removedWidth -= font.widthOfOrZero(text.substring(to - chop, to))
                            to -= chop
                        }

                        HAlignment.CENTER -> {
                            var deltaLeft: Int = if (chop % 2 == 1) {
                                roundUpOrDown = 1 - roundUpOrDown
                                (chop + roundUpOrDown) / 2
                            } else {
                                chop / 2
                            }
                            if (deltaLeft > length - from) deltaLeft = length - from
                            val deltaRight = chop - deltaLeft
                            removedWidth -= font.widthOfOrZero(text.substring(from, from + deltaLeft))
                            removedWidth -= font.widthOfOrZero(text.substring(to - deltaRight, to))
                            from += deltaLeft
                            to -= deltaRight
                        }
                    }
                } else {
                    // Decrease the amount of text to remove
                    when (ellipsisAlignment) {
                        HAlignment.RIGHT -> {
                            if (chop > from) chop = from
                            removedWidth += font.widthOfOrZero(text.substring(from - chop, from))
                            from -= chop
                        }

                        HAlignment.LEFT -> {
                            if (chop > length - to) chop = length - to
                            removedWidth += font.widthOfOrZero(text.substring(to, min(length, to + chop)))
                            to += chop
                        }

                        HAlignment.CENTER -> {
                            var deltaLeft: Int = if (chop % 2 == 1) {
                                roundUpOrDown = 1 - roundUpOrDown
                                (chop + roundUpOrDown) / 2
                            } else {
                                chop / 2
                            }
                            if (deltaLeft > from) deltaLeft = from
                            val deltaRight = chop - deltaLeft
                            removedWidth += font.widthOfOrZero(text.substring(max(0, from - deltaLeft), from))
                            if (to < text.length) {
                                removedWidth += font.widthOfOrZero(text.substring(to, min(length, to + deltaRight)))
                            }
                            from -= deltaLeft
                            to += deltaRight
                        }
                    }
                }

                if (chop < 2) {
                    // We can't chop anymore.
                    if (removedWidth < delta) {
                        when (ellipsisAlignment) {
                            HAlignment.RIGHT -> from --
                            HAlignment.LEFT -> to ++
                            HAlignment.CENTER -> to ++
                        }
                    }
                    return StringBuilder().apply {
                        if (from > 0) append(text.substring(0, from))
                        append("…")
                        if (to < length) append(text.substring(to))
                    }.toString()

                }

            }
        }
        return text
    }

    override fun drawChildren() {
        if (contentDisplay != ContentDisplay.TEXT_ONLY) {
            graphic?.drawAll()
        }
    }

    override fun draw() {
        super.draw()

        font.drawTopLeft(
            truncatedText, textColor,
            textX + sceneX,
            textY + sceneY,
            1
        )

    }

    override fun toString() =
        super.toString() + " $contentDisplay '$text'" + if (displayedGraphic() != null) " +graphic" else ""
}

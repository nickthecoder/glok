/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.boilerplate.ObservableColor
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

class RoundedBorder(val radius: Float) : Border {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        // If width or height are too small, make the radius smaller
        val r = min(min(radius, height / 2), width / 2)
        val innerR = r - max(size.top, size.left)

        val x1 = if (size.left == 0f) x else x + r
        val x3 = x + width
        val x2 = if (size.right == 0f) x3 else x3 - r

        val y1 = if (size.top == 0f) y else y + r
        val y3 = y + height
        val y2 = if (size.bottom == 0f) y3 else y3 - r

        with(backend) {
            fillRect(x1, y, x2, y + size.top, color) // Top
            fillRect(x, y1, x + size.left, y2, color) // Left
            fillRect(x3 - size.right, y1, x3, y2, color) // Right
            fillRect(x1, y3 - size.bottom, x2, y3, color) // Bottom

            if (size.top != 0f && size.left != 0f) {
                strokeQuarterCircle(x1, y1, x, y, color, r, innerR) // Top left corner
            }
            if (size.top != 0f && size.right != 0f) {
                strokeQuarterCircle(x2, y1, x3, y, color, r, innerR) // Top right corner
            }
            if (size.bottom != 0f && size.left != 0f) {
                strokeQuarterCircle(x1, y2, x, y3, color, r, innerR) // Bottom left corner
            }
            if (size.bottom != 0f && size.right != 0f) {
                strokeQuarterCircle(x2, y2, x3, y3, color, r, innerR) // Bottom right corner
            }
        }

    }

    override fun toString() = "RoundedBorder( $radius )"
}

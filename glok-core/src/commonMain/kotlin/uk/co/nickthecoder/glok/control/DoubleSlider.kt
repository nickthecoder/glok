/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.DoubleProperty
import uk.co.nickthecoder.glok.property.boilerplate.doubleProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedDoubleProperty
import uk.co.nickthecoder.glok.property.functions.div
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.theme.styles.SLIDER
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.max2DPs

/**
 * Edits a [DoubleProperty] by moving a slider.
 */
class DoubleSlider : SliderBase() {

    // region ==== Properties ====

    val valueProperty by validatedDoubleProperty(0.0) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    var value by valueProperty

    val minProperty by doubleProperty(0.0)
    var min by minProperty

    val maxProperty by doubleProperty(100.0)
    var max by maxProperty

    val smallStepProperty by doubleProperty(1.0)
    var smallStep by smallStepProperty

    val largeStepProperty by doubleProperty(10.0)
    var largeStep by largeStepProperty

    /**
     * When null, the slider will transition smoothly across the range. However, you can
     * set this, so that the value will snap to particular values.
     */
    val snapProperty: Property<Snap<Double>?> = SimpleProperty(null)
    var snap by snapProperty

    val markers: MutableObservableList<Double> = mutableListOf<Double>().asMutableObservableList()
    val minorMarkers: MutableObservableList<Double> = mutableListOf<Double>().asMutableObservableList()

    // endregion

    /**
     * The position of the slider as a proportion (0 when [value] == [min] to 1 when [value] == [max]).
     */
    override var ratio: Double
        get() = ratio(value)
        set(v) {
            val convertedValue = if (max <= min) min else min + ((max - min) * v).toFloat()
            value = snap?.snap(convertedValue) ?: convertedValue
        }

    init {
        styles.add(SLIDER)

        // When these properties change, we need to redraw this node.
        for (prop in listOf(minProperty, maxProperty, valueProperty)) {
            // NOTE. We are setting the thumb's position in drawChildren,
            // so we do NOT need to requestLayout, only requestRedraw.
            prop.addListener(requestRedrawListener)
        }
        markers.addListener(requestRedrawListener)
        minorMarkers.addListener(requestRedrawListener)

        smallRatioStepProperty.bindTo(smallStepProperty / (maxProperty - minProperty))
        largeRatioStepProperty.bindTo(largeStepProperty / (maxProperty - minProperty))
    }

    private fun ratio(value: Double) = if (max <= min) 0.0 else ((value - min) / (max - min))

    override fun drawChildren() {

        // Draw markers
        if (markers.isNotEmpty()) {
            drawMarkers(markers.map { ratio(it) }, markerLength)
        }
        if (minorMarkers.isNotEmpty()) {
            drawMarkers(minorMarkers.map { ratio(it) }, minorMarkerLength)
        }
        super.drawChildren()
    }
    // ==== Object Methods ====

    override fun toString() = super.toString() + " { ${min.max2DPs()} .. ${max.max2DPs()} } = ${value.max2DPs()}"

}

package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON2


class ToggleGroupDSL() {

    val toggleGroup = ToggleGroup()

    fun toggleButton(text: String, graphic: Node? = null, block: (ToggleButton.() -> Unit)? = null) =
        ToggleButton(text, graphic, toggleGroup).optionalApply(block)

    fun toggleButton(text: String, block: (ToggleButton.() -> Unit)? = null) =
        ToggleButton(text, null, toggleGroup).optionalApply(block)

    fun checkMenuItem(text: String, block: (ToggleMenuItem.() -> Unit)? = null) =
        ToggleMenuItem(text, toggleGroup).optionalApply(block)

    fun radioButton(text: String, graphic: Node? = null, block: (RadioButton.() -> Unit)? = null) =
        RadioButton(toggleGroup, text, graphic).optionalApply(block)

    fun radioButton(text: String, block: (RadioButton.() -> Unit)? = null) =
        RadioButton(toggleGroup, text).optionalApply(block)


    fun radioButton2(text: String, graphic: Node? = null, block: (RadioButton.() -> Unit)? = null) =
        RadioButton(toggleGroup, text, graphic).apply {
            styles.remove(RADIO_BUTTON)
            styles.add(RADIO_BUTTON2)
        }.optionalApply(block)

    fun radioButton2(text: String, block: (RadioButton.() -> Unit)? = null) =
        RadioButton(toggleGroup, text).apply {
            styles.remove(RADIO_BUTTON)
            styles.add(RADIO_BUTTON2)
        }.optionalApply(block)


    fun radioMenuItem(text: String, block: (RadioMenuItem.() -> Unit)? = null) =
        RadioMenuItem(toggleGroup, text).optionalApply(block)

    /**
     * Adds a [TitledPane], whose toggleGroup is set.
     * This makes it behave like JavaFX's `Accordion` control, i.e. only one TitlePane is expanded at any time.
     */
    fun accordionPane(text: String, block: (TitledPane.() -> Unit)? = null) =
        TitledPane(text).apply {
            expanded = false
            toggleGroup = this@ToggleGroupDSL.toggleGroup
        }.optionalApply(block)

}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.IntProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedIntProperty
import uk.co.nickthecoder.glok.theme.styles.INT_SPINNER
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.dumpStackTrace

class IntSpinner(initialValue : Int = 0) : ComparableSpinner<Int, IntProperty>() {

    // ==== Properties ====

    override val valueProperty by validatedIntProperty(0) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    override var value by valueProperty

    override val smallStepProperty by intProperty(1)
    override var smallStep by smallStepProperty

    override val largeStepProperty by intProperty(10)
    override var largeStep by largeStepProperty

    override val minProperty by intProperty(Int.MIN_VALUE)
    override var min by minProperty

    override val maxProperty by intProperty(Int.MAX_VALUE)
    override var max by maxProperty

    // ==== End of properties ====

    override var converter = object : Converter<Int, String> {
        override fun forwards(value: Int) = value.toString()
        override fun backwards(value: String) = value.toInt()
    }

    // ==== init ====

    init {
        styles.add(INT_SPINNER)
        initialise()
        value = initialValue
    }

    // ==== End of init ====

    override fun adjustment(direction: Int, byLargeStep: Boolean): Int {
        return (value + direction * (if (byLargeStep) largeStep else smallStep))
    }
}

package uk.co.nickthecoder.glok.backend

/**
 * A rectangle using Floats.
 * Used to pass rectangles from Glok's core to the [Backend].
 */
data class GlokRect(val left: Float, val top: Float, val right: Float, val bottom: Float) {

    constructor() : this(0f, 0f, 0f, 0f)

    val width get() = right - left
    val height get() = bottom - top


    fun plus(dx: Float, dy: Float) = GlokRect(
        left + dx,
        top + dy,
        right + dx,
        bottom + dy,
    )

    fun contains(other: GlokRect): Boolean =
        left <= other.left && right >= other.right &&
        top <= other.bottom && bottom >= other.top

    fun overlaps(other: GlokRect): Boolean =
        left < other.right && right > other.left &&
        top < other.bottom && bottom > other.top

    override fun equals(other: Any?): Boolean {
        if (other !is GlokRect) {
            return false
        }
        return other.left == left && other.bottom == bottom && other.right == right && other.top == top
    }

    override fun toString(): String = "($left , $top, $right , $bottom)"

}

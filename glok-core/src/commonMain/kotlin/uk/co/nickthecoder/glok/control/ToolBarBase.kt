/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOrientation
import uk.co.nickthecoder.glok.property.boilerplate.stylableSideProperty
import uk.co.nickthecoder.glok.property.functions.lrToVertical
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.box
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.theme.styles.CONTAINER
import uk.co.nickthecoder.glok.theme.styles.OVERFLOW_BUTTON
import uk.co.nickthecoder.glok.theme.styles.TOOL_BAR_OVERFLOW
import uk.co.nickthecoder.glok.util.log

/**
 * ToolBarBase contains the implementation of [ToolBar] and [MenuBar],
 * which are identical, but may be styled differently.
 *
 * [ToolBar]'s [styles] contains "tool_bar", whereas [MenuBar] contains "menu_bar",
 * and ToolBarBase has no styles.
 */
sealed class ToolBarBase(side: Side = Side.TOP) : Region(), WithItems {

    // region ==== Properties ====

    val sideProperty by stylableSideProperty(side)
    var side by sideProperty

    val orientationProperty: ObservableOrientation = sideProperty.lrToVertical()
    val orientation by orientationProperty

    // endregion properties

    // region ==== Fields ====

    /**
     * A specialised [BoxBase] which holds the [items].
     */
    private val container = ToolBarContainer()

    /**
     * Nodes which appear in the ToolBar. Most commonly [Button]s, but can be any [Node]s.
     */
    override val items = container.children

    private val overflowButton = ToggleButton("…").apply {
        styles.add(OVERFLOW_BUTTON)
        onAction {
            if (selected) {
                showOverflow()
            } else {
                overflowStage?.close()
            }
        }
    }

    /**
     * Contains [container] and [overflowButton].
     * [overflowButton] is not always visible, but is always present.
     */
    final override val children = listOf(container, overflowButton).asObservableList()

    private var overflowItems = listOf<Node>()
    private var overflowStage: Stage? = null

    // endregion fields

    // region ==== init ====
    init {
        claimChildren()

        sideProperty.addChangeListener { _, old, new ->
            pseudoStyles.remove(old.pseudoStyle)
            pseudoStyles.add(new.pseudoStyle)
            overflowStage?.close()
            requestLayout()
        }

        pseudoStyles.add(side.pseudoStyle)
    }
    // endregion init

    // region ==== Methods ====


    private fun showOverflow() {
        val myScene = scene ?: return
        val parentStage = myScene.stage ?: return

        overflowStage = overlayStage(parentStage, StageType.POPUP) {
            scene {
                root = box(orientation.orthogonal()) {
                    styles.add(TOOL_BAR_OVERFLOW)
                    children.addAll(overflowItems.map { ProxyNode(it) })
                }
            }

            onClosed {
                // Prevents the toggle button from firing if this is the button we clicked to
                // dismiss the overflowStage.
                // By clicking outside the overflowStage, the overflowButton.selected will be set to false
                // (regardless of where we clicked), so if we click it, it would be _selected_ again.
                overflowButton.armed = false
                overflowStage = null
                overflowButton.selected = false
                this@ToolBarBase.requestLayout()
            }

            show(overflowButton, side.opposite())
        }
    }

    // endregion methods

    // region ==== Layout ====
    override fun nodeMinWidth() = surroundX() + if (orientation == Orientation.HORIZONTAL) overflowButton.evalMinWidth() else container.evalMinWidth()
    override fun nodeMinHeight() = surroundY() + if (orientation == Orientation.VERTICAL) overflowButton.evalMinHeight() else container.evalMinHeight()
    override fun nodePrefWidth() = surroundX() + container.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + container.evalPrefHeight()
    override fun nodeMaxWidth() = NO_MAXIMUM
    override fun nodeMaxHeight() = NO_MAXIMUM

    override fun layoutChildren() {

        setChildBounds(container, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())

        if (container.children.isEmpty()) {
            overflowButton.visible = false
            return
        }

        // Lay out the container NOW. This isn't the usual order of things.
        // container's layoutChildren is a no-op, so it won't be laid out twice.
        container.layoutContainer()

        val last = items.last()
        if (orientation == Orientation.HORIZONTAL && last.localX + last.width > width - surroundRight() ||
            orientation == Orientation.VERTICAL && last.localY + last.height > height - surroundBottom()
        ) {
            // The toolbar isn't large enough to show all of its children.

            overflowButton.visible = true

            if (orientation == Orientation.HORIZONTAL) {
                setChildBounds(
                    overflowButton,
                    width - surroundX() - overflowButton.evalPrefWidth(),
                    surroundTop() + (height - surroundY() - overflowButton.evalPrefHeight()) / 2,
                    overflowButton.evalPrefWidth(),
                    overflowButton.evalPrefHeight()
                )
                container.width -= overflowButton.width
            } else {
                setChildBounds(
                    overflowButton,
                    surroundLeft() + (width - surroundX() - overflowButton.evalPrefWidth()) / 2,
                    height - surroundY() - overflowButton.evalPrefHeight(),
                    overflowButton.evalPrefWidth(),
                    overflowButton.evalPrefHeight()
                )
                container.height -= overflowButton.height

            }

            // Any children that are beyond the end of the container's bound are hidden.
            overflowItems = if (orientation == Orientation.HORIZONTAL) {
                items.filter { item -> item.localX + item.width + container.spacing > overflowButton.localX }
            } else {
                items.filter { item -> item.localY + item.height + container.spacing > overflowButton.localY }
            }
            // Give the overflow items their pref size, as we don't need to constrain them
            container.layoutOverflowItems()

        } else {
            overflowButton.visible = false
            overflowItems = emptyList()
        }

    }
    // endregion layout

    // region ==== Object Methods ====

    override fun toString() = super.toString() + " $orientation"

    // endregion

    // region == inner class ToolBarContainer ====

    private inner class ToolBarContainer : BoxBase() {

        override val fill get() = false
        override val orientation get() = this@ToolBarBase.orientation

        override val children = mutableListOf<Node>().asMutableObservableList().apply {
            addChangeListener(childrenListener)
        }

        init {
            styles.add(CONTAINER)
        }

        override fun nodeMinWidth() = if (orientation == Orientation.HORIZONTAL) 0f else super.nodeMinWidth()
        override fun nodeMinHeight() = if (orientation == Orientation.VERTICAL) 0f else super.nodeMinHeight()
        override fun nodeMaxWidth() = NO_MAXIMUM
        override fun nodeMaxHeight() = NO_MAXIMUM

        /**
         * [ToolBar.layoutChildren] is responsible for layout out my children, so this does nothing.
         * See [layoutContainer].
         */
        override fun layoutChildren() {}

        /**
         * Called by [ToolBar.layoutChildren]. Gives all nodes in overflowItems their pref width.
         * Without this, the children may be allocated less than their pref size
         * (because space is limited in the ToolBar),
         * but we can afford to give them their pref size in the overflowPopup.
         */
        fun layoutOverflowItems() {
            for (child in overflowItems) {
                setChildBounds(child, child.localX, child.localY, child.evalPrefWidth(), child.evalPrefHeight())
            }
        }

        /**
         * Called by [ToolBar.layoutChildren]
         */
        fun layoutContainer() {
            super.layoutChildren()
        }

        override fun drawChildren() {
            for (child in children) {
                if (child.visible && !overflowItems.contains(child)) {
                    try {
                        child.drawAll()
                    } catch (e: Exception) {
                        log.severe(e)
                    }
                }
            }
        }

    }
    // endregion inner class ToolBarContainer
}

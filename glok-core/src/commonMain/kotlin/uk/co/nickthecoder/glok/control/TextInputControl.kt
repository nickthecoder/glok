package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.GlokTimer
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.pixelsPerEm
import uk.co.nickthecoder.glok.property.functions.times
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.dsl.popupMenu
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.Indentation
import uk.co.nickthecoder.glok.theme.styles.READ_ONLY

/**
 * The base class for [TextField] and [TextArea]
 */
sealed class TextInputControl : Region(), HasReadOnly {

    // region ==== Properties ====

    abstract val textSelectedProperty : ObservableBoolean
    abstract val textSelected : Boolean

    val fontProperty by stylableFontProperty(Font.defaultFont)
    var font by fontProperty

    /**
     * The color of the text. Highlighted (selected) text uses [highlightTextColorProperty]
     */
    val textColorProperty by stylableColorProperty(Color.BLACK)
    var textColor by textColorProperty

    val caretColorProperty by stylableColorProperty(Color.BLACK)
    var caretColor by caretColorProperty

    /**
     * The background color of highlighted (selected) text.
     */
    val highlightColorProperty by stylableColorProperty(Color.LIGHT_BLUE)
    var highlightColor by highlightColorProperty

    /**
     * The color of the highlighted (selected) text.
     */
    val highlightTextColorProperty by stylableColorProperty(Color.LIGHT_BLUE)
    var highlightTextColor by highlightTextColorProperty

    final override val readOnlyProperty by booleanProperty(false)
    final override var readOnly by readOnlyProperty

    /**
     * Should the Tab key insert a Tab character or spaces?
     * How many columns should a tab character appear as / how many spaces should we insert
     * when the `Tab` key is pressed?
     *
     * This is initially bound to [GlokSettings.indentationProperty],
     * so if you wish to change it for a single [TextArea], then first unbind, and then set the value.
     *
     * [TextArea] and [StyledTextArea] support [Indentation.behaveLikeTabs], which positions the caret
     * to indentation boundaries whenever possible. Editing space-based documents feels as if they were
     * tab-based. This is preferred mode, and I'm very surprised that other editors do not support it.
     */
    val indentationProperty by indentationProperty(GlokSettings.indentation)
    var indentation by indentationProperty

    /**
     * This image for the caret. This image is scaled to match the font's height, maintaining its aspect ratio.
     */
    val caretImageProperty by stylableOptionalImageProperty(null)
    var caretImage by caretImageProperty

    // endregion Properties

    // region ==== Fields ====

    protected var caretVisible: Boolean = true
    protected val blinkTimer = GlokTimer(500.0, true) {
        caretVisible = ! caretVisible
        requestRedraw()
    }

    // endregion

    // region ==== init ====
    init {
        fontProperty.addListener(requestLayoutListener)

        readOnlyProperty.addChangeListener { _, _, isReadOnly ->
            pseudoStyleIf(isReadOnly, READ_ONLY)
        }

        focusedProperty.addChangeListener { _, _, isFocused ->
            if (isFocused) {
                resetBlink()
            } else {
                blinkTimer.stop()
            }
        }

        for (prop in listOf(
            textColorProperty, highlightColorProperty, highlightTextColorProperty, indentationProperty
        )) {
            prop.addListener(requestRedrawListener)
        }
    }

    // endregion init

    // region ==== Public Methods ====

    override fun Number.em(): ObservableOptionalFloat {
        return fontProperty.pixelsPerEm() * this.toFloat()
    }

    // endregion

    // region ==== Non Public Methods ====

    protected fun resetBlink() {
        caretVisible = true
        blinkTimer.start()
    }

    /**
     * @return The width of the given [text] in pixels (based on the [font]).
     */
    protected fun widthOf(text: String) = font.widthOfOrZero(text, indentation.columns)

    protected fun drawText(text: String, x: Float, y: Float, color: Color) {
        font.drawTopLeft(text, color, x, y, indentation.columns)
    }

    /**
     * Draws a line of text in 3 parts :
     * * Before the selection in [textColor]
     * * The selected part in [highlightTextColor]
     * * After the selection in [textColor]
     */
    protected fun drawLineText(x: Float, y: Float, text: String, start: Int, end: Int) {
        val before = text.substring(0, start)
        val selected = text.substring(start, end)
        val upToEnd = text.substring(0, end)
        val after = text.substring(end)
        val startX = x + font.widthOfOrZero(before, indentation.columns)
        val endX = x + font.widthOfOrZero(upToEnd, indentation.columns)
        drawText(before, x, y, textColor)
        drawText(selected, startX, y, highlightTextColor)
        drawText(after, endX, y, textColor)
    }


    protected fun showContextMenu(event: MouseEvent, commands: Commands, canChange: Boolean) {
        popupMenu {
            commands.build {
                with(TextAreaActions) {
                    if (canChange) {
                        + menuItem(UNDO)
                        + menuItem(REDO)
                        + Separator()
                        + menuItem(CUT)
                    }
                    + menuItem(COPY)
                    if (canChange) {
                        + menuItem(PASTE)
                        + menuItem(DUPLICATE)
                    }
                    + menuItem(SELECT_ALL)
                    + menuItem(SELECT_NONE)
                }
            }
        }.show(event.sceneX, event.sceneY, scene !!.stage !!)
    }
    // endregion non-public methods

}

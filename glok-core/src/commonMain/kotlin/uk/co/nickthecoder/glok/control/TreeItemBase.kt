/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.combineActionEventHandlers
import uk.co.nickthecoder.glok.property.boilerplate.SimpleIntProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty

/**
 * Common code shared by [TreeItem] and [MixedTreeItem]
 */
abstract class TreeItemBase {

    protected val availableProperty by booleanProperty(true)
    /**
     * `False` if any of the ancestors are not [expanded].
     * The root item is always available (even when [MixedTreeView.showRoot] == false).
     */
    var available by availableProperty
        protected set


    /**
     * An event handler which is called when this item is expanded, but before the tree is actually expanded.
     * This gives you the opportunity to adjust [children].
     * i.e. [children] can be lazily evaluated, just in time.
     */
    val onExpandingProperty by optionalActionEventHandlerProperty(null)
    var onExpanding by onExpandingProperty

    /**
     * An event handler which is called when this item is expanded, but before the tree is actually expanded.
     * This gives you the opportunity to adjust [children].
     * i.e. [children] can be lazily evaluated, just in time.
     */
    val onContractedProperty by optionalActionEventHandlerProperty(null)
    var onContracted by onContractedProperty


    val expandedProperty by booleanProperty(false)
    var expanded by expandedProperty

    val leafProperty by booleanProperty(false)
    var leaf by leafProperty

    internal val mutableLevelProperty = SimpleIntProperty(-1)

    /**
     * The root is level 0. Its children are level 1 etc.
     */
    val levelProperty = mutableLevelProperty.asReadOnly()
    internal var level by mutableLevelProperty

    // region ==== Events ====

    /**
     * A convenient way to set [onExpandingProperty] using a lambda.
     */
    fun onExpanding(handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (event: ActionEvent) -> Unit) {
        combineActionEventHandlers(onExpandingProperty, handlerCombination, block)
    }

    /**
     * A convenient way to set [onContractedProperty] using a lambda.
     */
    fun onContracted(handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (event: ActionEvent) -> Unit) {
        combineActionEventHandlers(onContractedProperty, handlerCombination, block)
    }

    // endregion Events


}

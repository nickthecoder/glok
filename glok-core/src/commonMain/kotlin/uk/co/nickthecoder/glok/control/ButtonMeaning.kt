/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

/**
 * Metadata for nodes added to a [ButtonBar], which determines the order the nodes appear with the bar.
 *
 * See [ButtonBar.add] and [ButtonBar.meaningOf]
 */
enum class ButtonMeaning(val code: Char) {
    /**
     * Code `A`. Save the changes, without closing the dialog.
     */
    APPLY('A'),

    /**
     * Code`B`. Back / Previous
     */
    BACK('B'),

    /**
     * Code`C`. Close the dialog without saving the changes.
     */
    CANCEL('C'),

    /**
     * Code`I`. Often used by the last step of a _wizard_.
     *
     * BEWARE, the code isn't obvious! I've followed JavaFX convention, and
     * I'm not sure why they didn't use `F` here!?!
     */
    FINISH('I'),

    /**
     * Code`H`. There are two _help_ meanings. On Windows this one is on the right,
     * and HELP_2 is on the left.
     *
     * On other platforms, both are on the left.
     */
    HELP('H'),

    /**
     * Code `E`. There are two _help_ meanings. On Windows this one is on the right,
     * and HELP_2 is on the left.
     *
     * On other platforms, both are on the left.
     */
    HELP_2('E'),

    /**
     * Code`L`. Always left aligned.
     */
    LEFT('L'),

    /**
     * Code `X`. Next / Forwards
     *
     * BEWARE. The code isn't obvious! (`N` is used by [NO])
     */
    NEXT('X'),

    /**
     * Code`N`. Used by a question with yes/no replies.
     *
     * If there is a third option, think carefully which [ButtonMeaning] it should have.
     * It's position should be sensible on all platforms :
     * [ButtonBar.WINDOWS_ORDER], [ButtonBar.MAC_OS_ORDER], [ButtonBar.GNOME_ORDER]
     */
    NO('N'),

    /**
     * Code`O`. Save changes, and close
     */
    OK('O'),

    /**
     * Code `U`. For buttons whose meaning do not fit any of the other categories.
     *
     * BEWARE, the code is not obvious (`O` is used for [OK]).
     */
    OTHER('U'),

    /**
     * Code `R`. Right aligned
     */
    RIGHT('R'),

    /**
     * Code `Y`. Used by a question with yes/no replies.
     *
     * If there is a third option, think carefully which [ButtonMeaning] it should have.
     * It's position should be sensible on all platforms :
     * [ButtonBar.WINDOWS_ORDER], [ButtonBar.MAC_OS_ORDER], [ButtonBar.GNOME_ORDER]
     */
    YES('Y')
}

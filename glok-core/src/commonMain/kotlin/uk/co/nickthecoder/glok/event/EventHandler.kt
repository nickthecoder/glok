package uk.co.nickthecoder.glok.event

import uk.co.nickthecoder.glok.property.boilerplate.OptionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.util.log

interface EventHandler<E : Event> {
    fun handle(event: E)
}

/**
 * When Glok calls event handlers, if they throw an exception, we catch it,
 * issue a `severe` message to the [log].
 */
internal fun <E : Event> EventHandler<E>.tryCatchHandle(event: E) {
    try {
        handle(event)
    } catch (e: Exception) {
        log.severe(e)
    }
}

/**
 * When Glok calls event handlers, if they throw an exception, we catch it,
 * issue a `severe` message to the [log].
 */
internal fun tryCatchHandle(block: () -> Unit) {
    try {
        block()
    } catch (e: Exception) {
        log.severe(e)
    }
}

internal fun actionEventHandler(block: (event : ActionEvent) -> Unit) = object : ActionEventHandler {
    override fun handle(event: ActionEvent) {
        block(event)
    }
}

internal fun combineActionEventHandlers(actionProperty: OptionalActionEventHandlerProperty, handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (ActionEvent) -> Unit) {
    val oldHandler = actionProperty.value
    actionProperty.value = if (oldHandler == null || handlerCombination == HandlerCombination.REPLACE) {
        actionEventHandler(block)
    } else {
        if (handlerCombination == HandlerCombination.BEFORE) {
            CompoundActionEventHandler(oldHandler, actionEventHandler(block))
        } else {
            CompoundActionEventHandler(oldHandler, actionEventHandler(block))
        }
    }
}

/**
 * See [EventHandlers]
 */
internal open class CompoundEventHandler<E : Event>(first: EventHandler<E>, second: EventHandler<E>) : EventHandler<E> {

    val handlers = mutableListOf(first, second)

    fun add(handler: EventHandler<E>, combination: HandlerCombination) {
        if (combination == HandlerCombination.BEFORE) {
            handlers.add(0, handler)
        } else {
            handlers.add(handler)
        }
    }

    fun remove(handler: EventHandler<E>) {
        handlers.remove(handler)
    }

    override fun handle(event: E) {
        for (h in handlers) {
            h.handle(event)
            if (event.isConsumed()) return
        }
    }
}

internal class CompoundActionEventHandler(first: ActionEventHandler, second: ActionEventHandler) :
    CompoundEventHandler<ActionEvent>(first, second), ActionEventHandler {
}

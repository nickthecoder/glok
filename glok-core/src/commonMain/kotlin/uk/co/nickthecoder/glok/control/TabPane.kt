/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.sizeProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.functions.greaterThan
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.CONTAINER
import uk.co.nickthecoder.glok.theme.styles.TAB_PANE
import uk.co.nickthecoder.glok.util.max

/**
 * Displays a row or column of [Tab]s. Clicking on a tab will display the tab's content.
 *
 * For greater flexibility, consider using a [TabBar] and a [SingleContainer] instead of a [TabPane].
 */
class TabPane(side: Side = Side.TOP) : Region(), Scrollable {

    val tabBar: TabBar = TabBar()

    //region ==== Properties ====

    val sideProperty = tabBar.sideProperty
    var side by tabBar.sideProperty

    val tabClosingPolicyProperty = tabBar.tabClosingPolicyProperty
    var tabClosingPolicy by tabBar.tabClosingPolicyProperty

    val currentContentProperty = tabBar.currentContentProperty
    val currentContent by tabBar.currentContentProperty

    /**
     * Hides the [TabBar] when there are 0 or 1 tabs, and shows it when there are more than 1.
     */
    val hideSingleTabProperty by booleanProperty(false)
    var hideSingleTab by hideSingleTabProperty

    //endregion

    //region ==== Fields ====

    val tabs = tabBar.tabs

    val container = SingleContainer().apply {
        style(CONTAINER)
    }

    private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children = mutableChildren.asReadOnly()

    val selection = tabBar.selection

    //endregion

    init {
        style(TAB_PANE)

        hideSingleTabProperty.addChangeListener { _, _, hideSingle ->
            if (hideSingle) {
                tabBar.visibleProperty.bindTo(tabs.sizeProperty().greaterThan(1))
            } else {
                tabBar.visibleProperty.unbind()
                tabBar.visible = true
            }
        }

        this.side = side
        mutableChildren.addChangeListener(childrenListener)
        mutableChildren.add(tabBar)
        mutableChildren.add(container)
        container.contentProperty.bindTo(tabBar.currentContentProperty)

        currentContentProperty.addChangeListener { _, old, new ->
            old?.section = false
            new?.section = true
        }
    }

    operator fun Tab.unaryPlus() {
        tabs.add(this)
    }

    override fun scrollTo(descendant: Node) {
        val tab = descendant.firstToRoot { it is Tab && tabs.contains(it) } as? Tab ?: return
        val content = tab.content ?: return
        if (! tab.disabled && descendant.isAncestorOf(content)) {
            println("TabPane $this\n   selecting tab $tab")
            selection.selectedItem = tab
        }
    }

    //region ==== Layout ====

    override fun nodePrefWidth() = if (side.lrToHorizontal() == Orientation.HORIZONTAL) {
        surroundX() + tabBar.evalPrefWidth() + (currentContent?.evalPrefWidth() ?: 0f)
    } else {
        surroundX() + max(tabBar.evalPrefWidth(), (currentContent?.evalPrefWidth() ?: 0f))
    }

    override fun nodePrefHeight() = if (side.lrToHorizontal() == Orientation.HORIZONTAL) {
        surroundY() + max(tabBar.evalPrefHeight(), (currentContent?.evalPrefHeight() ?: 0f))
    } else {
        surroundY() + tabBar.evalPrefHeight() + (currentContent?.evalPrefHeight() ?: 0f)
    }

    override fun layoutChildren() {
        var x = surroundLeft()
        var y = surroundTop()
        var availableX = width - surroundX()
        var availableY = height - surroundY()
        if (tabBar.visible) {
            when (side) {
                Side.TOP -> {
                    val firstHeight = tabBar.evalPrefHeight()
                    setChildBounds(tabBar, x, y, availableX, firstHeight)
                    y += firstHeight
                    availableY -= firstHeight
                }

                Side.BOTTOM -> {
                    val barPrefHeight = tabBar.evalPrefHeight()
                    val firstHeight = availableY - barPrefHeight
                    setChildBounds(tabBar, x, y + firstHeight, availableX, barPrefHeight)
                    availableY -= barPrefHeight
                }

                Side.LEFT -> {
                    val firstWidth = tabBar.evalPrefWidth()
                    setChildBounds(tabBar, x, y, firstWidth, availableY)
                    x += firstWidth
                    availableX -= firstWidth
                }

                Side.RIGHT -> {
                    val barPrefWidth = tabBar.evalPrefWidth()
                    val firstWidth = availableX - barPrefWidth
                    setChildBounds(tabBar, x + firstWidth, y, barPrefWidth, availableY)
                    availableX -= barPrefWidth
                }
            }
        }

        setChildBounds(container, x, y, availableX, availableY)

    }

    //endregion

}

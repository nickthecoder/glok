/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.scene.Stage

/**
 * See [Application.status].
 */
enum class ApplicationStatus {

    /**
     * The initial value. The status changes to [RUNNING] after [Application.start] has completed.
     */
    NOT_STARTED,

    RUNNING,

    /**
     * All stages will be requested to close. If the event sent to [Stage.onCloseRequested] is consumed,
     * (e.g. because there are unsaved files), then the stage is not closed, and therefore the application
     * does not end. (the status will revert to [RUNNING]).
     */
    REQUEST_QUIT,

    /**
     * Closes all stages without using [Stage.onCloseRequested], and therefore there is no possibility
     * to stop the application from ending.
     *
     * Each stage's [Stage.onClosed] are called as usual.
     */
    FORCE_QUIT,

    /**
     * Set only by Glok after all stages have been removed.
     *
     * You may add listener(s) to [Application.statusProperty], and check for this value to perform
     * last-minute tidy-up.
     */
    ENDED
}

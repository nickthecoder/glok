/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.scene.Node

/**
 * An easy way to build a control which has a single child.
 *
 * Many Glok controls are `final`, so you cannot extend them. This is deliberate for two reasons :
 *
 * 1. Inheritance breaks encapsulation (google it!)
 * 2. An API which allow inheritance is fragile, and future versions are much more likely to break old code.
 *    (this may be saying the same thing twice).
 *
 * So, for example, suppose you want a custom `ToolBar`. Instead of creating a subclass of `ToolBar`,
 * extend [WrappedNode], and make [inner] a `ToolBar`.
 *
 * A [WrappedNode] has a single child : [inner], which is a [Node] of type [N].
 */
abstract class WrappedNode<N : Node>(protected val inner: N) : Node() {

    override val children: ObservableList<Node> = listOf(inner).asObservableList()

    override fun nodeMinWidth() = inner.evalMinWidth()
    override fun nodeMinHeight() = inner.evalMinHeight()
    override fun nodePrefWidth() = inner.evalPrefWidth()
    override fun nodePrefHeight() = inner.evalPrefHeight()
    override fun nodeMaxWidth() = inner.evalMaxWidth()
    override fun nodeMaxHeight() = inner.evalMaxHeight()

    init {
        claimChildren()
    }

    override fun layoutChildren() {
        setChildBounds(inner, 0f, 0f, width, height)
    }

    override fun draw() {}
}

/**
 * Similar to [WrappedNode], but extends [Region], so we have the option of adding borders and padding.
 */
abstract class WrappedRegion<N : Node>(protected val inner: N) : Region() {

    override val children: ObservableList<Node> = listOf(inner).asObservableList()

    override fun nodeMinWidth() = surroundX() + inner.evalMinWidth()
    override fun nodeMinHeight() = surroundY() + inner.evalMinHeight()
    override fun nodePrefWidth() = surroundX() + inner.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + inner.evalPrefHeight()
    override fun nodeMaxWidth() = surroundX() + inner.evalMaxWidth()
    override fun nodeMaxHeight() = surroundY() + inner.evalMaxHeight()

    init {
        claimChildren()
    }

    override fun layoutChildren() {
        setChildBounds(inner, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
}

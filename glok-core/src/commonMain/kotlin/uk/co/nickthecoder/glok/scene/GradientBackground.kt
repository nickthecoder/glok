/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOrientation
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty

/**
 * Draws a [Background] as a linear gradient.
 * The left/bottom is [fromColor] and the right/top is [toColor] depending on [orientation].
 *
 */
class GradientBackground(
    val orientation: ObservableOrientation,
    fromColor: Color,
    toColor: Color
) : Background {

    val fromColorProperty by colorProperty(fromColor)
    var fromColor by fromColorProperty

    val toColorProperty by colorProperty(toColor)
    var toColor by toColorProperty

    override fun draw(x: Float, y: Float, width: Float, height: Float, color : Color, size: Edges) {
        val fromColor = fromColor
        val toColor = toColor
        val x2 = x + size.left
        val y2 = y + size.top
        val width2 = width - size.left - size.right
        val height2 = height - size.top - size.bottom
        backend.gradient(
            floatArrayOf(
                x2, y2, x2 + width2, y2, x2 + width2, y2 + height2,
                x2, y2, x2, y2 + height2, x2 + width2, y2 + height2
            ),
            if (orientation.value == Orientation.HORIZONTAL) {
                arrayOf(
                    fromColor, toColor, toColor,
                    fromColor, fromColor, toColor
                )
            } else {
                arrayOf(
                    toColor, toColor, fromColor,
                    toColor, fromColor, fromColor
                )
            }
        )
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.TextPosition

/**
 * A [HighlightRange], which is part of a matched pair, for example, an opening and closing bracket.
 *
 * Always create [PairedHighlightRange] in pairs, never alone, or with more than two items.
 *
 * Note, Scarea knows nothing of PairedHighlightRanges, and will happily remove one of them from its
 * list, leaving the other untouched.
 */
class PairedHighlightRange(
    start: TextPosition,
    end: TextPosition,
    highlight: Highlight,
    owner: Any? = null,
    other: PairedHighlightRange?

) : HighlightRange(start, end, highlight, owner) {

    /*
     * Note, because of a chicken and egg problem, we cannot have a simple 'val' for [other].
     * Instead, opening = null for the first of the pair, and then the second instance
     * will assign BOTH of the [other]s in init.
     */
    var other: PairedHighlightRange? = other
        private set

    init {
        if (other != null) {
            other.other = this
        }
    }

}

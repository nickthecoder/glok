/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

/**
 * Each character is assigned a category. We skip over character which have the same category as
 * the character at the start point.
 */
class SkipWordByCategory(
    val preSkipWhitespace: Boolean = true,
    val categories: List<(Char) -> Boolean>
) : SkipWord {

    override fun forwards(position: Int, text: CharSequence) = forwards(position, text, preSkipWhitespace)

    private fun forwards(caretPosition: Int, text: CharSequence, preSkipWhitespace: Boolean): Int {
        if (caretPosition >= text.length) return caretPosition
        var index = caretPosition

        if (preSkipWhitespace) {
            // Skip initial whitespace
            while (index < text.length && text[index].isWhitespace()) {
                index++
            }
        }

        val first = text[index]
        val category = categories.firstOrNull { it(first) }
        while (index < text.length) {
            val c = text[index]
            if (category == null) {
                if (c.isWhitespace()) break
            } else {
                if (!category(c)) break
            }
            index++
        }
        return index
    }

    override fun backwards(position: Int, text: CharSequence) = backwards(position, text, preSkipWhitespace)

    private fun backwards(caretPosition: Int, text: CharSequence, preSkipWhitespace: Boolean): Int {
        var index = caretPosition
        if (index == 0) return 0

        if (preSkipWhitespace) {
            // Skip initial whitespace
            while (index > 0 && text[index - 1].isWhitespace()) {
                index--
            }
            if (index == 0) return 0
        }

        val first = text[index - 1]
        val category = categories.firstOrNull { it(first) }


        while (index >= 0) {
            if (index == 0) return 0
            val c = text[index - 1]
            if (category == null) {
                if (c.isWhitespace()) break
            } else {
                if (!category(c)) break
            }
            index--
        }
        return index
    }

    override fun selectWord(position: Int, text: CharSequence): Pair<Int, Int> {

        val left = if (position > 0 && !text[position - 1].isWhitespace()) {
            backwards(position, text, false)
        } else {
            position
        }
        val right = if (position < text.length && !text[position].isWhitespace()) {
            forwards(position, text, false)
        } else {
            position
        }

        return Pair(left, right)
    }
}

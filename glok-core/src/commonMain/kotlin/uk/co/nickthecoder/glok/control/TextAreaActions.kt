/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Action
import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.control.TextAreaActions.INDENT
import uk.co.nickthecoder.glok.control.TextAreaActions.TAB
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.KeyCombination
import uk.co.nickthecoder.glok.event.KeyEvent

/**
 * [Action]s used by [TextArea] [StyledTextArea] and [TextField].
 *
 * [TextField] ignores all actions related to multi-line editing, as well as [INDENT]/[TAB].
 */
object TextAreaActions : Actions(null) {

    val UNDO = define("undo", "Undo", Key.Z.control())
    val REDO = define("redo", "Redo", Key.Z.control().shift()).apply {
        additionalKeyCombinations.add(Key.Y.control())
    }

    val SELECT_ALL = define("select_all", "Select All", Key.A.control())
    val SELECT_NONE = define("select_none", "Select None", Key.A.control().shift())

    val ENTER = define("enter", "Enter", Key.ENTER.noMods())

    /**
     * NOTE, There is a difference between [TAB] and [INDENT].
     * [INDENT] indents the current line (or multiple lines if there is a selection),
     * regardless of where the caret is.
     * [INDENT] has a [KeyCombination] of just the `Tab` key (no mods).
     *
     * The indentation uses either `tab` characters, or `spaces`, depending on [TextArea.indentation].
     *
     * NOTE. `TextField` does not use [INDENT] or [TAB].
     */
    val INDENT = define("indent", "Indent", Key.TAB.noMods())

    /**
     * [TAB] has two behaviours. If there is a multi-line selection, then it is the same as [INDENT].
     * If there is no selection, or the selection is limited to a single line,
     * then the current selection is replaced with a `TAB` character.
     * By default, [TAB] has no [KeyCombination], and is effectively disabled.
     * To enable it, change [INDENT]'s [KeyCombination] to null, and set [TAB]'s [KeyCombination]
     * to [Key.TAB].`noMods()`.
     *
     * PS. Adding tab characters in the middle of a line is, IMHO, daft, because the formatting of the text
     * becomes ambiguous.
     * Columns will only line up correctly if everybody sticks to a standard tab width.
     * There's a formal standard of 8 columns, which most developers ignore,
     * preferring 4 columns or 2 columns.
     *
     * This is why [INDENT] is enabled, and [TAB] isn't.
     */
    val TAB = define("insert_tab", "Insert Tab")

    /**
     * The opposite of [INDENT]. There is no opposite of [TAB].
     */
    val UNINDENT = define("unindent", "Unindent", Key.TAB.noMods().shift())

    val COPY = define("copy", "Copy", Key.C.control())
    val PASTE = define("paste", "Paste", Key.V.control())
    val DUPLICATE = define("duplicate", "Duplicate", Key.D.control())
    val CUT = define("cut", "Cut", Key.X.control())
    val DELETE = define("delete", "Delete", Key.DELETE.noMods())
    val BACKSPACE = define("backspace", "Backspace", Key.BACKSPACE.noMods())

    val LEFT = define("left", "Left", Key.LEFT.noMods())
    val SKIP_LEFT = define("skip_left", "Skip Left", Key.LEFT.control())
    val SELECT_LEFT = define("select_left", "Select Left", Key.LEFT.shift())
    val SELECT_SKIP_LEFT = define("select_skip_left", "Select Skip Left", Key.LEFT.control().shift())

    val RIGHT = define("right", "Right", Key.RIGHT.noMods())
    val SKIP_RIGHT = define("skip_right", "Skip Right", Key.RIGHT.control())
    val SELECT_RIGHT = define("select_right", "Select Right", Key.RIGHT.shift())
    val SELECT_SKIP_RIGHT = define("select_skip_right", "Select Skip Right", Key.RIGHT.control().shift())

    val UP = define("up", "Up", Key.UP.noMods())
    val SELECT_UP = define("select_up", "Select Up", Key.UP.shift())

    val DOWN = define("down", "Down", Key.DOWN.noMods())
    val SELECT_DOWN = define("select_down", "Select Down", Key.DOWN.shift())

    val START_OF_LINE = define("start_of_line", "Start of Line", Key.HOME.noMods())
    val SELECT_START_OF_LINE = define("select_start_of_line", "Select Start of Line", Key.HOME.shift())

    val START_OF_DOCUMENT = define("start_of_document", "Start of Document", Key.HOME.control())
    val SELECT_START_OF_DOCUMENT = define("select_start_of_document", "Select Start of Document", Key.HOME.shift().control())

    val END_OF_LINE = define("end_of_line", "End of Line", Key.END.noMods())
    val SELECT_END_OF_LINE = define("select_end_of_line", "Select End of Line", Key.END.shift())

    val END_OF_DOCUMENT = define("end_of_document", "End of Document", Key.END.control())
    val SELECT_END_OF_DOCUMENT = define("select_end_of_document", "Select End of Document", Key.END.shift().control())

    val PAGE_UP = define("page_up", "Select Page Up", Key.PAGE_UP.noMods())
    val SELECT_PAGE_UP = define("select_page_up", "Select Page Up", Key.PAGE_UP.shift())
    val PAGE_DOWN = define("page_up", "Select Page Up", Key.PAGE_DOWN.noMods())
    val SELECT_PAGE_DOWN = define("select_page_down", "Select Page Down", Key.PAGE_DOWN.shift())

}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.TextVAlignment
import uk.co.nickthecoder.glok.theme.styles.RULER
import uk.co.nickthecoder.glok.theme.styles.TOP
import uk.co.nickthecoder.glok.util.FixedFormat
import uk.co.nickthecoder.glok.util.Matrix
import kotlin.math.PI

/**
 * A horizontal or vertical ruler, often used at the top and left of a drawing (in a [BorderPane]).
 *
 *
 * ### Theme DSL
 *
 *     "ruler" {
 *
 *         font( value : Font )
 *         font( size : Int, style : FontStyle, family : String... )
 *         font( size : Int, family : String... )
 *
 *         format( value : Format ) // Converts numbers to Strings. See java.text.Format
 *
 *         textColor( value : Color )
 *         textColor( value : String )
 *
 *         ":top" { padding( ... ) }
 *         ":bottom" { padding( ... ) }
 *         ":left" { padding( ... ) }
 *         ":right" { padding( ... ) }
 *     }
 *
 * The padding determines the size of the markings along the ruler.
 *
 * Ruler inherits all the features of [Region].
 */
class Ruler(side: Side = Side.TOP) : Region() {

    val sideProperty by sideProperty(Side.TOP)
    var side by sideProperty

    val lowerBoundProperty by floatProperty(0f)
    var lowerBound by lowerBoundProperty

    val upperBoundProperty by floatProperty(100f)
    var upperBound by upperBoundProperty

    /**
     * If set to true, then the lower bound is at the bottom, and the upper bound at the top.
     * i.e. when your Y axis points upwards.
     *
     * This is designed for [side] = `LEFT` or `RIGHT`, but can also be used for `TOP` and `BOTTOM` too.
     */
    val reversedProperty by booleanProperty(false)
    var reversed by reversedProperty

    val stepProperty by floatProperty(0f)
    var step by stepProperty

    val subdivisionsProperty by intProperty(10)
    var subdivisions by subdivisionsProperty

    val fontProperty by stylableFontProperty(Font.defaultFont)
    var font by fontProperty

    val formatProperty by fixedFormatProperty(FixedFormat.places(1))
    var format by formatProperty

    val textColorProperty by stylableColorProperty(Color.BLACK)
    var textColor by textColorProperty

    /**
     * The position of a marker along the ruler. null (the default value) to hide the marker.
     * This marker is most frequently used to follow the mouse pointer.
     */
    val markerValueProperty by optionalFloatProperty(null)
    var markerValue by markerValueProperty

    /**
     * The color for the [markerValue].
     */
    val markerColorProperty by stylableColorProperty(Color.TRANSPARENT)
    var markerColor by markerColorProperty

    val markerThicknessProperty by stylableFloatProperty(1f)
    var markerThickness by markerThicknessProperty

    /**
     * The color for the [extraMarkers].
     */
    val extraMarkersColorProperty by stylableColorProperty(Color.TRANSPARENT)
    var extraMarkersColor by extraMarkersColorProperty

    /**
     * Any number of extra markers can be placed on the ruler.
     *
     * These could be used to indicate the positions of the document's margins, or the extent
     * of the currently selected shape within a diagram.
     *
     * The [markerValue] is drawn on top of the [extraMarkers] (i.e. [markerValue] is assumed to
     * be more important).
     */
    val extraMarkers = mutableListOf<Float>().asMutableObservableList()

    init {
        styles.add(RULER)
        pseudoStyles.add(TOP)

        sideProperty.addChangeListener { _, old, new ->
            pseudoStyles.remove(":${old.name.lowercase()}")
            pseudoStyles.add(":${new.name.lowercase()}")
        }

        for (prop in listOf(
            reversedProperty, stepProperty, subdivisionsProperty, fontProperty,
            formatProperty, textColorProperty, markerValueProperty, markerColorProperty
        )) {
            prop.addListener(requestRedrawListener)
        }
        extraMarkers.addListener(requestRedrawListener)

        this.side = side
    }

    fun isHorizontal() = side.isHorizontal()
    fun isVertical() = side.isHorizontal()

    // ==== Layout ====

    override fun nodePrefWidth() = surroundX() + font.height
    override fun nodePrefHeight() = surroundY() + font.height

    override fun draw() {
        super.draw()
        val markerValue = markerValue

        backend.clip(sceneX, sceneY, width, height) {
            val length = if (side.isHorizontal()) width else height

            val step = if (step > 0) {
                step
            } else {
                val pixelsPer100 = 100 * length / (upperBound - lowerBound)

                // step value...
                if (pixelsPer100 < 10) {
                    2000.0f
                } else if (pixelsPer100 < 12) {
                    1000.0f
                } else if (pixelsPer100 < 24) {
                    500.0f
                } else if (pixelsPer100 < 49) {
                    200.0f
                } else if (pixelsPer100 < 99) {
                    100.0f
                } else if (pixelsPer100 < 249) {
                    50.0f
                } else if (pixelsPer100 < 499) {
                    20.0f
                } else {
                    10.0f
                }
            }

            val scale = length / (upperBound - lowerBound)
            val thickness = if (reversed) -1f else 1f

            var major = lowerBound - (lowerBound % step)
            if (lowerBound < 0f) major -= step
            val majorPixels = step * scale * if (reversed) -1 else 1
            val minorPixels = majorPixels / subdivisions
            val lastMajor = upperBound - (lowerBound % step) + step

            val majorTick = when (side) {
                Side.TOP -> -padding.bottom
                Side.BOTTOM -> padding.top
                Side.LEFT -> -padding.right
                Side.RIGHT -> padding.left
            }
            val midTick = majorTick * 0.85f
            val minorTick = majorTick * 0.5f

            if (isHorizontal()) {
                var x = sceneX + (major - lowerBound) * scale + if (reversed) width else 0f
                val textY = if (side == Side.TOP) sceneY + surroundTop() else sceneY + height - surroundBottom()
                val textVAlignment = if (side == Side.TOP) TextVAlignment.TOP else TextVAlignment.BOTTOM
                val tickY = if (side == Side.TOP) sceneY + height else sceneY

                while (major <= lastMajor) {
                    // Major division
                    backend.fillRect(x, tickY, x + thickness, tickY + majorTick, textColor)

                    // Sub divisions
                    for (i in 1 until subdivisions) {
                        val height = if (i == subdivisions / 2) midTick else minorTick
                        backend.fillRect(
                            x + i * minorPixels, tickY,
                            x + i * minorPixels + thickness, tickY + height,
                            textColor
                        )

                    }

                    // Text
                    font.draw(format.format(major), textColor, x, textY, HAlignment.CENTER, textVAlignment, 0)

                    major += step
                    x += majorPixels
                }

                for (extraValue in extraMarkers) {
                    val markerX = sceneX - markerThickness / 2 +
                        (extraValue - lowerBound) / (upperBound - lowerBound) * width
                    backend.fillRect(markerX, sceneY, markerX + markerThickness, sceneY + height, extraMarkersColor)
                }
                if (markerValue != null) {
                    val markerX = sceneX - markerThickness / 2 +
                        (markerValue - lowerBound) / (upperBound - lowerBound) * width
                    backend.fillRect(markerX, sceneY, markerX + markerThickness, sceneY + height, markerColor)
                }

            } else {
                var y = sceneY + (major - lowerBound) * scale + if (reversed) height else 0f
                val textX = if (side == Side.LEFT) sceneX + surroundLeft() else sceneX + width - surroundRight()
                val textVAlignment = if ((side == Side.LEFT) xor reversed) TextVAlignment.BOTTOM else TextVAlignment.TOP
                val tickX = if (side == Side.LEFT) sceneX + width else sceneX

                while (major <= lastMajor) {
                    // Major division
                    backend.fillRect(tickX, y, tickX + majorTick, y + thickness, textColor)

                    // Sub divisions
                    for (i in 1 until subdivisions) {
                        val height = if (i == subdivisions / 2) midTick else minorTick
                        backend.fillRect(
                            tickX, y + i * minorPixels, tickX + height,
                            y + i * minorPixels + thickness,
                            textColor
                        )

                    }

                    // Text
                    font.draw(
                        format.format(major), textColor, 0f, 0f, HAlignment.CENTER, textVAlignment,
                        0, 0,
                        Matrix.translation(textX, y).rotate(if (reversed) - NINETY else NINETY)
                    )

                    major += step
                    y += majorPixels
                }

                for (extraValue in extraMarkers) {
                    val markerY = sceneY - markerThickness / 2 +
                        (extraValue - lowerBound) / (upperBound - lowerBound) * height
                    backend.fillRect(sceneX, markerY, sceneX + width, markerY + markerThickness, extraMarkersColor)
                }
                if (markerValue != null) {
                    val markerY = sceneY - markerThickness / 2 +
                        (markerValue - lowerBound) / (upperBound - lowerBound) * height
                    backend.fillRect(sceneX, markerY, sceneX + width, markerY + markerThickness, markerColor)
                }

            }
        }
    }

    companion object {
        private const val NINETY = PI / 2f
    }

}

package uk.co.nickthecoder.glok.theme.dsl

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle
import uk.co.nickthecoder.glok.theme.*
import uk.co.nickthecoder.glok.util.log

/**
 * Part of the DSL (Domain Specific Language) for building [Theme]s.
 *
 * A theme contains top-level rules in the form :
 *
 *     SELECTOR { ... }
 *
 * `...` is a block of code with [RuleDSL] as the receiver (i.e. `this is RuleDSL`)
 *
 * Within the block, you can define property values :
 *
 *     "PROPERTY_NAME" ( PROPERTY_VALUE )
 *
 * There are also convenience methods for common properties. e.g. :
 *
 *     textColor( color )
 *
 * Which is equivalent to :
 *
 *     "textColor" ( color )
 *
 * You can also define sub-rules :
 *
 *     child(SELECTOR) { ... }
 *     descendant(SELECTOR) { ... }
 *     SELECTOR { ... }
 *
 * The new rule's selector will be either a [ChildSelector], [DescendantSelector] or an [AndSelector].
 *
 * In the first two cases `SELECTOR` can be a `String` or a [Selector].
 * In the last case, it can also be an enum which implements [WithPseudoStyle]. e.g.
 *
 *     Orientation.HORIZONTAL { ... }
 *
 * In this example, the rule matches a node if the Node's `pseudoStyles` contains the String ":horizontal".
 * It is NOT checking the node's orientation property.
 */
class RuleDSL(val rule: Theme.Rule) {

    private fun addFloat(name: String, value: Number) = rule.settings.addFloat(name, value.toFloat())
    private fun addOptionalFloat(name: String, value: Number) = rule.settings.addOptionalFloat(name, value.toFloat())
    private fun addInt(name: String, value: Int) = rule.settings.addInt(name, value)
    private fun addBoolean(name: String, value: Boolean) = rule.settings.addBoolean(name, value)
    private fun addString(name: String, value: String) = rule.settings.addString(name, value)
    private fun addColor(name: String, value: Color) = rule.settings.addColor(name, value)
    private fun addEdges(name: String, value: Edges) = rule.settings.addEdges(name, value)
    private fun addPos(name: String, value: Alignment) = rule.settings.addPos(name, value)
    private fun addOrientation(name: String, value: Orientation) = rule.settings.addOrientation(name, value)
    private fun addImage(name: String, value: Image) = rule.settings.addImage(name, value)
    private fun addContentDisplay(name: String, value: ContentDisplay) = rule.settings.addContentDisplay(name, value)
    private fun addSpinnerArrowPositions(name: String, value: SpinnerArrowPositions) = rule.settings.addSpinnerArrowPositions(name, value)
    private fun addBackground(name: String, value: Background) = rule.settings.addBackground(name, value)
    private fun addBorder(name: String, value: Border) = rule.settings.addBorder(name, value)
    private fun addFont(name: String, value: Font) = rule.settings.addFont(name, value)

    //
    private fun addBorder(value: Border) = addBorder("border", value)
    private fun addBorderSize(value: Edges) = addEdges("borderSize", value)
    private fun addBorderSize(name: String, value: Edges) = addEdges(name, value)
    private fun addFont(value: Font) = addFont("font", value)
    private fun addSpinnerArrowPositions(value: SpinnerArrowPositions) = addSpinnerArrowPositions("spinnerArrowPositions", value)

    operator fun String.invoke(value: Float) = addFloat(this, value)
    operator fun String.invoke(value: Int) = addInt(this, value)
    operator fun String.invoke(value: Boolean) = addBoolean(this, value)
    operator fun String.invoke(value: String) = addString(this, value)
    operator fun String.invoke(value: Color) = addColor(this, value)
    operator fun String.invoke(value: Edges) = addEdges(this, value)
    operator fun String.invoke(value: Alignment) = addPos(this, value)
    operator fun String.invoke(value: Orientation) = addOrientation(this, value)
    operator fun String.invoke(value: Image) = addImage(this, value)
    operator fun String.invoke(value: ContentDisplay) = addContentDisplay(this, value)
    operator fun String.invoke(value: SpinnerArrowPositions) = addSpinnerArrowPositions(this, value)
    operator fun String.invoke(value: Background) = addBackground(this, value)
    operator fun String.invoke(value: Border) = addBorder(this, value)
    operator fun String.invoke(value: Font) = addFont(this, value)

    // Convenience functions for all properties used by Glok's controls.


    /**
     * For the 'root' selector.
     */
    fun sceneBackgroundColor(color: Color) = addColor("backgroundColor", color)
    fun sceneBackgroundColor(color: String) = addColor("backgroundColor", Color[color])


    fun minWidth(value: Number) = addOptionalFloat("overrideMinWidth", value)

    fun minHeight(value: Number) = addOptionalFloat("overrideMinHeight", value)

    fun prefHeight(value: Number) = addOptionalFloat("overridePrefHeight", value)

    fun prefWidth(value: Number) = addOptionalFloat("overridePrefWidth", value)

    fun maxWidth(value: Number) = addOptionalFloat("overrideMaxWidth", value)

    fun maxHeight(value: Number) = addOptionalFloat("overrideMaxHeight", value)

    fun spacing(value: Number) = addFloat("spacing", value)

    fun graphicTextGap(value: Number) = addFloat("graphicTextGap", value)
    fun textKeyCombinationGap(value: Number) = addFloat("textKeyCombinationGap", value)

    fun color(name: String, value: Color) = addColor(name, value)
    fun color(name: String, value: String) = addColor(name, Color[value])

    fun textColor(value: Color) = addColor("textColor", value)
    fun textColor(value: String) = textColor(Color[value])
    fun promptTextColor(value: Color) = addColor("promptTextColor", value)

    fun highlightColor(value: Color) = addColor("highlightColor", value)
    fun highlightColor(value: String) = highlightColor(Color[value])

    fun highlightTextColor(value: Color) = addColor("highlightTextColor", value)
    fun highlightTextColor(value: String) = highlightTextColor(Color[value])

    fun tint(value: Color) = addColor("tint", value)
    fun tint(value: String) = tint(Color[value])

    fun visible(value: Boolean) = addBoolean("visible", value)

    fun fill(value: Boolean = true) = addBoolean("fill", value)
    fun fillWidth(value: Boolean = true) = addBoolean("fillWidth", value)
    fun fillHeight(value: Boolean = true) = addBoolean("fillHeight", value)

    fun padding(value: Edges) = addEdges("padding", value)
    fun padding(size: Number) = padding(Edges(size.toFloat()))
    fun padding(y: Number, x: Number) = padding(Edges(y.toFloat(), x.toFloat()))
    fun padding(top: Number, right: Number, bottom: Number, left: Number) =
        padding(Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat()))

    fun labelPadding(value: Edges) = addEdges("labelPadding", value)
    fun labelPadding(size: Number) = labelPadding(Edges(size.toFloat()))
    fun labelPadding(y: Number, x: Number) = labelPadding(Edges(y.toFloat(), x.toFloat()))
    fun labelPadding(top: Number, right: Number, bottom: Number, left: Number) =
            labelPadding(Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat()))

    fun lineHeight(value: Number) = addFloat("lineHeight", value.toFloat())

    fun alignment(value: Alignment) = addPos("alignment", value)

    fun orientation(value: Orientation) = addOrientation("orientation", value)

    fun backgroundColor(color: Color) = addColor("backgroundColor", color)
    fun background(value: Background) = addBackground("background", value)

    fun noBackground() = background(NoBackground)

    fun plainBackground() = background(PlainBackground)

    fun underlineBackground(thickness: Float = 1f, ratio: Float = 0.9f) =
        background(UnderlineBackground(thickness, ratio))

    fun roundedBackground(radius: Number) = background(RoundedBackground(radius.toFloat()))
    fun roundedBackground(radius: Number, side: Side?) =
        background(RoundedBackground(radius.toFloat(), side))

    fun ninePatchBackground(ninePatch: NinePatch?) {
        if (ninePatch == null) {
            log.warn("Nine patch not found. Ignoring. Theme selector : ${rule.selector}")
        } else {
            background(NinePatchBackground(ninePatch))
        }
    }

    fun tintedNinePatchBackground(ninePatch: NinePatch?) {
        if (ninePatch == null) {
            log.warn("Nine patch not found. Ignoring. Theme selector : ${rule.selector}")
        } else {
            background(TintedNinePatchBackground(ninePatch))
        }
    }

    fun borderColor(color: Color) = addColor("borderColor", color)
    fun border(value: Border) = addBorder(value)

    fun noBorder() = border(NoBorder)

    fun plainBorder() = border(PlainBorder)

    fun raisedBorder(light: Color, dark: Color) = border(RaisedBorder(light, dark))

    fun roundedBorder(radius: Number) = border(RoundedBorder(radius.toFloat()))

    fun ninePatchBorder(ninePatch: NinePatch?) {
        if (ninePatch == null) {
            log.warn("Nine patch not found. Ignoring. Theme selector : ${rule.selector}")
        } else {
            border(NinePatchBorder(ninePatch))
        }
    }

    fun tintedNinePatchBorder(ninePatch: NinePatch?) {
        if (ninePatch == null) {
            log.warn("Nine patch not found. Ignoring. Theme selector : ${rule.selector}")
        } else {
            border(TintedNinePatchBorder(ninePatch))
        }
    }

    fun focusBorderColor(color: Color) = addColor("focusBorderColor", color)
    fun focusBorder(border: Border) = addBorder("focusBorder", border)

    fun focusHighlightColor(color : Color) = addColor("focusHighlightColor", color)
    fun focusHighlight(background: Background) = addBackground("focusHighlight", background)

    fun borderSize(size: Number) = addBorderSize(Edges(size.toFloat()))
    fun borderSize(topBottom: Number, leftRight: Number) =
        addBorderSize(Edges(topBottom.toFloat(), leftRight.toFloat()))

    fun borderSize(top: Number, right: Number, bottom: Number, left: Number) =
        addBorderSize(Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat()))

    fun focusBorderSize(borderSize: Edges) = addBorderSize("focusBorderSize", borderSize)
    fun focusBorderSize(size: Number) = addBorderSize("focusBorderSize", Edges(size.toFloat()))
    fun focusBorderSize(topBottom: Number, leftRight: Number) =
        addBorderSize("focusBorderSize", Edges(topBottom.toFloat(), leftRight.toFloat()))

    fun focusBorderSize(top: Number, right: Number, bottom: Number, left: Number) =
        addBorderSize("focusBorderSize", Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat()))

    fun font(value: Font) = addFont(value)
    fun font(fontSize: Float, vararg families: String) =
        addFont(Font.create(fontSize, FontStyle.PLAIN, *families))

    fun font(fontSize: Float, style: FontStyle, vararg families: String) =
        addFont(Font.create(fontSize, style, *families))

    fun image(name: String, value: Image) = addImage(name, value)
    fun image(name: String, images: NamedImages, imageName: String) {
        val image = images[imageName]
        if (image == null) {
            log.warn("Image $imageName not found. Ignoring. Theme Selector : ${rule.selector}")
        } else {
            image(name, image)
        }
    }

    fun caretImage(value: Image) = image("caretImage", value)
    fun caretImage(images: NamedImages, imageName: String) = image("caretImage", images, imageName)
    fun caretColor(color: Color) = addColor("caretColor", color)

    fun image(image: Image) = addImage("image", image)
    fun image(images: NamedImages, imageName: String) = images[imageName]?.let { image(it) }

    fun graphic(image: Image) = addImage("graphic", image)
    fun graphic(images: NamedImages, imageName: String) = images[imageName]?.let { graphic(it) }

    fun collapsable(value: Boolean) = addBoolean("collapsable", value)

    fun expanded(value: Boolean) = addBoolean("expanded", value)

    fun focusTraversable(value: Boolean) = addBoolean("focusTraversable", value)

    fun focusAcceptable(value: Boolean) = addBoolean("focusAcceptable", value)

    /**
     * Used by [SpinnerBase]
     */
    fun spinnerArrows(value: SpinnerArrowPositions) = addSpinnerArrowPositions(value)

    /**
     * Used by [Labelled]
     */
    fun contentDisplay(value: ContentDisplay) = addContentDisplay("contentDisplay", value)

    /**
     * Used by [ButtonBar]
     */
    fun buttonOrder(value: String) = addString("buttonOrder", value)

    /**
     * Example
     *
     *     "tool_bar" {
     *         ...
     *         child("button") { ... }
     *     }
     *
     */
    fun child(child: String, block: RuleDSL.() -> Unit) = child(StyleSelector(child), block)
    fun child(child: Selector, block: RuleDSL.() -> Unit) {
        val newSelector = ChildSelector(rule.selector, child)
        val newRule = rule.createRule(newSelector)
        RuleDSL(newRule).block()
    }

    /**
     * Example
     *
     *     "tool_bar" {
     *         ...
     *         child("button") { ... }
     *     }
     *
     */
    fun descendant(child: String, block: RuleDSL.() -> Unit) = descendant(child.toSelector(), block)
    fun descendant(child: Selector, block: RuleDSL.() -> Unit) {
        val newSelector = DescendantSelector(rule.selector, child)
        val newRule = rule.createRule(newSelector)
        RuleDSL(newRule).block()
    }

    /**
     * Example
     *
     *     "button" {
     *         ...
     *         "button" { ... }
     *     }
     *
     */
    operator fun Selector.invoke(block: RuleDSL.() -> Unit) {
        val newSelector = AndSelector(rule.selector, this)
        val newRule = rule.createRule(newSelector)
        RuleDSL(newRule).block()
    }

    operator fun String.invoke(block: RuleDSL.() -> Unit) =
            this.toSelector().invoke(block)

    operator fun WithPseudoStyle.invoke(block: RuleDSL.() -> Unit) =
            PseudoStyleSelector(this.pseudoStyle).invoke(block)
}

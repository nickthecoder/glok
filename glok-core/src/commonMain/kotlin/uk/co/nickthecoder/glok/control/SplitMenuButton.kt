/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.combineActionEventHandlers
import uk.co.nickthecoder.glok.event.tryCatchHandle
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.PlainBorder
import uk.co.nickthecoder.glok.scene.WithItems
import uk.co.nickthecoder.glok.theme.styles.ARROW_BUTTON
import uk.co.nickthecoder.glok.theme.styles.SPLIT_MENU_BUTTON
import uk.co.nickthecoder.glok.util.currentTimeMillis
import uk.co.nickthecoder.glok.util.max

/**
 * A Button split into two parts.
 *
 * The main part acts like a regular [Button], calling [onAction] when pressed.
 *
 * The other part is the [graphic] (from [Labelled]). It is a [Button], which acts like a [MenuButton].
 * i.e. when clicked a [PopupMenu] displays the menu [items].
 *
 * Tantalum renders it like this :
 *
 *       ┌───────────────────────────────┬─────────┐
 *       │                               │  ┌───┐  │
 *       │  text                         │  │ V │  │
 *       │                               │  └───┘  │
 *       └───────────────────────────────┴─────────┘
 *                                      graphic = Button
 *                                      style: .arrow_button
 *
 * The graphic is on the right, with a thin line on its left
 * using a [PlainBorder] with size (0,0,0,1) (left edge only).
 *
 * ### Theme DSL
 *
 *     "split_menu_button" {
 *         child( ".arrow_button" ) { // A Button
 *
 *             child(".graphic") {
 *                 image( image : Image )
 *
 *                 tint( tint : Color )
 *             }
 *         }
 *     }
 *
 * SplitMenuButton inherits all features of [ButtonBase].
 *
 */
class SplitMenuButton(
    text: String,
    graphic: Node? = null
) : ButtonBase(text, graphic), WithItems {

    // region ==== Properties ====

    /**
     * An event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    val onShowingProperty by optionalActionEventHandlerProperty(null)
    var onShowing by onShowingProperty

    private val mutableShowingProperty by booleanProperty(false)

    /**
     * Set to true, after the button is clicked, and before the [PopupMenu] is displayed.
     * It is reset to false when the [PopupMenu] is closed.
     *
     * Note, it is often easier to use [onShowing] rather than listening to this property.
     *
     * Applications may listen to this, and adjust [items] as required.
     *
     * History. [onShowing] was added later than [showingProperty], but we keep this for backwards compatibility.
     * Also, [showingProperty] could be used to listen for when the menu is closed.
     */
    val showingProperty = mutableShowingProperty.asReadOnly()
    var showing by mutableShowingProperty
        private set

    // endregion properties

    // region ==== Fields ====

    override val items = mutableListOf<Node>().asMutableObservableList()

    private val arrowButton = Button("Arrow", ImageView(null)).apply {
        contentDisplay = ContentDisplay.GRAPHIC_ONLY
        onAction { showMenu() }
    }

    private var lastClosed = 0.0

    // endregion

    init {
        styles.add(SPLIT_MENU_BUTTON)
        arrowButton.styles.add(ARROW_BUTTON)
        mutableChildren.add(arrowButton)
    }

    /**
     * Add an event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    fun onShowing(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onShowingProperty, handlerCombination, block)
    }

    private fun showMenu() {
        // If we click this menu, then the `MousePressed` event closed the menu, and we don't want to
        // open it again.
        if (lastClosed + GlokSettings.clickTimeThreshold > currentTimeMillis()) return

        onShowing?.tryCatchHandle(ActionEvent())
        showing = true
        val menu = PopupMenu()
        menu.items.addAll(items)
        menu.show(this).apply {
            onClosed {
                menu.items.clear()
                showing = false
                lastClosed = currentTimeMillis()
            }
        }
    }

    // region ==== Layout ====

    override fun nodePrefWidth() = super.nodePrefWidth() + arrowButton.evalPrefWidth()
    override fun nodePrefHeight() = max(super.nodePrefHeight(), surroundY() + arrowButton.evalPrefHeight())
    override fun layoutChildren() {
        val arrowWidth = arrowButton.evalPrefWidth()
        val arrowHeight = height - surroundY() //arrowButton.prefHeight()
        layoutLabelled(width - arrowWidth, height)

        setChildBounds(
            arrowButton,
            width - surroundRight() - arrowWidth,
            surroundTop() + (height - surroundY() - arrowHeight) / 2,
            arrowWidth,
            arrowHeight
        )
    }

    override fun drawChildren() {
        super.drawChildren()
        arrowButton.drawAll()
    }

    // endregion layout

}

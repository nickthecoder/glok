package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.theme.styles.TOOL_BAR

/**
 * A horizontal or vertical bar, most often used to hold [Button]s.
 * `Status Bars` (at the bottom of many application windows) also use `ToolBar`.
 *
 * If there are too many items to fit in the available space, a pull-down button appears,
 * giving access to the remaining items.
 * At time of writing, this button hasn't been implemented.
 *
 * ### Theme DSL
 *
 *     "tool_bar" {
 *
 *         orientation( Orientation.HORIZONTAL )
 *         orientation( Orientation.VERTICAL )
 *
 *         child(".overflow_button") {
 *             graphic( value : Image )
 *             graphic( images : NamedImages, imageName : String )
 *         }
 *
 *     }
 *
 * ToolBar inherits all features of [Region].
 *
 */
class ToolBar(side: Side = Side.TOP) : ToolBarBase(side) {

    init {
        styles.add(TOOL_BAR)
        section = true
    }
}

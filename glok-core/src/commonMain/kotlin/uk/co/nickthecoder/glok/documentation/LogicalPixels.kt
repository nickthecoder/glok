package uk.co.nickthecoder.glok.documentation

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.RegularStage

/**
 * This interface is _NOT USED_, it is only here for [Documentation].
 *
 * ## The Problem
 *
 * With the advent of high resolution screens (several thousand pixels wide),
 * it is useless to measure things in _physical_ pixels.
 * For example, suppose we want a thin rectangular border.
 * If we make it 1 _physical_ pixel wide, then on a very high resolution device, it would be virtually invisible.
 *
 * ## The Solution
 *
 * All coordinates and sizes are measured in _logical_ pixels, so our thin rectangular border
 * would be 1 _logical pixel_ wide.
 * On a high resolution screen, this may be 2 or more _physical_ pixels, but on a regular (low res)
 * screen it will be 1 _physical_ pixel.
 *
 * Your application can measure everything in _logical pixels_, and will appear a _sensible_ size,
 * regardless of display device.
 *
 * ## The Scaling Factor
 *
 * [GlokSettings.globalScale] holds the scaling factor. On my laptop, this is 2.0,
 * so 1 _logical_ pixel is 2x2 _physical_ pixels.
 *
 * This scaling factor is set when glok is started ([launch]).
 * It looks for environment variables used by other GUI toolkits :
 *
 * * GDK_SCALE
 * * QT_SCALE_FACTOR
 *
 * If neither are found, and the screen is more than 2,000 pixels wide, glok uses a heuristic
 * to guess a suitable scaling factor.
 *
 * ## Making Full Use of the Screen's Resolution
 *
 * A naive solution would stop here, scaling absolutely everything, but that wouldn't make full
 * use of the screen's resolution.
 * For example, if we use an icon which is 16x16 pixels wide, it would appear a _sensible_ size on
 * all devices, but would look equally blocky / pixelated too.
 *
 * A better solution would use a 16x16 icon on low-res screen, and a larger icon on high-res screen,
 * but still measure it in _logical_ pixels.
 *
 * Glok currently uses simple textures (bitmaps) for rendering fonts, but they are aware of
 * [GlokSettings.globalScale]. So on a high-res screen the font will look crisp and beautiful.
 *
 * ## Adjusting the Scaling Factor
 *
 * Glok uses a scaling factor to make your application appear the same size on all screens,
 * but it can also be used in other ways.
 *
 * We can temporarily increase [GlokSettings.globalScale], to make everything unnaturally BIG.
 * There's numerous reasons for doing this :
 *
 * * For the visually impaired
 * * Giving a presentation
 * * Too early in the morning / too hung-over to focus ;-))
 *
 * There's also good reasons to reduce the scaling too.
 * Suppose I use a complicated CAD program. I may want all the controls to be small, giving maximum space
 * to the drawing.
 * But when I flip to my web browser to relax and catch up on the news, I don't need everything to be small.
 * So my system setting might be `GDK_SCALE=2`, but my CAD program overrides it to `1.5` or less.
 *
 * So consider adding [GlokSettings.globalScale] to your application's settings.
 * Even if it is ignored by 99% of your users, the 1% will love you for it.
 *
 * ## Per-Stage Scaling
 *
 * Each stage has its own scaling factor : [RegularStage.scale].
 * By default, this is bound to [GlokSettings.globalScale].
 * So if you want one Stage to be scaled differently from the rest, unbind it, and then set it.
 *
 * There are downsides to this though. Suppose you have a high-res device, and for some weird reason
 * [GlokSettings.globalScale] == 1 (making everything too small to read).
 * If you now set a single Stage to use a scaling factor of 2, it will be readable, but the fonts won't
 * look crisp and beautiful. They are only being scale up.
 *
 * This can be considered a bug (Glok only considers the globalScale, not the per-stage scale when choosing
 * which font to use). But it's a bug that's unlikely to be fixed any time soon. Sorry.
 *
 */
interface LogicalPixels : Documentation

package uk.co.nickthecoder.glok.control

/**
 * Defines the position of the graphic in a [Labelled].
 */
enum class ContentDisplay {

    /**
     * The graphic (if it exists) is placed to the left of the text.
     *
     * Glok's default theme sets this for buttons _except_ those in a ToolBar
     * (where [GRAPHIC_ONLY] is used instead).
     * As with all Theme settings, you may override the theme's value on a per-node basis.
     */
    LEFT,

    /**
     * The graphic (if it exists) is placed to the right of the text.
     */
    RIGHT,

    /**
     * The graphic (if it exists) is placed above the text.
     */
    TOP,

    /**
     * The graphic (if it exists) is placed below the text.
     */
    BOTTOM,

    /**
     * If the graphic exists, then the text is not rendered.
     * If the graphic does not exist, then the text _is_ rendered.
     *
     * Glok's default theme sets this for buttons in a ToolBar.
     * As with all Theme settings, you may override the theme's value on a per-node basis.
     */
    GRAPHIC_ONLY,

    /**
     * The graphic is ignored, and only the text is rendered.
     * If the text is also blank, the button will be blank.
     */
    TEXT_ONLY;

    /**
     * True for [LEFT] and [RIGHT]
     */
    fun isHorizontal() = this == LEFT || this == RIGHT

    /**
     * True for [TOP] and [BOTTOM]
     */
    fun isVertical() = this == TOP || this == BOTTOM

}

package uk.co.nickthecoder.glok.util

interface Log {
    var level : Int
    fun add(level: Int, message: String)

    fun trace(message: String) = add(TRACE, "TRACE $message")
    fun debug(message: String) = add(DEBUG, "DEBUG $message")
    fun info(message: String) = add(INFO, "INFO $message")
    fun warn(message: String) = add(WARN, "WARN $message")
    fun error(message: String) = add(ERROR, "** ERROR ** $message")
    fun severe(e: Exception) {
        add(SEVERE, "**** SEVERE $e")
        // Stack trace in the log too?
        e.printStackTrace()
    }

    fun severe(message: String) {
        add(SEVERE, "**** SEVERE **** $message")
    }

    companion object {
        const val TRACE = 0
        const val DEBUG = 1
        const val INFO = 2
        const val WARN = 3
        const val ERROR = 4
        const val SEVERE = 5
    }
}

class IgnoreLog : Log {
    override var level = Log.SEVERE
    override fun add(level: Int, message: String) {}
}

fun <T : Any?> withLogLevel(level: Int, block: () -> T): T {
    val oldLevel = log.level
    log.level = level
    val result: T = block()
    log.level = oldLevel
    return result
}

var log: Log = ConsoleLog()

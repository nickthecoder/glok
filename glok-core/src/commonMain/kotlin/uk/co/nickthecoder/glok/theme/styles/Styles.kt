package uk.co.nickthecoder.glok.theme.styles

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.withExpandHeight
import uk.co.nickthecoder.glok.scene.dsl.withExpandWidth
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * Used by [Region]
 */
const val REGION = "region"

/**
 * Used by [ToolBar]
 */
const val TOOL_BAR = "tool_bar"

/**
 * Used by [MenuBar]
 */
const val MENU_BAR = "menu_bar"

/**
 * Used by [ToolBar]'s overflow popup
 */
const val TOOL_BAR_OVERFLOW = "tool_bar_overflow"

/**
 * Used by [ButtonBar]
 */
const val BUTTON_BAR = "button_bar"

/**
 * Used by [ButtonBar]
 */
const val BUTTON_BAR_SECTION = ".section"

/**
 * Used by [ImageView]
 */
const val IMAGE_VIEW = "image_view"

/**
 * Used by [Button]
 */
const val BUTTON = "button"

/**
 * Used by [Menu]
 */
const val MENU = "menu"

/**
 * Used by [MenuButton]
 */
const val MENU_BUTTON = "menu_button"

/**
 * Used by [SplitMenuButton]
 */
const val SPLIT_MENU_BUTTON = "split_menu_button"

/**
 * Used by [MenuItem]
 */
const val MENU_ITEM = "menu_item"

/**
 * Used by [CustomMenuItem]
 */
const val CUSTOM_MENU_ITEM = "custom_menu_item"

/**
 * Used by [ToggleMenuItem]
 */
const val CHECK_MENU_ITEM = "check_menu_item"

/**
 * Used by [SubMenu]
 */
const val SUB_MENU = "sub_menu"

/**
 * Used by [PopupMenu]
 */
const val POPUP_MENU = "popup_menu"

/**
 * Used by [ColorButton]
 */
const val COLOR_BUTTON = "color_button"

/**
 * Used by [ToggleButton]
 */
const val TOGGLE_BUTTON = "toggle_button"

/**
 * Used by [RadioButton]
 */
const val RADIO_BUTTON = "radio_button"

/**
 * An alternative style for a [RadioButton].
 * Tantalum renders these buttons similar to a [ToggleButton].
 */
const val RADIO_BUTTON2 = "radio_button2"

/**
 * Used by [ListView]
 */
const val LIST_VIEW = "list_view"

/**
 * Used by [TreeView]
 */
const val TREE_VIEW = "tree_view"

/**
 * Used by [ChoiceBox]
 */
const val CHOICE_BOX = "choice_box"

/**
 * Used by [ListCell]
 */
const val LIST_CELL = "list_cell"

/**
 * Used by [TreeCell]
 */
const val TREE_CELL = "tree_cell"

/**
 * Used by [TreeCell]
 */
const val TREE_ROW = "tree_row"

/**
 * See [SingleContainer].
 * Not to be confused with [CONTAINER]
 */
const val SINGLE_CONTAINER = "single_container"

/**
 * Used by [TabPane]
 */
const val TAB_PANE = "tab_pane"

/**
 * Al alternative style in [Tantalum] for a [TabPane]
 */
const val DOCUMENT_TAB_PANE = "document_tab_pane"

/**
 * Used by [TabBar]
 * NOTE. There is an alternate style in the [Tantalum] : [DOCUMENTS_TAB_BAR].
 * The default style for this [TAB_BAR] is large, not designed to have close buttons,
 * and is better suited to "sections", rather than "documents".
 */
const val TAB_BAR = "tab_bar"

/**
 * An alternate name for [TabBar], which is styled differently to [TAB_BAR].
 * This TabBar is more compact, and suitable for tabs containing "documents",
 * whereas [TAB_BAR] is more suitable for "sections", e.g. grouping preferences.
 */
const val DOCUMENTS_TAB_BAR = "documents_tab_bar"

/**
 * Used by [Tab]
 */
const val TAB = "tab"

/**
 * Used by [Label]
 */
const val LABEL = "label"

/**
 * Used for [Label]s which give secondary information, such as units to the right of a [TextField].
 */
const val INFORMATION = ".information"

/**
 * Used for [Label]s which appear to the right of text fields / spinners
 * containing the units, such as "pt", "pixel", "mm" "seconds" etc.
 */
const val UNITS = ".units"

/**
 * Used for [Label]s.
 */
const val HEADING = ".heading"

/**
 * Used for [Label]s.
 */
const val ROW_HEADING = ".row_heading"

/**
 * Used for error message [Label]s which indicate that a related node is invalid / not filled in.
 */
const val ERROR = ".error"

/**
 * Used by [Text]
 */
const val TEXT = "text"

/**
 * An extra selector on [Labelled] items to indicate they should use a fixed-width font.
 */
const val FIXED_WIDTH = "fixed_width"

/**
 * Used by [TextField]
 */
const val TEXT_FIELD = "text_field"

/**
 * Make a [TextField] appear similar to a simple label.
 */
const val LIKE_LABEL = ".like_label"

/**
 * Make a [Button] or a [ToggleButton] appear similar to a HTML link.
 */
const val LIKE_LINK = ".like_link"

/**
 * Changes background colors from
 */
const val ALT_BACKGROUND = ".alt_background"

/**
 * Used by [TextArea]
 */
const val TEXT_AREA = "text_area"

/**
 * Used by [StyledTextArea]
 */
const val STYLED_TEXT_AREA = "styled_text_area"

/**
 * Used by [TextTooltip], and possibly other [Tooltip] implementations
 */
const val TOOLTIP = "tooltip"

/**
 * Used by [CheckBox]
 */
const val CHECK_BOX = "check_box"

/**
 * Used by [CheckBox]
 */
const val SWITCH = "switch"

/**
 * Used by [FormGrid]
*/
const val FORM_GRID = "form_grid"

/**
 * Used by [FormRow]
*/
const val FORM_ROW = "form_row"

/**
 * Used by [TitledPane]
 */
const val TITLED_PANE = "titled_pane"

/**
 * Used by [SplitPane]
 */
const val SPLIT_PANE = "split_pane"

/**
 * Used by [Separator]
 */
const val SEPARATOR = "separator"

/**
 * Used by [ScrollBar]
 */
const val SCROLL_BAR = "scroll_bar"

/**
 * Used by [ScrollPane]
 */
const val SCROLL_PANE = "scroll_pane"

/**
 * Used by [ScrollPaneWithButtons]
 */
const val SCROLL_PANE_WITH_BUTTONS = "scroll_pane_with_buttons"

/**
 * Used by `FolderBar` (A JVM-only control).
 */
const val FOLDER_BAR = "folder_bar"

/**
 * Used by [ThreeRow]
 */
const val THREE_ROW = "three_row"

/**
 * Used by [Ruler]
 */
const val RULER = "ruler"

/**
 * Used by [WindowDecoration]
 */
const val WINDOW_DECORATION = "window_decoration"

val a: ProgressBar? = null

/**
 * Used by [ProgressBar].
 */
const val PROGRESS_BAR = "progress_bar"

/**
 * Used by the inner part of a [ProgressBar].
 */
const val PROGRESS = "progress"

/**
 * Used by [SpinnerBase]
 */
const val SPINNER = "spinner"

/**
 * Used by [DoubleSlider], [FloatSlider]
 */
const val SLIDER = "slider"

/**
 * Used by [Slider2d]
 */
const val SLIDER_2D = "slider_2d"

/**
 * Used by [ColorSlider]
 */
const val COLOR_SLIDER = "color_slider"

/**
 * Used by [IntSpinner]
 */
const val INT_SPINNER = "int_spinner"

/**
 * Used by [FloatSpinner]
 */
const val FLOAT_SPINNER = "float_spinner"

/**
 * Used by [DoubleSpinner]
 */
const val DOUBLE_SPINNER = "double_spinner"

// ==== Child nodes of controls ====

/**
 * Used by a child of [ToolBar]
 */
const val OVERFLOW_BUTTON = ".overflow_button"

/**
 * Used by a child of [Tab]
 */
const val TAB_LABEL = ".label"

/**
 * Used by a child of [Tab]
 */
const val CLOSE_BUTTON = ".close"

/**
 * Used by a child of [ToolBar] and [TabPane].
 * These are specialist containers not available in the public API.
 *
 * Not to be confused with [SINGLE_CONTAINER], which is a reusable component, which is in the public API.
 */
const val CONTAINER = ".container"

/**
 * Used by [TextAreaBase]
 */
const val SCROLLED = ".scrolled"

/**
 * Used by [TextAreaBase]
 */
const val LINE_NUMBER_GUTTER = ".line_number_gutter"

/**
 * Used by [TabBar] (and therefore also by [TabPane])
 * This is a [Box] containing [Tab]s.
 */
const val TABS_CONTAINER = ".tabs_container"

/**
 * Used by a child of [Labelled]
 */
const val GRAPHIC = ".graphic"

/**
 * Used by a child of [ScrollPane]
 */
const val CORNER = ".corner"

/**
 * Used by a child of [ScrollPane]
 */
const val VIEWPORT = ".viewport"

/**
 * Used by a child of [TitledPane]
 */
const val TITLE = ".title"

/**
 * Used by a child of [ButtonBase] to tint with the fontColor.
 */
const val TINTED = ".tinted"

/**
 * Used by a child of [SplitPane]
 */
const val DIVIDER = ".divider"

/**
 * Used by TextField.withFileButton / withFolderButton / withFolderAndFileButtons
 * Applies to a `HBox`. Themes may choose the spacing, and alignment.
 */
const val FIELD_WITH_BUTTONS = ".field_with_buttons"

/**
 * Used by [withExpandWidth] and [withExpandHeight]
 */
const val EXPAND_BAR = ".expand_width"

/**
 * Used by a child of [Separator]
 */
const val LINE = ".line"

/**
 * Used by a child of [ScrollBar] and [DoubleSlider]
 */
const val THUMB = ".thumb"

/**
 * Used by a child of [DoubleSlider]
 */
const val TRACK = ".track"

/**
 * Used by Spinners
 */
const val EDITOR = ".spin_field"

/**
 * Used by Spinners
 */
const val SPIN_UP = ".spin_up"

/**
 * Used by Spinners
 */
const val SPIN_DOWN = ".spin_down"

/**
 * Used by [WindowDecoration]'s title bar
 */
const val TITLE_BAR = ".title_bar"

/**
 * Used by [SubMenu], [MenuButton], [TreeView]'s `TreeRow`.
 */
const val ARROW = ".arrow"

/**
 * Used by [SplitMenuButton]
 */
const val ARROW_BUTTON = ".arrow_button"

// ==== Pseudo Styles ====

/**
 * A pseudo style used by [Button], [ToggleButton], [RadioButton] and other subclasses of [ButtonBase].
 */
const val SELECTED = ":selected"

/**
 * A pseudo style used by [CheckBox].
 */
const val INDETERMINATE = ":indeterminate" // For the 3rd state of CheckBox

/**
 * A pseudo style used by [ToolBar].
 */
const val HORIZONTAL = ":horizontal"

/**
 * A pseudo style used by [ToolBar].
 */
const val VERTICAL = ":vertical"

/**
 * A pseudo style used by [Ruler].
 */
const val TOP = ":top"

/**
 * A pseudo style used by [Ruler].
 */
const val BOTTOM = ":bottom"

/**
 * A pseudo style used by [Ruler].
 */
const val RIGHT = ":right"

/**
 * A pseudo style used by [Ruler].
 */
const val LEFT = ":left"

/**
 * A pseudo style used by [TitledPane].
 */
const val COLLAPSABLE = ":collapsable"

/**
 * A pseudo style used by [TitledPane].
 */
const val EXPANDED = ":expanded"

/**
 * A pseudo style used by [TitledPane].
 */
const val CONTRACTED = ":contracted"

/**
 * A pseudo style used by [TreeView]'s `TreeRow`.
 */
const val LEAF = ":leaf"

/**
 * A pseudo style used by [Node]
 */
const val FOCUSED = ":focused"

/**
 * A pseudo style used by [WindowDecoration]
 */
const val STAGE_FOCUSED = ":stage_focused"

/**
 * A pseudo style used by [WindowDecoration]
 */
const val STAGE_RESIZABLE = ":stage_resizable"

/**
 * A pseudo style used by all [Node]s.
 */
const val DISABLED = ":disabled"

/**
 * A pseudo style used by [Button] when defaultButton = true.
 */
const val DEFAULT_BUTTON = ":default_button"

/**
 * Used by [ButtonBase]. [Tantalum] uses this pseudo style for buttons in [ToolBar]s
 * to determine if the background should be omitted.
 */
const val HAS_GRAPHIC = ":has_graphic"

/**
 * A pseudo style used by [TextInputControl]s.
 */
const val READ_ONLY = ":read_only"

/**
 * A pseudo style used by [Button], [ToggleButton], [RadioButton] and other subclasses of [ButtonBase].
 */
const val ARMED = ":armed"

/**
 * A pseudo style used by [Button], [ToggleButton], [RadioButton] and other subclasses of [ButtonBase].
 */
const val HOVER = ":hover"

/**
 * A pseudo style used by [SpinnerBase]
 */
const val INVALID = ":invalid"

/**
 * A pseudo style used by [ListCell]
 */
const val EVEN = ":even"

/**
 * A pseudo style used by [ListCell]
 */
const val ODD = ":odd"

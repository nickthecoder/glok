package uk.co.nickthecoder.glok.text

enum class FontStyle(val awtValue: Int) {

    PLAIN(0),
    BOLD(1),
    ITALIC(2),
    BOLD_ITALIC(3)

}

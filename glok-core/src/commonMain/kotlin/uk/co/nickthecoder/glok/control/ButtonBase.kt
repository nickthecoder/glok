package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.control.ButtonBase.ButtonActions.ACTIVATE
import uk.co.nickthecoder.glok.control.ButtonBase.ButtonActions.CANCEL
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.theme.styles.ARMED
import uk.co.nickthecoder.glok.theme.styles.DISABLED
import uk.co.nickthecoder.glok.theme.styles.HAS_GRAPHIC
import uk.co.nickthecoder.glok.theme.styles.HOVER

/**
 * The base class for [Button], [ToggleButton], [RadioButton] ...
 *
 * ### Theme DSL
 *     ( "button" or "toggle_button" or "radio_button" or "check_box" ) {
 *
 *         ":armed" { ... }
 *
 *         ":focus" { ... }
 *
 *         ":hover" { ... }
 *     }
 *
 * ButtonBase inherits all the features of [Labelled].
 *
 * Note, Glok's default theme also uses the style `.tinted` to indicate that the button's [graphic]
 * should be tinted with the theme's font color.
 * When the button is [disabled], the tinted color is semi-transparent, to make it look disabled.
 * See [ImageView.tint].
 *
 * ## Class Diagram
 *
 *                                                 ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                                                 ┆ Labelled    ┆
 *                                                 ┆   text      ┆
 *                                                 ┆   graphic   ┆
 *                                                 ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                                                        △
 *                                                 ╭╌╌╌╌╌╌┴╌╌╌╌╌╌╮                                                ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *                                                 ┆ ButtonBase  ┆                      ┏━━━━━━━━━━━━━━━━┓        ┆ /Toggle/     ┆
 *                                                 ┆   disabled  ┆                      ┃ToggleGroup     ┃        ┆   selected   ┆
 *                                                 ┆   armed     ┆                      ┃  selectedToggle┃◇───────┤   toggleGroup┆
 *                                                 ┆   onAction  ┆                      ┃  userData()    ┃        ┆   userData   ┆
 *                                                 ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╯                      ┗━━━━━━━━━━━━━━━━┛        ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                                                        △                                                              △
 *               ┌─────────────────┬─────────────────┬────┴───────────────┬──────────────────┬───────────────┐           │
 *     ┏━━━━━━━━━┷━━━━━━━┓   ┏━━━━━┷━━━━━┓   ┏━━━━━━━┷━━━━━━━┓   ┏━━━━━━━━┷━━━━━━━━┓    ┏━━━━┷━━━━┓  ╭╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╮  │
 *     ┃Button           ┃   ┃MenuButton ┃   ┃SplitMenuButton┃   ┃ColorButton      ┃    ┃Menu     ┃  ┆SelectButtonBase┆  │
 *     ┃   defaultButton ┃   ┃  items    ┃   ┃  items        ┃   ┃  color          ┃    ┃  items  ┃  ┆  selected      ┆  │
 *     ┃   cancelButton  ┃   ┃           ┃   ┃               ┃   ┃   ...           ┃    ┃         ┃  ┆                ┆  │
 *     ┗━━━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━━━┛    ┗━━━━━━━━━┛  ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯  │
 *                                                                                  (within a MenuBar)   △               │
 *                                                                                                       │               │
 *                             ┌───────────────────────┬───────────────────────────┬─────────────────────┴──┐  ┌─────────┴───────┐
 *                   ┏━━━━━━━━━┷━━━━━━━━━┓   ┏━━━━━━━━━┷━━━━━━━━━┓   ┏━━━━━━━━━━━━━┷━━━━━━┓       ╭╌╌╌╌╌╌╌╌╌┴╌╌┴╌╌╌╮             ╵
 *                   ┃Checkbox           ┃   ┃PropertyRadioButton┃   ┃PropertyToggleButton┃       ┆ToggleButtonBase┆    ToggleMenuItemBase
 *                   ┃ intermediate      ┃   ┃  property         ┃   ┃  property          ┃       ┆                ┆
 *                   ┃ allowIndeterminate┃   ┃  value            ┃   ┃  value             ┃       ┆                ┆
 *                   ┗━━━━━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━━━━━━━┛       ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *                 (No menu-item equivalent)                                                              △
 *                                                                                                        │
 *                                                                                                ┌───────┴────────┐
 *                                                                                        ┏━━━━━━━┷━━━━┓   ┏━━━━━━━┷━━━┓
 *                                                                                        ┃ToggleButton┃   ┃RadioButton┃
 *                                                                                        ┃            ┃   ┃           ┃
 *                                                                                        ┗━━━━━━━━━━━━┛   ┗━━━━━━━━━━━┛
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart).
 */
abstract class ButtonBase(

    text: String,
    graphic: Node? = null

) : Labelled(text, graphic), Actionable, HasDisabled {

    // region ==== Properties ====
    /**
     * An event handler for when this button is pressed.
     */
    val onActionProperty by optionalActionEventHandlerProperty(null)
    var onAction by onActionProperty

    final override val disabledProperty by booleanProperty(false)
    final override var disabled by disabledProperty

    val armedProperty by booleanProperty(false)
    var armed by armedProperty

    // endregion properties

    protected var isMouseDown = false

    init {
        onMousePressed { mousePressed(it) }
        onMouseReleased { mouseReleased(it) }
        onMouseEntered { mouseEntered(it) }
        onMouseExited { mouseExited(it) }
        onKeyPressed { keyPressed(it) }
        onKeyReleased { keyReleased(it) }

        disabledProperty.addChangeListener { _, _, isDisabled -> pseudoStyleIf(isDisabled, DISABLED) }
        armedProperty.addChangeListener { _, _, isArmed -> pseudoStyleIf(isArmed, ARMED) }

        // See graphicOnlyProperty
        pseudoStyleIf(graphic != null, HAS_GRAPHIC)
        graphicProperty.addListener { pseudoStyleIf(this.graphic != null, HAS_GRAPHIC) }
    }

    /**
     * Usage :
     *
     *     myButton.onAction { ... }
     *
     * Equivalent to the overly verbose JavaFX style :
     *
     *     myButton.onAction = EventHandler(EventType.ActionEvent) { ... }
     *
     */
    fun onAction(handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (event: ActionEvent) -> Unit) {
        combineActionEventHandlers(onActionProperty, handlerCombination, block)
    }

    override fun performAction() {
        onAction?.tryCatchHandle(ActionEvent())
    }

    // ==== Events ====

    private fun mousePressed(event: MouseEvent) {
        if (disabled) return
        if (event.isPrimary) {
            armed = true
            isMouseDown = true

            firstToRoot { it.focusAcceptable }?.requestFocus()
            event.capture()
            event.consume()
        }
    }

    private fun mouseReleased(event: MouseEvent) {
        if (disabled) return
        if (event.isPrimary) {
            isMouseDown = false
            if (armed) {
                armed = false
                performAction()
                event.consume()
            }
        }
    }

    protected open fun mouseEntered(event: MouseEvent) {
        if (disabled) return
        pseudoStyles.add(HOVER)
        if (isMouseDown) {
            armed = true
        }
        event.consume()
        requestRedraw()
    }

    protected open fun mouseExited(event: MouseEvent) {
        armed = false
        pseudoStyles.remove(HOVER)
        event.consume()
        requestRedraw()
    }

    private fun keyPressed(event: KeyEvent) {
        if (disabled) return
        if (focused && ACTIVATE.matches(event)) {
            armed = true
        }
        if (CANCEL.matches(event)) {
            armed = false
        }
    }

    private fun keyReleased(event: KeyEvent) {
        if (disabled) return
        if (armed && ACTIVATE.matches(event)) {
            armed = false
            performAction()
        }
    }

    // ==== End of events ====

    /**
     * Keyboard combinations :
     *
     * [ACTIVATE] : Space. Perform the same action as clicking the button.
     * [CANCEL] : Escape. `MousePressed` followed by [CANCEL] disarms the button, so `MouseReleased` does nothing.
     *
     */
    object ButtonActions : Actions(null) {
        val ACTIVATE = define("activate", "Activate", Key.SPACE.noMods())
        val CANCEL = define("cancel", "Cancel", Key.ESCAPE.noMods())
    }
}

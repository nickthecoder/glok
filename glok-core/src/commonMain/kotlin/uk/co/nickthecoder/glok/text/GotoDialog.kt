/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.control.TextAreaBase
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.scene.dsl.*
import kotlin.math.min

class GotoDialog(val textArea: TextAreaBase<*>) : Dialog() {

    private val rowSpinner = intSpinner {
        min = 1
        max = textArea.document.lines.size
        value = textArea.caretPosition.row + 1
        autoUpdate = true
        editor.prefColumnCount = if (textArea.document.lines.size > 9999) 8 else 4
    }

    private val columnSpinner = intSpinner {
        min = 1
        max = 9999
        value = 1
        autoUpdate = true
        editor.prefColumnCount = 4
    }

    init {
        title = "Go To Line : Column"
        buttonTypes(ButtonType.OK, ButtonType.CANCEL) { result ->
            if (result == ButtonType.OK) {
                val lines = textArea.document.lines
                val row = min(lines.size - 1, rowSpinner.value - 1)
                val line = lines[row]
                val column = min(line.length, columnSpinner.value - 1)
                with(textArea) {
                    caretPosition = TextPosition(row, column)
                    anchorPosition = caretPosition
                    requestFocus()
                    scrollToCaret()
                }
            }
            stage.close()
        }

        content = hBox {
            padding(10)
            spacing(6)

            + label("Line")
            + rowSpinner
            + label(" ")
            + label("Column")
            + columnSpinner
        }
        onCreate { rowSpinner.requestFocus() }

    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.*
import uk.co.nickthecoder.glok.property.PropertyBase
import uk.co.nickthecoder.glok.property.PropertyException
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import uk.co.nickthecoder.glok.util.GlokException

sealed class SealedTextDocument : HistoryDocumentBase() {
    init {
        history.mergeTimeThresholdProperty.bindTo(GlokSettings.historyMergeTimeThresholdProperty)
    }
}

/**
 * A [TextArea] is a `view` of a [TextDocument]. Two or more [TextArea]s can share the same [TextDocument].
 * (e.g. a `SplitPane` with two views of the same documents side by side).
 *
 * The document is stored as a List of String, where each String must NOT include new line character.
 *
 * An empty document is NOT an empty list. It is a list containing a single blank string.
 * You may safely assume that [lines] is never an empty list.
 *
 * ## History / Undo / Redo
 *
 * All changes to a document are made via a [History], which is in charge of undo/redo.
 *
 * [Change]s related to a [TextDocument] are subclasses of [TextChange].
 * There are only 2 [DeleteText] and [InsertText].
 * Replacements are comprised of a [DeleteText] followed by an [InsertText] within a single [Batch].
 * As such, undo/redo will never split them; they appear to be atomic.
 *
 * NOTE. Changes to the caret/anchor positions of a [TextArea] are not part of a [TextDocument],
 * they are part of the [TextArea].
 * Therefore, you cannot undo/redo changes to the caret/anchor positions.
 */
open class TextDocument(initialText: String) : SealedTextDocument() {

    /**
     * [textProperty] is lazily evaluated, but we need to know if it is bound.
     * So we check this first before using `textProperty.isBound()`.
     */
    private var textPropertyCreated = false

    /**
     * TextDocument does NOT store the document as a String, it is stored as a list of strings
     * where each item in the list does NOT contain a new line character.
     *
     * Therefore, getting/setting is a slow operation for long documents.
     *
     * IMHO, the trade-off, with _everything else_ being _much_ faster is well worth it!
     * e.g. JavaFX's TextArea is painfully slow inserting a single character (for a moderate length document).
     *
     */
    val textProperty: StringProperty by lazy {
        textPropertyCreated = true
        TextProperty()
    }
    val text: String get() = lines.joinToString(separator = "\n")

    internal val mutableLines = mutableListOf<String>().asMutableObservableList()

    /**
     * The text is held as an array of lines.
     * This makes editing the text much more efficient than using one huge String (with new-line characters).
     * New line characters are NOT included in the list elements.
     *
     * `text` is the same as `lines.joinToString(separator="\n")`.
     *
     * NOTE, [lines] should not be an empty list. A blank document is
     * stored as an array with an empty string as its one and only element.
     */
    val lines = mutableLines.asReadOnly()

    init {
        mutableLines.addAll(initialText.split("\n"))
    }

    @Deprecated("Use endPosition")
    fun lastPosition() = endPosition()

    fun endPosition(): TextPosition {
        return TextPosition(lines.size - 1, lines.last().length)
    }

    /**
     * If [pos] is a valid TextPosition for this document, it is returned (unchanged).
     * Otherwise, a new TextPosition is returned, which is valid.
     * The row is in the range :
     *
     *     0 .. lines.size - 1
     *
     * and column is in the range :
     *
     *     0 .. lines[row].length
     */
    fun validPosition(pos : TextPosition ) : TextPosition {
        val row = pos.row
        val column = pos.column
        return if (row < 0) {
            TextPosition(0, 0)
        } else if (row >= lines.size) {
            endPosition()
        } else if (column < 0) {
            TextPosition(row, 0)
        } else if (column > lines[row].length) {
            TextPosition(row, lines[row].length)
        } else {
            pos
        }
    }

    private fun validatePosition(pos: TextPosition) {
        if (pos.row < 0) throw IllegalArgumentException("row out of bounds : $pos")
        if (pos.row >= lines.size) throw IllegalArgumentException("row out of bounds : $pos")
        if (pos.column < 0 || pos.column > lines[pos.row].length) throw IllegalArgumentException("column out of bounds : $pos")
    }

    private fun isPositionValid(pos: TextPosition): Boolean {
        if (pos.row < 0) return false
        if (pos.row >= lines.size) return false
        if (pos.column < 0 || pos.column > lines[pos.row].length) return false
        return true
    }

    private fun isFromToValid(from: TextPosition, to: TextPosition): Boolean {
        return isPositionValid(from) && isPositionValid(to) && from <= to
    }

    fun subset(from: TextPosition, to: TextPosition): List<String> {
        return if (from.row == to.row) {
            listOf(lines[from.row].substring(from.column, to.column))
        } else {
            val result = mutableListOf<String>()
            result.add(lines[from.row].substring(from.column))
            for (i in from.row + 1 until to.row) {
                result.add(lines[i])
            }
            result.add(lines[to.row].substring(0, to.column))
            result
        }
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun clearChange(): DeleteText? {
        if (textPropertyCreated && textProperty.isBound()) throw PropertyException(
            textProperty,
            "Cannot change a bound value"
        )
        if (lines.size == 1 && lines.first().isBlank()) return null
        val from = TextPosition(0, 0)
        val to = TextPosition(lines.size - 1, lines.lastOrNull()?.length ?: 0)
        if (! isFromToValid(from, to)) return null
        return DeleteTextImpl(from, to, false, "Clear")
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun deleteChange(from: TextPosition, to: TextPosition, allowMerge: Boolean): DeleteText? {
        if (textPropertyCreated && textProperty.isBound()) throw PropertyException(
            textProperty,
            "Cannot change a bound value"
        )
        if (! isFromToValid(from, to)) {
            println( "Invalid deletion. From = $from to = $to" )
            return null
        }
        return DeleteTextImpl(from, to, allowMerge)
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun insertChange(from: TextPosition, toInsert: List<String>, allowMerge: Boolean): InsertText? {
        if (textPropertyCreated && textProperty.isBound()) throw PropertyException(textProperty, "Cannot change a bound value")
        validatePosition(from)
        if (toInsert.isEmpty()) return null
        return InsertTextImpl(from, toInsert, allowMerge)
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun insertChange(from: TextPosition, toInsert: String, allowMerge: Boolean) = insertChange(from, toInsert.split("\n"), allowMerge)

    fun insert(from: TextPosition, toInsert: List<String>, allowMerge: Boolean = false) {
        insertChange(from, toInsert, allowMerge)?.now()
    }

    fun insert(from: TextPosition, toInsert: String, allowMerge: Boolean = false) {
        insertChange(from, toInsert, allowMerge)?.now()
    }

    fun append(toAppend: String) {
        insert(endPosition(), toAppend)
    }

    fun delete(from: TextPosition, to: TextPosition, allowMerge: Boolean = false) {
        deleteChange(from, to, allowMerge)?.now()
    }

    fun clear() {
        clearChange()?.now()
    }

    fun replace(from: TextPosition, to: TextPosition, text: String): TextPosition {
        return replace(from, to, text.split("\n"))
    }

    fun replace(from: TextPosition, to: TextPosition, text: List<String>): TextPosition {
        val delete = deleteChange(from, to, false)
        val insert = insertChange(from, text, false)
        history.batch("Replace") {
            + delete
            + insert
        }
        return insert?.caretPosition(false) ?: delete?.caretPosition(false) ?: from
    }

    fun setText(text: String) {
        history.batch("Set Text") {
            + clearChange()
            + insertChange(TextPosition(0, 0), text, false)
        }
    }

    // region ==== TextProperty ====
    /**
     * A [StringProperty], which converts the array of [lines] to a String using newline separators.
     */
    private inner class TextProperty : PropertyBase<String>(), StringProperty, DocumentListener {

        override val bean get() = this@TextDocument
        override val beanName: String get() = "text"

        private var cachedValue: String? = text

        override var value: String
            get() {
                return boundTo?.value ?: validateCache()
            }
            set(newText) {
                if (boundTo != null) {
                    throw PropertyException(this, "Cannot set a bound property : $this")
                }
                setText(newText)
            }

        init {
            this@TextDocument.addListener(this)
        }

        private fun validateCache(): String {
            var result = cachedValue
            if (result == null) {
                result = lines.joinToString(separator = "\n")
                cachedValue = result
            }
            return result
        }

        override fun boundValueChanged(old: String, new: String) {
            history.batch("Set Text") {
                val from = TextPosition(0, 0)
                val to = TextPosition(lines.size - 1, lines.lastOrNull()?.length ?: 0)
                if (! isFromToValid(from, to)) {
                    + DeleteTextImpl(from, to, false)
                }
                if (new.isNotEmpty()) {
                    + InsertTextImpl(TextPosition(0, 0), new.split("\n"), false)
                }
            }
            super.boundValueChanged(old, new)
        }

        private fun setText(newText: String) {
            this@TextDocument.setText(newText)
        }

        override fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean) {
            val oldValue = cachedValue ?: ""
            val newValue = text
            cachedValue = newValue
            fire(oldValue, newValue)
        }
    }

    // endregion TextProperty

    // region == InsertTextImpl ==
    /**
     * Copy/pasted from DeleteTextImpl, swapping undo/redo, and deleted/inserted
     *
     * NOTE. It IS possible to insert _blank_ text (i.e. to do nothing),
     * but the correct way to do this is [textList] = `listOf("")`, not `emptyList()`
     */
    private inner class InsertTextImpl(
        override var from: TextPosition,
        override var textList: List<String>,
        val allowMerge: Boolean,
        override val label: String = "Insert"
    ) : InsertText, ChangeImpl {

        override val document: TextDocument get() = this@TextDocument

        init {
            if (textList.isEmpty()) throw GlokException("Inserted text cannot be an empty list")
        }

        override var to = if (textList.size == 1) {
            TextPosition(from.row, from.column + (textList.first().length))
        } else {
            TextPosition(from.row + textList.size - 1, textList.last().length)
        }

        override fun caretPosition(isUndo: Boolean) = if (isUndo) from else to

        override fun redo() {
            if (from.row == to.row) {
                val row = from.row
                val line = document.lines[row]
                document.mutableLines[row] =
                    line.substring(0, from.column) + textList.first() + line.substring(from.column)
                if (textList.size > 1) {
                    document.mutableLines.addAll(from.row + 1, textList.slice(1 until textList.size))
                }
            } else {
                if (textList.size > 1) {
                    document.mutableLines.addAll(from.row + 1, textList.slice(1 until textList.size))
                }
                val line = document.lines[from.row]
                document.mutableLines[from.row] = line.substring(0, from.column) + textList.first()
                document.mutableLines[to.row] = textList.last() + line.substring(from.column)
            }
        }

        /**
         * Copy/pasted from DeleteTextImpl, swapping undo/redo, and deleted/inserted
         */
        override fun undo() {
            if (from.row == to.row) {
                val line = document.lines[from.row]
                document.mutableLines[from.row] = line.substring(0, from.column) + line.substring(to.column)
                document.mutableLines.removeBetween(from.row + 1..to.row)
            } else {
                document.mutableLines[from.row] = document.lines[from.row].substring(0, from.column) + document.lines[to.row].substring(to.column)
                document.mutableLines.removeBetween(from.row + 1..to.row)
            }
        }


        override fun canMergeWith(previous: Change): Boolean {
            return allowMerge && previous is InsertTextImpl && previous.allowMerge && (previous.from == this.to || previous.to == this.from) &&
                previous.from.row == previous.to.row && this.from.row == this.to.row
        }

        override fun mergeWith(previous: Change) {
            previous as InsertTextImpl
            if (previous.to == this.from) {

                // Merged insert after. e.g. typing successive characters.
                previous.to = this.to
                previous.textList = listOf(previous.textList.first() + this.textList.first())


            } else if (previous.from == this.to) {
                // Merged insert before. Not so useful. Type char, left, then type again???
                previous.from = this.from
                previous.textList = listOf(this.textList.first() + previous.textList.first())

            } else {
                // canMergeWith should prevent us getting here.
                throw IllegalStateException()
            }
        }

        override fun toString() = "InsertText $from .. $to $textList"

    }

    // endregion insert

    // region == DeleteTextImpl ==
    /**
     * For undo/redo, there are 4 different scenarios.
     *
     * 1. [from] is after [to] : Throw an exception.
     * 2. [from].row == [to].row : The deletion is only part of a line (or the whole line, but leaving a blank line behind).
     *    * Redo : delete `lines[from.row]` between `from.column` and `to.column`.
     *    * Undo : insert text at `lines[from.row]` at `from.column`.
     * 3. [to].row == [from].row + 1 : The deletion straddles 2 line.
     *    * Redo : Remove the end of line[from.row] and append the remainder of `line[to.row]`.
     *             Delete 1 or more lines after `from.row`
     *    * Undo : Insert 1 or more lines after `from.row`.
     *             Remember the end of line[from.row].
     *             Chop it off, and append `deleted.first()`
     *
     * 4. [to].row > [from].row + 1
     *    * Redo : same as (3), but also delete lines [from].row + 1 until `to.row`.
     *    * Undo : same as (3), but first, insert the sandwich filling of `deleted` after [from].row.
     *
     * NOTE. [InsertTextImpl] is coded virtually the same (with undo/redo reversed),
     * so any changes/bug fixes should be applied to it also.
     */
    private inner class DeleteTextImpl(
        override var from: TextPosition,
        override var to: TextPosition,
        val allowMerge: Boolean,
        override val label: String = "Delete"
    ) : DeleteText, ChangeImpl {

        override val document: TextDocument get() = this@TextDocument

        override var textList = document.subset(from, to)

        override fun caretPosition(isUndo: Boolean) = if (isUndo) to else from

        override fun redo() {
            if (from.row == to.row) {
                val line = document.lines[from.row]
                document.mutableLines[from.row] = line.substring(0, from.column) + line.substring(to.column)
                document.mutableLines.removeBetween(from.row + 1..to.row)
            } else {
                document.mutableLines[from.row] =
                    document.lines[from.row].substring(0, from.column) + document.lines[to.row].substring(to.column)
                document.mutableLines.removeBetween(from.row + 1..to.row)
            }
        }

        override fun undo() {
            if (from.row == to.row) {
                val row = from.row
                val line = document.lines[row]
                document.mutableLines[row] = line.substring(0, from.column) + textList.first() + line.substring(from.column)
                if (textList.size > 1) {
                    document.mutableLines.addAll(from.row + 1, textList.slice(1 until textList.size))
                }
            } else {
                if (textList.size > 1) {
                    document.mutableLines.addAll(from.row + 1, textList.slice(1 until textList.size))
                }
                val line = document.lines[from.row]
                document.mutableLines[from.row] = line.substring(0, from.column) + textList.first()
                document.mutableLines[to.row] = textList.last() + line.substring(from.column)
            }
        }


        /**
         * Only allow merges of single-row changes.
         */
        override fun canMergeWith(previous: Change): Boolean {
            return allowMerge && previous is DeleteTextImpl && previous.allowMerge && (previous.from == this.from || previous.from == this.to) &&
                textList.size == 1
        }

        /**
         * As we are only allowing single-line merges, `previous.textList.size == 1` and `this.textList.size == 1`.
         * This code is very similar to StringDocument.DeleteStringImpl (which is easier to read).
         *
         * The rows stay the same, and only the columns change.
         */
        override fun mergeWith(previous: Change) {
            previous as DeleteTextImpl
            if (previous.from == this.from) {

                // successive DELETE key presses.
                previous.to = TextPosition(previous.to.row, previous.to.column + textList.first().length)
                previous.textList = listOf(previous.textList.first() + this.textList.first())


            } else if (previous.from == this.to) {

                // e.g. successive BACKSPACE key presses.
                // "Hello" BACKSPACE BACKSPACE -> previous.from = 4 and this.to = 4
                // We want the previous to for from 3 to 5, with str="lo"
                previous.from = this.from
                previous.textList = listOf(this.textList.first() + previous.textList.first())

            } else {
                // canMergeWith should prevent us getting here.
                throw IllegalStateException()
            }
        }

        override fun toString() = "DeleteText $from .. $to $textList"
    }
    // endregion DeleteTextImpl
}

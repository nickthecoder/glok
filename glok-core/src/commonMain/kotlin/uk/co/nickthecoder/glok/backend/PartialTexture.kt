package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image

/**
 * Defines a rectangular region of a [Texture] as an [Image].
 * For example, it is common to place many icons in a single `.png` file, which is loaded as a single
 * [Texture]. Each icon is then defined using [PartialTexture].
 *
 * NOTE. Ensure that there is at least 1 transparent pixel between each icon, otherwise the colors of the neighbouring
 * icons can 'bleed' this icon when rendered. Google 'opengl texture bleeding' for details.
 */
class PartialTexture(

    val texture: Texture,
    val srcX: Float,
    val srcY: Float,
    override val imageWidth: Float,
    override val imageHeight: Float

) : Image {

    override fun batch(tint: Color?, block: TextureBatch.() -> Unit) {
        backend.batch(texture, tint, null, block)
    }

    override fun draw(x: Float, y: Float, tint: Color?) {
        if (tint == null) {
            backend.drawTexture(texture, srcX, srcY, imageWidth, imageHeight, x, y)
        } else {
            backend.drawTintedTexture(texture, tint, srcX, srcY, imageWidth, imageHeight, x, y)
        }
    }

    override fun drawBatched(batch: TextureBatch, x: Float, y: Float) {
        batch.draw(srcX, srcY, imageWidth, imageHeight, x, y)
    }

    override fun drawTo(x: Float, y: Float, destWidth: Float, destHeight: Float, tint: Color?) {
        if (tint == null) {
            backend.drawTexture(texture, srcX, srcY, imageWidth, imageHeight, x, y, destWidth, destHeight)
        } else {
            backend.drawTintedTexture(texture, tint, srcX, srcY, imageWidth, imageHeight, x, y, destWidth, destHeight)
        }
    }

    override fun drawToBatched(batch: TextureBatch, x: Float, y: Float, destWidth: Float, destHeight: Float) {
        batch.draw(srcX, srcY, imageWidth, imageHeight, x, y, destWidth, destHeight)
    }

    override fun partialImage(fromX: Float, fromY: Float, width: Float, height: Float): Image {
        return PartialTexture(texture, srcX + fromX, srcY + fromY, width, height)
    }

    override fun scaledBy(scaleX: Float, scaleY: Float) = ScaledPartialTexture(this, scaleX, scaleY)

    // ==== Object methods ====

    override fun toString() = "PartialTexture : ${texture.name} @ $srcX , $srcY ($imageWidth x $imageHeight)"
}

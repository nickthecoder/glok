package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.util.Converter

/**
 * The selection model used by [ListView] and [TreeView].
 *
 * NOTE. [selectedIndexProperty] and [selectedItemProperty] should change atomically.
 * i.e. when one value changes, the other value must change too, without a period of time when the application
 * can see an inconstancy between them.
 *
 * [T] is the type of the elements in the [items].
 * If [T] is nullable, then extra care must be taken when using [selectedItem].
 * Change the selection to `none`, by setting [selectedIndex] to -1, rather than setting [selectedItem] to null.
 * When reading [selectedIndex], a `null` value has an indeterminate meaning (either no selection, or `null`
 * is selected).
 */
/*
 * The atomic guarantee between [selectedIndexProperty] and [selectedItemProperty]
 * is achieved by the bi-direction bind between the two properties. This is the first ChangeListener
 * for both properties, and will therefore fire before any other ChangeListeners.
 * Also, in PropertyBase, ChangeListeners fire before InvalidationListeners.
*/
class SingleSelectionModel<T>(

    private val items: ObservableList<T>

) {

    @Suppress("unused")
    private val itemsListener = items.addWeakChangeListener { _, change ->
        // When the items list changes, we may need to change the selection too.
        // We can't do this based on the `item`, because this is a LIST, not a SET, and therefore,
        // an item can be in more than one place.
        val oldSelectedItem = selectedItem
        var newSelectedIndex = selectedIndex

        if (selectedIndex >= 0) {
            if (change.isRemoval()) {
                if (change.from + change.removed.size - 1 < selectedIndex) {
                    // Removed BEFORE the selected item
                    newSelectedIndex -= change.removed.size
                } else if (selectedIndex >= change.from && selectedIndex < change.from + change.removed.size) {
                    // Removed the selected item
                    newSelectedIndex = - 1
                }
                // We don't care if we remove AFTER the selected item.

            } else if (change.isAddition()) {
                if (change.from < selectedIndex) {
                    // Added BEFORE the selected item
                    newSelectedIndex += change.added.size
                }
                // We don't care if we added AFTER the selected item.
            }
        }
        // Finally, we set selectedIndex.
        // Let's try to keep the selection in the same "place".
        selectedIndex = if (newSelectedIndex >= 0 && items[newSelectedIndex] === oldSelectedItem) {
            // The new index STILL points to the old item. Great!
            newSelectedIndex
        } else {
            val foundIndex = items.indexOf(oldSelectedItem)
            if (foundIndex >= 0) {
                // There is ANOTHER copy of the item, so let's select the other copy
                foundIndex
            } else {
                // Fall back. This will change selectedItem
                newSelectedIndex
            }
        }
    }

    /**
     * Either -1 (nothing is selected) or the index of the selected item.
     */
    val selectedIndexProperty by intProperty(-1)
    var selectedIndex by selectedIndexProperty

    /**
     * Either null (when nothing is selected), or the selected item.
     *
     * If [T] is nullable, then a null value has two meanings (either the selected item is null,
     * or there is no selected item).
     */
    val selectedItemProperty: Property<T?> = SimpleProperty<T?>(null)
    var selectedItem by selectedItemProperty


    private val indexToItemConvertor = object : Converter<Int, T?> {
        override fun forwards(value: Int): T? {
            return if (value < 0) null else items[value]
        }

        override fun backwards(value: T?): Int {
            return if (value == null) -1 else items.indexOf(value)
        }
    }

    init {
        selectedIndexProperty.bidirectionalBind(selectedItemProperty, indexToItemConvertor)
    }

}

package uk.co.nickthecoder.glok.event

/**
 * These events are fired when a key is pressed or released.
 * Do _not_ use these events to detect characters being typed (use [KeyTypedEvent]).
 *
 * There is no simple relationship between [KeyEvent] and [KeyTypedEvent]s, it's complicated,
 * and platform dependent. For example, on my laptop, I can generate a [KeyTypedEvent] for the `degree`
 * character °, by holding down `CapsLock` and pressing `O` twice. (On Linux, I've set `CapsLock` as my `Compose`
 * key, which combines the next 2 keys pressed into a single character).
 *
 */
class KeyEvent(

    /**
     * Either [EventType.KEY_PRESSED] or [EventType.KEY_RELEASED]
     */
    override val eventType: EventType<KeyEvent>,

    /**
     * Which key was pressed / released.
     */
    val key: Key,

    /**
     * Keyboard specific identifier for the key pressed/released.
     * This is rarely used, as [key] is a device-independent identifier.
     */
    val scanCode: Int,

    /**
     * For events of type [EventType.KEY_PRESSED], which are generated due to a key is held down.
     */
    val repeated: Boolean,

    /**
     * Holds state information for the `Shift`, `Control`, `Alt` and `Super` keys.
     * See [isShiftDown], [isControlDown], [isAltDown], [isSuperDown].
     */
    val mods: Int

) : Event() {

    val isShiftDown get() = isShiftDown(mods)
    val isControlDown get() = isControlDown(mods)
    val isAltDown get() = isAltDown(mods)
    val isSuperDown get() = isSuperDown(mods)

    fun matches(keyCombination: KeyCombination): Boolean {
        if (key == keyCombination.key) {
            if (keyCombination.requiredMods and mods != keyCombination.requiredMods) return false
            if (keyCombination.maybeMods or mods != keyCombination.maybeMods) return false
            return true
        } else {
            return false
        }
    }

    override fun toString() = StringBuilder().apply {
        append("KeyEvent ")
        if (isControlDown) append("Ctrl+")
        if (isShiftDown) append("Shift+")
        if (isAltDown) append("Alt+")
        if (isSuperDown) append("Super+")
        append(key)
    }.toString()

}

/**
 * An event which is fired when a character has been typed.
 *
 * There is no simple relationship between [KeyTypedEvent] and [KeyEvent].
 * For example, on my laptop, a [KeyTypedEvent] is generated for the `degree` character °
 * when I hold down `CapsLock` and press `O` twice.
 */
class KeyTypedEvent(
    val codePoint: Int
) : Event() {

    val char get() = codePoint.toChar()

    override val eventType get() = EventType.KEY_TYPED

}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.SplitPane.Divider
import uk.co.nickthecoder.glok.property.boilerplate.stylableOrientationProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.WithItems
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.*

/**
 * Lays out [items] either horizontally or vertically, with movable [Divider]s between them.
 * Drag the [Divider] to alter how much space is allocated to each item.
 *
 * [Divider.position]s are stored as a ratio of the available space (in the range 0..1).
 *
 * Minimum sizes of the items are IGNORED in the orientation of the SplitPane.
 * Therefore, it is often useful to place each item in a [ScrollPane], otherwise the item may be clipped
 * (and partially inaccessible to the user without moving the Divider).
 *
 * When the [SplitPane] is resized, the proportion of additional space is governed by
 * the items' [Node.growPriority].
 * [Node.shrinkPriority] is ignored. It is ignored so that expanding, and then contracting the
 * SplitPane back to its original size will guarantee that the dividers will be in their original position.
 *
 * To replicate JavaFX's `setResizableWithParent`, set `growPriority` to 1 for nodes which should grow/shrink,
 * and 0 for those that should remain fixed.
 * Glok's default `growPriority` is 0, so you have to change all the resizable nodes in Glok,
 * whereas in JavaFX, you have to change all the fixed-size nodes.
 *
 * [Divider]s are added and removed as necessary, whenever you add/remove from [items].
 * Therefore, you cannot set divider positions before adding the items.
 *
 * ## Common uses for SplitPanes
 *
 * 1. Two documents side by side (or two views of the same document)
 * 2. Tool panels to the left / right, with the main content in the middle.
 *
 * In the first scenario, each panel should have the same growPriority (the value doesn't matter).
 *
 * In the second scenario, set the center's growPriority to 1, and the tool panels to 0.
 * When the window is expanded, the tool panels will remain the same size, and the main content will grow.
 *
 * Note, SplitPane respects [Node.visible] for its [items].
 * So, if an item becomes invisible, there will be one less divider visible too.
 * (The divider still exist though).
 * The item to the right/south will take up the additional available space
 * (except when it is the rightmost/bottom item which is made invisible, in which case,
 * the item to the left/north which will take up the additional space).
 *
 * ### Theme DSL
 *
 *     "split_pane" {
 *
 *         orientation( Orientation.HORIZONTAL )
 *         orientation( Orientation.VERTICAL )
 *
 *         child( "divider" ) {
 *             // a Region.
 *         }
 *     }
 *
 * The `divider` children are [Region]s.
 *
 * SplitPane inherits all the features of [Region]
 */
class SplitPane(initialOrientation: Orientation = Orientation.HORIZONTAL) : Region(), WithItems {

    // region ==== Constructors ====
    constructor(vararg children: Node) : this() {
        for (child in children) {
            items.add(child)
        }
        val dividerCount = dividers.size
        for ((index, divider) in dividers.withIndex()) {
            divider.position = index.toFloat() / dividerCount
        }
    }
    // endregion constructors

    // region ==== Properties ====

    val orientationProperty by stylableOrientationProperty(initialOrientation)
    var orientation by orientationProperty

    // endregion properties

    // region ==== Fields ====

    private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children: ObservableList<Node> = mutableChildren.asReadOnly()

    private val mutableDividers = mutableListOf<Divider>().asMutableObservableList()
    val dividers = mutableDividers.asReadOnly()

    override val items = mutableListOf<Node>().asMutableObservableList()

    // endregion fields

    // region ==== init ====

    init {
        styles.add(SPLIT_PANE)
        pseudoStyles.add(if (initialOrientation == Orientation.HORIZONTAL) HORIZONTAL else VERTICAL)

        orientationProperty.addChangeListener { _, _, new ->
            if (new == Orientation.HORIZONTAL) {
                pseudoStyles.add(HORIZONTAL)
                pseudoStyles.remove(VERTICAL)
            } else {
                pseudoStyles.remove(HORIZONTAL)
                pseudoStyles.add(VERTICAL)
            }
            requestLayout()
        }

        /*
         * When [items] change, we also add/remove those nodes to/from [children],
         * and we also add/remove a [Divider] to/from [dividers].
         */
        items.addChangeListener { _, change ->
            if (change.isReplacement()) {
                for (i in 0 until change.added.size) {
                    val removed = change.removed[i]
                    mutableChildren.remove(removed)
                    mutableChildren.add(change.added[i])
                }
            } else {
                for (node in change.removed) {
                    removedNode(node, change.from)
                }
                for ((index, node) in change.added.withIndex()) {
                    addedNode(node, change.from + index)
                }
            }
        }

        children.addChangeListener(childrenListener)
    }

    // endregion init

    // region ==== Public Methods ====

    fun isHorizontal() = orientation == Orientation.HORIZONTAL
    fun isVertical() = orientation == Orientation.VERTICAL

    /**
     * Adjusts a single divider, and alters other dividers using a heuristic.
     *
     * The same heuristic is used when dragging dividers with the mouse.
     *
     * You may set [Divider.position]s directly, if you prefer full control over the positions.
     */
    fun adjustDivider(dividerIndex: Int, newPosition: Float) {
        adjustDivider(dividers[dividerIndex], newPosition)
    }

    fun adjustDivider(divider: Divider, newPosition: Float) {
        val newClampedPosition = newPosition.clamp(0f, 1f)

        val dividerIndex = dividers.indexOf(divider)
        if (dividerIndex < 0) throw IllegalArgumentException("Divider is not owned by this SplitPane")

        val oldPosition = divider.position
        divider.position = newClampedPosition
        if (dividerIndex > 0) {
            // Allocate the space to the left.
            moveDividers(0, dividerIndex, newClampedPosition - oldPosition, false)
        }
        if (dividerIndex < dividers.size - 1) {
            // Allocate the space to the right
            moveDividers(dividerIndex + 1, dividers.size, newClampedPosition - oldPosition, true)
        }
    }

    /**
     * [fromIndex] and [toIndex] are indices of [dividers]. [toIndex] is _exclusive_.
     */
    private fun moveDividers(fromIndex: Int, toIndex: Int, positionDelta: Float, reverse: Boolean) {
        val visibleItems = items.toList().subList(fromIndex, toIndex + 1).filter { it.visible }

        var sumOfPriorities = visibleItems.sumOf { it.growPriority }
        if (sumOfPriorities > 0f) {
            for (i in fromIndex until toIndex) {
                val divider = dividers[i]
                val item = items[i]
                val growRatio =
                    if (reverse) 1 - (item.growPriority / sumOfPriorities) else item.growPriority / sumOfPriorities
                divider.position += positionDelta * growRatio
            }
        } else {
            sumOfPriorities = visibleItems.size.toFloat()
            for (i in fromIndex until toIndex) {
                val divider = dividers[i]
                val growRatio = if (reverse) 1 - (1 / sumOfPriorities) else 1 / sumOfPriorities
                divider.position += positionDelta * growRatio
            }
        }
    }

    // endregion public methods

    // region ==== Private Methods ====

    private fun addedNode(node: Node, index: Int) {
        mutableChildren.add(node)
        if (dividers.size + 1 < items.size) {
            val newDividerIndex = if (index == 0) 0 else index - 1
            val positionBefore = if (newDividerIndex == 0) 0f else dividers[newDividerIndex - 1].position
            val positionAfter = if (newDividerIndex >= dividers.size - 1) 1f else dividers[newDividerIndex].position
            val divider = Divider((positionBefore + positionAfter) / 2)
            mutableDividers.add(newDividerIndex, divider)
            mutableChildren.add(divider)
        }
    }

    private fun removedNode(node: Node, index: Int) {
        mutableChildren.remove(node)
        if (mutableDividers.isNotEmpty()) {
            val divider = mutableDividers.removeAt(if (index == 0) 0 else index - 1)
            mutableChildren.remove(divider)
        }
    }

    // endregion private methods

    // region ==== Layout ====

    override fun nodeMinHeight(): Float {
        val visibleItems = items.filter { it.visible }
        val pref = if (orientation == Orientation.HORIZONTAL) {
            visibleItems.maxMinHeight()
        } else {
            visibleItems.totalMinHeight()
        }
        return pref + surroundY()
    }

    override fun nodeMinWidth(): Float {
        val visibleItems = items.filter { it.visible }
        val pref = if (orientation == Orientation.HORIZONTAL) {
            visibleItems.totalMinHeight()
        } else {
            visibleItems.maxMinHeight()
        }
        return pref + surroundY()
    }

    override fun nodePrefHeight(): Float {
        val visibleItems = items.filter { it.visible }
        val pref = if (orientation == Orientation.HORIZONTAL) {
            visibleItems.maxPrefHeight()
        } else {
            visibleItems.totalPrefHeight() +
                if (dividers.isEmpty()) 0f else dividers.first().evalPrefHeight() * (visibleItems.size - 1)
        }
        return pref + surroundY()
    }

    override fun nodePrefWidth(): Float {
        val visibleItems = items.filter { it.visible }
        val pref = if (orientation == Orientation.VERTICAL) {
            visibleItems.maxPrefWidth()
        } else {
            visibleItems.totalPrefWidth() +
                    if (dividers.isEmpty()) 0f else dividers.first().evalPrefWidth() * (visibleItems.size - 1)
        }
        return pref + surroundY()
    }

    private var prevAvailable = -1f

    override fun layoutChildren() {
        for (divider in dividers) {
            divider.visible = false
        }

        val visibleItems = items.filter { it.visible }
        if (visibleItems.isEmpty()) {
            return
        }

        val isHorizontal = orientation == Orientation.HORIZONTAL

        var x = surroundLeft()
        var y = surroundTop()
        val availableWidth = width - surroundX()
        val availableHeight = height - surroundY()

        val visibleItemCount = visibleItems.size

        if (visibleItemCount == 1) {
            // Give all available space to the first visible item.
            setChildBounds(visibleItems.first(), x, y, availableWidth, availableHeight)
            for (divider in dividers) {
                divider.visible = false
            }
            return
        }

        // Now we know that there is at least two items visible, and at least 1 divider visible.

        val dividerSize: Float = if (isHorizontal) {
            dividers.first().evalPrefWidth()
        } else {
            dividers.first().evalPrefHeight()
        }

        val currentAvailable = if (isHorizontal) availableWidth else availableHeight
        // How much space is there for the items.
        val availableForItems = currentAvailable - dividerSize * (visibleItemCount - 1)

        if (prevAvailable != currentAvailable && prevAvailable > 0f) {
            // This SplitPane has changed size since the last layout.
            // Let's adjust Divider.positions based on the items' growPriority.
            val sumOfPriorities = items.sumOf { it.growPriority }
            if (sumOfPriorities != 0f) {
                val delta = currentAvailable - prevAvailable
                // Keep track of the x values based on the OLD divider positions and the NEW divider positions,
                // so we can work out the position in pixels of the dividers
                var oldX = 0f
                var newX = 0f
                for (i in 0 until dividers.size) {
                    val divider = dividers[i]
                    val item = items[i]
                    val oldWidth = prevAvailable * divider.position - oldX
                    val newWidth = oldWidth + delta * item.growPriority / sumOfPriorities
                    oldX += oldWidth
                    newX += newWidth

                    divider.position = newX / currentAvailable
                }
            }
        }
        prevAvailable = currentAvailable


        val lastVisibleItem = visibleItems.lastOrNull()

        var fromPosition = 0f
        for (i in 0 until items.size) {
            val item = items[i]
            if (!item.visible) {
                continue
            }

            val toPosition = if (item === lastVisibleItem) 1f else dividers[i].position
            val divider = if (item === lastVisibleItem || i >= dividers.size) null else dividers[i]

            if (isHorizontal) {
                val size = (toPosition - fromPosition) * availableForItems
                setChildBounds(item, x, y, size, availableHeight)
                x += size
                if (divider != null) {
                    divider.visible = true
                    // NOTE, divider is drawn within the padding, but not the border
                    setChildBounds(divider, x, y, dividerSize, availableHeight)
                    x += dividerSize
                }

            } else {
                val size = (toPosition - fromPosition) * availableForItems
                setChildBounds(item, x, y, availableWidth, size)
                y += size
                if (divider != null) {
                    divider.visible = true
                    // NOTE, divider is drawn within the padding, but not the border
                    setChildBounds(divider, x, y, availableWidth, dividerSize)
                    y += dividerSize
                }
            }

            fromPosition = toPosition
        }
    }
    // endregion layout

    // region ==== Draw ====
    /*
        override fun drawChildren() {
            if (isHorizontal()) {
                for (item in items) {
                    if (item.visible) {
                        item.drawAll(item.width < item.evalPrefWidth())
                    }
                }
            } else {
                for (item in items) {
                    if (item.visible) {
                        item.drawAll(item.height < item.evalPrefHeight())
                    }
                }
            }
            for (divider in dividers) {
                if (divider.visible) {
                    divider.drawAll()
                }
            }
        }
    */
    // endregion draw

    // region ==== Object methods ====

    override fun toString() = super.toString() + " $orientation"

    // endregion

    // region == Divider ==

    inner class Divider(position: Float) : Region() {

        /**
         * Determines the position of this Divider.
         * This is NOT measured in pixels, but is a ratio of the SplitPane's width (in the range 0..1).
         *
         * When setting this property, the value is clamped to (0..1).
         * Setting this value may also change other Divider's positions to ensure that the positions
         * never go downwards from left to right.
         *
         * e.g. If the positions were `0.4 , 0.6` and the first divider's position is set to `0.7`,
         * then the 2nd divider will also be set to `0.7`, to prevent the invalid positions : `0.7 , 0.6`.
         *
         * Likewise, starting with `0.4 , 0.6` and changing the second position to `0.1`, will result in
         * both being set to `0.1`.
         */
        var position: Float = position
            set(v) {
                field = v.clamp(0f, 1f)
                var foundMe = false

                // Make sure that the divider positions do not go "backwards"
                for (other in dividers) {
                    if (other === this) {
                        foundMe = true
                    } else {
                        if (foundMe) {
                            if (other.position < field) {
                                other.position = field
                            }
                        } else {
                            if (other.position > field) {
                                other.position = field
                            }
                        }
                    }
                }
            }

        init {
            styles.add(DIVIDER)

            onMousePressed {
                pseudoStyles.add(ARMED)
                it.capture()
            }
            onMouseReleased {
                pseudoStyles.remove(ARMED)
            }
            onMouseDragged { event ->
                val splitPane = this@SplitPane
                if (isHorizontal()) {
                    val x = event.sceneX - splitPane.sceneX - splitPane.surroundLeft()
                    adjustDivider(this, x / (splitPane.width - splitPane.surroundX()))
                } else {
                    val y = event.sceneY - splitPane.sceneY - surroundTop()
                    adjustDivider(this, y / (splitPane.height - splitPane.surroundY()))
                }
                requestLayout()
            }
            onMouseEntered {
                pseudoStyles.add(HOVER)
            }
            onMouseExited {
                pseudoStyles.remove(HOVER)
            }
        }

        override fun toString() = super.toString() + " position=$position"
    }

    // endregion divider
}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.TOGGLE_BUTTON

/**
 * A button which has a boolean [selected] state.
 * If a ToggleButton is part of a [ToggleGroup], then selecting one button will de-select all others.
 * If the same button is pressed _again_, then it is also de-selected (so no buttons in the [ToggleGroup]
 * are selected).
 * If you always want one button to be selected, then use [RadioButton] instead.
 *
 * ### Theme DSL
 *
 *     "toggle_button" {
 *
 *         ":selected" { ... }
 *
 *     }
 *
 * ToggleButton inherits all the features of [ButtonBase].
 *
 * The analogous class for use in menus is [ToggleMenuItem].
 */
class ToggleButton(

    text: String,
    graphic: Node? = null,
    initialToggleGroup: ToggleGroup? = null

) : ToggleButtonBase(text, graphic, initialToggleGroup) {

    init {
        style(TOGGLE_BUTTON)
    }

    override fun toggle() {
        if (! selectedProperty.isBound()) {
            selected = ! selected
        }
    }

}

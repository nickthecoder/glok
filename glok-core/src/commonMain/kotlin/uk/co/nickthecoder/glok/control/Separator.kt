package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.stylableOrientationProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.theme.styles.LINE
import uk.co.nickthecoder.glok.theme.styles.SEPARATOR
import uk.co.nickthecoder.glok.theme.styles.HORIZONTAL
import uk.co.nickthecoder.glok.theme.styles.VERTICAL

/**
 * Often used in [ToolBar]s to visually group buttons.
 * The default theme displays a Separator as a vertical or horizontal bar.
 *
 * ## Theme
 *     "separator" { // this
 *
 *         orientation( Orientation.HORIZONTAL )
 *         orientation( Orientation.VERTICAL )
 *
 *         ":horizontal" { ... }  // Exactly one of these pseudo styles are present.
 *
 *         ":vertical" { ... }
 *
 *         child( "line" ) {
 *             // A region
 *         }
 *     }
 *
 * The child [line] is a [Region].
 *
 * Separator inherits all the features of [Region].
 */
class Separator : Region() {

    // ==== Properties ====

    val orientationProperty by stylableOrientationProperty(Orientation.HORIZONTAL)
    var orientation by orientationProperty

    // ==== End of properties ====

    private val line = Region().apply {
        styles.add(LINE)
    }

    private val privateChildren = mutableListOf<Node>().asMutableObservableList()
    override val children: ObservableList<Node> = privateChildren


    // ==== End of fields ====

    init {
        styles.add(SEPARATOR)

        privateChildren.addChangeListener(childrenListener)
        privateChildren.add(line)

        orientationProperty.addChangeListener { _, _, orient ->
            if (orient == Orientation.HORIZONTAL) {
                pseudoStyles.add(HORIZONTAL)
                pseudoStyles.remove(VERTICAL)
            } else {
                pseudoStyles.remove(HORIZONTAL)
                pseudoStyles.add(VERTICAL)
            }
            requestLayout()
        }
    }

    // ==== End of init ====

    override fun nodePrefWidth() = surroundX() + line.surroundX()
    override fun nodePrefHeight() = surroundY() + line.surroundY()

    override fun layoutChildren() {
        setChildBounds(line, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    // ==== Object methods ====

    override fun toString() = super.toString() + " $orientation"

}

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Orientation

/**
 * Orientations LEFT and RIGHT are mapped to [Orientation.HORIZONTAL].
 * Orientations TOP and BOTTOM are mapped to [Orientation.VERTICAL].
 */
fun ObservableSide.lrToHorizontal(): ObservableOrientation =
    OrientationUnaryFunction(this) { it.lrToHorizontal() }

/**
 * Orientations LEFT and RIGHT are mapped to [Orientation.VERTICAL].
 * Orientations TOP and BOTTOM are mapped to [Orientation.HORIZONTAL].
 */
fun ObservableSide.lrToVertical(): ObservableOrientation =
    OrientationUnaryFunction(this) { it.lrToVertical() }

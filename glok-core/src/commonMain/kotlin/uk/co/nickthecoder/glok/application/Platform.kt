/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

expect object Platform {

    /**
     * The system property `os.name`.
     */
    val OS: String

    /**
     * True iff running in a JVM, and the system property `os.name` == `Windows`
     */
    fun isWindows(): Boolean

    /**
     * True iff running in a JVM, and the system property `os.name` == `Linux`
     */
    fun isLinux(): Boolean

    /**
     * True if system property `os.name` is `Linux`, `FreeBSD` or `SunOS`.
     *
     * Note, this is not true when running on macOS, despite macOS being a unix derived OS.
     */
    fun isUnix(): Boolean
    fun isMacOS(): Boolean

    /**
     * True iff environment variable `XDG_CURRENT_DESKTOP` == 'KDE'.
     *
     * This is used by `ButtonBar`, as KDE orders its buttons differently from Gnome.
     */
    fun isKDE(): Boolean

    /**
     * True iff environment variable `XDG_CURRENT_DESKTOP` == 'Gnome'
     *
     * This is used by `ButtonBar`, as KDE orders its buttons differently from Gnome.
     */
    fun isGnome(): Boolean

    internal fun begin()

    fun getEnv(name: String): String?

    fun isGlokThread(): Boolean

    /**
     * Executes the [runnable] code on Glok's thread at the end of the `event loop`.
     * This can be called from any Thread.
     */
    // IMHO, This should be on Application's companion object. But I chose to be consistent with JavaFX.
    fun runLater(runnable: () -> Unit)

    internal fun performPendingRunnable()
}


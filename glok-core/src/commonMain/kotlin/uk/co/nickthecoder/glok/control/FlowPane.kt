/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableVAlignmentProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.VAlignment
import uk.co.nickthecoder.glok.scene.WithChildren

/**
 * Lays out its children from left to right, then top to bottom,
 * moving onto a new row when the previous row is full.
 *
 * ## Warning
 * Glok isn't designed to handle flowing content. Don't use this control, until you understand its limitations...
 *
 * [nodeMinHeight] and [nodePrefHeight] are of little use, because it depends on the height of this control.
 * When Glok begins laying out nodes, the height isn't known. It is a chicken and egg problem.
 *
 * Breaking this cycle is a bit of a bodge. Before a layout occurs, [nodeMinHeight] and [nodePrefHeight]
 * are meaningless (they return 0).
 * At the end of [layoutChildren], we now know the height required.
 * [nodeMinHeight] and [nodePrefHeight] are now meaningful.
 *
 * But now the node may need to be laid out AGAIN, but we can't do that straight away, because we are currently
 * laying out this node. So we use [Platform.runLater].
 *
 * The net result : This node requires 2 frames to render correctly.
 * Example problems during the 1st frame :
 * * In a `ScrollPane` : The scroll bars won't be present. (Not so bad)
 * * In a `VBox` : This node will take up no space, and may overlap with sibling nodes (Very bad).
 *
 * NEVER place a FlowPane within another FlowPane,
 * because in combination they may cause a layout to be done every frame.
 */
class FlowPane : Region(), WithChildren {

    val xSpacingProperty by stylableFloatProperty(0f)
    var xSpacing by xSpacingProperty

    val ySpacingProperty by stylableFloatProperty(0f)
    var ySpacing by ySpacingProperty

    val vAlignmentProperty by stylableVAlignmentProperty(VAlignment.TOP)
    var vAlignment by vAlignmentProperty

    override val children = mutableListOf<Node>().asMutableObservableList()

    private var calculatedHeight = 0f

    init {
        children.addChangeListener(childrenListener)
        xSpacingProperty.addListener(requestLayoutListener)
        ySpacingProperty.addListener(requestLayoutListener)
        vAlignmentProperty.addListener(requestLayoutListener)
    }

    override fun nodeMinHeight() = calculatedHeight
    override fun nodePrefHeight() = calculatedHeight

    override fun layoutChildren() {

        val row = mutableListOf<Node>()
        var rowHeight = 0f
        fun alignRow() {
            for (child in row) {
                val deltaY = if (vAlignment == VAlignment.BOTTOM) {
                    rowHeight - child.height
                } else {
                    (rowHeight - child.height) / 2
                }
                setChildBounds(child, child.localX, child.localY + deltaY, child.height, child.width)
            }
        }

        var x = surroundLeft()
        var y = surroundTop()
        val right = width - surroundRight()

        for (child in children) {
            val childWidth = child.evalPrefWidth()
            val childHeight = child.evalPrefHeight()
            if (x + xSpacing + childWidth > right) {
                // Next row
                y += rowHeight + ySpacing
                x = surroundLeft()
                rowHeight = 0f
                if (vAlignment != VAlignment.TOP) {
                    alignRow()
                    row.clear()
                }
            }
            setChildBounds(child, x, y, childWidth, childHeight)
            x += childWidth + xSpacing
            if (childHeight > rowHeight) {
                rowHeight = childHeight
            }
        }
        if (vAlignment != VAlignment.TOP) {
            alignRow()
            row.clear()
        }

        val was = calculatedHeight
        calculatedHeight = y + rowHeight + surroundBottom()
        // We've laid out the node, but our pref/min height has changed. So we need to requestLayout.
        // If we do that immediately it will be ignored (because we are laying out), so the best we can do
        // is to use runLater. A nasty bodge.
        if (was != calculatedHeight && (overrideMinHeight == null || overridePrefHeight == null)) {
            Platform.runLater {
                requestLayout()
            }
        }
    }

}

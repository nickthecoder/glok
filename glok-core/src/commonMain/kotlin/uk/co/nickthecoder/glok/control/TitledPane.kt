package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.property.ChangeListener
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.WithContent
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.*

/**
 * Combines a `title`, and `content`.
 * The `title` is displayed above the `content`.
 *
 * If the `title` is clicked, the `content` is hidden, and revealed when the `title` is clicked again.
 * This behaviour can be disabled by setting [collapsable] to false.
 *
 * Note, this is structured differently to JavaFX, which extends [Labelled].
 * Glok's [TitledPane] is _composed_ of a [ButtonBase], and another [content] [Node].
 *
 * TitledPane implements [Toggle], and can therefore be part of a [ToggleGroup], where only one TitledPane
 * is expanded at any time.
 * If many TitledPanes are added to a VBox, then clicking one title will expand its content, and contract
 * the previously expanded TitlePane. This is the same behaviour as JavaFX's `Accordion`.
 *
 * ### Theme DSL
 *
 *     "titled_pane" {
 *
 *         collapsable( value : Boolean )
 *
 *         expanded( value : Boolean )
 *
 *         ":expanded" { ... }     // Pseudo styles, either one
 *         ":contracted" { ... }   // or the other are present.
 *
 *         child( ".title" ) {
 *             // a ButtonBase
 *         }
 *         child( "single_container" ) {
 *             // a SingleContainer
 *
 *             child {
 *                 // The `content` node
 *             }
 *         }
 *     }
 *
 * The child `title` is a [ButtonBase].
 * The other child is a [SingleContainer].
 *
 * TitledPane inherits all the features of [Region].
 */
class TitledPane(title: String, content: Node?) : Region(), Toggle, WithContent, Scrollable {

    constructor(title: String) : this(title, null)

    // region ==== Properties ====

    val titleProperty by stringProperty(title)
    var title by titleProperty

    val contentProperty by optionalNodeProperty(content)
    override var content by contentProperty

    val collapsableProperty by stylableBooleanProperty(true)
    var collapsable by collapsableProperty

    val expandedProperty by stylableBooleanProperty(true)
    var expanded by expandedProperty

    // endregion ==== Properties ====

    // region ==== Fields ====
    private val titleNode = Title()

    private val container = SingleContainer().apply {
        contentProperty.bindTo(this@TitledPane.contentProperty)
        visibleProperty.bindTo(expandedProperty)
    }

    override val children: ObservableList<Node> = listOf(titleNode, container).asObservableList()

    // endregion ==== Fields ====

    // region ==== Toggle ====

    /**
     * A synonym of [expanded].
     *
     * Part of the [Toggle] interface, so that [TitledPane]s can be grouped by a [ToggleGroup],
     * where only one [TitledPane] is expanded at any time.
     */
    override val selectedProperty: BooleanProperty = expandedProperty
    override var selected: Boolean
        get() = expanded
        set(v) {
            expanded = v
        }

    private var selectedToggleListener: ChangeListener<Toggle?, ObservableValue<Toggle?>>? = null

    /**
     * Part of the [Toggle] interface, so that [TitledPane]s can be grouped by a [ToggleGroup],
     * where only one [TitledPane] is expanded at any time.
     */
    override val toggleGroupProperty by optionalToggleGroupProperty(null)
    var toggleGroup by toggleGroupProperty

    /**
     * Part of the [Toggle] interface. Holds arbitrary data
     */
    override var userData: Any? = null

    // endregion ==== Toggle ====

    // ==== init ====
    init {
        styles.add(TITLED_PANE)
        pseudoStyles.addAll(COLLAPSABLE, EXPANDED)
        claimChildren()

        collapsableProperty.addChangeListener { _, _, isCollapsable ->
            pseudoStyleIf(isCollapsable, COLLAPSABLE)
        }

        expandedProperty.addChangeListener { _, _, isExpanded ->
            pseudoStyleIf(isExpanded, EXPANDED)
            pseudoStyleIf(! isExpanded, CONTRACTED)
            if (isExpanded) {
                toggleGroup?.let { group ->
                    group.selectedToggle = this@TitledPane
                }
            } else {
                toggleGroup?.let { group ->
                    if (group.selectedToggle == this@TitledPane) {
                        group.selectedToggle = null
                    }
                }
            }
            requestLayout()
        }

        /*
         * When a toggle group is assigned, we listen to that toggle group's `selectedToggleProperty`.
         * updating `this.expanded`.
         */
        toggleGroupProperty.addChangeListener { _, old, new ->
            if (new != null) {
                if (selectedToggleListener == null) {
                    selectedToggleListener = new.selectedToggleProperty.addChangeListener { _, _, newButton ->
                        expanded = (newButton == this@TitledPane)
                    }
                } else {
                    new.selectedToggleProperty.addChangeListener(selectedToggleListener !!)
                }
            }
            if (old != null) {
                selectedToggleListener?.let { old.selectedToggleProperty.removeChangeListener(it) }
            }
            if (selected) {
                toggleGroup?.selectedToggle = this@TitledPane
            }
        }

        titleNode.textProperty.bindTo(titleProperty)
    }
    // ==== End of init ====

    /**
     * If the content is contracted, and [descendant] is part of the [content],
     * then expand, so that [descendant] is visible.
     */
    override fun scrollTo(descendant: Node) {
        if (! expanded && content?.isAncestorOf(descendant) == true) {
            expanded = true
        }
    }

    // ==== Layout ====

    override fun nodeMinWidth() = surroundX() + children.maxMinWidth()
    override fun nodeMinHeight() = surroundY() + if (expanded) children.totalMinHeight() else titleNode.evalPrefHeight()
    override fun nodePrefWidth() = surroundX() + children.maxPrefWidth()
    override fun nodePrefHeight() =
        surroundY() + if (expanded) children.totalPrefHeight() else titleNode.evalPrefHeight()

    override fun nodeMaxWidth() = surroundX() + children.minMaxWidth()
    override fun nodeMaxHeight() = surroundY() + children.totalMaxHeight()

    override fun layoutChildren() {
        val x = surroundLeft()
        var y = surroundTop()
        val availableX = width - surroundX()
        val availableY = height - surroundY()
        val titlePrefHeight = titleNode.evalPrefHeight()
        val titlePrefWidth = if (titleNode.growPriority > 0f) availableX else min(availableX, titleNode.evalPrefWidth())

        setChildBounds(titleNode, x, y, titlePrefWidth, titlePrefHeight)
        y += titlePrefHeight
        setChildBounds(container, x, y, availableX, availableY - titlePrefHeight)
    }

    // ==== Object methods ====

    override fun toString() = super.toString() + " $title : expanded? $expanded"

    // ==== Title ====

    inner class Title : ButtonBase(title) {

        init {
            styles.add(TITLE)
            alignment = Alignment.CENTER_LEFT
            growPriority = 1f
        }

        override fun performAction() {
            if (collapsable) {
                expanded = !expanded
            }
        }

    }

}

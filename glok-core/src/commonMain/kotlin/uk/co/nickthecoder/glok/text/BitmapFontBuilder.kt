/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text


internal expect fun createBitmapFont(
    identifier: FontIdentifier,
    block: (BitmapFontBuilder.() -> Unit)? = null
): BitmapFont


internal interface BitmapFontBuilder {
    /**
     * Adds additional padding around each glyph.
     * This is useful if you want to create an outlined font.
     *
     * Call this before any calls to [includeGlyphs].
     */
    fun padding(x: Int, y: Int = x)

    /**
     * If you don't specify a maxTextureWidth, then a heuristic will choose a reasonable value
     * based on the font size. So you can safely ignore this!
     *
     * The glyphs will be arranged in a row from left to right until the row exceeds [maxTextureWidth],
     * and the next row will start.
     * If you set this to a very large number, then all glyphs will be in a single row.
     *
     * Do NOT call this after [includeGlyphs].
     */
    fun maxTextureWidth(value: Int)

    /**
     * Choose which characters to include in the final bitmap.
     * You can call this multiple times. If this is not called, then a default of 32..255 is used.
     */
    fun includeGlyphs(chars: Set<Char>)
}


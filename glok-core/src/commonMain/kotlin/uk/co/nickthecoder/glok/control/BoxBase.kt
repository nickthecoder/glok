/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.stylableAlignmentProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.VAlignment
import uk.co.nickthecoder.glok.util.*

/**
Does all the hard work for subclasses [Box], [HBox] and [VBox].
*/
sealed class BoxBase : Region() {

    // ==== Properties ====

    val spacingProperty by stylableFloatProperty(0f)
    var spacing by spacingProperty

    val alignmentProperty by stylableAlignmentProperty(Alignment.TOP_LEFT)
    var alignment by alignmentProperty

    // ==== End of Properties ====

    protected abstract val orientation: Orientation

    protected abstract val fill: Boolean

    // ==== End of Fields ====

    init {
        alignmentProperty.addListener(requestLayoutListener)
        spacingProperty.addListener(requestLayoutListener)
    }

    // ==== Horizontal ====
    // All the code which used to be in HBox, but private with a H suffix.

    private fun layoutChildrenH() {

        val children = children.ifVisible()
        if (children.isEmpty()) return

        val width = width
        val height = height
        val vAlignment = alignment.vAlignment
        val hAlignment = alignment.hAlignment

        val surroundTop = surroundTop()
        val surroundLeft = surroundLeft()

        val xDifference = width - nodePrefWidth()
        val availableY = height - surroundY()

        val filledHeight = height - surroundY()

        // Collect data for each child about its
        val (boxNodeData, remainder) = calculateChildWidths(children, xDifference)

        // Where should we start from?
        var x = if (remainder > 0) {
            when (hAlignment) {
                HAlignment.LEFT -> surroundLeft
                HAlignment.CENTER -> surroundLeft + xDifference / 2
                HAlignment.RIGHT -> surroundLeft + xDifference
            }
        } else {
            surroundLeft
        }

        for (bnd in boxNodeData) {
            val child = bnd.child
            if (child is Separator) {
                setChildBounds(child, x, surroundTop, child.evalPrefWidth(), height - surroundY())
            } else {

                if (fill) {
                    setChildBounds(child, x, surroundTop, bnd.result, filledHeight)
                } else {
                    val childHeight = min(child.evalPrefHeight(), filledHeight)
                    when (vAlignment) {

                        VAlignment.TOP -> {
                            setChildBounds(child, x, surroundTop, bnd.result, childHeight)
                        }

                        VAlignment.CENTER -> {
                            setChildBounds(
                                child,
                                x, surroundTop + (availableY - childHeight) / 2,
                                bnd.result, childHeight
                            )
                        }

                        VAlignment.BOTTOM -> {
                            setChildBounds(
                                child,
                                x, height - surroundBottom() - childHeight,
                                bnd.result, childHeight
                            )
                        }
                    }
                }
            }
            x += spacing + bnd.result
        }
    }

    private fun calcMinWidthH() =
        children.ifVisible().totalMinWidth() + surroundX() + (children.ifVisible().size - 1) * spacing

    private fun calcMinHeightH() = children.ifVisible().maxMinHeight() + surroundY()

    private fun calcPrefWidthH() =
        children.ifVisible().totalPrefWidth() + surroundX() + (children.ifVisible().size - 1) * spacing

    private fun calcPrefHeightH() = children.ifVisible().maxPrefHeight() + surroundY()

    private fun calcMaxWidthH() =
        children.ifVisible().totalMaxWidth() + surroundX() + (children.ifVisible().size - 1) * spacing

    private fun calcMaxHeightH() = children.ifVisible().minMaxHeight() + surroundY()

    // ==== Vertical ====
    // All the code which used to be in VBox, but private with a V suffix.

    private fun layoutChildrenV() {
        val children = children.ifVisible()
        if (children.isEmpty()) return

        val width = width
        val height = height
        val vAlignment = alignment.vAlignment
        val hAlignment = alignment.hAlignment

        val surroundTop = surroundTop()
        val surroundLeft = surroundLeft()

        val availableX = width - surroundX()
        val yDifference = height - nodePrefHeight()

        val filledWidth = width - surroundX()

        // Collect data for each child about its
        val (boxNodeData, remainder) = calculateChildHeights(children, yDifference)

        // Where should we start from?
        var y = if (remainder > 0) {
            when (vAlignment) {
                VAlignment.TOP -> surroundTop
                VAlignment.CENTER -> surroundTop + yDifference / 2
                VAlignment.BOTTOM -> surroundTop + yDifference
            }
        } else {
            surroundTop
        }

        for (bnd in boxNodeData) {
            val child = bnd.child
            if (child is Separator) {
                setChildBounds(child, surroundLeft, y, width - surroundX(), child.evalPrefHeight())
            } else {
                if (fill) {
                    setChildBounds(child, surroundLeft, y, filledWidth, bnd.result)
                } else {
                    val childWidth = min(child.evalPrefWidth(), filledWidth)
                    when (hAlignment) {

                        HAlignment.LEFT -> {
                            setChildBounds(child, surroundLeft, y, childWidth, bnd.result)
                        }

                        HAlignment.CENTER -> {
                            setChildBounds(
                                child,
                                surroundLeft + (availableX - childWidth) / 2, y,
                                childWidth, bnd.result
                            )
                        }

                        HAlignment.RIGHT -> {
                            setChildBounds(
                                child,
                                width - surroundRight() - childWidth, y,
                                childWidth, bnd.result
                            )
                        }
                    }
                }
            }
            y += spacing + bnd.result
        }
    }

    private fun calcMinHeightV() =
        children.ifVisible().totalMinHeight() + surroundY() + (children.ifVisible().size - 1) * spacing

    private fun calcMinWidthV() = children.ifVisible().maxMinWidth() + surroundX()

    private fun calcPrefHeightV() =
        children.ifVisible().totalPrefHeight() + surroundY() + (children.ifVisible().size - 1) * spacing

    private fun calcPrefWidthV() = children.ifVisible().maxPrefWidth() + surroundX()

    private fun calcMaxHeightV() =
        children.ifVisible().totalMaxHeight() + surroundY() + (children.ifVisible().size - 1) * spacing

    private fun calcMaxWidthV() = children.ifVisible().minMaxWidth() + surroundX()

    // ==== End Vertical ====

    override fun layoutChildren() {
        if (orientation == Orientation.HORIZONTAL) {
            layoutChildrenH()
        } else {
            layoutChildrenV()
        }
    }

    override fun nodeMinWidth() =
        if (orientation == Orientation.HORIZONTAL) calcMinWidthH() else calcMinWidthV()

    override fun nodeMinHeight() =
        if (orientation == Orientation.HORIZONTAL) calcMinHeightH() else calcMinHeightV()

    override fun nodePrefWidth() =
        if (orientation == Orientation.HORIZONTAL) calcPrefWidthH() else calcPrefWidthV()

    override fun nodePrefHeight() =
        if (orientation == Orientation.HORIZONTAL) calcPrefHeightH() else calcPrefHeightV()

    override fun nodeMaxWidth() =
        if (orientation == Orientation.HORIZONTAL) calcMaxWidthH() else calcMaxWidthV()

    override fun nodeMaxHeight() =
        if (orientation == Orientation.HORIZONTAL) calcMaxHeightH() else calcMaxHeightV()

    override fun toString() = super.toString() + " $alignment spacing($spacing)"
}

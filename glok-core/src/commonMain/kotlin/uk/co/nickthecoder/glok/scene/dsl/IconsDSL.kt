package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.scene.IconSheet
import uk.co.nickthecoder.glok.scene.Icons
import uk.co.nickthecoder.glok.scene.Sheets


fun Icons.sheets(block: Sheets.() -> Unit) {
    val sheets = Sheets(this)
    sheets.block()
    for (sheet in sheets.sheets) {
        add(sheet)
    }
}

fun Icons.texture(textureName: String, size: Int, block: IconSheet.() -> Unit) {
    val sheet = IconSheet(resources, textureName, size)
    sheet.block()
    add(sheet)
}

fun IconSheet.icon(name: String, x: Int, y: Int) {
    addIcon(name, x, y)
}

fun IconSheet.row(left: Int, top: Int, spacing: Int, vararg names: String) {
    var x = left
    for (name in names) {
        addIcon(name, x, top)
        x += size + spacing
    }
}

fun IconSheet.row(prefix: String, left: Int, top: Int, spacing: Int, vararg names: String) {
    var x = left
    for (name in names) {
        addIcon("$prefix$name", x, top)
        x += size + spacing
    }
}

class IconGrid(val iconSheet: IconSheet, val spacing: Int, var left: Int, var top: Int) {}

fun IconSheet.grid(left: Int, top: Int, spacing: Int, block: IconGrid.() -> Unit) {
    IconGrid(this, spacing, left, top).block()
}

fun IconGrid.row(prefix: String, vararg names: String) {
    var x = left
    for (name in names) {
        println("$prefix$name")
        iconSheet.addIcon("$prefix$name", x, top)
        x += iconSheet.size + spacing
    }
    top += iconSheet.size + spacing
}

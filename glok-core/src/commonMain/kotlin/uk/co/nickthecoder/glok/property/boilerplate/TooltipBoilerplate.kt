
// **** Autogenerated. Do NOT edit. ****

package uk.co.nickthecoder.glok.property.boilerplate

import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.scene.Tooltip
import uk.co.nickthecoder.glok.documentation.Boilerplate 


// region ==== Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ObservableValue<Tooltip>`, use `ObservableTooltip`.
 */
interface ObservableTooltip: ObservableValue<Tooltip> {}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Tooltip>`, use `ReadOnlyTooltipProperty`.
 */
interface ReadOnlyTooltipProperty : ObservableTooltip, ReadOnlyProperty<Tooltip>

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `Property<Tooltip>`, use `TooltipProperty`.
 */
interface TooltipProperty : Property<Tooltip>, ReadOnlyTooltipProperty {

    /**
     * Returns a read-only view of this mutable TooltipProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by TooltipProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyTooltipProperty = ReadOnlyTooltipPropertyWrapper( this )
}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `SimpleProperty<Tooltip>`, we can use `SimpleTooltipProperty`.
 */
open class SimpleTooltipProperty(initialValue: Tooltip, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Tooltip>(initialValue, bean, beanName), TooltipProperty

/**
 * Never use this class directly.
 * Use [TooltipProperty.asReadOnly] to obtain a read-only version of a mutable [TooltipProperty].
 *
 * [Boilerplate] which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Tooltip, Property<Tooltip>>`, use `ReadOnlyTooltipPropertyWrapper`.
 */
class ReadOnlyTooltipPropertyWrapper(wraps: TooltipProperty) :
    ReadOnlyPropertyWrapper<Tooltip, Property<Tooltip>>(wraps), ReadOnlyTooltipProperty


// endregion Basic Features

// region ==== Optional Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ObservableTooltip], but the [value] can also be `null`.
 */
interface ObservableOptionalTooltip : ObservableValue<Tooltip?> {
    
}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyTooltipProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalTooltipProperty : ObservableOptionalTooltip, ReadOnlyProperty<Tooltip?>

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [TooltipProperty], but the [value] can also be `null`.
 */
interface OptionalTooltipProperty : Property<Tooltip?>, ReadOnlyOptionalTooltipProperty {

    /**
     * Returns a read-only view of this mutable OptionalTooltipProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by optionalTooltipProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalTooltipProperty = ReadOnlyOptionalTooltipPropertyWrapper( this )

}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [SimpleTooltipProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalTooltipProperty(initialValue: Tooltip?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Tooltip?>(initialValue, bean, beanName), OptionalTooltipProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ValidatedTooltipProperty], but the [value] can also be `null`.
 */
open class OptionalValidatedTooltipProperty(initialValue: Tooltip?, bean: Any? = null, beanName: String? = null, validation: (ValidatedProperty<Tooltip?>,Tooltip?,Tooltip?)->Tooltip?) :
    ValidatedProperty<Tooltip?>(initialValue, bean, beanName, validation), OptionalTooltipProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyTooltipPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalTooltipPropertyWrapper(wraps: OptionalTooltipProperty) :
    ReadOnlyPropertyWrapper<Tooltip?, Property<Tooltip?>>(wraps), ReadOnlyOptionalTooltipProperty

// endregion optional basic features

// region ==== Delegate ====

/**
 * A Kotlin `delegate` to create a [TooltipProperty] (the implementation will be a [SimpleTooltipProperty].
 * Typical usage :
 *
 *     val fooProperty by tooltipProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun tooltipProperty(initialValue: Tooltip) = PropertyDelegate<Tooltip,TooltipProperty>(initialValue) { bean, name, value ->
    SimpleTooltipProperty(value, bean, name)
}


//endregion Delegate

// region ==== Optional Delegate ====
/**
 * A Kotlin `delegate` to create an [OptionalTooltipProperty] (the implementation will be a [SimpleOptionalTooltipProperty].
 * Typical usage :
 *
 *     val fooProperty by optionalTooltipProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun optionalTooltipProperty(initialValue: Tooltip?) = PropertyDelegate<Tooltip?,OptionalTooltipProperty>(initialValue) { bean, name, value ->
    SimpleOptionalTooltipProperty(value, bean, name)
}

// endregion Optional Delegate



/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.ChangeImpl

interface StyledTextChange : TextChange

// NOTE. It is important that HighlightRange.equals is coded correctly,
// because inserting/deleting text can change the `from` and `to` of those ranges,
// and this is implemented by creating new instances of the HighlightRange.
// Therefore, when we add a range via the history mechanism, when we undo it,
// the instance will not be identical (===), but it must be equal (==).

interface AddRange : StyledTextChange {
    val range: HighlightRange
}


interface RemoveRange : StyledTextChange {
    val range: HighlightRange
}

internal class AddRangeImpl(
    override val document: StyledTextDocument,
    override val range: HighlightRange
) : AddRange, ChangeImpl {

    override val label: String get() = "Add Range"

    override fun redo() {
        document.ranges.add(range)
    }

    override fun undo() {
        document.ranges.remove(range)
    }

    override fun caretPosition(isUndo: Boolean): TextPosition {
        return range.from
    }
}


internal class RemoveRangeImpl(
    override val document: StyledTextDocument,
    override val range: HighlightRange
) : RemoveRange, ChangeImpl {

    override val label: String get() = "Remove Range"

    override fun redo() {
        document.ranges.remove(range)
    }

    override fun undo() {
        document.ranges.add(range)
    }

    override fun caretPosition(isUndo: Boolean): TextPosition {
        return range.from
    }
}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.BooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.OptionalToggleGroupProperty

/**
 * The interface used by [ToggleButton] to allow it to be used as part of a [ToggleGroup].
 */
interface Toggle {

    val selectedProperty: BooleanProperty
    var selected: Boolean

    val toggleGroupProperty: OptionalToggleGroupProperty

    var userData: Any?

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.control.Rotation
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.GlokException

class RotationDSL {
    internal var node: Node? = null
    operator fun Node.unaryPlus() {
        if (node != null) throw GlokException("Rotation's node has already been set to : $node")
        node = this
    }
}

fun rotation(angle: Int, block: (RotationDSL.() -> Unit)? = null): Rotation {
    val dsl = RotationDSL().optionalApply(block)
    val node = dsl.node ?: throw GlokException("Rotation's node note specified")
    throw return Rotation(node, angle)
}

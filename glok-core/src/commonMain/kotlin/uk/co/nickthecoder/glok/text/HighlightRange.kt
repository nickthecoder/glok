/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.TextPosition

open class HighlightRange(
    from: TextPosition,
    to: TextPosition,
    val highlight: Highlight,
    val owner: Any? = null
) {

    var from = from
        internal set

    var to = to
        internal set

    override fun hashCode(): Int {
        var result = from.hashCode()
        result = 31 * result + to.hashCode()
        result = 31 * result + highlight.hashCode()
        result = 31 * result + (owner?.hashCode() ?: 0)
        return result
    }

    override fun equals(other: Any?): Boolean {
        return other is HighlightRange &&
            from == other.from &&
            to == other.to &&
            highlight == other.highlight &&
            owner === other.owner
    }

    override fun toString() = "HighlightRange( $from, $to, $highlight )"

}

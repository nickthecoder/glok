package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.util.Converter

class SpacesToTabsConverter(val columns: Int = 4) : Converter<String, String> {

    /**
     * Given a line containing spaces for indentation, convert it to use tabs instead.
     */
    override fun forwards(value: String): String {
        var counter = 0
        var index = 0
        for (c in value) {
            if (c == '\t') {
                break
            } else if (c.isWhitespace()) {
                counter++
            } else {
                break
            }
            index++
        }
        return if (counter == 0) {
            value
        } else {
            "\t".repeat(counter / columns) + " ".repeat(counter % columns) + value.trimStart()
        }
    }

    /**
     * Given a line containing tab characters for indentation, convert it to use spaces instead.
     */
    override fun backwards(value: String): String {
        var counter = 0
        for (c in value) {
            if (c == '\t') {
                counter++
            } else {
                break
            }
        }
        return if (counter == 0) {
            value
        } else {
            " ".repeat(counter * columns) + value.substring(counter)
        }
    }

}

package uk.co.nickthecoder.glok.text

data class BitmapGlyphData(

    /**
     * The position of the left of the glyph in the texture.
     */
    val left: Float,

    /**
     * The position of the _BASELINE_ of the glyph in the texture.
     */
    val baseline: Float,

    /**
     *   The width of this glyph (i.e. the width of the bounding rectangle which encompasses the glyph
     */
    val width: Float,

    /**
     * The starting position of the NEXT glyph relative to this one when rendering text.
     * This does not take into account kerning. Kerning is not supported by the fonts in Glok.
     */
    val advance: Float,

    /**
     * When drawing a glyph we use a `reference point` which is a point on the `baseline`.
     * Italic glyphs will have pixels to the left of this reference point.
     * So, for example, when drawing a lower case `j` in an italic font,
     * the reference point will be near the left edge of the j where it touches the baseline.
     * [extraBoundsLeft] are the number of pixels to the left of this reference point.
     *
     * NOTE. AWT's Font.getStringBound() does NOT give the correct bounds on my Linux machine.
     * It always returns the same as Font.charWidth() (which is the `advance`).
     * So Glok currently GUESSES this value.
     */
    val extraBoundsLeft : Float

)

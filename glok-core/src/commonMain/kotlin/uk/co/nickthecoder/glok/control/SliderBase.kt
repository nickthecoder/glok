/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyle
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.clamp
import kotlin.math.sign

/**
 * The base class for all sliders.
 * Internally, the slider's value is held as a [ratio] (in the range 0..1).
 */
abstract class SliderBase : Region(), HasReadOnly {

    // region ==== Properties ====

    final override val readOnlyProperty by booleanProperty(false)
    final override var readOnly by readOnlyProperty

    val orientationProperty by stylableOrientationProperty(Orientation.HORIZONTAL)
    var orientation by orientationProperty

    protected val smallRatioStepProperty = SimpleDoubleProperty(0.1)
    protected var smallRatioStep by smallRatioStepProperty

    protected val largeRatioStepProperty = SimpleDoubleProperty(0.1)
    protected var largeRatioStep by largeRatioStepProperty

    val markerColorProperty by stylableColorProperty(Color.WHITE)
    var markerColor by markerColorProperty

    val markerThicknessProperty by stylableFloatProperty(2f)
    var markerThickness by markerThicknessProperty

    val markerLengthProperty by stylableFloatProperty(10f)
    var markerLength by markerLengthProperty

    val minorMarkerLengthProperty by stylableFloatProperty(6f)
    var minorMarkerLength by minorMarkerLengthProperty

    // endregion

    // region ==== Fields ====
    protected abstract var ratio: Double

    /**
     * The movable part
     */
    val thumb = Region().apply { style(THUMB) }

    /**
     * The fixed part
     */
    val track = Region().apply { style(TRACK) }

    override val children = listOf<Node>(track, thumb).asObservableList()
    // endregion

    // region ==== Commands ====
    @Suppress("unused")
    private val commands = Commands().apply {
        with(SliderActions) {
            MINIMUM { if (! readOnly) ratio = 0.0 }
            MAXIMUM { if (! readOnly) ratio = 1.0 }
            DECREMENT { if (! readOnly) adjust(- 1, false) }
            INCREMENT { if (! readOnly) adjust(1, false) }
            SUBTRACT { if (! readOnly) adjust(- 1, true) }
            ADD { if (! readOnly) adjust(1, true) }
        }
        attachTo(this@SliderBase)
    }
    // endregion

    // region ==== init ====
    init {
        pseudoStyle(orientation.pseudoStyle)
        claimChildren()

        readOnlyProperty.addChangeListener { _, _, isReadOnly ->
            pseudoStyleIf(isReadOnly, READ_ONLY)
        }

        orientationProperty.addChangeListener { _, old, new ->
            pseudoStyles.remove(old.pseudoStyle)
            pseudoStyles.add(new.pseudoStyle)
            requestLayout()
        }

        // When these properties change, we need to redraw this node.
        for (prop in listOf(markerThicknessProperty, markerColorProperty, markerLengthProperty)) {
            prop.addListener(requestRedrawListener)
        }

        onMouseEntered {
            if (! readOnly) {
                pseudoStyles.add(HOVER)
            }
        }

        onMouseExited {
            pseudoStyles.remove(HOVER)
        }

        onMousePressed { event ->
            if (!readOnly) {
                firstToRoot { it.focusAcceptable }?.requestFocus()
                // If we want clicks to move in increments
                // adjust(if (ratioForMouse(event) > ratio) 1 else -1, true)
                ratio = ratioForMouse(event).toDouble().clamp(0.0, 1.0)
                pseudoStyles.add(ARMED)
                event.capture()
            }
        }

        onMouseDragged { event ->
            ratio = (ratioForMouse(event).toDouble()).clamp(0.0, 1.0)
        }

        onMouseReleased {
            pseudoStyles.remove(ARMED)
        }

        onScrolled { event ->
            if (!readOnly) {
                adjust(-event.deltaY.sign.toInt(), event.isControlDown)
            }
        }
    }

    // endregion init

    // region ==== Methods ====

    fun isHorizontal() = orientation == Orientation.HORIZONTAL
    fun isVertical() = orientation == Orientation.VERTICAL

    protected fun adjustment(direction: Int, byLargeStep: Boolean) =
        (ratio + direction * (if (byLargeStep) largeRatioStep else smallRatioStep)).clamp(0.0, 1.0)

    protected fun adjust(direction: Int, byLargeStep: Boolean = false) {
        ratio = adjustment(direction, byLargeStep)
    }

    private fun ratioForMouse(event: MouseEvent) = if (isHorizontal()) {
        (event.sceneX - track.sceneX - track.borderSize.left) / (track.width - track.borderSize.x)
    } else {
        1 - ((event.sceneY - track.sceneY - track.borderSize.top) / (track.height - track.borderSize.y))
    }

    //endregion

    // region ==== Layout =====

    // Note, we are relying on the theme to set `prefWidth` or `prefHeight`
    // or the app developer setting them.
    override fun nodePrefWidth() = surroundX() + track.evalPrefWidth()

    override fun nodePrefHeight() = surroundY() + track.evalPrefHeight()

    /**
     * Note, the track takes up available space, excluding the border and padding.
     */
    override fun layoutChildren() {
        val availableX = width - surroundX()
        val availableY = height - surroundY()
        val thumbWidth = thumb.evalPrefWidth()
        val thumbHeight = thumb.evalPrefHeight()
        val x = surroundLeft()
        val y = surroundTop()

        setChildBounds(track, x, y, availableX, availableY)

        if (isHorizontal()) {
            val position = ((track.width - track.borderSize.x) * ratio).toFloat()

            setChildBounds(
                thumb,
                x + track.borderSize.left + position - thumbWidth / 2,
                y + (availableY - thumbHeight) / 2,
                thumbWidth,
                thumbHeight
            )
        } else {
            val position = ((track.height - track.borderSize.y) * (1 - ratio)).toFloat()

            setChildBounds(
                thumb,
                x + (availableX - thumbWidth) / 2,
                y + track.borderSize.top + position - thumbHeight / 2,
                thumbWidth,
                thumbHeight
            )
        }
    }

    /**
     * So that we don't need to ask for [requestLayout] every time a value changes,
     * we lay out the children as part of [drawChildren] too.
     */
    override fun drawChildren() {
        layoutChildren()
        super.drawChildren()
    }

    protected fun drawMarkers(ratios: List<Double>, markerLength: Float) {
        if (orientation == Orientation.VERTICAL) {
            for (ratio in ratios) {
                val y = (sceneY + surroundTop() + (height - surroundY()) * ratio.toFloat() - markerThickness / 2)
                val x = sceneX + surroundLeft() + (width - surroundX() + track.width / 2) / 2
                backend.fillRect(x, y, x + markerLength, y + markerThickness, markerColor)
            }
        } else {
            for (ratio in ratios) {
                val x = (sceneX + surroundLeft() + (width - surroundX()) * ratio.toFloat() - markerThickness / 2)
                val y = sceneY + surroundTop() + (height - surroundY() - track.height / 2) / 2
                backend.fillRect(x, y, x + markerThickness, y - markerLength, markerColor)
            }
        }
    }
    // endregion

    // region == SliderActions ==
    object SliderActions : Actions(null) {

        val MINIMUM = define("minimum", "Minimum", Key.HOME.noMods())
        val MAXIMUM = define("maximum", "Maximum", Key.END.noMods())

        val DECREMENT = define("down", "Down", Key.DOWN.noMods()) {
            add(Key.LEFT.noMods())
        }
        val INCREMENT = define("up", "Up", Key.UP.noMods()) {
            add(Key.RIGHT.noMods())
        }

        val ADD = define("page_up", "Page Up", Key.PAGE_UP.noMods()) {
            add(Key.UP.control(), Key.RIGHT.control())
        }
        val SUBTRACT = define("page_down", "Page Down", Key.PAGE_DOWN.noMods()) {
            add(Key.DOWN.control(), Key.LEFT.control())
        }
    }
    // endregion

}

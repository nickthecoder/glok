/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.TREE_CELL

abstract class TreeCell<T>(val treeView: TreeView<T>, val treeItem: TreeItem<T>) : Region() {

    internal val leafProperty = SimpleBooleanProperty(false)

    internal val expandedProperty = SimpleBooleanProperty(false)

    init {
        style(TREE_CELL)

        leafProperty.bindTo(treeItem.leafProperty)
        expandedProperty.bindTo(treeItem.expandedProperty)

        onMousePressed {
            treeItem.let { treeItem ->
                treeView.selection.selectedItem = treeItem
            }
        }
    }

    override fun toString() = super.toString() + " $treeItem"

}


/**
 * An abstract implementation of [TreeCell] which has a single child node of type [N].
 * This takes care of [layoutChildren], and [nodePrefWidth] and [nodePrefHeight].
 */
abstract class SingleNodeTreeCell<T, N : Node>(
    treeView: TreeView<T>,
    treeItem: TreeItem<T>,
    val node: N
) : TreeCell<T>(treeView, treeItem) {

    // NOTE, this holds the child NODES of this TreeCell, and is not related to
    // TreeItem.children (which are not nodes).
    override val children by lazy { listOf<Node>(node).asObservableList() }

    init {
        claimChildren()
    }

    override fun nodePrefWidth() = node.evalPrefWidth() + surroundX()
    override fun nodePrefHeight() = node.evalPrefHeight() + surroundY()

    override fun layoutChildren() {
        setChildBounds(node, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
}

open class TextTreeCell<T>(

    treeView: TreeView<T>,
    treeItem: TreeItem<T>,
    text: String = treeItem.value.toString()

) : SingleNodeTreeCell<T, Label>(treeView, treeItem, Label(text))

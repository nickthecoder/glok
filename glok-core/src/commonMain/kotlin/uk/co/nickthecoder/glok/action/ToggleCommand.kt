/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.action

import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.property.boilerplate.BooleanProperty

/**
 * A [ToggleCommand] is a [Command] with an extra [selectedProperty].
 * Used with ToggleButton and ToggleMenuItem, where their `selectedProperty` is bound bidirectionally to this one.
 */
class ToggleCommand(
    definition: Action,
    val selectedProperty: BooleanProperty,
    lambda: ((ActionEvent) -> Unit)?
) : Command(definition, lambda)

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

/**
 * Determines the position of the up/down arrows in a [SpinnerBase].
 */
enum class SpinnerArrowPositions(val pseudoStyle: String) {

    /**
     * The value is in the center, with the arrows to the left and right
     */
    SPLIT(":sap_split"),

    /**
     * Left of the value, in a column
     */
    LEFT_VERTICAL(":sap_left_v"),

    /**
     * Left of the value. The arrows and the editor form one row.
     */
    LEFT_HORIZONTAL(":sap_left_h"),

    /**
     * Right of the value, in a column
     */
    RIGHT_VERTICAL(":sap_right_v"),

    /**
     * Right of the value. The arrows and the editor form one row.
     */
    RIGHT_HORIZONTAL(":sap_right_h");

    fun isVertical() = this == LEFT_VERTICAL || this == RIGHT_VERTICAL
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.documentListener
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.functions.or
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * Performs find/replace on a [StyledTextArea].
 *
 * This has no GUI components, see [FindBar] and [ReplaceBar] to add GUI controls.
 * However, you may write your own versions of these, and still use this [FindAndReplace].
 *
 * For keyboard shortcuts to work, you must attach [commands] to a suitable node in your scene graph.
 * e.g.
 *
 *     myMatcher.commands.attachTo( myScene.root )
 *
 * If you wish to change the keyboard shortcuts, change them in [FindAndReplaceActions].
 *
 * Note, multi-line find/replace is not supported.
 *
 * Regular expressions are currently only supported on the [find] string.
 * [replace] is plain text, _not_ a regex replacement.
 */
class FindAndReplace {

    // region ==== Properties ====

    /**
     * The [StyledTextArea] used to by this matcher.
     */
    val styledTextAreaProperty: Property<StyledTextArea?> = SimpleProperty(null, this, "styledTextArea")
    var styledTextArea by styledTextAreaProperty

    /**
     * The text to search for. If [matchRegex], then this should be a regular expression, otherwise it is plain text.
     */
    val findProperty by stringProperty("")
    var find by findProperty

    /**
     * The replacement text.
     */
    val replaceProperty by stringProperty("")
    var replace by replaceProperty

    /**
     * Is the matching case-sensitive?
     */
    val matchCaseProperty by booleanProperty(false)
    var matchCase by matchCaseProperty

    /**
     * Use regular expressions?
     */
    val matchRegexProperty by booleanProperty(false)
    var matchRegex by matchRegexProperty

    /**
     * Match whole words only?
     */
    val matchWordsProperty by booleanProperty(false)
    var matchWords by matchWordsProperty


    private val mutableFindVisibleProperty = SimpleBooleanProperty(false)
    val findVisibleProperty = mutableFindVisibleProperty.asReadOnly()

    /**
     * Is the find-bar visible?
     * Bind the [FindBar].visibleProperty to this.
     */
    var findVisible by mutableFindVisibleProperty
        private set
    private val mutableReplaceVisibleProperty = SimpleBooleanProperty(false)

    val replaceVisibleProperty = mutableReplaceVisibleProperty.asReadOnly()

    /**
     * Is the replace-bar visible?
     * Bind the replace-bar
     */
    var replaceVisible by mutableReplaceVisibleProperty
        private set

    /**
     * When moving to the next/prev match, should we allow looping back round to the first/last match?
     */
    val loopProperty by booleanProperty(true)
    var loop by loopProperty

    private val mutableHasNextProperty = SimpleBooleanProperty(false)
    private val mutableHasPrevProperty = SimpleBooleanProperty(false)

    val hasPrevProperty = mutableHasPrevProperty or (loopProperty and mutableHasNextProperty)
    val hasNextProperty = mutableHasNextProperty or (loopProperty and mutableHasPrevProperty)

    /**
     * Are there more matches before the current match?
     * Used to enable/disable the "previous" button.
     */
    var hasPrev by mutableHasPrevProperty
        private set

    /**
     * Are there more matches after the current match?
     * Used to enable/disable the "next" button.
     */
    var hasNext by mutableHasNextProperty
        private set


    private val mutableStatusProperty = SimpleStringProperty("")
    val statusProperty = mutableStatusProperty.asReadOnly()

    /**
     * Text such as "1 of 3 matches"
     */
    var status by mutableStatusProperty
        private set

    val iconSizeProperty by intProperty(24)
    var iconSize by iconSizeProperty

    /**
     * Is a match the TextArea's current selection.
     * Used to determine if REPLACE_ONE is disabled.
     */
    private val mutableMatchSelectedProperty = SimpleBooleanProperty(false)
    val matchSelectedProperty = mutableMatchSelectedProperty.asReadOnly()
    var matchSelected by mutableMatchSelectedProperty
        private set

    private val hasMatchesProperty = SimpleBooleanProperty(false)
    private var hasMatches by hasMatchesProperty
    // endregion Properties

    // region ==== Fields ====

    var matchHighlight: Highlight = ThemedHighlight(".find_match")
    var replacementHighlight: Highlight = ThemedHighlight(".find_replacement")

    private var regex: Regex = Regex("")

    private val matches = mutableListOf<HighlightRange>()

    /**
     * Parts of the document which have been REPLACED, i.e. they no longer match, but may still be of
     * interest.
     */
    private val replacements = mutableListOf<HighlightRange>()

    private var currentMatchIndex = - 1

    var maxMatches = 400

    /**
     * See [selectionListener]
     */
    private var weMovedCaret = false

    private var searchFrom = TextPosition(0, 0)

    // endregion Fields

    // region ==== Commands ====
    val commands = Commands().apply {

        with(FindAndReplaceActions) {

            CLOSE(consume = false) { // Allows the ESCAPE key to perform more than one action.
                findVisible = false
                replaceVisible = false
                clearMatches()
                styledTextArea?.requestFocus()
            }
            SHOW_FIND {
                // Hide, then show, to ensure that the FindBar takes input focus.
                findVisible = false
                findVisible = true
            }
            SHOW_REPLACE {
                if (findVisible) {
                    // Hide, then show, to ensure that the ReplaceBar takes input focus.
                    replaceVisible = false
                    replaceVisible = true
                } else {
                    replaceVisible = true
                    // Do this last, so that the FindBar takes focus, not the ReplaceBar
                    findVisible = true
                }
            }
            SHOW_GOTO_DIALOG {
                styledTextArea?.let { textArea ->
                    GotoDialog(textArea).createStage(textArea.scene !!.stage !!, StageType.MODAL).show()
                }
            }

            FIND_NEXT {
                nextMatch()
                styledTextArea?.requestFocus()
            }.disable(! hasNextProperty)

            FIND_PREV {
                previousMatch()
                styledTextArea?.requestFocus()
            }.disable(! hasPrevProperty)

            REPLACE_ONE {
                replaceOne()
                styledTextArea?.requestFocus()
            }.disable(! matchSelectedProperty)

            REPLACE_ALL {
                replaceAll()
                styledTextArea?.requestFocus()
            }.disable(! hasMatchesProperty)

            toggle(MATCH_CASE, matchCaseProperty)
            toggle(MATCH_REGEX, matchRegexProperty)
            toggle(MATCH_WORDS, matchWordsProperty)
        }
    }

    // endregion

    // region ==== Listeners ====

    /**
     * If the find text, or any of the three boolean options change, when start the matching process.
     */
    private val startFindListener = findProperty.addListener {
        performFind(true)
    }.apply {
        matchCaseProperty.addListener(this)
        matchWordsProperty.addListener(this)
        matchRegexProperty.addListener(this)
    }

    private val selectionListener = invalidationListener {
        styledTextArea?.let { styledTextArea ->
            // Keep our own copy of the caret position, so that when we start typing in the FindBar,
            // we can move the caret to the first position at, or after [searchFrom].
            if (! weMovedCaret) {
                searchFrom = styledTextArea.caretPosition
            }
            val from = styledTextArea.selectionStart
            val to = styledTextArea.selectionEnd
            val selectedMatchIndex = findMatchIndex(from, to)
            matchSelected = selectedMatchIndex >= 0
            if (selectedMatchIndex >= 0) {
                currentMatchIndex = selectedMatchIndex
                updatePrevNext()
            }
        }
    }

    private val documentListener = documentListener { _, _, _ ->
        if (! weMovedCaret) {
            performFind(false)
        }
    }

    /**
     * When the [styledTextArea] changes, we need to remove listeners from the old, and add listeners to the new.
     */
    private val controlListener = styledTextAreaProperty.addChangeListener { _, old, new ->
        clearMatches()

        new?.let { searchFrom = it.caretPosition }

        old?.document?.ranges?.removeAll { it.owner === this }

        old?.anchorPositionProperty?.removeListener(selectionListener)
        old?.caretPositionProperty?.removeListener(selectionListener)
        old?.document?.removeListener(documentListener)

        new?.anchorPositionProperty?.addListener(selectionListener)
        new?.caretPositionProperty?.addListener(selectionListener)
        new?.document?.addListener(documentListener)

        performFind(false)
    }

    /**
     * Remove the highlights when the find-bar is made invisible.
     * Perform find again when the find is made visible (but without moving the caret).
     */
    private val findVisibleListener = findVisibleProperty.addChangeListener { _, _, isVisible ->
        if (isVisible) {
            performFind(false)
        } else {
            clearMatches()
        }
    }

    // endregion listeners

    private fun performFind(moveCaret: Boolean = true) {
        clearMatches()
        currentMatchIndex = - 1

        if (! findVisible) return

        if (find.isEmpty()) {
            if (moveCaret) {
                styledTextArea?.let { styledTextArea ->
                    styledTextArea.caretPosition = searchFrom
                    styledTextArea.anchorPosition = searchFrom
                    styledTextArea.scrollToCaret()
                }
            }
        } else {

            var pattern = if (matchRegex) find else Regex.escape(find)
            if (matchWords) {
                pattern = "\\b$find\\b"
            }
            regex = if (matchCase) {
                Regex(pattern)
            } else {
                Regex(pattern, RegexOption.IGNORE_CASE)
            }

            styledTextArea?.let { styledTextArea ->
                for ((row, line) in styledTextArea.document.lines.withIndex()) {
                    for (matchResult in regex.findAll(line, 0)) {
                        addMatch(row, matchResult.range)
                    }
                }

                if (moveCaret) {
                    val nextMatch = matches.firstOrNull { it.from >= searchFrom } ?: matches.firstOrNull()
                    if (nextMatch != null) {
                        currentMatchIndex = matches.indexOf(nextMatch)
                        highlightCurrentMatch()
                    } else {
                        currentMatchIndex = - 1
                        styledTextArea.caretPosition = searchFrom
                        styledTextArea.anchorPosition = searchFrom
                        styledTextArea.scrollToCaret()
                    }
                }
            }

        }
        updatePrevNext()
    }

    private fun addMatch(row: Int, range: IntRange) {
        val start = TextPosition(row, range.first)
        val end = TextPosition(row, range.last + 1)
        val match = HighlightRange(start, end, matchHighlight, owner = this)
        matches.add(match)
        styledTextArea?.document?.ranges?.add(match)
        hasMatches = true
    }

    private fun addReplacement(row: Int, range: IntRange) {
        val start = TextPosition(row, range.first)
        val end = TextPosition(row, range.last + 1)
        val replacement = HighlightRange(start, end, replacementHighlight, owner = this)
        replacements.add(replacement)
        styledTextArea?.document?.ranges?.add(replacement)
    }

    private fun removeMatch(matchIndex: Int) {
        val range = matches.removeAt(matchIndex)
        styledTextArea?.document?.ranges?.remove(range)
        hasMatches = matches.isNotEmpty()
    }

    private fun clearMatches() {
        styledTextArea?.document?.ranges?.removeAll { it.owner === this }
        matches.clear()
        replacements.clear()
        currentMatchIndex = - 1
        hasMatches = false
        updatePrevNext()
    }

    private fun previousMatch() {
        styledTextArea?.let { styledTextArea ->
            // If we aren't AT one of the matches, then find the previous one based on the caret's position.
            if (currentMatchIndex < 0) {
                val caret = styledTextArea.caretPosition
                for (i in 0 until matches.size) {
                    val match = matches[i]
                    if (match.from >= caret) {
                        break
                    }
                    currentMatchIndex = i
                }
            } else {
                if (currentMatchIndex > 0) {
                    currentMatchIndex --
                } else if (loop && matches.size > 1) {
                    currentMatchIndex = matches.size - 1
                }
            }
            highlightCurrentMatch()
            updatePrevNext()
        }
    }

    private fun nextMatch() {
        styledTextArea?.let { styledTextArea ->
            // If we aren't AT one of the matches, then find the next one based on the caret's position.
            if (currentMatchIndex < 0) {
                val caret = styledTextArea.caretPosition
                for (i in 0 until matches.size) {
                    val match = matches[i]
                    if (match.from >= caret) {
                        currentMatchIndex = i
                        break
                    }
                }
            } else {
                if (currentMatchIndex < matches.size - 1) {
                    currentMatchIndex ++
                } else if (loop && matches.size > 1) {
                    currentMatchIndex = 0
                }
            }
            highlightCurrentMatch()
            updatePrevNext()
        }
    }

    private fun replaceOne() {
        if (matches.isEmpty() || currentMatchIndex < 0) return

        styledTextArea?.let { styledTextArea ->
            try {
                weMovedCaret = true
                val match = matches[currentMatchIndex]
                removeMatch(currentMatchIndex)

                styledTextArea.document.replace(match.from, match.to, replace)

                if (replace.isNotEmpty()) {
                    addReplacement(match.from.row, match.from.column until match.from.column + replace.length)
                }

                highlightCurrentMatch()
                updatePrevNext()
                if (currentMatchIndex >= matches.size) {
                    currentMatchIndex = - 1
                }
            } finally {
                weMovedCaret = false
            }
        }
    }

    fun replaceAll() {
        if (matches.isEmpty()) return

        styledTextArea?.let { styledTextArea ->
            try {
                weMovedCaret = true
                styledTextArea.document.ranges.removeAll { it.owner === this }
                val replacementLength = replace.length
                val newReplacements = if (replacementLength == 0) {
                    emptyList()
                } else {
                    matches.map {
                        val newTo = TextPosition(it.from.row, it.from.column + replacementLength)
                        HighlightRange(it.from, newTo, replacementHighlight, owner = this)
                    }
                }

                styledTextArea.document.history.batch("Replace All") {
                    currentMatchIndex = matches.size - 1
                    while (currentMatchIndex >= 0) {
                        val match = matches[currentMatchIndex]
                        + styledTextArea.document.deleteChange(match.from, match.to, false)
                        + styledTextArea.document.insertChange(match.from, replace, false)
                        currentMatchIndex --
                    }
                }

                styledTextArea.document.ranges.removeAll(matches)
                styledTextArea.document.ranges.addAll(newReplacements)
                replacements.addAll(newReplacements)
                matches.clear()
                currentMatchIndex = - 1
                updatePrevNext()

            } finally {
                weMovedCaret = false
            }
        }
    }

    /**
     * Highlight the current match. This changes the control's selection.
     */
    private fun highlightCurrentMatch() {
        styledTextArea?.let { styledTextArea ->
            try {
                weMovedCaret = true
                if (currentMatchIndex >= 0 && currentMatchIndex < matches.size) {
                    val match = matches[currentMatchIndex]
                    styledTextArea.caretPosition = match.from
                    styledTextArea.anchorPosition = match.to
                    styledTextArea.scrollToCaret()
                }
            } finally {
                weMovedCaret = false
            }
        }
    }

    private fun findMatchIndex(start: TextPosition, end: TextPosition): Int {
        // Optimisation : Test the current match first, as it is the most likely.
        if (currentMatchIndex >= 0 && currentMatchIndex < matches.size) {
            if (matches[currentMatchIndex].from == start && matches[currentMatchIndex].to == end) {
                return currentMatchIndex
            }
        }

        matches.forEachIndexed { i, m ->
            if (m.from == start && m.to == end) return i
            if (m.from > start) return - 1 // Matches are in order, so we can end early
        }
        return - 1
    }

    private fun updatePrevNext() {
        styledTextArea?.let { styledTextArea ->

            if (currentMatchIndex < 0) {
                hasPrev = matches.isNotEmpty() && styledTextArea.caretPosition > matches[0].from
                hasNext = matches.isNotEmpty() && styledTextArea.caretPosition < matches.last().from
            } else {
                hasPrev = currentMatchIndex > 0
                hasNext = currentMatchIndex < matches.size - 1
            }

            status = if (find.isEmpty()) {
                ""
            } else if (findVisible) {
                if (matches.isEmpty()) {
                    "No matches"
                } else if (matches.size == 1) {
                    "One match"
                } else if (matches.size >= maxMatches) {
                    "Too many matches"
                } else {
                    if (currentMatchIndex >= 0) {
                        "${currentMatchIndex + 1} of ${matches.size} matches"
                    } else {
                        "${matches.size} matches"
                    }
                }
            } else {
                ""
            }
        }
    }
}

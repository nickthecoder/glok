/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.text.Indentation

/**
 * Creates an [ObservableIndentation] based on 3 properties.
 *
 *       if (withTabs) {
 *           Indentation.tabIndentation(columns)
 *       } else {
 *           Indentation.spacesIndentation(columns, behaveLikeTabs)
 *       }
 */
fun indentationProperty(
    withTabsProperty: ObservableBoolean,
    columnsProperty: ObservableInt,
    behaveLikeTabsProperty: ObservableBoolean
): ObservableIndentation = IndentationTernaryFunction(
    withTabsProperty, columnsProperty, behaveLikeTabsProperty
) { withTabs, columns, behaveLikeTabs ->
    if (withTabs) {
        Indentation.tabIndentation(columns)
    } else {
        Indentation.spacesIndentation(columns, behaveLikeTabs)
    }
}


/**
 * Creates an [ObservableIndentation] based on 2 properties.
 *
 *
 *       if (withTabs) {
 *           Indentation.tabIndentation(columns)
 *       } else {
 *           Indentation.spacesIndentation(columns, behaveLikeTabs=false)
 *       }
 */
fun indentationProperty(
    usingTabsProperty: ObservableBoolean,
    columnsProperty: ObservableInt,
    behaveLikeTabs : Boolean = false
): ObservableIndentation = IndentationBinaryFunction(
    usingTabsProperty, columnsProperty,
) { withTabs, columns ->
    if (withTabs) {
        Indentation.tabIndentation(columns)
    } else {
        Indentation.spacesIndentation(columns, behaveLikeTabs)
    }
}

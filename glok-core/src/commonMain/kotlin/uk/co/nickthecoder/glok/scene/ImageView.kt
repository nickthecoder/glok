package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.theme.styles.IMAGE_VIEW
import uk.co.nickthecoder.glok.util.max2DPs
import kotlin.math.min

class ImageView(image: Image?) : Region() {

    constructor(imageProperty: ObservableOptionalImage) : this(imageProperty.value) {
        this.imageProperty.bindTo(imageProperty)
    }

    constructor(imageProperty: ObservableImage) : this(imageProperty.value) {
        this.imageProperty.bindCastTo(imageProperty)
    }

    // ==== Properties ====

    val imageProperty by stylableOptionalImageProperty(image)
    var image by imageProperty

    /**
     * The required height of this [ImageView], or the sentinel value [USE_IMAGE_SIZE] (-1),
     * to use the [image]'s width.
     *
     * NOTE, if [preserveAspectRatio] == true, then the [evalPrefWidth] may be smaller.
     */
    val fitWidthProperty by floatProperty(USE_IMAGE_SIZE)
    var fitWidth by fitWidthProperty

    /**
     * The required width of this [ImageView], or the sentinel value [USE_IMAGE_SIZE] (-1),
     * to use the [image]'s height.
     *
     * NOTE, if [preserveAspectRatio] == true, then the [evalPrefHeight] may be smaller.
     */
    val fitHeightProperty by floatProperty(USE_IMAGE_SIZE)
    var fitHeight by fitHeightProperty

    /**
     * Should the [image]'s aspect ratio be maintained when [fitWidth] and/or [fitHeight] are set?
     */
    val preserveAspectRatioProperty by booleanProperty(true)
    var preserveAspectRatio by preserveAspectRatioProperty

    /**
     * Multiplies the image's pixels by this color. Often used in one of two ways :
     * 1. Making the image (semi) transparent with R=1, G=1, B=1 and varying the alpha channel.
     * 2. Using a greyscale image, and then using the tint color to choose the hue.
     */
    val tintProperty by stylableColorProperty(Color.WHITE)
    var tint by tintProperty

    // End of properties

    init {
        styles.add(IMAGE_VIEW)
        for (prop in arrayOf(imageProperty, fitWidthProperty, fitHeightProperty, preserveAspectRatioProperty)) {
            prop.addListener(requestLayoutListener)
        }
        tintProperty.addListener(requestRedrawListener)
    }

    // End of init

    override fun nodePrefWidth(): Float {
        val image = image ?: return 0f

        if (fitWidth == USE_IMAGE_SIZE && fitHeight == USE_IMAGE_SIZE) {
            return image.imageWidth + surroundX()
        }
        val reqWidth = if (fitWidth == USE_IMAGE_SIZE) {
            image.imageWidth + surroundX()
        } else {
            fitWidth + surroundX()
        }
        if (!preserveAspectRatio) {
            return reqWidth + surroundX()
        }

        val reqHeight = if (fitHeight == USE_IMAGE_SIZE) {
            image.imageHeight
        } else {
            fitHeight
        }
        return min(reqWidth, reqHeight * image.aspectRatio()) + surroundX()
    }

    override fun nodePrefHeight(): Float {
        val image = image ?: return 0f

        if (fitWidth == USE_IMAGE_SIZE && fitHeight == USE_IMAGE_SIZE) {
            return image.imageHeight + surroundY()
        }
        val reqHeight = if (fitWidth == USE_IMAGE_SIZE) {
            image.imageHeight
        } else {
            fitWidth
        }
        if (!preserveAspectRatio) {
            return reqHeight + surroundY()
        }

        val reqWidth = if (fitWidth == USE_IMAGE_SIZE) {
            image.imageWidth
        } else {
            fitWidth
        }
        return min(reqHeight, reqWidth / image.aspectRatio()) + surroundY()
    }

    override fun draw() {
        super.draw()
        image?.drawTo(sceneX + surroundLeft(), sceneY + surroundTop(),
            width - surroundX(), height - surroundY(),
            tint)
    }

    // ==== Object methods ====

    override fun toString() = "${super.toString()} tint=$tint" + if (image == null) {
        " imageNotSet"
    } else {
        " ${image!!.imageWidth.max2DPs()} x ${image!!.imageHeight.max2DPs()}"
    }

    // ==== Companion ====

    companion object {
        /**
         * Used by [fitWidth] and [fitHeight] as sentinel values
         */
        const val USE_IMAGE_SIZE = -1f

    }

}

fun Image?.asImageView() = if (this == null) null else ImageView(this)

// Cannot bind an optional value to a non-optional value
//fun ObservableImage.asImageView() = ImageView( this )
fun ObservableOptionalImage.asImageView() = ImageView(this)

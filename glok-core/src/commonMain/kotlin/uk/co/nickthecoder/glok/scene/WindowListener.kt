/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.event.*

interface WindowListener {

    fun close()
    fun resize(width: Int, height: Int)
    fun minimize(minimized: Boolean)
    fun focus(focused: Boolean)

    fun key(key: Key, releasePressRepeat: Int, scanCode: Int, mods: Int)
    fun typed(codePoint: Int)

    fun mouseEnterExit(entered: Boolean)
    fun mouseMove(x: Int, y: Int)
    fun mouseButton(buttonIndex: Int, pressed: Boolean, mods: Int)
    fun scroll(deltaX: Float, deltaY: Float)

    /**
     * Files have been dropped onto the window.
     */
    fun droppedFiles(paths: List<String>)
}

/**
 * The [WindowListener] that [RegularStage] uses to listen to the messages issued from the window.
 * The raw messages are massaged to make them more acceptable to Glok.
 * Most message are turned in [Event]s. The exceptions being [close], [resize], [focus], [minimize].
 *
 * Some complexities of [RegularStage] and [OverlayStage] are dealt with here.
 * For example moving the mouse out of an [OverlayStage] will generate a `MouseExit` event for the [OverlayStage]
 * and a `MouseEnter` event for the [RegularStage].
 */
internal class StageWindowListener(val stage: RegularStage) : WindowListener {

    private var savedMouseY: Float = 0f
    private var savedMouseX: Float = 0f
    private var savedMods = 0
    private var mouseInStage: StageBase? = null
    private var savedMouseButtonIndex: Int = -1

    /**
     * Keep track of which stage received the latest mouseDown event, so that dragging
     * can be redirected to the same stage
     */
    private var mouseDownInStage: StageBase? = null

    override fun close() {
        stage.close()
    }

    override fun resize(width: Int, height: Int) {
        stage.closePopupMenus()
        stage.closePopups()
        stage.scene?.stageResized(width.toFloat() / stage.scale, height.toFloat() / stage.scale)
    }

    /**
     * When the window gains/loses focus, we change [RegularStage.windowFocused] too.
     * When the window loses focus, we close popups (such as tooltips) and menus.
     */
    override fun focus(focused: Boolean) {
        stage.windowFocused = focused
        if (! focused) {
            stage.closePopups()
            stage.closePopupMenus()
        }
    }

    override fun minimize(minimized: Boolean) {
        stage.minimized = minimized
    }

    override fun key(key: Key, releasePressRepeat: Int, scanCode: Int, mods: Int) {
        var fixedMods = mods
        val eventType = when (releasePressRepeat) {
            0 -> {
                when (key) {
                    Key.LEFT_CONTROL, Key.RIGHT_CONTROL -> fixedMods = mods and Event.MOD_CONTROL.inv()
                    Key.LEFT_SHIFT, Key.RIGHT_SHIFT -> fixedMods = mods and Event.MOD_SHIFT.inv()
                    Key.LEFT_ALT, Key.RIGHT_ALT -> fixedMods = mods and Event.MOD_ALT.inv()
                    Key.LEFT_SUPER, Key.RIGHT_SUPER -> fixedMods = mods and Event.MOD_SUPER.inv()
                    else -> Unit
                }
                EventType.KEY_RELEASED
            }

            1, 2 -> {
                when (key) {
                    Key.LEFT_CONTROL, Key.RIGHT_CONTROL -> fixedMods = mods or Event.MOD_CONTROL
                    Key.LEFT_SHIFT, Key.RIGHT_SHIFT -> fixedMods = mods or Event.MOD_SHIFT
                    Key.LEFT_ALT, Key.RIGHT_ALT -> fixedMods = mods or Event.MOD_ALT
                    Key.LEFT_SUPER, Key.RIGHT_SUPER -> fixedMods = mods or Event.MOD_SUPER
                    else -> Unit
                }
                EventType.KEY_PRESSED
            }

            else -> return
        }

        savedMods = fixedMods

        (stage.focusedStage as? StageBase)
            ?.onKeyEvent(KeyEvent(eventType, key, scanCode, releasePressRepeat == 2, fixedMods))
    }

    override fun typed(codePoint: Int) {
        val event = KeyTypedEvent(codePoint)
        (stage.focusedStage as? StageBase)?.onKeyTyped(event)
    }

    override fun droppedFiles(paths: List<String>) {

        val overlayOrRegular = stage.overlayAt(savedMouseX, savedMouseY) ?: stage

        // Prevent interaction, if we tried to scroll outside a Modal OverlayStage.
        val lastOverlay = stage.regularStage.overlayStages.lastOrNull()
        if (lastOverlay != null && lastOverlay.stageType == StageType.MODAL && lastOverlay != overlayOrRegular) return

        overlayOrRegular.scene?.let { scene ->
            val event = DroppedFilesEvent(scene, savedMouseX, savedMouseY, paths)
            overlayOrRegular.onDroppedFiles(event)
        }
    }

    override fun mouseEnterExit(entered: Boolean) {
        if (entered) {
            val overlayOrRegular = stage.overlayAt(savedMouseX, savedMouseY) ?: stage
            mouseInStage = overlayOrRegular
            overlayOrRegular.onMouseEnterExit(true)
            overlayOrRegular.regularStage.mouseInStage = overlayOrRegular
        } else {
            mouseInStage?.regularStage?.mouseInStage?.onMouseEnterExit(false)
            mouseInStage?.regularStage?.mouseInStage = null
            mouseInStage = null
        }
    }

    override fun mouseMove(x: Int, y: Int) {
        // Save the state, so that later events can use it
        savedMouseX = x.toFloat() / stage.scale
        savedMouseY = y.toFloat() / stage.scale

        if (mouseDownInStage == null) {
            val overlayOrRegular = stage.overlayAt(savedMouseX, savedMouseY) ?: stage
            val scene = overlayOrRegular.scene ?: return
            overlayOrRegular.onMouseMove(MouseEvent(EventType.MOUSE_MOVED, scene, savedMouseX, savedMouseY, savedMouseButtonIndex, 0, savedMods))
        } else {
            val scene = mouseDownInStage?.scene ?: return
            mouseDownInStage?.onMouseMove(MouseEvent(EventType.MOUSE_MOVED, scene, savedMouseX, savedMouseY, savedMouseButtonIndex, 0, savedMods))
        }
    }

    override fun mouseButton(buttonIndex: Int, pressed: Boolean, mods: Int) {

        val overlayOrRegular = if (mouseDownInStage?.capturedByNode != null) {
            // If a node has captured events, then the event must be redirected to that Stage's node.
            // This is important during a drag event, as the mouse up might not be within the overlay stage,
            // but the event still needs to be handled by the overlay stage.
            mouseDownInStage ?: return
        } else {
            // Otherwise, the button event belongs to the stage where the mouse currently is.
            stage.overlayAt(savedMouseX, savedMouseY) ?: stage
        }

        // Save the state, so that later events can use it
        savedMods = mods
        savedMouseButtonIndex = if (pressed) buttonIndex else - 1
        mouseDownInStage = if (pressed) overlayOrRegular else null

        val scene = overlayOrRegular.scene ?: return
        val eventType = if (pressed) EventType.MOUSE_PRESSED else EventType.MOUSE_RELEASED

        // Prevent interaction, as we tried to click outside a Modal OverlayStage.
        val lastOverlay = stage.regularStage.overlayStages.lastOrNull()
        if (lastOverlay != null && lastOverlay.stageType == StageType.MODAL && lastOverlay != overlayOrRegular) return

        stage.focusedStage = overlayOrRegular
        overlayOrRegular.onMouseButton(MouseEvent(eventType, scene, savedMouseX, savedMouseY, buttonIndex, 0, mods))

    }

    override fun scroll(deltaX: Float, deltaY: Float) {
        val overlayOrRegular = stage.overlayAt(savedMouseX, savedMouseY) ?: stage

        // Prevent interaction, if we tried to scroll outside a Modal OverlayStage.
        val lastOverlay = stage.regularStage.overlayStages.lastOrNull()
        if (lastOverlay != null && lastOverlay.stageType == StageType.MODAL && lastOverlay != overlayOrRegular) return

        overlayOrRegular.scene?.let { scene ->
            val event = if (GlokSettings.reverseScroll) {
                ScrollEvent(scene, savedMouseX, savedMouseY, deltaX, deltaY, savedMods)
            } else {
                ScrollEvent(scene, savedMouseX, savedMouseY, -deltaX, -deltaY, savedMods)
            }
            overlayOrRegular.onScroll(event)
        }
    }
}

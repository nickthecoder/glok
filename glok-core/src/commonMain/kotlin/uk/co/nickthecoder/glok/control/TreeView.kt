/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.ReadOnlyPropertyWrapper
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.GlokException
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min
import kotlin.math.ceil
import kotlin.math.floor

/**
 * Displays a hierarchical list of [TreeItem]s in a tree structure.
 * Each [TreeItem] can be a `leaf`, meaning it has no children.
 * If it is not a leaf, then its children can be shown/hidden by either clicking on
 * the "expand/contract" button (an arrow using Glok's default theme), or by double-clicking
 * anywhere on the item.
 *
 * This is most suitable when each [TreeItem] holds similar data, such as the hierarchical
 * view of a file system, where each [TreeItem] is related to a `File` (which may also be a folder).
 *
 * For trees with mixed types (e.g. the composition of a document, which may contain text,
 * images, layers, guides, meta-data ...), consider using [MixedTreeView] instead.
 *
 * [T] is the type of data held by a single [TreeItem]. e.g. `File` in the example above.
 */
 // NOTE, ListView, TreeView and MixedTreeView are all quite similar, but share little code.
 // Not using the DRY principle (Don't Repeat Yourself).
 // We are WET (Write Everything Thrice).
 // This was deliberate, as the cure may well be worse than the disease!
// Changes to one (especially bug fixes) should be applied to the others.
class TreeView<T> : Region() {

    // region ==== Properties ====

    val rootProperty: Property<TreeItem<T>?> = SimpleProperty(null)
    var root by rootProperty

    val showRootProperty by booleanProperty(true)
    var showRoot by showRootProperty

    /**
     * At a later date, TreeView may support variable size cells, and a negative number would indicate
     * that each cell has its own custom height.
     * But for now, this should always be set to greater than zero (either directly in code, or via the scene's [Theme]).
     */
    val fixedRowHeightProperty by stylableFloatProperty(0f)
    var fixedRowHeight by fixedRowHeightProperty

    val indentationWidthProperty by stylableFloatProperty(16f)
    var indentationWidth by indentationWidthProperty

    // endregion Properties

    // region ==== Fields ====
    /**
     * A Flattened version of the tree structure which is not expanded.
     * i.e. would be visible if scrolled.
     *
     * NOTE. This list always contains the [root] TreeItem, even when [showRoot] == false.
     */
    private val flattenedItems = mutableListOf<TreeItem<T>>().asMutableObservableList()

    val selection = SingleSelectionModel(flattenedItems)

    private val mutableSelectedValueProperty: Property<T?> = SimpleProperty<T?>(null)
    val selectedValueProperty: ObservableValue<T?> = ReadOnlyPropertyWrapper(mutableSelectedValueProperty)

    /**
     * The value of the currently selected item.
     */
    var selectedValue by mutableSelectedValueProperty
        private set

    var cellFactory: (TreeView<T>, TreeItem<T>) -> TreeCell<T> = { treeView, item -> TextTreeCell<T>(treeView, item) }
        set(v) {
            field = v
            requestRedraw()
        }

    private val content = Content()

    private val scrollPane = ScrollPane(content)

    override val children = listOf(scrollPane).asObservableList()

    // endregion fields

    // region ==== Commands ====
    val commands = Commands().apply {
        with(TreeViewActions) {
            SELECT_UP { selectUp() }
            SELECT_DOWN { selectDown() }
            EXPAND { expandSelected(true) }
            CONTRACT { expandSelected(false) }
        }
        attachTo(this@TreeView)
    }
    // endregion commands

    init {
        style(TREE_VIEW)
        claimChildren()

        selection.selectedItemProperty.addChangeListener { _, _, selectedTreeItem ->
            selectedValue = selectedTreeItem?.value
            requestLayout()
        }

        showRootProperty.addChangeListener { _, _, showRoot ->
            if (showRoot && flattenedItems.firstOrNull() !== root) {
                root?.let { flattenedItems.add(0, it) }
            } else {
                if (root != null && flattenedItems.firstOrNull() === root) {
                    flattenedItems.removeAt(0)
                }
            }
            content.requestLayout()
        }

        rootProperty.addChangeListener { _, oldRoot, newRoot ->
            oldRoot?.treeView = null
            flattenedItems.clear()
            newRoot?.let {
                it.level = 0
                it.parent = null
                it.treeView = this

                var nextIndex = 0
                if (showRoot) {
                    flattenedItems.add(newRoot)
                    nextIndex ++
                }

                if (newRoot.expanded) {
                    for (child in newRoot.children) {
                        nextIndex = addFlattenedItem(child, nextIndex)
                    }
                }
            }
            requestLayout()
        }


        onMousePressed {
            requestFocus()
        }
        scrollPane.commands.attachTo(this)
    }

    // region ==== Methods ====

    private fun expandSelected(value: Boolean) {
        selection.selectedItem?.let { selectedItem ->
            if (! selectedItem.leaf) {
                selectedItem.expanded = value
            }
        }
    }

    private fun selectUp() {
        val index = selection.selectedIndex
        if (index > 0) {
            selection.selectedIndex --
        }
        scrollTo(selection.selectedIndex)
    }

    private fun selectDown() {
        val index = selection.selectedIndex
        if (index < flattenedItems.size - 1) {
            selection.selectedIndex ++
        }
        scrollTo(selection.selectedIndex)
    }

    fun scrollTo(row: Int) {
        content.scrollTo(row)
    }

    fun scrollTo(treeItem: TreeItem<T>) {
        val index = flattenedItems.indexOf(treeItem)
        if (index >= 0) {
            scrollTo(index)
        }
    }

    /**
     * Expands all ancestor TreeItems, and repositions the vertical scroll bar, if needed,
     * so that [item] is _available_, and then scroll the tree to make it _visible_.
     */
    fun ensureVisible(item: TreeItem<T>) {
        var parent = item.parent
        while (parent != null) {
            parent.expanded = true
            parent = parent.parent
        }
        scrollTo(item)
    }

    internal fun expand(parent: TreeItem<T>): Int {

        fun expand(parent: TreeItem<T>, parentIndex: Int): Int {
            var nextIndex = parentIndex + 1
            for (child in parent.children) {
                flattenedItems.add(nextIndex, child)
                if (child.expanded) {
                    nextIndex = expand(child, nextIndex)
                } else {
                    nextIndex++
                }
            }
            return nextIndex
        }

        val parentIndex = flattenedItems.indexOf(parent)
        if (parentIndex < 0) throw GlokException("Attempted to add a child TreeItem, whose parent is not this TreeView")

        return expand(parent, parentIndex)
    }

    internal fun contract(parent: TreeItem<T>) {
        if (! isPartOfTree(parent)) return

        val parentIndex = flattenedItems.indexOf(parent)
        if (parentIndex < 0) throw GlokException("Attempted to contract a TreeItem not found : $parent")

        for (index in parentIndex + 1 until flattenedItems.size) {
            val otherItem = flattenedItems[parentIndex + 1]
            if (! otherItem.isDescendantOf(parent)) {
                break
            }
            flattenedItems.removeAt(parentIndex + 1)
        }
    }


    internal fun removed(item: TreeItem<T>) {
        if (item.expanded) {
            // Remove the child nodes from the flattened structure first.
            item.expanded = false
        }
        flattenedItems.remove(item)

        content.requestLayout()
    }


    private fun isPartOfTree(item: TreeItem<*>?): Boolean {

        var up = item
        while (up != null) {
            if (up === root) return true
            up = up.parent
        }
        println("$item is not part of the tree")
        return false
    }

    /**
     * Add [item] to [flattenedItems].
     *
     * To find the correct index...
     * Scan the [flattenedItems] starting at the [parent] item all the way to the end.
     * If we find a later sibling, then we can stop, and add before it (using its index into [flattenedItems]).
     * If we find an earlier sibling, keep going.
     * If we are no longer at a sibling, or a sibling's descendant, then we can stop.
     * If we reach the end, then add to the end.
     */
    internal fun added(item: TreeItem<T>, parent: TreeItem<T>?) {
        if (! isPartOfTree(item)) return

        fun findIndex(parent: TreeItem<T>): Int {

            val localChildIndex = parent.children.indexOf(item)
            val flattenedParentIndex = flattenedItems.indexOf(parent)

            if (localChildIndex < 0) throw GlokException("Item not found in the parent TreeItem")

            // There's an edge case : If the parent is the root node, and the root is hidden.
            // In this case, flattenedParentIndex == -1
            // By a happy coincidence, a value of -1 is exactly what we want for the remaining code to work!
            val parentIsHiddenRoot = parent === root && ! showRoot
            if (! parentIsHiddenRoot && flattenedParentIndex < 0) {
                throw GlokException("Attempted to add a child TreeItem, whose parent is not this TreeView")
            }

            // Adding a first-child is easy.
            if (localChildIndex == 0) {
                return flattenedParentIndex + 1
            }

            // Scan the flattenedItems, from just after our parent to the end of the list.
            for (index in flattenedParentIndex + 1 until flattenedItems.size) {
                val otherItem = flattenedItems[index]
                if (otherItem.parent === parent) {
                    // Found a sibling
                    if (parent.children.indexOf(otherItem) > localChildIndex) {
                        // Found a later sibling. Add here.
                        return index
                    }
                } else {
                    if (otherItem.level <= parent.level) {
                        // No longer in our siblings or their descendants.
                        return index
                    }
                }
            }
            return flattenedItems.size
        }

        if (item === root) {
            flattenedItems.add(0, item)
            content.requestLayout()
        } else {
            if (parent == null) {
                throw GlokException("Attempted to add a MixedTreeItem, with parent==null, which is not the root node")
            } else {
                flattenedItems.add(findIndex(parent), item)
                content.requestLayout()
            }
        }
    }


    private fun addFlattenedItem(item: TreeItem<T>, index: Int): Int {
        flattenedItems.add(index, item)

        var nextIndex = index + 1
        if (item.expanded) {
            nextIndex = expand(item)
        }
        return nextIndex
    }

    // endregion

    // region ==== layout ====

    /**
     * By default, TreeViews have an arbitrary prefWidth. Set [evalPrefWidth], or add the TreeView to a layout control
     * which will allocate a sensible area regardless of [evalPrefWidth].
     */
    override fun nodePrefWidth() = 200f

    /**
     * By default, TreeViews have an arbitrary prefHeight. Set [evalPrefHeight], or add the TreeView to a layout control
     * which will allocate a sensible area regardless of [evalPrefWidth].
     */
    override fun nodePrefHeight() = 80f

    override fun layoutChildren() {
        setChildBounds(scrollPane, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    // endregion

    // region == inner class Content ==
    private inner class Content : Region() {

        override val children = mutableListOf<TreeRow>().asMutableObservableList()
        private val rowsListener = children.addChangeListener { list, changes ->
            childrenListener.changed(list, changes)
        }

        init {
            fixedRowHeightProperty.addListener(requestLayoutListener)
        }

        fun scrollTo(row: Int) {
            val viewport = parent ?: return
            val cellHeight = rowHeight()
            val rowsVisible = ceil(viewport.height / cellHeight).toInt()
            val totalRows = flattenedItems.size

            // If all rows are visible, then do nothing.
            if (rowsVisible >= totalRows) return

            // Is it already in view? Note, there are issues with rounding here!
            if (row > firstVisibleItemIndex + 1 && row < firstVisibleItemIndex - 1 + rowsVisible) return

            // We are trying to make "row" appear in the middle of the viewport.
            val firstRow = (row - rowsVisible / 2).clamp(0, totalRows - 1)
            scrollPane.vScrollValue = firstRow * cellHeight
            requestLayout()
        }

        // region === layout ===

        override fun nodePrefWidth() = 10f
        override fun nodePrefHeight() = flattenedItems.size * rowHeight()

        /**
         * The index of the first visible item within the [flattenedItems].
         */
        private var firstVisibleItemIndex = 0

        // Bad style. This is a side effect used during layout()
        private var madeNewRow = false

        private fun rowHeight() = if (fixedRowHeight == 0f) {
            children.firstOrNull()?.evalPrefHeight()
                ?: if (flattenedItems.isEmpty() || (! showRoot && flattenedItems.size == 1)) {
                    30f // It doesn't matter what size we return! There are no rows.
                } else {
                    firstVisibleItemIndex = min(firstVisibleItemIndex, flattenedItems.size - 1)
                    val row = findOrCreateRow(flattenedItems[firstVisibleItemIndex])
                    children.add(row)
                    row.evalPrefHeight()
                }
        } else {
            fixedRowHeight
        }

        private fun findOrCreateRow(treeItem: TreeItem<T>): TreeRow {
            val existing = children.firstOrNull { it.treeCell.treeItem === treeItem }
            if (existing != null) return existing

            val cell = cellFactory(this@TreeView, treeItem)
            madeNewRow = true
            return TreeRow(cell)
        }

        override fun layoutChildren() {
            madeNewRow = false

            val viewport = parent ?: return
            val rowHeight = rowHeight()

            firstVisibleItemIndex = floor(- localY / rowHeight).toInt()

            val spaceForRows = ceil(viewport.height / rowHeight).toInt() + 1

            // The number of cells we need to render.
            // This is either limited by the height of the viewport, or the number of items.
            val rowsRequired = min(spaceForRows, flattenedItems.size - firstVisibleItemIndex)

            val w = width - surroundX()
            val x = surroundLeft()
            var y = surroundTop() + firstVisibleItemIndex * rowHeight

            val newChildren = mutableListOf<TreeRow>()
            for (itemsIndex in firstVisibleItemIndex until firstVisibleItemIndex + rowsRequired) {
                val treeItem = flattenedItems[itemsIndex]
                val row = findOrCreateRow(treeItem)
                newChildren.add(row)

                setChildBounds(row, x, y, w, rowHeight)
                y += rowHeight
            }

            if (madeNewRow || children.size != newChildren.size) {
                // We made changes, so replace children.
                children.clear()
                children.addAll(newChildren)
            }
        }
        // endregion == layout ==

    }
    // endregion inner class Content

    // region == inner class CellRow ==

    private inner class TreeRow(val treeCell: TreeCell<T>) : Region() {

        val arrowButton = Button("").apply {
            style(ARROW)
            disabledProperty.bindTo(treeCell.leafProperty)
            onAction { toggleExpanded() }
        }

        override val children = listOf(arrowButton, treeCell).asObservableList()

        private val selectionListener = selection.selectedItemProperty.addChangeListener { _, _, selectedItem ->
            pseudoStyleIf(selectedItem === treeCell.treeItem, SELECTED)
        }

        private val expandedListener = treeCell.expandedProperty.addChangeListener { _, _, expanded ->
            pseudoStyleIf(expanded, EXPANDED)
        }

        private val leafListener = treeCell.leafProperty.addChangeListener { _, _, leaf ->
            pseudoStyleIf(leaf, LEAF)
        }

        init {
            style(TREE_ROW)
            pseudoStyleIf(selection.selectedItem === treeCell.treeItem, SELECTED)
            pseudoStyleIf(treeCell.expandedProperty.value, EXPANDED)
            pseudoStyleIf(treeCell.leafProperty.value, LEAF)

            claimChildren()
            onMouseClicked { event ->
                if (event.isPrimary && event.clickCount == 2) {
                    toggleExpanded()
                    event.consume()
                }
            }
        }

        private fun toggleExpanded() {
            val item = treeCell.treeItem
            selection.selectedItem = item

            if (!item.expanded) {
                item.onExpanding?.handle(ActionEvent())
            }

            item.expanded = !item.expanded

            if (!item.expanded) {
                item.onContracted?.handle(ActionEvent())
            }
        }

        override fun nodePrefWidth() = treeCell.evalPrefWidth() + surroundX()
        override fun nodePrefHeight() = max(treeCell.evalPrefHeight(), arrowButton.evalPrefHeight()) + surroundY()

        override fun layoutChildren() {
            var indent = treeCell.treeItem.level * indentationWidth
            if (!showRoot) indent -= indentationWidth

            var x = surroundLeft() + indent
            val y = surroundTop()
            val availableY = height - surroundY()
            val availableX = width - surroundX() - indent

            val arrowHeight = arrowButton.evalPrefHeight()
            val arrowWidth = arrowButton.evalPrefWidth()
            setChildBounds(arrowButton, x, y + (availableY - arrowHeight) / 2f, arrowWidth, arrowHeight)

            x += arrowWidth
            val cellHeight = treeCell.evalPrefHeight()
            setChildBounds(treeCell, x, y + (availableY - cellHeight) / 2f, availableX - arrowWidth, cellHeight)
        }
    }

    // endregion
}

/**
 * Actions for [TreeView] and [MixedTreeView].
 */
object TreeViewActions : Actions(null) {

    val SELECT_UP = define("up", "Up", Key.UP.noMods())
    val SELECT_DOWN = define("down", "Down", Key.DOWN.noMods())

    val EXPAND = define("expand", "Expand", Key.DOWN.control()) {
        this.additionalKeyCombinations.add(Key.RIGHT.control())
    }
    val CONTRACT = define("contract", "Contract", Key.UP.control()) {
        this.additionalKeyCombinations.add(Key.LEFT.control())
    }

}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.functions.or

/**
 * The base class for `Spinners`, whose values are comparable, and have a [min] and [max] value.
 */
abstract class ComparableSpinner<V : Comparable<V>, P : Property<V>> : SpinnerBase<V, P>() {

    abstract val minProperty: P
    abstract val min: V

    abstract val maxProperty: P
    abstract val max: V

    val cycleProperty by booleanProperty(false)
    var cycle by cycleProperty

    private val extraCommands = Commands().apply {
        with(SpinnerActions) {
            MINIMUM { if (! readOnly) value = min }
            MAXIMUM { if (! readOnly) value = max }
        }
        attachTo(this@ComparableSpinner)
    }

    override fun isValid(newValue: V) = newValue in min..max

    override fun initialise() {
        super.initialise()
        downButton.disabledProperty.unbind()
        upButton.disabledProperty.unbind()
        downButton.disabledProperty.bindTo((valueProperty.equalTo(minProperty) and ! cycleProperty) or readOnlyProperty)
        upButton.disabledProperty.bindTo((valueProperty.equalTo(maxProperty) and ! cycleProperty) or readOnlyProperty)
    }

    override fun adjust(direction: Int, byLargeStep: Boolean) {
        updateValue()
        val new = adjustment(direction, byLargeStep)
        value = if (new > max) {
            if (cycle) min else max
        } else if (new < min) {
            if (cycle) max else min
        } else {
            new
        }
    }
}


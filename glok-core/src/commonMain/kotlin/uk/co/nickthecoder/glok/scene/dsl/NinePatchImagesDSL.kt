package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.backend.Resources
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.NinePatch
import uk.co.nickthecoder.glok.scene.NinePatchImages

/**
 * Part of a DSL (Domain specific language) for loading nine patches.
 * Example (taken from DemoNinePatch) :
 *
 *     val ninePatches = ninePatchImages(demoResources, "demoButtons.png") {
 *         ninePatch("normal", 1, 1, 98, 58, Edges(15f))
 *         ninePatch("hover", 101, 1, 98, 58, Edges(15f))
 *         ninePatch("armed", 201, 1, 98, 58, Edges(15f))
 *         ninePatch("selected", 301, 1, 98, 58, Edges(15f))
 *     }
 *
 * In this example, all nine-patches are the same size, and have the same `edges`,
 * i.e. all the corner patches are 15x15 pixels.
 *
 * You can retrieve one of the nine-patches like so :
 *
 *     val hoverNinePatch = ninePatches["hover"]
 */
fun ninePatchImages(resources: Resources, textureName: String, block: NinePatchImages.() -> Unit): NinePatchImages {
    val npb = NinePatchImages(resources, textureName)
    npb.block()
    return npb
}

/**
 * Part of a DSL (Domain specific language) for loading nine patches.
 * See [ninePatchImages] for details.
 */
fun NinePatchImages.ninePatch(name: String, x: Number, y: Number, width: Number, height: Number, edges: Edges) {
    val image = texture.partialImage(x.toFloat(), y.toFloat(), width.toFloat(), height.toFloat())
    val ninePatch = NinePatch(image, edges)
    this[name] = ninePatch
}

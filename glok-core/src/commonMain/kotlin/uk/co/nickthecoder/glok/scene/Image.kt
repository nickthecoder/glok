package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.TextureBatch
import uk.co.nickthecoder.glok.property.boilerplate.IntProperty
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalImage
import uk.co.nickthecoder.glok.property.boilerplate.OptionalImageUnaryFunction

interface Image {

    val imageWidth: Float
    val imageHeight: Float

    fun batch(tint: Color?, block: TextureBatch.() -> Unit)

    /**
     * @param tint Default=null. An optional [Color], which is multiplied by the image's color.
     * If RGB are all 1, and alpha is not 1, this has the effect of making the image
     * transparent.
     *
     * Using RBG values which are not 1 is generally only used when the source image is greyscale.
     */
    fun draw(x: Float, y: Float, tint: Color? = null)

    fun drawBatched(batch: TextureBatch, x: Float, y: Float)

    /**
     * @param tint Default=null. An optional [Color], which is multiplied by the image's color.
     * If RGB are all 1, and alpha is not 1, this has the effect of making the image
     * transparent.
     *
     * Using RBG values which are not 1 is generally only used when the source image is greyscale.
     */
    fun drawTo(x: Float, y: Float, destWidth: Float, destHeight: Float, tint: Color? = null)

    /**
     * The same as the other [drawTo], but using a [TextureBatch] for efficiency.
     *
     * [batch] is obtained from `this`.[batch]()
     */
    fun drawToBatched(batch: TextureBatch, x: Float, y: Float, destWidth: Float, destHeight: Float)

    /**
     * The ratio of the [imageWidth] to the [imageHeight].
     */
    fun aspectRatio() = imageWidth / imageHeight

    /**
     * Takes a rectangular region of this [Image], and returns it as an [Image].
     * Note, the pixel data is NOT copied
     */
    fun partialImage(fromX: Float, fromY: Float, width: Float, height: Float): Image

    /**
     * Returns an [Image] using the same pixel data as this image, but it lies about its actual size.
     *
     * e.g. A 64x64 pixel image will still be 64x64, but will report that it is 32x32, and will render
     * as 32x32 (i.e. the scaling is done which rendering, not as part of this method call).
     *
     * This is particularly useful for icons and other small graphics, which would become pixelated
     * if we _actually_ scaled them. On a high-DPI display the full resolution of the image will be used,
     * and only on regular (low DPI) displays will the image become pixelated.
     */
    fun scaledBy(scaleX: Float, scaleY: Float = scaleX): Image

    /**
     * Returns a scaled version of this image, scaled such that width and height are at most [size].
     * This is often used for square images, icons in particular.
     */
    fun scaleTo(size: Int): Image =
        if (imageWidth > imageHeight) {
            this.scaledBy(size / imageWidth)
        } else {
            this.scaledBy(size / imageHeight)
        }

    /**
     * Returns an [ObservableOptionalImage] whose value is a scaled version of this image,
     * scaled such that width and height are at most [sizeProperty].value.
     *
     * This is often used for square images, icons in particular, where the result is bound to
     * [ImageView.imageProperty]
     *
     * This allows us to build the GUI, and change the size of all icons which use [sizeProperty]
     * just by changing its value.
     */
    fun resizable(sizeProperty: IntProperty): ObservableOptionalImage = OptionalImageUnaryFunction(sizeProperty) { size ->
        this.scaleTo(size)
    }

}

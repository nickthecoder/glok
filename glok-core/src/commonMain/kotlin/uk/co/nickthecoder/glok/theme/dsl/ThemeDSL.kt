package uk.co.nickthecoder.glok.theme.dsl

import uk.co.nickthecoder.glok.scene.WithPseudoStyle
import uk.co.nickthecoder.glok.theme.*


/**
 * The starting point for the [Theme] DSL (Domain Specific Language).
 */
fun theme(block: ThemeDSL.() -> Unit): Theme {

    ThemeDSL().apply {
        block()
        return theme
    }

}

/**
 * DSL (Domain Specific Language) helper functions for building [Theme]s.
 *
 * Adds syntax to define top-level rules in the form :
 *
 *     SELECTOR { ... }
 *
 * Where `SELECTOR` is either a `String`, a [WithPseudoStyle] or a [Selector].
 *
 * `...` is a block of code with a [RuleDSL] as the receiver.
 */
class ThemeDSL {

    val theme = Theme()

    val root = RootSelector

    /**
     * Allows a string to be used as a selector for a rule.
     * The type of selector depends on the string's contents.
     * If it begins with a colon, then it uses [PseudoStyleSelector],
     * otherwise it uses [StyleSelector]. e.g.
     *
     *     "tool_bar" { ... }   // A StyleSelector
     *
     *     ":selected" { ... }  // A PseudoStyleSelector
     */
    operator fun String.invoke(block: RuleDSL.() -> Unit): RuleDSL {
        return RuleDSL(addSection(this.toSelector(), block))
    }

    /**
     * Allows us to use enums (which implement WithPseudoStyle) as selectors. e.g.
     *
     *     Side.TOP { ... }
     */
    operator fun WithPseudoStyle.invoke(block: RuleDSL.() -> Unit): RuleDSL {
        return RuleDSL(addSection(PseudoStyleSelector(this.pseudoStyle), block))
    }

    /**
     * For example :
     *
     *     ("tool_bar" or "split_pane") { ... }
     *
     * The "or" function creates an [OrSelector]. The rule in curly brackets is being applied
     * by using the `invoke` operator of the [OrSelector].
     */
    operator fun Selector.invoke(block: RuleDSL.() -> Unit): RuleDSL {
        return RuleDSL(addSection(this, block))
    }

    private fun addSection(selector: Selector, block: RuleDSL.() -> Unit): Theme.Rule {
        val ruleDSL = RuleDSL(theme.Rule(selector))
        theme.rules.add(ruleDSL.rule)
        ruleDSL.block()
        return ruleDSL.rule
    }

}

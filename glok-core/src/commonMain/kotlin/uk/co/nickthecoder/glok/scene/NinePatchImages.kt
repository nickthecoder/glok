package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.Resources
import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.glok.scene.dsl.ninePatchImages

/**
 * Stores a map of [NinePatch]es keyed on name (as String), loaded from [Resources].
 *
 * See also : [ninePatchImages].
 */
class NinePatchImages(val resources: Resources, private val textureName: String) {

    val texture: Texture by lazy {
        resources.loadTexture(textureName)
    }

    private val ninePatchMap = mutableMapOf<String, NinePatch>()

    operator fun set(name: String, ninePatch: NinePatch) {
        ninePatchMap[name] = ninePatch
    }

    operator fun get(name: String) = ninePatchMap[name]

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.PlainBackground
import uk.co.nickthecoder.glok.scene.PlainBorder
import uk.co.nickthecoder.glok.util.StringConverter

fun ObservableColor.contrasting(dark: ObservableColor, light: ObservableColor): ObservableColor = ColorTernaryFunction(this, dark, light) { me, d, l -> if (me.red >= 0.5f || me.green >= 0.5f || me.blue >= 0.5f) d else l }
fun ObservableColor.contrasting(dark: Color, light: Color): ObservableColor = ColorUnaryFunction(this) { me -> if (me.toHSL()[2] > 0.5f) dark else light }

fun ObservableColor.red(): ObservableFloat = FloatUnaryFunction(this) { c -> c.red }
fun ObservableColor.green(): ObservableFloat = FloatUnaryFunction(this) { c -> c.green }
fun ObservableColor.blue(): ObservableFloat = FloatUnaryFunction(this) { c -> c.blue }
fun ObservableColor.alpha(): ObservableFloat = FloatUnaryFunction(this) { c -> c.alpha }

fun ObservableColor.hue(): ObservableFloat = FloatUnaryFunction(this) { c -> c.toHCV()[0] }
fun ObservableColor.saturation(): ObservableFloat = FloatUnaryFunction(this) { c -> c.toHSV()[1] }
fun ObservableColor.brightness(): ObservableFloat = FloatUnaryFunction(this) { c -> c.toHSV()[2] }
fun ObservableColor.lightness(): ObservableFloat = FloatUnaryFunction(this) { c -> c.toHSL()[2] }

fun ObservableColor.withRed(value: Float): ObservableColor = ColorUnaryFunction(this) { c -> c.withRed(value) }
fun ObservableColor.withGreen(value: Float): ObservableColor = ColorUnaryFunction(this) { c -> c.withGreen(value) }
fun ObservableColor.withBlue(value: Float): ObservableColor = ColorUnaryFunction(this) { c -> c.withBlue(value) }
fun ObservableColor.withAlpha(value: Float): ObservableColor = ColorUnaryFunction(this) { c -> c.withAlpha(value) }
fun ObservableColor.opaque(): ObservableColor = ColorUnaryFunction(this) { c -> c.withAlpha(1f) }
fun ObservableColor.transparent(): ObservableColor = ColorUnaryFunction(this) { c -> c.withAlpha(0f) }

fun Color.withRed(other: ObservableFloat): ObservableColor = ColorUnaryFunction(other) { o -> this@withRed.withRed(o) }
fun Color.withGreen(other: ObservableFloat): ObservableColor = ColorUnaryFunction(other) { o -> this@withGreen.withGreen(o) }
fun Color.withBlue(other: ObservableFloat): ObservableColor = ColorUnaryFunction(other) { o -> this@withBlue.withBlue(o) }
fun Color.withAlpha(other: ObservableFloat): ObservableColor = ColorUnaryFunction(other) { o -> this@withAlpha.withAlpha(o) }

fun ObservableColor.withRed(other: ObservableFloat): ObservableColor = ColorBinaryFunction(this, other) { c, o -> c.withRed(o) }
fun ObservableColor.withGreen(other: ObservableFloat): ObservableColor = ColorBinaryFunction(this, other) { c, o -> c.withGreen(o) }
fun ObservableColor.withBlue(other: ObservableFloat): ObservableColor = ColorBinaryFunction(this, other) { c, o -> c.withBlue(o) }
fun ObservableColor.withAlpha(other: ObservableFloat): ObservableColor = ColorBinaryFunction(this, other) { c, o -> c.withAlpha(o) }

fun ObservableColor.tint(tint: Color): ObservableColor = ColorUnaryFunction(this) { c -> c.tint(tint) }

fun ObservableColor.redOnly(): ObservableColor = ColorUnaryFunction(this) { c -> Color(c.red, 0f, 0f, 1f) }
fun ObservableColor.greenOnly(): ObservableColor = ColorUnaryFunction(this) { c -> Color(0f, c.green, 0f, 1f) }
fun ObservableColor.blueOnly(): ObservableColor = ColorUnaryFunction(this) { c -> Color(0f, 0f, c.blue, 1f) }
fun ObservableColor.alphaOnly(): ObservableColor = ColorUnaryFunction(this) { c -> Color(0f, 0f, 0f, c.alpha) }

fun ObservableColor.lerp(other: ObservableColor, ot: ObservableFloat): ObservableColor = ColorTernaryFunction(this, other, ot) { me, o, t -> me.lerp(o, t) }
fun ObservableColor.lerp(other: ObservableColor, ot: ObservableDouble): ObservableColor = ColorTernaryFunction(this, other, ot) { me, o, t -> me.lerp(o, t.toFloat()) }

fun ObservableColor.lerp(other: ObservableColor, t: Float): ObservableColor = ColorBinaryFunction(this, other) { me, o -> me.lerp(o, t) }
fun ObservableColor.lerp(other: ObservableColor, t: Double): ObservableColor = ColorBinaryFunction(this, other) { me, o -> me.lerp(o, t.toFloat()) }

fun ObservableColor.toHashRGB(): ObservableString = StringUnaryFunction(this) { me -> me.toHashRGB() }
fun ObservableColor.toHashRGBA(): ObservableString = StringUnaryFunction(this) { me -> me.toHashRGBA() }

/**
 * Converts a [Color] to/from a String. The color may have any value for [Color.alpha].
 * If you want to ensure the alpha value is 1, then use [opaqueColorStringConverter] instead.
 */
fun colorStringConverter(default: ObservableValue<Color>) = object : StringConverter<Color> {
    override fun toString(value: Color) = value.toHashRGBA()
    override fun fromString(str: String) = Color.find(str) ?: default.value
}

fun opaqueColorStringConverter(default: ObservableValue<Color>) = object : StringConverter<Color> {
    override fun toString(value: Color) = value.toHashRGB()

    override fun fromString(str: String): Color {
        val color = Color.find(str) ?: default.value
        if (color.alpha != 1f) return color.withAlpha(1f)
        return color
    }
}


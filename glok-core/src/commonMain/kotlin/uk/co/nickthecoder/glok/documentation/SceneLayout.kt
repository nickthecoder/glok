package uk.co.nickthecoder.glok.documentation

import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Scene
import uk.co.nickthecoder.glok.scene.Stage

/**
 * This interface is _NOT USED_, it is only here for [Documentation].
 *
 * This document describes how position and sizes of [Node]s are calculated.
 *
 * The [Scene]'s [Scene.root] Node always takes up the entire area of the [Stage].
 * So when a window is resized, so is the root node.
 *
 * All other nodes' size and positions are determined by their [Node.parent] node,
 * in [Node.layoutChildren].
 * The child node gives _hints_ to its parent via these six 6 methods :
 *
 * * [Node.nodeMinWidth], [Node.nodePrefWidth], [Node.nodeMaxWidth]
 * * [Node.nodeMinHeight], [Node.nodePrefHeight], [Node.nodeMaxHeight]
 *
 * These may be overruled by explicitly setting any of the 6 variables :
 *
 * * [Node.overrideMinWidth], [Node.overridePrefWidth], [Node.overrideMaxWidth]
 * * [Node.overrideMinHeight], [Node.overridePrefHeight], [Node.overrideMaxHeight]
 *
 * In most cases they are all `null` (setting explicit sizes is generally a bad idea).
 *
 * These values are only _hints_, and the parent may sometimes ignore them.
 * For example, if we have one tall and one short node in a [HBox] with [HBox.fillHeight] = `true,
 * both will be assigned the same height.
 * The minHeight and prefHeight of the short child is ignored.
 */
interface SceneLayout : Documentation

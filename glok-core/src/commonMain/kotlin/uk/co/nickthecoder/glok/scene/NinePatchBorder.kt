package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.control.Region

/**
 * A [Border] which uses a [NinePatch].
 *
 * Note, the `size` of the border passed into [draw] is ignored.
 */
class NinePatchBorder(val ninePatch: NinePatch) : Border {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        ninePatch.draw(x, y, width, height, false)
    }

}

/**
 * Similar to [NinePatchBorder], but the source texture's pixels are multiplied
 * by a color (usually from [Region.borderColor]).
 *
 * Used in two ways :
 *
 * 1. Making the border semi-transparent by using only the alpha channel of [tint]
 * 2. Using the RGB channels of [tint] on a greyscale nine-patch.
 */
class TintedNinePatchBorder(val ninePatch: NinePatch) : Border {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        ninePatch.draw(x, y, width, height, false, color)
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.sumOf

// Functions, and a single class, which greatly help calculate the required sizees
// for child nodes in box-like containers.
// These were first used in HBox and VBox.
// BorderPane also uses them.


/**
 * For each child, we collect its preferred size, as well as the limiting size.
 * The limiting size will be `minWidth` when [difference] < 0, and `maxWidth` when [difference] > 0.
 * If difference == 0, then no calculations are needed, and we short-circuit and use the prefWidth immediately.
 * We then call either [shrink] or [grow] which apportions the extra space according to each child's
 * [Node.growPriority] or [Node.shrinkPriority].
 *
 * It is then up to the parent to iterate over the results, and call
 * [Node.setChildBounds].
 *
 * @param children The child nodes that we wish to lay out
 * @param difference The amount of additional space which needs to be apportioned to [children].
 *
 * * Positive when there is extra space available : children may be given more space than their pref.
 * * Negative when there is too little space available : children will be given less space than their pref.
 * * Zero : children will be given exactly their pref size.
 *
 * NOTE, It is up to the parent to include spacing/margins when calculating [difference].
 *
 */
internal fun calculateChildWidths(children: Iterable<Node>, difference: Float): Pair<List<BoxNodeData>, Float> {
    var remainder = 0f

    val boxNodeData = if (difference < 0) {
        children.map { BoxNodeData(it, it.shrinkPriority, it.evalPrefWidth(), it.evalMinWidth()) }
            .also {
                shrink(it, -difference)
            }
    } else if (difference > 0) {
        children.map { BoxNodeData(it, it.growPriority, it.evalPrefWidth(), it.evalMaxWidth()) }
            .also {
                remainder = grow(it, difference)
            }
    } else {
        children.map { BoxNodeData(it, 0f, 0f, 0f, it.evalPrefWidth()) }
    }
    return Pair(boxNodeData, remainder)
}

/**
 * See [calculateChildWidths] for details.
 *
 * @return first == true if all available space has been used.
 */
internal fun calculateChildHeights(children: Iterable<Node>, difference: Float): Pair<List<BoxNodeData>, Float> {
    var remainder = 0f
    val boxNodeData = if (difference < 0) {
        children.map { BoxNodeData(it, it.shrinkPriority, it.evalPrefHeight(), it.evalMinHeight()) }
            .also {
                shrink(it, -difference)
            }
    } else if (difference > 0) {
        children.map { BoxNodeData(it, it.growPriority, it.evalPrefHeight(), it.evalMaxHeight()) }
            .also {
                remainder = grow(it, difference)
            }
    } else {
        children.map { BoxNodeData(it, 0f, 0f, 0f, it.evalPrefHeight()) }
    }
    return Pair(boxNodeData, remainder)
}


/**
 * Holds data about 1 child node when laying out [HBox], [VBox] and possibly other box-like [Node]s.
 *
 * We perform identical calculations for widths and heights.
 * We also perform similar calculation when growing / shrinking.
 *
 * Rather than duplicating 4 times (2x2)
 */
internal data class BoxNodeData(
    val child: Node,
    /**
     * Either [Node.shrinkPriority] or [Node.growPriority]
     */
    var priority: Float,
    /**
     * Initially, this will be the [child]'s [Node.evalPrefWidth]/[Node.evalPrefHeight]
     */
    var pref: Float,
    /**
     * Either the min/max width/height
     */
    val limit: Float,

    /**
     * The width/height that will be allocated to [child]
     */
    var result: Float = 0f

) {
    override fun toString() = "Pri $priority Pref $pref Lim $limit *$result*"
}


/**
 * See [BoxNodeData].
 *
 * @return The remainder that couldn't be filled.
 */
internal fun grow(calculations: Iterable<BoxNodeData>, totalExtra: Float): Float {
    val prioritySum = calculations.sumOf { it.child.growPriority }
    var remaining = totalExtra
    for (cal in calculations) {
        if (cal.priority == 0f) {
            cal.result = cal.pref
        } else {
            val extra = totalExtra * cal.priority / prioritySum
            val maxExtra = cal.limit - cal.pref
            if (extra > maxExtra) {
                cal.result = cal.limit
                cal.priority = 0f
                remaining -= maxExtra
            } else {
                cal.result = cal.pref + extra
                remaining -= extra
            }
        }
    }
    // At this point, [remaining] will often still be positive, due to a child hitting its max limit.
    // So we do another pass, giving as much as possible to each node in turn with a growPriority != 0,
    // until [remaining] = 0
    if (remaining > 0) {
        for (cal in calculations) {
            if (cal.priority != 0f) {
                val maxExtra = cal.limit - cal.result
                if (remaining > maxExtra) {
                    cal.result = cal.limit
                    remaining -= maxExtra
                } else {
                    cal.result += remaining
                    return 0f
                }
            }
        }
    }
    return remaining
}

/**
 * See [BoxNodeData].
 * FYI, I'm sure if we were clever, we could combine [grow] and [shrink] into 1 function.
 * That was the original plan, but I got tired, and writing two copies seemed easier at the time!
 * But DRY (Don't Repeat Yourself) is better than WET (Write Everything Twice) ;-)
 * They *are* doing the same thing really - shrink is just a -ve growth!
 */
internal fun shrink(calculations: Iterable<BoxNodeData>, reduceBy: Float) {
    val prioritySum = calculations.sumOf { it.child.shrinkPriority }
    var remaining = reduceBy
    for (cal in calculations) {
        if (cal.priority == 0f) {
            cal.result = cal.pref
        } else {
            val shrinkage = reduceBy * cal.priority / prioritySum
            val maxShrinkage = cal.pref - cal.limit

            if (shrinkage > maxShrinkage) {
                cal.result = cal.limit
                cal.priority = 0f
                remaining -= maxShrinkage
            } else {
                cal.result = cal.pref - shrinkage
                remaining -= shrinkage
            }
        }
    }

    // At this point, [remaining] will often still be positive, due to a child hitting its min limit.
    // So we do another pass, shrinking each node in turn as much as possible with a shrinkPriority != 0,
    // until [remaining] = 0.
    if (remaining > 0) {
        for (cal in calculations) {
            if (cal.priority != 0f) {
                val maxShrinkage = cal.result - cal.limit
                if (remaining > maxShrinkage) {
                    cal.result = cal.limit
                    remaining -= maxShrinkage
                } else {
                    cal.result -= remaining
                    return
                }
            }
        }
    }
    // At this point, [remaining] might STILL be > 0, e.g. because the window is smaller than the scene's min size.
    // So we do a third pass, and shrink nodes, whose shrinkPriority are zero.
    if (remaining > 0) {
        for (cal in calculations) {
            if (cal.priority == 0f) {
                val maxShrinkage = cal.result - cal.limit
                if (remaining > maxShrinkage) {
                    cal.result = cal.limit
                    remaining -= maxShrinkage
                } else {
                    cal.result -= remaining
                    return
                }
            }
        }
    }
}



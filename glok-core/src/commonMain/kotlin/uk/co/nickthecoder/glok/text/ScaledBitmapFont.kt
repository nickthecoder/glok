/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.util.Matrix

internal class ScaledBitmapFont(

    override val identifier: FontIdentifier,
    val unscaled: BitmapFont,
    val scale: Float = 1f

) : Font {

    override val height = unscaled.height / scale
    override val ascent = unscaled.ascent / scale
    override val descent = unscaled.descent / scale
    override val top = unscaled.top / scale
    override val bottom = unscaled.bottom / scale
    override val italicInverseSlope = unscaled.italicInverseSlope

    override val maxWidthOfDigit: Float by lazy {
        listOf("0", "1", "2", "3,", "4", "5", "6", "7", "8", "9", "-", ".").maxOf { widthOfOrZero(it, 0) }
    }

    override val fixedWidth: Float

    init {
        val w = widthOfOrZero("W", 0)
        fixedWidth = if (w == widthOfOrZero(".", 0)) w else 0f
    }

    override fun image() = unscaled.image()

    override fun widthOf(text: CharSequence, indentationColumns: Int, startColumn: Int) =
        unscaled.widthOf(text, indentationColumns, startColumn) / scale

    override fun hasGlyph(c: Char) = unscaled.hasGlyph(c)

    override fun caretPosition(text: CharSequence, x: Float, indentationColumns: Int) =
        unscaled.caretPosition(text, x * scale, indentationColumns)

    override fun drawTopLeft(
        text: CharSequence,
        color: Color,
        x: Float,
        y: Float,
        indentationColumns: Int,
        startColumn: Int,
        modelMatrix: Matrix?
    ) {
        val scaledMatrix = if (scale == 1f) {
            modelMatrix
        } else {
            (modelMatrix?.translate(x, y) ?: Matrix.translation(x, y))
                .scale(1f / scale)
                .translate(- x, - y)
        }
        unscaled.drawTopLeft(text, color, x, y, indentationColumns, startColumn, scaledMatrix)
    }

}

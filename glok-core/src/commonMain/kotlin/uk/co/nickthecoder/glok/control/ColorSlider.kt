/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.ObservableColor
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.property.boilerplate.doubleProperty
import uk.co.nickthecoder.glok.property.functions.lerp
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.GradientBackground
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.COLOR_SLIDER

/**
 * A slider which allows the user to select a color between two values [fromColor]..[toColor].
 * The intermediate values are linear interpolations of the red, green, blue and alpha channels.
 *
 * Used by the [CustomColorPicker], so you can see it in action in the `DemoColorPicker`.
 */
class ColorSlider : SliderBase() {

    // region ==== Properties ====

    val fromColorProperty by colorProperty(Color.BLACK)
    var fromColor by fromColorProperty

    val toColorProperty by colorProperty(Color.WHITE)
    var toColor by toColorProperty

    val ratioProperty by doubleProperty(0.0)
    public override var ratio by ratioProperty

    val colorProperty: ObservableColor = fromColorProperty.lerp(toColorProperty, ratioProperty)
    val color by colorProperty

    // endregion

    private val gradientBackground = GradientBackground(orientationProperty, Color.TRANSPARENT, Color.TRANSPARENT).apply {
        fromColorProperty.bindTo(this@ColorSlider.fromColorProperty)
        toColorProperty.bindTo(this@ColorSlider.toColorProperty)
    }

    init {
        style(COLOR_SLIDER)

        colorProperty.addChangeListener { _, _, color ->
            // Change the thumb color based on the selected color.
            thumb.pseudoStyleIf(color.isDark(), ":light")
        }
        for (prop in listOf(fromColorProperty, toColorProperty, ratioProperty)) {
            // NOTE. We are setting the thumb's position in drawChildren,
            // so we do NOT need to requestLayout, only requestRedraw.
            prop.addListener(requestRedrawListener)
        }
        smallRatioStep = 0.01
        largeRatioStep = 0.1
        track.background = gradientBackground
    }

    /**
     * The color picker changes the background from a [GradientBackground] to a `HueGradientBackground`.
     * This restores the original background.
     */
    internal fun restoreGradientBackground() {
        track.background = gradientBackground
    }

    // ==== Object Methods ====

    override fun toString() = super.toString() + " { $fromColor .. $toColor } = $color"

}


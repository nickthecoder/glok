/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.SCROLL_PANE_WITH_BUTTONS
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

/**
 * Similar functionality to a [ScrollPane], but instead of displaying scroll bars, buttons are used instead.
 * Scrolling is only permitting in one orientation.
 *
 * You may also scroll using a trackpad drag gestures as well as a mouse's scroll-wheel.
 *
 * This is implemented using a [ScrollPane] using [ScrollBarPolicy.NEVER]
 * (so scroll bars are never displayed).
 * The [ScrollPane] is the `center` of a [ThreeRow], with `left` and `right` for the scroll buttons.
 * The scroll buttons only appear when needed (i.e. when there is insufficient space
 * to show the whole of the [content]).
 *
 * This is used by [PopupMenu] when the menu is longer than the window's height.
 * It is also used by [TabPane]/[TabBar] when there are more tabs than the available space.
 *
 * ### Anatomy
 *
 * Buttons are on the left & right when [orientation] == HORIZONTAL :
 *
 *     ╭─────────ScrollPaneWithButtons─────────╮
 *     │ ╭──────────────ThreeRow─────────────╮ │
 *     │ │     ╭───────ScrollPane──────╮     │ │
 *     │ │  <  │        content        │  >  │ │
 *     │ │     ╰───────────────────────╯     │ │
 *     │ ╰───────────────────────────────────╯ │
 *     ╰───────────────────────────────────────╯
 *
 * The scroll buttons (displayed above using < and >) are only visible when [content] is wider than the available space.
 */
class ScrollPaneWithButtons(content: Node? = null) : WrappedNode<ThreeRow>(ThreeRow()) {

    /**
     * Scroll buttons are on the `left` and `right` (or `top` and `bottom`),
     * The `center` is a [scrollPane] whose `content` is bound to this control's [content].
     */
    private val threeRow: ThreeRow get() = inner

    /**
     * The center of [threeRow].
     */
    private val scrollPane = ScrollPane()


    // region ==== Properties ====
    val orientationProperty by orientationProperty(Orientation.HORIZONTAL)
    var orientation by orientationProperty

    val scrollByProperty by floatProperty(50f)
    var scrollBy by scrollByProperty

    val contentProperty by optionalNodeProperty(null)
    var content by contentProperty

    private val requiresScrollButtonsProperty: ObservableBoolean = BooleanTernaryFunction(
        orientationProperty, scrollPane.hScrollMaxProperty, scrollPane.vScrollMaxProperty
    ) { orientation, hMax, vMax ->
        if (orientation == Orientation.HORIZONTAL) {
            hMax > 0f
        } else {
            vMax > 0f
        }
    }

    // endregion

    init {
        style(SCROLL_PANE_WITH_BUTTONS)

        requiresScrollButtonsProperty.addListener {
            // Hmm, we need runLater, because requiresScrollButtonsProperty is changing during a layout phase.
            // I consider the need for runLater to be a bug, so ideally, I should find a better solution.
            Platform.runLater { inner.requestLayout() }
        }

        inner.orientationProperty.bindTo(orientationProperty)
        this.content = content

        inner.apply {
            center = scrollPane.apply {
                shrinkPriority = 1f
                contentProperty.bindTo(this@ScrollPaneWithButtons.contentProperty)
                hPolicy = ScrollBarPolicy.NEVER
                vPolicy = ScrollBarPolicy.NEVER
            }
        }

        createButtons()
        orientationProperty.addListener { createButtons() }
    }

    // Prevent being squashed.
    // Without this, the center of the threeRow (a scrollPane) can be squashed to 0.
    override fun nodeMinHeight() =
        if (orientation == Orientation.HORIZONTAL) nodePrefHeight() else super.nodeMinHeight()

    override fun nodeMinWidth() =
        if (orientation == Orientation.VERTICAL) nodePrefWidth() else super.nodeMinWidth()

    private fun createButtons() {
        if (orientation == Orientation.VERTICAL) {
            with(inner) {
                top = button("^") {
                    style(".scroll_up")
                    growPriority = 1f
                    visibleProperty.bindTo(requiresScrollButtonsProperty)
                    onAction { scrollPane.vScrollValue = max(0f, scrollPane.vScrollValue - scrollBy) }
                }
                bottom = button("v") {
                    style(".scroll_down")
                    growPriority = 1f
                    visibleProperty.bindTo(requiresScrollButtonsProperty)
                    onAction {
                        scrollPane.vScrollValue = min(scrollPane.vScrollMax, scrollPane.vScrollValue + scrollBy)
                    }
                }
            }
        } else {
            with(inner) {
                left = button("<") {
                    style(".scroll_left")
                    growPriority = 1f
                    visibleProperty.bindTo(requiresScrollButtonsProperty)
                    onAction { scrollPane.hScrollValue = max(0f, scrollPane.hScrollValue - scrollBy) }
                }
                right = button(">") {
                    style(".scroll_right")
                    growPriority = 1f
                    visibleProperty.bindTo(requiresScrollButtonsProperty)
                    onAction {
                        scrollPane.hScrollValue = min(scrollPane.hScrollMax, scrollPane.hScrollValue + scrollBy)
                    }
                }
            }
        }
    }

    fun scrollTo(contentDescendant: Node) {
        scrollPane.scrollTo(contentDescendant)
    }

    fun scrollToX(contentX: Float) {
        scrollPane.scrollToX(contentX)
    }

    fun scrollToY(contentY: Float) {
        scrollPane.scrollToY(contentY)
    }

}

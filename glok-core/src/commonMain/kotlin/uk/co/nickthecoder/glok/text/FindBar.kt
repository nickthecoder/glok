/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.control.ToolBar
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.glok.scene.dsl.spacer

class FindBar(val findAndReplace: FindAndReplace) : WrappedNode<ToolBar>(ToolBar()) {

    val searchTextField = TextField().apply {
        textProperty.bidirectionalBind(findAndReplace.findProperty)
        promptText = "find"
        focusedProperty.addChangeListener { _, _, focused ->
            if (focused) {
                selectAll()
            } else {
                anchorIndex = caretIndex
            }
        }
    }

    /**
     * Whenever the [FindBar] is made visible, focus on the [searchTextField].
     */
    @Suppress("unused")
    private val focusListener = visibleProperty.addWeakChangeListener { _, _, isVisible ->
        if (isVisible) {
            searchTextField.requestFocus()
        }
    }

    init {
        visibleProperty.bindTo(findAndReplace.findVisibleProperty)

        findAndReplace.commands.build(findAndReplace.iconSizeProperty) {
            with(FindAndReplaceActions) {
                inner.apply {
                    + searchTextField
                    + toggleButton(MATCH_CASE)
                    + toggleButton(MATCH_WORDS)
                    + toggleButton(MATCH_REGEX)
                    + button(FIND_PREV)
                    + button(FIND_NEXT)
                    + Label("").apply { textProperty.bindTo(findAndReplace.statusProperty) }
                    + spacer()
                    + button(CLOSE)
                }
            }
        }
    }

}


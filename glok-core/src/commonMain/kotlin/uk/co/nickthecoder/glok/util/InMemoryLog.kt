package uk.co.nickthecoder.glok.util

/**
 * An in-memory log, split into two parts, so that when the maximum entries are exceeded,
 * the start and end of the log are preserved, and the middle is discarded.
 *
 * [halfMaxEntries] is the size of `start` and `end`, so the maximum number of entries is
 * [halfMaxEntries] * 2.
 */
class InMemoryLog(val halfMaxEntries: Int) : Log {

    override var level = Log.TRACE

    private var headIsFull = false
    private var tailIsFull = false
    private val head = mutableListOf<String>()
    private var tail = mutableListOf<String>()

    /**
     * The index into [tail] where new entries will be placed.
     */
    private var tailEndIndex = 0

    val size get() = head.size + tail.size

    fun clear() {
        tail.clear()
        head.clear()
        headIsFull = false
        tailIsFull = false
        tailEndIndex = 0
    }

    fun isEmpty() = head.isEmpty()
    fun isNotEmpty() = head.isNotEmpty()
    fun isFull() = tailIsFull

    /**
     * Middle entries are lost. So there may be a `jump` at [halfMaxEntries] and -[halfMaxEntries]
     * when [isFull] == true.
     * You should see a message in the log itself when log entries have been lost.
     *
     * @index When positive, it counts from the start of the log, when negative, it counts from the end of the log.
     * The acceptable range is -[size] .. [size] (both exclusive).
     */
    operator fun get(index: Int): String {
        if (index >= 0) {
            if (index >= head.size && headIsFull) {
                val tailIndex = index - head.size
                if (tailIndex >= tail.size) throw IndexOutOfBoundsException(index.toString())
                return tail[(tailEndIndex + tailIndex) % tail.size]
            } else {
                return head[index]
            }
        } else {
            val tailIndex = tail.size + index
            if (tailIndex >= 0) {
                if (tailIndex >= tail.size) throw IndexOutOfBoundsException(index.toString())
                return tail[(tailEndIndex + tailIndex) % tail.size]
            } else {
                return head[head.size + tailIndex]
            }
        }
    }

    override fun add(level: Int, message: String) {
        if (level < this.level) return

        if (headIsFull) {
            if (tailIsFull) {
                tail[tailEndIndex % halfMaxEntries] = message
                tailEndIndex++
            } else {
                tail.add(message)
                if (tail.size >= halfMaxEntries) {
                    tailIsFull = true
                    head.add("LOG FULL. Some entries after this have been lost...")
                }
            }
        } else {
            head.add(message)
            if (head.size >= halfMaxEntries) {
                headIsFull = true
            }
        }
    }

    override fun toString(): String {
        return "${head.joinToString("\n")} \n...\n ${tail.joinToString("\n")}\n"
    }
}

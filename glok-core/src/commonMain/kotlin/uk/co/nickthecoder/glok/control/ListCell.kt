/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.EVEN
import uk.co.nickthecoder.glok.theme.styles.LIST_CELL
import uk.co.nickthecoder.glok.theme.styles.ODD
import uk.co.nickthecoder.glok.theme.styles.SELECTED

/**
 * ListCells are the visible [Node]s within a [ListView].
 * ListCells are created as-needed (and left to be garbage collected when unused),
 * as the user scrolls through the [ListView].
 *
 * ### Theme DSL
 *
 *     "list_cell" {
 *         // It is common to change the background color of odd or even cells.
 *         ":odd" { ... }
 *         ":even" { ... }
 *
 *         // The selected row of the ListView.
 *         ":selected" { ... }
 *     }
 *
 * ListCell inherits all the features of [Region].
 */
abstract class ListCell<T>(val listView: ListView<T>, val item: T) : Region() {

    internal val mutableIndexProperty by intProperty(- 1)
    val indexProperty = mutableIndexProperty.asReadOnly()
    var index by mutableIndexProperty
        internal set

    // A weak listener, as this ListCell has a shorter life than the ListView (and its selection).
    @Suppress("unused")
    private val selectedIndexListener =
        listView.selection.selectedIndexProperty.addWeakListener { selectionOrIndexChanged() }

    init {
        style(LIST_CELL)
        pseudoStyleIf(index % 2 == 0, EVEN, ODD)

        mutableIndexProperty.addChangeListener { _, _, value ->
            pseudoStyleIf(value % 2 == 0, EVEN, ODD)
            selectionOrIndexChanged()
        }
        selectionOrIndexChanged()

        onMousePressed { listView.selection.selectedIndex = index }
        onMousePressed { event -> if (listView.canReorder && event.isPrimary) event.capture() }
        onMouseDragged { event -> if (listView.canReorder) listCellDragged(event, this) }
    }

    private fun selectionOrIndexChanged() {
        pseudoStyleIf(listView.selection.selectedIndex == index, SELECTED)
    }


    private fun listCellDragged(event: MouseEvent, draggingListCell: ListCell<T>) {

        val listView = draggingListCell.listView

        val node = draggingListCell.scene?.findDeepestNodeAt(event.sceneX, event.sceneY) ?: return
        val destListCell = node.firstToRoot { it is ListCell<*> }
        if (destListCell is ListCell<*> && destListCell.listView === listView) {

            // The mouse is over a different ListCell, so use its index for the ListCell we are dragging.
            listView.moveItem(draggingListCell.index, destListCell.index)
            // Ensures that the list cell isn't partially visible (at the top/bottom of the list)
            listView.scrollToRow(draggingListCell.index)

        } else {
            if (event.sceneX >= listView.sceneX && event.sceneX < listView.sceneX + listView.width) {
                if (event.sceneY < listView.sceneY) {
                    // The mouse is directly above the ListView, so let's try to scroll up.

                    val distance = listView.sceneY - event.sceneY
                    // Reorder to be the first visible
                    listView.moveItem(draggingListCell.index, listView.visibleCells().minOf { it.index })
                    // We scroll to the row AND scrollBy because if we only did scrollBy, the
                    // cell could be only half visible. If we only did scrollToRow, then scrolling would stop.
                    listView.scrollToRow(draggingListCell.index)
                    listView.scrollBy(if (distance > 50f) - 10f else - 1f)

                } else if (event.sceneY > listView.sceneY + listView.height) {
                    // The mouse is directly below the ListView, so let's try to scroll down.

                    val distance = event.sceneY - listView.sceneY - listView.height
                    // Reorder to be the last visible
                    listView.moveItem(draggingListCell.index, listView.visibleCells().maxOf { it.index })
                    listView.scrollToRow(draggingListCell.index)
                    listView.scrollBy(if (distance > 50f) 10f else 1f)
                }
            }
        }

    }

    override fun toString() = super.toString() + " #$index"

}

/**
 * An abstract implementation of [ListCell] which has a single child node of type [N].
 * Consider using a [HBox] for [node] if you want a [ListCell] with more than one child nodes.
 */
open class SingleNodeListCell<T, N : Node>(
    listView: ListView<T>,
    item: T,
    val node: N
) : ListCell<T>(listView, item) {

    override val children = listOf<Node>(node).asObservableList()

    init {
        claimChildren()
    }

    override fun nodePrefWidth() = node.evalPrefWidth() + surroundX()
    override fun nodePrefHeight() = node.evalPrefHeight() + surroundY()

    override fun layoutChildren() {
        setChildBounds(node, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
}

/**
 * A [ListCell] which uses a [Label] to display the cell's value.
 * This is the default type of [ListCell], if [ListView.cellFactory] is unchanged.
 *
 * ### Theme DSL
 *
 *     "list_cell" {
 *         child("label") { ... }
 *     }
 *
 * TextListCell inherits all features of [ListCell].
 */
open class TextListCell<T>(
    listView: ListView<T>,
    item: T,
    text: String = item.toString()
) : SingleNodeListCell<T, Label>(listView, item, Label(text))

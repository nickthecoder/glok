/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.util.min

/**
 * Draws a rounded rectangle.
 * If [side] is set, then only 1 side of the rectangle is rounded (i.e. two corners).
 */
class RoundedBackground(val radius: Float, val side: Side? = null) : Background {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        // If width or height are too small, make the radius smaller
        val r = min(min(radius, height / 2), width / 2)

        val x1 = x + r
        val x3 = x + width
        val x2 = x3 - r

        val y1 = y + r
        val y3 = y + height
        val y2 = y3 - r
        with(backend) {
            if (side == Side.BOTTOM || side == Side.RIGHT) {
                fillRect(x1, y1, x, y, color)
            } else {
                fillQuarterCircle(x1, y1, x, y, color, r) // Top left corner
            }
            fillRect(x1, y, x2, y1, color) // Top
            if (side == Side.BOTTOM || side == Side.LEFT) {
                fillRect(x1, y, x2, y1, color)
            } else {
                fillQuarterCircle(x2, y1, x3, y, color, r) // Top right corner
            }
            fillRect(x, y1, x3, y2, color) // Left, center and right
            if (side == Side.TOP || side == Side.RIGHT) {
                fillRect(x1, y2, x, y3, color)
            } else {
                fillQuarterCircle(x1, y2, x, y3, color, r) // Bottom left corner
            }
            fillRect(x1, y2, x2, y3, color) // Bottom
            if (side == Side.TOP || side == Side.LEFT) {
                fillRect(x2, y2, x3, y3, color)
            } else {
                fillQuarterCircle(x2, y2, x3, y3, color, r) // Bottom right corner
            }
        }

    }

    override fun toString() = "RoundedBackground( $radius )"
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.IntUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableInt
import uk.co.nickthecoder.glok.property.boilerplate.ObservableTextPosition
import uk.co.nickthecoder.glok.property.boilerplate.TextPositionBinaryFunction
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

fun min(oa: ObservableTextPosition, ob: ObservableTextPosition): ObservableTextPosition = TextPositionBinaryFunction(oa, ob) { a, b -> min(a, b) }
fun max(oa: ObservableTextPosition, ob: ObservableTextPosition): ObservableTextPosition = TextPositionBinaryFunction(oa, ob) { a, b -> max(a, b) }

fun ObservableTextPosition.row(): ObservableInt = IntUnaryFunction(this) { a -> a.row }
fun ObservableTextPosition.column(): ObservableInt = IntUnaryFunction(this) { a -> a.column }

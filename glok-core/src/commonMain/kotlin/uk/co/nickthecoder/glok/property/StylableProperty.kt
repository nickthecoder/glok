/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import kotlin.reflect.KClass

/**
 * Properties which can be styled by a theme must use this instead of a [Property].
 *
 * The purpose of a [StylableProperty] is to hold the value prior to being styled,
 * so that it can be reverted if the property becomes un-styled.
 *
 * For example, imagine we have a Label with black text, and we add a `"highlight"` style,
 * which causes the text to turn bright green.
 *
 * Later, when we remove the `"highlight"` style, the text will revert to black (using [revert]).
 *
 * If a [StylableProperty]'s value is set after it has been styled, the value applied by the [Theme]
 * will be overwritten. removing the theme will have no effect, the new value will remain.
 */
interface StylableProperty<V> : Property<V> {
    /**
     * Returns the Class for this Property's value, or null if [bean] or [beanName] have not been specified.
     */
    fun kclass(): KClass<*>
    fun style(newValue: Any)
    fun revert()
}

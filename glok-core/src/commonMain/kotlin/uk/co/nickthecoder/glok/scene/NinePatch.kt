package uk.co.nickthecoder.glok.scene

/**
 * Draws a rectangular region using 9 'patches' the four corner patches are drawn 1:1 (unscaled).
 * The top and bottom patches are stretched (or shrunk) horizontally to fit between the corners.
 * The left and right patches are similarly stretched (or shrunk) vertically.
 * The center path is stretch (or shrunk) in both directions.
 *
 * Typically used to give pictorial backgrounds to buttons.
 * Each state of the button( hover, armed, selected etc. ) will typically have their own NinePatch.
 *
 * The center patch may optionally be omitted (e.g. if the center is fully transparent, or the if it already
 * contains the appropriate flat color).
 *
 * See also : [NinePatchBackground] and [NinePatchBorder], [NinePatchImages].
 *
 */
class NinePatch(

    val image: Image,
    val edges: Edges
) {

    val topLeftPatch = image.partialImage(
        0f,
        0f,
        edges.left,
        edges.top
    )
    val topPatch = image.partialImage(
        edges.left,
        0f,
        image.imageWidth - edges.left - edges.right,
        edges.top
    )
    val topRightPatch = image.partialImage(
        image.imageWidth - edges.right,
        0f,
        edges.right,
        edges.top
    )


    val leftPatch = image.partialImage(
        0f,
        edges.top,
        edges.left,
        image.imageHeight - edges.top - edges.bottom
    )

    val centerPatch = image.partialImage(
        edges.left,
        edges.top,
        image.imageWidth - edges.left - edges.right,
        image.imageHeight - edges.top - edges.bottom
    )
    val rightPatch = image.partialImage(
        image.imageWidth - edges.right,
        edges.top,
        edges.right,
        image.imageHeight - edges.top - edges.bottom
    )

    val bottomLeftPatch = image.partialImage(
        0f,
        image.imageHeight - edges.bottom,
        edges.left,
        edges.bottom
    )

    val bottomPatch = image.partialImage(
        edges.left,
        image.imageHeight - edges.bottom,
        image.imageWidth - edges.left - edges.right,
        edges.bottom
    )

    val bottomRightPatch = image.partialImage(
        image.imageWidth - edges.right,
        image.imageHeight - edges.bottom,
        edges.right,
        edges.bottom
    )

    fun draw(x: Float, y: Float, width: Float, height: Float, drawCenter: Boolean = true, tint: Color? = null) {
        val x1 = x + edges.left
        val x2 = x + width - edges.right
        val y1 = y + edges.top
        val y2 = y + height - edges.bottom
        val centerWidth = width - edges.left - edges.right
        val centerHeight = height - edges.top - edges.bottom

        image.batch(tint) {
            topLeftPatch.drawBatched(this, x, y)
            topPatch.drawToBatched(this, x1, y, centerWidth, edges.top)
            topRightPatch.drawBatched(this, x2, y)

            leftPatch.drawToBatched(this, x, y1, edges.left, centerHeight)
            if (drawCenter) {
                centerPatch.drawToBatched(this, x1, y1, centerWidth, centerHeight)
            }
            rightPatch.drawToBatched(this, x2, y1, edges.right, centerHeight)

            bottomLeftPatch.drawBatched(this, x, y2)
            bottomPatch.drawToBatched(this, x1, y2, centerWidth, edges.top)
            bottomRightPatch.drawBatched(this, x2, y2)
        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.VAlignment

fun observablePos(hAlignment: ObservableHAlignment, vAlignment: ObservableVAlignment): ObservableAlignment = AlignmentBinaryFunction(hAlignment, vAlignment) { h, v -> Alignment.value(h, v) }
fun observablePos(hAlignment: ObservableHAlignment, vAlignment: VAlignment): ObservableAlignment = AlignmentUnaryFunction(hAlignment) { h -> Alignment.value(h, vAlignment) }
fun observablePos(hAlignment: HAlignment, vAlignment: ObservableVAlignment): ObservableAlignment = AlignmentUnaryFunction(vAlignment) { v -> Alignment.value(hAlignment, v) }

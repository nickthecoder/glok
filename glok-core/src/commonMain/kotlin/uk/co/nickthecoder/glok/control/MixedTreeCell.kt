/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.EventType
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.TREE_CELL


abstract class MixedTreeCell<I : MixedTreeItem<I>>(val treeView: MixedTreeView<I>, val treeItem: I) : Region() {

    internal val leafProperty = SimpleBooleanProperty(false)

    internal val expandedProperty = SimpleBooleanProperty(false)

    init {
        style(TREE_CELL)

        leafProperty.bindTo(treeItem.leafProperty)
        expandedProperty.bindTo(treeItem.expandedProperty)

        // Remember who has focus in a filter, then check again in an event handler.
        // If it has stayed the same, then focus on the cell (the normal behaviour).
        // If it has changed, then a node INSIDE the cell has requested focus, and
        // we should NOT steal the focus away from it.
        var oldFocus: Node? = null

        addEventFilter(EventType.MOUSE_PRESSED) {
            oldFocus = scene?.focusOwner
        }

        onMousePressed {
            treeItem.let { treeItem ->
                treeView.selection.selectedItem = treeItem
                if (oldFocus === scene?.focusOwner && focusAcceptable) {
                    requestFocus()
                }
            }
        }

    }

    override fun toString() = super.toString() + " $treeItem"

}


/**
 */
open class SingleNodeMixedTreeCell<I : MixedTreeItem<I>, N : Node>(
    treeView: MixedTreeView<I>,
    treeItem: I,
    val node: N
) : MixedTreeCell<I>(treeView, treeItem) {

    // NOTE, this holds the child NODES of this TreeCell, and is not related to
    // TreeItem.children (which are not nodes).
    override val children by lazy { listOf<Node>(node).asObservableList() }

    init {
        claimChildren()
    }

    override fun nodePrefWidth() = node.evalPrefWidth() + surroundX()
    override fun nodePrefHeight() = node.evalPrefHeight() + surroundY()

    override fun layoutChildren() {
        setChildBounds(node, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
}

open class TextMixedTreeCell<I : MixedTreeItem<I>>(

    treeView: MixedTreeView<I>,
    treeItem: I,
    text: String = treeItem.toString()

) : SingleNodeMixedTreeCell<I, Label>(treeView, treeItem, Label(text))

package uk.co.nickthecoder.glok.event

import uk.co.nickthecoder.glok.scene.Scene

class ScrollEvent(
    scene: Scene,
    sceneX: Float,
    sceneY: Float,

    val deltaX: Float,
    val deltaY: Float,
    mods: Int

) : MouseEventBase(scene, sceneX, sceneY, mods) {

    override val eventType get() = EventType.SCROLL

    override fun toString() = "ScrollEvent @ $sceneX , $sceneY : Scroll by ( $deltaX , $deltaY )"
}

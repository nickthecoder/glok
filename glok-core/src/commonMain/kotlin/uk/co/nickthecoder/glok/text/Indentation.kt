/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.TextArea
import kotlin.math.min

/**
 * Determines how [TextArea] and [StyledTextArea] should display leading tab and space characters,
 * and the behaviour when the tab key is pressed.
 */
interface Indentation {

    val columns: Int

    val indentation: String

    /**
     * For space-based indentation, should the caret move as if the underlying data structure uses tab characters?
     * i.e. using the arrow keys, clicking/dragging with the mouse, backspace and delete should treat an
     * indentation consisting of many spaces as if it were a single tab-character.
     *
     * When set to `true`, the caret will not appear in the middle of an indentation.
     * Delete and Backspace will delete the whole indentation, not just a single space.
     * This leads to an asymmetry which may annoy some people : Pressing space 4 times at the start of a line,
     * then hitting Backspace _once_ will get back to the starting state. (assuming [columns] == 4).
     *
     * The tab vs spaces flame wars have good arguments on both sides :
     *
     * ### Pro Spaces
     * * There is no standard for what a `tab` character means. The only `standard` is 8 columns,
     *   which virtually nobody uses.
     *   Storing text documents with spaces ensures they look correct everywhere.
     *
     * ### Pro Tabs
     * * Your editor should never allow misalignment.
     * * Manually tinkering with spaces is a waste of time an effort.
     * * The tab key should always indent by the correct amount.
     * * The caret should never appear part-way within an indentation.
     * * _My_ choice of indentation size should have no bearing on _your_ choice.
     *   Using spaces forces us to use the same indentation width.
     *
     * Using [spacesIndentation] with [behaveLikeTabs] solves these issues except for the last one.
     * Well, it would, if every editor had a [behaveLikeTabs] option.
     * I've yet to see another editor with this feature. A first for Glok?
     *
     * At the very least this option should be a god-send for tab-lovers who are forced to work with
     * spaces!
     *
     * Note. This feature only works for indentation at the start of a line. IMHO, adding tab-character mid-line
     * is crazy. I had a coworker who spent ages, lining up code with mid-line tab-characters,
     * and it was unreadable everywhere other than his editor.
     * This is a problem for indentation within dokka comments. WTF are all those asterisks for anyway? IDK.
     *
     */
    val behaveLikeTabs: Boolean

    /**
     * @Return the text which should be prefixed to [line]
     */
    fun indent(line: String): String

    /**
     * @Return the number of characters which should be removed from [line] to unindent it.
     */
    fun unindent(line: String): Int

    companion object {
        @Deprecated(
            "Moved to GlokSettings", ReplaceWith(
                "GlokSettings.defaultIndentation",
                "uk.co.nickthecoder.glok.application.GlokSettings"
            )
        )
        var defaultIndentation
            get() = GlokSettings.indentation
            set(v) {
                GlokSettings.indentation = v
            }

        fun tabIndentation(columns: Int): Indentation = IndentationImpl("\t", columns, false)
        fun spacesIndentation(columns: Int, behaveLikeTabs: Boolean): Indentation =
            IndentationImpl(" ".repeat(columns), columns, behaveLikeTabs)
    }
}

internal class IndentationImpl(

    override val indentation: String,
    override val columns: Int,
    override val behaveLikeTabs: Boolean

) : Indentation {

    override fun indent(line: String) = indentation

    override fun unindent(line: String): Int {

        for (i in 0 until min(line.length, columns)) {
            val c = line[i]
            if (c == '\t') {
                return i + 1
            }
            if (c != ' ') {
                return i
            }
        }
        return min(columns, line.length)
    }

    override fun hashCode(): Int {
        var result = indentation.hashCode()
        result = 31 * result + columns
        result = 31 * result + behaveLikeTabs.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        return other is IndentationImpl && indentation == other.indentation && columns == other.columns && behaveLikeTabs == other.behaveLikeTabs
    }

    override fun toString(): String {
        val label = when (indentation) {
            "\t" -> "Tabs"
            " ".repeat(columns) -> "Spaces"
            else -> "'$indentation'"
        }
        return "$label ($columns Columns) behaveLikeTabs:$behaveLikeTabs"
    }

}

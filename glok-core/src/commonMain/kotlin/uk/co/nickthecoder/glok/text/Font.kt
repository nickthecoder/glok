/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.util.Matrix
import uk.co.nickthecoder.glok.util.log


interface Font {

    val identifier: FontIdentifier

    /**
     * This is the distance between the baseline of adjacent lines of text.
     * It is the sum of the leading + ascent + descent.
     */
    val height: Float

    /**
     * The font ascent is the distance from the font's baseline to the top of most alphanumeric characters.
     * NOTE. I've read that [ascent] is sometimes defined as the position of the top of the glyph relative
     * to the baseline, and is therefore negative. [ascent] is always positive.
     */
    val ascent: Float

    /**
     * The font descent is the distance from the font's baseline to the bottom of most alphanumeric characters
     * with descenders.
     */
    val descent: Float

    /**
     * [top] + [bottom]
     */
    val maxHeight: Float get() = this.top + bottom

    /**
     * The maximum ascent of the Font. No character should extend further above the font's baseline than this.
     * "Badly behaved" fonts to not adhere to this though. This field is taken from the font's meta-data,
     * not from the shape of the glyph itself, and these can very well differ :-(
     *
     * NOTE. [top] is always positive.
     */
    val top: Float

    /**
     * The maximum descent of the Font. No character should extend further below the font's baseline than this.
     * "Badly behaved" fonts do not adhere to this though. This field is taken from the font's meta-data,
     * not from the shape of the glyph itself, and these can very well differ :-(
     */
    val bottom: Float

    /**
     * The inverse slope of the caret which best matches the posture of this Font.
     */
    val italicInverseSlope: Float

    /**
     * The width of all glyphs (if this is a fixed width font, otherwise 0.
     */
    val fixedWidth: Float

    /**
     * Calculates the width of [text] in pixels.
     * [indentationColumns] and [startColumn] are required to correctly work out the width of a tab character.
     */
    fun widthOf(
        text: CharSequence,
        indentationColumns: Int = GlokSettings.indentation.columns,
        startColumn: Int = 0
    ): Float

    fun widthOfOrZero(
        text: CharSequence,
        indentationColumns: Int = GlokSettings.indentation.columns,
        startColumn: Int = 0
    ): Float {
        return try {
            widthOf(text, indentationColumns, startColumn)
        } catch (e: GlyphMissingException) {
            0f
        }
    }

    /**
     * The width of the widest digit as well as "." and "-"
     * Suitable for working out prefWidths of TextFields which contain only numbers.
     */
    val maxWidthOfDigit: Float

    fun hasGlyph(c: Char): Boolean

    fun caretPosition(text: CharSequence, x: Float, indentationColumns: Int): Int

    /**
     * For bitmap based fonts, returns the texture, otherwise null.
     * I use this for debugging only, and probably has no other value.
     */
    fun image(): Image? = null

    /**
     * Draw [text] at position [x],[y].
     *
     * The meaning of [x],[y] is dependent on [hAlignment] and [vAlignment].
     * e.g. For [HAlignment.CENTER] [x] is the center of the rendered text.
     */
    fun draw(
        text: CharSequence,
        color: Color,
        x: Float, y: Float,
        hAlignment: HAlignment = HAlignment.LEFT,
        vAlignment: TextVAlignment = TextVAlignment.BASELINE,
        indentationColumns: Int = 0,
        startColumn: Int = 0,
        modelMatrix: Matrix? = null
    ) {
        val left = when (hAlignment) {
            HAlignment.LEFT -> x
            HAlignment.CENTER -> x - widthOfOrZero(text, indentationColumns, startColumn) / 2
            HAlignment.RIGHT -> x - widthOfOrZero(text, indentationColumns, startColumn)
        }
        val top = when (vAlignment) {
            TextVAlignment.TOP -> y
            TextVAlignment.CENTER -> y - (top + bottom) / 2
            TextVAlignment.BASELINE -> y - top
            TextVAlignment.BOTTOM -> y - bottom - top
        }
        drawTopLeft(text, color, left, top, indentationColumns, startColumn, modelMatrix)
    }

    /**
     * @param lineSpacingMultiplier The ratio of the distance between baselines, compared to the Font's [height].
     * A value of 1.0 would tightly pack the lines together. 1.25 is often a visually pleasing value,
     * and therefore 1.25 is the default value.
     */
    fun drawMultiLine(
        text: CharSequence, color: Color, x: Float, y: Float,
        hAlignment: HAlignment, vAlignment: TextVAlignment,
        lineSpacingMultiplier: Float = 1.25f,
        indentationColumns: Int,
        modelMatrix: Matrix? = null
    ) {
        if (text.contains('\n')) {
            drawMultiLine(
                text.split('\n'), color,
                x, y, hAlignment, vAlignment,
                indentationColumns = indentationColumns
            )
        } else {
            draw(text, color, x, y, hAlignment, vAlignment, indentationColumns, 0, modelMatrix)
        }
    }

    /**
     * @param lineSpacingMultiplier The ratio of the distance between baselines, compared to the Font's [height].
     * A value of 1.0 would tightly pack the lines together. 1.25 is often a visually pleasing value,
     * and therefore 1.25 is the default value.
     * @param lineSpacingExtra Extra space in pixels between each line (this is in addition to the spacing
     * due to [lineSpacingMultiplier]. The default is 0.
     */
    fun drawMultiLine(
        text: List<CharSequence>,
        color: Color,
        x: Float, y: Float,
        hAlignment: HAlignment, vAlignment: TextVAlignment,
        lineSpacingMultiplier: Float = 1.25f,
        lineSpacingExtra: Float = 0f,
        indentationColumns: Int,
        modelMatrix: Matrix? = null
    ) {
        if (text.isEmpty()) return

        val lineHeight = height * lineSpacingMultiplier + lineSpacingExtra
        val maxWidth = if (hAlignment == HAlignment.RIGHT) text.maxOf { widthOfOrZero(it, indentationColumns) } else 0f

        // What is the y value of the baseline for the first line of text?
        val singleLineTop = when (vAlignment) {
            TextVAlignment.TOP -> y + top
            TextVAlignment.CENTER -> y + (top - bottom) / 2
            TextVAlignment.BASELINE -> y
            TextVAlignment.BOTTOM -> y - bottom + top
        }
        var top = when (vAlignment) {
            TextVAlignment.TOP -> singleLineTop
            TextVAlignment.CENTER -> singleLineTop - lineHeight * (text.size - 1) / 2
            TextVAlignment.BASELINE -> singleLineTop - lineHeight * (text.size - 1) / 2
            TextVAlignment.BOTTOM -> y - lineHeight * (text.size - 1)
        }
        for (line in text) {
            val left = when (hAlignment) {
                HAlignment.LEFT -> x
                HAlignment.CENTER -> x - widthOfOrZero(line, indentationColumns) / 2
                HAlignment.RIGHT -> x - maxWidth
            }
            drawTopLeft(line, color, left, top, indentationColumns, modelMatrix = modelMatrix)
            top += lineHeight
        }
    }

    /**
     * Draws text where the top left of the first glyph is at [x], [y]
     *
     * This is the `low level` draw method (all other `draw` methods end up calling this).
     * It is unusual for application code to call this directly, because you normally want
     * to specify a [HAlignment] and [TextVAlignment], and have the [x] [y] calculated
     * for you!
     *
     * @param indentationColumns How many columns does a tab character advance by.
     * @param startColumn Often not required.
     * Imaging we want to draw the text : `H TAB ello` (without the spaces), with 4 columns per indentation.
     * The TAB character will only advance by 3 columns (because of the 'H').
     * Now suppose we want to color the 'H' red, and the rest blue.
     * That's two separate calls. The second call must set [startColumn] to 1,
     * so that we know that the `TAB` should only advance by 3 columns.
     * We don't need to know the actual column number, only modulo `indentationColumns`.
     * e.g. in this example, `1` and `5` would both be fine.
     *
     * @return true iff all glyphs were found
     */
    fun drawTopLeft(
        text: CharSequence,
        color: Color,
        x: Float, y: Float,
        indentationColumns: Int, startColumn: Int = 0,
        modelMatrix: Matrix? = null
    )

    fun plain() = basedOn(this, style = FontStyle.PLAIN)

    fun size(size: Float) = basedOn(this, size = size)

    fun bold() =
        basedOn(this, style = if (identifier.style == FontStyle.ITALIC) FontStyle.BOLD_ITALIC else FontStyle.BOLD)

    fun noBold() =
        basedOn(
            this,
            style = if (identifier.style == FontStyle.BOLD_ITALIC) FontStyle.ITALIC else if (identifier.style == FontStyle.ITALIC) FontStyle.ITALIC else FontStyle.PLAIN
        )

    fun italic() =
        basedOn(this, style = if (identifier.style == FontStyle.BOLD) FontStyle.BOLD_ITALIC else FontStyle.ITALIC)

    fun noItalic() =
        basedOn(this, style = if (identifier.style == FontStyle.BOLD_ITALIC) FontStyle.BOLD else if (identifier.style == FontStyle.BOLD) FontStyle.BOLD else FontStyle.PLAIN)

    fun boldItalic() = basedOn(this, style = FontStyle.BOLD_ITALIC)

    companion object {

        /**
         * The default font.
         */
        val defaultFont: Font by lazy {
            create(16f, FontStyle.PLAIN, "Liberation", "Verdana", "SansSerif")
        }

        fun create(family: String, size: Float, style: FontStyle = FontStyle.PLAIN): Font {
            val identifier = FontIdentifier(family, size, style)
            val found = FontCache[identifier]
            if (found != null) return found

            val globalScale = GlokSettings.globalScale

            val requiredSize = size * globalScale
            val unscaledIdentifier = FontIdentifier(family, requiredSize, style)
            val unscaled = createBitmapFont(unscaledIdentifier) {}

            val scaleAwareFont = ScaleAwareBitmapFont(ScaledBitmapFont(identifier, unscaled, globalScale))
            FontCache[identifier] = scaleAwareFont
            return scaleAwareFont
        }

        fun basedOn(font: Font, family: String? = null, size: Float? = null, style: FontStyle? = null): Font {
            return create(
                family ?: font.identifier.family,
                size ?: font.identifier.size,
                style ?: font.identifier.style
            )
        }

        /**
         * Each font family in [families] are tried sequentially.
         * If the first fails, it tries the second etc.
         * The last should be a fall-back such as "Serif", "SansSerif", "MonoSpaced".
         */
        fun create(size: Float, style: FontStyle, vararg families: String): Font {
            for (family in families) {
                try {
                    return create(family, size, style)
                } catch (e: Exception) {
                    // Do nothing
                }
            }
            log.warn("Failed to create a font from family : $families")
            return create("Verdana", size, style)
        }

        fun sansSerif(size: Float, style: FontStyle = FontStyle.PLAIN) =
            create(size, style, "Liberation", "Verdana", "SansSerif")

        fun serif(size: Float, style: FontStyle = FontStyle.PLAIN) = create(size, style, "Serif")

        fun monospaced(size: Float, style: FontStyle = FontStyle.PLAIN) = create(size, style, "Monospaced")

        fun allFontFamilies() = uk.co.nickthecoder.glok.text.allFontFamilies()

    }
}

internal expect fun allFontFamilies(): List<String>

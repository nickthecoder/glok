/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.action

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.KeyCombination
import uk.co.nickthecoder.glok.scene.NamedImages
import uk.co.nickthecoder.glok.scene.StageBase
import uk.co.nickthecoder.glok.text.FindAndReplaceActions

/**
 * GUI applications can get messy, the classes in this package help reduce the messiness when defining
 * [Button]s, [ToggleButton]s, [PropertyRadioButton]s, [Menu]s, [MenuItem]s, [SubMenu]s, [ToggleMenuItem].
 * It also simplifies keyboard shortcuts.
 *
 * Create a subclass of [Actions], and [define] the text, graphic, keyboard shortcut
 * ([KeyCombination]),
 * for each [Button], [ToggleButton], [PropertyRadioButton], [Menu], [MenuItem], [SubMenu], [ToggleMenuItem]
 * within your application.
 *
 * Then create an instance of [Commands] using these definitions, adding the code to be executed
 * when the [Command] is performed.
 *
 * See the `DemoAction` application for an example of how to use [Actions], [Commands] etc.
 *
 * Here's the latest version of the
 * [demos](https://gitlab.com/nickthecoder/glok/-/tree/main/glok-demos/src/main/kotlin/uk/co/nickthecoder/glok/demos?ref_type=heads)
 *
 */
abstract class Actions(var icons: NamedImages?) {

    val actions = mutableMapOf<String, Action>()

    /**
     * Adds a single [Action] to the map ([actions]).
     *
     * [name] is never shown to the end user, and is only used internally.
     * If you name your icons, then buttons, menuItems etc. will automatically use that icon.
     *
     * [text] is the text within the Button, MenuItem etc.
     *
     * [keyCombination] can be created using helper functions. e.g.
     *
     *     Key.S.noMods() // Just the S key
     *     Key.S.control() // Ctrl+S
     *     Key.S.control().shift() // Ctrl+Shift().S
     *
     * [tooltip] isn't implemented yet (Glok doesn't support tooltips yet).
     */
    fun define(name: String, text: String, keyCombination: KeyCombination? = null, tooltip: String? = null): Action {
        val action = Action(this, name, text, keyCombination, tooltip)
        actions[name] = action
        return action
    }

    fun define(name: String, text: String, keyCombination: KeyCombination? = null, tooltip: String? = null, block: Action.() -> Unit): Action {
        val action = Action(this, name, text, keyCombination, tooltip)
        actions[name] = action
        action.block()
        return action
    }

    companion object {

        /**
         * A list of all [Actions] used within glok-core.
         * This may be handy to inspect the keyCombinations, especially if your application lets
         * the user redefine those keyCombinations.
         *
         * NOTE. There may still be some hard-coded key combinations (i.e. key event handlers which do NOT use [Actions]).
         *
         * Actions from outside glok-core :
         *
         *     DockActions, PlacesActions, PlacesDockActions, NodeInspectorActions (glok-dock)
         *     FileCompletionActions (jvm)
         */
        val glokCoreActions = listOf<Actions>(
            StageBase.StageActions,
            ButtonBase.ButtonActions, ListViewActions, MenuItemActions,
            PopupMenu.PopupMenuActions, ScrollPane.ScrollPaneActions,
            SliderBase.SliderActions, Slider2d.Slider2dActions, SpinnerBase.SpinnerActions,
            TextAreaActions, FindAndReplaceActions,
        )
    }
}

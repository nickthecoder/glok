/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.util

import kotlin.math.cos
import kotlin.math.sin

class Matrix(

    val m00: Float = 1f, var m01: Float = 0f,
    var m10: Float = 0f, var m11: Float = 1f,
    var m20: Float = 0f, var m21: Float = 0f
) {

    fun rotate(radians: Double): Matrix {
        val cos: Float = cos(radians).toFloat()
        val sin: Float = sin(radians).toFloat()

        return Matrix(
            m00 * cos + m10 * sin, m01 * cos + m11 * sin,
            m00 * - sin + m10 * cos, m01 * - sin + m11 * cos,
            m20, m21
        )
    }

    fun translate(x: Float, y: Float): Matrix {
        return Matrix(
            m00, m01,
            m10, m11,
            m00 * x + m10 * y + m20, m01 * x + m11 * y + m21
        )
    }

    fun scale(scale: Float) = scale(scale, scale)

    fun scale(x: Float, y: Float): Matrix {
        return Matrix(
            m00 * x, m01 * x,
            m10 * y, m11 * y,
            m20, m21
        )
    }

    companion object {
        fun rotation(radians: Double): Matrix {
            val cos = cos(radians).toFloat()
            val sin = sin(radians).toFloat()
            return Matrix(
                cos, sin,
                - sin, cos,
                0f, 0f
            )
        }

        fun translation(x: Float, y: Float) = Matrix(
            1f, 0f,
            0f, 1f,
            x, y
        )
    }

}

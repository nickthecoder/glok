package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.min
import kotlin.math.abs

/**
 * Holds a color as red, green, blue, alpha float value in the range 0..1.
 * If a color is created with values outside the range 0..1, they are silently clamped.
 *
 * Methods with use hue,saturation,brightness also use the range 0..1.
 *
 * NOTE. Color is immutable.
 *
 * If there is another `Color` class within your application, I recommend using `import ...Color as GlokColor`
 */
class Color(r: Float, g: Float, b: Float, a: Float) {

    constructor(r: Float, g: Float, b: Float) : this(r, g, b, 1f)

    val red = r.clamp()
    val green = g.clamp()
    val blue = b.clamp()
    val alpha = a.clamp()

    /**
     * Linear interpolation between two colors.
     */
    fun lerp(other: Color, t: Float): Color {
        val s = 1 - t
        return Color(
            red * s + other.red * t,
            green * s + other.green * t,
            blue * s + other.blue * t,
            alpha * s + other.alpha * t
        )
    }

    fun withRed(value: Float) = Color(value, green, blue, alpha)
    fun withGreen(value: Float) = Color(red, value, blue, alpha)
    fun withBlue(value: Float) = Color(red, green, value, alpha)
    fun withAlpha(value: Float) = Color(red, green, blue, value)
    fun opacity(alpha: Float) = Color(red, green, blue, alpha)
    fun opaque() = Color(red, green, blue, 1f)
    fun transparent() = Color(red, green, blue, 0f)

    fun tint(other: Color) = Color(red * other.red, green * other.green, blue * other.blue, alpha * other.alpha)
    operator fun times(other: Color) = Color(red * other.red, green * other.green, blue * other.blue, alpha * other.alpha)

    /**
     * Convert to Hue / Chroma / Value
     * https://www.chilliant.com/rgb2hsv.html
     * @return Array size 3 (HCV) all in the range 0..1
     */
    fun toHCV(): FloatArray {
        val p = if (green < blue) {
            floatArrayOf(blue, green, -1f, 2f / 3f)
        } else {
            floatArrayOf(green, blue, 0f, -1f / 3f)
        }
        val q = if (red < p[0]) {
            floatArrayOf(p[0], p[1], p[3], red)
        } else {
            floatArrayOf(red, p[1], p[2], p[0])
        }
        val c = q[0] - min(q[3], q[1])
        val h = abs((q[3] - q[1]) / (6 * c + epsilon) + q[2])
        return floatArrayOf(h, c, q[0])
    }

    fun isDark() = toHSL()[2] < 0.7f

    /**
     * Convert to Hue / Saturation / Lightness.
     * https://www.chilliant.com/rgb2hsv.html
     * @return Array size 3 (HSL) all in the range 0..1
     */
    fun toHSL(): FloatArray {
        val hcv = toHCV()
        val l = hcv[2] - hcv[1] * 0.5f
        val s = hcv[1] / (1 - abs(l * 2 - 1) + epsilon)
        return floatArrayOf(hcv[0], s, l)
    }

    /**
     * Convert to Hue / Saturation / Lightness.
     * https://www.chilliant.com/rgb2hsv.html
     * @return Array size 4 (HSL and alpha) all in the range 0..1
     */
    fun toHSLA(): FloatArray {
        val hcv = toHCV()
        val l = hcv[2] - hcv[1] * 0.5f
        val s = hcv[1] / (1 - abs(l * 2 - 1) + epsilon)
        return floatArrayOf(hcv[0], s, l, alpha)
    }

    /**
     * Convert to Hue / Saturation / Lightness.
     * https://www.chilliant.com/rgb2hsv.html
     * @return Array size 3 (HSV) all in the range 0..1
     */
    fun toHSV(): FloatArray {
        val hcv = toHCV()
        val s = hcv[1] / (hcv[2] + epsilon)
        return floatArrayOf(hcv[0], s, hcv[2])
    }

    /**
     * Convert to Hue / Saturation / Lightness.
     * https://www.chilliant.com/rgb2hsv.html
     * @return Array size 4 (HSV and alpha) all in the range 0..1
     */
    fun toHSVA(): FloatArray {
        val hcv = toHCV()
        val s = hcv[1] / (hcv[2] + epsilon)
        return floatArrayOf(hcv[0], s, hcv[2], alpha)
    }


    fun toHashRGB() = "#${twoDigitHex(red)}${twoDigitHex(green)}${twoDigitHex(blue)}"

    fun toHashRGBA() = "#${twoDigitHex(red)}${twoDigitHex(green)}${twoDigitHex(blue)}${twoDigitHex(alpha)}"

    // region ==== Object methods ====

    override fun equals(other: Any?): Boolean {
        if (other is Color) {
            return equals(other)
        }
        return false
    }

    fun equals(other: Color): Boolean {
        return other.red == red && other.green == green && other.blue == blue && other.alpha == alpha
    }

    override fun hashCode(): Int {
        return (red * 256).toInt() + (green * 256).toInt().shl(8) + (blue * 256).toInt().shl(16) +
            (alpha * 256).toInt().shl(24)
    }

    override fun toString() = if (alpha == 1f) toHashRGB() else toHashRGBA()

    // endregion Object methods

    // ==== Companion Object ====

    companion object {

        private const val epsilon = 1e-10f

        fun hsv(hue: Float, saturation: Float, value: Float, alpha: Float = 1f): Color {
            val hueColor = hue(hue)
            return Color(
                ((hueColor.red - 1) * saturation + 1) * value,
                ((hueColor.green - 1) * saturation + 1) * value,
                ((hueColor.blue - 1) * saturation + 1) * value,
                alpha
            )
        }

        /**
         * Create a color from hue / saturation / lightness / (alpha)
         * https://www.chilliant.com/rgb2hsv.html
         */
        fun hsl(hue: Float, saturation: Float, lightness: Float, alpha: Float = 1f): Color {
            val hueColor = hue(hue)
            val c = (1f - abs(2f * lightness - 1)) * saturation
            return Color(
                (hueColor.red - 0.5f) * c + lightness,
                (hueColor.green - 0.5f) * c + lightness,
                (hueColor.blue - 0.5f) * c + lightness,
                alpha
            )
        }

        fun hue(hue: Float) = Color(
            abs(hue * 6f - 3f) - 1,
            2f - abs(hue * 6f - 2),
            2f - abs(hue * 6f - 4)
        )

        fun fromString(str: String): Color {
            var color: String = str

            if (str.startsWith("#")) run {
                color = color.substring(1)
            }
            val len = color.length
            val r: Int
            val g: Int
            val b: Int
            val a: Int

            try {
                if (len == 3) {
                    r = color.substring(0, 1).toInt(16)
                    g = color.substring(1, 2).toInt(16)
                    b = color.substring(2, 3).toInt(16)
                    return Color(r / 15f, g / 15f, b / 15f, 1f)
                } else if (len == 4) {
                    r = color.substring(0, 1).toInt(16)
                    g = color.substring(1, 2).toInt(16)
                    b = color.substring(2, 3).toInt(16)
                    a = color.substring(3, 4).toInt(16)
                    return Color(r / 15f, g / 15f, b / 15f, a / 15f)
                } else if (len == 6) {
                    r = color.substring(0, 2).toInt(16)
                    g = color.substring(2, 4).toInt(16)
                    b = color.substring(4, 6).toInt(16)
                    return Color(r / 255f, g / 255f, b / 255f, 1f)
                } else if (len == 8) {
                    r = color.substring(0, 2).toInt(16)
                    g = color.substring(2, 4).toInt(16)
                    b = color.substring(4, 6).toInt(16)
                    a = color.substring(6, 8).toInt(16)
                    return Color(r / 255f, g / 255f, b / 255f, a / 255f)
                }
            } catch (e: Exception) {
                throw IllegalArgumentException("Not a valid color string", e)
            }
            throw IllegalArgumentException("Not a valid color string")
        }

        /**
         * A convenient way to create colors, or retrieve named colors.
         * If [str] starts with `#`, then it is parsed using [fromString].
         * Otherwise [str] is treated as a named color.
         *
         *
         *     Color["blue"]
         *     Color["#00f"]
         *     Color["#0000ff"]
         *
         * @throws IllegalArgumentException If a named color is not found, or [str] is not valid for [fromString].
         */
        operator fun get(str: String): Color {
            return if (str.startsWith("#")) {
                fromString(str)
            } else {
                namedColors[str.lowercase()] ?: throw IllegalArgumentException("Color not found : $str")
            }
        }

        fun find(str: String): Color? {
            return try {
                get(str)
            } catch (e: Exception) {
                null
            }
        }

        val TRANSPARENT = Color(1f, 1f, 1f, 0f)

        val TRANSPARENT_BLACK = Color(0f, 0f, 0f, 0f)

        val ALICE_BLUE = Color(0.9411765f, 0.972549f, 1.0f)

        val ANTIQUE_WHITE = Color(0.98039216f, 0.92156863f, 0.84313726f)

        val AQUA = Color(0.0f, 1.0f, 1.0f)

        val AQUA_MARINE = Color(0.49803922f, 1.0f, 0.83137256f)

        val AZURE = Color(0.9411765f, 1.0f, 1.0f)

        val BEIGE = Color(0.9607843f, 0.9607843f, 0.8627451f)

        val BISQUE = Color(1.0f, 0.89411765f, 0.76862746f)

        val BLACK = Color(0.0f, 0.0f, 0.0f)

        val BLANCHED_ALMOND = Color(1.0f, 0.92156863f, 0.8039216f)

        val BLUE = Color(0.0f, 0.0f, 1.0f)

        val BLUE_VIOLET = Color(0.5411765f, 0.16862746f, 0.8862745f)

        val BROWN = Color(0.64705884f, 0.16470589f, 0.16470589f)

        val BURLY_WOOD = Color(0.87058824f, 0.72156864f, 0.5294118f)

        val CADET_BLUE = Color(0.37254903f, 0.61960787f, 0.627451f)

        val CHARTREUSE = Color(0.49803922f, 1.0f, 0.0f)

        val CHOCOLATE = Color(0.8235294f, 0.4117647f, 0.11764706f)

        val CORAL = Color(1.0f, 0.49803922f, 0.3137255f)

        val CORNFLOWER_BLUE = Color(0.39215687f, 0.58431375f, 0.92941177f)

        val CORN_SILK = Color(1.0f, 0.972549f, 0.8627451f)

        val CRIMSON = Color(0.8627451f, 0.078431375f, 0.23529412f)

        val CYAN = Color(0.0f, 1.0f, 1.0f)

        val DARK_BLUE = Color(0.0f, 0.0f, 0.54509807f)

        val DARK_CYAN = Color(0.0f, 0.54509807f, 0.54509807f)

        val DARK_GOLDENROD = Color(0.72156864f, 0.5254902f, 0.043137256f)

        val DARK_GRAY = Color(0.6627451f, 0.6627451f, 0.6627451f)

        val DARK_GREEN = Color(0.0f, 0.39215687f, 0.0f)

        val DARK_GREY = DARK_GRAY

        val DARK_KHAKI = Color(0.7411765f, 0.7176471f, 0.41960785f)

        val DARK_MAGENTA = Color(0.54509807f, 0.0f, 0.54509807f)

        val DARK_OLIVE_GREEN = Color(0.33333334f, 0.41960785f, 0.18431373f)

        val DARK_ORANGE = Color(1.0f, 0.54901963f, 0.0f)

        val DARK_ORCHID = Color(0.6f, 0.19607843f, 0.8f)

        val DARK_RED = Color(0.54509807f, 0.0f, 0.0f)

        val DARK_SALMON = Color(0.9137255f, 0.5882353f, 0.47843137f)

        val DARK_SEA_GREEN = Color(0.56078434f, 0.7372549f, 0.56078434f)

        val DARK_SLATE_BLUE = Color(0.28235295f, 0.23921569f, 0.54509807f)

        val DARK_SLATE_GRAY = Color(0.18431373f, 0.30980393f, 0.30980393f)

        val DARK_SLATE_GREY = DARK_SLATE_GRAY

        val DARK_TURQUOISE = Color(0.0f, 0.80784315f, 0.81960785f)

        val DARK_VIOLET = Color(0.5803922f, 0.0f, 0.827451f)

        val DEEP_PINK = Color(1.0f, 0.078431375f, 0.5764706f)

        val DEEP_SKY_BLUE = Color(0.0f, 0.7490196f, 1.0f)

        val DIM_GRAY = Color(0.4117647f, 0.4117647f, 0.4117647f)

        val DIM_GREY = DIM_GRAY

        val DODGER_BLUE = Color(0.11764706f, 0.5647059f, 1.0f)

        val FIREBRICK = Color(0.69803923f, 0.13333334f, 0.13333334f)

        val FLORAL_WHITE = Color(1.0f, 0.98039216f, 0.9411765f)

        val FOREST_GREEN = Color(0.13333334f, 0.54509807f, 0.13333334f)

        val FUCHSIA = Color(1.0f, 0.0f, 1.0f)

        val GAINSBORO = Color(0.8627451f, 0.8627451f, 0.8627451f)

        val GHOST_WHITE = Color(0.972549f, 0.972549f, 1.0f)

        val GOLD = Color(1.0f, 0.84313726f, 0.0f)

        val GOLDENROD = Color(0.85490197f, 0.64705884f, 0.1254902f)

        val GRAY = Color(0.5019608f, 0.5019608f, 0.5019608f)

        val GREEN = Color(0.0f, 0.5019608f, 0.0f)

        val GREEN_YELLOW = Color(0.6784314f, 1.0f, 0.18431373f)

        val GREY = GRAY

        val HONEYDEW = Color(0.9411765f, 1.0f, 0.9411765f)

        val HOT_PINK = Color(1.0f, 0.4117647f, 0.7058824f)

        val INDIAN_RED = Color(0.8039216f, 0.36078432f, 0.36078432f)

        val INDIGO = Color(0.29411766f, 0.0f, 0.50980395f)

        val IVORY = Color(1.0f, 1.0f, 0.9411765f)

        val KHAKI = Color(0.9411765f, 0.9019608f, 0.54901963f)

        val LAVENDER = Color(0.9019608f, 0.9019608f, 0.98039216f)

        val LAVENDER_BLUSH = Color(1.0f, 0.9411765f, 0.9607843f)

        val LAWN_GREEN = Color(0.4862745f, 0.9882353f, 0.0f)

        val LEMON_CHIFFON = Color(1.0f, 0.98039216f, 0.8039216f)

        val LIGHT_BLUE = Color(0.6784314f, 0.84705883f, 0.9019608f)

        val LIGHT_CORAL = Color(0.9411765f, 0.5019608f, 0.5019608f)

        val LIGHT_CYAN = Color(0.8784314f, 1.0f, 1.0f)

        val LIGHT_GOLDENROD_YELLOW = Color(0.98039216f, 0.98039216f, 0.8235294f)

        val LIGHT_GRAY = Color(0.827451f, 0.827451f, 0.827451f)

        val LIGHT_GREEN = Color(0.5647059f, 0.93333334f, 0.5647059f)

        val LIGHT_GREY = LIGHT_GRAY

        val LIGHT_PINK = Color(1.0f, 0.7137255f, 0.75686276f)

        val LIGHT_SALMON = Color(1.0f, 0.627451f, 0.47843137f)

        val LIGHT_SEA_GREEN = Color(0.1254902f, 0.69803923f, 0.6666667f)

        val LIGHT_SKY_BLUE = Color(0.5294118f, 0.80784315f, 0.98039216f)

        val LIGHT_SLATE_GRAY = Color(0.46666667f, 0.53333336f, 0.6f)

        val LIGHT_SLATE_GREY = LIGHT_SLATE_GRAY

        val LIGHT_STEEL_BLUE = Color(0.6901961f, 0.76862746f, 0.87058824f)

        val LIGHT_YELLOW = Color(1.0f, 1.0f, 0.8784314f)

        val LIME = Color(0.0f, 1.0f, 0.0f)

        val LIME_GREEN = Color(0.19607843f, 0.8039216f, 0.19607843f)

        val LINEN = Color(0.98039216f, 0.9411765f, 0.9019608f)

        val MAGENTA = Color(1.0f, 0.0f, 1.0f)

        val MAROON = Color(0.5019608f, 0.0f, 0.0f)

        val MEDIUM_AQUAMARINE = Color(0.4f, 0.8039216f, 0.6666667f)

        val MEDIUM_BLUE = Color(0.0f, 0.0f, 0.8039216f)

        val MEDIUM_ORCHID = Color(0.7294118f, 0.33333334f, 0.827451f)

        val MEDIUM_PURPLE = Color(0.5764706f, 0.4392157f, 0.85882354f)

        val MEDIUM_SEA_GREEN = Color(0.23529412f, 0.7019608f, 0.44313726f)

        val MEDIUM_SLATE_BLUE = Color(0.48235294f, 0.40784314f, 0.93333334f)

        val MEDIUM_SPRING_GREEN = Color(0.0f, 0.98039216f, 0.6039216f)

        val MEDIUM_TURQUOISE = Color(0.28235295f, 0.81960785f, 0.8f)

        val MEDIUM_VIOLET_RED = Color(0.78039217f, 0.08235294f, 0.52156866f)

        val MIDNIGHT_BLUE = Color(0.09803922f, 0.09803922f, 0.4392157f)

        val MINT_CREAM = Color(0.9607843f, 1.0f, 0.98039216f)

        val MISTY_ROSE = Color(1.0f, 0.89411765f, 0.88235295f)

        val MOCCASIN = Color(1.0f, 0.89411765f, 0.70980394f)

        val NAVAJO_WHITE = Color(1.0f, 0.87058824f, 0.6784314f)

        val NAVY = Color(0.0f, 0.0f, 0.5019608f)

        val OLD_LACE = Color(0.99215686f, 0.9607843f, 0.9019608f)

        val OLIVE = Color(0.5019608f, 0.5019608f, 0.0f)

        val OLIVE_DRAB = Color(0.41960785f, 0.5568628f, 0.13725491f)

        val ORANGE = Color(1.0f, 0.64705884f, 0.0f)

        val ORANGE_RED = Color(1.0f, 0.27058825f, 0.0f)

        val ORCHID = Color(0.85490197f, 0.4392157f, 0.8392157f)

        val PALE_GOLDENROD = Color(0.93333334f, 0.9098039f, 0.6666667f)

        val PALE_GREEN = Color(0.59607846f, 0.9843137f, 0.59607846f)

        val PALE_TURQUOISE = Color(0.6862745f, 0.93333334f, 0.93333334f)

        val PALE_VIOLET_RED = Color(0.85882354f, 0.4392157f, 0.5764706f)

        val PAPAYA_WHIP = Color(1.0f, 0.9372549f, 0.8352941f)

        val PEACH_PUFF = Color(1.0f, 0.85490197f, 0.7254902f)

        val PERU = Color(0.8039216f, 0.52156866f, 0.24705882f)

        val PINK = Color(1.0f, 0.7529412f, 0.79607844f)

        val PLUM = Color(0.8666667f, 0.627451f, 0.8666667f)

        val POWDER_BLUE = Color(0.6901961f, 0.8784314f, 0.9019608f)

        val PURPLE = Color(0.5019608f, 0.0f, 0.5019608f)

        val RED = Color(1.0f, 0.0f, 0.0f)

        val ROSY_BROWN = Color(0.7372549f, 0.56078434f, 0.56078434f)

        val ROYAL_BLUE = Color(0.25490198f, 0.4117647f, 0.88235295f)

        val SADDLE_BROWN = Color(0.54509807f, 0.27058825f, 0.07450981f)

        val SALMON = Color(0.98039216f, 0.5019608f, 0.44705883f)

        val SANDY_BROWN = Color(0.95686275f, 0.6431373f, 0.3764706f)

        val SEA_GREEN = Color(0.18039216f, 0.54509807f, 0.34117648f)

        val SEASHELL = Color(1.0f, 0.9607843f, 0.93333334f)

        val SIENNA = Color(0.627451f, 0.32156864f, 0.1764706f)

        val SILVER = Color(0.7529412f, 0.7529412f, 0.7529412f)

        val SKY_BLUE = Color(0.5294118f, 0.80784315f, 0.92156863f)

        val SLATE_BLUE = Color(0.41568628f, 0.3529412f, 0.8039216f)

        val SLATE_GRAY = Color(0.4392157f, 0.5019608f, 0.5647059f)

        val SLATE_GREY = SLATE_GRAY

        val SNOW = Color(1.0f, 0.98039216f, 0.98039216f)

        val SPRING_GREEN = Color(0.0f, 1.0f, 0.49803922f)

        val STEEL_BLUE = Color(0.27450982f, 0.50980395f, 0.7058824f)

        val TAN = Color(0.8235294f, 0.7058824f, 0.54901963f)

        val TEAL = Color(0.0f, 0.5019608f, 0.5019608f)

        val THISTLE = Color(0.84705883f, 0.7490196f, 0.84705883f)

        val TOMATO = Color(1.0f, 0.3882353f, 0.2784314f)

        val TURQUOISE = Color(0.2509804f, 0.8784314f, 0.8156863f)

        val VIOLET = Color(0.93333334f, 0.50980395f, 0.93333334f)

        val WHEAT = Color(0.9607843f, 0.87058824f, 0.7019608f)

        val WHITE = Color(1.0f, 1.0f, 1.0f)

        val WHITE_SMOKE = Color(0.9607843f, 0.9607843f, 0.9607843f)

        val YELLOW = Color(1.0f, 1.0f, 0.0f)

        val YELLOW_GREEN = Color(0.6039216f, 0.8039216f, 0.19607843f)

        val namedColors: Map<String, Color> = mapOf(
            "aliceblue" to ALICE_BLUE,
            "antiquewhite" to ANTIQUE_WHITE,
            "aqua" to AQUA,
            "aquamarine" to AQUA_MARINE,
            "azure" to AZURE,
            "beige" to BEIGE,
            "bisque" to BISQUE,
            "black" to BLACK,
            "blanchedalmond" to BLANCHED_ALMOND,
            "blue" to BLUE,
            "blueviolet" to BLUE_VIOLET,
            "brown" to BROWN,
            "burlywood" to BURLY_WOOD,
            "cadetblue" to CADET_BLUE,
            "chartreuse" to CHARTREUSE,
            "chocolate" to CHOCOLATE,
            "coral" to CORAL,
            "cornflowerblue" to CORNFLOWER_BLUE,
            "cornsilk" to CORN_SILK,
            "crimson" to CRIMSON,
            "cyan" to CYAN,
            "darkblue" to DARK_BLUE,
            "darkcyan" to DARK_CYAN,
            "darkgoldenrod" to DARK_GOLDENROD,
            "darkgray" to DARK_GRAY,
            "darkgreen" to DARK_GREEN,
            "darkgrey" to DARK_GREY,
            "darkkhaki" to DARK_KHAKI,
            "darkmagenta" to DARK_MAGENTA,
            "darkolivegreen" to DARK_OLIVE_GREEN,
            "darkorange" to DARK_ORANGE,
            "darkorchid" to DARK_ORCHID,
            "darkred" to DARK_RED,
            "darksalmon" to DARK_SALMON,
            "darkseagreen" to DARK_SEA_GREEN,
            "darkslateblue" to DARK_SLATE_BLUE,
            "darkslategray" to DARK_SLATE_GRAY,
            "darkslategrey" to DARK_SLATE_GREY,
            "darkturquoise" to DARK_TURQUOISE,
            "darkviolet" to DARK_VIOLET,
            "deeppink" to DEEP_PINK,
            "deepskyblue" to DEEP_SKY_BLUE,
            "dimgray" to DIM_GRAY,
            "dimgrey" to DIM_GREY,
            "dodgerblue" to DODGER_BLUE,
            "firebrick" to FIREBRICK,
            "floralwhite" to FLORAL_WHITE,
            "forestgreen" to FOREST_GREEN,
            "fuchsia" to FUCHSIA,
            "gainsboro" to GAINSBORO,
            "ghostwhite" to GHOST_WHITE,
            "gold" to GOLD,
            "goldenrod" to GOLDENROD,
            "gray" to GRAY,
            "green" to GREEN,
            "greenyellow" to GREEN_YELLOW,
            "grey" to GREY,
            "honeydew" to HONEYDEW,
            "hotpink" to HOT_PINK,
            "indianred" to INDIAN_RED,
            "indigo" to INDIGO,
            "ivory" to IVORY,
            "khaki" to KHAKI,
            "lavender" to LAVENDER,
            "lavenderblush" to LAVENDER_BLUSH,
            "lawngreen" to LAWN_GREEN,
            "lemonchiffon" to LEMON_CHIFFON,
            "lightblue" to LIGHT_BLUE,
            "lightcoral" to LIGHT_CORAL,
            "lightcyan" to LIGHT_CYAN,
            "lightgoldenrodyellow" to LIGHT_GOLDENROD_YELLOW,
            "lightgray" to LIGHT_GRAY,
            "lightgreen" to LIGHT_GREEN,
            "lightgrey" to LIGHT_GREY,
            "lightpink" to LIGHT_PINK,
            "lightsalmon" to LIGHT_SALMON,
            "lightseagreen" to LIGHT_SEA_GREEN,
            "lightskyblue" to LIGHT_SKY_BLUE,
            "lightslategray" to LIGHT_SLATE_GRAY,
            "lightslategrey" to LIGHT_SLATE_GREY,
            "lightsteelblue" to LIGHT_STEEL_BLUE,
            "lightyellow" to LIGHT_YELLOW,
            "lime" to LIME,
            "limegreen" to LIME_GREEN,
            "linen" to LINEN,
            "magenta" to MAGENTA,
            "maroon" to MAROON,
            "mediumaquamarine" to MEDIUM_AQUAMARINE,
            "mediumblue" to MEDIUM_BLUE,
            "mediumorchid" to MEDIUM_ORCHID,
            "mediumpurple" to MEDIUM_PURPLE,
            "mediumseagreen" to MEDIUM_SEA_GREEN,
            "mediumslateblue" to MEDIUM_SLATE_BLUE,
            "mediumspringgreen" to MEDIUM_SPRING_GREEN,
            "mediumturquoise" to MEDIUM_TURQUOISE,
            "mediumvioletred" to MEDIUM_VIOLET_RED,
            "midnightblue" to MIDNIGHT_BLUE,
            "mintcream" to MINT_CREAM,
            "mistyrose" to MISTY_ROSE,
            "moccasin" to MOCCASIN,
            "navajowhite" to NAVAJO_WHITE,
            "navy" to NAVY,
            "oldlace" to OLD_LACE,
            "olive" to OLIVE,
            "olivedrab" to OLIVE_DRAB,
            "orange" to ORANGE,
            "orangered" to ORANGE_RED,
            "orchid" to ORCHID,
            "palegoldenrod" to PALE_GOLDENROD,
            "palegreen" to PALE_GREEN,
            "paleturquoise" to PALE_TURQUOISE,
            "palevioletred" to PALE_VIOLET_RED,
            "papayawhip" to PAPAYA_WHIP,
            "peachpuff" to PEACH_PUFF,
            "peru" to PERU,
            "pink" to PINK,
            "plum" to PLUM,
            "powderblue" to POWDER_BLUE,
            "purple" to PURPLE,
            "red" to RED,
            "rosybrown" to ROSY_BROWN,
            "royalblue" to ROYAL_BLUE,
            "saddlebrown" to SADDLE_BROWN,
            "salmon" to SALMON,
            "sandybrown" to SANDY_BROWN,
            "seagreen" to SEA_GREEN,
            "seashell" to SEASHELL,
            "sienna" to SIENNA,
            "silver" to SILVER,
            "skyblue" to SKY_BLUE,
            "slateblue" to SLATE_BLUE,
            "slategray" to SLATE_GRAY,
            "slategrey" to SLATE_GREY,
            "snow" to SNOW,
            "springgreen" to SPRING_GREEN,
            "steelblue" to STEEL_BLUE,
            "tan" to TAN,
            "teal" to TEAL,
            "thistle" to THISTLE,
            "tomato" to TOMATO,
            "transparent" to TRANSPARENT,
            "transparentBlack" to TRANSPARENT_BLACK,
            "turquoise" to TURQUOISE,
            "violet" to VIOLET,
            "wheat" to WHEAT,
            "white" to WHITE,
            "whitesmoke" to WHITE_SMOKE,
            "yellow" to YELLOW,
            "yellowgreen" to YELLOW_GREEN
        )

    }
}

private val hexDigits = "0123456789abcdef"
private fun twoDigitHex(channel: Float): String {
    val value = (channel * 255).toInt()
    val first = value / 16
    val second = value % 16
    return "${hexDigits[first]}${hexDigits[second]}"
}

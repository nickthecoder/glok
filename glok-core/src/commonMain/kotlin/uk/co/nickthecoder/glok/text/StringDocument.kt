/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.history.*
import uk.co.nickthecoder.glok.property.PropertyBase
import uk.co.nickthecoder.glok.property.PropertyException
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty

/**
 * The [HistoryDocument] used by [TextField].
 */
class StringDocument(initialText: String) : HistoryDocumentBase() {

    /**
     * [textProperty] is lazily evaluated, but we need to know if it is bound.
     * So we check this first before using `textProperty.isBound()`.
     */
    private var textPropertyCreated = false

    var text: String = initialText
        private set //  Only [StringChange] implementations should change this.

    val textProperty: StringProperty by lazy {
        textPropertyCreated = true
        TextProperty()
    }

    val length: Int get() = text.length

    private fun validateFromTo(from: Int, to: Int): Boolean {
        if (from < 0) throw IllegalArgumentException("`from` out of bounds : $from")
        if (to > text.length) throw IllegalArgumentException("`to` out of bounds : $to")
        if (from > to) throw IllegalArgumentException("`from` cannot be before `to` : $from .. $to")
        return (from == to)
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun clearChange(): DeleteString? {
        if (textPropertyCreated && textProperty.isBound()) throw PropertyException(textProperty, "Cannot change a bound value")
        val from = 0
        val to = text.length
        if (validateFromTo(from, to)) return null
        return DeleteStringImpl(from, to, false, "Clear")
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun deleteChange(from: Int, to: Int, allowMerge: Boolean): DeleteString? {
        if (textPropertyCreated && textProperty.isBound()) throw PropertyException(textProperty, "Cannot change a bound value")
        if (validateFromTo(from, to)) return null
        return DeleteStringImpl(from, to, allowMerge)
    }

    /**
     * Returns a [TextChange] object, but does not apply that change.
     * Useful for batching multiple changes together so that undo/redo treats them as a single step.
     */
    fun insertChange(from: Int, toInsert: String, allowMerge: Boolean): InsertString? {
        if (textPropertyCreated && textProperty.isBound()) throw PropertyException(textProperty, "Cannot change a bound value")
        if (toInsert.isEmpty()) return null
        return InsertStringImpl(from, toInsert, allowMerge)
    }

    fun insert(from: Int, toInsert: String, allowMerge: Boolean = false) {
        insertChange(from, toInsert, allowMerge)?.now()
    }

    fun delete(from: Int, to: Int, allowMerge: Boolean = false) {
        deleteChange(from, to, allowMerge)?.now()
    }

    fun clear() {
        clearChange()?.now()
    }

    fun replace(from: Int, to: Int, text: String) {
        history.batch("Replace") {
            + deleteChange(from, to, false)
            + insertChange(from, text, false)
        }
    }

    fun setText(text: String) {
        history.batch("Set Text") {
            + clearChange()
            + insertChange(0, text, false)
        }
    }

    // region ==== TextProperty ====

    /**
     * A [StringProperty] for [StringDocument]'s [text].
     * If the value is changed, we do _not_ change [text] directly, but instead use [StringDocument.setText],
     * which adds a new batch to [History].
     *
     * It also takes care of changes to the value when the property is bound.
     */
    private inner class TextProperty : PropertyBase<String>(), StringProperty, DocumentListener {

        override val bean get() = this@StringDocument
        override val beanName: String get() = "text"

        private var cachedValue: String? = text

        override var value: String
            get() {
                return boundTo?.value ?: validateCache()
            }
            set(newText) {
                if (boundTo != null) {
                    throw PropertyException(this, "Cannot set a bound property : $this")
                }
                setText(newText)
            }

        init {
            this@StringDocument.addListener(this)
        }

        private fun validateCache(): String {
            var result = cachedValue
            if (result == null) {
                result = text
                cachedValue = result
            }
            return result
        }

        override fun boundValueChanged(old: String, new: String) {
            history.batch("Set Text") {
                + DeleteStringImpl(0, old.length, false)
                if (new.isNotEmpty()) {
                    + InsertStringImpl(0, new, false)
                }
            }
            super.boundValueChanged(old, new)
        }

        override fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean) {
            val oldValue = cachedValue ?: text
            val newValue = text
            cachedValue = newValue
            fire(oldValue, newValue)
        }
    }

    // endregion TextProperty

    // region == InsertStringImpl ==
    private inner class InsertStringImpl(
        override var from: Int,
        override var str: String,
        val allowMerge: Boolean,
        override val label: String = "Insert"
    ) : InsertString, ChangeImpl {

        override val document: StringDocument get() = this@StringDocument

        override var to = from + str.length

        init {
            if (from < 0 || from > text.length) throw IndexOutOfBoundsException("from ($from) must be in the range 0 .. ${text.length}")
        }

        override fun redo() {
            document.text = document.text.substring(0, from) + str + document.text.substring(from)
        }

        /**
         * Copy/pasted from DeleteTextImpl, swapping undo/redo, and deleted/inserted
         */
        override fun undo() {
            document.text = document.text.substring(0, from) + document.text.substring(to)
        }

        override fun canMergeWith(previous: Change): Boolean {
            return allowMerge && previous is InsertStringImpl && previous.allowMerge && (previous.from == this.to || previous.to == this.from)
        }

        override fun mergeWith(previous: Change) {
            previous as InsertStringImpl
            if (previous.to == this.from) {

                // Merged insert after. e.g. typing successive characters.
                previous.to = this.to
                previous.str += this.str


            } else if (previous.from == this.to) {
                // Merged insert before. Not so useful. Type char, left, then type again???
                previous.from = this.from
                previous.str = this.str + previous.str

            } else {
                // canMergeWith should prevent us getting here.
                throw IllegalStateException()
            }
        }

        override fun toString() = "InsertString $from .. $to $str"

    }
    // endregion InsertStringImpl

    // region DeleteStringImpl
    private inner class DeleteStringImpl(
        override var from: Int,
        override var to: Int,
        val allowMerge: Boolean,
        override val label: String = "Delete",
    ) : DeleteString, ChangeImpl {

        override val document: StringDocument get() = this@StringDocument

        override var str = document.text.substring(from, to)

        init {
            if (from < 0 || from > text.length) throw IndexOutOfBoundsException("from ($from) must be in the range 0 .. ${text.length}")
            if (to < 0 || to > text.length) throw IndexOutOfBoundsException("to ($to) must be in the range 0 .. ${text.length}")
            if (from > to) throw IllegalArgumentException("From is after to")
        }

        override fun redo() {
            document.text = document.text.substring(0, from) + document.text.substring(to)
        }

        override fun undo() {
            document.text = document.text.substring(0, from) + str + document.text.substring(from)
        }

        override fun canMergeWith(previous: Change): Boolean {
            return allowMerge && previous is DeleteStringImpl && previous.allowMerge && (previous.from == this.from || previous.from == this.to)
        }

        override fun mergeWith(previous: Change) {
            previous as DeleteStringImpl
            if (previous.from == this.from) {

                // successive DELETE key presses.
                previous.to += this.str.length
                previous.str += this.str


            } else if (previous.from == this.to) {

                // e.g. successive BACKSPACE key presses.
                // "Hello" BACKSPACE BACKSPACE -> previous.from = 4 and this.to = 4
                // We want the previous to for from 3 to 5, with str="lo"
                previous.from = this.from
                previous.str = this.str + previous.str

            } else {
                // canMergeWith should prevent us getting here.
                throw IllegalStateException()
            }
        }

        override fun toString() = "DeleteString $from .. $to $str"
    }
    // endregion DeleteStringImpl
}

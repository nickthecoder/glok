/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.dsl.pane
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyle
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.HORIZONTAL
import uk.co.nickthecoder.glok.theme.styles.PROGRESS
import uk.co.nickthecoder.glok.theme.styles.PROGRESS_BAR
import uk.co.nickthecoder.glok.theme.styles.VERTICAL
import uk.co.nickthecoder.glok.util.clamp

/**
 * Displays a value (called [progress]) in the range 0..1 as a bar.
 * The appearance of the bar is given by the scene's Theme.
 *
 * The Tantalum theme includes two sets of rules. If you add the style ".fat",
 * then the progress bar is thick enough, that labels can be placed on top of it
 * (using a [StackPane]).
 */
class ProgressBar : Region() {

    // region Properties
    /**
     * The progress in the range 0..1
     *
     * There is nothing preventing this value being outside the range 0..1,
     * and any value over 1 will be displayed the same as 1.
     */
    val progressProperty by floatProperty(0f)
    var progress by progressProperty

    /**
     * A read-only version of [progressProperty] clamped to 0..1
     */
    val clampedProgressProperty: ObservableFloat = FloatUnaryFunction(progressProperty) { it.clamp(0f, 1f) }
    val clampedProgress by clampedProgressProperty

    /**
     * The default orientation is horizontal.
     * When horizontal, the bar extends from left to right.
     * When vertical, the bar extends from the bottom to the top.
     */
    val orientationProperty by orientationProperty(Orientation.HORIZONTAL)
    var orientation by orientationProperty

    /**
     * The preferred length of the bar.
     * i.e. the preferred width for this node, when [orientation] == horizontal,
     * and the preferred height when vertical.
     *
     * If you set [growPriority], and place this inside a Node which changes size, then
     * the ProgressBar will also change length.
     *
     * The preferred size in the other direction (i.e. the thickness) is governed by the Theme.
     *
     * If you don't like the default thickness, then create your own theme (combined with Tantalum),
     * and set the preferred Width/Height and the radius of the backgrounds appropriately.
     * FYI, It is possible to change the thickness on an ad-hoc basis (without using a Theme),
     * but it isn't recommended.
     *
     * As this is Stylable, you can have the Theme set the default length.
     * Tantalum set the length to 200.
     */
    val lengthProperty by stylableFloatProperty(0f)
    var length by lengthProperty

    // endregion Properties

    // region Nodes
    private val progressRegion = pane {
        style(PROGRESS)
    }

    override val children: ObservableList<Node> = listOf(progressRegion).asObservableList()
    // endregion Nodes

    // region init
    init {
        style(PROGRESS_BAR)
        pseudoStyle(HORIZONTAL)
        claimChildren()

        orientationProperty.addChangeListener { _, _, newValue ->
            pseudoStyleIf(newValue == Orientation.VERTICAL, VERTICAL)
            pseudoStyleIf(newValue == Orientation.HORIZONTAL, HORIZONTAL)
            requestLayout()
        }
        clampedProgressProperty.addListener { requestLayout() }
    }
    // endregion init

    // region Layout

    override fun nodePrefWidth(): Float {
        return if (orientation == Orientation.HORIZONTAL) {
            length
        } else {
            0f // The theme must supply a rule for setting the prefWidth
        }
    }

    override fun nodePrefHeight(): Float {
        return if (orientation == Orientation.VERTICAL) {
            length
        } else {
            0f // The theme must supply a rule for setting the prefHeight
        }
    }

    override fun layoutChildren() {
        if (orientation == Orientation.HORIZONTAL) {
            val size = (width - surroundX()) * clampedProgress
            setChildBounds(progressRegion, surroundLeft(), surroundTop(), size, height - surroundY())
        } else {
            val size = (height - surroundY()) * clampedProgress
            // Note, the progress fills from the bottom upwards
            setChildBounds(progressRegion, surroundLeft(), surroundTop() + height - size, width - surroundX(), size)
        }
    }

    // endregion Layout
}

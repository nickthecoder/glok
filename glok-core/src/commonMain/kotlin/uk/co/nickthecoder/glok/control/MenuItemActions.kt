/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.Key

object MenuItemActions : Actions(null) {

    val ACTIVATE = define("activate", "Activate", Key.ENTER.noMods()) {
        add(Key.SPACE.noMods())
    }

    /**
     * Navigate forwards in the menu tree structure.
     * For a [SubMenu], this will open the open.
     * For other menu items, it will bubble up the tree till it gets to a Menu,
     * where it will cause the next Menu in the MenuBar to be selected.
     */
    val FORWARDS = define("forwards", "Forwards", Key.RIGHT.noMods())

    /**
     * Navigate backwards in the menu tree structure.
     * For a PopupMenu, it just closes it.
     * Otherwise, it will bubble up the tree till it gets to a Menu,
     * where it will cause the previous Menu in the MenuBar to be selected.
     */
    val BACKWARDS = define("forwards", "Forwards", Key.LEFT.noMods())
}

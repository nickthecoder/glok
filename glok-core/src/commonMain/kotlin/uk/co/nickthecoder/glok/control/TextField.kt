package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.event.KeyTypedEvent
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.max
import uk.co.nickthecoder.glok.property.functions.min
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.functions.observableTrue
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.text.*
import uk.co.nickthecoder.glok.theme.styles.TEXT_FIELD
import uk.co.nickthecoder.glok.util.Clipboard
import uk.co.nickthecoder.glok.util.clamp

/**
 * A single-line text editor. For multiple lines, use [TextArea].
 *
 * ### Theme DSL
 *
 *     "text_field" {
 *
 *         columns( value : Number )
 *
 *         caretImage( image : Image )
 *         caretImage( images : NamedImages, imageName : String )
 *
 *         font( value : Font )
 *
 *         selectionColor( value : Color )
 *         selectionColor( value : String )
 *
 *         textColor( value : Color )
 *         textColor( value : String )
 *
 *     }
 *
 * TextField inherits all the features of [Region].
 */
class TextField(val document: StringDocument) : TextInputControl() {

    constructor(text: String = "") : this(StringDocument(text))
    constructor(stringProperty: StringProperty) : this("") {
        textProperty.bindTo(stringProperty)
    }

    // region ==== Properties ====

    /**
     * If you bind this property, you should also set [readOnly] to true, as Themes may style a read-only
     * TextArea differently. If this is bound, then the TextArea cannot be altered by the user
     * regardless of [readOnly].
     */
    val textProperty by lazy { document.textProperty }
    var text: String
        get() = document.text
        set(v) {
            document.setText(v)
            document.history.clear()
        }

    /**
     * Text to display when [text] is blank.
     */
    val promptTextProperty by stringProperty("")
    var promptText by promptTextProperty

    val promptTextColorProperty by stylableColorProperty(Color.WHITE)
    var promptTextColor by promptTextColorProperty


    val undoableProperty = document.history.undoableProperty
    val undoable by undoableProperty

    val redoableProperty = document.history.redoableProperty
    val redoable by redoableProperty

    val caretIndexProperty by validatedIntProperty(0) { _, _, newValue ->
        newValue.clamp(0, text.length)
    }
    var caretIndex by caretIndexProperty

    /**
     * Along with [caretIndex], this defines the extent of the selection.
     * If [anchorIndex] == [caretIndex], then nothing is selected.
     *
     * Note that [anchorIndex] can be greater than, equal to or less than [caretIndex].
     */
    val anchorIndexProperty by validatedIntProperty(0) { _, _, newValue ->
        newValue.clamp(0, text.length)
    }
    var anchorIndex by anchorIndexProperty

    /**
     * True iff [caretIndex] != [anchorIndex]
     */
    override val textSelectedProperty: ObservableBoolean = caretIndexProperty.notEqualTo(anchorIndexProperty)
    override val textSelected by textSelectedProperty
    private val nothingSelectedProperty = ! textSelectedProperty

    /**
     * The minimum of [anchorIndex] and [caretIndex]
     */
    val selectionStartProperty = min(anchorIndexProperty, caretIndexProperty)
    val selectionStart by selectionStartProperty

    /**
     * The maximum of [anchorIndex] and [caretIndex]
     */
    val selectionEndProperty = max(anchorIndexProperty, caretIndexProperty)
    val selectionEnd by selectionEndProperty


    val prefColumnCountProperty by stylableIntProperty(20)
    var prefColumnCount by prefColumnCountProperty

    val minColumnCountProperty by stylableIntProperty(1)
    var minColumnCount by minColumnCountProperty

    /**
     * If set to true, the [prefColumnCount] and [minColumnCount] will use the widths of digits,
     * rather than the letter `W` to calculate [evalPrefWidth] and [evalMinWidth].
     */
    val expectDigitsProperty by booleanProperty(false)
    var expectDigits by expectDigitsProperty

    // endregion  properties

    // region ==== Fields ====

    /**
     * For scrolling the text when it doesn't fit within the bounds.
     */
    private var offsetX = 0f

    @Suppress("unused")
    private val documentListener = document.addWeakListener { _, change, isUndo ->
        if (change is StringChange) updatePositions(change, isUndo)
        checkOffset()
        requestRedraw()
    }

    // endregion fields

    // region ==== Commands ====
    private val commands = Commands().apply {
        with(TextAreaActions) {

            SELECT_ALL {
                caretIndex = 0
                anchorIndex = text.length
            }

            SELECT_NONE {
                caretIndex = 0
                anchorIndex = 0
            }.disable(nothingSelectedProperty)

            LEFT {
                if (caretIndex == anchorIndex) {
                    if (caretIndex > 0) {
                        caretIndex --
                    }
                } else {
                    caretIndex = selectionStart
                }
                anchorIndex = caretIndex
                resetBlink()
            }
            SELECT_LEFT {
                if (caretIndex > 0) {
                    caretIndex --
                }
                resetBlink()
            }
            SKIP_LEFT {
                skipWordBackwards()
                anchorIndex = caretIndex
                resetBlink()
            }
            SELECT_SKIP_LEFT {
                skipWordBackwards()
                resetBlink()
            }

            RIGHT {
                if (caretIndex == anchorIndex) {
                    if (caretIndex < text.length) {
                        caretIndex ++
                    }
                } else {
                    caretIndex = selectionEnd
                }
                anchorIndex = caretIndex
                resetBlink()
            }
            SELECT_RIGHT {
                if (caretIndex < text.length) {
                    caretIndex ++
                }
                resetBlink()
            }
            SKIP_RIGHT {
                skipWordForwards()
                anchorIndex = caretIndex
                resetBlink()
            }
            SELECT_SKIP_RIGHT {
                skipWordForwards()
                resetBlink()
            }

            START_OF_LINE {
                caretIndex = 0
                anchorIndex = 0
                resetBlink()
            }
            SELECT_START_OF_LINE {
                caretIndex = 0
                resetBlink()
            }

            END_OF_LINE {
                caretIndex = text.length
                anchorIndex = caretIndex
                resetBlink()
            }
            SELECT_END_OF_LINE {
                caretIndex = text.length
                resetBlink()
            }

            // These are disabled, as they are only applicable to TextAreaBase
            // It is important that these are disabled when the TextField is part of a Spinner.
            // If we didn't disable them, then the event is consumed here, and the Spinner won't hear the
            // Ctrl+Home or Ctrl+End events.
            START_OF_DOCUMENT { }.disable(observableTrue)
            SELECT_START_OF_DOCUMENT { }.disable(observableTrue)
            END_OF_DOCUMENT { }.disable(observableTrue)
            SELECT_END_OF_DOCUMENT { }.disable(observableTrue)

            COPY { Clipboard.textContents = selection() }.disable(nothingSelectedProperty)

            // commands which alter the document...

            UNDO {
                if (canChange()) {
                    document.history.undo(false)
                    resetBlink()
                }
            }.disable(! undoableProperty)

            REDO {
                if (canChange()) {
                    document.history.redo(false)
                    resetBlink()
                }
            }.disable(! redoableProperty)

            CUT {
                if (textSelected && canChange()) {
                    Clipboard.textContents = selection()
                    document.delete(selectionStart, selectionEnd)
                    resetBlink()
                }
            }.disable(nothingSelectedProperty)

            PASTE {
                if (canChange()) {
                    Clipboard.textContents?.let { toPaste ->
                        val from = selectionStart
                        if (textSelected) {
                            document.replace(selectionStart, selectionEnd, toPaste)
                        } else {
                            document.insert(caretIndex, toPaste)
                        }

                        caretIndex = from + toPaste.length
                        anchorIndex = caretIndex
                        resetBlink()
                    }
                }
            }.disable(Clipboard.noTextProperty)

            DUPLICATE {
                if (textSelected && canChange()) {
                    val start = selectionStart
                    val toPaste = text.substring(selectionStart, selectionEnd)
                    document.insert(selectionEnd, toPaste)

                    caretIndex = start + toPaste.length
                    anchorIndex = start
                    resetBlink()
                }
            }.disable(nothingSelectedProperty)

            DELETE {
                if (canChange()) {

                    if (textSelected) {
                        val from = selectionStart
                        val to = selectionEnd
                        caretIndex = from
                        anchorIndex = from
                        document.delete(from, to)
                    } else {
                        if (caretIndex < text.length) {
                            document.delete(caretIndex, caretIndex + 1, true)
                        }
                    }
                    resetBlink()
                }
            }

            BACKSPACE {
                if (canChange()) {
                    if (textSelected) {
                        val from = selectionStart
                        val to = selectionEnd
                        caretIndex = from
                        anchorIndex = from
                        document.delete(from, to)
                    } else {
                        if (caretIndex > 0) {
                            caretIndex --
                            anchorIndex = caretIndex
                            document.delete(caretIndex, caretIndex + 1, true)
                        }
                    }
                    resetBlink()
                }
            }
        }
        attachTo(this@TextField)
    }

    //endregion commands

    // region ==== init ====
    init {
        style(TEXT_FIELD)

        for (prop in listOf(
            textProperty, caretIndexProperty, anchorIndexProperty
        )) {
            prop.addListener(requestRedrawListener)
        }
        prefColumnCountProperty.addListener(requestLayoutListener)
        expectDigitsProperty.addListener(requestLayoutListener)

        onMousePressed { mousePressed(it) }
        onMouseClicked { mouseClicked(it) }
        onMouseDragged { mouseDragged(it) }
        onKeyTyped { keyTyped(it) }
        onPopupTrigger { showContextMenu(it, commands, canChange()) }

    }
    // endregion init

    // region ==== Public Methods ====

    fun selection() = text.substring(selectionStart, selectionEnd)

    fun selectAll() {
        anchorIndex = 0
        caretIndex = text.length
    }

    fun undo() = document.history.undo()
    fun redo() = document.history.redo()

    fun delete(from: Int, to: Int) = document.delete(from, to)
    fun insert(from: Int, str: String) = document.insert(from, str)
    fun replace(from: Int, to: Int, str: String) = document.replace(from, to, str)
    fun clear() = document.clear()

    // endregion public methods

    // region ==== private Methods ====

    private fun canChange() = ! (readOnly || textProperty.isBound())

    /**
     * When the document changes, ensure that [caretIndex] and [anchorIndex] are adjusted accordingly.
     */
    private fun updatePositions(change: StringChange, isUndo: Boolean) {
        fun adjustDelete(index: Int, from: Int, to: Int): Int {
            return if (index <= from) {
                // Before the deletion
                index
            } else if (index <= to) {
                // Inside the deletion
                from
            } else {
                // After the deletion
                index - (to - from)
            }
        }

        fun adjustInsert(index: Int, from: Int, inserted: String): Int {
            return if (index <= from) {
                // Before the insertion
                index
            } else {
                index + (inserted.length)
            }
        }
        when (change) {
            is DeleteString -> {
                if (isUndo) {
                    caretIndex = adjustInsert(caretIndex, change.from, change.str)
                    anchorIndex = adjustInsert(anchorIndex, change.from, change.str)
                } else {
                    caretIndex = adjustDelete(caretIndex, change.from, change.to)
                    anchorIndex = adjustDelete(anchorIndex, change.from, change.to)
                }
            }

            is InsertString -> {
                if (isUndo) {
                    caretIndex = adjustDelete(caretIndex, change.from, change.to)
                    anchorIndex = adjustDelete(anchorIndex, change.from, change.to)
                } else {
                    caretIndex = adjustInsert(caretIndex, change.from, change.str)
                    anchorIndex = adjustInsert(anchorIndex, change.from, change.str)
                }
            }
        }
    }

    private fun selectWord() {
        val (left, right) = SkipWord.instance.selectWord(caretIndex, text)
        caretIndex = left
        anchorIndex = right
    }

    private fun skipWordForwards() {
        caretIndex = SkipWord.instance.forwards(caretIndex, text)
    }

    private fun skipWordBackwards() {
        caretIndex = SkipWord.instance.backwards(caretIndex, text)
    }

    // endregion private methods

    // region ==== Events ====

    private fun mouseClicked(event: MouseEvent) {
        if (event.clickCount == 2 && event.isPrimary) {
            selectWord()
        } else if (event.clickCount == 3) {
            // Select all
            caretIndex = 0
            anchorIndex = text.length
        }
    }

    private fun mousePressed(event: MouseEvent) {
        if (event.isPrimary) {
            caretIndex = font.caretPosition(
                text, event.sceneX - sceneX - surroundLeft() - offsetX,
                indentation.columns
            )
            if (!event.isShiftDown) {
                anchorIndex = caretIndex
            }
            event.capture()
            firstToRoot { it.focusAcceptable }?.requestFocus()
            // Prevent a node higher up the node-tree acting on this event
            // First added to prevent a ListView with an IntSpinner in the cells from stealing focus from the IntSpinner.
            event.consume()
        }
    }

    private fun mouseDragged(event: MouseEvent) {
        if (event.isPrimary) {
            caretIndex = font.caretPosition(
                text, event.sceneX - sceneX - surroundLeft() - offsetX,
                indentation.columns
            )
        }
    }

    /**
     * If the text is now small enough to fit within the bounds, adjust the offset.
     */
    private fun checkOffset() {
        if (offsetX != 0f) {
            val textWidth = widthOf(text)
            if (textWidth < width - surroundX()) {
                offsetX = 0f
            } else {
                val right = offsetX + textWidth
                if (right < width - surroundX()) {
                    offsetX += (width - surroundX()) - right
                }
            }
        }
    }

    private fun keyTyped(event: KeyTypedEvent) {
        if (canChange()) {
            val replacement = event.char.toString()

            val from = selectionStart
            if (textSelected) {
                document.replace(from, selectionEnd, replacement)
            } else {
                document.insert(from, replacement, true)
            }
            caretIndex = from + 1
            anchorIndex = caretIndex
            resetBlink()
        }
    }



    // endregion events

    // region ==== Layout ====

    override fun nodeMinWidth() = surroundX() + minColumnCount * (if (expectDigits) font.maxWidthOfDigit else widthOf("W"))
    override fun nodeMinHeight() = nodePrefHeight()

    override fun nodePrefWidth() = surroundX() + prefColumnCount * (if (expectDigits) font.maxWidthOfDigit else widthOf("W"))
    override fun nodePrefHeight() = font.height + surroundY()

    override fun draw() {
        super.draw()
        val text = text

        val caretX = widthOf(text.substring(0, caretIndex))
        val centerY = (height - nodePrefHeight()) / 2

        if (caretX + offsetX < 0) {
            // Caret would have appeared left of the control
            offsetX = -caretX
        }
        if (caretX + offsetX > width - surroundX()) {
            // Caret would have appeared right of the control
            offsetX -= caretX + offsetX - width + surroundX()
        }

        val x = surroundLeft() + sceneX + offsetX
        val y = surroundTop() + sceneY + centerY

        // Clip so that text or selection backgrounds do not spill out beyond the control.
        backend.clip(x - offsetX, y, width - surroundX(), height - surroundY()) {

            // Draw the selection background
            if (textSelected) {
                val fromX = widthOf(text.substring(0, selectionStart))
                val toX = widthOf(text.substring(0, selectionEnd))
                backend.fillRect(
                    x + fromX, sceneY + surroundTop(),
                    x + toX, sceneY + height - surroundBottom(), highlightColor
                )
            }

            // Draw the text
            if (textSelected) {
                drawLineText(x, y, text, selectionStart, selectionEnd)
            } else {
                if (text.isBlank()) {
                    if (! focused && promptText.isNotBlank()) {
                        font.drawTopLeft(promptText, promptTextColor, x, y, indentation.columns)
                    }
                } else {
                    font.drawTopLeft(text, textColor, x, y, indentation.columns)
                }
            }
        }

        // Draw the caret
        if (focused && caretVisible) {
            caretImage?.let { image ->
                val scale = font.height / image.imageHeight
                val width = image.imageWidth * scale
                val height = image.imageHeight * scale
                image.drawTo(x + caretX - width / 2, y, width, height, caretColor)
            }
        }
    }
    // endregion layout

}

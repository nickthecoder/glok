package uk.co.nickthecoder.glok.text

enum class TextVAlignment {
    TOP, CENTER, BASELINE, BOTTOM
}

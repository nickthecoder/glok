/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableOptionalFloatProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.theme.styles.FORM_GRID
import uk.co.nickthecoder.glok.util.*

/**
 * A layout, which is quite useful for laying out forms.
 */
class FormGrid : Region() {

    // ==== Properties ====

    val minLeftWidthProperty by stylableOptionalFloatProperty(null)
    var minLeftWidth by minLeftWidthProperty

    val columnSpacingProperty by stylableFloatProperty(0f)
    var columnSpacing by columnSpacingProperty

    val rowSpacingProperty by stylableFloatProperty(0f)
    var rowSpacing by rowSpacingProperty

    // ==== End of properties ====

    val rows = mutableListOf<FormRow>().asMutableObservableList()
    override val children = rows.asReadOnly()

    // ==== End of fields ====

    init {
        styles.add(FORM_GRID)

        rows.addChangeListener { list, change ->
            childrenListener.changed(list, change)
            for (row in change.removed) {
                row.columnSpacingProperty.unbind()
            }
            for (row in change.added) {
                row.columnSpacingProperty.bindTo(columnSpacingProperty)
            }
        }

        for (prop in listOf(columnSpacingProperty, rowSpacingProperty)) {
            prop.addListener(requestLayoutListener)
        }
    }


    // ==== End if init ====

    operator fun FormRow.unaryPlus() {
        rows.add(this)
    }

    private fun calculateColumnWidths(): Triple<Float, Float, Float> {
        var leftNeeds = minLeftWidth ?: 0f
        var rightNeeds = 0f
        var fullWidthNeeds = 0f

        for (row in rows) {
            row.left?.let { child ->
                val needs = child.evalPrefWidth()
                if (needs > leftNeeds) leftNeeds = needs
            }
            row.right?.let { child ->
                val needs = child.evalPrefWidth()
                if (needs > rightNeeds) rightNeeds = needs
            }
            row.above?.let { child ->
                val needs = child.evalPrefWidth()
                if (needs > fullWidthNeeds) fullWidthNeeds = needs
            }
            row.below?.let { child ->
                val needs = child.evalPrefWidth()
                if (needs > fullWidthNeeds) fullWidthNeeds = needs
            }
            row.errors?.let { child ->
                val needs = child.evalPrefWidth()
                if (needs > fullWidthNeeds) fullWidthNeeds = needs
            }
        }
        return Triple(leftNeeds, rightNeeds, fullWidthNeeds)
    }

    override fun nodePrefWidth(): Float {
        val (leftNeeds, rightNeeds, fullWidthNeeds) = calculateColumnWidths()
        return max(fullWidthNeeds, leftNeeds + rightNeeds + columnSpacing) + surroundX()
    }

    override fun nodePrefHeight(): Float {
        val visibleItems = rows.ifVisible()
        val spacing = max(0, visibleItems.size - 1) * rowSpacing
        return surroundY() + spacing + visibleItems.totalPrefHeight()
    }

    override fun layoutChildren() {
        val (leftNeeds, _, _) = calculateColumnWidths()

        val x = surroundLeft()
        var y = surroundTop()
        val availableWidth = width - surroundX()
        for (item in rows) {
            if (! item.visible) continue

            var subRowCount = item.aboveBelowErrors().size
            if (item.left?.visible == true || item.right?.visible == true) subRowCount ++
            setChildBounds(
                item, x, y, availableWidth, item.evalPrefHeight() + (subRowCount - 1) * item.spacing
            )
            val column1 = item.surroundLeft()
            val column2 = column1 + leftNeeds + columnSpacing
            // availableColumn1 = leftNeeds
            val availableColumn2 = availableWidth - columnSpacing - leftNeeds

            var y2 = item.surroundTop()
            item.above?.let { child ->
                if (child.visible) {
                    val childHeight = child.evalPrefHeight()
                    val childWidth = if (child.growPriority == 0f) {
                        child.evalPrefWidth()
                    } else {
                       child.evalMaxWidth()
                    }
                    item.setChildBounds2(child, column1, y2,  min(availableWidth, childWidth), childHeight)
                    y2 += item.spacing + childHeight
                }
            }
            val subRowHeight = listOfVisible(item.left, item.right).maxPrefHeight()
            item.left?.let { child ->
                val childHeight = child.evalPrefHeight()
                val childWidth = if (child.growPriority == 0f) {
                    child.evalPrefWidth()
                } else {
                    child.evalMaxWidth()
                }
                val centerY = (subRowHeight - childHeight) / 2
                item.setChildBounds2(child, column1, y2 + centerY, min(leftNeeds, childWidth), childHeight)
            }

            item.right?.let { child ->
                val childHeight = child.evalPrefHeight()
                val childWidth = if (child.growPriority == 0f) {
                    child.evalPrefWidth()
                } else {
                    child.evalMaxWidth()
                }
                val centerY = (subRowHeight - childHeight) / 2
                item.setChildBounds2(child, column2, y2 + centerY, min(availableColumn2, childWidth), childHeight)
            }
            if (item.left?.visible == true || item.right?.visible == true) {
                y2 += item.spacing + subRowHeight
            }
            item.below?.let { child ->
                if (child.visible) {
                    val childHeight = child.evalPrefHeight()
                    val childWidth = if (child.growPriority == 0f) {
                        child.evalPrefWidth()
                    } else {
                        child.evalMaxWidth()
                    }
                    item.setChildBounds2(child, column1, y2, min(availableWidth, childWidth), childHeight)
                    y2 += item.spacing + childHeight
                }
            }
            item.errors?.let { child ->
                if (child.visible) {
                    val childHeight = child.evalPrefHeight()
                    val childWidth = if (child.growPriority == 0f) {
                        child.evalPrefWidth()
                    } else {
                        child.evalMaxWidth()
                    }
                    item.setChildBounds2(child, column1, y2, min(availableWidth, childWidth), childHeight)
                    y2 += item.spacing + childHeight
                }
            }

            y += y2 + rowSpacing - item.spacing
        }
    }

    // ==== Object methods ====

}

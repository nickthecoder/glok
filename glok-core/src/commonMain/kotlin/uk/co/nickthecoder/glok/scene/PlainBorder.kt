package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.property.boilerplate.ObservableColor
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty

/**
 * A plain, rectangular border using a single color.
 */
object PlainBorder : Border {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        if (color.alpha != 0f) {
            var top = y
            if (size.top > 0f) {
                backend.fillRect(x, y, x + width, y + size.top, color)
                top += size.top
            }
            var bottom = y + height
            if (size.bottom > 0f) {
                backend.fillRect(x, bottom, x + width, bottom - size.bottom, color)
                bottom -= size.bottom
            }
            if (size.left > 0f) {
                backend.fillRect(x, top, x + size.left, bottom, color)
            }
            if (size.right > 0f) {
                backend.fillRect(x + width, top, x + width - size.right, bottom, color)
            }
        }
    }

    override fun toString() = "PlainBorder"

}

/**
 * A border which ignores [Region.borderSize], and uses the size passed to the constructor instead.
 * This may cause the Region's content to overlap the border.
 */
class OverlappingBorder(val size: Edges) : Border {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, ignoreSize: Edges) {
        if (color.alpha != 0f) {
            var top = y
            if (size.top > 0f) {
                backend.fillRect(x, y, x + width, y + size.top, color)
                top += size.top
            }
            var bottom = y + height
            if (size.bottom > 0f) {
                backend.fillRect(x, bottom, x + width, bottom - size.bottom, color)
                bottom -= size.bottom
            }
            if (size.left > 0f) {
                backend.fillRect(x, top, x + size.left, bottom, color)
            }
            if (size.right > 0f) {
                backend.fillRect(x + width, top, x + width - size.right, bottom, color)
            }
        }
    }

    override fun toString() = "OverlappingBorder"

}

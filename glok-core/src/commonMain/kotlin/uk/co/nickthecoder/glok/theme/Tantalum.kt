package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.property.property
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.*
import uk.co.nickthecoder.glok.theme.Tantalum.darkColorScheme
import uk.co.nickthecoder.glok.theme.Tantalum.darkProperty
import uk.co.nickthecoder.glok.theme.Tantalum.lightColorScheme
import uk.co.nickthecoder.glok.theme.dsl.and
import uk.co.nickthecoder.glok.theme.dsl.or
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.Converter

/**
 * The default [Theme].
 *
 * Supports a dark and light color scheme based on a single property : [darkProperty].
 * Each color scheme is just a map of [ColorProperty] keyed on the color's name (a String).
 *
 * ## Colors
 *
 * There are only 8 basic colors and 5 additional special purpose colors.
 * However, many of these colors are used with varying amounts of opacity, which gives the
 * appearance of more colors. For example a simple Button uses a background color named "button".
 * But when the mouse hover over it, the opacity changes. When you press it, the opacity changes again.
 *
 * ### background1
 * The default background color for the whole scene.
 * * TabPane's selected Tab
 *
 * ### background2
 * An alternative to "background1" to help distinguish one region from another.
 * * Unselected tabs
 * * TextArea's line number gutter
 * * TitledPane's title
 * * PopupMenus
 * * Tooltips
 * * AlertDialog's heading
 * * The sides of a Harbour
 * * A Dock's toolbar
 *
 * ### accent
 * A color to grab attention. e.g.
 * * Default buttons
 * * Border of the TextField/TextArea with focus
 * * Highlight for the selected Tab in a TabPane
 *
 * ### font
 * * Text in Buttons, Labels, TextFields, TextAreas etc.
 *
 * ### font2 : Similar to "font", but with lower contrast
 * * Disabled Buttons, TextFields, TextAreas etc.
 *
 * ### textField
 * * The background color for TextField, TextArea, Spinner
 *
 * ### button
 * * The background color for buttons
 *
 * ### stroke
 * * Borders around TextField, TextArea, Tabs, a TabPane's content etc.
 * * A Separator
 *
 * ## Speciality Colors
 *
 * * caret : The caret in a TextField, TextArea, Spinner etc.
 * * white : Always white, regardless of the color scheme.
 *   e.g. to represent a sheet of paper
 * * black : Always black, regardless of the color scheme.
 *   e.g. for text, when the background uses color "white" (above)
 * * find_match : The highlight color for matched text when using `FindAndReplace` on a StyledTextArea.
 * * find_replacement : The highlight color of replaced text using `FindAndReplace`.
 */
/*
 * Comparison with JavaFX's default theme.
 * Tantalum has roughly 2,500 lines compared with Caspian's 4,200. Nice ;-)
 * I think Tantalum is easier to read and much easier to maintain. (But I'm biased).
 */
object Tantalum : CustomisableTheme() {

    val darkProperty by booleanProperty(false)
    var dark by darkProperty

    val darkColors = mapOf(
        "background1" to "#2d2d30",
        "background2" to "#363638",

        "accent" to "#2093fe",
        "font" to "#dddddd",
        "font2" to "#989898",
        "caret" to "#eb436d",
        "textField" to "#242427",
        "button" to "#5e6061",
        "stroke" to "#1e1e1e",
        "white" to "#fff",
        "black" to "#000",

        // Note, these have alpha values.
        // When choosing the values, ensure that the caret is still prominent, and
        // the highlight can still be seen when text is selected too.
        // i.e. They have to work well with partially transparent accent color too.
        "find_match" to "#00ff0050",
        "find_replacement" to "#ffff0080"
    ).mapValues { Color[it.value] }
    val lightColors = mapOf(
        "background1" to "#f5f5f6",
        "background2" to "#e6e9ed",

        "accent" to "#70afea",
        "font" to "#20203f",
        "font2" to "#929292",
        "caret" to "#eb436d",
        "textField" to "#ffffff",
        "button" to "#cfd5dd",
        "stroke" to "#bec4cc",
        "white" to "#fff",
        "black" to "#000",

        // Used by FindAndReplace
        // Note, these have alpha values.
        // When choosing the values, ensure that the caret is still prominent, and
        // the highlight can still be seen when text is selected too.
        // i.e. They have to work well with partially transparent accent color too.
        "find_match" to "#00ff0c36",
        "find_replacement" to "#ffff0044"
    ).mapValues { Color[it.value] }

    val colorLabels = mapOf(
        "background1" to "Main Background Color",
        "background2" to "Alternate Background Color",

        "accent" to "Accent Color",
        "font" to "Text Color",
        "font2" to "Alternate Text Color",
        "caret" to "Caret Color",
        "textField" to "Text Background Color",
        "button" to "Button Background Color",
        "stroke" to "Dividing Line Color",
        "white" to "White",
        "black" to "Black",

        "find_match" to "Highlight Matching Text",
        "find_replacement" to "Highlight Replaced Text"
    )

    val darkColorScheme: Map<String, ColorProperty> =
        darkColors.mapValues { SimpleColorProperty(it.value) }.toMap()

    val lightColorScheme: Map<String, ColorProperty> =
        lightColors.mapValues { SimpleColorProperty(it.value) }.toMap()

    /**
     * By changing this one property, we can change the entire color scheme.
     */
    private val colorSchemeProperty by property(lightColorScheme)
    val colorScheme by colorSchemeProperty

    /**
     * If we attempt to look up a color property which doesn't exist, then this color property is returned.
     * It is a vibrant violet/red, which makes it obvious that something is wrong!
     */
    private val defaultColorProperty by colorProperty(Color.MEDIUM_VIOLET_RED)

    // All color properties are IndirectProperties through colorSchemeProperty.

    val background1ColorProperty: ColorProperty = namedColorProperty("background1")
    val background2ColorProperty: ColorProperty = namedColorProperty("background2")
    val accentColorProperty: ColorProperty = namedColorProperty("accent")
    val fontColorProperty: ColorProperty = namedColorProperty("font")
    val fontColor2Property: ColorProperty = namedColorProperty("font2")
    val caretColorProperty: ColorProperty = namedColorProperty("caret")
    val textFieldColorProperty: ColorProperty = namedColorProperty("textField")
    val buttonColorProperty: ColorProperty = namedColorProperty("button")
    val strokeColorProperty: ColorProperty = namedColorProperty("stroke")
    val whiteColorProperty: ColorProperty = namedColorProperty("white")
    val blackColorProperty: ColorProperty = namedColorProperty("black")

    // Used by `FindAndReplace`
    val findMatchColorProperty = namedColorProperty("find_match")
    val findReplacementColorProperty = namedColorProperty("find_replacement")

    var background1Color by background1ColorProperty
    var background2Color by background2ColorProperty
    var accentColor by accentColorProperty
    var fontColor by fontColorProperty
    var fontColor2 by fontColor2Property
    var caretColor by caretColorProperty
    var textFieldColor by textFieldColorProperty
    var buttonColor by buttonColorProperty
    var strokeColor by strokeColorProperty
    var whiteColor by whiteColorProperty
    var blackColor by blackColorProperty

    // Used by `FindAndReplace`
    var findMatchColor by findMatchColorProperty
    var findReplacementColor by findReplacementColorProperty


    init {
        colorSchemeProperty.bindTo(UnaryFunction(darkProperty) { isDark -> if (isDark) darkColorScheme else lightColorScheme })

        theme = buildTheme()

        dependsOn(
            background1ColorProperty, background2ColorProperty,
            accentColorProperty,
            fontColorProperty, fontColor2Property, caretColorProperty, textFieldColorProperty,
            buttonColorProperty,
            strokeColorProperty,
            whiteColorProperty, blackColorProperty,
            findMatchColorProperty, findReplacementColorProperty
        )
    }


    private fun namedColorProperty(name: String): ColorProperty = IndirectColorProperty(colorSchemeProperty) {
        it[name] ?: defaultColorProperty
    }

    /**
     * Resets properties back to their default values.
     *
     */
    fun reset() {
        dark = false
        for ((name, color) in lightColors) {
            lightColorScheme[name]?.value = color
        }
        for ((name, color) in darkColors) {
            darkColorScheme[name]?.value = color
        }
        fontFamily = Font.defaultFont.identifier.family
        fontSize = Font.defaultFont.identifier.size

        fixedWidthFontSize = fontSize
        fixedWidthFontFamily = "MonoSpaced"

        borderThickness = 2f
        radius = 4f
        buttonPadding = Edges(3f, 10f)
        labelPadding = Edges(6f, 6f)
        focusBorderThickness = 2f

        iconSize = 24
    }

    /*
     * This method is tagged with "region" "endregion" tags. Within IntelliJ, the shortcut:
     *     Ctrl+Shift+Num-* 3
     * will fold each of the regions, which makes navigating this long method much easier.
     */
    override fun buildTheme() = theme {

        // Theme.defaultFont allows lengths to be specified using `em` units (rather than pixels),
        // even for nodes which do not have a `font` property.
        // For example, a Region's border could be specified as 0.5em.
        theme.defaultFont = font

        val buttons = BUTTON or TOGGLE_BUTTON or RADIO_BUTTON2 or MENU_BUTTON or SPLIT_MENU_BUTTON
        val labelled = LABEL or BUTTON or TOGGLE_BUTTON or RADIO_BUTTON or RADIO_BUTTON2 or
            MENU_BUTTON or SPLIT_MENU_BUTTON or MENU

        //region  ==== root =====
        root {
            sceneBackgroundColor(background1Color)
        }
        //endregion

        // region ==== Region ====
        ".background1" {
            backgroundColor(background1Color)
            plainBackground()
        }

        ".background2" {
            backgroundColor(background2Color)
            plainBackground()
        }
        // endregion region

        //region  ==== Labelled ====
        labelled {
            graphicTextGap(6)
        }

        (labelled or TEXT) {
            // NOTE, These are NOT the styles used by Text in a StyledTextArea.
            font(font)
            textColor(fontColor)

            FIXED_WIDTH {
                font(fixedWidthFont)
                ".bold" {
                    font(fixedWidthFont.bold())
                }
                ".italic" {
                    font(fixedWidthFont.italic())
                    ".bold" {
                        font(fixedWidthFont.boldItalic())
                    }
                }
            }
            ".bold" {
                font(font.bold())
            }
            ".italic" {
                font(font.italic())
                ".bold" {
                    font(font.boldItalic())
                }
            }
        }

        LABEL {
            (INFORMATION or UNITS) {
                textColor(fontColor2)
                font(font.italic())
            }
            ERROR {
                textColor(accentColor)
                font(font.bold())
            }
        }
        //endregion

        //region  ==== Buttons ====

        BUTTON {
            DEFAULT_BUTTON {
                backgroundColor(accentColor.opacity(0.8f))

                HOVER { backgroundColor(accentColor.opacity(0.5f)) }
                ARMED { backgroundColor(accentColor) }
            }
        }

        (BUTTON or TOGGLE_BUTTON) {
            LIKE_LINK {
                noBackground()
                padding(2, 6)
                textColor(accentColor.opacity(0.85f))

                HOVER { textColor(accentColor.opacity(0.7f)) }
                ARMED { textColor(accentColor) }

                DISABLED {
                    textColor(fontColor2)
                }
            }
        }

        buttons {
            alignment(Alignment.CENTER_CENTER)
            backgroundColor(buttonColor.opacity(0.7f))
            roundedBackground(radius)
            padding(buttonPadding)
            labelPadding(labelPadding)
            focusTraversable(true)
            focusAcceptable(true)

            focusBorderColor(accentColor)
            focusBorder(RoundedBorder(radius + focusBorderThickness))
            focusBorderSize(focusBorderThickness)

            DISABLED {
                focusTraversable(false)
                focusAcceptable(false)

                backgroundColor(buttonColor.opacity(0.5f))
                textColor(fontColor2)

                child(IMAGE_VIEW) {
                    tint(Color.WHITE.opacity(0.3f))
                }
            }

            HOVER { backgroundColor(buttonColor.opacity(0.5f)) }

            ARMED { backgroundColor(buttonColor) }

            TINTED {
                child(IMAGE_VIEW) {
                    tint(fontColor)
                }
                DISABLED {
                    child(IMAGE_VIEW) {
                        tint(fontColor2.opacity(0.5f))
                    }
                }
            }

        }

        // RADIO_BUTTON2 looks like a toggle-button. See RADIO_BUTTON below for a standard radio-button.
        (TOGGLE_BUTTON or RADIO_BUTTON2) {
            SELECTED { backgroundColor(accentColor.opacity(0.5f)) }
            ARMED { backgroundColor(accentColor) }
            ".plain" {
                padding(2)
                contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                noBackground()
                DISABLED { noBackground() }
                SELECTED { noBackground() }
            }
        }

        // Much of this is identical to CHECK_BOX, but with different graphics.
        RADIO_BUTTON {
            val radius = 12f
            graphic(smallIcons, "blank")
            padding(4, 2)
            textColor(fontColor)
            border(NoBorder)
            borderSize(1f)
            graphicTextGap(6f)
            focusTraversable(true)
            focusAcceptable(true)

            child(IMAGE_VIEW) {
                borderSize(1)
                padding(2)
                borderColor(fontColor)
                roundedBorder(radius)
                tint(accentColor)
            }

            (HOVER or SELECTED) {
                textColor(fontColor)
                child(IMAGE_VIEW) {
                    borderColor(fontColor)
                }
            }

            SELECTED {
                graphic(smallIcons, "radio_button:selected")
            }

            DISABLED {
                focusTraversable(false)
                focusAcceptable(false)
                textColor(fontColor2)

                child(IMAGE_VIEW) {
                    borderColor(fontColor2)
                    tint(fontColor2)
                }
            }
        }

        // MenuButton
        MENU_BUTTON {
            child(ARROW) {
                padding(0, 0, 0, 6)
                image(smallIcons, "down")
                tint(fontColor.opacity(0.6f))
            }
            DISABLED {
                child(ARROW) {
                    tint(fontColor2)
                }
            }
            ".no_arrow" {
                child(ARROW) {
                    visible(false)
                }
            }
        }

        // SplitMenuButton
        SPLIT_MENU_BUTTON {
            labelPadding(labelPadding.top, labelPadding.right + 6, labelPadding.bottom, labelPadding.left)
            padding(buttonPadding.top, 4, buttonPadding.bottom, buttonPadding.left)

            child(ARROW_BUTTON) {
                padding(0, 6, 0, 6)
                plainBorder()
                borderSize(0, 0, 0, 1)
                borderColor(strokeColor)

                plainBackground()
                backgroundColor(Color.TRANSPARENT)

                child(GRAPHIC) {
                    image(smallIcons, "down")
                    tint(fontColor.opacity(0.6f))
                }
            }
            DISABLED {
                child(ARROW_BUTTON) {
                    child(GRAPHIC) {
                        tint(fontColor2.opacity(0.7f))
                    }
                }
            }
        }

        // File / Folder buttons

        BUTTON {
            ".file_button" {
                padding(4f, 8f)
                graphic(scaledIcons, "file_tinted")
                contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                child(IMAGE_VIEW) {
                    tint(fontColor)
                }
            }
            ".folder_button" {
                padding(4f, 8f)
                graphic(scaledIcons, "folder_tinted")
                contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                child(IMAGE_VIEW) {
                    tint(fontColor)
                }
            }
        }

        //endregion

        //region  ==== editable text (TextArea/TextField/Spinner's editor) ====
        (TEXT_FIELD or TEXT_AREA or STYLED_TEXT_AREA) {
            font(font)
            caretImage(icons, "caret")
            caretColor(caretColor)

            textColor(fontColor.opacity(0.9f))

            highlightColor(Color.TRANSPARENT)
            highlightColor(accentColor.opacity(0.5f))
            highlightTextColor(fontColor)

            FIXED_WIDTH {
                font(fixedWidthFont)
            }

            READ_ONLY {
                caretImage(icons, "blank") // Hides the caret
            }

            FOCUSED {
                textColor(fontColor)
                highlightColor(accentColor.opacity(0.4f))
            }
        }

        TEXT_FIELD {
            promptTextColor(fontColor.opacity(0.5f))
        }

        (TEXT_FIELD or TEXT_AREA or STYLED_TEXT_AREA or SPINNER) {
            borderSize(1)

            roundedBorder(radius)
            borderColor(strokeColor)

            roundedBackground(radius)
            backgroundColor(textFieldColor.opacity(0.5f))

            focusTraversable(true)
            focusAcceptable(true)

            FOCUSED {
                borderColor(accentColor)
                backgroundColor(textFieldColor)

                ":conversion_error" {
                    backgroundColor(accentColor.opacity(0.2f))
                }
            }
        }
        //endregion

        // region  ==== TextField ====
        TEXT_FIELD {
            padding(6)

            LIKE_LABEL {
                borderSize(0)
                noBorder()
                noBackground()
                padding(0)
            }
        }
        // endregion

        // region ==== TextArea ====
        (TEXT_AREA or STYLED_TEXT_AREA) {
            lineHeight(1.2)

            child(SCROLL_PANE) {
                child(VIEWPORT) {
                    child(SCROLLED) {
                        child(LINE_NUMBER_GUTTER) {
                            padding( 8, 8 )
                            borderSize(0, 1, 0, 0)

                            plainBorder()
                            borderColor(strokeColor)

                            plainBackground()
                            backgroundColor(background2Color)

                            textColor(fontColor2)
                        }
                        child(CONTAINER) {
                            padding(8)
                        }
                    }
                }
            }

        }
        // endregion

        // region  ==== StyledTextField ====

        STYLED_TEXT_AREA {
            font(fixedWidthFont)
            descendant(CONTAINER) {
                child(TEXT) {
                    font(fixedWidthFont)

                    // Used by MarkdownToStyled, and are also for general-purpose use.
                    ".bold" {
                        font(fixedWidthFont.bold())
                    }
                    ".boldItalic" {
                        font(fixedWidthFont.boldItalic())
                    }
                    ".italic" {
                        font(fixedWidthFont.italic())
                        ".bold" {
                            font(fixedWidthFont.boldItalic())
                        }
                    }
                    ".code" {
                        roundedBackground(3f)
                        backgroundColor(accentColor.opacity(0.25f))
                    }
                    ".blockQuoteIndent" {
                        border(OverlappingBorder(Edges(0f, 0f, 0f, 10f)))
                        borderColor(strokeColor)
                    }
                    ".blockQuote" {
                        textColor(fontColor.opacity(0.8f))
                        plainBackground()
                        backgroundColor(strokeColor.opacity(0.15f))
                    }
                    ".link" {
                        font(fixedWidthFont.bold())
                        textColor(accentColor)
                    }
                    ".underline" {
                        borderColor(fontColor)
                        border(OverlappingBorder(Edges(0f, 0f, 1f, 0f)))
                    }
                    // Note, as this use a [Background], it cannot also be used with a regular background.
                    ".strikethrough" {
                        backgroundColor(fontColor)
                        underlineBackground(1f, 0.5f)
                    }
                    ".heading1" {
                        font(fixedWidthFont.bold())
                        borderColor(fontColor)
                        border(OverlappingBorder(Edges(0f, 0f, 2f, 0f)))
                    }
                    ".heading2" {
                        font(fixedWidthFont.bold())
                        borderColor(fontColor)
                        border(OverlappingBorder(Edges(0f, 0f, 1f, 0f)))
                    }
                    ".heading3" {
                        font(fixedWidthFont.bold())
                    }
                    ".heading4" {
                        font(fixedWidthFont.italic())
                    }


                    // Used by `FindAndReplace`.
                    ".find_match" {
                        backgroundColor(findMatchColor)
                        plainBackground()
                    }
                    ".find_replacement" {
                        plainBackground()
                        backgroundColor(findReplacementColor)
                    }

                }
            }
        }

        //endregion

        //region  ==== Spinners ====
        SPINNER {
            focusTraversable(true)
            focusAcceptable(true)
            spinnerArrows(SpinnerArrowPositions.RIGHT_VERTICAL)
            padding(0, 4)
            child(EDITOR) {
                borderSize(0)
                noBackground()
                noBorder()
                focusTraversable(false)
                focusAcceptable(false)
            }

            INVALID {
                child(EDITOR) {
                    // plainBackground()
                    backgroundColor(accentColor.opacity(0.3f))
                }
            }

            READ_ONLY {

                child(EDITOR) {
                    textColor(fontColor2)
                    caretImage(icons, "blank") // Hides the caret
                }

                child(SPIN_UP or SPIN_DOWN) {
                    focusTraversable(false)
                    focusAcceptable(false)

                    child(IMAGE_VIEW) {
                        tint(fontColor2)
                    }
                }
            }

            child(SPIN_UP or SPIN_DOWN) {
                padding(0)
                alignment(Alignment.CENTER_CENTER)
                contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                noBackground()

                HOVER {
                    backgroundColor(background2Color)
                }
                ARMED {
                    backgroundColor(accentColor)
                }

                child(IMAGE_VIEW) {
                    tint(fontColor)
                }
                DISABLED {
                    textColor(fontColor2)

                    child(IMAGE_VIEW) {
                        tint(fontColor2)
                    }
                }
            }

            (":sap_split" or ":sap_left_h" or ":sap_right_h") {
                child(SPIN_UP) {
                    graphic(smallIcons, "up")
                }
                child(SPIN_DOWN) {
                    graphic(smallIcons, "down")
                }
            }
            (":sap_left_v" or ":sap_right_v") {
                child(SPIN_UP) {
                    graphic(halfIcons, "up")
                }
                child(SPIN_DOWN) {
                    graphic(halfIcons, "down")
                }
            }
        }
        //endregion

        // region ==== ProgressBar ====
        PROGRESS_BAR {
            val lineThickness = 1f
            val normalThickness = 20f

            "length"(200f)

            HORIZONTAL {
                prefHeight(normalThickness)
            }
            VERTICAL {
                prefWidth(normalThickness)
            }
            backgroundColor(background2Color)
            roundedBackground(normalThickness / 2)

            child(PROGRESS) {
                borderSize(lineThickness)
                roundedBackground(normalThickness / 2)
                backgroundColor(accentColor)
                borderColor(strokeColor)
            }

            ".fat" {
                val fatThickness = 30f

                HORIZONTAL {
                    prefHeight(fatThickness)
                }
                VERTICAL {
                    prefWidth(fatThickness)
                }
                roundedBackground(fatThickness / 2)

                child(PROGRESS) {
                    borderSize(lineThickness)

                    backgroundColor(accentColor)
                    roundedBackground(fatThickness / 2)

                    borderColor(strokeColor)
                    roundedBorder(fatThickness / 2)
                }
            }
        }
        // endregion ==== ProgressBar ====

        // region ==== Slider ====
        SLIDER {
            val thumbRadius = 10f
            val trackThickness = 6f

            borderSize(1)
            focusTraversable(true)
            focusAcceptable(true)
            "markerColor"(buttonColor)//.opacity(0.6f))
            "markerThickness"(2f)
            "markerLength"(8f)
            "minorMarkerLength"(6f)

            HORIZONTAL {
                padding(thumbRadius - trackThickness / 2, thumbRadius)
                prefWidth(150)
            }
            VERTICAL {
                padding(thumbRadius + trackThickness / 2, thumbRadius)
                prefHeight(150)
            }

            child(THUMB) {
                prefWidth(thumbRadius * 2)
                prefHeight(thumbRadius * 2)
                borderSize(1)

                borderColor(strokeColor)
                roundedBorder(thumbRadius)

                backgroundColor(buttonColor)
                roundedBackground(thumbRadius)
            }

            // HOVER {}

            ARMED {
                child(THUMB) {
                    backgroundColor(accentColor)
                }
            }

            child(TRACK) {
                backgroundColor(strokeColor.opacity(0.6f))
                plainBackground()
                padding(trackThickness / 2)
            }

            HORIZONTAL {
                child(TRACK) {
                    prefHeight(trackThickness)
                }
            }

            VERTICAL {
                child(TRACK) {
                    prefWidth(trackThickness)
                }
            }
        }
        // endregion

        // region ==== ColorSlider ====
        COLOR_SLIDER {
            val thumbHeight = 36f
            val thumbWidth = 1f
            val trackThickness = 32f
            val size = 150f

            focusTraversable(true)
            focusAcceptable(true)

            child(TRACK) {
                borderSize(1)
                raisedBorder(Color.BLACK.opacity(0.5f), Color.WHITE.opacity(0.5f))
                plainBackground()
                backgroundColor(strokeColor.opacity(0.5f))
            }

            child(THUMB) {
                borderSize(1)
                borderColor(Color.BLACK)
                plainBorder()

                ":light" {
                    borderColor(Color.WHITE)
                }
            }

            HORIZONTAL {
                padding((thumbHeight - trackThickness) / 2, 6)

                child(TRACK) {
                    prefHeight(trackThickness)
                    prefWidth(size)
                }
                child(THUMB) {
                    prefWidth(thumbWidth)
                    prefHeight(thumbHeight)
                }
            }
            VERTICAL {
                padding(6, (thumbHeight - trackThickness) / 2)

                child(TRACK) {
                    prefWidth(trackThickness)
                    prefHeight(size)
                }
                child(THUMB) {
                    prefHeight(thumbWidth)
                    prefWidth(thumbHeight)
                }
            }
        }
        // endregion

        // region ==== Slider2D ====
        SLIDER_2D {
            val thumbRadius = 6f
            val size = 150f

            focusTraversable(true)
            focusAcceptable(true)
            padding(thumbRadius)

            child(TRACK) {
                borderSize(1)
                raisedBorder(blackColor.opacity(0.5f), whiteColor.opacity(0.5f))

                plainBackground()
                backgroundColor(strokeColor.opacity(0.5f))

                prefHeight(size)
                prefHeight(size)
            }

            child(THUMB) {
                borderSize(1)
                roundedBorder(thumbRadius)
                borderColor(Color.BLACK) // TODO BLACK? Really???
                prefWidth(thumbRadius * 2)
                prefHeight(thumbRadius * 2)
            }
        }
        // endregion

        //region ==== Container ====
        SINGLE_CONTAINER {
        }
        //endregion

        //region  ==== TabPane ====
        TAB_PANE {
            child(CONTAINER) {
                plainBorder()
                borderColor(strokeColor)

                plainBackground()
                backgroundColor(background2Color)
            }
        }
        //endregion

        // region ==== ScrollPaneWithButtons ====
        SCROLL_PANE_WITH_BUTTONS {
            child(THREE_ROW) {
                spacing(8)

                child(".scroll_left") {
                    graphic(smallIcons, "left")
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                    backgroundColor(Color.TRANSPARENT)

                    child(IMAGE_VIEW) {
                        tint(fontColor.opacity(0.7f))
                    }
                }
                child(".scroll_right") {
                    graphic(smallIcons, "right")
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                    backgroundColor(Color.TRANSPARENT)

                    child(IMAGE_VIEW) {
                        tint(fontColor.opacity(0.7f))
                    }
                }

                child(".scroll_up") {
                    graphic(smallIcons, "up")
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                    noBackground()

                    child(IMAGE_VIEW) {
                        tint(fontColor.opacity(0.7f))
                    }
                }

                child(".scroll_down") {
                    graphic(smallIcons, "down")
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                    noBackground()

                    child(IMAGE_VIEW) {
                        tint(fontColor.opacity(0.7f))
                    }
                }
            }
        }
        // endregion ScrollPaneWithButtons

        //region ==== TabBar ====
        TAB_BAR {

            plainBorder()
            borderColor(strokeColor)

            TOP {
                alignment(Alignment.BOTTOM_LEFT)
                descendant(TAB) {
                    borderSize(0, 0, 4, 0)
                }
            }
            BOTTOM {
                alignment(Alignment.TOP_LEFT)
                descendant(TAB) {
                    borderSize(4, 0, 0, 0)
                }
            }
            LEFT {
                "overrideMinWidth"(250f)
                padding(20, 0, 0, 0)
                borderSize(0, 1, 0, 0)

                fill(true)

                alignment(Alignment.TOP_LEFT)
                descendant(TAB) {
                    borderSize(0, 0, 0, 4)
                    padding(8, 20, 8, 10)
                    child(LABEL) { alignment(Alignment.CENTER_LEFT) }
                }
            }
            RIGHT {
                "overrideMinWidth"(250f)
                padding(20, 0, 0, 0)
                borderSize(0, 1, 0, 0)
                fill(true)

                alignment(Alignment.TOP_LEFT)
                descendant(TAB) {
                    borderSize(0, 4, 0, 0)
                    padding(8, 10, 8, 20)
                }
            }

            child(SCROLL_PANE_WITH_BUTTONS) {
                child(THREE_ROW) {
                    spacing(0)
                }
            }

            descendant(TAB) {
                padding(8, 16)
                noBorder()
                noBackground()
                focusTraversable(true)
                focusAcceptable(true)

                SELECTED {
                    plainBorder()
                    borderColor(accentColor)

                    plainBackground()
                    backgroundColor(accentColor.opacity(0.1f))
                    child(LABEL) {
                        font(Tantalum.font.bold())
                    }
                }

                DISABLED {
                    focusTraversable(false)
                    focusAcceptable(false)

                    child(LABEL) {
                        textColor(fontColor2)
                    }
                }

                child(LABEL) {
                    graphicTextGap(4)
                }

                TINTED {
                    child(LABEL) {
                        child(IMAGE_VIEW) {
                            tint(fontColor)
                        }
                    }
                }
            }
        }
        //endregion

        //region ==== DocumentsTabBar ====
        DOCUMENTS_TAB_BAR {

            TOP {
                alignment(Alignment.BOTTOM_LEFT)
                descendant(TAB) {
                    borderSize(1, 1, 0, 1)
                }
            }
            BOTTOM {
                alignment(Alignment.TOP_LEFT)
                descendant(TAB) {
                    borderSize(0, 1, 1, 1)
                }
            }

            child(SCROLL_PANE_WITH_BUTTONS) {
                child(THREE_ROW) {
                    spacing(0)
                }
            }
            descendant(TAB) {
                val tabRadius = 4f

                roundedBorder(tabRadius)
                borderColor(strokeColor)

                roundedBackground(tabRadius)
                backgroundColor(background2Color)

                padding(4,6)
                alignment(Alignment.CENTER_CENTER)
                focusTraversable(true)
                focusAcceptable(true)

                SELECTED {
                    borderColor(accentColor.opacity(0.35f))
                    backgroundColor(accentColor.opacity(0.2f))

                    child(LABEL) {
                        textColor(fontColor)
                    }
                }

                DISABLED {
                    focusTraversable(false)
                    focusAcceptable(false)
                    backgroundColor(background2Color.opacity(0.5f))

                    child(LABEL) {
                        textColor(fontColor2.opacity(0.7f))
                    }
                }

                child(LABEL) {
                    textColor(fontColor2)
                    graphicTextGap( 4)
                }

                child(CLOSE_BUTTON) {
                    focusTraversable(false)
                    padding(0, -2, 4, 4)
                    labelPadding(0)
                    noBackground()
                    graphic(smallIcons, "close")
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)

                    child(IMAGE_VIEW) {
                        tint(fontColor)
                    }
                }

                TINTED {
                    child(LABEL) {
                        child(IMAGE_VIEW) {
                            tint(fontColor)
                        }
                    }
                }
            }
        }
        //endregion

        //region  ==== CheckBox ====
        CHECK_BOX {
            graphic(smallIcons, "blank")
            padding(4, 2)
            textColor(fontColor)
            border(NoBorder)
            borderSize(1f)
            graphicTextGap(6f)
            focusTraversable(true)
            focusAcceptable(true)

            child(IMAGE_VIEW) {
                borderSize(2)
                roundedBorder(radius)
                borderColor(fontColor)
                tint(fontColor)
            }

            (HOVER or SELECTED) {
                textColor(fontColor)
                child(IMAGE_VIEW) {
                    borderColor(fontColor)
                }
            }

            SELECTED {
                graphic(smallIcons, "check_box:selected")
            }

            INDETERMINATE {
                graphic(smallIcons, "check_box:indeterminate")
                child(IMAGE_VIEW) {
                    backgroundColor(fontColor2.opacity(0.2f))
                    tint(fontColor)
                }
            }

            DISABLED {
                focusTraversable(false)
                focusAcceptable(false)
                textColor(fontColor2)

                child(IMAGE_VIEW) {
                    borderColor(fontColor2)
                    tint(fontColor2)
                }
            }

        }

        // An alternative look for a checkbox, which looks like a switch moving from one position to another.
        SWITCH {
            val switchRadius = 10f
            val switchPadding = 18
            graphic(smallIcons, "switch_knob")
            padding(4, 2)
            textColor(fontColor)
            border(NoBorder)
            borderSize(1f)
            graphicTextGap(6f)
            focusTraversable(true)
            focusAcceptable(true)

            child(IMAGE_VIEW) {
                borderSize(2)
                padding(0, switchPadding, 0, 0)

                roundedBackground(switchRadius)
                backgroundColor(fontColor2)

                tint(background2Color)
            }

            SELECTED {
                child(IMAGE_VIEW) {
                    backgroundColor(accentColor)
                    padding(0, 0, 0, switchPadding)
                }
            }

            INDETERMINATE {
                child(IMAGE_VIEW) {
                    padding(0, switchPadding/2, 0, switchPadding/2)
                }
            }

            DISABLED {
                focusTraversable(false)
                focusAcceptable(false)
                textColor(fontColor2)

                child(IMAGE_VIEW) {
                    backgroundColor(fontColor2.opacity(0.5f))
                    tint(background2Color.opacity(0.5f))
                }
            }

        }
        //endregion

        //region  ==== Separator ====
        SEPARATOR {
            child(LINE) {
                borderSize(0.5)
                plainBorder()
                borderColor(strokeColor)
            }
        }
        //endregion

        //region  ==== ToolBar ====
        TOOL_BAR {
            TOP { borderSize(0, 0, 1, 0) }
            BOTTOM { borderSize(1, 0, 0, 0) }
            LEFT { borderSize(0, 1, 0, 0) }
            RIGHT { borderSize(0, 0, 0, 1) }
        }

        (TOOL_BAR or MENU_BAR) {
            plainBackground()
            backgroundColor(background1Color)

            plainBorder()
            borderColor(strokeColor)

            padding(4)

            ".secondary" {
                plainBackground()
                backgroundColor(background2Color)
            }

            (TOP or BOTTOM) {
                child(CONTAINER) {
                    alignment(Alignment.CENTER_LEFT)

                    child(SEPARATOR) {
                        padding(0, 4)
                        orientation(Orientation.VERTICAL)
                    }
                }
            }

            (LEFT or RIGHT) {

                child(CONTAINER) {
                    alignment(Alignment.TOP_LEFT)

                    child(SEPARATOR) {
                        padding(4, 0)
                        orientation(Orientation.HORIZONTAL)
                    }
                }
            }

            child(OVERFLOW_BUTTON) {
                noBorder()
                noBackground()
                padding(2)
                contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                graphic(scaledIcons, "more")

                // TODO Unused, and there is no border???
                (HOVER or FOCUSED) { borderColor(accentColor) }

                child(IMAGE_VIEW) {
                    tint(fontColor)
                }
            }

            child(CONTAINER) {
                spacing(6)

                // Special case for button-like nodes in a ToolBar
                // Buttons are more compact, and omit the text, if a graphic is present.
                child(BUTTON or TOGGLE_BUTTON or RADIO_BUTTON2 or MENU_BUTTON or SPLIT_MENU_BUTTON) {
                    // graphic-only Buttons have no background in their "normal" or disabled states.
                    HAS_GRAPHIC {
                        backgroundColor(Color.TRANSPARENT)
                        HOVER { backgroundColor(buttonColor.opacity(0.5f)) }
                        ARMED { backgroundColor(buttonColor) }
                    }
                    padding(2, 4)
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                }
                child(TOGGLE_BUTTON or RADIO_BUTTON) {
                    HAS_GRAPHIC {
                        SELECTED { backgroundColor(accentColor.opacity(0.5f)) }
                    }
                }

                child(SPLIT_MENU_BUTTON) {
                    padding(4, 0, 4, 4)
                }
            }
        }

        // The popup which appears showing the items which do no fit within the toolbar itself.
        TOOL_BAR_OVERFLOW {
            borderSize(1)
            padding(2)
            spacing(6)

            roundedBorder(6)
            borderColor(strokeColor)

            plainBackground()
            backgroundColor(background1Color)
        }

        MENU_BAR {
            padding(2, 0)
            child(CONTAINER) {
                spacing(2)
            }
        }

        //endregion

        // region ==== ButtonBar ====
        BUTTON_BAR {
            spacing(16)
            padding(16, 16)
            fillHeight(true)
            borderSize(1, 0, 0, 0)
            plainBorder()
            borderColor(strokeColor)

            child(BUTTON_BAR_SECTION) {
                spacing(12)
                child(BUTTON) {
                    minWidth(120)
                }
                child(LABEL) {
                    alignment(Alignment.CENTER_LEFT)
                }
            }
        }
        // endregion

        //region  ==== TitledPane ====
        TITLED_PANE {

            child(TITLE) {
                textColor(fontColor)
                borderSize(1)

                plainBorder()
                borderColor(strokeColor)

                plainBackground()
                backgroundColor(background2Color)

                padding(4)
            }
            child(SINGLE_CONTAINER) {
                plainBackground()
                backgroundColor(background1Color)

                plainBorder()
                borderColor(strokeColor)

                borderSize(0, 1, 1, 1)
            }

            ALT_BACKGROUND {
                child(SINGLE_CONTAINER) {
                    backgroundColor(background2Color)
                }

                child(TITLE) {
                    backgroundColor(background1Color)
                }
            }

            COLLAPSABLE {
                child(TITLE) {
                    graphic(scaledIcons, "expanded")

                    (HOVER or ARMED) {
                        borderColor(accentColor)
                    }

                    child(IMAGE_VIEW) {
                        tint(fontColor.opacity(0.8f))
                    }
                }
                CONTRACTED {
                    child(TITLE) {
                        graphic(scaledIcons, "contracted")
                    }
                }

            }
        }
        //endregion

        //region  ==== SplitPane ====
        SPLIT_PANE {

            child(DIVIDER) {
                plainBackground()
                backgroundColor(buttonColor.opacity(0.5f))

                plainBorder()
                borderColor(strokeColor)

                HOVER { backgroundColor(accentColor.opacity(0.2f)) }
                ARMED { backgroundColor(accentColor.opacity(0.5f)) }
            }

            HORIZONTAL {
                child(DIVIDER) {
                    borderSize(0, 1)
                    padding(0, 3)
                }
            }
            VERTICAL {
                child(DIVIDER) {
                    borderSize(1, 0)
                    padding(3, 0)
                }
            }
        }
        //endregion

        //region ==== FormGrid ====
        FORM_GRID {
            padding(10)
            "columnSpacing"(20f)
            "rowSpacing"(10f)

            ".form" {
                padding(30f)
                "minLeftWidth"(250f)
            }
        }
        FORM_ROW {
            "spacing"(2f)

            descendant(LABEL) {
                ROW_HEADING {
                    font(Tantalum.font.bold())
                }
            }
        }
        //endregion

        // region ==== FIELD_WITH_BUTTONS ====

        // A HBox containing a TextField and one or more buttons.
        // In some cases the first node may be a TextArea instead (and possibly wrapped by with an `expandBar`).
        FIELD_WITH_BUTTONS {
            spacing(2)
            "fillHeight"(true)
            "growPriority"(1f)
        }

        // endregion

        // region ==== EXPAND_BAR ====

        EXPAND_BAR {
            plainBackground()
            backgroundColor(buttonColor.opacity(0.5f))

            plainBorder()
            borderColor(strokeColor)

            HOVER { backgroundColor(accentColor.opacity(0.2f)) }
            ARMED { backgroundColor(accentColor.opacity(0.5f)) }
            padding(3)
        }

        // endregion

        // region ==== ScrollPane ====

        // endregion

        //region  ==== ScrollBar ====
        SCROLL_BAR {

            VERTICAL {
                padding(0, 1)
                child(THUMB) {
                    minHeight(40)
                }
            }
            HORIZONTAL {
                padding(1, 0)
                child(THUMB) {
                    minWidth(40)
                }
            }

            HOVER {
                child(THUMB) {
                    roundedBackground(radius)
                    backgroundColor(fontColor.opacity(0.3f))
                }
            }

            child(THUMB) {
                // borderSize + padding determines thickness of the thumb.
                // borderSize(1)
                padding(radius + 1)
                // Thumbs are quite transparent when not being used.
                roundedBackground(radius)
                backgroundColor(fontColor.opacity(0.1f))

                ARMED {
                    backgroundColor(fontColor.opacity(0.4f))
                }
            }

        }
        //endregion

        // region ==== FolderBar ====
        FOLDER_BAR {
            padding(8, 4)
        }
        // endregion

        // region ==== ColorButton ====
        COLOR_BUTTON {
            "radius"(2f)
            minWidth(34f)
            minHeight(34f)
            padding(6)

            child(GRAPHIC) {
                roundedBackground(3f)
                roundedBorder(3f)
                borderColor(strokeColor)
                borderSize(1)

                minWidth(26)
                minHeight(26)
            }
        }
        // endregion

        // region ==== ChoiceBox ====
        CHOICE_BOX {
            child(BUTTON) {
                contentDisplay(ContentDisplay.RIGHT)
                graphic(smallIcons, "down")
                child(IMAGE_VIEW) {
                    tint(fontColor.opacity(0.7f))
                }
            }
        }
        // endregion

        // region ==== PaletteColorPicker ====
        "palette_color_picker" {

            "swatchSize"(24f)

            child(".grid") {
                spacing(1)
                child(".row") {
                    spacing(1)
                    child(RADIO_BUTTON) {

                        borderSize(2)
                        noBorder()

                        SELECTED {
                            borderColor(whiteColor)
                        }
                    }
                }
            }
        }

        // endregion

        // region ==== ListView ====
        LIST_VIEW {
            focusAcceptable(true)
            focusTraversable(true)

            plainBorder()
            borderColor(strokeColor)
            borderSize(1)

            descendant(LIST_CELL) {
                plainBackground()
                padding(2, 4)
                ODD { backgroundColor(fontColor.opacity(0.05f)) }

                SELECTED { backgroundColor(fontColor.opacity(0.2f)) }
            }

            FOCUSED {
                borderColor(accentColor)
                descendant(LIST_CELL) {
                    SELECTED { backgroundColor(accentColor.opacity(0.5f)) }
                }
            }

        }

        // endregion

        // region ==== TreeView ====
        TREE_VIEW {
            focusAcceptable(true)
            focusTraversable(true)

            plainBorder()
            borderColor(strokeColor)
            borderSize(1)

            plainBackground()
            backgroundColor(background1Color)

            descendant(TREE_ROW) {
                SELECTED { backgroundColor(fontColor.opacity(0.2f)) }
            }

            FOCUSED {
                borderColor(accentColor)

                descendant(TREE_ROW) {
                    SELECTED { backgroundColor(accentColor.opacity(0.5f)) }
                }
            }

        }

        TREE_ROW {
            padding(4, 2)
            plainBackground()

            child(ARROW) {
                focusTraversable(false)
                focusAcceptable(false)
                noBackground()
                borderSize(0)
                noBorder()
                padding(2, 4)
                labelPadding(0)
                graphic(smallIcons, "right")
                child(IMAGE_VIEW) {
                    tint(fontColor.opacity(0.7f))
                }
            }
            EXPANDED {
                child(ARROW) {
                    graphic(smallIcons, "down")
                }
            }
            LEAF {
                child(ARROW) {
                    graphic(smallIcons, "blank")
                    DISABLED {
                        noBackground()
                    }
                }
            }
        }
        // endregion

        // region ==== PopupMenu ====
        POPUP_MENU {
            // Focus is needed when using the arrow keys to traverse the menu.
            focusTraversable(true)
            focusAcceptable(true)

            borderSize(1)
            plainBorder()
            borderColor(strokeColor)

            plainBackground()
            backgroundColor(background2Color)

        }
        // endregion

        // region ==== MenuItem ====
        (MENU_ITEM or CHECK_MENU_ITEM or SUB_MENU or CUSTOM_MENU_ITEM) {
            // Focus is needed when using the arrow keys to traverse the menu.
            focusTraversable(true)
            focusAcceptable(true)

            // NOTE. Menu items can appear "selected" in two ways :
            // When they have the focus, or are hovered over. Much of the time, they will have both states.
            // However, a SubMenu will retain its HOVER state and lose its FOCUSED state when its PopupMenu is open.
            (HOVER or FOCUSED) {
                plainBackground()
                backgroundColor(accentColor.opacity(0.5f))

                DISABLED {
                    noBackground()
                }
            }
        }

        (MENU_ITEM or CHECK_MENU_ITEM or SUB_MENU) {
            "graphicSize"(24f)
            graphicTextGap(4)
            textKeyCombinationGap(24)
            font(font)
            textColor(fontColor)
            padding(4, 16, 4, 8)
            labelPadding(3)

            DISABLED {
                textColor(fontColor2)
                child(IMAGE_VIEW) {
                    tint(Color.WHITE.opacity(0.5f))
                }
            }

            TINTED {
                child(IMAGE_VIEW) {
                    tint(fontColor)
                }
                DISABLED {
                    child(IMAGE_VIEW) {
                        tint(fontColor2)
                    }
                }
            }
        }

        CHECK_MENU_ITEM {
            child(GRAPHIC) {
                image(smallIcons, "blank")
            }
            SELECTED {
                child(GRAPHIC) {
                    image(smallIcons, "tick")
                    tint(fontColor)
                }
            }
        }

        SUB_MENU {
            padding(4, 0, 4, 8)
            child(ARROW) {
                image(smallIcons, "right")
                padding(0, 6, 0, 24)
                tint(fontColor.opacity(0.7f))
            }
        }

        // endregion

        // region ==== Menu ====
        MENU {
            padding(2, 4)
            labelPadding(2)

            focusTraversable(true)
            focusAcceptable(true)

            SELECTED {
                backgroundColor(accentColor.opacity(0.5f))
            }
        }
        // endregion

        // region  ==== Ruler ====
        RULER {
            font(12f, "SansSerif")
            textColor(fontColor)
            "markerColor"(caretColor.opacity(0.7f))
            "extraMarkersColor"(accentColor.opacity(0.7f))
            "markerThickness"(2f)

            TOP { padding(1, 0, 12, 0) }
            BOTTOM { padding(12, 0, 1, 0) }
            RIGHT { padding(0, 1, 0, 12) }
            LEFT { padding(0, 12, 0, 1) }
        }
        // endregion

        // region ==== General Extra Styles ====
        ".border_all" {
            borderColor(strokeColor)
            borderSize(1)
        }
        ".border_top" {
            borderColor(strokeColor)
            borderSize(1, 0, 0, 0)
        }
        ".border_bottom" {
            borderColor(strokeColor)
            borderSize(0, 0, 1, 0)
        }
        ".border_left" {
            borderColor(strokeColor)
            borderSize(0, 0, 0, 1)
        }
        ".border_right" {
            borderColor(strokeColor)
            borderSize(0, 1, 0, 0)
        }
        ".border_top_bottom" {
            borderColor(strokeColor)
            borderSize(1, 0, 1, 0)
        }
        ".border_left_right" {
            borderColor(strokeColor)
            borderSize(0, 1, 0, 1)
        }

        ".swatch" {
            borderSize(1f)
            raisedBorder(Color.BLACK.opacity(0.5f), Color.WHITE.opacity(0.5f))
        }

        // endregion

        // region ==== AlertDialog ====
        ".alert_pane" {
            minWidth(400)
            child(".heading") {
                padding(8, 10)
                labelPadding(10, 0) // As the graphic is larger than the text, the Y only affects when there is no graphic.
                font(fontSize * 1.5f, FontStyle.PLAIN, fontFamily)
                plainBackground()
                backgroundColor(background2Color)
                contentDisplay(ContentDisplay.RIGHT)
                borderSize(0, 0, 1, 0) // A line between the heading and the messageArea.
                plainBorder()
                borderColor(strokeColor)
            }
            child(".message") {
                READ_ONLY {
                    textColor(fontColor)
                }
            }
            ".information" { child(".heading") { graphic(icons, "information") } }
            ".warning" { child(".heading") { graphic(icons, "warning") } }
            ".error" { child(".heading") { graphic(icons, "error") } }
            ".confirmation" { child(".heading") { graphic(icons, "confirmation") } }
        }
        // endregion

        // region ==== PromptDialog ====
        ".prompt_pane" {
            minWidth(400)
            child(".heading") {
                padding(8, 10)
                labelPadding(
                    10,
                    0
                ) // As the graphic is larger than the text, the Y only affects when there is no graphic.
                font(fontSize * 1.5f, FontStyle.PLAIN, fontFamily)
                plainBackground()
                backgroundColor(background2Color)
                contentDisplay(ContentDisplay.RIGHT)
                borderSize(0, 0, 1, 0) // A line between the heading and the messageArea.
                plainBorder()
                borderColor(strokeColor)
            }
            child(".content") {
                padding(8)
                spacing(8)
            }
        }
        // endregion

        // region ==== Harbour ====
        "harbour" {
            "iconSize"(dockIconSize)
        }
        "harbour_side" {
            child(".buttons") {
                plainBackground()
                backgroundColor(background2Color)

                descendant(TOGGLE_BUTTON) {
                    noBackground()
                    padding(2, 4)
                    labelPadding(1)
                    HOVER { backgroundColor(buttonColor.opacity(0.7f)) }
                    SELECTED {
                        plainBackground()
                        backgroundColor(accentColor.opacity(0.2f))
                    }
                    ARMED { backgroundColor(accentColor) }

                    child(IMAGE_VIEW) {
                        tint(fontColor)
                    }
                }

                child(".primary") {
                    descendant(TOGGLE_BUTTON) {
                        ":drag_hover" {
                            // Highlight to indicate we are inserting the dock before this button
                            borderSize(0, 0, 0, 20)
                            borderColor(accentColor.opacity(0.7f))
                        }
                    }
                }
                child(".secondary") {
                    descendant(TOGGLE_BUTTON) {
                        ":drag_hover" {
                            // Highlight to indicate we are inserting the dock before this button
                            borderSize(0, 20, 0, 0)
                            borderColor(accentColor.opacity(0.7f))
                        }
                    }
                }

            }

            // The left buttons are rotated, such that the "gap" needs to be on the other edge.
            ":left" {
                child(".buttons") {
                    child(".primary") {
                        descendant(TOGGLE_BUTTON) {
                            ":drag_hover" {
                                // Highlight to indicate we are inserting the dock before this button
                                borderSize(0, 20, 0, 0)
                            }
                        }
                    }
                    child(".secondary") {
                        descendant(TOGGLE_BUTTON) {
                            ":drag_hover" {
                                // Highlight to indicate we are inserting the dock before this button
                                borderSize(0, 0, 0, 20)
                            }
                        }
                    }
                }
            }

            ":drag_target" {
                descendant(".gap") {
                    // Highlight to indicate the dock can be dragged to this side.
                    plainBackground()
                    backgroundColor(accentColor.opacity(0.3f))
                    minWidth(20)
                    minHeight(20)

                    ":drag_hover" {
                        // Highlight to indicate we are inserting the dock after other docks.
                        backgroundColor(accentColor.opacity(0.7f))
                    }
                }
            }

        }
        // endregion

        // region ==== Dock ====
        "dock" {
            child(".container") {
                child(TOOL_BAR) {
                    plainBackground()
                    backgroundColor(background2Color)
                    child(".container") {
                        child(".title") {
                            minWidth(20)
                            child(IMAGE_VIEW) {
                                tint(fontColor)
                            }
                        }
                    }
                }
            }
        }
        // endregion

        // region ==== Tooltip ====
        TOOLTIP {
            val radius = 10
            borderSize(1)

            roundedBorder(radius)
            borderColor(strokeColor)

            roundedBackground(radius - 1)
            backgroundColor(background2Color)

            padding(10)
            spacing(4)
        }
        // endregion tooltip

        // region ==== WindowDecoration ====
        // For the window border and title bar of stages which are not native windows, and the
        // stage is a `fake` OverlayStage within the client area of a `real` RegularStage, which
        // does use a native window.
        WINDOW_DECORATION {
            val radius = 6f
            val resizableBorderThickness = 6f
            val fixedBorderThickness = 1f
            borderSize(fixedBorderThickness)
            roundedBorder(radius)
            borderColor(fontColor2)

            roundedBackground(radius)
            backgroundColor(background1Color)

            child(TITLE_BAR) {
                roundedBackground(radius - fixedBorderThickness, Side.TOP)
                backgroundColor(background2Color)

                borderSize(0, 0, 1, 0)
                plainBorder()
                borderColor(strokeColor)

                padding(6)

                child(BUTTON) { // The stage's close button
                    noBackground()
                    padding(0)
                    graphic(smallIcons, "close")
                    contentDisplay(ContentDisplay.GRAPHIC_ONLY)
                    child(IMAGE_VIEW) {
                        tint(fontColor)
                    }
                }
            }

            STAGE_RESIZABLE {
                borderSize(resizableBorderThickness)
                // Special border and background, which leaves a transparent margin.
                // When the mouse is in this transparent margin, the use can click and drag to resize the stage.
                border(OverlayStageBorder(radius, 1f))
                borderColor(fontColor2)

                background(OverlayStageBackground(radius, 1f))
                backgroundColor(background1Color)
            }

            STAGE_FOCUSED {
                child(TITLE_BAR) {
                    backgroundColor(accentColor.opacity(0.8f))
                }
            }
            (STAGE_RESIZABLE and STAGE_FOCUSED) {
                child(TITLE_BAR) {
                    backgroundColor(accentColor.opacity(0.8f))
                }
            }

        }

        // endregion

        // region ---- Focus ----

        // Sections which are highlighted with a semi-transparent "background", which is drawn ON TOP
        // of the node.
        REGION {
            focusHighlight(PlainBackground)
            focusHighlightColor(accentColor.opacity(0.2f))
        }

        // Sections which are NOT highlighted
        (TEXT_AREA or STYLED_TEXT_AREA or TEXT_FIELD or LIST_VIEW or TREE_VIEW) {
            focusHighlight(NoBackground)
        }

        // Controls which have an extra border around them, when pressing TAB or Shift+TAB
        (CHECK_BOX or SWITCH or TAB or MENU or SLIDER or SLIDER_2D or COLOR_SLIDER) {
            focusBorder(PlainBorder)
            focusBorderColor(accentColor)
            focusBorderSize(focusBorderThickness)
        }

        // endregion
    }

    /**
     * Builds a [FormGrid], letting the user change some aspects of the Tantalum Theme.
     *
     * Include this as a tab within your application's preferences/setting dialog.
     * Wrap it in a [ScrollPane].
     *
     * You can add additional rows to the FormGrid if you wish.
     * Each row is given a [Node.id], which could be used to remove rows too.
     *
     * The layout has been shamelessly copied from `Muse Score`.
     */
    fun appearanceForm() = formGrid {
        style(".form")

        + rowHeading("Theme")

        + row {
            id = "theme"
            radioChoices(darkProperty) {
                left = vBox {
                    spacing = 12f
                    + propertyRadioButton2(false, "") {
                        padding(0)
                        graphicProperty.bindTo(themeGraphic(false))
                    }
                    + propertyRadioButton(false, "Light")
                }
                right = vBox {
                    spacing = 8f
                    + propertyRadioButton2(true, "") {
                        padding(0)
                        graphicProperty.bindTo(themeGraphic(true))
                    }
                    + propertyRadioButton(true, "Dark")
                }
            }
        }

        + row("Accent Color") {
            id = "accent"
            val darkColors = listOf("#f25555", "#e1720b", "#ac8c1a", "#27a341", "#2093fe", "#926bff", "#e454c4")
            val lightColors = listOf("#f28585", "#edb17a", "#e0cc87", "#8bc9c5", "#70afea", "#a09eef", "#dba0c7")
            toggleGroup {
                right = hBox {
                    for (index in darkColors.indices) {
                        + accentRadioButton(Color[darkColors[index]], Color[lightColors[index]])
                    }
                }
            }
        }

        + rowSeparator()

        + rowHeading("Appearance")

        + row("Font Family") {
            id = "fontFamily"
            right = choiceBox<String> {
                overrideMinWidth = 250f
                items.addAll(Font.allFontFamilies())
                valueProperty.bidirectionalBind(fontFamilyProperty, object : Converter<String?, String> {
                    override fun backwards(value: String) = value
                    override fun forwards(value: String?) = value ?: Font.defaultFont.identifier.family
                })
            }
        }
        + row("Text Size") {
            id = "textSize"
            right = floatSpinner {
                editor.prefColumnCount = 6
                min = 10f
                max = 30f
                valueProperty.bidirectionalBind(fontSizeProperty)
            }
        }

        + row("Icon Size") {
            id = "iconSize"
            right = intSpinner {
                editor.prefColumnCount = 6
                min = 10
                max = 40
                valueProperty.bidirectionalBind(iconSizeProperty)
            }
        }

        + row("Global Scaling") {
            id = "globalScaling"
            right = floatSpinner {
                editor.prefColumnCount = 6
                min = 0.6f
                max = 4.0f
                smallStep = 0.1f
                largeStep = 1.0f
                valueProperty.bidirectionalBind(GlokSettings.globalScaleProperty)
            }
        }
    }

    /**
     * Part of [appearanceForm].
     *
     * Creates a [RadioButton], which changes the accent color for BOTH the dark and light
     * color schemes. e,g, when the user chooses blue, the `accent` color for the
     * [lightColorScheme] and [darkColorScheme] are changed to blue (which are different shades of blue
     * for the dark and light color scheme).
     */
    private fun ToggleGroupDSL.accentRadioButton(darkColor: Color, lightColor: Color) =
        radioButton("") {
            selected = accentColor == darkColor || accentColor == lightColor
            graphic = imageView(icons.size(40)["radio_button:selected"]) {
                padding(0)
                tintProperty.bindTo(ColorUnaryFunction(darkProperty) { if (it) darkColor else lightColor })
                border = RoundedBorder(40f)
                borderColorProperty.bindTo(
                    ColorBinaryFunction(selectedProperty, fontColorProperty) { selected, fontColor ->
                        if (selected) {
                            fontColor
                        } else {
                            Color.TRANSPARENT
                        }
                    })
            }
            onAction {
                darkColorScheme["accent"] !!.value = darkColor
                lightColorScheme["accent"] !!.value = lightColor
            }
        }

    /**
     * Part of [appearanceForm].
     *
     * Builds a graphic, made up of [Pane]s with different background colors.
     */
    private fun themeGraphic(dark: Boolean) = OptionalNodeUnaryFunction(themeProperty) {
        val colors = if (dark) darkColorScheme else lightColorScheme

        stackPane {
            alignment = Alignment.TOP_LEFT
            + pane {
                background = RoundedBackground(radius)
                backgroundColorProperty.bindTo(colors["background1"] !!)
                overridePrefWidth = 124f
                overridePrefHeight = 88f
            }
            + hBox {
                padding(12, 0, 0, 18)
                + pane {
                    background = PlainBackground
                    backgroundColorProperty.bindTo(colors["background2"] !!)

                    border = RoundedBorder(radius)
                    borderColorProperty.bindTo(colors["stroke"] !!)

                    borderSize(1)
                    overridePrefWidth = 106f
                    overridePrefHeight = 76f
                }
            }
            + hBox {
                padding(22, 0, 0, 30)
                + pane {
                    background = RoundedBackground(radius)
                    backgroundColorProperty.bindTo(colors["background1"] !!)
                    border = RoundedBorder(radius)
                    borderColorProperty.bindTo(colors["stroke"] !!)

                    borderSize(1)
                    overridePrefWidth = 80f
                    overridePrefHeight = 38f
                }
            }
            + vBox {
                padding(28, 0, 0, 36)
                spacing(5)
                + pane {
                    background = RoundedBackground(radius)
                    backgroundColorProperty.bindTo(colors["font"] !!)
                    overridePrefWidth = 64f
                    overridePrefHeight = 5f
                }
                + pane {
                    background = RoundedBackground(radius)
                    backgroundColorProperty.bindTo(colors["font"] !!)

                    overridePrefWidth = 64f
                    overridePrefHeight = 5f
                }
                + pane {
                    background = RoundedBackground(radius)
                    backgroundColorProperty.bindTo(colors["font"] !!)

                    overridePrefWidth = 32f
                    overridePrefHeight = 5f
                }
                + hBox {
                    padding(10, 0, 0, - 6)
                    spacing(6)
                    + pane {
                        background = RoundedBackground(radius)
                        backgroundColorProperty.bindTo(colors["accent"] !!)
                        overridePrefWidth = 38f
                        overridePrefHeight = 14f
                    }
                    + pane {
                        background = RoundedBackground(radius)
                        backgroundColorProperty.bindTo(colors["button"] !!)
                        overridePrefWidth = 38f
                        overridePrefHeight = 14f
                    }
                }
            }

        }
    }

    fun customColorsForm(): Node {

        val nameProperty = SimpleOptionalStringProperty("")

        val currentColorProperty = SimpleColorProperty(accentColor)
        var oldProperty: ColorProperty? = null

        nameProperty.addChangeListener { _, _, name ->
            oldProperty?.let { currentColorProperty.bidirectionalUnbind(it) }
            oldProperty = colorScheme[name]
            oldProperty?.let { currentColorProperty.bidirectionalBind(it) }
        }

        // When the color scheme changes, we need to reselect the color.
        // Otherwise, we will be editing the color in the other color scheme.
        darkProperty.addListener {
            val name = nameProperty.value
            nameProperty.value = ""
            nameProperty.value = name
        }

        nameProperty.value = "accent"

        fun HBox.colorSchemeButtons(colorScheme: Map<String, ColorProperty>) {
            radioChoices(nameProperty) {
                for ((name, colorProperty) in colorScheme) {
                    + propertyRadioButton(name, "") {
                        contentDisplay = ContentDisplay.GRAPHIC_ONLY
                        tooltip = TextTooltip(colorLabels[name] ?: "?")
                        background = NoBackground
                        graphic = imageView(icons.size(40)["radio_button:selected"]) {
                            padding(0)
                            tintProperty.bindTo(colorProperty)
                            if (name == "background2") {
                                background = RoundedBackground(42f)
                                backgroundColorProperty.bindTo(background1ColorProperty)
                            }
                            border = RoundedBorder(40f)
                            borderColorProperty.bindTo(
                                ColorBinaryFunction(selectedProperty, fontColorProperty) { selected, fontColor ->
                                    if (selected) {
                                        fontColor
                                    } else {
                                        Color.TRANSPARENT
                                    }
                                }
                            )
                        }
                    }
                }
            }
        }

        return vBox {
            fillWidth = true
            alignment = Alignment.TOP_CENTER
            padding(30f, 0)
            spacing = 30f

            + hBox {
                alignment = Alignment.TOP_CENTER
                spacing(20)
                fillHeight = true

                + choiceBox<String> {
                    overridePrefWidth = 300f
                    converter = { colorLabels[it] ?: "?" }

                    items.addAll(colorScheme.keys)
                    valueProperty.bidirectionalBind(nameProperty)
                }
                + button("Reset") {
                    tooltip = TextTooltip("Reset this color to the default")
                    style(TINTED)
                    graphic = ImageView(scaledIcons["refresh"])
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    onAction {
                        currentColorProperty.value =
                            (if (dark) darkColors else lightColors)[nameProperty.value] !!
                    }
                }
            }

            + hBox {
                alignment = Alignment.TOP_CENTER
                visibleProperty.bindTo(darkProperty)
                colorSchemeButtons(darkColorScheme)
            }
            + hBox {
                alignment = Alignment.TOP_CENTER
                visibleProperty.bindTo(! darkProperty)
                colorSchemeButtons(lightColorScheme)
            }

            + hBox {
                alignment = Alignment.TOP_CENTER

                + customColorPicker(currentColorProperty.value) {
                    showCurrentColor = false
                    editAlphaProperty.bindTo(BooleanUnaryFunction(nameProperty) { name ->
                        name == "find_match" || name == "find_replacement"
                    })
                    colorProperty.bidirectionalBind(currentColorProperty)
                }
            }

            + separator {
                padding(0, 40)
                growPriority = 1f
            }

            + hBox {
                padding(0, 40)
                spacing(30)
                + styledTextArea("Example text. Matched Text. Replaced Text.") {
                    growPriority = 1f
                    prefRowCount = 1
                    with(document.ranges) {
                        val find = ThemedHighlight(".find_match")
                        val replace = ThemedHighlight(".find_replacement")
                        add(HighlightRange(TextPosition(0, 14), TextPosition(0, 26), find))
                        add(HighlightRange(TextPosition(0, 28), TextPosition(0, 41), replace))
                    }
                }
                + button("Example Disabled") {
                    disabled = true
                }
            }

        }
    }

}

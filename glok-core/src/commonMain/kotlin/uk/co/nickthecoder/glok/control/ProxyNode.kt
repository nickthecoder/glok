/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.Matrix

/**
 * Makes a node appear in a different part of the scene-graph.
 *
 * This was originally designed to display nodes from [TabBar] in its `overflow` popup.
 *
 * The nodes which cannot fit in the [TabBar] appear in a `Popup`. Originally, I _moved_
 * them to the `Popup`, but the styling failed (because they are no longer in the [TabBar],
 * and therefore different `Theme` rules apply).
 *
 * So instead, we _keep_ them in the [TabBar], but don't draw them.
 * The `Popup` has a [ProxyNode] for each Node which cannot fit in the [ToolBar].
 *
 * This proxy then draws the node, by using a translation matrix to move it from its position in the
 * toolbar, to the ProxyNode's position.
 *
 * We also add event handlers/filters and forward them on to the [wrapped] node (and its descendants).
 */
class ProxyNode(val wrapped: Node) : Node() {

    init {
        focusTraversable = true
        focusAcceptable = true

        for (eventType in listOf(
            EventType.MOUSE_ENTERED,
            EventType.MOUSE_EXITED,
            EventType.MOUSE_PRESSED,
            EventType.MOUSE_RELEASED,
            EventType.MOUSE_MOVED,
            EventType.MOUSE_DRAGGED
        )) {
            addEventFilter(eventType, object : EventHandler<MouseEvent> {
                override fun handle(event: MouseEvent) {
                    val x = event.sceneX - sceneX + wrapped.sceneX
                    val y = event.sceneY - sceneY + wrapped.sceneY
                    val event2 = MouseEvent(event.eventType, event.scene, x, y,
                        event.buttonIndex, event.clickCount, event.mods)

                    fun bubbleDown(node: Node) {
                        if (node !== wrapped) {
                            node.parent?.let { bubbleDown(it) }
                        }
                        if (!event2.isConsumed()) {
                            node.findEventFilter(event.eventType)?.handle(event2)
                        }
                    }

                    bubbleDown(wrapped.findDeepestNodeAt(x, y))
                }
            })
            addEventHandler(eventType, object : EventHandler<MouseEvent> {
                override fun handle(event: MouseEvent) {
                    val x = event.sceneX - sceneX + wrapped.sceneX
                    val y = event.sceneY - sceneY + wrapped.sceneY
                    val event2 = MouseEvent(event.eventType, event.scene, x, y,
                        event.buttonIndex, event.clickCount, event.mods)

                    fun bubbleUp(node: Node) {
                        node.findEventHandler(event.eventType)?.handle(event2)
                        if (!event2.isConsumed() && node !== wrapped) {
                            node.parent?.let { bubbleUp(it) }
                        }
                    }

                    bubbleUp(wrapped.findDeepestNodeAt(x, y))
                    if (eventType == EventType.MOUSE_PRESSED) {
                        if (wrapped.scene?.focusOwner?.firstToRoot { it === wrapped } != null) {
                            requestFocus()
                        }
                    }
                }
            })
        }

        addEventFilter(EventType.SCROLL, object : EventHandler<ScrollEvent> {
            override fun handle(event: ScrollEvent) {
                val x = event.sceneX - sceneX + wrapped.sceneX
                val y = event.sceneY - sceneY + wrapped.sceneY
                val event2 = ScrollEvent(event.scene, x, y, event.deltaX, event.deltaY, event.mods)

                fun bubbleDown(node: Node) {
                    if (node !== wrapped) {
                        node.parent?.let { bubbleDown(it) }
                    }
                    if (!event2.isConsumed()) {
                        node.findEventFilter(event.eventType)?.handle(event2)
                    }
                }
                bubbleDown(wrapped.findDeepestNodeAt(x, y))
            }
        })
        addEventHandler(EventType.SCROLL, object : EventHandler<ScrollEvent> {
            override fun handle(event: ScrollEvent) {
                val x = event.sceneX - sceneX + wrapped.sceneX
                val y = event.sceneY - sceneY + wrapped.sceneY
                val event2 = ScrollEvent(event.scene, x, y, event.deltaX, event.deltaY, event.mods)

                fun bubbleUp(node: Node) {
                    node.findEventHandler(event.eventType)?.handle(event2)
                    if (!event2.isConsumed() && node !== wrapped) {
                        node.parent?.let { bubbleUp(it) }
                    }
                }

                bubbleUp(wrapped.findDeepestNodeAt(x, y))
            }
        })

        for (eventType in listOf(
            EventType.KEY_PRESSED,
            EventType.KEY_RELEASED
        )) {
            addEventFilter(eventType, object : EventHandler<KeyEvent> {
                override fun handle(event: KeyEvent) {
                    fun bubbleDown(node: Node) {
                        if (node !== wrapped) {
                            node.parent?.let { bubbleDown(it) }
                        }
                        if (!event.isConsumed()) {
                            node.findEventFilter(event.eventType)?.handle(event)
                        }
                    }
                    wrapped.scene?.focusOwner?.let { bubbleDown(it) }
                }
            })
            addEventHandler(eventType, object : EventHandler<KeyEvent> {
                override fun handle(event: KeyEvent) {
                    fun bubbleUp(node: Node) {
                        node.findEventHandler(event.eventType)?.handle(event)
                        if (!event.isConsumed() && node !== wrapped) {
                            node.parent?.let { bubbleUp(it) }
                        }
                    }
                    wrapped.scene?.focusOwner?.let { bubbleUp(it) }
                }
            })
        }

        addEventFilter(EventType.KEY_TYPED, object : EventHandler<KeyTypedEvent> {
            override fun handle(event: KeyTypedEvent) {
                fun bubbleDown(node: Node) {
                    if (node !== wrapped) {
                        node.parent?.let { bubbleDown(it) }
                    }
                    if (!event.isConsumed()) {
                        node.findEventFilter(event.eventType)?.handle(event)
                    }
                }
                wrapped.scene?.focusOwner?.let { bubbleDown(it) }
            }
        })
        addEventHandler(EventType.KEY_TYPED, object : EventHandler<KeyTypedEvent> {
            override fun handle(event: KeyTypedEvent) {

                fun bubbleUp(node: Node) {
                    node.findEventHandler(event.eventType)?.handle(event)
                    if (!event.isConsumed() && node !== wrapped) {
                        node.parent?.let { bubbleUp(it) }
                    }
                }
                wrapped.scene?.focusOwner?.let { bubbleUp(it) }
            }
        })
    }

    // region ==== Layout ====
    override fun nodeMinWidth() = wrapped.width
    override fun nodeMinHeight() = wrapped.height

    override fun nodePrefWidth() = wrapped.width
    override fun nodePrefHeight() = wrapped.height

    override fun nodeMaxWidth() = wrapped.width
    override fun nodeMaxHeight() = wrapped.height

    override fun layoutChildren() {}

    override fun draw() {

        val dx = sceneX - wrapped.sceneX
        val dy = sceneY - wrapped.sceneY

        val matrix = Matrix.translation(dx, dy)

        backend.transform(matrix) {
            wrapped.drawAll()
        }
    }
    // endregion
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.boilerplate.ObservableColor
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.property.boilerplate.orientationProperty
import uk.co.nickthecoder.glok.property.functions.*

/**
 * Draws a [Background] as a linear gradient.
 * The left/bottom is [fromHue] and the right/top is [toHue] depending on [orientation].
 *
 */
class HSVGradientBackground(

    orientation: Orientation,

    fromHue: Float,
    fromSaturation: Float,
    fromValue: Float,
    fromAlpha: Float,

    toHue: Float,
    toSaturation: Float,
    toValue: Float,
    toAlpha: Float

) : Background {

    val orientationProperty by orientationProperty(orientation)
    var orientation by orientationProperty

    val fromHueProperty by floatProperty(fromHue)
    var fromHue by fromHueProperty

    val fromSaturationProperty by floatProperty(fromSaturation)
    var fromSaturation by fromSaturationProperty

    val fromValueProperty by floatProperty(fromValue)
    var fromValue by fromValueProperty

    val fromAlphaProperty by floatProperty(fromAlpha)
    var fromAlpha by fromAlphaProperty


    val toHueProperty by floatProperty(toHue)
    var toHue by toHueProperty

    val toSaturationProperty by floatProperty(toSaturation)
    var toSaturation by toSaturationProperty

    val toValueProperty by floatProperty(toValue)
    var toValue by toValueProperty

    val toAlphaProperty by floatProperty(toAlpha)
    var toAlpha by toAlphaProperty


    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        val x2 = x + size.left
        val y2 = y + size.top
        val width2 = width - size.left - size.right
        val height2 = height - size.top - size.bottom

        val fromHSVA = floatArrayOf(fromHue, fromSaturation, fromValue, fromAlpha)
        val toHSVA = floatArrayOf(toHue, toSaturation, toValue, toAlpha)

        backend.hsvGradient(
            floatArrayOf(
                x2, y2, x2 + width2, y2, x2 + width2, y2 + height2,
                x2, y2, x2, y2 + height2, x2 + width2, y2 + height2
            ),
            if (orientation == Orientation.HORIZONTAL) {
                arrayOf(
                    fromHSVA, toHSVA, toHSVA,
                    fromHSVA, fromHSVA, toHSVA
                )
            } else {
                arrayOf(
                    toHSVA, toHSVA, fromHSVA,
                    toHSVA, fromHSVA, fromHSVA
                )
            }
        )
    }

    companion object {
        fun forHue(orientation: Orientation, colorProperty: ObservableColor, fromHue: Float, toHue: Float): HSVGradientBackground {
            val saturation = colorProperty.saturation()
            val lightness = colorProperty.lightness()
            val alpha = colorProperty.alpha()

            return HSVGradientBackground(orientation, fromHue, 0f, 0f, 0f, toHue, 0f, 0f, 0f).apply {

                fromSaturationProperty.bindTo(saturation)
                toSaturationProperty.bindTo(saturation)

                fromValueProperty.bindTo(lightness)
                toValueProperty.bindTo(lightness)

                fromAlphaProperty.bindTo(alpha)
                toAlphaProperty.bindTo(alpha)
            }
        }
        fun forSaturation(orientation: Orientation, colorProperty: ObservableColor, fromSaturation: Float, toSaturation: Float): HSVGradientBackground {
            val hue = colorProperty.hue()
            val brightness = colorProperty.brightness()
            val alpha = colorProperty.alpha()

            return HSVGradientBackground(orientation, 0f, fromSaturation, 0f, 0f, 0f, toSaturation, 0f, 0f).apply {

                fromHueProperty.bindTo(hue)
                toHueProperty.bindTo(hue)

                fromValueProperty.bindTo(brightness)
                toValueProperty.bindTo(brightness)

                fromAlphaProperty.bindTo(alpha)
                toAlphaProperty.bindTo(alpha)
            }
        }
        fun forValue(orientation: Orientation, colorProperty: ObservableColor, fromValue: Float, toValue: Float): HSVGradientBackground {
            val hue = colorProperty.hue()
            val saturation = colorProperty.saturation()
            val alpha = colorProperty.alpha()

            return HSVGradientBackground(orientation, 0f, fromValue, 0f, 0f, 0f, 0f, toValue, 0f).apply {

                fromHueProperty.bindTo(hue)
                toHueProperty.bindTo(hue)

                fromSaturationProperty.bindTo(saturation)
                toSaturationProperty.bindTo(saturation)

                fromAlphaProperty.bindTo(alpha)
                toAlphaProperty.bindTo(alpha)
            }
        }
    }
}

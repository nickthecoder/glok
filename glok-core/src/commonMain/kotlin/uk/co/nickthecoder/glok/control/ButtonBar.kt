/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.ButtonBar.Companion.defaultOrder
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableStringProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.theme.styles.BUTTON_BAR
import uk.co.nickthecoder.glok.theme.styles.BUTTON_BAR_SECTION


/**
 * A horizontal container primarily for buttons. The order of the buttons
 * matches the platform's standard, and _not_ the order the buttons were added.
 * For example, the buttons `OK`, `Cancel`, `Apply` appear in these orders :
 *
 * * Windows : `OK`, `Cancel`, `Apply`
 * * MacOs : `Cancel`, `OK`, `Apply`
 * * Linux (Gnome) : `Apply`, `Cancel`, `OK`
 *
 * The order of the buttons is governed by [buttonOrder].
 * This is a string, where each character is the code of a [ButtonMeaning].
 * The default value is dependent on the platform. See [defaultOrder].
 *
 * You assign a meaning to each button either by adding buttons using the
 * special [add] method, or by adding nodes to [items] as usual, and then
 * calling [meaningOf].
 * If no meaning is specified, then it is assumed to be [ButtonMeaning.OTHER].
 *
 * ### Scene Graph DSL
 *
 *     buttonBar {
 *         add(ButtonMeaning.OK) {
 *             button( "OK" ) { onAction { ... } }
 *         }
 *         add(ButtonMeaning.CANCEL) {
 *             button( "Cancel" ) { onAction { ... } }
 *         }
 *     }
 *
 * ### Theme DSL
 *
 *     "button_bar" {
 *
 *         buttonOrder( string )
 *         fillHeight( boolean )
 *         spacing( number ) // A feature of BoxBase, not ButtonBar itself, but included here anyway.
 *
 *         child(BUTTON_BAR_SECTION) {
 *             spacing( number ) // Should be the same as button_bar's spacing.
 *
 *             child(BUTTON) {
 *                 minWidth( number ) // minWidth is a feature common to all nodes.
 *             }
 *         }
 *
 *     }
 *
 * ButtonBar and each _section_ inherit all the features of [BoxBase].
 *
 * ### Differences from JavaFX.
 *
 * * There is no `buttonMinWidth`. Instead, the theme should include a rule such as :
 *   "button_bar" { child( "button" ) { minWidth( 100 ) } }
 *
 * * [ButtonMeaning] is the equivalent of JavaFX's `ButtonBar.ButtonData`.
 *   IMHO, their name is bad, and using an inner class makes code verbose, and hard to read.
 *
 * * Glok has two defaults for linux. See [defaultOrder].
 *
 * * The default order can be set using an environment variable. See [defaultOrder].
 *
 */
class ButtonBar : BoxBase() {

    // region ==== Properties ====

    val buttonOrderProperty by stylableStringProperty(defaultOrder)
    val buttonOrder by buttonOrderProperty

    val fillHeightProperty by stylableBooleanProperty(true)
    var fillHeight by fillHeightProperty

    // endregion ==== Properties ====

    override val orientation: Orientation
        get() = Orientation.HORIZONTAL

    override val fill: Boolean
        get() = fillHeight

    private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children = mutableChildren.asReadOnly()

    val items = mutableListOf<Node>().asMutableObservableList()

    private val childrenMeaning = mutableMapOf<Node, ButtonMeaning>()

    private val sections = mutableMapOf<Char, HBox>()

    // region ==== init =====

    init {
        styles.add(BUTTON_BAR)
        section = true

        children.addChangeListener(childrenListener)

        items.addChangeListener { _, change ->
            for (node in change.removed) {
                for (section in sections.values) {
                    section.children.remove(node)
                }
            }
            for (node in change.added) {
                val meaning = childrenMeaning[node] ?: ButtonMeaning.OTHER
                sections[meaning.code]?.children?.add(node)
            }
            buildChildren()
        }
        buttonOrderProperty.addListener { buildChildren() }
        fillHeightProperty.addListener(requestLayoutListener)

        for (meaning in ButtonMeaning.values()) {
            sections[meaning.code] = HBox().apply {
                styles.add(BUTTON_BAR_SECTION)
            }
        }

        buildChildren()
    }

    // endregion ==== init ====


    // region ==== Methods ====

    private fun buildChildren() {
        mutableChildren.clear()
        for (c in buttonOrder) {
            when (c) {
                '+' -> mutableChildren.add(Pane().apply {
                    growPriority = 1f
                })

                '_' -> mutableChildren.add(Pane().apply {
                    growPriority = 0.5f
                })

                else -> {
                    sections[c]?.let {
                        mutableChildren.add(it)
                        it.visible = it.children.isNotEmpty()
                    }
                }
            }
        }
    }

    fun add(meaning: ButtonMeaning, node: Node) {
        childrenMeaning[node] = meaning
        items.add(node)
    }

    fun add(meaning: ButtonMeaning, nodeBuilder: () -> Node) {
        add(meaning, nodeBuilder())
    }

    /**
     * If you add nodes directly via [children], then you should also specify their meaning, via this function.
     * Otherwise, the nodes are assumed to be [ButtonMeaning.OTHER], and won't be positioned
     * as you (or the users) might like.
     *
     * It is usually more convenient to use [add] instead.
     */
    fun meaningOf(meaning: ButtonMeaning, node: Node) {
        childrenMeaning[node] = meaning
    }

    // endregion ==== Methods ====

    // region ==== Layout =====

    override fun layoutChildren() {
        for (section in sections.values) {
            section.visible = (section.children.isNotEmpty())
        }

        var prevSpacer: Pane? = null
        for (child in children) {
            if (child is Pane) {
                child.visible = false
                // If there are two spacers without any buttons between them, then we only care about the larger one.
                if (prevSpacer == null || prevSpacer.growPriority < child.growPriority) {
                    prevSpacer = child
                }
            } else {
                if (child.children.firstOrNull { it.visible } != null) {
                    child.visible = true
                    prevSpacer?.let {
                        it.visible = true
                        prevSpacer = null
                    }
                } else {
                    child.visible = false
                }
            }
        }
        // If the first or last child are small spacers, then we ignore them.
        // The ensures that the buttons are left/right aligned apart from a large spacer.
        children.firstOrNull { it.visible }?.let {
            if (it is Pane && it.growPriority < 1f) it.visible = false
        }
        children.lastOrNull { it.visible }?.let {
            if (it is Pane && it.growPriority < 1f) it.visible = false
        }

        super.layoutChildren()
    }

    // endregion ==== Layout ====

    // region ==== Object methods ====

    override fun toString() = super.toString() + " Order($buttonOrder)"

    // endregion

    companion object {
        // NOTE, JavaFX docs stated L_E+U+FBXI_YNOCAH_R but there is no F code
        /**
         * Left _ Help2 + Other + Back Next Finish _ Yes No Ok Cancel Apply Help1 _ Right
         */
        const val WINDOWS_ORDER = "L_E+U+BXI_YNOCAH_R"
        // NOTE, JavaFX docs stated L_HE+U+FBIX_NCYOA_R but there is no F code.
        // Also, does MacOD really used Back, Finish, Forwards ?
        /**
         * Left _ Help1 Help2 + Other + Back Finish Next _ No Cancel Yes Ok Apply _ Right
         */
        const val MAC_OS_ORDER = "L_HE+U+BIX_NCYOA_R"

        /**
         * Left _ Help1 Help2 + Other No Yes Apply Cancel Back Next Finish OK _ Right
         */
        const val GNOME_ORDER = "L_HE+UNYACBXIO_R"

        /**
         * Left _ Help1 Help2 + Yes No Cancel Ok Apply Back Next Finish _ Right
         */
        const val KDE_ORDER = "L_HEU+YNCOABXI_R" // This is my own concoction

        /**
         * The default order of buttons in a ButtonBar.
         * One of [WINDOWS_ORDER], [MAC_OS_ORDER], [GNOME_ORDER], based on system properties `os.name`, and
         * `XDG_CURRENT_DESKTOP`.
         *
         */
        val defaultOrder: String by lazy {
            if (Platform.isWindows()) {
                WINDOWS_ORDER
            } else if (Platform.isMacOS()) {
                MAC_OS_ORDER
            } else {
                if (Platform.isKDE()) {
                    KDE_ORDER
                } else {
                    GNOME_ORDER
                }
            }
        }
    }
}

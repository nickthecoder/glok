/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.theme.styles.FORM_ROW
import uk.co.nickthecoder.glok.util.*

/**
 * ## Layout
 *
 *    ┌────────────────────────────────┐
 *    │ above                          │
 *    ├╌╌╌╌╌╌╌╌╌╌╌╌╌╌┰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┤
 *    │ left         ┃ right           │
 *    ├╌╌╌╌╌╌╌╌╌╌╌╌╌╌┸╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┤
 *    │ below                          │
 *    ├╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┤
 *    │ errors                         │
 *    └────────────────────────────────┘
 *
 * [FormGrid] ensures that the x position of `right` is the same for all rows.
 *
 * Typical uses for each child node :
 * * `left` : A Label
 * * `right` : An input control, such as a TextArea, ChoiceBox etc.
 *   It may be decorated (e.g. using a HBox, with a label for units when entering numbers).
 * * `below` : Information message describing what this row is for.
 * * `errors` : A Label, or a set of Labels in a VBox explaining why the current value is invalid.
 *   e.g. _"Required"_
 *
 * Each child node is automatically given [styles] (".above", ".left" etc.).
 */
class FormRow : Region() {

    // region ==== Properties ====

    val aboveProperty by optionalNodeProperty(null)
    var above by aboveProperty

    val leftProperty by optionalNodeProperty(null)
    var left by leftProperty

    val rightProperty by optionalNodeProperty(null)
    var right by rightProperty

    val belowProperty by optionalNodeProperty(null)
    var below by belowProperty

    val errorsProperty by optionalNodeProperty(null)
    var errors by errorsProperty

    /**
     * The spacing between `above`, the `left` and `right` row, `below` and `errors`.
     */
    val spacingProperty by stylableFloatProperty(0f)
    var spacing by spacingProperty

    /**
     * This is bound to [FormGrid.columnSpacingProperty] when this FormRow is an item of a [FormGrid].
     */
    val columnSpacingProperty by stylableFloatProperty(0f)
    val columnSpacing by columnSpacingProperty

    // endregion properties

    override val children: MutableObservableList<Node> = mutableListOf<Node>().asMutableObservableList()

    // region ==== init ====

    init {
        styles.add(FORM_ROW)

        children.addChangeListener(childrenListener)

        aboveProperty.addChangeListener { prop, old, new ->
            old?.let {
                children.remove(it)
                old.styles.removeAll(listOf(".above", ".left", ".right", ".below", ".errors"))
            }
            new?.let {
                children.add(it)
                when (prop) {
                    aboveProperty -> new.styles.add(".above")
                    leftProperty -> new.styles.add(".left")
                    rightProperty -> new.styles.add(".right")
                    belowProperty -> new.styles.add(".below")
                    errorsProperty -> new.styles.add(".errors")
                }
            }

        }.also {
            leftProperty.addChangeListener(it)
            rightProperty.addChangeListener(it)
            belowProperty.addChangeListener(it)
            errorsProperty.addChangeListener(it)
        }

    }
    // endregion init

    internal fun aboveBelowErrors() = listOfVisible(above, below, errors)
    internal fun leftAndRight() = listOfVisible(left, right)

    internal fun updateLabelFor() {
        (left as? Label)?.let { label ->
            label.labelFor = right
        }
    }

    override fun nodeMinWidth(): Float {
        val leftAndRight = leftAndRight()
        return max(
            aboveBelowErrors().maxMinWidth(),
            leftAndRight.totalMinWidth() + columnSpacing
        ) + surroundX()
    }

    override fun nodePrefWidth(): Float {
        val leftAndRight = leftAndRight()
        return max(
            aboveBelowErrors().maxPrefWidth(),
            leftAndRight.totalPrefWidth() + columnSpacing
        ) + surroundX()
    }

    override fun nodeMaxWidth() = NO_MAXIMUM

    override fun nodeMinHeight(): Float {
        val row = leftAndRight()
        val aboveAndBelow = aboveBelowErrors()
        return row.maxMinHeight() + aboveAndBelow.totalMinHeight() + surroundY()
    }

    override fun nodePrefHeight(): Float {
        val row = leftAndRight()
        val aboveAndBelow = aboveBelowErrors()
        var totalSpacing = (aboveAndBelow.size - 1) * spacing
        if (left?.visible == true || right?.visible == true) totalSpacing += spacing
        return row.maxPrefHeight() + aboveAndBelow.totalPrefHeight() + totalSpacing + surroundY()
    }

    override fun nodeMaxHeight(): Float {
        val row = leftAndRight()
        val aboveAndBelow = aboveBelowErrors()
        return row.minMaxHeight() + aboveAndBelow.totalMaxHeight() + surroundY()
    }

    internal fun setChildBounds2( child : Node, x : Float, y : Float, width : Float, height : Float ) {
        setChildBounds(child, x, y, width, height)
    }

    override fun layoutChildren() {
        if (parent is FormGrid) return

        log.warn("FormRow should not layout children, FormGrid should do this")
        var y = surroundTop()
        var x = surroundLeft()
        val row = leftAndRight()
        val rowHeight = row.maxPrefHeight()
        above?.let { child ->
            val prefHeight = child.evalPrefHeight()
            if (child.visible) {
                setChildBounds(child, x, y, child.evalPrefWidth(), prefHeight)
                y += prefHeight + spacing
            }
        }
        left?.let { child ->
            if (child.visible) {
                val prefHeight = child.evalPrefHeight()
                val prefWidth = child.evalPrefWidth()
                setChildBounds(child, x, y, child.evalPrefWidth(), prefHeight)
                x += prefWidth + columnSpacing
            }
        }
        right?.let { child ->
            if (child.visible) {
                setChildBounds(child, x, y, child.evalPrefWidth(), child.evalPrefHeight())
            }
        }
        if (row.isNotEmpty()) {
            y += rowHeight + spacing
        }
        below?.let { child ->
            val prefHeight = child.evalPrefHeight()
            setChildBounds(child, surroundLeft(), y, child.evalPrefWidth(), prefHeight)
            y += prefHeight + spacing
        }
        errors?.let { child ->
            setChildBounds(child, surroundLeft(), y, child.evalPrefWidth(), child.evalPrefHeight())
        }
    }
}

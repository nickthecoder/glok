/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dialog

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.scene.dsl.textArea
import uk.co.nickthecoder.glok.scene.dsl.vBox

/**
 * ### Theme DSL
 *
 *     ".alert_pane" { // A VBox.
 *         // This will also have a style of ".information", ".warning", ".error", ".none" or ".confirmation"
 *         // depending on the AlertType.
 *
 *         child( ".heading" ) {
 *             // A Label
 *         }
 *
 *         child( ".message" ) {
 *             // A TextArea
 *         }
 *     }
 */
class AlertDialog(alertType: AlertType = AlertType.NONE) : Dialog() {

    val headingProperty by stringProperty("")
    var heading by headingProperty

    val messageProperty by stringProperty("")
    var message by messageProperty

    lateinit var textArea: TextArea
        private set

    init {
        content = vBox {
            fillWidth = true
            style(".alert_pane")
            style(alertType.style)

            fillWidth = true

            + label(heading) {
                textProperty.bindTo(headingProperty)
                style(".heading")
            }

            + textArea(messageProperty) {
                textArea = this
                growPriority = 1f
                shrinkPriority = 1f
                style(".message")
                readOnly = true
            }
        }
    }
}


fun alertDialog(alertType: AlertType = AlertType.NONE, block: AlertDialog.() -> Unit) = AlertDialog(alertType).apply(block)

/**
 * Builds, and shows an [AlertDialog].
 *
 * @param callback Called when a button is pressed.
 */
fun alert(
    parentStage: Stage,
    title: String,
    heading: String = title,
    message: String,
    alertType: AlertType = AlertType.NONE,
    buttonTypes: List<ButtonType>,
    callback: (ButtonType?) -> Unit
) = alert(parentStage, title, heading, message, alertType, buttonTypes, 0, 0, callback)

fun alert(
    parentStage: Stage,
    title: String,
    heading: String = title,
    message: String,
    alertType: AlertType = AlertType.NONE,
    buttonTypes: List<ButtonType>,
    messageRowCount: Int = 0,
    messageColumnCount: Int = 0,
    callback: (ButtonType?) -> Unit
) {
    val dialog = alertDialog(alertType) {
        this.title = title
        this.heading = heading
        this.message = message
        buttonTypes(* buttonTypes.toTypedArray(), handler = callback)
    }
    dialog.createStage(parentStage, StageType.MODAL) {
        (dialog.textArea.children.firstOrNull() as? ScrollPane)?.let { scrollPane ->

            if (messageColumnCount > 0) {
                dialog.textArea.prefColumnCount = messageColumnCount
            } else {
                scrollPane.hPolicy = ScrollBarPolicy.NEVER
                dialog.textArea.overridePrefWidth = scrollPane.fullWidth() + dialog.textArea.surroundX()
            }
            if (messageRowCount > 0) {
                dialog.textArea.prefRowCount = messageRowCount
            } else {
                scrollPane.hPolicy = ScrollBarPolicy.NEVER
                dialog.textArea.prefRowCount = message.lines().size
            }
        }
        show()
    }
}

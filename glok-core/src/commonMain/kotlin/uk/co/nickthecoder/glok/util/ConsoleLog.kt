package uk.co.nickthecoder.glok.util

/**
 * Logs messages to the console.
 * This is stderr on the jvm, and console.log for JS
 */
expect class ConsoleLog() : Log

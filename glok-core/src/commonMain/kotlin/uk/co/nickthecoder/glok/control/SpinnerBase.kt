package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalFloat
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableSpinnerArrowPositionsProperty
import uk.co.nickthecoder.glok.property.functions.pixelsPerEm
import uk.co.nickthecoder.glok.property.functions.times
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyle
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.*
import kotlin.math.sign

/**
 * Displays a [TextField] where a [value] can by typed in, as well 'up' and 'down' buttons
 * where the value can be adjusted stepwise.
 * This base class doesn't define what `stepwise` means.
 * For example, [IntSpinner] has a [IntSpinner.smallStep], therefore stepwise does not mean increment/decrement.
 * The [value] of a spinner does not have to be [Comparable], but is required for the subclass [ComparableSpinner].
 *
 */
abstract class SpinnerBase<V, P : Property<V>> : Region(), HasReadOnly {

    // region ==== Properties ====

    final override val readOnlyProperty by booleanProperty(false)
    final override var readOnly by readOnlyProperty

    abstract val valueProperty: P
    abstract var value: V

    abstract val smallStepProperty: P
    abstract var smallStep: V

    abstract val largeStepProperty: P
    abstract var largeStep: V

    /**
     * Should [value] change while typing into the text-field?
     *
     * The default is false. i.e. [value] will only be updated when the spinner loses input focus,
     * or when the `Enter` key is pressed.
     * This mimics JavaFX's behaviour.
     *
     * For simple forms, using `autoUpdate = true` may be more user-friendly, because invalid input
     * is highlighted immediately.
     *
     * However, in other scenarios, `autoUpdate = true` may be jarring. For example, suppose we have spinners
     * to set the X and Y positions of an object. It may be jarring to see the object moving wildly
     * as we type.
     *
     * As this is Stylable, you can set this from within your Theme (and therefore all spinners will behave the same).
     * You could also set it based on context. For example, spinners in a "form" use `autoUpdate == true`,
     * but spinners in a Toolbar use `autoUpdate == false`.
     */
    val autoUpdateProperty by stylableBooleanProperty(false)
    var autoUpdate by autoUpdateProperty

    /**
     *
     */
    val spinnerArrowPositionsProperty by stylableSpinnerArrowPositionsProperty(SpinnerArrowPositions.RIGHT_VERTICAL)
    var spinnerArrowPositions by spinnerArrowPositionsProperty

    // endregion  Properties

    // region ==== Child Nodes ====

    protected val upButton = Button("+").apply {
        styles.add(SPIN_UP)
        styles.remove(BUTTON)
        onAction { if (!readOnly) adjust(1) }
    }

    protected val downButton = Button("-").apply {
        styles.add(SPIN_DOWN)
        styles.remove(BUTTON)
        onAction { if (!readOnly) adjust(-1) }
    }

    val editor = TextField("").apply {
        styles.add(EDITOR)
        expectDigits = true
    }
    // endregion

    // region ==== Fields ====
    /**
     * Set during [updateText] and [updateValue] so that a change to one does recursively call the other.
     */
    private var updating = false

    protected abstract val converter: Converter<V, String>

    override val children = listOf<Node>(editor, upButton, downButton).asObservableList()
    // endregion

    // region ==== Commands ====
    private val commands = Commands().apply {
        with(SpinnerActions) {
            SET_VALUE { if (! readOnly) updateValue() }.disable(autoUpdateProperty)
            DECREMENT { if (! readOnly) adjust(- 1, false) }
            INCREMENT { if (! readOnly) adjust(1, false) }
            LARGE_DECREMENT { if (! readOnly) adjust(- 1, true) }
            LARGE_INCREMENT { if (! readOnly) adjust(1, true) }
        }
        attachTo(this@SpinnerBase)
    }

    // region ==== init ====
    init {
        styles.add(SPINNER)
        pseudoStyles.add(spinnerArrowPositions.pseudoStyle)

        readOnlyProperty.addChangeListener { _, _, isReadOnly ->
            if (isReadOnly) {
                pseudoStyles.add(READ_ONLY)
            } else {
                pseudoStyles.remove(READ_ONLY)
            }
        }

        spinnerArrowPositionsProperty.addChangeListener { _, old, new ->
            pseudoStyles.remove(old.pseudoStyle)
            pseudoStyles.add(new.pseudoStyle)
            requestLayout()
        }

        editor.textProperty.addListener { if (autoUpdate) updateValue() }

        // Update the value whenever the spinner loses focus.
        focusedProperty.addChangeListener { _, _, isFocused ->
            if (! isFocused) {
                updateValue()
            }
        }

        onScrolled {
            if (focused && ! readOnly) adjust(- it.deltaY.sign.toInt(), it.isControlDown)
            it.consume()
        }
        onKeyTyped { if (! readOnly) editor.findEventHandler(it.eventType)?.handle(it) }
        onKeyPressed { editor.findEventHandler(it.eventType)?.handle(it) }
    }
    // endregion init

    protected open fun initialise() {
        upButton.parent = this
        downButton.parent = this
        editor.parent = this
        editor.text = converter.forwards(value)
        fakeFocus(editor)
        valueProperty.addListener { updateText() }
        // NOTE ComparableSpinner changes these bindings.
        downButton.disabledProperty.bindTo(readOnlyProperty)
        upButton.disabledProperty.bindTo(readOnlyProperty)
    }


    override fun Number.em(): ObservableOptionalFloat {
        return editor.fontProperty.pixelsPerEm() * this.toFloat()
    }

    abstract fun adjustment(direction: Int, byLargeStep: Boolean): V

    open fun adjust(direction: Int, byLargeStep: Boolean = false) {
        // If the text has been changed, try to update the value, before applying the adjustment.
        updateValue()
        value = adjustment(direction, byLargeStep)
    }

    abstract fun isValid(newValue: V): Boolean

    protected fun updateText() {
        if (updating) return
        try {
            updating = true
            editor.text = converter.forwards(value)
            pseudoStyles.remove(INVALID)
        } finally {
            updating = false
        }
    }

    protected fun updateValue() {
        if (updating) return
        try {
            updating = true
            val newValue = converter.backwards(editor.text)
            val isValid = isValid(newValue)
            if (isValid) {
                value = newValue
            }
            pseudoStyleIf(! isValid, INVALID)

        } catch (e: Exception) {
            pseudoStyle(INVALID)
        } finally {
            updating = false
        }
    }

    // region ==== Layout =====

    override fun nodeMinWidth() = surroundX() + if (spinnerArrowPositions.isVertical()) {
        editor.evalMinWidth() + max(upButton.evalMinWidth(), downButton.evalMinWidth())
    } else {
        children.totalMinWidth()
    }

    override fun nodeMinHeight() = nodePrefHeight()

    override fun nodePrefWidth() = surroundX() + if (spinnerArrowPositions.isVertical()) {
        editor.evalPrefWidth() + max(upButton.evalPrefWidth(), downButton.evalPrefWidth())
    } else {
        children.totalPrefWidth()
    }

    override fun nodePrefHeight() = surroundY() + if (spinnerArrowPositions.isVertical()) {
        max(editor.evalPrefHeight(), upButton.evalPrefHeight() + downButton.evalPrefHeight())
    } else {
        children.maxPrefHeight()
    }

    override fun layoutChildren() {
        val upWidth = upButton.evalPrefWidth()
        val downWidth = downButton.evalPrefWidth()
        val arrowWidth = max(upWidth, downWidth)
        val availableX = width - surroundX()
        val availableY = height - surroundY()
        var x = surroundLeft()
        val y = surroundTop()
        val editorWidth = if (spinnerArrowPositions.isVertical()) {
            availableX - arrowWidth
        } else {
            availableX - upWidth - downWidth
        }
        val editorHeight = editor.evalPrefHeight()
        val editorY = y + (availableY - editorHeight) / 2

        when (spinnerArrowPositions) {
            SpinnerArrowPositions.SPLIT -> {
                setChildBounds(upButton, x, y, upWidth, availableY)
                x += downWidth
                setChildBounds(editor, x, editorY, editorWidth, editorHeight)
                x += editorWidth
                setChildBounds(downButton, x, y, downWidth, availableY)
            }


            SpinnerArrowPositions.LEFT_HORIZONTAL -> {
                setChildBounds(upButton, x, y, upWidth, availableY)
                x += downWidth
                setChildBounds(downButton, x, y, downWidth, availableY)
                x += upWidth
                setChildBounds(editor, x, editorY, editorWidth, editorHeight)
            }

            SpinnerArrowPositions.LEFT_VERTICAL -> {
                setChildBounds(upButton, x, y, arrowWidth, availableY / 2)
                setChildBounds(downButton, x, y + availableY / 2, arrowWidth, availableY / 2)
                x += arrowWidth
                setChildBounds(editor, x, editorY, editorWidth, editorHeight)
            }

            SpinnerArrowPositions.RIGHT_HORIZONTAL -> {
                setChildBounds(editor, x, editorY, editorWidth, editorHeight)
                x += editorWidth
                setChildBounds(upButton, x, y, upWidth, availableY)
                x += downWidth
                setChildBounds(downButton, x, y, arrowWidth, availableY)
            }

            SpinnerArrowPositions.RIGHT_VERTICAL -> {
                setChildBounds(editor, x, y, editorWidth, availableY)
                x += editorWidth
                setChildBounds(upButton, x, y, arrowWidth, availableY / 2)
                setChildBounds(downButton, x, y + availableY / 2, arrowWidth, availableY / 2)
            }
        }
    }
    // endregion

    // region ==== Object methods ====
    override fun toString() = super.toString() + " = $value"
    // endregion

    // region == SpinnerActions ==
    object SpinnerActions : Actions(null) {

        /**
         * The ENTER key updates the spinner's value, based on the text.
         */
        val SET_VALUE = define("set_value", "Set Value", Key.ENTER.noMods())

        /**
         * The Ctrl+HOME key, sets the value to its minimum value.
         */
        val MINIMUM = define("minimum", "Minimum", Key.HOME.control())

        /**
         * The Ctrl+END key, sets the value to its maximum value.
         */
        val MAXIMUM = define("maximum", "Maximum", Key.END.control())

        /**
         * The DOWN key adjusts the value by a small step.
         */
        val DECREMENT = define("decrement", "Decrement", Key.DOWN.noMods())

        /**
         * The UP key adjusts the value by a small step.
         */
        val INCREMENT = define("increment", "Increment", Key.UP.noMods())

        /**
         * The PAGE_DOWN key, and Ctrl+DOWN adjusts the value by a large step.
         */
        val LARGE_DECREMENT = define("largeDecrement", "Large Decrement", Key.PAGE_DOWN.noMods()) {
            add(Key.DOWN.control())
        }

        /**
         * The PAGE_UP key, and Ctrl+UP adjusts the value by a large step.
         */
        val LARGE_INCREMENT = define("largeIncrement", "Large Increment", Key.PAGE_UP.noMods()) {
            add(Key.UP.control())
        }
    }
    // endregion
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.dialog.ColorPickerDialog
import uk.co.nickthecoder.glok.dialog.CustomColorPickerDialog
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.PlainBackground
import uk.co.nickthecoder.glok.scene.RoundedBackground
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.BUTTON
import uk.co.nickthecoder.glok.theme.styles.COLOR_BUTTON

/**
 * A [Button] for selecting a color.
 * Clicking shows a [ColorPickerDialog], which is a [CustomColorPickerDialog], but you can choose a
 * different implementation (such as [PaletteColorPickerDialog]) by changing [dialogFactory].
 *
 */
class ColorButton(text: String, initialColor: Color = Color.WHITE) : ButtonBase(text) {

    val colorProperty by colorProperty(initialColor)
    var color by colorProperty

    val chooseAlphaProperty by booleanProperty(false)
    var chooseAlpha by chooseAlphaProperty

    val radiusProperty by stylableFloatProperty(0f)
    var radius by radiusProperty

    val titleProperty by stringProperty("")
    var title by titleProperty

    /**
     * When set, [color] changes while the user is choosing their color.
     * Otherwise [color] changes only when `Select` is pressed.
     * If `Cancel` is pressed, [color] reverts to its original value.
     *
     * This is not applicable when the [dialogFactory] returns a [PaletteColorPickerDialog].
     */
    val liveProperty by booleanProperty(false)
    var live by liveProperty

    /**
     * The default is to use a [CustomColorPickerDialog], but you may change this to [ColorPickerDialog] like so :
     *
     *     dialogFactory = { PaletteColorPickerDialog() }
     *
     */
    var dialogFactory: () -> ColorPickerDialog = {
        CustomColorPickerDialog()
    }

    private val swatch = Pane()

    init {
        style(BUTTON)
        style(COLOR_BUTTON)

        colorProperty.addListener(requestRedrawListener)
        graphic = swatch
        swatch.backgroundColorProperty.bindTo(this@ColorButton.colorProperty)

        onAction {
            with(dialogFactory()) {
                title = this@ColorButton.title
                color = this@ColorButton.color
                if (live) {
                    this@ColorButton.colorProperty.bidirectionalBind(colorProperty)
                }
                chooseAlpha = this@ColorButton.chooseAlpha
                createStage(scene?.stage!!) {
                    onClosed {
                        this@ColorButton.color = color
                        this@ColorButton.colorProperty.bidirectionalUnbind(colorProperty)
                    }
                    show()
                }
            }
        }
    }
}

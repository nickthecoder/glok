/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.WithChildren
import uk.co.nickthecoder.glok.util.toString

/**
 * Lays out its [children] in a column, with the first child at the top,
 * and the last child at the bottom.
 *
 * For a diagram of how children are laid out, see [HBox] and rotate your screen clockwise ;-)
 *
 * Glok includes an application `TestHBox` that you can play with, if you download the source code.
 *
 */
open class VBox : BoxBase(), WithChildren {

    // ==== Properties ====

    val fillWidthProperty by stylableBooleanProperty(false)
    var fillWidth by fillWidthProperty

    // ==== End of Properties ====

    final override val fill: Boolean get() = fillWidth
    final override val orientation get() = Orientation.VERTICAL

    final override val children = mutableListOf<Node>().asMutableObservableList().apply {
        addChangeListener(childrenListener)
    }

    // ==== End of Fields ====

    init {
        fillWidthProperty.addListener(requestLayoutListener)
    }

    // ==== End of init ====


    // ==== Object overrides ====

    override fun toString() = super.toString() + " fillWidth($fillWidth)"
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.WithItems
import uk.co.nickthecoder.glok.theme.styles.MENU
import uk.co.nickthecoder.glok.theme.styles.SELECTED
import uk.co.nickthecoder.glok.util.currentTimeMillis
import kotlin.time.TimeMark

/**
 * An item in a [MenuBar]. When you click on the Menu a [PopupMenu] appears,
 * containing [items].
 *
 * NOTE, this is very similar to [MenuButton], but [MenuButton]s live outside of [MenuBar]s,
 * and look like a regular [Button], with the addition of an arrow on the right hand side.
 *
 *
 */
class Menu(

    text: String,
    graphic: Node? = null

) : ButtonBase(text, graphic), WithItems {

    // region ==== Properties ====

    /**
     * An event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    val onShowingProperty by optionalActionEventHandlerProperty(null)
    var onShowing by onShowingProperty

    private val mutableShowingProperty by booleanProperty(false)

    /**
     * Set to true, after the menu is clicked, and before the popup menu is displayed.
     * Reset to false when the menu is closed.
     *
     * Applications may listen to this, and adjust [items] as required.
     */
    val showingProperty = mutableShowingProperty.asReadOnly()
    var showing by mutableShowingProperty
        private set

    // endregion

    // region ==== Fields ====
    override val items = mutableListOf<Node>().asMutableObservableList()

    private var popupMenu: PopupMenu? = null

    private var lastClosed = 0.0

    // endregion

    init {
        styles.add(MENU)
        onAction {
            // If we click this menu, then the `MousePressed` event closed the menu, and we don't want to
            // open it again.
            if (lastClosed + GlokSettings.clickTimeThreshold < currentTimeMillis()) {
                openMenu()
            }
        }
    }

    /**
     * Add an event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    fun onShowing(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onShowingProperty, handlerCombination, block)
    }

    override fun mouseEntered(event: MouseEvent) {
        if (openMenu != null && openMenu !== this) {
            // A menu is already open, and we have hovered over another Menu,
            // so close the old PopupMenu, and open this one.
            scene?.stage?.regularStage?.let {
                it.closePopupMenus()
                it.closePopups()
            }
            openMenu()

        } else {
            super.mouseEntered(event)
        }
    }

    internal fun forwardsBackwards(delta: Int) {
        val menuBar = this.firstToRoot { it is MenuBar } as? MenuBar
        if (menuBar != null) {
            popupMenu?.scene?.stage?.close()
            val myIndex = menuBar.items.indexOf(this)
            val selectIndex = myIndex + delta
            if (myIndex >= 0 && selectIndex >= 0 && selectIndex < menuBar.items.size) {
                val item = menuBar.items[selectIndex]
                if (item is Menu) {
                    item.performAction()
                } else {
                    item.requestFocus()
                }
            }
        }
    }

    /**
     * Note. [SubMenu.openSubMenu] is very similar.
     */
    private fun openMenu(focusFirst: Boolean = false) {
        // Is the popupMenu already open?
        if (popupMenu != null) return

        onShowing?.tryCatchHandle(ActionEvent())
        showing = true
        pseudoStyles.add(SELECTED)
        val popupMenu = PopupMenu(this)
        this.popupMenu = popupMenu
        popupMenu.items.addAll(items)
        popupMenu.show(this).apply {
            onClosed {
                popupMenu.deselectAll()
                popupMenu.items.clear()
                this@Menu.popupMenu = null
                showing = false
                pseudoStyles.remove(SELECTED)
                lastClosed = currentTimeMillis()

                // NOTE. If another menu has just been opened, then openMenu !== this@Menu.
                // That's normal. (I used to have an `else` with a warning, which is wrong).
                if (openMenu === this@Menu) {
                    openMenu = null
                }
            }
        }

        if (focusFirst) {
            popupMenu.items.firstOrNull()?.requestFocus()
        }
        openMenu = this
    }

    private companion object {
        /**
         * The parent node of the currently opened [Menu].
         */
        private var openMenu: Menu? = null
    }

}

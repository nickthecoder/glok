/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.event

/**
A [KeyCombination] (AKA keyboard shortcut), is a combination of a [Key],
and set of zero or more `modifier` keys (`Shift`, `Control`, `Alt` and `Super`).

They are commonly used to define `shortcuts` for actions. For example, the standard `Undo` [KeyCombination]
is `Control` + [Key.Z].

Use [KeyEvent.matches] to test if a [KeyEvent] matches a particular [KeyCombination].

There are methods on [Key] and [KeyCombination] to create [KeyCombination]s in
a readable manner. e.g. :

 *    val undoShortcut = Key.Z.control()
 *    val redoShortcut = Key.Y.control()
 *    val redoShortcut2 = Key.Z.control().shift()

In most cases the `modifiers` are boolean, but can also use a third `don't care` state.

For example, we could define `Zoom In` to be just key `MINUS` (with no modifiers).
But if you do not care about the state of the `Shift` modifier, then use [Key.maybeShift].
Now, `Zoom In` would match `MINUS` as well as `Shift` + `MINUS`.

I chose this example, because of `Zoom Out`. There is no `PLUS` key! (`PLUS` is `Shift` + `EQUALS`).

 *    val zoomOut = Key.MINUS.maybeShift() // Or we could use Key.MINUS.noMods()
 *    val zoomIn = Key.EQUAL.maybeShift()

Pressing the `Equals` key will match `zoomIn`, regardless of whether `Shift` is down or up,
but will not match if `Control`, `Super` or `Alt` are pressed.
 */
class KeyCombination private constructor(
    val key: Key,

    /**
     * Which modifier keys must be pressed.
     * The sum of any of : [Event.MOD_SHIFT], [Event.MOD_CONTROL], [Event.MOD_ALT], [Event.MOD_SUPER]
     */
    val requiredMods: Int,

    /**
     * Which modifier keys could be pressed.
     * The sum of any of : [Event.MOD_SHIFT], [Event.MOD_CONTROL], [Event.MOD_ALT], [Event.MOD_SUPER]
     */
    val maybeMods: Int
) {
    constructor(key: Key) : this(key, 0, 0)

    private fun withMod(mod: Int) = KeyCombination(key, requiredMods or mod, maybeMods or mod)
    private fun maybeMod(mod: Int) = KeyCombination(key, requiredMods, maybeMods or mod)

    /**
     * Creates a new [KeyCombination], based on this one, and also requiring `Shift` to be down.
     */
    fun shift() = withMod(Event.MOD_SHIFT)

    /**
     * Creates a new [KeyCombination], based on this one, and also requiring `Control` to be down.
     */
    fun control() = withMod(Event.MOD_CONTROL)

    /**
     * Creates a new [KeyCombination], based on this one, and also requiring `Alt` to be down.
     */
    fun alt() = withMod(Event.MOD_ALT)

    /**
     * Creates a new [KeyCombination], based on this one, and also requiring `Super` to be down.
     * Note, this method is not called `super` to avoid a clash with the `super` keyword.
     */
    fun sup() = withMod(Event.MOD_SUPER)

    /**
     * Creates a new [KeyCombination], based on this one, where `Shift` may be either _down_ or _up_.
     */
    fun maybeShift() = maybeMod(Event.MOD_SHIFT)

    /**
     * Creates a new [KeyCombination], based on this one, where `Control` may be either _down_ or _up_.
     */
    fun maybeControl() = maybeMod(Event.MOD_CONTROL)

    /**
     * Creates a new [KeyCombination], based on this one, where `Alt` may be either _down_ or _up_.
     */
    fun maybeAlt() = maybeMod(Event.MOD_ALT)

    /**
     * Creates a new [KeyCombination], based on this one, where `Super` may be either _down_ or _up_.
     */
    fun maybeSuper() = maybeMod(Event.MOD_SUPER)

    /**
     * Note, this does not include info from [maybeMods].
     */
    val displayText: String
        get() {
            return StringBuilder().apply {
                if (requiredMods and Event.MOD_CONTROL != 0) append("Ctrl+")
                if (requiredMods and Event.MOD_ALT != 0) append("Alt+")
                if (requiredMods and Event.MOD_SHIFT != 0) append("Shift+")
                if (requiredMods and Event.MOD_SUPER != 0) append("Super+")
                append(key.label)
            }.toString()
        }

    override fun toString(): String {
        // Only print the maybeMods if they affect this KeyCombination. In most cases, maybeMods won't be used.
        val maybe = if (maybeMods or requiredMods != requiredMods) " Maybe Mods $maybeMods" else ""
        return "KeyCombination $displayText$maybe"
    }
}

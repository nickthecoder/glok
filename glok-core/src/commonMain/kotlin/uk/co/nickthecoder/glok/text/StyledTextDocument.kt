/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.Change

class StyledTextDocument(initialText: String) : TextDocument(initialText) {

    /**
     * The highlighted ranges.
     * This is publicly mutable, which means you can add/remove highlights without going through the [history] mechanism.
     * (e.g. to add highlights text which matches a `search`).
     *
     * When/if ranges are added as part of the history mechanism, they will be stored separately.
     */
    val ranges = mutableListOf<HighlightRange>().asMutableObservableList()

    @Suppress("unused")
    private val documentListener = addWeakListener { _, change: Change, isUndo: Boolean ->
        if (change is TextChange) updateRanges(change, isUndo)
    }

    /**
     * Returns a [StyledTextChange] which adds a range on redo, and removes it again on undo.
     *
     * Note. ranges can be added / removed without the history mechanism being used.
     * But sometime, it is important to include these changes as part of the history mechanism.
     *
     */
    fun addRangeChange(range: HighlightRange): AddRange = AddRangeImpl(this, range)

    /**
     * Returns a [StyledTextChange] which remove a range on redo, and adds it again on undo.
     */
    fun removeRangeChange(range: HighlightRange): RemoveRange = RemoveRangeImpl(this, range)

    private fun updateRanges(change: TextChange, isUndo: Boolean) {

        fun adjustPositionDelete(pos: TextPosition, from: TextPosition, to: TextPosition): TextPosition =
            if (pos <= from) {
                // Before the deletion. No change
                pos
            } else if (pos <= to) {
                // Inside the deletion.
                from
            } else {
                // Beyond the deletion.
                if (pos.row == to.row) {
                    TextPosition(pos.row - (to.row - from.row), pos.column - (to.column - from.column))
                } else {
                    TextPosition(pos.row - (to.row - from.row), pos.column)
                }
            }

        fun adjustPositionInsert(pos: TextPosition, from: TextPosition, inserted: List<String>, extend: Boolean)
            : TextPosition =
            if (pos < from || (extend && pos == from)) {
                // Before the insertion. No change
                pos
            } else {
                // After the insertion.
                if (pos.row == from.row) {
                    // Same row.
                    if (inserted.size == 1) {
                        // Single line
                        TextPosition(pos.row, pos.column + inserted.last().length)
                    } else {
                        //Multi-line insertion.
                        // Wrong?
                        TextPosition(pos.row + inserted.size - 1, pos.column - from.column + inserted.last().length)
                    }
                } else {
                    // Only the row is changed.
                    TextPosition(pos.row + inserted.size - 1, pos.column)
                }
            }

        fun deleted(from: TextPosition, to: TextPosition) {
            val iterator = ranges.listIterator()
            for (range in iterator) {
                if (range.to >= from) {

                    val newFrom = adjustPositionDelete(range.from, from, to)
                    val newTo = adjustPositionDelete(range.to, from, to)
                    if (newFrom != range.from || newTo != range.to) {
                        if (newFrom == newTo) {
                            iterator.remove()
                        } else {
                            range.from = newFrom
                            range.to = newTo
                        }
                    }
                }
            }
        }

        fun inserted(from: TextPosition, inserted: List<String>) {
            val iterator = ranges.listIterator()
            for (range in iterator) {
                if (range.to >= from) {

                    val newFrom = adjustPositionInsert(range.from, from, inserted, true)
                    val newTo = adjustPositionInsert(range.to, from, inserted, false)
                    if (newFrom != range.from || newTo != range.to) {
                        if (newFrom == newTo) {
                            iterator.remove()
                        } else {
                            range.from = newFrom
                            range.to = newTo
                        }
                    }
                }
            }
        }

        when (change) {
            is DeleteText -> {
                if (isUndo) {
                    inserted(change.from, change.textList)
                } else {
                    deleted(change.from, change.to)
                }
            }

            is InsertText -> {
                if (isUndo) {
                    deleted(change.from, change.to)
                } else {
                    inserted(change.from, change.textList)
                }
            }

            is StyledTextChange -> {}
        }
    }
}

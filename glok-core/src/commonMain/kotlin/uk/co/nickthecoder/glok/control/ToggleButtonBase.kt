/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.DefaultIndirectProperty
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalToggleGroupProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.theme.styles.SELECTED

/**
 * The base class for [ToggleButton] and [RadioButton].
 * The only difference between these two, is what happens when a selected menu item is clicked.
 * A [ToggleMenuItem] becomes unselected, but a [RadioMenuItem] stays selected.
 *
 * There is an analogous class for menus : [ToggleMenuItemBase].
 */
abstract class ToggleButtonBase(

    text: String,
    graphic: Node? = null,
    initialToggleGroup: ToggleGroup? = null
) : SelectButtonBase(text, graphic), Toggle {

    // region ==== Properties ====

    final override val selectedProperty by booleanProperty(false)
    final override var selected by selectedProperty

    /**
     * The [toggleGroup] ensures that more than one button in the same [ToggleGroup] are selected at the
     * same time. For a [ToggleButton] the user can choose to select no items
     * (by clicking the selected [ToggleButton]).
     * But, for a [RadioButton] this is not possible. Clicking the selected [RadioButton] does nothing.
     * It stays selected.
     */
    final override val toggleGroupProperty by optionalToggleGroupProperty(null)
    var toggleGroup by toggleGroupProperty

    private val selectedToggleProperty =
        DefaultIndirectProperty(toggleGroupProperty, SimpleProperty(null)) { it.selectedToggleProperty }
    private var selectedToggle by selectedToggleProperty

    // endregion properties

    override var userData: Any? = null

    // region ==== init ====
    init {
        selectedProperty.addChangeListener { _, _, selected ->
            pseudoStyleIf(selected, SELECTED)
            if (selected) {
                selectedToggle = this@ToggleButtonBase
            } else {
                if (selectedToggle === this@ToggleButtonBase) {
                    selectedToggle = null
                }
            }
        }

        selectedToggleProperty.addChangeListener { _, _, selectedToggle ->
            selected = selectedToggle === this@ToggleButtonBase
        }

        toggleGroup = initialToggleGroup
    }
    // endregion init

}

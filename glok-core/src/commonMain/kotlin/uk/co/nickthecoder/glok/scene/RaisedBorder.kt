package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend

/**
 * A rectangular border, with a 3D effect.
 *
 * The top and left edges are drawn using a lighter color than the base.
 * The bottom and right edges are drawn using a darker color than the base color.
 */
class RaisedBorder(val light: Color, val dark: Color) : Border {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        if (light.alpha != 0f) {
            var top = y
            if (size.top > 0f) {
                backend.fillRect(x, y, x + width, y + size.top, light)
                top += size.top
            }
            var bottom = y + height
            if (size.bottom > 0f) {
                backend.fillRect(x, bottom, x + width, bottom - size.bottom, dark)
                bottom -= size.bottom
            }
            if (size.left > 0f) {
                backend.fillRect(x, top, x + size.left, bottom, light)
            }
            if (size.right > 0f) {
                backend.fillRect(x + width, top, x + width - size.right, bottom, dark)
            }
        }
    }

    override fun toString() = "RaisedBorder( $light , $dark )"

}

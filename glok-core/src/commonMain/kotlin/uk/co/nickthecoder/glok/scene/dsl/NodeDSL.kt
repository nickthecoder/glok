package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalImage
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalNode
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.text.StyledTextDocument
import uk.co.nickthecoder.glok.text.TextDocument
import uk.co.nickthecoder.glok.theme.styles.*
import uk.co.nickthecoder.glok.util.GlokException
import uk.co.nickthecoder.glok.util.log
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

internal fun checkAlreadySet(node: Node?, name: String) {
    if (node != null) throw GlokException("$name is already set. Have you omitted items{ ... } or children{ ... } ?")
}

/**
 * Removes the need to use `.apply { ... }` when building scene graphs.
 */
operator fun <N : Node> N.invoke(block: N.() -> Unit): N {
    this.apply(block)
    return this
}


// ==== Extras ====

/**
Sets [Node.overridePrefWidth]
*/
fun Node.width(width: Number) {
    overridePrefWidth = width.toFloat()
}

/**
Sets [Node.overridePrefHeight]
*/
fun Node.height(height: Number) {
    overridePrefWidth = height.toFloat()
}

/**
Adds a style to [Node.styles]
*/
fun Node.style(name: String) {
    if (name.startsWith(":")) {
        log.warn("Using a style beginning with a colon. Either omit the colon, or use Node.pseudoStyle instead.")
    }
    styles.add(name)
}

/**
Adds or removes the [style] depending on [test]. See [Node.styles]
*/
fun Node.styleIf(test: Boolean, style: String) {
    if (style.startsWith(":")) {
        log.warn("Using a style beginning with a colon. Either omit the colon, or use Node.pseudoStyle instead.")
    }
    if (test) {
        styles.add(style)
    } else {
        styles.remove(style)
    }
}

/**
Adds a style to [Node.pseudoStyle]
*/
fun Node.pseudoStyle(name: String) {
    if (! name.startsWith(":")) {
        log.warn("pseudoStyle $name. Expected a name beginning with a colon.")
    }
    pseudoStyles.add(name)
}

/**
Adds or removes the [pseudoStyle] depending on [test]. See [Node.pseudoStyles]
*/
fun Node.pseudoStyleIf(test: Boolean, pseudoStyle: String) {
    if (! pseudoStyle.startsWith(":")) {
        log.warn("pseudoStyle $pseudoStyle. Expected a name beginning with a colon.")
    }
    if (test) {
        pseudoStyles.add(pseudoStyle)
    } else {
        pseudoStyles.remove(pseudoStyle)
    }
}

/**
When [test] is `true`, [trueStyle] is added, and [falseStyle] is removed from [Node.pseudoStyles].
*/
fun Node.pseudoStyleIf(test: Boolean, trueStyle: String, falseStyle: String) {
    if (! trueStyle.startsWith(":")) {
        log.warn("pseudoStyle $trueStyle. Expected a name beginning with a colon.")
    }
    if (! falseStyle.startsWith(":")) {
        log.warn("pseudoStyle $falseStyle. Expected a name beginning with a colon.")
    }
    if (test) {
        pseudoStyles.add(trueStyle)
        pseudoStyles.remove(falseStyle)
    } else {
        pseudoStyles.remove(trueStyle)
        pseudoStyles.add(falseStyle)
    }
}

fun TextField.readOnly() {
    readOnly = true
}

/**
Sets [TextField.expectDigits] = true and [TextField.prefColumnCount] to [digits].
*/
fun TextField.prefDigits(digits: Int) {
    prefColumnCount = digits
    expectDigits = true
}

/**
Sets [BoxBase.spacing].
*/
fun BoxBase.spacing(size: Number) {
    spacing = size.toFloat()
}

/**
Sets [Region.padding].
*/
fun Region.padding(size: Number) {
    padding = Edges(size.toFloat())
}

/**
Sets [Region.padding].
*/
fun Region.padding(topBottom: Number, leftRight: Number) {
    padding = Edges(topBottom.toFloat(), leftRight.toFloat())
}

/**
Sets [Region.padding].
*/
fun Region.padding(top: Number, right: Number, bottom: Number, left: Number) {
    padding = Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat())
}

/**
Sets [Region.borderSize].
*/
fun Region.borderSize(size: Number) {
    borderSize = Edges(size.toFloat())
}

/**
Sets [Region.borderSize].
*/
fun Region.borderSize(topBottom: Number, leftRight: Number) {
    borderSize = Edges(topBottom.toFloat(), leftRight.toFloat())
}

/**
Sets [Region.borderSize].
*/
fun Region.borderSize(top: Number, right: Number, bottom: Number, left: Number) {
    borderSize = Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat())
}

/**
Sets [Region.background to [NoBackground]
*/
fun Region.noBackground() {
    background = NoBackground
}

/**
Sets [Region.background] to [PlainBackground]( [color] )
*/
fun Region.plainBackground(color: Color) {
    background = PlainBackground
    backgroundColor = color
}

/**
Sets [Region.background] to [PlainBackground]( Color[color] )
*/
fun Region.plainBackground(color: String) {
    background = PlainBackground
    backgroundColor = Color[color]
}

fun Region.roundedBackground(color: Color, radius: Number) {
    background = RoundedBackground(radius.toFloat())
    backgroundColor = color
}

fun Region.roundedBackground(color: String, radius: Number) {
    background = RoundedBackground(radius.toFloat())
    backgroundColor = Color[color]
}

fun Region.noBorder() {
    border = NoBorder
}

fun Region.plainBorder(color: Color) {
    border = PlainBorder
    borderColor = color
}

fun Region.plainBorder(color: String) {
    border = PlainBorder
    borderColor = Color[color]
}

fun Region.roundedBorder(color: Color, radius: Number) {
    border = RoundedBorder(radius.toFloat())
    borderColor = color
}

fun Region.roundedBorder(color: String, radius: Number) {
    border = RoundedBorder(radius.toFloat())
    borderColor = Color[color]
}

fun <N : Node> N.dsl(block: (N.() -> Unit)?): N {
    if (block != null) {
        block()
    }
    return this
}

internal fun <V : Any> V.optionalApply(block: (V.() -> Unit)?): V {
    if (block != null) {
        block()
    }
    return this
}

// ==== Separator ====
fun separator(block: (Separator.() -> Unit)? = null) = Separator().optionalApply(block)

// ==== Box ====
fun box(orientation: Orientation, block: (Box.() -> Unit)? = null) = Box(orientation).optionalApply(block)

// ==== HBox ====
fun hBox(block: (HBox.() -> Unit)? = null) = HBox().optionalApply(block)

// ==== VBox ====
fun vBox(block: (VBox.() -> Unit)? = null) = VBox().optionalApply(block)

// ==== FlowPane ====
fun flowPane(block: (FlowPane.() -> Unit)? = null) = FlowPane().optionalApply(block)

// ==== ButtonBar ====
fun buttonBar(block: (ButtonBar.() -> Unit)? = null) = ButtonBar().optionalApply(block)

// ==== ToolBar ====
fun toolBar(block: (ToolBar.() -> Unit)? = null) = ToolBar().optionalApply(block)
fun toolBar(side: Side, block: (ToolBar.() -> Unit)? = null) = ToolBar(side).optionalApply(block)

// ==== MenuBar ====
fun menuBar(block: (MenuBar.() -> Unit)? = null) = MenuBar().optionalApply(block)

// ==== Titled Pane ====
fun titledPane(text: String, block: (TitledPane.() -> Unit)? = null) = TitledPane(text).optionalApply(block)

// ==== SplitPane ====
fun splitPane(orientation: Orientation, block: (SplitPane.() -> Unit)? = null) =
    SplitPane(orientation).optionalApply(block)

fun splitPane(block: (SplitPane.() -> Unit)? = null) = SplitPane().optionalApply(block)

// ==== StackPane ====
fun stackPane(block: (StackPane.() -> Unit)? = null) = StackPane().optionalApply(block)

// ==== ColorPicker ====

fun customColorPicker(initialColor: Color = Color.WHITE, block: (CustomColorPicker.() -> Unit)? = null) =
    CustomColorPicker(initialColor).optionalApply(block)

fun customColorPicker(
    initialColor: Color = Color.WHITE,
    editAlpha: Boolean,
    block: (CustomColorPicker.() -> Unit)? = null
) =
    CustomColorPicker(initialColor, editAlpha).optionalApply(block)

// ==== PaletteColorPicker ====
fun paletteColorPicker(initialColor: Color = Color.WHITE, block: (PaletteColorPicker.() -> Unit)? = null) =
    PaletteColorPicker(initialColor).optionalApply(block)

// ==== ThreeRow ====
fun threeRow(block: ThreeRow.() -> Unit) = ThreeRow().optionalApply(block)

// ==== Single Container ====
fun container(contentProperty: ObservableOptionalNode, block: (SingleContainer.() -> Unit)? = null) =
    SingleContainer(contentProperty).optionalApply(block)

fun container(block: (SingleContainer.() -> Unit)? = null) = SingleContainer().optionalApply(block)

// ==== ListView ====
fun <T> listView(list: MutableObservableList<T>, block: (ListView<T>.() -> Unit)? = null) =
    ListView(list).optionalApply(block)

fun <T> listView(block: (ListView<T>.() -> Unit)? = null) = ListView<T>().optionalApply(block)

// ==== TreeView ====

fun <T> treeView(block: (TreeView<T>.() -> Unit)? = null) = TreeView<T>().optionalApply(block)

// ==== MixedTreeView ====
fun <I : MixedTreeItem<I>> mixedTreeView(block: (MixedTreeView<I>.() -> Unit)? = null) =
    MixedTreeView<I>().optionalApply(block)

// ==== ChoicesButton ====
fun <V> choiceBox(block: (ChoiceBox<V>.() -> Unit)? = null) = ChoiceBox<V>().optionalApply(block)
fun <V> choiceBox(list: MutableObservableList<V>, block: (ChoiceBox<V>.() -> Unit)? = null) =
    ChoiceBox<V>(list).optionalApply(block)

// ==== Tab Bar ====
fun tabBar(block: (TabBar.() -> Unit)? = null) = TabBar().optionalApply(block)

fun documentTabBar(block: (TabBar.() -> Unit)? = null) = TabBar().apply {
    tabClosingPolicy = TabClosingPolicy.ALL_TABS
    styles.remove(TAB_BAR)
    styles.add(DOCUMENTS_TAB_BAR)
}.optionalApply(block)

// ==== TabPane ====
fun tabPane(side: Side = Side.TOP, block: (TabPane.() -> Unit)? = null) = TabPane(side).optionalApply(block)

/**
 * Creates a [TabPane], but using the style "document_tab_pane" instead of the standard "tab_pane".
 * This style is suitable for tabs containing documents
 */
fun documentTabPane(block: (TabPane.() -> Unit)? = null) = TabPane().apply {
    tabClosingPolicy = TabClosingPolicy.ALL_TABS
    tabBar.styles.remove(TAB_BAR)
    tabBar.styles.add(DOCUMENTS_TAB_BAR)
}.optionalApply(block)

fun tab(text: String, block: (Tab.() -> Unit)? = null) = Tab(text).optionalApply(block)
fun tab(text: String, graphic: Node?, block: (Tab.() -> Unit)? = null) = Tab(text, graphic).optionalApply(block)

// ==== Border Pane ====
fun borderPane(block: (BorderPane.() -> Unit)? = null) = BorderPane().optionalApply(block)

// ==== Text ====
fun text(text: String, block: (Text .() -> Unit)? = null) = Text(text).optionalApply(block)

// ==== Labelled ====
fun Labelled.labelPadding(size: Number) {
    labelPadding = Edges(size.toFloat())
}

fun Labelled.labelPadding(topBottom: Number, leftRight: Number) {
    labelPadding = Edges(topBottom.toFloat(), leftRight.toFloat())
}

fun Labelled.labelPadding(top: Number, right: Number, bottom: Number, left: Number) {
    labelPadding = Edges(top.toFloat(), right.toFloat(), bottom.toFloat(), left.toFloat())
}

// ==== Label ====
fun label(text: String, block: (Label.() -> Unit)? = null) = Label(text).optionalApply(block)
fun label(text: String, graphic: Node? = null, block: (Label .() -> Unit)? = null) =
    Label(text, graphic).optionalApply(block)

fun information(text: String, block: (Label.() -> Unit)? = null) = label(text) {
    style(INFORMATION)
}.optionalApply(block)

fun heading(text: String, block: (Label.() -> Unit)? = null) = label(text) {
    style(HEADING)
}.optionalApply(block)

fun Node.units(units: String) = hBox {
    growPriority = 1f
    alignment = Alignment.CENTER_LEFT
    spacing(10)

    + this@units
    + label(units) {
        style(UNITS)
    }
}

fun errorMessage(text: String, block: (Label.() -> Unit)? = null) = label(text) {
    style(ERROR)
}.optionalApply(block)

// ==== TextField ====
fun textField(block: (TextField.() -> Unit)? = null) = TextField("").optionalApply(block)
fun textField(text: String, block: (TextField.() -> Unit)? = null) = TextField(text).optionalApply(block)

// ==== TextArea ====
fun textArea(block: (TextArea.() -> Unit)? = null) = TextArea("").optionalApply(block)
fun textArea(text: String, block: (TextArea.() -> Unit)? = null) = TextArea(text).optionalApply(block)
fun textArea(textDocument: TextDocument, block: (TextArea.() -> Unit)? = null) =
    TextArea(textDocument).optionalApply(block)

fun textArea(stringProperty: StringProperty, block: (TextArea.() -> Unit)? = null) =
    TextArea("").optionalApply(block).also {
        it.textProperty.bindTo(stringProperty)
    }

// ==== StyledTextArea ====
fun styledTextArea(block: (StyledTextArea.() -> Unit)? = null) = StyledTextArea("").optionalApply(block)
fun styledTextArea(text: String, block: (StyledTextArea.() -> Unit)? = null) = StyledTextArea(text).optionalApply(block)
fun styledTextArea(textDocument: StyledTextDocument, block: (StyledTextArea.() -> Unit)? = null) =
    StyledTextArea(textDocument).optionalApply(block)

fun styledTextArea(stringProperty: StringProperty, block: (StyledTextArea.() -> Unit)? = null) =
    StyledTextArea("").optionalApply(block).also {
        it.textProperty.bindTo(stringProperty)
    }

// ==== Slider ====
fun slider(initialValue: Double, block: (DoubleSlider.() -> Unit)? = null) =
    doubleSlider(block).also { it.value = initialValue }

fun slider(initialValue: Float, block: (FloatSlider.() -> Unit)? = null) =
    floatSlider(block).also { it.value = initialValue }

fun doubleSlider(block: (DoubleSlider.() -> Unit)? = null) = DoubleSlider().optionalApply(block)
fun floatSlider(block: (FloatSlider.() -> Unit)? = null) = FloatSlider().optionalApply(block)
fun intSlider(block: (IntSlider.() -> Unit)? = null) = IntSlider().optionalApply(block)

fun colorSlider(block: (ColorSlider.() -> Unit)? = null) = ColorSlider().optionalApply(block)

fun slider2d(block: (Slider2d.() -> Unit)? = null) = Slider2d().optionalApply(block)

// ==== ProgressBar ====

fun progressBar(block: (ProgressBar.() -> Unit)? = null) = ProgressBar().optionalApply(block)

// ==== Spinners ====
fun spinner(initialValue: Int, block: (IntSpinner.() -> Unit)? = null) = intSpinner(initialValue, block)
fun spinner(initialValue: Float, block: (FloatSpinner.() -> Unit)? = null) = floatSpinner(initialValue, block)
fun spinner(initialValue: Double, block: (DoubleSpinner.() -> Unit)? = null) = doubleSpinner(initialValue, block)

fun intSpinner(initialValue: Int, block: (IntSpinner.() -> Unit)? = null) =
    IntSpinner(initialValue).optionalApply(block)

fun intSpinner(block: (IntSpinner.() -> Unit)? = null) = IntSpinner().optionalApply(block)

fun floatSpinner(initialValue: Float, block: (FloatSpinner.() -> Unit)? = null) =
    FloatSpinner(initialValue).optionalApply(block)

fun floatSpinner(block: (FloatSpinner.() -> Unit)? = null) = FloatSpinner().optionalApply(block)

fun doubleSpinner(initialValue: Double, block: (DoubleSpinner.() -> Unit)? = null) =
    DoubleSpinner(initialValue).optionalApply(block)

fun doubleSpinner(block: (DoubleSpinner.() -> Unit)? = null) = DoubleSpinner().optionalApply(block)

// ==== Button ====
fun button(text: String, block: (Button.() -> Unit)? = null) = Button(text).optionalApply(block)
fun button(text: String, graphic: Node?, block: (Button.() -> Unit)? = null) =
    Button(text, graphic).optionalApply(block)

// ==== MenuItem ====
fun menuItem(text: String, graphic: Node?, block: (MenuItem.() -> Unit)? = null) =
    MenuItem(text, graphic).optionalApply(block)

fun menuItem(text: String, block: MenuItem.() -> Unit) = MenuItem(text).optionalApply(block)

// ==== CustomMenuItem ====
fun customMenuItem(block: (CustomMenuItem.() -> Unit)? = null) = CustomMenuItem().optionalApply(block)

// ==== CheckMenuItem ====
fun toggleMenuItem(text: String, block: (ToggleMenuItem.() -> Unit)? = null) = ToggleMenuItem(text).optionalApply(block)

// ==== SubMenu ====
fun subMenu(text: String, block: (SubMenu.() -> Unit)? = null) = SubMenu(text).optionalApply(block)
fun subMenu(text: String, graphic: Node?, block: (SubMenu.() -> Unit)? = null) =
    SubMenu(text, graphic).optionalApply(block)

// ==== MenuButton ====
fun menuButton(text: String, block: (MenuButton.() -> Unit)? = null) = MenuButton(text).optionalApply(block)
fun menuButton(text: String, graphic: Node?, block: (MenuButton.() -> Unit)? = null) =
    MenuButton(text, graphic).optionalApply(block)

// ==== SplitMenuButton ====
fun splitMenuButton(text: String, block: (SplitMenuButton.() -> Unit)? = null) =
    SplitMenuButton(text).optionalApply(block)

fun splitMenuButton(text: String, graphic: Node?, block: (SplitMenuButton.() -> Unit)? = null) =
    SplitMenuButton(text, graphic).optionalApply(block)

// ==== Menu ====
fun menu(text: String, block: (Menu.() -> Unit)? = null) = Menu(text).optionalApply(block)
fun menu(text: String, graphic: Node? = null, block: (Menu.() -> Unit)? = null) =
    Menu(text, graphic).optionalApply(block)

// ==== PopupMenu ====
fun popupMenu(block: (PopupMenu.() -> Unit)? = null) = PopupMenu().optionalApply(block)

// ==== ColorButton ====
fun colorButton(block: (ColorButton.() -> Unit)? = null) = ColorButton("").optionalApply(block)
fun colorButton(text: String, block: (ColorButton.() -> Unit)? = null) = ColorButton(text).optionalApply(block)

// ==== CheckBox ====
fun checkBox(block: (CheckBox.() -> Unit)? = null) = CheckBox("").optionalApply(block)
fun checkBox(text: String, block: (CheckBox.() -> Unit)? = null) = CheckBox(text).optionalApply(block)

fun switch(block: (CheckBox.() -> Unit)? = null) = switch("", block)
fun switch(text: String, block: (CheckBox.() -> Unit)? = null) = CheckBox(text).apply {
    styles.remove(CHECK_BOX)
    styles.add(SWITCH)
}.optionalApply(block)


// ==== Toggle Group ====
fun toggleGroup(block: (ToggleGroupDSL.() -> Unit)? = null) = ToggleGroupDSL().optionalApply(block)

// ==== ToggleChoices ====
fun <V, P : Property<V?>> toggleChoices(property: P, block: (PropertyToggleChoicesDSL<V, P>.() -> Unit)? = null) =
    PropertyToggleChoicesDSL(property).optionalApply(block)

fun <V, P : Property<V?>> propertyToggleButton(
    property: P,
    value: V,
    text: String,
    block: (PropertyToggleButton<V, P>.() -> Unit)? = null
) = PropertyToggleButton(property, value, text).optionalApply(block)

// ==== RadioChoices ====
fun <V, P : Property<V>> radioChoices(property: P, block: (PropertyRadioChoicesDSL<V, P>.() -> Unit)? = null) =
    PropertyRadioChoicesDSL(property).optionalApply(block)

fun <V, P : Property<V>> propertyRadioButton(
    property: P,
    value: V,
    text: String,
    block: (PropertyRadioButton<V, P>.() -> Unit)? = null
) = PropertyRadioButton(property, value, text).optionalApply(block)

// ==== SingleContainer ====

fun singleContainer(block: (SingleContainer.() -> Unit)? = null) = SingleContainer().optionalApply(block)
fun singleContainer(content: Node, block: (SingleContainer.() -> Unit)? = null) =
    SingleContainer(content).optionalApply(block)

fun singleContainer(contentProperty: ObservableOptionalNode, block: (SingleContainer.() -> Unit)? = null) =
    SingleContainer(contentProperty).optionalApply(block)

// ==== Toggle Button ====
fun toggleButton(
    text: String, graphic: Node? = null, toggleGroup: ToggleGroup? = null,
    block: (ToggleButton.() -> Unit)? = null
) = ToggleButton(text, graphic, toggleGroup).optionalApply(block)

// ==== Radio Button ====

fun radioButton(
    toggleGroup: ToggleGroup, text: String,
    block: (RadioButton.() -> Unit)? = null
) = RadioButton(toggleGroup, text, null).optionalApply(block)

fun radioButton(
    toggleGroup: ToggleGroup, text: String, graphic: Node,
    block: (RadioButton.() -> Unit)? = null
) = RadioButton(toggleGroup, text, graphic).optionalApply(block)

/**
 * The same as [radioButton], but gives it a different [Node.style].
 * Tantalum renders this button in a similar fashion to a [ToggleButton]
 * (but it still behaves like a radio-button, not a toggle-button).
 */
fun radioButton2(
    toggleGroup: ToggleGroup, text: String,
    block: (RadioButton.() -> Unit)? = null
) = RadioButton(toggleGroup, text, null).apply {
    styles.remove(RADIO_BUTTON)
    styles.add(RADIO_BUTTON2)
}.optionalApply(block)

fun radioButton2(
    toggleGroup: ToggleGroup, text: String, graphic: Node,
    block: (RadioButton.() -> Unit)? = null
) = RadioButton(toggleGroup, text, graphic).apply {
    styles.remove(RADIO_BUTTON)
    styles.add(RADIO_BUTTON2)
}.optionalApply(block)

// ==== Image View ====

fun imageView(image: Image?, block: (ImageView.() -> Unit)? = null) = ImageView(image).optionalApply(block)

fun imageView(observableImage: ObservableOptionalImage, block: (ImageView.() -> Unit)? = null) =
    ImageView(observableImage).optionalApply(block)

// ==== Pane ====
fun pane(block: (Pane.() -> Unit)? = null) = Pane().optionalApply(block)

// ==== Spacer ====
/**
 * The default priority is small, but larger than 0, so that other controls will fill the space
 * in preference to the spacer.
 * The spacer will only take significant space if there are no other control with a non-zero growPriority,
 * or they reach their max size.
 */
fun spacer(priority: Number = 0.001f) = Pane().apply {
    growPriority = priority.toFloat()
    shrinkPriority = priority.toFloat()
}

// ==== ScrollBar ====
fun scrollBar(block: (ScrollBar.() -> Unit)? = null) = ScrollBar().optionalApply(block)
fun scrollBar(orientation: Orientation, block: ScrollBar.() -> Unit) = ScrollBar(orientation).optionalApply(block)

// ==== ScrollPane ====
fun scrollPane(child: Node, block: (ScrollPane.() -> Unit)? = null) = ScrollPane(child).optionalApply(block)
fun scrollPane(block: (ScrollPane.() -> Unit)? = null) = ScrollPane().optionalApply(block)

// ==== ScrollPaneWithButtons ====
fun scrollPaneWithButtons(child: Node, block: (ScrollPaneWithButtons.() -> Unit)? = null) =
    ScrollPaneWithButtons(child).optionalApply(block)

fun scrollPaneWithButtons(block: (ScrollPaneWithButtons.() -> Unit)? = null) =
    ScrollPaneWithButtons().optionalApply(block)

// ==== Ruler ====
fun ruler(side: Side = Side.TOP, block: (Ruler.() -> Unit)? = null) = Ruler(side).optionalApply(block)

// ==== FormGrid ====
fun form(block: (FormGrid.() -> Unit)? = null) = FormGrid().optionalApply(block)
fun formGrid(block: (FormGrid.() -> Unit)? = null) = FormGrid().optionalApply(block)

fun row(block: (FormRow.() -> Unit)? = null): FormRow {
    return FormRow().apply {
        updateLabelFor()
        optionalApply(block)
    }
}

fun row(label: String, block: (FormRow.() -> Unit)? = null): FormRow {
    return FormRow().apply {
        left = Label(label)
        updateLabelFor()
    }.optionalApply(block)
}

fun row(label: String, text: String, block: (FormRow.() -> Unit)? = null): FormRow {
    return FormRow().apply {
        left = Label(label)
        right = Label(text)
        updateLabelFor()
    }.optionalApply(block)
}

fun rowHeading(text: String): FormRow {
    return row {
        above = label(text) {
            style(ROW_HEADING)
        }
    }
}

fun rowSeparator(): FormRow {
    return row {
        above = vBox {
            growPriority = 1f
            fillWidth
            + Separator().apply {
                padding(10f, 0f)
            }
        }
    }
}


// ==== withExpandHeight ====

/**
 * Adds a thin bar below this node.
 * Dragging the bar changes the [Node.overridePrefHeight] of the node, therefore the node grows/shrinks.
 *
 * Because [VBox] respects the minimum height, it is easy to set a minimum height.
 *
 * This works well with a `TextArea` / `StyledTextArea`.
 */
fun Node.withExpandBar(orientation: Orientation): Box {
    val adjustableNode = this
    return box(orientation) {
        fill = true
        + adjustableNode
        + pane {
            style(EXPAND_BAR)
            onMousePressed {
                pseudoStyles.add(ARMED)
                it.capture()
            }
            onMouseReleased {
                pseudoStyles.remove(ARMED)
            }
            onMouseEntered {
                pseudoStyles.add(HOVER)
            }
            onMouseExited {
                pseudoStyles.remove(HOVER)
            }
            onMouseDragged { event ->
                if (orientation == Orientation.VERTICAL) {
                    adjustableNode.overridePrefHeight = max(
                        adjustableNode.evalMinHeight(),
                        min(adjustableNode.evalMaxHeight(), event.sceneY - adjustableNode.sceneY)
                    )
                } else {
                    adjustableNode.overridePrefWidth = max(
                        adjustableNode.evalMinWidth(),
                        min(adjustableNode.evalMaxWidth(), event.sceneX - adjustableNode.sceneX)
                    )
                }
            }
        }
    }
}

fun Node.withExpandHeight() = withExpandBar(Orientation.VERTICAL)
fun Node.withExpandWidth() = withExpandBar(Orientation.HORIZONTAL)

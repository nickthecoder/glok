package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.property.boilerplate.orientationProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedFloatProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.theme.styles.ARMED
import uk.co.nickthecoder.glok.theme.styles.HOVER
import uk.co.nickthecoder.glok.theme.styles.SCROLL_BAR
import uk.co.nickthecoder.glok.theme.styles.THUMB
import uk.co.nickthecoder.glok.util.clamp
import kotlin.math.sign

/**
 * Part of a [ScrollPane], but can also be used as a stand-alone component.
 */
class ScrollBar(orientation: Orientation = Orientation.HORIZONTAL) : Region() {

    // ==== Properties ====

    val minProperty by floatProperty(0f)
    var min by minProperty

    val maxProperty by floatProperty(0f)
    var max by maxProperty

    val valueProperty by validatedFloatProperty(0f) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    var value by valueProperty

    val visibleAmountProperty by floatProperty(0.5f)
    var visibleAmount by visibleAmountProperty

    /**
     * How much to change [value] by from each scroll event`
     */
    val unitIncrementProperty by floatProperty(1.0f)
    var unitIncrement by unitIncrementProperty

    /**
     * How much to change [value] by, when the scroll bar is clicked (outside the thumb).
     * Note, there is no `unitIncrement`
     */
    val blockIncrementProperty by floatProperty(1.0f)
    var blockIncrement by blockIncrementProperty

    val orientationProperty by orientationProperty(orientation)
    var orientation by orientationProperty

    // ==== End of properties ====

    private val thumb = Thumb()

    override val children = listOf<Node>(thumb).asObservableList()

    // ==== End of fields ====

    init {
        styles.add(SCROLL_BAR)
        pseudoStyles.add(orientation.pseudoStyle)

        orientationProperty.addListener(requestLayoutListener)

        valueProperty.addListener { reposition() }.also {
            for (prop in listOf(minProperty, maxProperty, visibleAmountProperty)) {
                prop.addListener(it)
            }
        }

        orientationProperty.addChangeListener { _, old, new ->
            pseudoStyles.remove(old.pseudoStyle)
            pseudoStyles.add(new.pseudoStyle)
            requestLayout()
        }

        onMouseClicked { event ->
            val v = valueForEvent(event)
            if (v < value) {
                value = (value - blockIncrement).clamp(min, max)
            } else {
                value = (value + blockIncrement).clamp(min, max)
            }
        }
        onScrolled { event ->
            val delta = if (isHorizontal()) {
                if (event.deltaX == 0f) event.deltaY else event.deltaX
            } else {
                event.deltaY
            }
            scrolledBy(delta)
        }
        onMouseEntered {
            pseudoStyles.add(HOVER)
        }
        onMouseExited {
            pseudoStyles.remove(HOVER)
        }
    }

    // ==== Methods ====

    fun scrollBy(delta: Float) {
        if (min < max) {
            value = (value + delta).clamp(min, max)
        }
    }

    internal fun scrolledBy(delta: Float) {
        scrollBy(unitIncrement * sign(delta))
    }

    fun isHorizontal() = orientation == Orientation.HORIZONTAL
    fun isVertical() = orientation == Orientation.VERTICAL

    private fun ratio(value: Float = this.value): Float {
        val range = (max - min)
        return if (range <= 0) 0f else (value - min) / range
    }


    private fun valueForEvent(event: MouseEvent): Float {
        val ratio = if (isHorizontal()) {
            val local = event.sceneX - sceneX - surroundLeft()
            val rangePixels = width - surroundX() - thumb.width
            local / rangePixels
        } else {
            val local = event.sceneY - sceneY - surroundTop()
            val rangePixels = height - surroundX() - thumb.height
            local / rangePixels
        }
        return (max - min) * ratio + min
    }


    // ==== Layout ====

    override fun nodeMinWidth() = surroundX() + thumb.evalPrefWidth()
    override fun nodeMinHeight() = surroundY() + thumb.evalPrefHeight()
    override fun nodePrefWidth() = surroundX() + thumb.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + thumb.evalPrefHeight()

    private fun reposition() {
        layoutChildren()
        requestRedraw()
    }

    override fun layoutChildren() {
        val range = max - min
        val isHorizontal = orientation == Orientation.HORIZONTAL

        // `if (range <= 0f)` is to protect against divide by zero and other problems
        // caused by invalid values for min or max.

        // value as a ratio between min and max.
        val ratio = ratio() // if (range <= 0f) 0f else (value - min) / range
        // A value in the range 0..1
        val position = if (range <= 0f) 0f else (range - visibleAmount) / range * ratio
        // Examples :
        // If max=10, min=0 visibleAmount = 2
        // ..........
        // ##             0.0 When value = 0 (min)
        //         ##     0.8 When value = 10 (max)
        //     ##         0.4 When value = 5 (middle)
        // ..........

        // Now convert to pixels.

        val availablePixels: Float = if (isHorizontal) {
            width - surroundX()
        } else {
            height - surroundY()
        }
        val positionPixels = position * availablePixels
        val sizePixels = if (range <= 0f) availablePixels else (visibleAmount / range) * availablePixels
        val minSize = if (isHorizontal) thumb.evalMinWidth() else thumb.evalMinHeight()
        // How much bigger than sizePixels do we need to make this.
        // Negative, if sizePixels is fine.
        val growBy = minSize - sizePixels

        if (orientation == Orientation.HORIZONTAL) {
            if (growBy > 0f) {
                setChildBounds(thumb, surroundLeft() + positionPixels - position * (growBy), surroundTop(), minSize, height - surroundY())
            } else {
                setChildBounds(thumb, surroundLeft() + positionPixels, surroundTop(), sizePixels, height - surroundY())
            }
        } else {
            if (growBy > 0f) {
                setChildBounds(thumb, surroundLeft(), surroundTop() + positionPixels - position * (growBy), width - surroundX(), minSize)
            } else {
                setChildBounds(thumb, surroundLeft(), surroundTop() + positionPixels, width - surroundX(), sizePixels)
            }
        }
    }

    // ==== Object methods ====

    override fun toString() = super.toString() + " $orientation $min .. $max ($value)"

    // ==== Thumb ====
    inner class Thumb : Region() {

        private var pressedOffset = 0f

        init {
            styles.add(THUMB)
            parent = this@ScrollBar

            onMousePressed { event ->
                pseudoStyles.add(ARMED)
                pressedOffset = valueForEvent(event) - value
                event.capture()
            }
            onMouseReleased {
                pseudoStyles.remove(ARMED)
                parent?.pseudoStyles?.remove(HOVER)
            }
            onMouseDragged { event ->
                value = (valueForEvent(event) - pressedOffset).clamp(min, max)
            }
            onMouseClicked { it.consume() }
        }

        override fun nodePrefWidth() = surroundX()
        override fun nodePrefHeight() = surroundY()

    }

}

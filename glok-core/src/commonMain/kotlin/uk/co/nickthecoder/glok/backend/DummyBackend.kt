package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.util.Matrix

/**
 * A [Backend] used for unit-testing, which renders nothing.
 * By having a backend which doesn't actually render anything, unit tests which are not testing the actual
 * display output do not need to create a window, or use OpenGL.
 */
class DummyBackend : Backend {

    // ===== View =====

    override fun drawFrame(window: Window, block: () -> Unit) {
        block()
    }
    override fun drawView(width: Float, height: Float, scale: Float, block: ()->Unit) {
        block()
    }

    override fun processEvents(timeoutSeconds: Double) {}
    override fun setMousePointer(window: Window, cursor: MousePointer) {}

    // ===== Drawing =====

    override fun clear(color: Color) {}

    override fun beginClipping(left: Float, top: Float, width: Float, height: Float) = true
    override fun endClipping() {}

    override fun noClipping(block: () -> Unit) {
        block()
    }

    override fun transform(transformation: Matrix) {}
    override fun clearTransform() {}
    override fun transform(transformation: Matrix, block: () -> Unit) {
        block()
    }


    override fun fillRect(rect: GlokRect, color: Color) {}
    override fun fillRect(left: Float, top: Float, right: Float, bottom: Float, color: Color) {}
    override fun fillQuarterCircle(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, radius: Float) {}
    override fun strokeQuarterCircle(
        x1: Float, y1: Float, x2: Float, y2: Float,
        color: Color, radius: Float, innerRadius: Float
    ) {
    }

    override fun strokeInsideRect(rect: GlokRect, thickness: Float, color: Color) {}

    override fun strokeInsideRect(
        left: Float, top: Float, right: Float, bottom: Float,
        thickness: Float, color: Color
    ) {
    }

    override fun drawTexture(
        texture: Texture,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float, destHeight: Float,
        modelMatrix: Matrix?
    ) {
    }

    override fun drawTintedTexture(
        texture: Texture,
        tint: Color,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float, destHeight: Float,
        modelMatrix: Matrix?
    ) {
    }

    override fun gradient(triangles: FloatArray, colors: Array<Color>) {}

    override fun hsvGradient(triangles: FloatArray, colors: Array<FloatArray>) {}


    override fun batch(texture: Texture, tint: Color?, modelMatrix: Matrix?, block: TextureBatch.() -> Unit) {
        object : TextureBatch {
            override fun draw(srcX: Float, srcY: Float, width: Float, height: Float, destX: Float, destY: Float, destWidth: Float, destHeight: Float) {
            }
        }.block()
    }

    // ===== Factory methods =====

    lateinit var latestWindow: DummyWindow
    override fun createWindow(width: Int, height: Int): Window {
        latestWindow = DummyWindow()
        return latestWindow
    }

    override fun createTexture(name: String, width: Int, height: Int, pixels: IntArray): Texture =
        DummyTexture(name, width, height)

    override fun resources(path : String): Resources = DummyResources()
    override fun fileResources(path : String): Resources = DummyResources()

    // ===== Other =====

    override fun monitorSize() = Pair(1280, 1024)

}

/**
 * A dummy implementation of [Texture], used for unit-testing.
 * See [DummyBackend].
 */
internal class DummyTexture(override val name: String, override val width: Int, override val height: Int) : Texture {
    override fun bind() {}
}

/**
 * A dummy implementation of [Window], used for unit-testing.
 * See [DummyBackend].
 */
class DummyWindow : Window {

    lateinit var stage: RegularStage

    override var title: String = ""
    override var shouldClose: Boolean = false

    override var windowListener: WindowListener? = null

    override var resizable: Boolean = false
    override var maximized: Boolean = false
    override var minimized: Boolean = false

    override fun show() {}
    override fun hide() {}
    override fun closeNow() {}

    override fun size() = Pair(1280, 1024)
    override fun resize(width: Int, height: Int) {}
    override fun moveTo(x: Int, y: Int) {}
    override fun position(): Pair<Int, Int> = Pair(0, 0)

    fun mockMouseMove(x: Int, y: Int) {
        windowListener?.mouseMove(x, y)
    }

    fun mockPress(buttonIndex: Int = 0, mods: Int = 0) {
        windowListener?.mouseButton(buttonIndex, true, mods)
    }

    fun mockRelease(buttonIndex: Int = 0, mods: Int = 0) {
        windowListener?.mouseButton(buttonIndex, false, mods)
    }

    override fun setIcon(images: List<Image>, tint: Color?) {
    }
}

/**
 * A dummy implementation of [Resources], used for unit-testing.
 * See [DummyBackend].
 */
internal class DummyResources : Resources {
    override fun loadTexture(name: String) = DummyTexture(name, 1, 1)
}

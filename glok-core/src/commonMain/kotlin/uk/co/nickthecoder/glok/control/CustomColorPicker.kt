/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.dialog.CustomColorPickerDialog
import uk.co.nickthecoder.glok.property.BidirectionalBind
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.FIXED_WIDTH
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.noDecimalPlaces

/**
 * A control that lets the user choose a color using sliders and/or spinners
 * for each RGB channel and/or HSV values
 *
 * NOTE, this is _not_ a dialog, it is a reusable component.
 * See [CustomColorPickerDialog].
 */
class CustomColorPicker(
    val initialColor: Color = Color.WHITE,
    editAlpha: Boolean = false
) : Region() {

    // region ==== Properties ====
    val colorProperty by colorProperty(initialColor)
    var color by colorProperty

    val opaqueColorProperty by colorProperty(Color.TRANSPARENT)
    var opaqueColor by opaqueColorProperty

    val editAlphaProperty by booleanProperty(editAlpha)
    var editAlpha by editAlphaProperty

    /**
     * The currently selected channel. Either `R`, `G`, `B` for picking colors via Red/Green/Blue and
     * `H`, `S`, `V` for picking colors via Hue/Saturation/Value.
     */
    val basisProperty by stringProperty(initialBasis)
    var basis by basisProperty

    /**
     * The size of the width and height of the square area, and the height of the color slider.
     */
    val colorFieldSizeProperty by stylableFloatProperty(256f)
    var colorFieldSize by colorFieldSizeProperty

    /**
     * The size of the width and height of the `current` and `new` color swatches.
     */
    val swatchSizeProperty by stylableFloatProperty(50f)
    var swatchSize by swatchSizeProperty

    val showLabelsProperty by stylableBooleanProperty(true)
    var showLabels by showLabelsProperty

    val showCurrentColorProperty by stylableBooleanProperty(true)
    var showCurrentColor by showCurrentColorProperty

    // endregion

    // region ==== Fields ====

    private val hBox = HBox()

    /**
     * A slider for either the red, green or blue channel, depending on which of the choice buttons are pressed.
     * Will be hidden when `hue`, `saturation` or `brightness` is the [initialBasis].
     */
    private lateinit var channelSlider: ColorSlider
    private lateinit var alphaSlider: ColorSlider
    private lateinit var colorField: Slider2d

    private lateinit var xLabel: Label
    private lateinit var yLabel: Label
    private lateinit var zLabel: Label

    private val checkeredBackground = RepeatedBackground(Tantalum.icons["checkered"], false)

    override val children = listOf(hBox).asObservableList()

    private val contrastColorProperty = colorProperty.contrasting(Color.BLACK, Color.WHITE)

    // xxx255Converter convert the current color to a float suitable for the FloatSpinners,
    // where a channel value can be typed.
    private val red255Converter = object : Converter<Float, Color> {
        override fun forwards(value: Float): Color {
            return Color((value / 255f).clamp(), color.green, color.blue, color.alpha)
        }

        override fun backwards(value: Color) = value.red * 255f
    }
    private val green255Converter = object : Converter<Float, Color> {
        override fun forwards(value: Float): Color {
            return Color(color.red, (value / 255f).clamp(), color.blue, color.alpha)
        }

        override fun backwards(value: Color): Float = value.green * 255f
    }
    private val blue255Converter = object : Converter<Float, Color> {
        override fun forwards(value: Float): Color {
            return Color(color.red, color.green, (value / 255f).clamp(), color.alpha)
        }

        override fun backwards(value: Color): Float = value.blue * 255f
    }

    private val hue359Converter = object : Converter<Float, Color> {
        override fun forwards(value: Float): Color {
            val hsv = color.toHSV()
            val alpha = color.alpha
            return Color.hsv(value / 359f, hsv[1], hsv[2], alpha)
        }

        override fun backwards(value: Color): Float = value.toHSV()[0] * 359f
    }
    private val saturation100Converter = object : Converter<Float, Color> {
        override fun forwards(value: Float): Color {
            val hsv = color.toHSV()
            val alpha = color.alpha
            return Color.hsv(hsv[0], value / 100f, hsv[2], alpha)
        }

        override fun backwards(value: Color): Float = value.toHSV()[1] * 100f
    }
    private val value100Converter = object : Converter<Float, Color> {
        override fun forwards(value: Float): Color {
            val hsv = color.toHSV()
            val alpha = color.alpha
            return Color.hsv(hsv[0], hsv[1], value / 100f, alpha)
        }

        override fun backwards(value: Color): Float = value.toHSV()[2] * 100f
    }

    // Converts the `ratio` of the channelSlider to an actual color, using the current color's
    // other channels in the forward direction.

    private val redConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            return Color(value.toFloat().clamp(), color.green, color.blue, color.alpha)
        }

        override fun backwards(value: Color) = value.red.toDouble()
    }
    private val greenConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            return Color(color.red, value.toFloat().clamp(), color.blue, color.alpha)
        }

        override fun backwards(value: Color): Double = value.green.toDouble()
    }
    private val blueConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            return Color(color.red, color.green, value.toFloat().clamp(), color.alpha)
        }

        override fun backwards(value: Color): Double = value.blue.toDouble()
    }
    private val alphaConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            return Color(color.red, color.green, color.blue, value.toFloat().clamp())
        }

        override fun backwards(value: Color): Double = value.alpha.toDouble()
    }

    private val hueConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            val hsv = color.toHSV()
            val alpha = color.alpha
            // The clamp is needed, because the slider goes from 0..1 INCLUSIVE,
            // But a hue of 1 is the same as 0, so "circle" would flip to the left.
            return Color.hsv(value.toFloat().clamp(0f, 0.999f), hsv[1], hsv[2], alpha)
        }

        override fun backwards(value: Color): Double = value.toHSV()[0].toDouble()
    }
    private val saturationConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            val hsv = color.toHSV()
            val alpha = color.alpha
            return Color.hsv(hsv[0], value.toFloat(), hsv[2], alpha)
        }

        override fun backwards(value: Color): Double = value.toHSV()[1].toDouble()
    }
    private val valueConverter = object : Converter<Double, Color> {
        override fun forwards(value: Double): Color {
            val hsv = color.toHSV()
            val alpha = color.alpha
            return Color.hsv(hsv[0], hsv[1], value.toFloat(), alpha)
        }

        override fun backwards(value: Color): Double = value.toHSV()[2].toDouble()
    }

    private val colorStringConverter = object : Converter<String, Color> {
        override fun backwards(value: Color): String {
            return if (this@CustomColorPicker.editAlpha) value.toHashRGBA() else value.toHashRGB()
        }

        override fun forwards(value: String): Color {
            return Color.find(value) ?: color
        }
    }

    // endregion

    // region ==== Listeners ====

    // Bidirectional bindings for the colorField/colorSlider's ratios.
    private var xRatioBind: BidirectionalBind? = null
    private var yRatioBind: BidirectionalBind? = null
    private var zRatioBind: BidirectionalBind? = null
    private val alphaBind: BidirectionalBind
    private val aRatioBind: BidirectionalBind

    // endregion

    init {
        section = true
        hBox.build()
        claimChildren()
        changeBasis()
        aRatioBind = alphaSlider.ratioProperty.bidirectionalBind(colorProperty, alphaConverter)
        alphaBind = opaqueColorProperty.bidirectionalBind(colorProperty, object : Converter<Color, Color> {
            override fun forwards(value: Color) = Color(value.red, value.green, value.blue, alphaSlider.ratio.toFloat())
            override fun backwards(value: Color) = Color(value.red, value.green, value.blue, 1f)
        })

        basisProperty.addListener { changeBasis() }
        // Remember changes to the basis, for the next time a CustomColorPicker is opened.
        basisProperty.addChangeListener { _, _, basis -> initialBasis = basis }
    }

    /**
     * Called when one of the H, S, V, R, G, B choice buttons are pressed.
     */
    private fun changeBasis() {
        channelSlider.fromColorProperty.unbind()
        channelSlider.toColorProperty.unbind()
        xRatioBind?.unbind()
        yRatioBind?.unbind()
        zRatioBind?.unbind()
        when (basis) {
            "R" -> {
                xRatioBind = colorField.ratioXProperty.bidirectionalBind(colorProperty, blueConverter)
                yRatioBind = colorField.ratioYProperty.bidirectionalBind(colorProperty, greenConverter)
                zRatioBind = channelSlider.ratioProperty.bidirectionalBind(colorProperty, redConverter)
                xLabel.text = "B"
                yLabel.text = "G"
                zLabel.text = "R"

                channelSlider.restoreGradientBackground()
                channelSlider.fromColorProperty.bindTo(opaqueColorProperty.withRed(0f))
                channelSlider.toColorProperty.bindTo(opaqueColorProperty.withRed(1f))

                colorField.track.background = ColorFieldBackground(
                    Color(0f, 1f, 0f).withRed(opaqueColorProperty.red()),
                    Color(0f, 1f, 1f).withRed(opaqueColorProperty.red()),
                    opaqueColorProperty.redOnly(),
                    Color(0f, 0f, 1f).withRed(opaqueColorProperty.red()),
                )
            }

            "G" -> {
                xRatioBind = colorField.ratioXProperty.bidirectionalBind(colorProperty, blueConverter)
                yRatioBind = colorField.ratioYProperty.bidirectionalBind(colorProperty, redConverter)
                zRatioBind = channelSlider.ratioProperty.bidirectionalBind(colorProperty, greenConverter)
                xLabel.text = "B"
                yLabel.text = "R"
                zLabel.text = "G"

                channelSlider.restoreGradientBackground()
                channelSlider.fromColorProperty.bindTo(opaqueColorProperty.withGreen(0f))
                channelSlider.toColorProperty.bindTo(opaqueColorProperty.withGreen(1f))

                colorField.track.background = ColorFieldBackground(
                    Color(1f, 0f, 0f).withGreen(opaqueColorProperty.green()),
                    Color(1f, 0f, 1f).withGreen(opaqueColorProperty.green()),
                    opaqueColorProperty.greenOnly(),
                    Color(0f, 0f, 1f).withGreen(opaqueColorProperty.green())
                )
            }

            "B" -> {
                xRatioBind = colorField.ratioXProperty.bidirectionalBind(colorProperty, redConverter)
                yRatioBind = colorField.ratioYProperty.bidirectionalBind(colorProperty, greenConverter)
                zRatioBind = channelSlider.ratioProperty.bidirectionalBind(colorProperty, blueConverter)

                xLabel.text = "R"
                yLabel.text = "G"
                zLabel.text = "B"

                channelSlider.restoreGradientBackground()
                channelSlider.fromColorProperty.bindTo(opaqueColorProperty.withBlue(0f))
                channelSlider.toColorProperty.bindTo(opaqueColorProperty.withBlue(1f))

                colorField.track.background = ColorFieldBackground(
                    Color(0f, 1f, 0f).withBlue(opaqueColorProperty.blue()),
                    Color(1f, 1f, 0f).withBlue(opaqueColorProperty.blue()),
                    opaqueColorProperty.blueOnly(),
                    Color(1f, 0f, 0f).withBlue(opaqueColorProperty.blue())
                )
            }

            "H" -> {
                xRatioBind = colorField.ratioXProperty.bidirectionalBind(colorProperty, saturationConverter)
                yRatioBind = colorField.ratioYProperty.bidirectionalBind(colorProperty, valueConverter)
                zRatioBind = channelSlider.ratioProperty.bidirectionalBind(colorProperty, hueConverter)

                xLabel.text = "S"
                yLabel.text = "V"
                zLabel.text = "H"

                channelSlider.track.background = HSVGradientBackground(Orientation.VERTICAL,
                    0f, 1f, 1f, 1f,
                    1f, 1f, 1f, 1f)
                colorField.track.background = HueColorFieldBackground(opaqueColorProperty.hue())
            }

            "S" -> {
                xRatioBind = colorField.ratioXProperty.bidirectionalBind(colorProperty, hueConverter)
                yRatioBind = colorField.ratioYProperty.bidirectionalBind(colorProperty, valueConverter)
                zRatioBind = channelSlider.ratioProperty.bidirectionalBind(colorProperty, saturationConverter)

                xLabel.text = "H"
                yLabel.text = "V"
                zLabel.text = "S"

                channelSlider.track.background = HSVGradientBackground.forSaturation(Orientation.VERTICAL, opaqueColorProperty, 0f, 1f)
                colorField.track.background = SaturationColorFieldBackground(opaqueColorProperty.saturation())
            }

            "V" -> {
                xRatioBind = colorField.ratioXProperty.bidirectionalBind(colorProperty, hueConverter)
                yRatioBind = colorField.ratioYProperty.bidirectionalBind(colorProperty, saturationConverter)
                zRatioBind = channelSlider.ratioProperty.bidirectionalBind(colorProperty, valueConverter)

                xLabel.text = "H"
                yLabel.text = "S"
                zLabel.text = "V"

                channelSlider.track.background = HSVGradientBackground.forValue(Orientation.VERTICAL, opaqueColorProperty, 0f, 1f)
                colorField.track.background = BrightnessColorFieldBackground(opaqueColorProperty.brightness())
            }
        }
    }

    // region ==== Layout ====

    private fun HBox.build() {

        padding(0 ,10, 0, 0)

        // Color Field (Slider2d)
        + vBox {
            alignment = Alignment.TOP_CENTER
            + hBox {
                alignment = Alignment.CENTER_LEFT
                + label("") {
                    yLabel = this
                    styles.add(FIXED_WIDTH)
                    visibleProperty.bindTo(showLabelsProperty)
                }
                + slider2d {
                    colorField = this
                    track.overridePrefHeightProperty.bindCastTo(colorFieldSizeProperty)
                    track.overridePrefWidthProperty.bindCastTo(colorFieldSizeProperty)
                    thumb.border = RoundedBorder(6f)
                    thumb.borderColorProperty.bindTo(contrastColorProperty)
                }
            }
            + label("") {
                xLabel = this
                styles.add(FIXED_WIDTH)
                visibleProperty.bindTo(showLabelsProperty)
            }
        }

        // Z Slider
        + vBox {
            alignment = Alignment.TOP_CENTER
            + colorSlider {
                channelSlider = this
                orientation = Orientation.VERTICAL
                thumb.border = RoundedBorder(6f)
                track.overridePrefHeightProperty.bindCastTo(colorFieldSizeProperty)
                thumb.borderColorProperty.bindTo(contrastColorProperty)
            }
            + label("") {
                zLabel = this
                styles.add(FIXED_WIDTH)
                visibleProperty.bindTo(showLabelsProperty)
            }
        }

        // Alpha Slider
        + vBox {
            alignment = Alignment.TOP_CENTER
            visibleProperty.bindTo(editAlphaProperty)
            + colorSlider {
                alphaSlider = this
                fromColor = Color.TRANSPARENT
                toColorProperty.bindTo(opaqueColorProperty)
                orientation = Orientation.VERTICAL
                track.overridePrefHeightProperty.bindCastTo(colorFieldSizeProperty)
                track.background = CompoundBackground(checkeredBackground, track.background)
                thumb.border = PlainBorder
                thumb.borderColorProperty.bindTo(contrastColorProperty)
            }
            + label("A") {
                styles.add(FIXED_WIDTH)
                visibleProperty.bindTo(showLabelsProperty)
                alignment = Alignment.CENTER_CENTER
            }
        }

        + vBox {

            overrideMinHeightProperty.bindCastTo(colorFieldSizeProperty + 6f)
            + vBox {
                padding(6f, 0, 0, 8f)

                // New and Original swatches.
                + vBox {
                    // New swatch
                    + hBox {
                        alignment = Alignment.CENTER_LEFT

                        + pane {
                            style(".swatch")
                            overridePrefWidthProperty.bindCastTo(swatchSizeProperty)
                            overridePrefHeightProperty.bindCastTo(swatchSizeProperty)
                            overrideMinWidthProperty.bindCastTo(swatchSizeProperty)
                            overrideMinHeightProperty.bindCastTo(swatchSizeProperty)
                            background = PlainBackground
                            backgroundColorProperty.bindTo(this@CustomColorPicker.opaqueColorProperty)
                        }

                        + pane {
                            visibleProperty.bindTo(editAlphaProperty)
                                style(".swatch")
                                overridePrefWidthProperty.bindCastTo(swatchSizeProperty)
                                overridePrefHeightProperty.bindCastTo(swatchSizeProperty)
                                overrideMinWidthProperty.bindCastTo(swatchSizeProperty)
                                overrideMinHeightProperty.bindCastTo(swatchSizeProperty)
                            background = CompoundBackground(checkeredBackground, PlainBackground)
                            backgroundColorProperty.bindTo(this@CustomColorPicker.colorProperty)
                        }


                        + label("New") {
                            padding(0, 0, 0, 12f)
                            visibleProperty.bindTo(showCurrentColorProperty)
                        }
                    }

                    // Original color swatch (opaque and alpha swatches)
                    + hBox {
                        alignment = Alignment.CENTER_LEFT
                        visibleProperty.bindTo(showCurrentColorProperty)

                        + pane {
                            styles.add(".swatch")
                            overridePrefWidthProperty.bindCastTo(swatchSizeProperty)
                            overridePrefHeightProperty.bindCastTo(swatchSizeProperty)
                            background = PlainBackground
                            backgroundColor = initialColor.opaque()
                        }

                        + pane {
                            visibleProperty.bindTo(editAlphaProperty)
                            styles.add(".swatch")
                            overridePrefWidthProperty.bindCastTo(swatchSizeProperty)
                            overridePrefHeightProperty.bindCastTo(swatchSizeProperty)
                            background = CompoundBackground(checkeredBackground, PlainBackground)
                            backgroundColor = initialColor
                        }

                        + label("Current") {
                            padding(0, 0, 0, 12f)
                        }
                    }
                }
            }

            // basis
            + hBox {
                alignment = Alignment.CENTER_LEFT
                padding(12, 0, 12, 8f)
                spacing(12)

                // HSB
                + vBox {
                    spacing(2)
                    + hBox {
                        spacing(4)
                        + propertyRadioButton(basisProperty, "H", "H") {
                            styles.add(FIXED_WIDTH)
                        }
                        + createChannelSpinner("H")
                    }

                    // SATURATION
                    + hBox {
                        spacing(4)
                        + propertyRadioButton(basisProperty, "S", "S") {
                            styles.add(FIXED_WIDTH)
                        }
                        + createChannelSpinner("S")
                    }

                    + hBox {
                        spacing(4)
                        + propertyRadioButton(basisProperty, "V", "V") {
                            styles.add(FIXED_WIDTH)
                        }
                        + createChannelSpinner("V")
                    }
                }

                // RGB
                + vBox {
                    spacing(2)
                    // RED Basis
                    + hBox {
                        spacing(4)
                        + propertyRadioButton(basisProperty, "R", "R") {
                            styles.add(FIXED_WIDTH)
                        }
                        + createChannelSpinner("R")
                    }

                    // GREEN Basis
                    + hBox {
                        spacing(4)
                        + propertyRadioButton(basisProperty, "G", "G") {
                            styles.add(FIXED_WIDTH)
                        }
                        + createChannelSpinner("G")
                    }

                    // BLUE Basis
                    + hBox {
                        spacing(4)
                        + propertyRadioButton(basisProperty, "B", "B") {
                            styles.add(FIXED_WIDTH)
                        }
                        + createChannelSpinner("B")
                    }
                }

            }

            + spacer()

            // #rrggbb (aa) TextField
            + hBox {
                padding(0, 0, 2, 8f)
                + textField {
                    styles.add(FIXED_WIDTH)
                    prefColumnCountProperty.bindTo(IntUnaryFunction(editAlphaProperty) { if (it) 9 else 7 })
                    textProperty.bidirectionalBind(colorProperty, colorStringConverter)
                }
            }
        }
    }

    private fun createChannelSpinner(channel: String) = FloatSpinner().apply {
        max = when (channel) {
            "H" -> 359f
            "S" -> 100f
            "V" -> 100f
            else -> 255f
        }
        format = noDecimalPlaces
        editor.prefColumnCount = 3
        if (channel == "H") cycle = true
        when (channel) {
            "R" -> red255Converter
            "G" -> green255Converter
            "B" -> blue255Converter
            "H" -> hue359Converter
            "S" -> saturation100Converter
            "V" -> value100Converter
            else -> null
        }?.let { converter ->
            valueProperty.bidirectionalBind(colorProperty, converter)
        }
    }

    override fun nodePrefWidth() = surroundX() + hBox.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + hBox.evalPrefHeight()
    override fun nodeMinWidth() = surroundX() + hBox.evalMinWidth()
    override fun nodeMinHeight() = surroundY() + hBox.evalMinHeight()
    override fun nodeMaxWidth() = hBox.evalMaxWidth()
    override fun nodeMaxHeight() = hBox.evalMaxHeight()

    override fun layoutChildren() {
        setChildBounds(hBox, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    // endregion

    companion object {

        val initialBasisProperty by stringProperty("H")

        /**
         * When a [CustomColorPicker] is created, which of the four radio buttons is selected.
         * Possible values : `H`, `S`, `V`, `R`, `G`, `B`.
         *
         * [CustomColorPicker] updates this each time one of the buttons is pressed.
         * So the next time, it will use the same basis as before.
         * However, this property is NOT saved, and therefore closing the application,
         * and restarting, it will reset back to using `H` (hue).
         */
        var initialBasis by initialBasisProperty
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.Matrix

abstract class Transformation(val child: Node) : Node() {

    /**
     * The matrix, which is applied to the `view` matrix while drawing the child node.
     */
    var matrix: Matrix = Matrix()

    /**
     * The inverse of [matrix]. Used to convert mouse positions into local positions.
     */
    var inverse: Matrix = Matrix()

    override val children: ObservableList<Node> = listOf(child).asObservableList()

    init {
        claimChildren()
    }

    override fun findDeepestNodeAt(sceneX: Float, sceneY: Float): Node {
        val x = inverse.m00 * sceneX + inverse.m10 * sceneY + inverse.m20
        val y = inverse.m01 * sceneX + inverse.m11 * sceneY + inverse.m21
        return super.findDeepestNodeAt(x, y)
    }

    override fun draw() {}

    override fun drawChildren() {
        backend.transform(matrix) {
            super.drawChildren()
        }
    }
}

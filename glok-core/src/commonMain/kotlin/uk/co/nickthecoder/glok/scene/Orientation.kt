package uk.co.nickthecoder.glok.scene

enum class Orientation(override val pseudoStyle: String) : WithPseudoStyle {

    VERTICAL(":vertical"),
    HORIZONTAL(":horizontal");

    fun orthogonal() = if (this == VERTICAL) HORIZONTAL else VERTICAL
}

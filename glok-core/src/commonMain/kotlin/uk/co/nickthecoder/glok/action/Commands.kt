/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.action

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.EventHandler
import uk.co.nickthecoder.glok.event.EventType
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.PropertyRadioChoicesDSL
import uk.co.nickthecoder.glok.scene.dsl.PropertyToggleChoicesDSL
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON2
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.glok.util.GlokException
import uk.co.nickthecoder.glok.util.log

/**
 * [Commands] define the _behaviours_ of [Actions], i.e. the code that executed when a button, menu-item etc. is pressed.
 * The [Commands] can then be used to build the buttons, menu items etc.
 *
 * For example, we define [Actions] like so :
 *
 *     object MyApplicationActions : Actions( myIcons ) {
 *         val FILE_OPEN = define( "file_open", "Open", Key.O.control() )
 *         val FILE_SAVE = define( "file_save", "Save", Key.S.control() )
 *     }
 *
 * Each [Action] has a `key` (such as `file_open`), which is never seen by the end-user.
 * Next is the text that will appear in the menu-item, button etc.
 * We can optionally add a keyboard shortcut and tooltip text.
 *
 * Notice that [Action]s do NOT define the behavior. This is where [Commands] come in.
 *
 *     val commands = Commands().apply {
 *         // Attach to a node (often the root node) for keyboard-shortcuts.
 *         // When input focus is elsewhere, the keyword-shortcuts won't apply.
 *         attachTo( aNode )
 *
 *         MyApplicationActions.FILE_OPEN { println( "Not implemented!" ) }
 *         MyApplicationActions.FILE_SAVE { println( "Not implemented!" ) }
 *     }
 *
 * Simple behaviours can be written in-line, but longer behaviours are usually function calls.
 *
 * Now, we can start building our GUI :
 *
 *     fun myToolBar( commands : Commands ) = toolBar {
 *         commands.build {
 *            + button( MyApplicationActions.FILE_OPEN )
 *            + button( MyApplicationActions.FILE_SAVE )
 *         }
 *     }
 *
 *     fun myMenuBar( commands : Commands ) = menuBar {
 *         commands.build {
 *             + menuItem( MyApplicationActions.FILE_OPEN )
 *             + menuItem( MyApplicationActions.FILE_SAVE )
 *         }
 *     }
 *
 * Note, it is tiresome to keep including `MyApplicationActions.`, so you could wrap them in a `with` block instead.
 *
 * It is typical for the GUI components (i.e. myToolBar() and myMenuBar()) to be in a separate source files,
 * which is why I declared them as functions, and passed `commands` to them.
 *
 * See `DemoCommand.kt` in glok-demos subproject for real example code.
 *
 * Missing from the example code :
 *
 * * Setting the size of icons. See [NodeBuilder.iconSizeProperty]
 * * Enabling/disabling buttons/menu items. See [Command.disable]
 *
 * Some applications have a single instance of [Commands], but more complex application
 * may have many (one for each part of the application).
 *
 * For shortcuts to work, you must [attachTo] a node.
 * The shortcuts will only work when the input focus is one of that node's descendants (or itself).
 * Therefore, [Scene.root] is often a good choice.
 */
open class Commands {

    private val commands = mutableMapOf<Action, Command>()

    private val keyPressHandler = object : EventHandler<KeyEvent> {
        override fun handle(event: KeyEvent) {
            val actionEvent = ActionEvent()
            val matching = commands.values.firstOrNull { it.action.matches(event) }
            if (matching != null) {
                if (matching.disableProperty?.value != true) {
                    if (matching is ToggleCommand) {
                        matching.selectedProperty.value = ! matching.selectedProperty.value
                        if (matching.consume) {
                            event.consume()
                        }
                    } else if (matching is PropertyRadioCommand<*, *>) {
                        matching.select()
                    }
                    matching.lambda?.invoke(actionEvent)
                    if (matching.consume || actionEvent.isConsumed()) {
                        event.consume()
                    }
                }
            }
        }
    }

    /**
     * Adds an `onKeyPressed` event handler to [node], which checks the [KeyEvent]
     * against each action's [Action.keyCombination] and [Action.additionalKeyCombinations].
     *
     * If there is a matching [Command], and [Command.disableProperty].value != true :
     *
     * 1. If it is a [ToggleCommand], then [ToggleCommand.selectedProperty] is toggled.
     * 2. [Command.lambda] is invoked.
     *
     * If there are multiple matches, only the first matching [Command] is considered.
     */
    fun attachTo(node: Node) {
        node.addEventHandler(EventType.KEY_PRESSED, keyPressHandler)
    }

    /**
     * Removes the key handler added by [attachTo].
     *
     * This method is rarely (if ever) used, because the key handler is usually required for the lifetime of the node.
     *
     */
    fun detachFrom(node: Node) {
        node.removeEventHandler(EventType.KEY_PRESSED, keyPressHandler)
    }

    // region ==== Define Actions ====

    private fun checkExists(action: Action) {
        if (commands[action] != null) {
            log.warn("Command for Action ${action.name} has already been defined")
        }
    }

    /**
     * Syntactic sugar, which adds an [action].
     */
    operator fun Action.invoke(consume: Boolean, lambda: (ActionEvent) -> Unit) = action(this, lambda).apply {
        this.consume = consume
    }

    operator fun Action.invoke(lambda: (ActionEvent) -> Unit) = action(this, lambda)

    fun removeAction(action: Action) {
        commands.remove(action)
    }

    fun action(action: Action, lambda: (ActionEvent) -> Unit): Command {
        checkExists(action)
        val command = Command(action, lambda)
        commands[action] = command
        return command
    }

    fun toggle(action: Action, selected: BooleanProperty): ToggleCommand {
        checkExists(action)
        val command = ToggleCommand(action, selected, null)
        commands[action] = command
        return command
    }

    fun toggle(action: Action, selected: BooleanProperty, lambda: (ActionEvent) -> Unit): ToggleCommand {
        checkExists(action)
        val command = ToggleCommand(action, selected, lambda)
        commands[action] = command
        return command
    }

    fun <V, P : Property<V?>> toggleChoices(property: P, block: PropertyToggleChoicesDSL<V, P>.() -> Unit) {
        PropertyToggleChoicesDSL(property).block()
    }

    fun <V, P : Property<V?>> PropertyToggleChoicesDSL<V, P>.choice(action: Action, value: V): PropertyToggleCommand<V, P> {
        checkExists(action)
        val command = PropertyToggleCommand(property, action, value, null)
        commands[action] = command
        return command
    }

    fun <V, P : Property<V>> radioChoices(property: P, block: PropertyRadioChoicesDSL<V, P>.() -> Unit) {
        PropertyRadioChoicesDSL(property).block()
    }

    fun <V, P : Property<V>> PropertyRadioChoicesDSL<V, P>.choice(action: Action, value: V): PropertyRadioCommand<V, P> {
        checkExists(action)
        val command = PropertyRadioCommand(property, action, value, null)
        commands[action] = command
        return command
    }

    // endregion Define Actions

    /**
     * Build nodes based on these Commands.
     *
     * Any icons are dynamically resized, based on the [iconSizeProperty].
     * i.e. if you change the property's value, all icons will change size.
     *
     * If [iconSizeProperty] is omitted, the default is taken from [Tantalum.iconSizeProperty].
     *
     * Inside the [block], the receiver (`this`) is a [NodeBuilder], which has methods to create
     * various types of [Node].
     */
    fun build(iconSizeProperty: ObservableInt = Tantalum.iconSizeProperty, block: NodeBuilder.() -> Unit) {
        NodeBuilder(iconSizeProperty).block()
    }

    /**
     * Build nodes based on these Commands.
     *
     * Any icons will be a fixed size. The alternate [build] method, which takes a
     */
    fun build(iconSize: Int, block: NodeBuilder.() -> Unit) = build(SimpleIntProperty(iconSize), block)


    // region ==== Helper ====
    fun findCommand(action: Action): Command? {
        val command = commands[action]
        if (command == null) {
            // This is only "debug" level, because in some circumstances, creating a Button, MenuItem etc.
            // from an Action, with no corresponding Command is appropriate.
            log.debug("Command ${action.name} not found.")
        }
        return command
    }

    /**
     * Looks for the command matching [action], checks that it isn't disabled, and then calls the [Command.lambda].
     */
    fun fire(action: Action) {
        commands[action]?.let { command ->
            if (command.disableProperty?.value != true) {
                command.lambda?.invoke(ActionEvent())
            }
        }
    }

    inner class NodeBuilder(val iconSizeProperty: ObservableInt) {

        val iconSize by iconSizeProperty

        private fun setGraphic(labelled: Labelled, action: Action) {
            if (action.iconName.isNotBlank()) {
                val observableIcon = action.actions.icons?.resizableImage(action.iconName, iconSizeProperty)
                if (observableIcon?.value != null) {
                    labelled.graphic = ImageView(observableIcon)
                    if (action.tinted) labelled.style(TINTED)
                }
            }
        }

        /**
         * If [Action.alternateIconName] is set, then the toggle button's graphic
         * will change based on its `selectedProperty`.
         */
        private fun setToggleGraphic(toggleButton: ToggleButton, action: Action) {
            val alternateIconName = action.alternateIconName

            if (alternateIconName == null) {
                setGraphic(toggleButton, action)
                return
            }
            val observableSelectedIcon = action.actions.icons?.resizableImage(action.iconName, iconSizeProperty)
            val observableAltIcon = action.actions.icons?.resizableImage(alternateIconName, iconSizeProperty)

            if (observableSelectedIcon != null && observableAltIcon != null) {

                toggleButton.graphic = ImageView(
                    OptionalImageTernaryFunction(
                        toggleButton.selectedProperty, observableSelectedIcon, observableAltIcon
                    ) { test, defaultIcon, altIcon ->
                        if (test) altIcon else defaultIcon
                    }
                )

            } else {
                setGraphic(toggleButton, action)
            }
        }

        private fun updateButton(button: ButtonBase, action: Action) {
            button.textProperty.bindTo(action.textProperty)

            if (! action.tooltip.isNullOrBlank() || action.text.isNotBlank()) {
                button.tooltip = ActionTooltip(action)
            }

            if (button is ToggleButton) {
                setToggleGraphic(button, action)
            } else {
                setGraphic(button, action)
            }
            findCommand(action)?.let { command ->
                command.lambda?.let {
                    button.onAction(block = it)
                }
                command.disableProperty?.let {
                    button.disabledProperty.bindTo(it)
                }
            }
        }

        private fun updateToggle(toggle: Toggle, action: Action) {
            val command = findCommand(action)
            if (command is ToggleCommand) {
                toggle.selectedProperty.bidirectionalBind(command.selectedProperty)
            }
        }

        private fun updateMenuItem(menuItem: LabelledMenuItem, action: Action) {
            menuItem.textProperty.bindTo(action.textProperty)

            if (action.iconName.isNotBlank()) {
                val observableIcon = action.actions.icons?.resizableImage(action.iconName, iconSizeProperty)
                if (observableIcon?.value != null) {
                    menuItem.graphic = ImageView(observableIcon)
                    if (action.tinted) menuItem.style(TINTED)
                }
            }

            findCommand(action)?.let { command ->
                command.lambda?.let {
                    menuItem.onAction(block = it)
                }
                command.disableProperty?.let {
                    menuItem.disabledProperty.bindTo(it)
                }
            }

            if (menuItem !is SubMenu) {
                menuItem.keyCombinationProperty.bindTo(action.keyCombinationProperty)
            }

        }

        // endregion

        // region ==== Create Controls ====

        // menu
        fun menu(action: Action) = Menu("").apply { textProperty.bindTo(action.textProperty) }
        fun menu(action: Action, block: Menu.() -> Unit) =
            Menu("").apply { textProperty.bindTo(action.textProperty) }.apply(block)

        // menuItem
        fun menuItem(action: Action) = MenuItem("").apply {
            updateMenuItem(this, action)
        }

        fun menuItem(action: Action, block: MenuItem.() -> Unit) = menuItem(action).apply(block)

        // checkMenuItem
        fun checkMenuItem(action: Action) = ToggleMenuItem("").apply {
            updateMenuItem(this, action)
            updateToggle(this, action)
        }

        fun checkMenuItem(action: Action, block: ToggleMenuItem.() -> Unit) = checkMenuItem(action).apply(block)

        // subMenu
        fun subMenu(action: Action) = SubMenu("").apply {
            updateMenuItem(this, action)
        }

        fun subMenu(action: Action, block: SubMenu.() -> Unit) = subMenu(action).apply(block)

        // button
        fun button(action: Action) = Button("").apply {
            updateButton(this, action)
        }

        fun button(action: Action, block: Button.() -> Unit) = button(action).apply(block)

        // toggleButton
        fun toggleButton(action: Action) = ToggleButton("").apply {
            updateButton(this, action)
            updateToggle(this, action)
        }

        fun toggleButton(action: Action, block: ToggleButton.() -> Unit) = toggleButton(action).apply(block)

        fun propertyRadioButton(action: Action): PropertyRadioButton<*, *> {
            val command = (findCommand(action) as? PropertyRadioCommand<*, *>)
                ?: throw GlokException("Command ${action.name} not found")
            val button = command.createButton()
            updateButton(button, action)
            return button
        }

        fun propertyRadioButton(action: Action, block: PropertyRadioButton<*, *>.() -> Unit) =
            propertyRadioButton(action).apply(block)

        fun propertyRadioButton2(action: Action): PropertyRadioButton<*, *> {
            val command = (findCommand(action) as? PropertyRadioCommand<*, *>)
                ?: throw GlokException("Command ${action.name} not found")
            val button = command.createButton().apply {
                styles.remove(RADIO_BUTTON)
                styles.add(RADIO_BUTTON2)
            }
            updateButton(button, action)
            return button
        }

        fun propertyRadioButton2(action: Action, block: PropertyRadioButton<*, *>.() -> Unit) =
            propertyRadioButton2(action).apply(block)

        fun propertyToggleButton(action: Action): PropertyToggleButton<*, *> {
            val command = (findCommand(action) as? PropertyToggleCommand<*, *>)
                ?: throw GlokException("Command ${action.name} not found")
            val button = command.createButton()
            updateButton(button, action)
            return button
        }

        fun propertyToggleButton(action: Action, block: PropertyToggleButton<*, *>.() -> Unit) =
            propertyToggleButton(action).apply(block)


        fun menuButton(action: Action) = MenuButton("").apply {
            updateButton(this, action)
        }

        fun menuButton(action: Action, block: MenuButton.() -> Unit) = menuButton(action).apply(block)

        fun splitMenuButton(action: Action) = SplitMenuButton("").apply {
            updateButton(this, action)
        }

        fun splitMenuButton(action: Action, block: SplitMenuButton.() -> Unit) = splitMenuButton(action).apply(block)

        fun propertyRadioMenuItem(action: Action): PropertyRadioMenuItem<*, *> {
            val command = (findCommand(action) as? PropertyRadioCommand<*, *>)
                ?: throw GlokException("Action ${action.name} not found")
            val menuItem = command.createMenuItem()
            updateMenuItem(menuItem, action)
            return menuItem
        }

        fun propertyRadioMenuItem(action: Action, block: PropertyRadioMenuItem<*, *>.() -> Unit) =
            propertyRadioMenuItem(action).apply(block)


        fun propertyToggleMenuItem(action: Action): PropertyToggleMenuItem<*, *> {
            val command = (findCommand(action) as? PropertyToggleCommand<*, *>)
                ?: throw GlokException("Action ${action.name} not found")
            val menuItem = command.createMenuItem()
            updateMenuItem(menuItem, action)
            return menuItem
        }

        fun propertyToggleMenuItem(action: Action, block: PropertyToggleMenuItem<*, *>.() -> Unit) =
            propertyToggleMenuItem(action).apply(block)

        // endregion Create Controls

    }

}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.theme.styles.LABEL

/**
 * Displays a single line of text, with an optional extra [graphic].
 * The [graphic] is often an icon, but can be any [Node].
 *
 * ### Theme DSL
 *
 *     "label" {
 *     }
 *
 * Label inherits all the feature of [Labelled].
 *
 */
class Label(text: String, graphic: Node?) : Labelled(text, graphic) {

    constructor(text: String, image: Image?) : this(text, ImageView(image))

    constructor(text: String) : this(text, null as Node?)
    constructor(image: Image?) : this("", ImageView(image))
    constructor(graphic: Node?) : this("", graphic)

    val labelForProperty by optionalNodeProperty(null)
    var labelFor by labelForProperty

    init {
        styles.add(LABEL)

        onMouseClicked {
            (labelFor as? Actionable?)?.performAction()
        }
    }
}

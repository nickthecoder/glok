package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.NamedImages
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.texture
import uk.co.nickthecoder.glok.scene.icons
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle

/**
 * The base class for the default theme [Tantalum].
 * This doesn't declare any styles, or colors.
 */
abstract class CustomisableTheme : ThemeBuilder() {

    val icons = icons(backend.resources("uk/co/nickthecoder/glok")) {
        texture("glokIcons.png", 64) {
            row(
                2, 2, 4,
                "blank", "check_box:selected", "check_box:indeterminate", "more",
                "expanded", "contracted", "caret",
                "down", "up", "left", "right", "close", "checkered", "tick", "minimize"
            )
            row(
                2, 2 + 1 * 68, 4,
                "information", "warning", "error", "confirmation", "refresh", "root", "visible",
                "sync", "sync_once", "back", "forward", "cross_hairs", "hamburger", "folder", "file"
            )
            row(
                2, 2 + 2 * 68, 4,
                "file_tinted", "folder_tinted", "switch_knob", "remove", "add", "settings_tinted",
                "radio_button:selected"
            )
        }
    }

    // ==== Properties ====

    val fontSizeProperty by floatProperty(Font.defaultFont.identifier.size)
    var fontSize by fontSizeProperty

    val fontFamilyProperty by stringProperty(Font.defaultFont.identifier.family)
    var fontFamily by fontFamilyProperty

    val fontProperty = FontBinaryFunction(fontSizeProperty, fontFamilyProperty) { size, family ->
        Font.create(size, FontStyle.PLAIN, family)
    }
    val font by fontProperty

    val fixedWidthFontSizeProperty by floatProperty(fontSize)
    var fixedWidthFontSize by fixedWidthFontSizeProperty

    val fixedWidthFontFamilyProperty by stringProperty("MonoSpaced")
    var fixedWidthFontFamily by fixedWidthFontFamilyProperty

    val fixedWidthFontProperty =
        FontBinaryFunction(fixedWidthFontSizeProperty, fixedWidthFontFamilyProperty) { size, family ->
            Font.create(size, FontStyle.PLAIN, family)
    }
    val fixedWidthFont by fixedWidthFontProperty

    // ==== Sizes ====

    val borderThicknessProperty by floatProperty(2f)
    var borderThickness by borderThicknessProperty

    val radiusProperty by floatProperty(4f)
    var radius by radiusProperty

    val buttonPaddingProperty by edgesProperty(Edges(3f, 10f))
    var buttonPadding by buttonPaddingProperty

    val labelPaddingProperty by edgesProperty(Edges(6f, 6f))
    var labelPadding by labelPaddingProperty

    val focusBorderThicknessProperty by floatProperty(2f)
    var focusBorderThickness by focusBorderThicknessProperty

    // ==== Icons ====

    val iconSizeProperty by intProperty(24)
    var iconSize by iconSizeProperty

    val dockIconSizeProperty by intProperty(18)
    var dockIconSize by dockIconSizeProperty


    private val scaledIconsProperty: ObservableValue<NamedImages> = UnaryFunction(iconSizeProperty) { size ->
        icons.size(size)
    }

    /**
     * Scaled versions of Tantalum's [icons] sized using [iconSize].
     * Use these icons in toolbars etc.
     *
     */
    val scaledIcons by scaledIconsProperty

    val smallIcons = icons.size(16)

    private val halfIconsProperty: ObservableValue<NamedImages> = UnaryFunction(iconSizeProperty) { size ->
        icons.size(size / 2)
    }
    val halfIcons by halfIconsProperty

    // ==== Theme ====

    // ==== End of properties ====

    init {

        dependsOn(
            fontProperty, fixedWidthFontProperty,
            borderThicknessProperty, radiusProperty, buttonPaddingProperty, labelPaddingProperty, focusBorderThicknessProperty,
            iconSizeProperty,
            dockIconSizeProperty
        )

    }

    // ==== End of fields ====

    fun icon(name: String) = scaledIcons[name]?.let { ImageView(it) }

}

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.control.Region

class NinePatchBackground(val ninePatch: NinePatch) : Background {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        ninePatch.draw(x, y, width, height)
    }

}

/**
 * Similar to [NinePatchBackground], but the source texture's pixels are multiplied
 * by a color (usually from [Region.backgroundColor]).
 * Used in two ways :
 *
 * 1. Making the background semi-transparent by using only the alpha channel of the color.
 * 2. Using the RGB channels of the color on a greyscale nine-patch.
 */
class TintedNinePatchBackground(val ninePatch: NinePatch) : Background {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        ninePatch.draw(x, y, width, height, tint = color)
    }

}

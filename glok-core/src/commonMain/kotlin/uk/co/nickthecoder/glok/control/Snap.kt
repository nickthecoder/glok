/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.math.roundToLong

/**
 * Given an input, produces an output, which is _snapped_ to key values, when the input is close to it.
 * Used by [SliderBase].
 *
 * For example, suppose [V] is Int, and the values are a percentage. We could snap to 0, 25, 50, 75 and 100.
 * If the threshold were 2, then inputs 23, 24, 25, 26, and 27 would all return 25.
 */
interface Snap<V> {
    fun snap(input: V): V
}

/**
 * If you need a FloatSpinner, which returns whole numbers (as Floats)
 */
class FloatSnapWholeNumber : Snap<Float> {
    override fun snap(input: Float) = input.roundToInt().toFloat()
}

/**
 * If you need a [DoubleSlider], which returns whole numbers (as Doubles)
 */
class DoubleSnapWholeNumber : Snap<Double> {
    override fun snap(input: Double) = input.roundToLong().toDouble()
}

// Implementation note. I'm pretty sure there's a smarter way to do this,
// But I'm in a bad mood, and right now, good enough is good enough!

class FloatSnapEvery(val every: Float, val threshold: Float) : Snap<Float> {
    override fun snap(input: Float): Float {
        val div = input / every
        val near = div.toInt() * every
        val diff = abs(input - near)
        if (diff < threshold) {
            return near
        }
        if (diff > every - threshold) {
            return if (input >= 0f) near + every else near - every
        }
        return input
    }
}

class DoubleSnapEvery(val every: Double, val threshold: Double) : Snap<Double> {
    override fun snap(input: Double): Double {
        val div = input / every
        val near = div.toInt() * every
        val diff = abs(input - near)
        if (diff < threshold) {
            return near
        }
        if (diff > every - threshold) {
            return if (input >= 0f) near + every else near - every
        }
        return input
    }
}

class IntSnapEvery(val every: Int, val threshold: Int) : Snap<Int> {
    override fun snap(input: Int): Int {
        val div = input / every
        val near = div * every
        val diff = abs(input - near)
        if (diff < threshold) {
            return near
        }
        if (diff > every - threshold) {
            return if (input >= 0) near + every else near - every
        }
        return input
    }
}

class LongSnapEvery(val every: Long, val threshold: Long) : Snap<Long> {
    override fun snap(input: Long): Long {
        val div = input / every
        val near = div * every
        val diff = abs(input - near)
        if (diff < threshold) {
            return near
        }
        if (diff > every - threshold) {
            return if (input >= 0) near + every else near - every
        }
        return input
    }
}

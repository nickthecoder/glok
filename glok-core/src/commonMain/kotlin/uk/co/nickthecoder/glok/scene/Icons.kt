package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.backend.Resources
import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.glok.property.boilerplate.*
import kotlin.math.roundToInt

interface NamedImages {

    /**
     * Returns an [Image] with the given name at the default size.
     */
    operator fun get(name: String): Image?

    /**
     * Returns an [Image] with the given name at the requested size.
     */
    fun get(name: String, size: Int): Image?

    /**
     * Returns another view of the same named images, with an additional feature :
     * See [ResizableNamedImages.getResizable].
     */
    fun resizableNamedImages(sizeProperty: ObservableInt): ResizableNamedImages

    fun resizableImage(name: String, iconSizeProperty: ObservableInt): ObservableOptionalImage? =
        if (get(name) == null) {
            null
        } else {
            OptionalImageUnaryFunction(iconSizeProperty) { size ->
                get(name, size)
            }
        }
}


fun ObservableOptionalImage.resizable(sizeProperty: IntProperty): ObservableOptionalImage = OptionalImageBinaryFunction(this, sizeProperty) { image, size ->
    image?.scaleTo(size)
}

/**
 * Used for images which can be resized on the fly.
 * This is designed to be used by [Icons], so that you can build the GUI Nodes,
 * and then later, change the value of an `iconSizeProperty`, all icons will change size.
 *
 * You can use more than one [ResizableNamedImages], e.g. One for toolBar buttons, and
 * another for buttons outside a toolBar.
 */
interface ResizableNamedImages : NamedImages {

    val iconSizeProperty: ObservableInt
    val iconSize: Int

    /**
     * Return an [ObservableOptionalImage] whose value changes whenever [iconSizeProperty] changes.
     */
    fun getResizable(name: String): ObservableOptionalImage
}

/**
 * Loads icons from [Resources], where a single `.png` file contains many icons.
 * These icons should have a gap between them, to prevent _bleeding_ from one icon into its neighbours.
 * You give a name to each icon, via a DSL (domain specific language).
 *
 * You may optionally supply multiple `.png` files, at various resolutions.
 * Icons can be requested at any size, and scaling is used if there isn't a `.png` file for that particular resolution.
 *
 * The names and positions of icons within the `.png` file are defined like so :
 *
 *     myIcons = icons( myResources ) {
 *         texture( "foo64.png", 64 ) {
 *             row( "file_", 2, 2, 1, "save", "save_as", "open" )
 *             row( "edit_", 2, 68, 1, "copy", "cut", "paste" )
 *         }
 *     }
 *
 * The above only uses 1 resolution (64x64 pixels).
 * The `row` method takes an optional `prefix` (in this case `file_` and `edit_`) as well
 * as the start of the row (2,2) and (2,68), and the column spacing (1).
 * The net results are 6 named icons `file_save`, `file_save_as` etc.
 *
 * To get a `full-size` icon :
 *
 *     myIcons[ "file_save" ]
 *
 * To get a smaller icons either :
 *
 *     myIcons.get( "file_save", 32 )
 *
 *     val smallIcons = myIcons.size(32)
 *     smallIcons[ "file_save" ]
 *
 * There's also a clever feature, which allows all of your icons to scale automatically using an IntProperty :
 *
 *     val iconSizeProperty = SimpleIntProperty( 32 )
 *     val buttonIcons = myIcons.resizableIcons( iconSizeProperty )
 *     fileSaveButton.graphic = buttonIcons[ "file_save" ]
 *     fileSaveAsButton.graphic = buttonIcons[ "file_save_as" ]
 *
 * If we now change `iconsSizeProperty`, all button icons will update.
 * Notice that `buttonIcons` return `ImageView`s, not `Image`s.
 *
 */
class Icons(val resources: Resources) : NamedImages {

    private var defaultSize: Int = 0

    private val iconSheetsBySize = mutableMapOf<Int, IconSheet>()

    fun add(iconSheet: IconSheet) {
        if (defaultSize == 0) {
            defaultSize = iconSheet.size
        }
        iconSheetsBySize[iconSheet.size] = iconSheet
    }

    fun names(): List<String> {
        val result = mutableSetOf<String>()
        for (sheet in iconSheetsBySize.values) {
            result.addAll(sheet.iconPositions.keys)
        }
        return result.toList().sorted()
    }

    override operator fun get(name: String) = get(name, defaultSize)

    override fun get(name: String, size: Int): Image? {
        // If we are scaling the output by 2 (for high DPI monitor), then we actually want an
        // image twice the size, and then scale it by 0.5, so that it reports its size in "logical"
        // units. Therefore, we end up with pixel perfect images.
        val actualSize = (size * GlokSettings.globalScale).roundToInt()

        iconSheetsBySize[actualSize]?.let { sheet ->
            sheet[name]?.let { (x, y) ->
                val found =
                    sheet.texture.partialImage(x.toFloat(), y.toFloat(), sheet.size.toFloat(), sheet.size.toFloat())
                return if (actualSize == size) {
                    found
                } else {
                    found.scaledBy(1f / GlokSettings.globalScale)
                }
            }
        }
        for (key in iconSheetsBySize.keys.sortedBy { -it }) {
            iconSheetsBySize[key]?.let { sheet ->
                sheet[name]?.let { (x, y) ->
                    return sheet.texture.partialImage(
                        x.toFloat(),
                        y.toFloat(),
                        sheet.size.toFloat(),
                        sheet.size.toFloat()
                    ).scaledBy(size.toFloat() / key.toFloat())
                }
            }
        }

        return null
    }

    /**
     * Allows easy access to images of a given size. Example usage :
     *
     *     val myIcons = icons{ ... } // Build metadata for icons of various sizes.
     *     val sizedImages = myIcons( 32 ) // But we only want icons of size 32.
     *     // Find an icon of size 32, or scale a larger version down to size 32.
     *     val saveImage = sizedImages( "document-save" )
     */
    fun size(size: Int) = SizedIcons(size)

    /**
     * If you want the icon size to be user definable, then create an IntProperty for the preferred size.
     * And use it like so :
     *
     *     val iconSizeUserPreference : IntProperty = ...
     *     val myIcons = icons{ ... } // Build metadata for many sizes
     *     val myResizableIcons = myIcons.resizable( iconSizeUserPreference )
     *     val myImageView = ImageView(rmyResizableIcons[ "document-save" ])
     */
    fun resizableIcons(sizeProperty: ObservableInt) = ResizableIcons(sizeProperty)
    override fun resizableNamedImages(sizeProperty: ObservableInt) = ResizableIcons(sizeProperty)

    inner class SizedIcons(val size: Int) : NamedImages {
        override operator fun get(name: String) = this@Icons.get(name, size)
        override fun get(name: String, size: Int) = this@Icons.get(name, size)
        override fun resizableNamedImages(sizeProperty: ObservableInt) = this@Icons.resizableNamedImages(sizeProperty)
        fun names() = this@Icons.names()

        override fun toString() = "SizedIcons($size) : ${this@Icons}"
    }

    inner class ResizableIcons(override val iconSizeProperty: ObservableInt) : ResizableNamedImages {

        override val iconSize: Int by iconSizeProperty

        override operator fun get(name: String) = get(name, iconSizeProperty.value)
        override fun get(name: String, size: Int) = this@Icons.get(name, size)

        override fun getResizable(name: String): ObservableOptionalImage {
            get(name, iconSizeProperty.value) ?: return SimpleOptionalImageProperty(null)
            return OptionalImageUnaryFunction(iconSizeProperty) {
                get(name, it)
            }
        }

        override fun resizableNamedImages(sizeProperty: ObservableInt) = this@Icons.resizableNamedImages(sizeProperty)

        fun names() = this@Icons.names()
        override fun toString() = "ResizableIcons : $iconSizeProperty"
    }

    // ==== Object methods ====

    override fun toString(): String {
        val names = mutableSetOf<String>()
        for (sheet in iconSheetsBySize.values) {
            names.addAll(sheet.iconPositions.keys)
        }
        return names.joinToString()
    }

}

class IconSheet(private val resources: Resources, private val textureName: String, val size: Int) {

    internal val iconPositions = mutableMapOf<String, Pair<Int, Int>>()

    val texture: Texture by lazy {
        resources.loadTexture(textureName)
    }

    fun addIcon(name: String, x: Int, y: Int) {
        iconPositions[name] = Pair(x, y)
    }

    operator fun get(name: String) = iconPositions[name]
}

/**
 * Collect metadata about the size and positions of [Icons].
 * Textures are not loaded yet, they will be loaded as required when [Icons.get] is called.
 */
fun icons(resources: Resources, block: Icons.() -> Unit): Icons {

    val icons = Icons(resources)
    icons.block()
    return icons

}

class Sheets(private val icons: Icons) {
    internal val sheets = mutableListOf<IconSheet>()

    private var row = 0
    private var top = 0
    private var left = 0
    private var spacing = 0

    fun sheet(textureName: String, size: Int) {
        val sheet = IconSheet(icons.resources, textureName, size)
        sheets.add(sheet)
    }

    fun grid(left: Int, top: Int, spacing: Int, block: Sheets.() -> Unit) {
        this.left = left
        this.top = top
        this.spacing = spacing
        block()
    }

    fun row(prefix: String, vararg names: String) {
        for (sheet in sheets) {
            var x = left
            for (name in names) {
                //println("$prefix$name @ $x, ${top + row * (sheet.size + spacing)}")
                sheet.addIcon("$prefix$name", x, top + row * (sheet.size + spacing))
                x += sheet.size + spacing
            }
        }
        row++
    }
}

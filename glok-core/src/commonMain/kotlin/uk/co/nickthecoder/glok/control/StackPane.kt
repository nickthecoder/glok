/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.alignmentProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.util.ifVisible
import uk.co.nickthecoder.glok.util.maxPrefHeight
import uk.co.nickthecoder.glok.util.maxPrefWidth
import uk.co.nickthecoder.glok.util.min

class StackPane : Region(), WithChildren {

    // region ==== Properties ====
    /**
     * If set, then each child is allocated the entire available space of this StackPane,
     * (limited to the child's max size).
     *
     * Otherwise, each child is allocated their preferred size
     * (limited by the available space of this StackPane)
     */
    val fillProperty by booleanProperty(false)
    var fill by fillProperty

    val alignmentProperty by alignmentProperty(Alignment.CENTER_CENTER)
    var alignment by alignmentProperty
    // endregion properties

    // region ==== Fields ====
    override val children = mutableListOf<Node>().asMutableObservableList()

    // endregion fields

    init {
        children.addChangeListener(childrenListener)
        alignmentProperty.addListener(requestLayoutListener)
        fillProperty.addListener(requestLayoutListener)
    }

    // region ==== Layout ====

    override fun nodeMinWidth() = children.ifVisible().minOf { it.evalMinWidth() }
    override fun nodeMinHeight() = children.ifVisible().minOf { it.evalMinHeight() }
    override fun nodePrefWidth() = children.ifVisible().maxPrefWidth()
    override fun nodePrefHeight() = children.ifVisible().maxPrefHeight()

    override fun layoutChildren() {
        val left = surroundLeft()
        val top = surroundTop()
        val availableX = width - surroundX()
        val availableY = height - surroundY()

        for (child in children) {
            if (child.visible) {
                val childWidth = min(availableX, if (fill) child.evalMaxWidth() else child.evalPrefWidth())
                val childHeight = min(availableY, if (fill) child.evalMaxHeight() else child.evalPrefHeight())
                val extraX = availableX - childWidth
                val extraY = availableY - childHeight
                val dx = when (alignment.hAlignment) {
                    HAlignment.LEFT -> 0f
                    HAlignment.CENTER -> extraX / 2
                    HAlignment.RIGHT -> extraX
                }
                val dy = when (alignment.vAlignment) {
                    VAlignment.TOP -> 0f
                    VAlignment.CENTER -> extraY / 2
                    VAlignment.BOTTOM -> extraY
                }
                setChildBounds(child, left + dx, top + dy, childWidth, childHeight)
            }
        }

    }

    // endregion layout

}

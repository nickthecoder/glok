
// **** Autogenerated. Do NOT edit. ****

package uk.co.nickthecoder.glok.property.boilerplate

import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.application.Restart
import uk.co.nickthecoder.glok.documentation.Boilerplate 


// region ==== Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ObservableValue<Restart>`, use `ObservableRestart`.
 */
interface ObservableRestart: ObservableValue<Restart> {}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Restart>`, use `ReadOnlyRestartProperty`.
 */
interface ReadOnlyRestartProperty : ObservableRestart, ReadOnlyProperty<Restart>

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `Property<Restart>`, use `RestartProperty`.
 */
interface RestartProperty : Property<Restart>, ReadOnlyRestartProperty {

    /**
     * Returns a read-only view of this mutable RestartProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by RestartProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyRestartProperty = ReadOnlyRestartPropertyWrapper( this )
}

/**
 * [Boilerplate] which avoids having to use generics.
 * Instead of `SimpleProperty<Restart>`, we can use `SimpleRestartProperty`.
 */
open class SimpleRestartProperty(initialValue: Restart, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Restart>(initialValue, bean, beanName), RestartProperty

/**
 * Never use this class directly.
 * Use [RestartProperty.asReadOnly] to obtain a read-only version of a mutable [RestartProperty].
 *
 * [Boilerplate] which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Restart, Property<Restart>>`, use `ReadOnlyRestartPropertyWrapper`.
 */
class ReadOnlyRestartPropertyWrapper(wraps: RestartProperty) :
    ReadOnlyPropertyWrapper<Restart, Property<Restart>>(wraps), ReadOnlyRestartProperty


// endregion Basic Features

// region ==== Optional Basic Features ====

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ObservableRestart], but the [value] can also be `null`.
 */
interface ObservableOptionalRestart : ObservableValue<Restart?> {
    
}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyRestartProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalRestartProperty : ObservableOptionalRestart, ReadOnlyProperty<Restart?>

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [RestartProperty], but the [value] can also be `null`.
 */
interface OptionalRestartProperty : Property<Restart?>, ReadOnlyOptionalRestartProperty {

    /**
     * Returns a read-only view of this mutable OptionalRestartProperty.
     * Typical usage :
     *
     *     private val mutableFooProperty by optionalRestartProperty( initialValue )
     *     val fooProperty = mutableFooProperty.asReadOnly()
     *     val foo by mutableFooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalRestartProperty = ReadOnlyOptionalRestartPropertyWrapper( this )

}

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [SimpleRestartProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalRestartProperty(initialValue: Restart?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Restart?>(initialValue, bean, beanName), OptionalRestartProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ValidatedRestartProperty], but the [value] can also be `null`.
 */
open class OptionalValidatedRestartProperty(initialValue: Restart?, bean: Any? = null, beanName: String? = null, validation: (ValidatedProperty<Restart?>,Restart?,Restart?)->Restart?) :
    ValidatedProperty<Restart?>(initialValue, bean, beanName, validation), OptionalRestartProperty

/**
 * [Boilerplate] which avoids having to use generics.
 *
 * Similar to [ReadOnlyRestartPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalRestartPropertyWrapper(wraps: OptionalRestartProperty) :
    ReadOnlyPropertyWrapper<Restart?, Property<Restart?>>(wraps), ReadOnlyOptionalRestartProperty

// endregion optional basic features

// region ==== Delegate ====

/**
 * A Kotlin `delegate` to create a [RestartProperty] (the implementation will be a [SimpleRestartProperty].
 * Typical usage :
 *
 *     val fooProperty by restartProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun restartProperty(initialValue: Restart) = PropertyDelegate<Restart,RestartProperty>(initialValue) { bean, name, value ->
    SimpleRestartProperty(value, bean, name)
}


//endregion Delegate

// region ==== Optional Delegate ====
/**
 * A Kotlin `delegate` to create an [OptionalRestartProperty] (the implementation will be a [SimpleOptionalRestartProperty].
 * Typical usage :
 *
 *     val fooProperty by optionalRestartProperty( initialValue )
 *     var foo by fooProperty
 *
 */
fun optionalRestartProperty(initialValue: Restart?) = PropertyDelegate<Restart?,OptionalRestartProperty>(initialValue) { bean, name, value ->
    SimpleOptionalRestartProperty(value, bean, name)
}

// endregion Optional Delegate



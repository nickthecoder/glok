/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedIntProperty
import uk.co.nickthecoder.glok.property.functions.div
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.property.functions.toDouble
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.SLIDER
import uk.co.nickthecoder.glok.util.clamp
import kotlin.math.roundToInt

/**
 * Edits a [FloatProperty] by typing digits into [TextArea], via up/down buttons, up/down keys
 * or by scrolling the mouse/trackpad.
 *
 * The range of values are bounded ([min] and [max]).
 *
 * For more details, see the base classes [SliderBase].
 */
class IntSlider : SliderBase() {

    // region ==== Properties ====

    val valueProperty by validatedIntProperty(0) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    var value by valueProperty

    val minProperty by intProperty(0)
    var min by minProperty

    val maxProperty by intProperty(100)
    var max by maxProperty

    val smallStepProperty by intProperty(1)
    var smallStep by smallStepProperty

    val largeStepProperty by intProperty(10)
    var largeStep by largeStepProperty

    /**
     * When null, the slider will transition smoothly across the range. However, you can
     * set this, so that the value will snap to particular values.
     */
    val snapProperty: Property<Snap<Int>?> = SimpleProperty(null)
    var snap by snapProperty

    val markers: MutableObservableList<Int> = mutableListOf<Int>().asMutableObservableList()
    val minorMarkers: MutableObservableList<Int> = mutableListOf<Int>().asMutableObservableList()

    // endregion

    // region ==== Fields ====

    override var ratio: Double
        get() = ratio(value)
        set(v) {
            val convertedValue = if (max <= min) min else min + ((max - min) * v).roundToInt()
            value = snap?.snap(convertedValue) ?: convertedValue
        }

    // endregion fields

    // region ==== init ====

    init {
        style(SLIDER)

        // When these properties change, we need to redraw this node.
        for (prop in listOf(minProperty, maxProperty, valueProperty)) {
            // NOTE. We are setting the thumb's position in drawChildren,
            // so we do NOT need to requestLayout, only requestRedraw.
            prop.addListener(requestRedrawListener)
        }
        markers.addListener(requestRedrawListener)

        smallRatioStepProperty.bindTo((smallStepProperty / (maxProperty - minProperty)).toDouble())
        largeRatioStepProperty.bindTo((largeStepProperty / (maxProperty - minProperty)).toDouble())
    }

    private fun ratio(value: Int) = if (max <= min) 0.0 else ((value - min).toDouble() / (max - min).toDouble())

    // endregion init

    override fun drawChildren() {

        // Draw markers
        if (markers.isNotEmpty()) {
            drawMarkers(markers.map { ratio(it) }, markerLength)
        }
        if (minorMarkers.isNotEmpty()) {
            drawMarkers(minorMarkers.map { ratio(it) }, minorMarkerLength)
        }
        super.drawChildren()
    }

    // ==== Object Methods ====

    override fun toString() = super.toString() + " { $min .. $max } = $value"

}

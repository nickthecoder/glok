package uk.co.nickthecoder.glok.event

/**
 * Holds a map of event handlers by their [EventType].
 *
 * Application developers do not need to use this class directly,
 * unless they have a non-Node class which requires event handling, and
 * multiple event handlers can be associated with a single [EventType].
 *
 * There may be multiple handlers for a single [EventType], in which case they are handled in
 * the order they were added. If one of them consumes the event, then the process stops
 * (i.e. later handlers do not fire).
 */
class EventHandlers {

    /**
     * NOTE, the `values` of the map are not strongly typed with respect to `E : Event`, so
     * we need UNCHECKED_CAST in numerous places.
     * Because [add] is strongly typed, these casts should never go wrong.
     */
    private val handlers = mutableMapOf<EventType<*>, EventHandler<*>>()

    /**
     * Adds a handler for a specific [eventType].
     * If there is already a handler for this event type, then the two are merged into a `CompoundEventHandler`,
     * and [find] will return the compound.
     */
    fun <E : Event> add(eventType: EventType<E>, handler: EventHandler<E>, combination: HandlerCombination) {
        val existing = handlers[eventType]
        if (existing == null || combination == HandlerCombination.REPLACE) {
            handlers[eventType] = handler
        } else {

            if (existing is CompoundEventHandler<*>) {
                @Suppress("UNCHECKED_CAST")
                (existing as CompoundEventHandler<E>).add(handler, combination)
            } else {
                @Suppress("UNCHECKED_CAST")
                existing as EventHandler<E>
                if (combination == HandlerCombination.BEFORE) {
                    handlers[eventType] = CompoundEventHandler(handler, existing)
                } else {
                    handlers[eventType] = CompoundEventHandler(existing, handler)
                }
            }
        }
    }

    /**
     * To remove an event handler, you must keep a reference to the handler passed to [add].
     * Do NOT use the result from [find], because you may remove more than you expect.
     *
     * For example, suppose you have a `Button`, and add an addition MOUSE_PRESSED handler
     * (which does something special when the middle button is pressed).
     * The result from [find] would be a `Compound` of Button's handler and your additional handler.
     * Removing the `Compound` would stop the button from working correctly.
     */
    fun <E : Event> remove(eventType: EventType<E>, handler: EventHandler<E>) {
        val existing = handlers[eventType]
        if (existing === handler) {
            handlers.remove(eventType)
        } else if (existing is CompoundEventHandler<*>) {
            @Suppress("UNCHECKED_CAST")
            (existing as CompoundEventHandler<E>).remove(handler)
        }
    }

    /**
     * Returns `null` if there are no handlers for `eventType`.
     *
     * If there is more than one handler for `eventType`, then a `Compound` is returned.
     * When [EventHandler.handle] is called on the `Compound` it will call each handler in turn (in the order they were added).
     * If one of the handles consumes the event, then processing stops.
     * (i.e. later handlers do not fire).
     */
    @Suppress("UNCHECKED_CAST")
    fun <E : Event> find(eventType: EventType<E>): EventHandler<E>? = handlers[eventType] as? EventHandler<E>

}

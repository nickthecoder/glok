package uk.co.nickthecoder.glok.control

/**
 * Determines when scroll bars should appear in a [ScrollPane].
 */
enum class ScrollBarPolicy {
    NEVER, ALWAYS, AS_NEEDED
}

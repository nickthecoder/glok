/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.ApplicationStatus
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.backend.Window
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.control.Transformation
import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.and

/**
 * The middleman between [Scene] and [Window].
 *
 * [Stage] receives _raw_ events from [Window] ([MouseEvent]s, [KeyEvent]s)
 */
class RegularStage : StageBase() {

    // region ==== Properties ====

    /**
     * The cursor that is actually used. In most cases, this is the same as [mousePointerProperty].
     * However, when the mouse is inside an [OverlayStage]s, this is bound to THEIR [mousePointerProperty]
     */
    internal val actualMousePointerProperty by mousePointerProperty(MousePointer.ARROW)
    internal var actualMousePointer by actualMousePointerProperty

    private val actualMousePointerListener = actualMousePointerProperty.addChangeListener { _, _, pointer ->
        window?.let { backend.setMousePointer(it, pointer) }
    }

    private val resizableListener = resizableProperty.addChangeListener { _, _, value ->
        window?.resizable = value
    }

    private val maximizedListener = maximizedProperty.addChangeListener { _, _, value ->
        window?.maximized = value
    }

    private val minimizedListener = minimizedProperty.addChangeListener { _, _, value ->
        window?.minimized = value
        if (!value) {
            scene?.requestRedraw = true
        }
    }

    private val titleListener = titleProperty.addChangeListener { _, _, new -> window?.title = new }

    private val mutableScaleProperty by floatProperty(1f)

    /**
     * The scaling factor to convert between [LogicalPixels] use by all Glok coordinates, and
     * `physical pixels` of the display. For very high resolution devices this is greater than 1.
     *
     * It is bound to [GlokSettings.globalScale].
     *
     * Note. It is NOT possible to scale [OverlayStage]s differently from their parent [RegularStage].
     */
    val scaleProperty = mutableScaleProperty.asReadOnly()
    val scale by scaleProperty
    private val scaleListener = mutableScaleProperty.addListener {
        window?.size()?.let { (width, height) ->
            scene?.stageResized(width / scale, height / scale)
        }
    }

    internal val mutableFocusedStageProperty by stageProperty(this)
    fun focusedStageProperty(): ObservableStage = mutableFocusedStageProperty.asReadOnly()

    /**
     * Which [Stage] has focus for this window.
     * If there are no [OverlayStage]s, (such as those used by popup menus), then it will also be `this`.
     */
    var focusedStage by mutableFocusedStageProperty
        internal set

    /**
     * Does this window have focus?
     * If so, the either this [RegularStage] has focus, or one of its [OverlayStage]s has the focus
     * (governed by [focusedStageProperty]).
     */
    internal val windowFocusedProperty by booleanProperty(true)
    internal var windowFocused by windowFocusedProperty

    /**
     * Note. If one of the [OverlayStage]s has focus, then this will be `false`.
     * i.e. this does NOT tell you if the _window_ has focus.
     */
    override val focusedProperty: ObservableBoolean =
        windowFocusedProperty and mutableFocusedStageProperty.sameInstance(this)
    override val focused: Boolean by focusedProperty

    // endregion properties

    // region ==== Fields ====
    /**
     * Set when [show] is called.
     * NOTE, the GLBackend will have already created a hidden [Window] when the application is started
     * (before a [Stage] or [Scene] have been created). The _primaryStage_ will use that window.
     * Subsequent stages (if there are any), will cause a new Window to be created.
     */
    var window: Window? = null

    private var closed = false

    internal val mutableOverlayStages = mutableListOf<OverlayStage>().asMutableObservableList()
    val overlayStages = mutableOverlayStages.asReadOnly()

    override val regularStage: RegularStage
        get() = this

    /**
     * The currently opened `Tooltip` stage created from [Node.tooltip] via [Tooltip.show].
     */
    internal var tooltipStage: Stage? = null

    /**
     * The stage that the mouse was previously in.
     */
    internal var mouseInStage : StageBase? = null

    /**
     * If [moveTo] is called before the [window] is created, then we store the data, and use it in [show].
     * [position] also returns this value before the [window] has been created.
     */
    private var preShowWindowPosition: Pair<Int, Int>? = null

    // endregion fields

    // region ==== init ====
    init {
        mutableScaleProperty.bindTo(GlokSettings.globalScaleProperty)
        actualMousePointerProperty.bindTo(mousePointerProperty)
    }
    // endregion init

    // region ==== Methods ====

    /**
     * See [StageType.POPUP]
     */
    fun closePopups() {
        for (overlay in overlayStages.toList()) {
            if (overlay.stageType == StageType.POPUP) {
                overlay.close()
            }
        }
    }

    /**
     * See [StageType.POPUP_MENU]
     */
    fun closePopupMenus() {
        for (overlay in overlayStages.toList()) {
            if (overlay.stageType == StageType.POPUP_MENU) {
                overlay.close()
            }
        }
    }

    internal fun overlayAt(x: Float, y: Float): OverlayStage? {
        for (overlay in mutableOverlayStages.asReversed()) {
            if (overlay.visible) {
                val overlayScene = overlay.scene
                if (overlayScene != null && overlayScene.root.containsScenePoint(x, y)) {
                    return overlay
                }
            }
        }
        return null
    }

    override fun show() {
        val scene = scene ?: return

        // If we are showing the primary stage when the application is first starting up, then run
        // the Platform.runLater tasks RIGHT NOW.
        // This gives Themes the chance to rebuild themselves.
        // Without this, we may be a jarring _flash_ because the first frame will be rendered using one theme
        // and the next frame with another theme. Particularly bad when switching between dark/light.
        if (Application.instance.status == ApplicationStatus.NOT_STARTED) {
            Platform.performPendingRunnable()
        }

        window?.let {
            it.show()
            return
        }

        preShow()
        val (monWidth, monHeight) = backend.monitorSize() ?: return
        val x = preShowWindowPosition?.first ?: ((monWidth - scene.width * GlokSettings.globalScale) / 2).toInt()
        val y = preShowWindowPosition?.second ?: ((monHeight - scene.height * GlokSettings.globalScale) / 2).toInt()

        // postShow
        val width = scene.width
        val height = scene.height

        if (window == null) {
            // Create the window.
            window = backend.createWindow((width * scale).toInt(), (height * scale).toInt()).apply {
                title = this@RegularStage.title
                windowListener = StageWindowListener(this@RegularStage)
                resizable = this@RegularStage.resizable
                moveTo(x, y)
                show()
            }
            Application.instance.mutableStages.add(this)
        }
        if (maximized) {
            window?.maximized = true
        }
        scene.requestLayout = true
        scene.requestRedraw = true
        scene.root.requestFocus(true)
    }

    /**
     * Hides the window, but does not close it. See [close], [show].
     */
    override fun hide() {
        window?.hide()
    }

    override fun setIcon(images: List<Image?>, tint: Color?) {
        window?.setIcon(images.filterNotNull(), tint)
    }

    /**
     * Moves the window. [x] and [y] are measured in physical pixels,
     * and therefore are NOT the same units as most of Glok's coordinates. See [GlokSettings.globalScale].
     *
     * According to the LWJGL docs, moveTo isn't supported under Wayland, and this will have no effect.
     * No exception is thrown. Untested.
     *
     * Moving a window can be very disorienting, therefore only move the window
     * before it is shown, or immediately after it is shown. YMMV.
     */
    fun moveTo(x: Int, y: Int) {
        if (window == null) {
            preShowWindowPosition = Pair(x, y)
        } else {
            window !!.moveTo(x, y)
        }
    }

    /**
     * The position of the window in physical pixels.
     * If the window hasn't been shown yet, and you haven't explicitly set the position using [moveTo],
     * then the result is null.
     *
     * According to the LWJGL docs, position is not supported by Wayland. null is returned. Untested.
     */
    fun position(): Pair<Int, Int>? {
        if (window == null) {
            return preShowWindowPosition
        }
        return window?.position()
    }

    internal fun focusOnTopmostStage() {
        focusedStage = overlayStages.lastOrNull { it.visible && it.stageType != StageType.POPUP } ?: regularStage
        scene?.requestRedraw = true
    }

    /**
     * Closes the window, right now.
     * (Note, [close] is only a request to close at the end of this frame's processing).
     */
    internal fun closeNow() {
        closed = true
        onClosed?.handle(ActionEvent())
        scene = null
        window?.closeNow()
        window = null
    }

    override fun resize(width: Float, height: Float) {
        val scene = scene ?: return
        scene.width = width
        scene.height = height
        window?.resize((width * scale).toInt(), (height * scale).toInt())
        scene.requestLayout = true
    }

    fun draw() {
        var foundModal = false
        val window = window ?: return

        backend.drawFrame(window) {
            scene?.let { scene ->
                if (scene.requestRestyling) {
                    scene.restyle()
                }
                if (scene.requestLayout) {
                    scene.layout()
                }
                try {
                    backend.drawView(scene.width, scene.height, scale) {
                        backend.clear(scene.backgroundColor)
                        scene.root.drawAll()

                        for (overlay in mutableOverlayStages) {
                            overlay.scene?.let { overlayScene ->
                                if (overlayScene.requestRestyling) {
                                    overlayScene.restyle()
                                }
                                if (overlayScene.requestLayout) {
                                    overlayScene.layout()
                                }
                                if (! foundModal && overlay.stageType == StageType.MODAL) {
                                    foundModal = true
                                    backend.fillRect(0f, 0f, scene.width, scene.height, Stage.modalGrey)
                                }
                                overlayScene.root.drawAll()
                                overlayScene.requestRedraw = false
                            }
                        }
                    }

                    if (focused) {
                        drawFocusOwner(scene)
                    } else {
                        for (overlay in mutableOverlayStages) {
                            if (overlay.focused) {
                                overlay.scene?.let { drawFocusOwner(it) }
                                break
                            }
                        }
                    }

                } finally {
                    scene.requestRedraw = false
                }
            }
        }
    }

    private fun drawFocusOwner(scene: Scene) {
        scene.focusOwner?.apply {
            try {
                if (focusedByKeyboard && focused && this is Region) {
                    this.forEachFromRoot {
                        if (it is Transformation) {
                            backend.transform(it.matrix)
                        }
                    }
                    if (focusBorderSize !== Edges.NONE) {
                        val fbs = focusBorderSize
                        focusBorder.draw(
                            sceneX - fbs.left, sceneY - fbs.top, width + fbs.x, height + fbs.y,
                            focusBorderColor,
                            fbs
                        )
                    }
                    if (section) {
                        focusHighlight.draw(sceneX, sceneY, width, height, focusHighlightColor, Edges.NONE)
                    }
                }

            } finally {
                backend.clearTransform()
            }
        }
    }

    // endregion methods

    // region ==== Companion ====
    companion object {
        internal var lastMouseMovementTime: Double = 0.0
    }
    // endregion companion
}

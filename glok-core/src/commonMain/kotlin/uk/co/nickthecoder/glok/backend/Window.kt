package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.WindowListener

interface Window {

    var title: String

    var windowListener: WindowListener?

    var resizable: Boolean
    var maximized: Boolean
    var minimized: Boolean
    var shouldClose: Boolean

    fun show()
    fun hide()
    fun closeNow()

    fun setIcon(images: List<Image>, tint : Color? = null)

    fun moveTo(x: Int, y: Int)
    fun resize(width: Int, height: Int)

    fun size(): Pair<Int, Int>
    fun position(): Pair<Int, Int>?

}

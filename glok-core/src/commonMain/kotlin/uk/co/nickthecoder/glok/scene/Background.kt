package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.property.boilerplate.ColorProperty


/**
 * Draws the background of a [Region].
 *
 */
interface Background {
    /**
     * Draw the background of an area.
     *
     * @param size The size of the border which will be drawn on top of the background.
     * Often ignored
     */
    fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges)

}

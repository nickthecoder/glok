/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dialog

import uk.co.nickthecoder.glok.control.ButtonBar
import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.combineActionEventHandlers
import uk.co.nickthecoder.glok.event.tryCatchHandle
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.GlokException


fun dialog(block: Dialog.() -> Unit) = Dialog().apply(block)

/**
 * A quick and easy way to create dialogs, with a button bar.
 *
 * Example :
 *
 *    dialog {
 *        title = "Hello"
 *        content {
 *            Label("Hello World")
 *        }
 *        buttons(ButtonType.OK, ButtonType.CANCEL) { reply ->
 *            if (reply == ButtonType.OK) println("OK Pressed")
 *        }
 *    }.show(parentStage)
 *
 */

open class Dialog : WithContent {

    val titleProperty by stringProperty("")
    var title by titleProperty

    val resizableProperty by booleanProperty(true)
    var resizable by resizableProperty

    val contentProperty by optionalNodeProperty(null)
    final override var content: Node? by contentProperty

    val buttonBar = ButtonBar()

    /**
     * The [ButtonType] that was pressed to dismiss this [Dialog].
     *
     * If this is null, the client can assume the stage was closed use the window's close button,
     * or `Alt+F4`.
     *
     * Note, this is only updated by buttons added via [buttonTypes].
     * So, if you add additional buttons directly to the [buttonBar] (which close the stage),
     * consider setting this manually, or provide another mechanism for the client to know
     * the result of the dialog.
     */
    val replyProperty by optionalButtonTypeProperty(null)
    var reply by replyProperty

    /**
     *
     */
    val onCreateProperty by optionalActionEventHandlerProperty(null)
    var onCreate by onCreateProperty

    /**
     * A `lateinit var`, which is set in [createStage]. Accessing it before [createStage] is called
     * will throw an Exception.
     */
    lateinit var stage: Stage
        private set

    /**
     * Adds buttons to the [buttonBar]. All these buttons close the popup stage before the [handler] is called.
     *
     * You may add additional buttons to the [buttonBar], which do not close the popup.
     */
    fun buttonTypes(vararg buttonTypes: ButtonType, handler: ((ButtonType) -> Unit)? = null) {
        buttonBar.apply {
            for (buttonType in buttonTypes) {
                meaning(buttonType.meaning) {
                    + button(buttonType.text) {
                        defaultButton = buttonType == ButtonType.OK
                        cancelButton = buttonType == ButtonType.CANCEL || buttonType == ButtonType.CLOSE
                        onAction {
                            reply = buttonType
                            if (handler != null) {
                                tryCatchHandle { handler(buttonType) }
                            }
                            stage.close()
                        }
                    }
                }
            }
        }
    }

    fun buttons(block: ButtonBar.() -> Unit) {
        buttonBar.block()
    }

    /**
     * A convenient way to set [onCreateProperty] using a lambda.
     * However, if an event handler is already present, then BOTH will be used.
     */
    fun onCreate(handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (event: ActionEvent) -> Unit) {
        combineActionEventHandlers(onCreateProperty, handlerCombination, block)
    }

    fun createStage(parentStage: Stage, stageType: StageType = StageType.NORMAL, block: (Stage.() -> Unit)? = null): Stage {
        if (::stage.isInitialized) throw GlokException("Stage has already been created")

        stage = stage(parentStage, stageType) {
            title = this@Dialog.title
            resizableProperty.bidirectionalBind(this@Dialog.resizableProperty)
            scene {
                root = borderPane {
                    centerProperty.bindTo(contentProperty)
                    bottom = buttonBar
                }
            }
        }

        onCreate?.tryCatchHandle(ActionEvent())

        if (block != null) stage.block()

        return stage
    }
}

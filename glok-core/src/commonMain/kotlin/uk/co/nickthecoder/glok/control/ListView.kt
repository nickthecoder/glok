/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.LIST_VIEW
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.min
import kotlin.math.ceil
import kotlin.math.floor

/**
 * Displays a list of items vertically inside a ScrollPane.
 *
 * Each visible item in the list is displayed using a [ListCell].
 * By default, [cellFactory] creates [TextListCell]s, whose text is each [items] value converted to
 * a string using [Any.toString]. However, you can create a custom [cellFactory], which creates any
 * [ListCell]s containing whatever you need.
 *
 * There are only ever enough [ListCell]s for the _visible_ items.
 * For example, if `items.size` == 100, but only 10 fit within the [ListView]'s bounds, then there be 10
 * [ListCell]s, and as you scroll, the same 10 [ListCell]s will be reused, and their contents will be updated
 * to reflect the data within the [items].
 *
 * If type [T] is mutable, then the [cellFactory] must `bind` to the item's properties, to ensure that
 * mutating the data is reflected in the [ListCell].
 *
 * Currently, all cells must be the same height, which is governed by [fixedRowHeight].
 * and if this is <= 0, then the first cell's `prefHeight` is used instead.
 *
 * ### Theme DSL
 *
 *     "list_view" {
 *         "fixedCellSize"( float )
 *
 *         descendant("list_cell") {
 *             ":odd" { ... }
 *             ":even" { ... }
 *             // The selected row of the listView.
 *             ":selected" { ... }
 *         }
 *     }
 */
// NOTE, ListView, TreeView and MixedTreeView are all quite similar, but share no code.
// Not using the DRY principle (Don't Repeat Yourself).
// We are WET (Write Everything Thrice).
// This was deliberate, as the cure may well be worse than the disease!
// Changes to one (especially bug fixes) should be applied to the others.
class ListView<T>(

    val items: MutableObservableList<T>

) : Region() {

    constructor() : this(mutableListOf<T>().asMutableObservableList())

    val selection = SingleSelectionModel(items)

    // region ==== Properties ====

    val valueProperty = selection.selectedItemProperty
    var value by selection.selectedItemProperty

    /**
     * When set, items in the ListView can be reordered by dragging with the mouse,
     * and also using Ctrl+Up / Down.
     * The default value is `false`.
     *
     * NOTE. If you require special handling when dragging [ListCell]s
     * (for example moving items between two or more lists), then you should keep this `false`
     * and implement you own `onMousePressed` and `onMouseDragged` events on a custom [ListCell].
     * (See [cellFactory]).
     */
    val canReorderProperty by booleanProperty(false)
    var canReorder by canReorderProperty


    /**
     * Glok currently assumes that all rows are the same height.
     * The easiest way to do this is settings [fixedRowHeight]. The default value is 0,
     * in which case Glok looks at the size of the first visible row, and assumes the same is true
     * for all the others. If this assumption is wrong, then scrolling will go wrong (it will
     * jump weirdly).
     */
    val fixedRowHeightProperty by stylableFloatProperty(0f)
    var fixedRowHeight by fixedRowHeightProperty

    // endregion

    // region ==== Fields ====

    /**
     * A ListView has a list of [ListCell]s for the [items] which are visible.
     * The [ListCell]s are created and destroyed as you scroll through the list.
     *
     * The default [ListCell]s are simple [TextListCell]s, but you can create richer cells
     * by using a `cellFactory`.
     *
     * You will also use a `cellFactory` if you want the cell to have additional functionality,
     * for example, if you want to add tooltips, or addition event handlers.
     */
    var cellFactory: (ListView<T>, T) -> ListCell<T> = { listView, item -> TextListCell(listView, item) }
        set(v) {
            field = v
            content.children.clear()
            requestRedraw()
        }

    private val content = Content()

    private val scrollPane = ScrollPane(content)

    override val children = listOf(scrollPane).asObservableList()
    // endregion fields

    // region ==== Commands ====
    val commands = Commands().apply {
        with(ListViewActions) {
            SELECT_UP {
                if (selection.selectedIndex > 0) {
                    selection.selectedIndex --
                    scrollToRow(selection.selectedIndex)
                }
            }
            SELECT_DOWN {
                if (selection.selectedIndex < items.size - 1) {
                    selection.selectedIndex ++
                    scrollToRow(selection.selectedIndex)
                }
            }
            MOVE_UP {
                if (canReorder && selection.selectedIndex > 0) {
                    moveItem(selection.selectedIndex, selection.selectedIndex - 1)
                    scrollToRow(selection.selectedIndex)
                }
            }
            MOVE_DOWN {
                if (canReorder && selection.selectedIndex < items.size - 1) {
                    moveItem(selection.selectedIndex, selection.selectedIndex + 1)
                    scrollToRow(selection.selectedIndex)
                }
            }
        }
        attachTo(this@ListView)
    }
    // endregion commands

    init {
        style(LIST_VIEW)
        claimChildren()

        fixedRowHeightProperty.addListener(requestLayoutListener)

        onMousePressed {
            requestFocus()
        }
        scrollPane.commands.attachTo(this)
    }


    // region ==== methods ====

    fun visibleCells(): List<ListCell<T>> = content.children

    fun scrollToRow(row: Int) {
        content.scrollToRow(row)
    }

    /**
     * Find the index of `value` within [items], using `===` as the test.
     * This is different from [List.indexOf], which uses `equals()` rather than `===`.
     * Therefore [List.indexOf] would find the wrong item if there are more than one item in the list
     * that met the `equals()` test.
     *
     * @return The first index of `value` in the `items` list, or -1 if not found.
     */
    fun indexOf(value: T): Int {
        for ((index, item) in items.withIndex()) {
            if (item === value) {
                return index
            }
        }
        return - 1
    }

    fun scrollToItem(value: T) {
        val index = indexOf(value)
        if (index >= 0) {
            scrollToRow(index)
        }
    }

    fun scrollBy(delta: Float) {
        content.scrollBy(delta)
    }

    /**
     * Moves an item within [items].
     * If the item was previously selected, it remains selected.
     * (If you were to just remove and add the item, this would not be true).
     */
    fun moveItem(oldIndex: Int, newIndex: Int) {
        if (newIndex < 0 || newIndex >= items.size) throw IllegalArgumentException("Illegal newIndex : $newIndex")
        if (oldIndex < 0 || oldIndex >= items.size) throw IllegalArgumentException("Illegal oldIndex : $oldIndex")

        val wasSelected = oldIndex == selection.selectedIndex
        val item = items.removeAt(oldIndex) !!
        items.add(newIndex, item)
        for (cell in content.children) {
            cell.index = indexOf(cell.item)
        }
        if (wasSelected) {
            selection.selectedIndex = newIndex
        }
    }

    // endregion

    // region ==== layout ====

    /**
     * By default, ListViews have an arbitrary prefWidth. Set [evalPrefWidth], or add the ListView to a layout control
     * which will allocate a sensible area regardless of [evalPrefWidth].
     */
    override fun nodePrefWidth() = 200f

    /**
     * By default, ListViews have an arbitrary prefHeight. Set [evalPrefHeight], or add the ListView to a layout control
     * which will allocate a sensible area regardless of [evalPrefWidth].
     */
    override fun nodePrefHeight() = 80f

    override fun layoutChildren() {
        setChildBounds(scrollPane, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    // endregion

    // region ==== Object methods ====
    override fun toString() = super.toString() + " ${items.size} items." +
        if (selection.selectedIndex >= 0) " #${selection.selectedIndex} selected" else ""
    // endregion

    // region == inner class Content ==
    private inner class Content : Region() {

        override val children = mutableListOf<ListCell<T>>().asMutableObservableList()

        init {
            items.addChangeListener { _, _ ->
                requestLayout()
            }
            children.addChangeListener { list, changes -> childrenListener.changed(list, changes) }
        }

        fun scrollToRow(row: Int) {
            val viewport = parent ?: return
            val cellHeight = rowHeight()

            val scrollValueAtTop = cellHeight * row
            if (scrollPane.vScrollValue > scrollValueAtTop) {
                scrollPane.vScrollValue = scrollValueAtTop
            } else {
                val scrollValueAtBottom = cellHeight * (row + 1) - viewport.height
                if (scrollPane.vScrollValue < scrollValueAtBottom) {
                    scrollPane.vScrollValue = scrollValueAtBottom
                }
            }
            requestLayout()
        }

        fun scrollBy(delta: Float) {
            scrollPane.vScrollValue = (scrollPane.vScrollValue + delta).clamp(0f, scrollPane.vScrollMax)
        }

        // region === layout ===

        init {
            fixedRowHeightProperty.addListener(requestLayoutListener)
        }

        override fun nodePrefWidth() = 100f
        override fun nodePrefHeight() = surroundY() + items.size * rowHeight()

        private fun rowHeight() = if (fixedRowHeight == 0f) {
            children.firstOrNull()?.evalPrefHeight()
                ?: if (items.isEmpty()) {
                    24f // It doesn't matter what size we return! There are no rows.
                } else {
                    firstVisibleItemIndex = min(firstVisibleItemIndex, items.size - 1)
                    val cell = findOrCreateCell(firstVisibleItemIndex)
                    children.add(cell)
                    cell.evalPrefHeight()
                }
        } else {
            fixedRowHeight
        }

        private fun findOrCreateCell(index: Int): ListCell<T> {
            val item = items[index] !!
            var cell: ListCell<T>? = children.firstOrNull { it.item === item }
            if (cell == null) {
                cell = cellFactory(this@ListView, item)
                cell.index = index
                madeNewRow = true
            }
            return cell
        }

        private var firstVisibleItemIndex = 0

        private var madeNewRow = false

        override fun layoutChildren() {
            super.layoutChildren()
            madeNewRow = false

            val viewport = parent ?: return

            // Create one cell, so that fixedSize() can return a valid value.
            if (children.isEmpty() && items.isNotEmpty()) {
                val cell = cellFactory(this@ListView, items.first())
                children.add(cell)
            }
            val rowHeight = rowHeight()

            firstVisibleItemIndex = floor(- localY / rowHeight).toInt()
            val spaceForRows = ceil(viewport.height / rowHeight).toInt() + 1

            // The number of cells visible based on the height.
            val required = min(spaceForRows, items.size - firstVisibleItemIndex)

            val w = width - surroundX()
            val x = surroundLeft()
            var y = surroundTop() + firstVisibleItemIndex * rowHeight

            val newChildren = mutableListOf<ListCell<T>>()
            for (itemsIndex in firstVisibleItemIndex until firstVisibleItemIndex + required) {

                val cell = findOrCreateCell(itemsIndex)
                newChildren.add(cell)

                setChildBounds(cell, x, y, w, rowHeight)
                y += rowHeight
            }

            if (madeNewRow || children.size != newChildren.size) {
                // We made changes, so replace children.
                children.clear()
                children.addAll(newChildren)
            }

        }
        // endregion == layout ==

    }

}

object ListViewActions : Actions(null) {

    val SELECT_UP = define("up", "Up", Key.UP.noMods())
    val SELECT_DOWN = define("down", "Down", Key.DOWN.noMods())
    val MOVE_UP = define("move_up", "Move Up", Key.UP.control())
    val MOVE_DOWN = define("move_down", "Move Down", Key.DOWN.control())

}

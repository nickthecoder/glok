package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.VAlignment
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.THREE_ROW
import uk.co.nickthecoder.glok.util.*

/**
 * Lays out up to 3 children in a single row (or column), where the [center] node is guaranteed to be centered.
 * [left] and [right] Nodes are aligned left and right.
 *
 * All nodes are given their prefHeight, and their vertical alignment is
 * determined by [vAlignment].
 *
 * [center]'s [Node.shrinkPriority] determines which nodes shrink if there is insufficient room in the X direction.
 *
 *   Zero -> [center] is allocated its pref width. [left] and [right] may shrink.
 *   Non-Zero -> [left] and [right] are allocated their pref width. [center] may shrink.
 *
 * If [center]'s [Node.shrinkPriority] != 0f, then [left] and [right] are given their `prefWidth`,
 * and [center] may shrink if there isn't sufficient space.
 *
 * Each child's [Node.growPriority] determines if the child is given the full height, or their preferred height.
 *
 * [ThreeRow] originally had no [orientation], and the ability to be vertical was added later.
 */
class ThreeRow : Region() {

    override val children: MutableObservableList<Node> = mutableListOf<Node>().asMutableObservableList()

    // region ==== Properties ====

    val orientationProperty by orientationProperty(Orientation.HORIZONTAL)
    var orientation by orientationProperty

    val leftProperty by optionalNodeProperty(null)
    var left by leftProperty

    val centerProperty by optionalNodeProperty(null)
    var center by centerProperty

    val rightProperty by optionalNodeProperty(null)
    var right by rightProperty

    /**
     * An alias for [leftProperty], useful for when [orientation] = `VERTICAL`.
     */
    val topProperty = leftProperty

    /**
     * An alias for [rightProperty], useful for when [orientation] = `VERTICAL`.
     */
    val bottomProperty = rightProperty

    /**
     * An alias for [left], useful for when [orientation] = `VERTICAL`.
     */
    var top by leftProperty

    /**
     * An alias for [right], useful for when [orientation] = `VERTICAL`.
     */
    var bottom by rightProperty

    val spacingProperty by stylableFloatProperty(0f)
    var spacing by spacingProperty

    /**
     * Only used when [orientation] = `HORIZONTAL`
     */
    val vAlignmentProperty by stylableVAlignmentProperty(VAlignment.CENTER)
    var vAlignment by vAlignmentProperty

    /**
     * Only used when [orientation] = `VERTICAL`
     */
    val hAlignmentProperty by stylableHAlignmentProperty(HAlignment.CENTER)
    var hAlignment by hAlignmentProperty

    // endregion properties

    // region ==== Init ====

    init {
        style(THREE_ROW)
        orientationProperty.addListener(requestLayoutListener)
        vAlignmentProperty.addListener(requestLayoutListener)

        /*
         * When we change any of [center],  [right], or [left],
         * ensure that [children] list is updated.
         */
        centerProperty.addChangeListener { _, old, new ->
            old?.let { children.remove(it) }
            new?.let { children.add(it) }
        }.also {
            leftProperty.addChangeListener(it)
            rightProperty.addChangeListener(it)
        }

        children.addChangeListener(childrenListener)
    }

    // ==== End if init ====

    private fun row() = listOfVisible(left, center, right)
    private fun leftAndRight() = listOfVisible(left, right)


    override fun nodeMinHeight() = if (orientation == Orientation.HORIZONTAL) {
        surroundY() + row().maxMinHeight()
    } else {
        val forLeftAndRight = leftAndRight().totalMinHeight()
        val forCenter = center?.evalMinHeight() ?: 0f
        val forSpacing = if (center?.visible != false) {
            if (leftAndRight().isEmpty()) 0f else spacing
        } else {
            if (leftAndRight().isEmpty()) 0f else spacing * 2
        }
        surroundY() + forLeftAndRight + forCenter + forSpacing
    }

    override fun nodeMinWidth() = if (orientation == Orientation.VERTICAL) {
        surroundX() + row().maxMinHeight()
    } else {
        val forLeftAndRight = leftAndRight().totalMinWidth()
        val forCenter = center?.evalMinWidth() ?: 0f
        val forSpacing = if (center?.visible != false) {
            if (leftAndRight().isEmpty()) 0f else spacing
        } else {
            if (leftAndRight().isEmpty()) 0f else spacing * 2
        }
        surroundY() + forLeftAndRight + forCenter + forSpacing
    }

    override fun nodePrefHeight() = if (orientation == Orientation.HORIZONTAL) {
        surroundY() + row().maxPrefHeight()
    } else {
        val forLeftAndRight = leftAndRight().totalPrefHeight()
        val forCenter = center?.evalPrefHeight() ?: 0f
        val forSpacing = if (center?.visible != false) {
            if (leftAndRight().isEmpty()) 0f else spacing
        } else {
            if (leftAndRight().isEmpty()) 0f else spacing * 2
        }
        surroundY() + forLeftAndRight + forCenter + forSpacing
    }

    override fun nodePrefWidth() = if (orientation == Orientation.VERTICAL) {
        surroundX() + row().maxPrefWidth()
    } else {
        val forLeftAndRight = leftAndRight().totalPrefWidth()
        val forCenter = center?.evalPrefWidth() ?: 0f
        val forSpacing = if (center?.visible != false) {
            if (leftAndRight().isEmpty()) 0f else spacing
        } else {
            if (leftAndRight().isEmpty()) 0f else spacing * 2
        }
        surroundY() + forLeftAndRight + forCenter + forSpacing
    }

    override fun layoutChildren() {
        if (orientation == Orientation.HORIZONTAL) {
            layoutHorizontal()
        } else {
            layoutVertical()
        }
    }

    private fun layoutHorizontal() {
        val width = width
        val height = height

        val availableX = width - surroundX()
        val availableY = height - surroundY()

        val availableCenter = availableX - 2 * max(
            if (left?.visible == true) (left !!.evalPrefWidth() + spacing) else 0f,
            if (right?.visible == true) (right !!.evalPrefWidth() + spacing) else 0f
        )
        var nextMax = availableX

        center?.let { child ->
            val prefWidth = child.evalPrefWidth()
            val prefHeight = child.evalPrefHeight()
            val w = if (child.shrinkPriority == 0f) min(availableX, prefWidth) else availableCenter
            val h = if (child.growPriority == 0f) min(availableY, prefHeight) else availableY
            val x = surroundLeft() + (availableX - w) / 2
            val y = surroundTop() + when (vAlignment) {
                VAlignment.TOP -> 0f
                VAlignment.BOTTOM -> availableY - h
                VAlignment.CENTER -> (availableY - h) / 2
            }
            setChildBounds(child, x, y, w, h)
            nextMax = availableX - w / 2 - spacing
        }

        left?.let { child ->
            val prefWidth = child.evalPrefWidth()
            val prefHeight = child.evalPrefHeight()
            val w = min(prefWidth, nextMax)
            val h = if (child.growPriority == 0f) min(availableY, prefHeight) else availableY
            val y = surroundTop() + when (vAlignment) {
                VAlignment.TOP -> 0f
                VAlignment.BOTTOM -> availableY - h
                VAlignment.CENTER -> (availableY - h) / 2
            }
            setChildBounds(child, surroundLeft(), y, w, h)
            if (center == null) {
                nextMax = availableX - spacing - w
            }
        }

        right?.let { child ->
            val prefWidth = child.evalPrefWidth()
            val prefHeight = child.evalPrefHeight()
            val w = min(prefWidth, nextMax)
            val h = if (child.growPriority == 0f) min(availableY, prefHeight) else availableY
            val y = surroundTop() + when (vAlignment) {
                VAlignment.TOP -> 0f
                VAlignment.BOTTOM -> availableY - h
                VAlignment.CENTER -> (availableY - h) / 2
            }
            setChildBounds(child, surroundLeft() + availableX - w, y, w, h)
        }
    }

    private fun layoutVertical() {
        val height = height
        val width = width

        val availableY = height - surroundY()
        val availableX = width - surroundX()

        val availableCenter = availableY - 2 * max(
            if (top?.visible == true) (top !!.evalPrefHeight() + spacing) else 0f,
            if (bottom?.visible == true) (bottom !!.evalPrefHeight() + spacing) else 0f
        )
        var nextMax = availableY

        center?.let { child ->
            val prefHeight = child.evalPrefHeight()
            val prefWidth = child.evalPrefWidth()
            val h = if (child.shrinkPriority == 0f) min(availableY, prefHeight) else availableCenter
            val w = if (child.growPriority == 0f) min(availableX, prefWidth) else availableX
            val y = surroundTop() + (availableY - h) / 2
            val x = surroundLeft() + when (hAlignment) {
                HAlignment.LEFT -> 0f
                HAlignment.RIGHT -> availableX - w
                HAlignment.CENTER -> (availableX - w) / 2
            }
            setChildBounds(child, x, y, w, h)
            nextMax = availableY - h / 2 - spacing
        }

        top?.let { child ->
            val prefHeight = child.evalPrefHeight()
            val prefWidth = child.evalPrefWidth()
            val h = min(prefHeight, nextMax)
            val w = if (child.growPriority == 0f) min(availableX, prefWidth) else availableX
            val x = surroundLeft() + when (hAlignment) {
                HAlignment.LEFT -> 0f
                HAlignment.RIGHT -> availableX - w
                HAlignment.CENTER -> (availableX - w) / 2
            }
            setChildBounds(child, x, surroundTop(), w, h)
            if (center == null) {
                nextMax = availableY - spacing - h
            }
        }

        bottom?.let { child ->
            val prefHeight = child.evalPrefHeight()
            val prefWidth = child.evalPrefWidth()
            val h = min(prefHeight, nextMax)
            val w = if (child.growPriority == 0f) min(availableX, prefWidth) else availableX
            val x = surroundLeft() + when (hAlignment) {
                HAlignment.LEFT -> 0f
                HAlignment.RIGHT -> availableX - w
                HAlignment.CENTER -> (availableX - w) / 2
            }
            setChildBounds(child, x, surroundTop() + availableY - h, w, h)
        }

    }

}

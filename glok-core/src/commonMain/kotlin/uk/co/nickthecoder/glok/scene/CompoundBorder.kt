/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

/**
 * A [Border] composed of two other [Border]s.
 *
 * This is used in the `ColorPicker` for the alpha slider.
 * Replace the flat-color [PlainBackground], with a compound of the original, as a [RepeatedBackground]
 * with a checkered image.
 */
class CompoundBorder(val a: Border, val b: Border) : Border {
    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        a.draw(x, y, width, height, color, size)
        b.draw(
            x, y, width, height, color,
            Edges(size.top / 2, size.right / 2, size.bottom / 2, size.left / 2)
        )
    }
}

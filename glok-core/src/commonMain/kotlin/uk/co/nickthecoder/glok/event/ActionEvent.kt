package uk.co.nickthecoder.glok.event

class ActionEvent : Event() {
    override val eventType get() = EventType.ACTION
}

interface ActionEventHandler : EventHandler<ActionEvent>

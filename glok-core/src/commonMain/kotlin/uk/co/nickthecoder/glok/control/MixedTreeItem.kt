/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.asReadOnly
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.util.max

abstract class MixedTreeItem<I : MixedTreeItem<I>> : TreeItemBase() {

    internal var treeView: MixedTreeView<I>? = null
        set(v) {
            if (v !== field) {
                field = v
                for (child in children) {
                    child.treeView = v
                }
            }
        }

    private val privateParentProperty = SimpleProperty<MixedTreeItem<I>?>(null, MixedTreeItem::class)
    val parentProperty = privateParentProperty.asReadOnly()
    var parent: MixedTreeItem<I>? by privateParentProperty
        internal set

    val children = mutableListOf<I>().asMutableObservableList()

    private val childrenListener = children.addChangeListener { _, change ->
        for (removed in change.removed) {
            removed.parent = null
            removed.level = - 1
            removed.treeView = null
            // If we aren't expanded, then removing items doesn't affect the flattened items.
            if (expanded) {
                treeView?.removed(removed)
            }
        }
        for (added in change.added) {
            added.parent = this
            added.level = level + 1
            added.treeView = treeView
            // If we aren't expanded, then adding items doesn't affect the flattened items.
            if (expanded) {
                @Suppress("UNCHECKED_CAST")
                treeView?.added(added, this as I?)
            }
        }
        treeView?.requestLayout()
    }

    // region ==== init ====

    init {
        parentProperty.addChangeListener { _, _, parent ->
            availableProperty.unbind()
            if (parent == null) {
                available = true
            } else {
                availableProperty.bindTo(parent.availableProperty and parent.expandedProperty)
            }
        }

        expandedProperty.addChangeListener { _, _, expanded ->
            val treeView = treeView
            if (available && treeView != null) {
                if (expanded) {
                    treeView.expand(this)
                } else {
                    treeView.contract(this)
                }
                treeView.requestLayout()
            }
        }

        levelProperty.addChangeListener { _, _, level ->
            for (child in children) {
                child.level = level + 1
            }
        }

    }

    // endregion init

    abstract fun createCell(treeView: MixedTreeView<I>): MixedTreeCell<I>


    /**
     * Expands this item, and all ancestors
     */
    fun expandUpwards() {
        expanded = true
        parent?.expandUpwards()
    }

    /**
     * Expands this item, and all descendants.
     *
     * BEWARE. It is generally a bad idea to call on large trees!
     */
    fun expandDownwards() {
        expanded = true
        for (child in children) {
            expandDownwards()
        }
    }

    /**
     * Return true iff this MixedTreeItem is a descendantOf [other], or they are the same MixedTreeItem.
     */
    fun isDescendantOf(other: MixedTreeItem<I>): Boolean {
        var ancestor: MixedTreeItem<I>? = this
        while (ancestor != null) {
            if (ancestor == other) return true
            ancestor = ancestor.parent
        }
        return false
    }

    fun dumpItems() {
        println("    ".repeat(max(0, level)) + this)
        for (child in children) {
            child.dumpItems()
        }
    }

    override fun toString(): String {
        val parentString = if (parent == null) if (treeView?.root === this) "" else " **parent not set** " else ""
        val levelString = if (level < 0) "**level not set** " else "level=$level"
        return "${this::class.simpleName} $parentString $levelString expanded=$expanded leaf=$leaf childCount=${children.size}"
    }
}

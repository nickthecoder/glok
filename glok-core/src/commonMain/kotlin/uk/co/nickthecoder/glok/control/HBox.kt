/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.WithChildren
import uk.co.nickthecoder.glok.util.toString

/**
 * Lays out its [children] in a row, with the first child on the left,
 * and the last child on the right.
 *
 * Glok includes an application `TestHBox` that you can play with, if you download the source code.
 *
 * ### `fillHeight : true`
 *
 *     ┌──────────────────────────┐
 *     │ ┌───┐ ┌────────┐ ┌──┐    │
 *     │ │   │ │        │ │  │    │
 *     │ │   │ │        │ │  │    │
 *     │ │   │ │        │ │  │    │
 *     │ │   │ │        │ │  │    │
 *     │ │   │ │        │ │  │    │
 *     │ └───┘ └────────┘ └──┘    │
 *     └──────────────────────────┘
 *
 * ### fillHeight : `false`
 *
 * VAlignment.BOTTOM :
 *
 *     ┌──────────────────────────┐
 *     │ ┌───┐                    │
 *     │ │   │                    │
 *     │ │   │            ┌──┐    │
 *     │ │   │            │  │    │
 *     │ │   │ ┌────────┐ │  │    │
 *     │ │   │ │        │ │  │    │
 *     │ └───┘ └────────┘ └──┘    │
 *     └──────────────────────────┘
 *
 * VAlignment.TOP (the default) :
 *
 *     ┌──────────────────────────┐
 *     │ ┌───┐ ┌────────┐ ┌──┐    │
 *     │ │   │ │        │ │  │    │
 *     │ │   │ └────────┘ │  │    │
 *     │ │   │            │  │    │
 *     │ │   │            └──┘    │
 *     │ │   │                    │
 *     │ └───┘                    │
 *     └──────────────────────────┘
 *
 * VAlignment.CENTER :
 *
 *     ┌──────────────────────────┐
 *     │ ┌───┐                    │
 *     │ │   │            ┌──┐    │
 *     │ │   │ ┌────────┐ │  │    │
 *     │ │   │ │        │ │  │    │
 *     │ │   │ └────────┘ │  │    │
 *     │ │   │            └──┘    │
 *     │ └───┘                    │
 *     └──────────────────────────┘
 *
 *
 * Then for each of these, we also align horizontally according to [alignment]'s [Alignment.hAlignment].
 *
 * When [fillHeight] == `true`, the children's height is set to this [HBox]'s height minus its [padding].
 * In all cases, the children's heights are set to their [evalPrefHeight].
 *
 * Note, in these diagrams, there is additional space to the right of the last child.
 * This is either because the children set their [growPriority] to 0,
 * or they reached their [evalMaxWidth].
 * Otherwise, the children would have filled the [HBox] horizontally.
 *
 */
class HBox : BoxBase(), WithChildren {

    // region ==== Properties ====

    val fillHeightProperty by stylableBooleanProperty(false)
    var fillHeight by fillHeightProperty

    // endregion Properties

    override val fill: Boolean get() = fillHeight
    override val orientation get() = Orientation.HORIZONTAL

    override val children = mutableListOf<Node>().asMutableObservableList()

    // region ==== init ====

    init {
        children.addChangeListener(childrenListener)
        fillHeightProperty.addListener(requestLayoutListener)
    }

    // endregion init


    // region ==== Object methods ====

    override fun toString() = super.toString() + " fillHeight($fillHeight)"

    // endregion
}

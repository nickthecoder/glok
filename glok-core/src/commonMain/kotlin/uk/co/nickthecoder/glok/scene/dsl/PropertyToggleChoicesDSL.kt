package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.scene.Node

class PropertyToggleChoicesDSL<V, P : Property<V?>>(val property: P) {

    fun propertyToggleButton(
        value: V, text: String,
        block: (PropertyToggleButton<V, P>.() -> Unit)? = null
    ) = PropertyToggleButton(property, value, text).optionalApply(block)

    fun propertyToggleButton(
        value: V, text: String,
        graphic: Node?,
        block: (PropertyToggleButton<V, P>.() -> Unit)? = null
    ) = PropertyToggleButton(property, value, text, graphic).optionalApply(block)

    fun propertyToggleMenuItem(value: V, text: String, block: (PropertyToggleMenuItem<V, P>.() -> Unit)? = null) =
        PropertyToggleMenuItem(property, value, text).optionalApply(block)
}

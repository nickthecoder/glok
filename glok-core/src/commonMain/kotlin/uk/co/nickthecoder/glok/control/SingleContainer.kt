/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.LazyObservableValue
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.SINGLE_CONTAINER
import uk.co.nickthecoder.glok.util.min

/**
 * A container with one child, determined by [contentProperty].
 *
 * It may not be obvious at first, why this is so useful.
 * If part of your scene-graph is dynamic, and changes whenever a property (or more than one property)
 * changes, then consider using a SingleContainer, and binding its [contentProperty] to :
 *
 * * [SimpleOptionalNodeProperty] (if you want to update date it _manually_)
 * * [OptionalNodeUnaryFunction] (if it depends on a single property)
 * * [OptionalNodeBinaryFunction] (if is depends on two properties)
 * * [OptionalNodeTernaryFunction] (if is depends on three properties)
 *
 * For more than 3 dependencies, you could add additional dependencies to [OptionalNodeTernaryFunction]
 * using [LazyObservableValue.addDependent].
 *
 * For examples of SingleContainer look at the source code for `TabPane`, `NodeInspector` or `DemoChoices`.
 */
open class SingleContainer(initialNode: Node? = null) : Region(), WithContent {

    constructor(contentProperty: ObservableOptionalNode) : this(contentProperty.value) {
        this.contentProperty.bindTo(contentProperty)
    }

    // region ==== Properties ====
    val contentProperty by optionalNodeProperty(null)
    final override var content by contentProperty

    /**
     * When true, [visibleProperty] is bound to [content]'s `visibleProperty`.
     * and when the [content] is null, [visibleProperty] is set to false.
     *
     * The default value is false.
     *
     * The primary reason for this feature is when [SingleContainer] in used in conjunction with [SplitPane],
     * The [SplitPane] won't show superfluous dividers when the _real_ content is null, or not visible.
     */
    val bindVisibleProperty by booleanProperty(false)
    var bindVisible by bindVisibleProperty

    /**
     * If true, then the width of [content] is the full height of the container
     * (minus border and padding).
     * Otherwise, the [content] is allocated at most its `prefWidth`, and is positioned
     * according to [alignment].
     */
    val fillWidthProperty by booleanProperty(true)
    var fillWidth by fillWidthProperty

    /**
     * If true, then the height of [content] is the full extent of the container.
     * (minus border and padding).
     * Otherwise, the [content] is allocated at most its `prefHeight`, and is positioned
     * according to [alignment].
     */
    val fillHeightProperty by booleanProperty(true)
    var fillHeight by fillHeightProperty

    /**
     * If this Node has more space
     */
    val alignmentProperty by alignmentProperty(Alignment.TOP_LEFT)
    var alignment by alignmentProperty

    // endregion

    // region ==== Fields ====
    val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    final override val children = mutableChildren.asReadOnly()

    // endregion

    init {
        style(SINGLE_CONTAINER)

        contentProperty.addChangeListener { _, _, new ->
            if (bindVisible) {
                visibleProperty.unbind()
            }
            mutableChildren.clear()
            if (new == null) {
                if (bindVisible) {
                    visible = false
                }
            } else {
                if (bindVisible) {
                    visibleProperty.bindTo(new.visibleProperty)
                }
                mutableChildren.add(new)
            }
            requestLayout()
        }

        bindVisibleProperty.addChangeListener { _, _, new ->
            if (new) {
                if (content == null) {
                    visible = false
                }
                content?.let {
                    visibleProperty.bindTo(it.visibleProperty)
                }
            }
        }

        mutableChildren.addChangeListener(childrenListener)
        content = initialNode

        if (bindVisible && initialNode != null) {
            visibleProperty.bindTo(initialNode.visibleProperty)
        }

        for (prop in listOf(fillWidthProperty, fillHeightProperty, alignmentProperty)) {
            prop.addListener(requestLayoutListener)
        }
    }

    override fun nodeMinWidth() = surroundX() + (content?.evalMinWidth() ?: 0f)
    override fun nodeMinHeight() = surroundY() + (content?.evalMinHeight() ?: 0f)

    override fun nodePrefWidth() = surroundX() + (content?.evalPrefWidth() ?: 0f)
    override fun nodePrefHeight() = surroundY() + (content?.evalPrefHeight() ?: 0f)

    override fun layoutChildren() {
        content?.let { content ->
            val availableX = width - surroundX()
            val availableY = height - surroundY()

            val newWidth = if (fillWidth) {
                availableX
            } else {
                min(availableX, content.evalPrefWidth())
            }
            val newHeight = if (fillHeight) {
                availableY
            } else {
                min(availableY, content.evalPrefHeight())
            }

            val x = surroundLeft() + when (alignment.hAlignment) {
                HAlignment.LEFT -> 0f
                HAlignment.RIGHT -> availableX - newWidth
                HAlignment.CENTER -> (availableX - newWidth) / 2
            }
            val y = surroundTop() + when (alignment.vAlignment) {
                VAlignment.TOP -> 0f
                VAlignment.BOTTOM -> availableY - newHeight
                VAlignment.CENTER -> (availableY - newHeight) / 2
            }

            setChildBounds(content, x, y, newWidth, newHeight)
        }
    }

    override fun toString() = super.toString() + " $alignment fillWidth=$fillWidth fillHeight=$fillHeight"
}

package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.PropertyRadioButton
import uk.co.nickthecoder.glok.control.PropertyRadioMenuItem
import uk.co.nickthecoder.glok.control.ToggleGroup
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.BorderBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ColorProperty
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.NoBackground
import uk.co.nickthecoder.glok.scene.NoBorder
import uk.co.nickthecoder.glok.scene.RoundedBorder
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Tantalum.fontColorProperty
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON2

class PropertyRadioChoicesDSL<V, P : Property<V>>(val property: P) {
    val toggleGroup = ToggleGroup()

    /**
     * A [PropertyRadioButton].
     * [Tantalum] renders this with a circle, which is filled when selected.
     *
     * See [propertyRadioButton2].
     */
    fun propertyRadioButton(
        value: V, text: String,
        block: (PropertyRadioButton<V, P>.() -> Unit)? = null
    ) = PropertyRadioButton(property, value, text).optionalApply(block)

    /**
     * A [PropertyRadioButton].
     * [Tantalum] renders this like a [ToggleButton] i.e. a button with a solid background.
     * The `selected` radio button will use the `accent` color for its background.
     * The others will use a grey background.
     *
     * See [propertyRadioButton].
     */
    fun propertyRadioButton2(
        value: V, text: String,
        block: (PropertyRadioButton<V, P>.() -> Unit)? = null
    ) = PropertyRadioButton(property, value, text).apply {
        styles.remove(RADIO_BUTTON)
        styles.add(RADIO_BUTTON2)
    }.optionalApply(block)

    fun propertyRadioMenuItem(value: V, text: String, block: (PropertyRadioMenuItem<V, P>.() -> Unit)? = null) =
        PropertyRadioMenuItem(property, value, text).optionalApply(block)

}

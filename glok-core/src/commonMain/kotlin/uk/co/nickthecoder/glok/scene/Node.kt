package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.*
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.StylableProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.observableFalse
import uk.co.nickthecoder.glok.property.functions.pixelsPerEm
import uk.co.nickthecoder.glok.property.functions.times
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.Selector
import uk.co.nickthecoder.glok.theme.Styled
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.theme.ValueAndImportance
import uk.co.nickthecoder.glok.theme.styles.FOCUSED
import uk.co.nickthecoder.glok.util.*

/**
 * The base class for all GUI elements in a [Scene].
 * The nodes form a `Scene Graph` (which is a misnomer, because it is a `tree` structure, not a `graph`),
 * starting at the [Scene.root] Node.
 *
 * Each Node may only appear once (i.e. you cannot use the same node in two place, or in two scenes).
 *
 * See [Scene] for a Class Diagram.
 *
 * Nodes are arranged in a tree structure, starting at [Scene.root].
 * All nodes may have 0 or more child Nodes (see [children]).
 * However, [children] is often private.
 *
 * Any changes of a Node's state which change its appearance must call [requestRedraw].
 * The redrawing doesn't happen immediately, it happens once at the end of a `frame`.
 *
 * Any changes of a Node's state which affects its size must call [requestLayout].
 * Again, the recalculation of the node's size and position aren't done immediately,
 * but at the end of a `frame`.
 *
 * `frame` is in quotes, because unlike a game, the screen isn't being continuously redrawn.
 * If nothing needs to be drawn, then the CPU/GPU is idle.
 *
 * Glok never attempts to redraw only parts of a scene, so even a blinking caret causes
 * a complete redraw of the scene. This may sound inefficient, but considering that games
 * often run at 60fps, updating at 1 or 2 fps isn't taxing.
 *
 * For expensive nodes (such as a view in a 3D CAD program),
 * the node could draw to an off-screen buffer whenever it is _stale_,
 * and then simply draw that off-screen buffer to the screen within [draw].
 *
 * ## Position and Size
 *
 *       (0,0)
 *         ┌────────────────────────────────────────▶ x
 *         │
 *         │        (sceneX,sceneY)
 *         │               ┌────────────────┐   ▲
 *         │               │                │   │
 *         │               │      node      │height
 *         │               │                │   │
 *         │               │                │   │
 *         │               └────────────────┘   ▼
 *         ▼               ◀──────width─────▶
 *         y
 *
 * Every node has an axis aligned bounding defined by its top left corner [sceneX], [sceneY]
 * and its [width] and [height].
 *
 * The coordinate system has the Y axis pointing downwards. i.e. (0,0) is at the top left.
 *
 * Nodes are not positioned nor sized directly by the application developer.
 * Instead, each node's parent decides its children's position and size.
 * The only exception to this is the root node, [Scene.root], which is always positioned at `(0,0)`
 * and is given the size of the window's client area.
 * (i.e. the size of the window, excluding the title bar and borders).
 *
 * Every node calculates its size requirements via the methods :
 *
 * [nodeMinWidth] [nodePrefWidth] [nodeMaxWidth] [nodeMinHeight] [nodePrefHeight] [nodeMaxHeight]
 *
 * In addition, every node can ignore the calculations above, and instead use these properties :
 *
 * [overrideMinWidth] [overridePrefWidth] [overrideMaxWidth] [overrideMinHeight] [overridePrefHeight] [overrideMaxHeight]
 *
 * These are usually `null`. If you use these a lot, it might be an indication that you are doing something wrong.
 * Notable exceptions are for heights for nodes such as `ListView`, `TreeView` and `MixedTreeView`, which don't have a
 * natural height.
 *
 * When you want to interrogate a node's size requires, don't use any of the above and instead use :
 *
 * [evalMinWidth] [evalPrefWidth] [evalMaxWidth] [evalMinHeight] [evalPrefHeight] [evalMaxHeight]
 *
 * These are one-liners which return either the `override` if it is set, or otherwise the calculated size.
 */
abstract class Node : Styled {

    // region ==== Properties ====

    /**
     * Supposedly unique identifier (or blank).
     * However, it is up to the application developer to ensure the id is unique.
     *
     *  See [Scene.findNodeById] and Node.[findNodeById]
     *
     * This id can be used as a selector within a [Theme]. For example, if this id is `myNamedNode`, then
     * a rule within a theme DSL may look like this :
     *
     *    "#myNamedNode" {
     *        padding( 10 )
     *    }
     *
     */
    val idProperty by stringProperty("")
    var id by idProperty

    /**
     * The [Scene] this node belongs to, or `null`, if isn't part of a scene.
     */
    private val _sceneProperty by optionalSceneProperty(null)
    val sceneProperty = _sceneProperty.asReadOnly()
    var scene by _sceneProperty
        internal set

    /**
     * The parent Node. This will be `null` before the node is added to a scene's tree of nodes,
     * and also for the [Scene.root].
     *
     * Note, a node cannot be used in two places within a scene (or within 2 different scenes).
     * If you attempt to add this node as a child to another node, an exception is thrown.
     *
     * See [children].
     */
    private val _parentProperty by optionalNodeProperty(null)
    val parentProperty = _parentProperty.asReadOnly()
    var parent by _parentProperty
        internal set

    /**
    On a scale of 0..1, if this Node's parent has extra space compared to its preferred size,
    how much extra space should be give to this [Node] relative to its siblings?

    0 = Never stretch. 1 = Take as much space as possible.

    e.g. If two children have a growPriority of 1, and the parent's allotted size is 12 pixels wider than the
    children's width, then both will be given an extra 6 pixels.

    e.g.2 If one child has a priority of 1, and the other has a priority of 0.5, then the 12 pixels will be shared
    in a 2:1 ratio i.e. child#1 gets 8 pixels extra, and child#2 gets 4.

    The default is 0. So Nodes tend not to grow beyond their preferred size.
     */
    val growPriorityProperty by stylableFloatProperty(0f)
    var growPriority: Float by growPriorityProperty

    /**
    On a scale of 0..1, if this Node's parent has less space compared to its preferred size,
    how much should this node shrink by compared to its siblings.

    0 = Never shrink. 1 = Shrink as much as possible

    The default is 0. So Nodes tend not to shrink below their preferred size.
    */
    val shrinkPriorityProperty by stylableFloatProperty(0f)
    var shrinkPriority: Float by shrinkPriorityProperty


    /**
    A simple way to override the value calculated by [nodeMinWidth].
    Most of the time, this value is `null`, and therefore [nodeMinWidth] is used.

    See [evalMinWidth]
     */
    val overrideMinWidthProperty by stylableOptionalFloatProperty(null)
    var overrideMinWidth: Float? by overrideMinWidthProperty

    /**
    A simple way to override the value calculated by [nodeMinHeight].
    Most of the time, this value is `null`, and therefore [nodeMinHeight] is used.

    See [evalMinHeight]
     */
    val overrideMinHeightProperty by stylableOptionalFloatProperty(null)
    var overrideMinHeight by overrideMinHeightProperty

    /**
    A simple way to override the value calculated by [nodeMinWidth].
    Most of the time, this value is `null`, and therefore [nodeMinWidth] is used.

    See [evalMinWidth]
     */
    val overridePrefWidthProperty by stylableOptionalFloatProperty(null)
    var overridePrefWidth by overridePrefWidthProperty

    /**
    A simple way to override the value calculated by [nodePrefHeight].
    Most of the time, this value is `null`, and therefore [nodePrefHeight] is used.

    See [evalPrefHeight]
     */
    val overridePrefHeightProperty by stylableOptionalFloatProperty(null)
    var overridePrefHeight by overridePrefHeightProperty

    /**
    See [evalMaxWidth].
    */
    val overrideMaxWidthProperty by stylableOptionalFloatProperty(null)
    var overrideMaxWidth by overrideMaxWidthProperty

    /**
    See [evalMaxHeight].
    */
    val overrideMaxHeightProperty by stylableOptionalFloatProperty(null)
    var overrideMaxHeight by overrideMaxHeightProperty


    private val mutableWidthProperty by floatProperty(0f)
    val widthProperty = mutableWidthProperty.asReadOnly()

    /**
    The actual width of this node, in [LogicalPixels].

    The position and size of Nodes are assigned by their parent in [layout], using [setChildBounds].
    The parent node uses any of : [evalMinWidth], [evalPrefWidth] and [evalMaxWidth] and its own logic
    to determine the actual width of this Node.

    To specify a fixed width for this node, you may set [overridePrefWidth].
    [evalPrefWidth] will ignore [nodePrefWidth] and return [overridePrefWidth] instead.

    However, this _still_ does not guarantee the node will be the size requested
    (e.g. if the parent node isn't big enough to accommodate its children's preferred sizes).
    */
    var width by mutableWidthProperty
        internal set

    private val mutableHeightProperty by floatProperty(0f)
    val heightProperty = mutableHeightProperty.asReadOnly()

    /**
    The actual height of this node, in [LogicalPixels].

    The position and size of Nodes are assigned by their parent in [layout], using [setChildBounds].
    The parent node uses any of : [evalMinHeight], [evalPrefHeight] and [evalMaxHeight] and its own logic
    to determine the actual width of this Node.

    To specify a fixed width for this node, you may set [overridePrefHeight].
    [evalPrefHeight] will ignore [nodePrefWidth] and return [overridePrefHeight] instead.

    However, this _still_ does not guarantee the node will be the size requested
    (e.g. if the parent node isn't big enough to accommodate its children's preferred sizes).
    */
    var height: Float by mutableHeightProperty
        internal set

    /**
    When `false`, this node will not appear in the scene, and will not take up any spaces.

    This is different to JavaFX, where invisible nodes _do_ take up space.
    */
    val visibleProperty by stylableBooleanProperty(true)
    var visible by visibleProperty

    /**
    Is this part of the cycle of node that can accept keyboard focus
    when he user presses the `Tab` key (or `Shift-Tab`).

    Focus can also be moved programmatically using [Scene.focusNext] and [Scene.focusPrevious].
    Any node can be given the focus using [requestFocus] (i.e. [focusTraversable] is ignored in this case).

    [Scene.focusOwner] indicates which node currently has focus, and that node's [focusedProperty] is set to true.

    See also [focusAcceptable]
    */
    val focusTraversableProperty by stylableBooleanProperty(false)
    var focusTraversable by focusTraversableProperty

    /**
    If a mouse is clicked on a node, should it request the focus?
    For controls which can be `read-only` (such as [TextArea] / [TextField]), we may want to jump over them
    when pressing `Tab` (i.e. [focusTraversable] = false), but if they are clicked, then they _should_
    accept input focus. The user may wish to select, and copy part of the text.

    Typically, when clicked, a node should navigate up the scene graph to find the first `focusAcceptable`
    Node. e.g. the up/down buttons in a `Spinner` should cause the `Spinner` to gain focus, not the button.
    Similarly to the `container` of a `TextArea`, the `container` should never have focus itself.
     */
    val focusAcceptableProperty by stylableBooleanProperty(false)
    var focusAcceptable by focusAcceptableProperty

    /**
     * To help navigating a scene using keyboard shortcuts (not using a mouse), it is helpful to jump
     * to different sections of the scene.
     * The keyboard shortcut `F6` is similar to `TAB`, but instead of moving one Node at a time, it
     * jumps to the next node, where this is `true`.
     * `Shift+F6` navigates in the opposite direction.
     *
     * Nodes which are sections : ToolBar, MenuBar, Dock, HarbourSide (when it contains Docks),
     * contents of a Tab.
     */
    val sectionProperty by stylableBooleanProperty(false)
    var section by sectionProperty

    internal val mutableFocusedProperty by booleanProperty(false)

    /**
    `true` when this node has the input focus.
    Only 1 node has the input focus.

    If the Stage loses focus, so will the focused node.
    When the Stage gains focus again, the node will regain focus too
    (assuming another node hasn't requested focus inbetween).
     */
    val focusedProperty = mutableFocusedProperty.asReadOnly()
    var focused by mutableFocusedProperty
        internal set

    internal val focusedByKeyboardProperty by booleanProperty(false)
    internal var focusedByKeyboard by focusedByKeyboardProperty

    /**
    Defines a popup that appears when the user hover the mouse over this node for a period of time.
    Typically, containing help text and a [KeyCombination.displayText] (if this node can be activated
    using a keyboard shortcut).
    */
    val tooltipProperty by optionalTooltipProperty(null)
    var tooltip by tooltipProperty

    /**
    An ObservableOptionalFont, whose value is [Theme.defaultFont].
    To access this is quite convoluted, because both [Node.scene] and [Scene.theme] can change.

    Therefore, this is lazily evaluated, and in the vast majority of cases, it is never evaluated.

    Used by the [em] function to convert from `ems` to `pixels` for nodes which do not have a [Font] property.
    */
    val scenesFontProperty: ObservableOptionalFont by lazy { ScenesFont(sceneProperty) }

    // endregion Properties

    // region ==== Fields ====
    /**
    Used by the [Theme] to determine how this node should be styled.
    Similar in concept to CSS's `class` attribute.

    By convention, styles which represent the node's class, using the class name in all lower case,
    with underscores between words. e.g. `split_pane`.

    Other styles begin with a period. e.g. `.information`.

    Usually, styles are set when the node is first created, and remained unchanged thereafter.

    If a style is added or removed, then all rules within the [Theme] are checked to see if they match
    this node, and all descendant nodes.
    */
    val styles = mutableSetOf<String>().asMutableObservableSet()

    /**
    Similar to [styles], but used for temporary styles, such as `:hover`.

    By convention, all pseudoStyles begin with a colon (similar to CSS).

    If a pseudoStyle is added or removed, this node and all descendant nodes are restyled
    from a cached subset of the [Theme]s rules. This is quicker than the restyling when
    [styles] are changed.
    */
    val pseudoStyles = mutableSetOf<String>().asMutableObservableSet()

    /**
    The base class [Node] has no children, so this is an empty list.

    Many subclasses (e.g. [HBox], [VBox]) expose [children] as a [MutableObservableList],
    which means you can modify the children directly.

    More complex subclasses (such as [ToolBar], [BorderPane] and [ScrollPane]), [children] remains immutable.
    Nodes can be added to them in other ways. e.g. [ToolBar.items], [BorderPane.top],
    [ScrollPane.content].
    For these subclasses, the API lets you examine [children], but this isn't recommended,
    because the structure of the node may change. For example, in early versions of [ToolBar], the items
    in the toolBar were direct child nodes, but now they are grandchildren,
    with a `container` node placed in between.

    NOTE. There is no special interface for nodes which have children. There are very few node types
    which never have children. [Text] and [ImageView] are the only ones that come to mind.
    Even the humble [Label] can have a `graphic` as a child node.
    */
    open val children: ObservableList<Node> = NO_CHILDREN

    /**
    Ensures the `parent` <-> `child` relationship is correctly maintained,
    as well as the `scene` property.

    When the [children] list is modified :

    Removed nodes' [parent] and [scene] are set to null.
    Added nodes' [parent] is set to `this`, and [scene] is set to `this.scene`.
    */
    protected val childrenListener = object : ListChangeListener<Node> {
        override fun changed(list: ObservableList<Node>, change: ListChange<Node>) {
            for (removed in change.removed) {
                scene?.theme?.removedNode(removed)
                removed.parent = null
                removed.scene = null
            }
            for (added in change.added) {
                if (added.parent != null) {
                    throw GlokException("Node added as a child of two parent nodes. $added")
                }
                added.parent = this@Node
                added.scene = scene
                scene?.theme?.applyToNewNode(added)
            }
            requestLayout()
        }
    }

    private val mutableLocalXProperty by floatProperty(0f)
    val localXProperty = mutableLocalXProperty.asReadOnly()

    /**
    The position of this Node, relative to its [parent] Node in [LogicalPixels].
    The position and size of Nodes are assigned by their [parent] in [layout], using [setChildBounds].
    */
    var localX by mutableLocalXProperty
        internal set

    private val mutableLocalYProperty by floatProperty(0f)
    val localYProperty = mutableLocalYProperty.asReadOnly()

    /**
    The position of this Node, relative to its [parent] Node in [LogicalPixels].
    The position and size of Nodes are assigned by their [parent] in [layout], using [setChildBounds].
    */
    var localY by mutableLocalYProperty
        internal set


    private val mutableSceneXProperty by floatProperty(0f)
    val sceneXProperty = mutableSceneXProperty.asReadOnly()

    /**
    The X position of the Node relative to the top left of the window in [LogicalPixels].
    The position and size of Nodes are assigned by their [parent] in [layout], using [setChildBounds].

    NOTE. an [OverlayStage]'s scene uses coorinates relative to the [RegularStage].
    Therefore, the root node is NOT at (0,0).
    */
    var sceneX by mutableSceneXProperty
        internal set

    private val mutableSceneYProperty by floatProperty(0f)
    val sceneYProperty = mutableSceneYProperty.asReadOnly()

    /**
    The Y position of the Node relative to the top left of the window in [LogicalPixels].
    The position and size of Nodes are assigned by their [parent] in [layout], using [setChildBounds].

    NOTE. an [OverlayStage]'s scene uses coordinates relative to the [RegularStage].
    Therefore, the root node is NOT at (0,0).
    */
    var sceneY by mutableSceneYProperty
        internal set

    /**
    While performing [layoutChildren], prevent calls to [requestLayout]
    having any effect. See [requestLayout], and [layout].

    For example, a control may make one if its child [visible], if it
    doesn't fit within the bounds of the control.
     */
    private var allowRequestLayout = true

    /**
     * Add this listener to any properties which change the appearance of this node,
     * which does NOT change its size.
     * For example, color properties.
     *
     * See [requestLayoutListener]
     */
    protected val requestRedrawListener = invalidationListener { requestRedraw() }

    /**
     * Add this listener to any properties which change the size requirements of this node.
     * For example, borderSize, padding, font.
     */
    protected val requestLayoutListener = invalidationListener { requestLayout() }

    /**
     * Used by [cachedRuleSettings], [restyleFromCachedRules], [clearCachedRules].
     */
    internal class RuleCache(
        val selector: Selector,
        val styleOnlyPropertyToValue: Map<StylableProperty<*>, ValueAndImportance>,
        val propertyToValueMap: Map<StylableProperty<*>, ValueAndImportance>
    )

    /**
     * Holds rules from a [Theme] which *might* apply to this node, depending on
     * [pseudoStyles].
     * When [pseudoStyles] changes, [restyleFromCachedRules] applies (or removes)
     * property settings.
     * This cache should be small, as there are very few pseudo styles choices.
     * By using this cache, restyling is much quicker than looking through the
     * [Theme]'s entire set of rules.
     */
    internal val cachedRuleSettings = mutableListOf<RuleCache>()

    // endregion Fields

    // region ==== init ====
    init {
        mutableFocusedProperty.addChangeListener { _, _, isFocused ->
            pseudoStyleIf(isFocused, FOCUSED)
            if (! isFocused) {
                focusedByKeyboard = false
            }
        }

        idProperty.addListener { scene?.requestRestyling = true }
        styles.addListener { scene?.requestRestyling = true }
        pseudoStyles.addListener { restyleFromCachedRules() }

        _sceneProperty.addListener {
            for (child in children) {
                child.scene = scene
            }
        }

        arrayOf(
            visibleProperty, shrinkPriorityProperty, growPriorityProperty,
            overrideMinWidthProperty, overrideMinHeightProperty,
            overridePrefWidthProperty, overridePrefHeightProperty,
            overrideMaxWidthProperty, overrideMaxHeightProperty
        ).forEach { it.addListener(requestLayoutListener) }

    }
    // endregion init

    // region ==== Methods ====

    internal fun clearCachedRules() {
        cachedRuleSettings.clear()
    }

    /**
     * Sets/resets property values for rules based on [pseudoStyles].
     * This is designed to be quicker than restyling properties from scratch.
     * Alas, this means duplication of code. There is similar code in [Theme.applyToNewNode] and [Theme.collectedSettings].
     * What's worse, the duplicated code is different. Here, we reset properties which no longer have matching rules.
     * This isn't done when re-styling from scratch.
     * I haven't seen a real-world problem related to this, but it has been logged as a bug.
     */
    private fun restyleFromCachedRules() {
        // This might be called before the node is fully created, in which case, do nothing.
        if (scene == null) return

        val affectedProperties = mutableMapOf<StylableProperty<*>, ValueAndImportance>()
        val defaultValues = mutableMapOf<StylableProperty<*>, Any?>()

        for (ruleCache in cachedRuleSettings) {
            for ((property, valueAndImportance) in ruleCache.propertyToValueMap) {
                if (ruleCache.selector.matches(this)) {
                    val existing = affectedProperties[property]
                    if (existing == null || existing < valueAndImportance) {
                        affectedProperties[property] = valueAndImportance
                    }
                } else {
                    if (! affectedProperties.containsKey(property)) {
                        affectedProperties[property] = NO_MATCH
                        defaultValues[property] = ruleCache.styleOnlyPropertyToValue[property]?.value
                    }
                }
            }
        }

        for ((property, valueAndImportance) in affectedProperties) {
            if (valueAndImportance === NO_MATCH) {
                val defaultValue = defaultValues[property]
                if (defaultValue == null) {
                    property.revert()
                } else {
                    property.style(defaultValue)
                }
            } else {
                property.style(valueAndImportance.value)
            }
        }

        // Recurse downwards through the tree of nodes.
        // e.g. If a button's pseudo style changes, the graphic may also need to be restyled.
        for (child in children) {
            child.restyleFromCachedRules()
        }
    }

    /**
    Sets `[children].parent = this`.

    Most commonly used within a nodes constructor by parent nodes that have a fixed list of children.

    For nodes with dynamic children, add [childrenListener] to [children] instead
    (which sets/resets [parent] automatically).

    @throws GlokException if the child is owned by another node i.e. parent != null && parent != this
    */
    protected fun claimChildren() {
        for (child in children) {
            if (child.parent === this) continue
            if (child.parent != null) throw GlokException("Child is already has a parent : $child")
            child.parent = this
            child.scene = this.scene
        }
    }

    /**
    Converts `em` units to [LogicalPixels].
    */
    open fun Number.em(): ObservableOptionalFloat {
        return scenesFontProperty.pixelsPerEm() * this.toFloat()
    }

    /**
    Controls, such as Spinner, are focusTraversable, but one of their children needs to _appear_
    to have focus, when really it is the parent control which has focus.
    In such cases, bind the child's focus to the parent Node's focus.

    The [fakeFocusNode] must never attempt to be focusTraversable.
    To enforce this, its focusTraversableProperty is bound to [observableFalse].
     */
    protected fun fakeFocus(fakeFocusNode: Node) {
        // Make sure that the fake child never claims to be focusTraversable.
        fakeFocusNode.focusTraversableProperty.bindTo(observableFalse)
        fakeFocusNode.mutableFocusedProperty.bindTo(focusedProperty)
    }

    fun nextFocusableNode() = FocusTraversableWalker.next(this)

    fun previousFocusableNode() = FocusTraversableWalker.previous(this)

    fun nextSection() = FocusTraversableWalker.next(this, usingSections = true)

    fun previousSection() = FocusTraversableWalker.previous(this, usingSections = true)

    /**
     * Ensure that this node is visible, by asking all ancestor nodes, which are [Scrollable],
     * to scroll to this node. ScrollPanes will adjust their scroll bars, TabPanes will select the appropriate
     * tab, TitledPanes will open their content etc.
     */
    fun scrollTo() {
        forEachToRoot { ancestor ->
            if (ancestor is Scrollable) {
                ancestor.scrollTo(this)
            }
        }
    }

    /**
    Checks this node, and all descendants for a matching id.
    @return The first node which matches, or null if none match.
    */
    fun findNodeById(id: String): Node? {
        if (this.id == id) return this
        for (child in children) {
            val found = child.findNodeById(id)
            if (found != null) return found
        }
        return null
    }

    /**
    Visits this node, and all descendant nodes, performing an [action] on each.
    Each node is filtered. If [filter] returns false, [action] will not be called on that node,
    and its children will not be visited.
    */
    fun treeVisitor(filter: (Node) -> Boolean = { true }, action: (Node) -> Unit) {
        fun walk(node: Node) {
            if (filter(node)) {
                action(node)
            }
            for (child in node.children) {
                walk(child)
            }
        }
        walk(this)
    }

    /**
    Return true, if [other] === this, or this is a parent, grandparent etc. of [other].
    */
    fun isAncestorOf(other: Node): Boolean {
        return other.firstToRoot { it === this } === this
    }

    fun forEachDescending(block: (Node) -> Unit) {
        block(this)
        for (child in children) {
            child.forEachDescending(block)
        }
    }

    /**
    Applies a block of code on this node, and all of its ancestors, starting at the root node
    and working downwards to this node.
    */
    fun forEachFromRoot(block: (Node) -> Unit) {
        parent?.forEachFromRoot(block)
        block(this)
    }

    /**
    Applies a block of code on this node, and all of its ancestors, starting at this node
    and working up to the root node.
    */
    fun forEachToRoot(block: (Node) -> Unit) {
        var node: Node? = this
        while (node != null) {
            block(node)
            node = node.parent
        }
    }

    /**
    Tests each node from this one to the root node.
    When [filter] hold true, that node is returned, stopping the iteration.
    If none are found, returns null.
    */
    fun firstToRoot(filter: (Node) -> Boolean): Node? {
        var node: Node? = this
        while (node != null) {
            if (filter(node)) return node
            node = node.parent
        }
        return null
    }


    /**
    @return `this` if none of my children are at [sceneX,sceneY].
    Otherwise, the child (or one of its ancestors).
    */
    open fun findDeepestNodeAt(sceneX: Float, sceneY: Float): Node {
        for (child in children.asReversed()) {
            if (child.containsScenePoint(sceneX, sceneY)) {
                return child.findDeepestNodeAt(sceneX, sceneY)
            }
        }
        return this
    }

    /**
    Informs all ancestors that the pref/min/max size of this node has changed,
    and therefore [layoutChildren] needs to be called again before the scene is rendered.
    Cached data, such as prefWidth/height should be invalidated.
    */
    open fun requestLayout() {
        if (allowRequestLayout) {
            cachedSizes = null
            parent?.requestLayout()
            if (parent == null) {
                scene?.requestLayout = true
            }
        }
    }

    /**
     * Gives the input focus to this node, so that keyboard events are sent to it.
     *
     * @param useFocusTraversable If true, and this node isn't [focusTraversable],
     * then look for the next node in the scene tree which is.
     *
     * @param showFocusBorder Normally, the [Region.focusBorder].
     * Setting [showFocusBorder] = true turns on this border.
     */
    fun requestFocus(useFocusTraversable: Boolean = false, showFocusBorder: Boolean = false) {
        scene?.requestFocus(this, useFocusTraversable, showFocusBorder)
    }

    /**
    Informs that this node needs to be redrawn.
    Currently, this causes the whole scene to be redrawn.
     */
    fun requestRedraw() {
        scene?.requestRedraw = true
    }

    fun containsScenePoint(sceneX: Float, sceneY: Float) =
        visible && this.sceneX <= sceneX && this.sceneX + this.width > sceneX &&
            this.sceneY <= sceneY && this.sceneY + this.height > sceneY

    // endregion methods

    // region ==== Draw ====

    open fun requiresClipping() = width < evalPrefWidth() || height < evalPrefHeight()

    /**
    Draws this node, and also all [children].
    */
    internal open fun drawAll(clip: Boolean = false) {
        if (scene == null && warnWhenSceneUnset) {
            warnWhenSceneUnset = false
            log.error("Node's scene not set : $this")
            log.error("Ensure its parent uses `Node.childrenListener` or `Node.claimChildren()`")
            log.error("To prevent spamming the log, this message will NOT be repeated")
        }
        if (parent == null && scene?.root !== this && warnWhenParentUnset) {
            warnWhenParentUnset = false
            log.error("Node's parent not set : $this")
            log.error("To prevent spamming the log, this message will NOT be repeated")
        }

        if (clip || requiresClipping()) {
            backend.clip(sceneX, sceneY, width, height) {
                draw()
                if (backend.reportError()) log.error("$this")
                drawChildren()
            }
        } else {
            draw()
            if (backend.reportError()) log.error("$this")
            drawChildren()
        }
    }


    /**
    Draws just this node (NOT the [children]).

    If the node has been squashed smaller than its minimum size, then Glok will automatically
    clip all drawing to the node's bounds.
    Otherwise, no automatic clipping is performed.
    You can apply clipping using :

    backend.clip( sceneX, sceneY, width, height )

     */
    protected abstract fun draw()

    /**
    Draws the [children] of this node.

    As with [draw], automatic clipping is applied if this node is squashed smaller than its minimum size.
    */
    protected open fun drawChildren() {
        for (child in children) {
            if (child.visible) {
                try {
                    child.drawAll()
                } catch (e: Exception) {
                    log.severe(e)
                }
            }
        }
    }
    // endregion Draw

    // region ==== Layout ====

    private var cachedSizes: CalculatedSizes? = null
    private fun validateCachedSizes(): CalculatedSizes {
        cachedSizes?.let { return it }

        val sizes = CalculatedSizes(
            nodeMinWidth(), nodeMinHeight(),
            nodePrefWidth(), nodePrefHeight(),
            nodeMaxWidth(), nodeMaxHeight()
        )
        cachedSizes = sizes
        return sizes
    }

    /**
     * If [overrideMinWidth] is set, then return that, otherwise return [nodeMinWidth].
     * Caching is used to improve speed, as calculating sizes can be relatively expensive.
     */
    fun evalMinWidth() = overrideMinWidth ?: validateCachedSizes().minWidth

    /**
     * If [overrideMinWidth] is set, then return that, otherwise return [nodeMinHeight].
     * Caching is used to improve speed, as calculating sizes can be relatively expensive.
     */
    fun evalMinHeight() = overrideMinHeight ?: validateCachedSizes().minHeight

    /**
     * If [overridePrefWidth] is set, then return that, otherwise return [nodePrefWidth].
     * Caching is used to improve speed, as calculating sizes can be relatively expensive.
     */
    fun evalPrefWidth() = overridePrefWidth ?: max(evalMinWidth(), validateCachedSizes().prefWidth)

    /**
     * If [overridePrefWidth] is set, then return that, otherwise return [nodePrefHeight].
     * Caching is used to improve speed, as calculating sizes can be relatively expensive.
     */
    fun evalPrefHeight() = overridePrefHeight ?: max(evalMinHeight(), validateCachedSizes().prefHeight)

    /**
     * If [overrideMaxWidth] is set, then return that, otherwise return [nodeMaxWidth].
     * Caching is used to improve speed, as calculating sizes can be relatively expensive.
     */
    fun evalMaxWidth() = overrideMaxWidth ?: validateCachedSizes().maxWidth

    /**
     * If [overrideMaxHeight] is set, then return that, otherwise return [nodeMaxHeight].
     * Caching is used to improve speed, as calculating sizes can be relatively expensive.
     */
    fun evalMaxHeight() = overrideMaxHeight ?: validateCachedSizes().maxHeight

    /**
     * Each subclass should override this to calculate the minimum width it requires.
     * In general, do not call this, and instead call [evalMinWidth].
     *
     * The default implementation returns 0.
     *
     * The actual [width] of the node is determined by its [parent], which may ignore this node's requirements.
     *
     * * The root node of a scene is always the size of the window's client area,
     *   and therefore can be smaller than the minimum.
     *   This often causes nodes further down the tree of nodes to be squashed below their minimums too.
     * * Split pane ignores its children's minimums.
     *
     */
    open fun nodeMinWidth(): Float = 0f

    /**
     * Each subclass should override this, to calculate the minimum height it requires.
     * In general, do not call this, and instead call [evalMinHeight].
     *
     * This can be overridden on an ad-hoc basis using [overrideMinHeight].
     *
     * The default implementation returns 0.
     *
     * The actual [height] of the node is determined by its [parent], which may ignore this node's requirements.
     *
     * * The root node of a scene is always the size of the window's client area,
     *   and therefore can be smaller than the minimum.
     *   This often causes nodes further down the tree of nodes to be squashed below their minimums too.
     * * Split pane ignores its children's minimums.
     *
     */
    open fun nodeMinHeight(): Float = 0f

    /**
     * Each subclass much override this to calculate its preferred width.
     * In general, do not call this, and instead call [evalPrefWidth].
     *
     * The actual [width] of the node is determined by its [parent], which may ignore this node's requirements.
     *
     */
    abstract fun nodePrefWidth(): Float


    /**
     * Each subclass much override this to calculate its preferred height.
     * In general, do not call this, and instead call [evalPrefHeight].
     *
     * The actual [height] of the node is determined by its [parent], which may ignore this node's requirements.
     *
     */
    abstract fun nodePrefHeight(): Float

    /**
     * The default implementation returns [NO_MAXIMUM] (an arbitrary large number : 100,000).
     *
     * In general, do not call this, and instead call [evalMaxWidth].
     *
     */
    open fun nodeMaxWidth(): Float = NO_MAXIMUM

    /**
     * The default implementation returns [NO_MAXIMUM] (an arbitrary large number : 100,000).
     *
     * In general, do not call this, and instead call [evalMaxHeight].
     *
     */
    open fun nodeMaxHeight(): Float = NO_MAXIMUM

    /**
    Called by the [parent] [Node] during [layoutChildren].

    [localX], [localY] is the child's top left corner relative to this parent node.
    */
    protected fun setChildBounds(child: Node, localX: Float, localY: Float, width: Float, height: Float) {
        child.localX = localX
        child.localY = localY
        child.sceneX = sceneX + localX
        child.sceneY = sceneY + localY
        child.width = width
        child.height = height
    }

    /**
    Sometimes we want to hide a Node, without changing [visible].
    For example, `items` in a ToolBar are hidden when there isn't enough space.
    We cannot use [visible] in this case, because Glok doesn't _own_ this setting,
    the application developer does. (They may choose to hide/show icons depending on some state).

    Sets the bounds to 0,0 width 0 height 0.
    */
    protected fun hideChild(child: Node) = setChildBounds(child, 0f, 0f, 0f, 0f)

    internal fun layout() {
        try {
            allowRequestLayout = false
            layoutChildren()
        } finally {
            allowRequestLayout = true
        }

        for (child in children) {
            child.layout()
        }
    }

    /**
    The default behaviour does nothing.
    Override this on all subclasses which have children.
    */
    protected open fun layoutChildren() {}
    // endregion layout

    // region ==== Events ====

    internal fun <E : Event> findEventFilter(eventType: EventType<E>): EventHandler<E>? = eventFilters?.find(eventType)
    internal fun <E : Event> findEventHandler(eventType: EventType<E>): EventHandler<E>? =
        eventHandlers?.find(eventType)

    private var eventHandlers: EventHandlers? = null
    private var eventFilters: EventHandlers? = null

    fun <E : Event> addEventFilter(
        eventType: EventType<E>,
        handler: EventHandler<E>,
        combination: HandlerCombination = HandlerCombination.AFTER
    ) {
        eventFilters?.let {
            it.add(eventType, handler, combination)
            return
        }
        eventFilters = EventHandlers().apply {
            add(eventType, handler, combination)
        }
    }

    fun <E : Event> addEventFilter(
        eventType: EventType<E>,
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        handler: (E) -> Unit
    ) {
        addEventFilter(eventType, object : EventHandler<E> {
            override fun handle(event: E) {
                handler(event)
            }
        }, handlerCombination)
    }


    fun <E : Event> addEventHandler(
        eventType: EventType<E>,
        handler: EventHandler<E>,
        combination: HandlerCombination = HandlerCombination.AFTER
    ) {
        eventHandlers?.let {
            it.add(eventType, handler, combination)
            return
        }
        eventHandlers = EventHandlers().apply {
            add(eventType, handler, combination)
        }
    }

    /**
    Consider using these convenience functions instead :
    [onMousePressed], [onMouseReleased], [onPopupTrigger], [onMouseClicked],
    [onMouseDragged], [onMouseMoved], [onMouseEntered], [onMouseExited],
    [onKeyPressed], [onKeyReleased].
    */
    fun <E : Event> addEventHandler(
        eventType: EventType<E>,
        combination: HandlerCombination = HandlerCombination.AFTER,
        handler: (E) -> Unit
    ) {
        addEventHandler(eventType, object : EventHandler<E> {
            override fun handle(event: E) {
                handler(event)
            }
        }, combination)
    }

    fun <E : Event> removeEventFilter(eventType: EventType<E>, handler: EventHandler<E>) {
        eventFilters?.remove(eventType, handler)
    }

    fun <E : Event> removeEventHandler(eventType: EventType<E>, handler: EventHandler<E>) {
        eventHandlers?.remove(eventType, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_PRESSED].
    */
    fun onMousePressed(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_PRESSED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_RELEASED].
    */
    fun onMouseReleased(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_RELEASED, combination, handler)
    }


    /**
    [handler] will fire when the secondary mouse button is pressed/released depending on the platform.

    * * Windows : Secondary button released
    * * All other platforms : Secondary button pressed.

    Note, you do not need to test for `MouseEvent.isSecondary`.
    */
    fun onPopupTrigger(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        if (Platform.isWindows()) {
            onMouseReleased(combination) { event ->
                if (event.isSecondary) {
                    handler(event)
                }
            }
        } else {
            onMousePressed(combination) { event ->
                if (event.isSecondary) {
                    handler(event)
                }
            }
        }
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_CLICKED].
    */
    fun onMouseClicked(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_CLICKED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_DRAGGED].

    NOTE. To allow dragging, you must call [MouseEvent.capture] in the [onMousePressed] event.
    */
    fun onMouseDragged(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_DRAGGED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_MOVED].
    */
    fun onMouseMoved(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_MOVED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_ENTERED].
    */
    fun onMouseEntered(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_ENTERED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.MOUSE_EXITED].
    */
    fun onMouseExited(combination: HandlerCombination = HandlerCombination.AFTER, handler: (MouseEvent) -> Unit) {
        addEventHandler(EventType.MOUSE_EXITED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.SCROLL].
     */
    fun onScrolled(combination: HandlerCombination = HandlerCombination.AFTER, handler: (ScrollEvent) -> Unit) {
        addEventHandler(EventType.SCROLL, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.FILES_DROPPED].
     */
    fun onFilesDropped(
        combination: HandlerCombination = HandlerCombination.AFTER,
        handler: (DroppedFilesEvent) -> Unit
    ) {
        addEventHandler(EventType.FILES_DROPPED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.KEY_PRESSED].
     */
    fun onKeyPressed(combination: HandlerCombination = HandlerCombination.AFTER, handler: (KeyEvent) -> Unit) {
        addEventHandler(EventType.KEY_PRESSED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.KEY_RELEASED].
    */
    fun onKeyReleased(combination: HandlerCombination = HandlerCombination.AFTER, handler: (KeyEvent) -> Unit) {
        addEventHandler(EventType.KEY_RELEASED, combination, handler)
    }

    /**
    A more convenient version of [addEventHandler] for [EventType.KEY_TYPED].
    */
    fun onKeyTyped(combination: HandlerCombination = HandlerCombination.AFTER, handler: (KeyTypedEvent) -> Unit) {
        addEventHandler(EventType.KEY_TYPED, combination, handler)
    }
    // endregion Events

    // region ==== Object methods ====

    /**
    Dumps the scene graph to stdout starting at this Node, and walking through all descendants.
    Useful for debugging, and to give insights into the structure of controls.

    See [Scene.dump] to dump starting at the scene's root.

    * @param prefix The prefix of each line. Used to build the tree graphics.
    * @param borders If true, then an additional line is added for [Region]s showing border and padding data.
    * @param size If true, then an additional line shows the min/pref/max width/height.
    * @param filter Filters nodes. The default filters visible Nodes.
    */
    fun dump(
        prefix: String = "",
        borders: Boolean = true,
        size: Boolean = true,
        filter: (Node) -> Boolean = { it.visible }
    ) {
        val children = children.filter(filter)
        val isLastChild = parent?.children?.lastOrNull(filter) === this || parent == null
        print(prefix)
        if (isLastChild) {
            print("└──")
        } else {
            print("├──")
        }
        println(this)

        if (borders && this is Region) {
            print(prefix)
            if (isLastChild) {
                print("   ")
            } else {
                print("│  ")
            }
            if (children.isEmpty()) {
                print("")
            } else {
                print("│ ")
            }
            println("$background $border$borderSize Padding$padding")
        }
        if (size) {
            print(prefix)
            if (isLastChild) {
                print("   ")
            } else {
                print("│  ")
            }
            if (children.isEmpty()) {
                print("")
            } else {
                print("│ ")
            }
            println("Pref ${evalPrefWidth()} x ${evalPrefHeight()}  Min ${evalMinWidth()} x ${evalMinHeight()}  Max ${evalMaxWidth()} x ${evalMaxHeight()}")
        }

        if (children.isNotEmpty()) {
            for (child in children) {
                if (isLastChild) {
                    child.dump("$prefix   ", size = size, borders = borders, filter = filter)
                } else {
                    child.dump("$prefix│  ", size = size, borders = borders, filter = filter)
                }
            }
        }
    }

    fun toShortString() =
        "${simpleName()}${if (id.isBlank()) "" else " ($id)"} (${sceneX.toInt()},${sceneY.toInt()}) (${width.toInt()}x${height.toInt()})"

    override fun toString(): String {
        val id = id.ifBlank { hashCode().toString() }
        val focusT = if (focusTraversable) " FT" else ""
        val focused = if (focused) " Focused" else ""
        val parentage = if (scene == null) {
            if (parent == null) {
                " **detached**"
            } else {
                " **scene=null**"
            }
        } else if (parent == null && scene?.root !== this) {
            " **parent=null**"
        } else {
            ""
        }
        val hidden = if (visible) "" else "hidden "

        val at = "${localX.max2DPs()} , ${localY.max2DPs()}"
        val size = "${width.max2DPs()} x ${height.max2DPs()}"
        val grow = if (growPriority != 0f) " Grow(${growPriority.max2DPs()})" else ""
        val shrink = if (shrinkPriority != 0f) " Shrink(${shrinkPriority.max2DPs()})" else ""

        return "${simpleName()} #$id $at -> $size $hidden$styles $pseudoStyles$focusT$focused$parentage$grow$shrink"
    }
    // endregion object methods

    // region ==== Companion ====
    companion object {

        private val NO_CHILDREN = emptyObservableList<Node>()

        /**
        An arbitrary large number (100,000) for the default implementation of [nodeMaxWidth] and [nodeMaxHeight].

        NOTE. [Float.MAX_VALUE] is _not_ used, so that a parent node can safely sum the max widths/heights
        of its children.
        */
        const val NO_MAXIMUM = 100_000f

        /*
        To prevent the log being spammed, these are set to false after the first time an error is found.
        */
        private var warnWhenSceneUnset = true
        private var warnWhenParentUnset = true
        private val NO_MATCH = ValueAndImportance("NO_MATCH", 0, 0)
    }
    // endregion companion
}

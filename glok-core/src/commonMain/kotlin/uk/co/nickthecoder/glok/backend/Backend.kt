package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.util.Log
import uk.co.nickthecoder.glok.util.Matrix

/**
 * All drawing operations are done from here.
 * By having this as an interface, this lets us have different _backends_,
 * which leaves open the possibility of creating a multi-platform kotlin project
 * with a different [Backend] for the `js` platform for use in a browser.
 *
 * NOTE. [LoggingBackend] doesn't draw anything, but lets us create test cases
 * without setting up an OpenGL.
 */
interface Backend {

    fun reportError(): Boolean = false

    // ===== View =====

    fun drawFrame(window: Window, block: () -> Unit)
    fun drawView(width: Float, height: Float, scale: Float, block: () -> Unit)
    fun processEvents(timeoutSeconds: Double)
    fun setMousePointer(window: Window, cursor: MousePointer)

    /**
     * If you need to use OpenGL while Glok is rendering a scene, you must perform these actions within a [block].
     * This saves the OpenGL state, runs the block, and then restores the state again.
     */
    fun saveRestoreState(block: () -> Unit) {
        block()
    }

    // ===== Drawing =====

    /**
     * Draws a flot color over the entire viewport.
     * Used at the start of each redraw.
     */
    fun clear(color: Color)

    /**
     * Clip (scissor). The values are [LogicalPixels] measured from the TOP left of the window.
     * The clipped region is the intersection of the currently clipped region and the newly requested region.
     * @return false if the clipped region is empty (i.e. if nothing will be drawn due to this clipping).
     */
    fun beginClipping(left: Float, top: Float, width: Float, height: Float): Boolean
    fun endClipping()

    /**
     * A convenient way to call [beginClipping] and [endClipping], with a [block] of code in between.
     */
    fun clip(left: Float, top: Float, width: Float, height: Float, block: () -> Unit) {
        if (beginClipping(left, top, width, height)) {
            block()
            endClipping()
        }
    }

    fun clip(clip: Boolean, left: Float, top: Float, width: Float, height: Float, block: () -> Unit) {
        if (clip) {
            if (beginClipping(left, top, width, height)) {
                try {
                    block()
                } finally {
                    endClipping()
                }
            }
        } else {
            block()
        }
    }

    fun noClipping(block: () -> Unit)

    /**
     * Transforms the coordinates of all drawing calls within the [block] of code.
     * Used by the `Rotation` node and `ProxyNode`.
     */
    fun transform(transformation: Matrix, block: () -> Unit)
    fun transform(transformation: Matrix)
    fun clearTransform()

    fun fillRect(rect: GlokRect, color: Color)
    fun fillRect(left: Float, top: Float, right: Float, bottom: Float, color: Color)
    fun fillQuarterCircle(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, radius: Float)

    fun strokeInsideRect(rect: GlokRect, thickness: Float, color: Color)
    fun strokeInsideRect(left: Float, top: Float, right: Float, bottom: Float, thickness: Float, color: Color)
    fun strokeQuarterCircle(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, radius: Float, innerRadius: Float)

    /**
     * Draws the part of the [texture] defined by the rectangle [srcX], [srcY]  [width], [height]
     * to [destX], [destY] (which is where the top-left of the source corresponds to).
     */
    fun drawTexture(
        texture: Texture,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float = width, destHeight: Float = height,
        modelMatrix: Matrix? = null
    )

    fun drawTintedTexture(
        texture: Texture,
        tint: Color,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float = width, destHeight: Float = height,
        modelMatrix: Matrix? = null
    )

    /**
     * A gradient fill.
     * Each vertex of each triangle has a color.
     *
     * @param triangles Each triangle is 6 floats : x1,y1, x2, y2, x4, y3
     * @param colors Each corner of the triangle has an associated color.
     *        The length of [colors] must be half the length of `triangles`.
     */
    fun gradient(triangles: FloatArray, colors: Array<Color>)

    fun hsvGradient(triangles: FloatArray, colors: Array<FloatArray>)

    /**
     * OpenGL is moe efficient when operations can be batched.
     * So rather than call [drawTexture] multiple times (with the same texture, tint, and modelMatrix),
     * batch them and call [TextureBatch.draw] instead.
     */
    fun batch(texture: Texture, tint: Color?, modelMatrix: Matrix?, block: TextureBatch.() -> Unit)

    // ===== Factory methods =====

    fun createWindow(width: Int, height: Int): Window

    fun createTexture(name: String, width: Int, height: Int, pixels: IntArray): Texture

    fun resources(path: String): Resources

    fun fileResources(path: String): Resources

    // ===== Other =====

    fun monitorSize(): Pair<Int, Int>?

    companion object {

        fun beginLogging(log: Log) {
            backend = LoggingBackend(backend, log)
        }


        fun endLogging() {
            val current = backend
            if (current is LoggingBackend) {
                backend = current.inner
            }
        }

        fun log(log: Log, block: () -> Unit) {
            val oldBackend = backend
            backend = LoggingBackend(oldBackend, log)
            block()
            backend = oldBackend
        }

    }

}

/**
 * OpenGL is more efficient if you `batch` operations. [Backend.batch] lets us create such a batch.
 * Currently, the only operation is the rectangular [draw].
 *
 * This was first used for drawing glyphs of bitmap fonts, and later used by NinePatch.
 */
interface TextureBatch {
    fun draw(
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float = width, destHeight: Float = height,
    )
}

lateinit var backend: Backend

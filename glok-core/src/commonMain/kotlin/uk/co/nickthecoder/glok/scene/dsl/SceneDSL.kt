package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.property.boilerplate.ObservableTheme
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Scene
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.util.GlokException

fun scene(width: Number, height: Number, block: (SceneDSL.() -> Unit)? = null): Scene {
    val dsl = SceneDSL(width.toFloat(), height.toFloat()).optionalApply(block)
    return dsl.scene ?: throw GlokException("No root node specified for the scene")
}

fun scene(block: (SceneDSL.() -> Unit)? = null): Scene {
    val dsl = SceneDSL().optionalApply(block)
    return dsl.scene ?: throw GlokException("No root node specified for the scene")
}

fun Stage.scene(width: Number, height: Number, block: (SceneDSL.() -> Unit)? = null): Scene {
    val dsl = SceneDSL(width.toFloat(), height.toFloat()).optionalApply(block)
    scene = dsl.scene
    return dsl.scene ?: throw GlokException("No root node specified for the scene")
}

fun Stage.scene(block: (SceneDSL.() -> Unit)? = null): Scene {
    val dsl = SceneDSL().optionalApply(block)
    scene = dsl.scene
    return dsl.scene ?: throw GlokException("No root node specified for the scene")
}

class SceneDSL constructor(val width: Float = 0f, val height: Float = 0f) {

    internal var scene: Scene? = null
    var root: Node?
        get() = scene?.root
        set(v) {
            if (scene != null) {
                throw GlokException("Only one (root) Node can be added to a Scene. Have you omitted items{ ... } or children{ ... } ?")
            }
            val newScene = Scene(v!!, width, height)

            theme?.let {
                newScene.themeProperty.unbind()
                newScene.theme = it
            }
            themeProperty?.let { newScene.themeProperty.bindTo(it) }

            scene = newScene
        }

    private var theme: Theme? = null
    private var themeProperty: ObservableTheme? = null

    fun theme(theme: Theme) {
        this.theme = theme
    }

    fun theme(themeProperty: ObservableTheme) {
        this.themeProperty = themeProperty
    }

}

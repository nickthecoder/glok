/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene.dsl

import uk.co.nickthecoder.glok.control.ButtonBar
import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.scene.Node

class MeaningDSL(val buttonBar: ButtonBar, val meaning: ButtonMeaning) {
    operator fun Node.unaryPlus() {
        buttonBar.add(meaning, this)
    }
}

fun ButtonBar.meaning(meaning: ButtonMeaning, block: (MeaningDSL.() -> Unit)? = null) {
    MeaningDSL(this, meaning).optionalApply(block)
}

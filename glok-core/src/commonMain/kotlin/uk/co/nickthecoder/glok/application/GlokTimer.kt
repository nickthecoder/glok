/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.util.currentTimeMillis

/**
 * Runs code after a given time interval.
 * This is not a high precision timer, there is no guarantee about how soon after [delay] milliseconds,
 * the [action] will be performed.
 *
 * The timer is _not_ started on creation. You must call [start].
 *
 * [delay] is measured in milliseconds, and stored as a Double for compatibility with javascript,
 * which doesn't support the `Long` type.
 *
 * If [repeating] == `false`, [action] is performed only once, and then the timer stops.
 * Otherwise, [action] will be repeated every [delay] milliseconds until the timer is [stopped][stop].
 */
class GlokTimer(val delay: Double, val repeating: Boolean = false, val action: () -> Unit) : Comparable<GlokTimer> {

    private var dueTimestamp: Double = 0.0
    private var status: Int = STOPPED

    /**
     * Allows timers to be ordered by the time they are next due to fire.
     */
    override fun compareTo(other: GlokTimer): Int {
        return dueTimestamp.compareTo(other.dueTimestamp)
    }

    /**
     * A timer is not started on creation, so this must be called at least once for the timer to have any effect.
     *
     * You may call start _again_, before [action] had been performed, in which case, the timer resets,
     * and it will be at least [delay] milliseconds before [action] is performed.
     */
    fun start(): GlokTimer {
        when (status) {
            STOPPED -> {
                status = PENDING
                pending.add(this)
            }

            RUNNING -> {
                status = RESTART
                pending.add(this)
            }

            STOPPING -> {
                status = PENDING
                if (! pending.contains(this)) {
                    pending.add(this)
                }
            }
        }
        return this
    }

    fun stop() {
        if (status == RUNNING) {
            status = STOPPING
        }
    }

    companion object {
        private const val STOPPED = 0
        private const val PENDING = 1
        private const val RUNNING = 2
        private const val RESTART = 3
        private const val STOPPING = 4

        private val pending = mutableListOf<GlokTimer>()
        private val running = mutableListOf<GlokTimer>()

        internal fun clearTimers() {
            pending.clear()
            running.clear()
        }

        internal fun processAll() {
            val now = currentTimeMillis()

            if (pending.isNotEmpty()) {
                for (timer in pending) {
                    if (timer.status == PENDING) {
                        timer.dueTimestamp = now + timer.delay
                        running.add(timer)
                        timer.status = RUNNING
                    } else if (timer.status == RESTART) {
                        running.remove(timer)
                        timer.dueTimestamp = now + timer.delay
                        running.add(timer)
                        timer.status = RUNNING
                    }
                }
                pending.clear()
            }
            val iterator = running.iterator()
            while (iterator.hasNext()) {
                val timer = iterator.next()
                if (timer.status == STOPPING) {
                    timer.status = STOPPED
                    iterator.remove()
                } else {
                    // This used to use a sorted set, and used break, but JS has no sortedSet
                    // It is now less efficient. Consider a slow version for the web, and optimised version for desktop.
                    if (timer.dueTimestamp > now) {
                        continue
                    }
                    timer.action()
                    iterator.remove()
                    if (timer.repeating) {
                        timer.status = PENDING
                        pending.add(timer)
                    }
                }
            }
        }
    }

}

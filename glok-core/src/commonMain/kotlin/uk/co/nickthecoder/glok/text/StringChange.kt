/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.history.Batch
import uk.co.nickthecoder.glok.history.Change
import uk.co.nickthecoder.glok.history.HistoryDocument

/**
 * A [Change] for the [HistoryDocument]s (used by [TextField]s).
 *
 * There are only 2 implementations. [DeleteText] and [InsertText].
 * Replacing text is achieved by a combination of [DeleteText] and [InsertText] within a single [Batch].
 */
sealed interface StringChange : Change {
    override val document: StringDocument
}

interface DeleteString : StringChange {
    /**
     * The start of the text to be deleted.
     * This must NOT be after [to].
     */
    val from: Int

    /**
     * The end of the text to be deleted.
     */
    val to: Int

    /**
     * This is a calculated when the [DeleteString] is created using
     * `document.substring(from, to)`
     */
    val str: String
}

interface InsertString : StringChange {
    val from: Int

    /**
     * A calculated field, based on [from] and [str].
     * This is the position of the end of the inserted text after it has been inserted.
     */
    val to: Int

    /**
     *
     */
    val str: String
}

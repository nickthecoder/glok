/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.theme.styles.MENU_BAR

/**
 * Identical to a [ToolBar], but may be styled differently.
 *
 * MenuBar's [items] are typically only [Menu]s, but it possible to include any Nodes.
 *
 * Styling a MenuBar is similar to [ToolBar], except [styles] uses `"menu_bar"` instead of `"tool_bar"`.
 */
class MenuBar : ToolBarBase(Side.TOP) {

    init {
        styles.add(MENU_BAR)
        section = true
    }
}

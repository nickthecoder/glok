/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

/**
 *
 */
enum class StageType( val allowOverlap : Boolean ) {
    /**
     * A normal stage, with window decorations (title bar and borders). Not modal.
     */
    NORMAL(true),

    /**
     * A modal dialog, with window decorations (title bar and borders).
     * Begin modal means that the stage(s) below cannot be accessed until the model dialog is closed.
     */
    MODAL(true),

    /**
     * A transient stage, which is automatically closed when the mouse is pressed outside the stage.
     * No window decorations (title bar or borders).
     *
     * Used for tooltips and the `overflow` in ToolBar.
     */
    POPUP(false),

    /**
     * A transient stage, which is automatically closed when the mouse is pressed outside the stage.
     * No window decorations (title bar or borders).
     *
     * Used for PopupMenus
     */
    POPUP_MENU(false)
}

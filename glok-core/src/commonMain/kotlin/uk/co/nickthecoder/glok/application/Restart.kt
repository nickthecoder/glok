/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import kotlin.reflect.KClass

/**
 * When an [Application] ends, it can choose to restart by adding an item to [GlokSettings.restarts].
 * [applicationClass] can be the same as the previous application, or another one.
 *
 * The 'DemoMenu' (in the `glok-demos` subproject) and all the actual demos that it launches uses [Restart]s.
 * This allows each demo to be a self-contained [Application], and also launched via the `DemoMenu` [Application].
 */
class Restart(
    val applicationClass: KClass<out Application>,
    val rawArgs: Array<out String> = emptyArray(),
)

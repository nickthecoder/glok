/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.styles.TEXT

/**
 * A [Node] containing just plain text.
 * This is much simpler than [Label].
 */
class Text(text: String) : Region() {

    // region ==== Properties ====

    val textProperty by stringProperty(text)
    var text by textProperty

    val alignmentProperty by alignmentProperty(Alignment.TOP_LEFT)
    var alignment by alignmentProperty

    val fontProperty by stylableFontProperty(Font.defaultFont)
    var font by fontProperty

    val textColorProperty by stylableColorProperty(Color.BLACK)
    var textColor by textColorProperty

    val indentationColumnsProperty by stylableIntProperty(4)
    var indentationColumns by indentationColumnsProperty

    // endregion properties

    init {
        styles.add(TEXT)
        textProperty.addListener(requestLayoutListener)
        alignmentProperty.addListener(requestRedrawListener)
        fontProperty.addListener(requestLayoutListener)
        textColorProperty.addListener(requestRedrawListener)
        indentationColumnsProperty.addListener(requestLayoutListener)
    }

    // region ==== Layout ====
    override fun nodePrefWidth() = surroundX() + font.widthOfOrZero(text, indentationColumns)
    override fun nodePrefHeight() = surroundY() + font.height
    override fun nodeMinWidth() = nodePrefWidth()
    override fun nodeMinHeight() = nodePrefHeight()

    override fun draw() {
        super.draw()
        val hAlignment = alignment.hAlignment
        val vAlignment = alignment.vAlignment
        val x = when (hAlignment) {
            HAlignment.LEFT -> sceneX
            HAlignment.RIGHT -> sceneX + width
            HAlignment.CENTER -> sceneX + width / 2
        }
        val y = when (vAlignment) {
            VAlignment.TOP -> sceneY
            VAlignment.BOTTOM -> sceneY + height
            VAlignment.CENTER -> sceneY + height / 2
        }
        font.draw(
            text, textColor,
            x + surroundLeft(), y + surroundTop(), hAlignment, vAlignment.textVAlignment, indentationColumns
        )
    }
    // endregion layout

}


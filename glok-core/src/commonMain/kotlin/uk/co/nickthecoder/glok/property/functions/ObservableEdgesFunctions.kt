/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property.functions

import uk.co.nickthecoder.glok.property.boilerplate.EdgesUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.FloatUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableEdges
import uk.co.nickthecoder.glok.property.boilerplate.ObservableFloat
import uk.co.nickthecoder.glok.scene.Edges

fun ObservableEdges.withTop(top: Float): ObservableEdges = EdgesUnaryFunction(this) { e -> Edges(top, e.right, e.bottom, e.left) }
fun ObservableEdges.withRight(right: Float): ObservableEdges = EdgesUnaryFunction(this) { e -> Edges(e.top, right, e.bottom, e.left) }
fun ObservableEdges.withBottom(bottom: Float): ObservableEdges = EdgesUnaryFunction(this) { e -> Edges(e.top, e.right, bottom, e.left) }
fun ObservableEdges.withLeft(left: Float): ObservableEdges = EdgesUnaryFunction(this) { e -> Edges(e.top, e.right, e.bottom, left) }

fun ObservableEdges.top(): ObservableFloat = FloatUnaryFunction(this) { e -> e.top }
fun ObservableEdges.right(): ObservableFloat = FloatUnaryFunction(this) { e -> e.right }
fun ObservableEdges.bottom(): ObservableFloat = FloatUnaryFunction(this) { e -> e.bottom }
fun ObservableEdges.left(): ObservableFloat = FloatUnaryFunction(this) { e -> e.left }


package uk.co.nickthecoder.glok.backend

/**
 * An abstraction for loading resources, without needing to know the ultimate source of those resources.
 * At time of writing, the only implementation is [GLJarResources], though another implementation which
 * loads from the filesystem would also be useful.
 */
interface Resources {

    /**
     * Loads a texture.
     *
     * @param name The 'filename' of the resource, such as `myImage.png`.
     *        For a hierarchical tree of resources, `name` may use a forward-slash `/`
     *        e.g. `icons/myIcon.png`. Do NOT use backslashes, even when you _know_ that the
     *        resources will be loaded from a Windows filesystem.
     */
    fun loadTexture(name: String): Texture
}

package uk.co.nickthecoder.glok.application

/**
 * Unlike JavaFX, starting a Glok application is the same as any other JVM based application.
 * Declare a 'main' function, and call [launch].
 *
 * Glok does not parse the command-line arguments for you, but does supply two functions
 * that you can use if you wish :  [posixArguments], [doubleDashArguments]
 * Both return [Arguments].
 *
 * You can parse the command line in the `main` method, or in [Application.start],
 * whichever is applicable to your needs.
 * If you application works as a GUI as well as a command-line only program,
 * it may even parse the arguments in both places.
 *
 * There are a number of competing conventions for parsing command line arguments :
 *
 *     myProgram -fooValue    // Use posixArguments()
 *     myProgram --foo=Value  // GNU's convention - no implementation supplied by Glok
 *     myProgram --foo Value  // Use doubleDashArguments()
 */
interface Arguments {

    /**
     * Is the single-character flag parameter present?
     */
    fun flag(shortName: Char): Boolean

    /**
     * Is the multi-character flag parameter present?
     */
    fun flag(longName: String): Boolean

    /**
     * Is either the single-character or the equivalent multi-character flag present.
     *
     * A convenience function equivalent to :
     *
     *     flag(shortName) || flag(longName)
     *
     */
    fun flag(shortName: Char, longName: String) = flag(shortName) || flag(longName)

    /**
     * The value of a named parameter. If multiple values are present, then only the first is returned.
     */
    fun value(name: String): String?

    /**
     * A Kotlin operator equivalent to [value]. i.e.
     *
     *     myArgs["paramName"] == myArgs.value( "paramName" )
     */
    operator fun get(name: String) = value(name)

    /**
     * The values of a named parameter, which is expected to have multiple values.
     * If no values are found, this returns an empty list.
     */
    fun values(name: String): List<String>

    /**
     * Remaining (unnamed) parameters.
     */
    fun remainder(): List<String>

    /**
     * Used for debugging - lists all parameters.
     */
    fun dump()
}

class ArgumentsException(val arg: String, message: String) : Exception(message) {
    constructor(arg: String) : this(arg, "Error parsing argument: $arg")
}

private class ArgumentsImpl : Arguments {

    val flagsFound = mutableSetOf<Char>()

    val longFlagsFound = mutableSetOf<String>()

    val values = mutableMapOf<String, MutableList<String>>()

    val remaining = mutableListOf<String>()

    fun addValue(name: String, value: String) {
        val existingList = values[name]
        if (existingList == null) {
            values[name] = mutableListOf(value)
        } else {
            existingList.add(value)
        }
    }

    override fun flag(shortName: Char) = flagsFound.contains(shortName)
    override fun flag(longName: String) = longFlagsFound.contains(longName)
    override fun remainder() = remaining
    override fun value(name: String) = values[name]?.first()
    override fun values(name: String) = values[name] ?: emptyList()

    override fun dump() {
        println("flags : $flagsFound")
        println("longFlags : $longFlagsFound")
        println("values :")
        for ((name, list) in values) {
            println("    parameter $name : $list")
        }
        println("Remainder : $remaining")
    }
}

/**
 * See [POSIX Conventions for Command Line Arguments](http://booksonline.nl/tutorial/essential/attributes/_posix.html)
 *
 * In order to parse, we need to know which characters are flags, which strings are option names,
 * and if the option has an argument (or is just a long form of a flag).
 *
 * For example, this could be parsed in many ways :
 *
 *     myProgram -abc -de foo bar
 *
 * If `abc` is an option with no value, then it is a long name for a flag.
 * If `-abc` is an option _with_ a value, then the value is `-de`.
 * If `a` is an option _with_ a value, then the value is `bc`.
 * If `abc` isn't an option, then it is three flags, `a`, `b`, `c`.
 *
 * IMHO, this is a mess, and I would never use it.
 *
 * @param flags A list of single-character flags
 * @param longFlags A list of multi-character flags
 * @param parameters A list of parameter names
 * @param args The arguments to be parsed (the values passed to the program's `main` method)
 *
 * @throws ArgumentsException
 *
 */
fun posixArguments(

    flags: List<Char>,
    longFlags: List<String>,
    parameters: List<String>,

    args: Array<out String>

): Arguments {
    val result = ArgumentsImpl()

    val singleCharParameters = parameters.filter { it.length == 1 }

    var endOfArguments = false
    var skip = false

    for ((index, arg) in args.withIndex()) {
        if (skip) {
            skip = false
            continue
        }

        if (endOfArguments) {
            result.remaining.add(arg)
        } else {

            if (arg.startsWith('-')) {
                if (arg == "--") {
                    endOfArguments = true
                } else {
                    val withoutDash = arg.substring(1)

                    if (longFlags.contains(withoutDash)) {
                        // e.g. -verbose
                        result.longFlagsFound.add(withoutDash)

                    } else if (parameters.contains(withoutDash)) {
                        // e.g. -out result.txt
                        if (args.size > index) {
                            val value = args[index + 1]
                            result.addValue(withoutDash, value)
                            skip = true
                        } else {
                            throw ArgumentsException("Expected a value after $arg")
                        }
                    } else {
                        // Not a long name
                        val firstLetter = withoutDash.substring(0, 1)
                        if (singleCharParameters.contains(firstLetter)) {
                            // e.g. -oresult.txt (the value is result.txt). IMHO, Horrible!
                            val value = arg.substring(1)
                            result.addValue(firstLetter, value)
                        } else {
                            if (arg == "-") {
                                // Looks like flags, but none specified.
                                throw ArgumentsException(arg)
                            } else {
                                // A set of single character flags. e.g. -abc, where a, b and c are flags.
                                for (c in withoutDash) {
                                    if (flags.contains(c)) {
                                        result.flagsFound.add(c)
                                    } else {
                                        throw ArgumentsException(arg)
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                // Not an option e.g. result.txt
                result.remaining.add(arg)
            }
        }
    }
    return result
}

/*
 * I was going to write GNU style, but it's so horrible to use - tab completion on filenames cannot work
 * when the filename is squished up against the `=`.
 * See [GNU Program Argument Syntax Conventions](https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html)
 *
 */

/**
 * Similar to [posixArguments], but uses two dashes for long names. Single dashes are _only_ used for flags.
 *
 * Values are always separated by a space after the long name. e.g. --out result.txt
 *
 * This is my favourite convention, because it is less messy than the Posix convention,
 * and doesn't have the problem with filename completion caused by the GNU convention.
 *
 * @param flags A list of single-character flags
 * @param longFlags A list of multi-character flags
 * @param parameters A list of parameter names
 * @param args The arguments to be parsed (the values passed to the program's `main` method)
 *
 * @throws ArgumentsException
 */
fun doubleDashArguments(
    flags: List<Char>,
    longFlags: List<String>,
    parameters: List<String>,

    args: Array<out String>

): Arguments {
    val result = ArgumentsImpl()

    var endOfArguments = false
    var skip = false

    for ((index, arg) in args.withIndex()) {
        if (skip) {
            skip = false
            continue
        }

        if (endOfArguments) {
            result.remaining.add(arg)
        } else {

            if (arg.startsWith("--")) {
                if (arg == "--") {
                    endOfArguments = true
                } else {
                    val withoutDashes = arg.substring(2)

                    if (longFlags.contains(withoutDashes)) {
                        // e.g. --verbose
                        result.longFlagsFound.add(withoutDashes)

                    } else if (parameters.contains(withoutDashes)) {
                        // e.g. --out result.txt
                        if (args.size > index + 1) {
                            val value = args[index + 1]
                            result.addValue(withoutDashes, value)
                            skip = true
                        } else {
                            throw ArgumentsException("Expected a value after $arg", arg)
                        }
                    } else {
                        throw ArgumentsException(arg)
                    }
                }

            } else if (arg.startsWith('-')) {

                if (arg == "-") {
                    // Looks like flags, but none specified.
                    throw ArgumentsException(arg)
                } else {
                    val withoutDash = arg.substring(1)
                    // A set of single character flags. e.g. -abc, where a, b and c are flags.
                    for (c in withoutDash) {
                        if (flags.contains(c)) {
                            result.flagsFound.add(c)
                        } else {
                            throw ArgumentsException(arg)
                        }
                    }
                }

            } else {
                // Not an option e.g. result.txt
                result.remaining.add(arg)
            }
        }
    }

    return result
}

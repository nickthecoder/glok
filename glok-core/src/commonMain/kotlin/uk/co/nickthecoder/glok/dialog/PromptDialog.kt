/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dialog

import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.property.functions.isNotBlank
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.scene.dsl.textField
import uk.co.nickthecoder.glok.scene.dsl.vBox

/**
 * A dialog box where the user can enter a single String (in a TextField).
 */
class PromptDialog : Dialog() {

    val headingProperty by stringProperty("")
    var heading by headingProperty

    val textProperty by stringProperty("")
    var text by textProperty

    val aboveProperty by stringProperty("")
    var above by aboveProperty

    private var textField: TextField? = null

    init {
        content = vBox {
            fillWidth = true
            style(".prompt_pane")

            + label(heading) {
                visibleProperty.bindTo(headingProperty.isNotBlank())
                textProperty.bindTo(headingProperty)
                style(".heading")
            }
            + vBox {
                fillWidth = true
                style(".content")
                + label("") {
                    visibleProperty.bindTo(aboveProperty.isNotBlank())
                    textProperty.bindTo(aboveProperty)
                }
                + textField("") {
                    textField = this
                    this@PromptDialog.textProperty.bidirectionalBind(textProperty)
                    growPriority = 1f
                    shrinkPriority = 1f
                }
            }
        }
    }

    fun show(parentStage: Stage, callback: (String) -> Unit) {
        createStage(parentStage, StageType.MODAL) {
            buttonTypes(ButtonType.OK) {
                close()
                callback(text)
            }
            buttonTypes(ButtonType.CANCEL)
            show()
            textField?.requestFocus()
        }
    }
}

/**
 * A convenience method to create a [PromptDialog]
 */
fun promptDialog(block: PromptDialog.() -> Unit) =
    PromptDialog().apply(block)

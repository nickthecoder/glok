package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.property.boilerplate.ColorProperty

/**
 * A drawn border, around a [Region].
 *
 * Unlike JavaFX, Border is an interface with many implementations.
 * [NoBorder] (the default) and [PlainBorder] are the simplest.
 *
 *      (x,y)
 *        ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐   ▲  ▲
 *        ┊  the drawn region ┊   ┊  ┆ size.top
 *        ┊     ┏━━━━━━━━━┓   ┊   ┊  ▼
 *        ┊     ┃ Node's  ┃   ┊   ┊
 *        ┊     ┃ content ┃   ┊   ┊height
 *        ┊     ┃         ┃   ┊   ┊
 *        ┊     ┗━━━━━━━━━┛   ┊   ┊  ▲
 *        ┊                   ┊   ┊  ┆ size.bottom
 *        └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘   ▼  ▼
 *
 *        ◀┄┄┄┄┄┄┄┄width┄┄┄┄┄┄▶
 *        ◀┄┄┄┄┄▶         ◀┄┄┄▶
 *        size.left       size.right
 *
 * The dashed outer rectangle is the node's bounds.
 * In general, Borders should only draw within the region between the two rectangles.
 *
 * This diagram doesn't include a Node's padding, as this isn't relevant to its border.
 *
 * A border is often the same width on all sides, but in this diagram, `size.left` is
 * larger than the other side(s). (For me, top and bottom appear the same as right,
 * but may depend on the font being used).
 */
interface Border {

    /**
     * Draw just the border, not the area within it.
     * However, if implementations wish to draw the inside area too, that's ok.
     */
    fun draw(x: Float, y: Float, width: Float, height: Float, color : Color, size: Edges)

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.action

import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean

/**
 * A Command defines what should happen when a GUI element is `pressed`, without being associated with a particular
 * node.
 *
 * This means that a single Command can be used by more than one Node. For example,
 * an 'Undo' button in a ToolBar can use the same Action as an 'Undo' menu item in a Menu.
 *
 * The `DRY` principal (Don't Repeat Yourself).
 *
 * In a 'traditional' application, `DRY` is achieved by calling an 'undo()' method from the `onAction` event of
 * the Undo Button, and the Undo MenuItem.
 * However, this means that when we build our scene graphs, we would need to reference the classes that perform
 * the action, and in a large application, this can get messy.
 *
 * Instead, we define the [Command]s completely separately from the [Action]s.
 *
 * Commands are found into the [Commands] class, and [Action]s with the [Actions] class.
 *
 * FYI. This was named [Command] after the `Gang of Four's` `Design Patterns` book.
 */
open class Command(
    val action: Action,
    val lambda: ((ActionEvent) -> Unit)?
) {
    var disableProperty: ObservableBoolean? = null

    /**
     * Should this command consume the event, which prevents other event handlers firing?
     */
    var consume = true


    fun disable(p: ObservableBoolean): Command {
        disableProperty = p
        return this
    }

    override fun toString() = "Command for $action"
}

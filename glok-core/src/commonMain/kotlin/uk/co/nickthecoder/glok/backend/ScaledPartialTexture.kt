package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image

/**
 * Defines a rectangular region of a [Texture] as an [Image], but its size is reported scaled.
 * i.e. a 64x64 image, scaled by 0.5 will report is size to be 32x32. NOTE, its actual size is still 64x64,
 * and if you draw it (without additional scaling), it will be drawn as 32x32.
 *
 * Example usage. Suppose we have a [Texture] with many 64x64 icons. This is typically too large for buttons
 * in a toolbar, but if we scale them by 0.5, then they are fine.
 * So, why not just use 32x32 icons instead? I'm glad you asked. On a high DPI display, the entire scene is
 * _also_ scaled up (maybe by a factor of 2).
 * If we used 32x32 icons, then they would look equally bad (pixelated) on high-DPI displays as regular displays.
 * i.e. we wouldn't be taking advantage of the extra pixels.
 * By using [ScaledPartialTexture] the icons will be 32x32 `logical` pixels, but on a high-DPI display,
 * the full 64x64 pixels will be put to use.
 *
 * NOTE, the overhead of scaling down to 32x32 pixels on a regular display is very minimum
 * (assuming the graphics card isn't older than 20+ years!).
 */
class ScaledPartialTexture(val partialTexture: PartialTexture, val scaleX: Float, val scaleY: Float) : Image {

    override val imageWidth get() = partialTexture.imageWidth * scaleX
    override val imageHeight get() = partialTexture.imageHeight * scaleY

    override fun batch(tint: Color?, block: TextureBatch.() -> Unit) {
        backend.batch(partialTexture.texture, tint, null, block)
    }

    override fun draw(x: Float, y: Float, tint: Color?) {
        if (tint == null) {
            backend.drawTexture(
                partialTexture.texture,
                partialTexture.srcX, partialTexture.srcY, partialTexture.imageWidth, partialTexture.imageHeight,
                x, y, imageWidth, imageHeight
            )
        } else {
            backend.drawTintedTexture(
                partialTexture.texture, tint,
                partialTexture.srcX, partialTexture.srcY, partialTexture.imageWidth, partialTexture.imageHeight,
                x, y, imageWidth, imageHeight
            )
        }
    }

    override fun drawBatched(batch: TextureBatch, x: Float, y: Float) {
        batch.draw(
            partialTexture.srcX, partialTexture.srcY, partialTexture.imageWidth, partialTexture.imageHeight,
            x, y, imageWidth, imageHeight
        )
    }

    override fun drawTo(x: Float, y: Float, destWidth: Float, destHeight: Float, tint: Color?) {
        partialTexture.drawTo(x, y, destWidth, destHeight, tint)
    }

    override fun drawToBatched(batch: TextureBatch, x: Float, y: Float, destWidth: Float, destHeight: Float) {
        partialTexture.drawToBatched(batch, x, y, destWidth, destHeight)
    }

    override fun partialImage(fromX: Float, fromY: Float, width: Float, height: Float): Image {
        return PartialTexture(
            partialTexture.texture,
            partialTexture.srcX + fromX / scaleX,
            partialTexture.srcY + fromY / scaleY,
            width / scaleX,
            height / scaleY
        ).scaledBy(scaleX, scaleY)
    }

    override fun scaledBy(scaleX: Float, scaleY: Float) =
        ScaledPartialTexture(partialTexture, scaleX * this.scaleX, scaleY * this.scaleY)

    // ==== Object methods ====

    override fun toString() = "$partialTexture scaled by $scaleX, $scaleY"

}

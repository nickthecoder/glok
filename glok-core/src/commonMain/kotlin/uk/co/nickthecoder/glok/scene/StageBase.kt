/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.control.Button
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.util.GlokException
import uk.co.nickthecoder.glok.util.commonAncestor
import uk.co.nickthecoder.glok.util.currentTimeMillis
import kotlin.math.abs

abstract class StageBase : Stage {

    // region ==== Properties ====

    val mutableClosingProperty by booleanProperty(false)
    final override val closingProperty = mutableClosingProperty.asReadOnly()
    final override var closing by mutableClosingProperty
        internal set

    final override val onCloseRequestedProperty by optionalActionEventHandlerProperty(null)
    final override var onCloseRequested by onCloseRequestedProperty

    final override val onClosedProperty by optionalActionEventHandlerProperty(null)
    final override var onClosed by onClosedProperty

    final override val mousePointerProperty by mousePointerProperty(MousePointer.ARROW)
    final override var mousePointer by mousePointerProperty

    private val mutableSceneProperty by optionalSceneProperty(null)
    final override val sceneProperty = mutableSceneProperty.asReadOnly()
    final override var scene by mutableSceneProperty

    private val sceneListener = mutableSceneProperty.addChangeListener { _, old, newValue ->
        old?.stage = null
        newValue?.stage = this@StageBase
    }

    final override val resizableProperty by booleanProperty(true)
    final override var resizable by resizableProperty

    final override val maximizedProperty by booleanProperty(false)
    final override var maximized by maximizedProperty

    final override val minimizedProperty by booleanProperty(false)
    final override var minimized by minimizedProperty

    final override val titleProperty by stringProperty("")
    final override var title by titleProperty

    // endregion properties

    // region ==== Fields ====

    /**
     * The position of the mouse pressed event in scene coordinates.
     */
    private var mousePressedX: Float = 0f
    private var mousePressedY: Float = 0f
    private var mouseButton: MouseButton = MouseButton.NONE
    private var mouseClickPossible = false
    private var mouseClickCount: Int = 0
    private var mousePressedNode: Node? = null
    private var mousePressedMillis: Double = 0.0

    /**
     * Used to generate mouse enter/exit events.
     * This holds the deepest node at the mouse pointer's position.
     */
    private var previousMouseInNode: Node? = null

    /**
     * Save in [onMouseMove], and used in [processTooltip], as the caller (`Application`)
     * doesn't have access to the mouse position.
     */
    private var savedMouseX: Float = 0f
    private var savedMouseY: Float = 0f

    /**
     * Used while turning [onMouseMove] into [EventType.MOUSE_DRAGGED] events.
     */
    private var dragStartX: Float = 0f
    private var dragStartY: Float = 0f
    private var dragDetected: Boolean = false

    /**
     * When a mouse button is PRESSED, we remember the [Node] under the mouse pointer
     * which consumed the press event (if there was one).
     * Subsequent [onMouseMove] will be preferentially sent to [capturedByNode]
     * (as [EventType.MOUSE_DRAGGED] events).
     */
    internal var capturedByNode: Node? = null
        private set

    // endregion fields

    override fun close() {
        closing = true
    }

    protected fun preShow() {
        val scene = scene ?: throw GlokException("Scene not set")

        val width = scene.width
        val height = scene.height
        if (width <= 0f || height <= 0f) {
            if (scene.requestRestyling) scene.restyle()
            if (scene.requestLayout) scene.layout()
            if (width <= 0f) scene.width = scene.root.evalPrefWidth()
            if (height <= 0f) scene.height = scene.root.evalPrefHeight()
        }
    }

    internal fun processTooltip(): Boolean {
        // Don't show a tooltip if there already is one, or if we are dragging
        if (regularStage.tooltipStage != null || capturedByNode != null) return false

        scene?.findDeepestNodeAt(savedMouseX, savedMouseY)?.let { deepest ->
            val root = scene?.root ?: return false
            if (! root.containsScenePoint(savedMouseX, savedMouseY)) return false

            val nodeWithToolTip = deepest.firstToRoot { it.tooltip != null }
            if (nodeWithToolTip != null) {
                regularStage.tooltipStage = nodeWithToolTip.tooltip !!.show(nodeWithToolTip, savedMouseX, savedMouseY)
                regularStage.tooltipStage?.onClosed {
                    regularStage.tooltipStage = null
                }
            }
        }
        return true
    }

    // region ==== Events ====

    open fun onMouseEnterExit(entered: Boolean) {
        // When the mouse exists the stage, we must also cause a mouse-exit event for the node that
        // previously contained the mouse.
        if (! entered) {
            // Don't do this for popup-menus, because moving from a SubMenuItem into the SubMenuItem#'s PopupMenu
            // will cause that PopupMenu to be closed.
            // This is a bit of a bodge, but MUCH easier than fixing it in a 100% clean manner.
            if (this is OverlayStage && stageType == StageType.POPUP_MENU) return

            previousMouseInNode?.let { node ->
                scene?.let { scene ->
                    val event = MouseEvent(EventType.MOUSE_EXITED, scene, savedMouseX, savedMouseY, 0, 0, 0)
                    bubbleUpwards(event, node)
                }
                previousMouseInNode = null
            }

            savedMouseX = - 1f
            savedMouseY = - 1f
        }
    }

    fun onKeyEvent(event: KeyEvent) {

        val scene = scene ?: return

        scene.focusOwner?.let { focusOwner ->
            bubbleDownwards(event, focusOwner)
            bubbleUpwards(event, focusOwner)
        }

        // Check for Tab / Shift+Tab to focus on the next/previous node.
        // This code was previously between bubbleDownwards and bubbleUpwards.
        if (! event.isConsumed()) {

            if (event.eventType == EventType.KEY_PRESSED) {
                if (StageActions.FOCUS_NEXT.matches(event)) {
                    scene.focusNext()
                    scene.focusOwner?.focusedByKeyboard = true
                    event.consume()
                }
                if (StageActions.FOCUS_PREVIOUS.matches(event)) {
                    scene.focusPrevious()
                    scene.focusOwner?.focusedByKeyboard = true
                    event.consume()
                }

                if (StageActions.NEXT_SECTION.matches(event)) {
                    scene.sectionNext()
                    scene.focusOwner?.focusedByKeyboard = true
                    event.consume()
                }

                if (StageActions.PREVIOUS_SECTION.matches(event)) {
                    scene.sectionPrevious()
                    scene.focusOwner?.focusedByKeyboard = true
                    event.consume()
                }
            }
        }

        // Check for default and cancel buttons if ENTER / ESCAPE were pressed
        (scene.focusOwner ?: scene.root).let { start ->

            fun findButton(filter: (Button) -> Boolean): Button? {
                fun findDownwards(parent: Node): Button? {
                    if (parent is Button && filter(parent)) return parent
                    for (child in parent.children) {
                        val found = findDownwards(child)
                        if (found != null) return found
                    }
                    return null
                }

                fun findUpwards(from: Node): Button? {
                    val parent = from.parent ?: return null
                    for (child in parent.children) {
                        if (child != from) {
                            val found = findDownwards(child)
                            if (found != null) return found
                        }
                    }
                    return findUpwards(parent)
                }

                var found = findDownwards(start)
                if (found != null) return found
                found = findUpwards(start)
                return found
            }

            // Close popups if ESCAPE pressed (and not consumed)
            if (event.eventType == EventType.KEY_PRESSED && ! event.isConsumed()) {
                if (StageActions.CANCEL.matches(event)) {
                    var foundPopup = false
                    for (overlay in regularStage.overlayStages) {
                        if (overlay.stageType == StageType.POPUP || overlay.stageType == StageType.POPUP_MENU) {
                            overlay.close()
                            foundPopup = true
                        }
                    }
                    if (! foundPopup) {
                        // Look for a Button with cancelButton == true
                        findButton { it.cancelButton }?.onAction?.handle(ActionEvent())
                    }
                }

                if (StageActions.DEFAULT_BUTTON.matches(event)) {
                    // Look for a Button with defaultButton == true
                    findButton { it.defaultButton }?.onAction?.handle(ActionEvent())
                }
            }
        }
    }

    fun onKeyTyped(event: KeyTypedEvent) {
        scene?.focusOwner?.let { focusOwner ->
            bubbleEvent(event, focusOwner)
        }
    }

    fun onMouseMove(event: MouseEvent) {
        // Close the tooltip if there is one showing.
        regularStage.tooltipStage?.close()
        RegularStage.lastMouseMovementTime = currentTimeMillis()
        if (regularStage.mouseInStage != this) {
            if (regularStage != this) {
                regularStage.onMouseEnterExit(entered = false)
            }
            for (overlayStage in regularStage.overlayStages) {
                if (overlayStage != this) {
                    overlayStage.onMouseEnterExit(entered = false)
                }
            }
            onMouseEnterExit(entered = true)
            regularStage.mouseInStage = this
        }

        val scene = scene ?: return
        val sceneX = event.sceneX
        val sceneY = event.sceneY
        savedMouseX = sceneX
        savedMouseY = sceneY
        val capturedByNode = capturedByNode

        val deepestNode = scene.root.findDeepestNodeAt(event.sceneX, event.sceneY)

        // Issue EventType.MOUSE_EXITED and MOUSE_ENTERED events
        // if the mouse has moved to a different node.
        if (deepestNode !== previousMouseInNode) {
            issueMouseEnterExit(deepestNode, sceneX, sceneY)
        }

        // Should we stop future button pressed/released events to fire a CLICK event?
        // (due to the mouse moving too far away)
        if (mouseClickPossible) {
            if (abs(mousePressedX - sceneX) > GlokSettings.clickDistanceThreshold &&
                abs(mousePressedY - sceneY) > GlokSettings.clickDistanceThreshold
            ) {
                mouseClickPossible = false
                mouseClickCount = 0
            }
        }

        if (capturedByNode != null) {
            if (!dragDetected) {
                if (abs(sceneX - dragStartX) > GlokSettings.dragDistanceThreshold || abs(sceneY - dragStartY) > GlokSettings.dragDistanceThreshold) {
                    dragDetected = true
                }
            }
            if (dragDetected) {
                val dragEvent = MouseEvent(
                    EventType.MOUSE_DRAGGED, event.scene, sceneX, sceneY, event.buttonIndex, 0, event.mods
                )
                capturedByNode.findEventFilter(dragEvent.eventType)?.tryCatchHandle(event)
                if (!dragEvent.isConsumed()) {
                    capturedByNode.findEventHandler(dragEvent.eventType)?.tryCatchHandle(event)
                }
            }
        } else {
            bubbleEvent(event, deepestNode)
        }
    }

    private fun issueMouseEnterExit(deepestNode: Node, sceneX: Float, sceneY: Float) {
        val scene = scene ?: return
        val prev = previousMouseInNode
        val captured = capturedByNode

        val commonAncestor = commonAncestor(prev, deepestNode)
        val exitedEvent = MouseEvent(
            EventType.MOUSE_EXITED, scene, sceneX, sceneY, - 1, 0, 0
        )
        val enteredEvent = MouseEvent(
            EventType.MOUSE_ENTERED, scene, sceneX, sceneY, - 1, 0, 0
        )

        if (captured != null) {
            // We should only issue entered/exited events to the captured node.

            val nowInsideCaptured = captured.containsScenePoint(sceneX, sceneY)
            val wasInsideCaptured = prev?.firstToRoot { it === captured } === captured

            // Entered captured?
            if (nowInsideCaptured && ! wasInsideCaptured) {
                captured.findEventHandler(enteredEvent.eventType)?.tryCatchHandle(enteredEvent)
            }
            // Exited captured?
            if (! nowInsideCaptured && wasInsideCaptured) {
                captured.findEventHandler(exitedEvent.eventType)?.tryCatchHandle(exitedEvent)
            }

        } else {
            // Issue mouse exit events to prev, and its ancestors up to (but excluding) the common ancestor
            prev?.firstToRoot { node ->
                if (node === commonAncestor) {
                    true
                } else {
                    node.findEventHandler(exitedEvent.eventType)?.tryCatchHandle(exitedEvent)
                    false
                }
            }

            // Issue mouse enter events to deepestNode and its ancestors,
            // up to (but excluding) the common ancestor
            deepestNode.firstToRoot { node ->
                if (node === commonAncestor) {
                    true
                } else {
                    node.findEventHandler(enteredEvent.eventType)?.tryCatchHandle(enteredEvent)
                    false
                }
            }
        }

        previousMouseInNode = deepestNode
    }

    fun onMouseButton(event: MouseEvent) {
        val scene = scene ?: return
        val sceneX = event.sceneX
        val sceneY = event.sceneY

        // Close (other) OverlayStages when the mouse is pressed outside them.
        if (event.eventType === EventType.MOUSE_PRESSED) {
            for (overlay in regularStage.overlayStages) {

                if (overlay.stageType == StageType.POPUP && overlay !== this) {
                    overlay.close()

                } else if (overlay.stageType == StageType.POPUP_MENU && (this as? OverlayStage)?.stageType != StageType.POPUP_MENU) {
                    // Don't close other POPUP_MENUs when we click a POPUP_MENU.
                    overlay.close()
                }
            }
        }

        var clickEvent: MouseEvent? = null

        // Set up for detecting if this is part of a CLICK
        if (event.eventType == EventType.MOUSE_PRESSED) {
            // Look to the deepest node at the mouse position which has a MOUSE_CLICKED handler.
            // By ignoring nodes without a click handler, the parent (or other ancestor) can take responsibility
            // for the double click. e.g. In StyledTextArea, there are Text nodes (without click handlers)
            // which are ignored, and instead `node` is the StyledTextArea.
            val deepest = scene.findDeepestNodeAt(sceneX, sceneY)
            val node = deepest?.firstToRoot { it.findEventHandler(EventType.MOUSE_CLICKED) != null }

            val now = currentTimeMillis()
            if (now > mousePressedMillis + GlokSettings.clickTimeThreshold ||
                abs(mousePressedX - sceneX) > GlokSettings.clickDistanceThreshold ||
                abs(mousePressedY - sceneY) > GlokSettings.clickDistanceThreshold ||
                mousePressedNode != node ||
                mouseButton != event.button
            ) {
                mouseClickCount = 0
            }
            mouseButton = event.button
            mousePressedX = sceneX
            mousePressedY = sceneY
            mouseClickPossible = true
            mousePressedNode = node
            mousePressedMillis = now
        }

        // Check to see if a MOUSE_CLICKED event should be sent.
        if (event.eventType == EventType.MOUSE_RELEASED) {
            if (mouseClickPossible &&
                currentTimeMillis() - mousePressedMillis < GlokSettings.clickTimeThreshold &&
                abs(mousePressedX - sceneX) < GlokSettings.clickDistanceThreshold &&
                abs(mousePressedY - sceneY) < GlokSettings.clickDistanceThreshold
            ) {
                mouseClickCount++
                clickEvent = MouseEvent(
                    EventType.MOUSE_CLICKED,
                    event.scene,
                    sceneX, sceneY,
                    event.buttonIndex,
                    mouseClickCount,
                    event.mods
                )
            } else {
                mouseClickCount = 0
            }
        }

        val deepestNode = capturedByNode ?: scene.root.findDeepestNodeAt(sceneX, sceneY)
        bubbleEvent(event, deepestNode)

        if (clickEvent != null) {
            bubbleEvent(clickEvent, deepestNode)
        }

        if (event.eventType == EventType.MOUSE_PRESSED) {
            dragStartX = event.sceneX
            dragStartY = event.sceneY
            dragDetected = false
        } else {
            capturedByNode = null
        }
    }

    fun onDroppedFiles(event: DroppedFilesEvent) {
        val scene = scene ?: return
        val deepest = scene.findDeepestNodeAt(event.sceneX, event.sceneY) ?: return
        bubbleEvent(event, deepest)
    }

    fun onScroll(event: ScrollEvent) {
        val scene = scene ?: return
        val deepest = scene.findDeepestNodeAt(event.sceneX, event.sceneY) ?: return
        bubbleEvent(event, deepest)
    }
    // endregion events

    // region ==== Bubble ====
    private fun <E : Event> bubbleDownwards(event: E, bottomNode: Node) {
        val isPress = event.eventType == EventType.MOUSE_PRESSED

        @Suppress("UNCHECKED_CAST")
        val eventType = event.eventType as EventType<E>
        val mouseEvent = event as? MouseEvent

        bottomNode.forEachFromRoot { node ->
            if (! event.isConsumed()) {
                node.findEventFilter(eventType)?.tryCatchHandle(event)
                if (isPress && mouseEvent?.captured == true) {
                    capturedByNode = node
                    mouseEvent.captured = false
                }
            }
        }
    }

    private fun <E : Event> bubbleUpwards(event: E, bottomNode: Node) {
        val isPress = event.eventType == EventType.MOUSE_PRESSED
        val mouseEvent = event as? MouseEvent

        @Suppress("UNCHECKED_CAST")
        val eventType = event.eventType as EventType<E>

        if (! event.isConsumed()) {
            bottomNode.forEachToRoot { node ->
                if (! event.isConsumed()) {
                    node.findEventHandler(eventType)?.tryCatchHandle(event)
                    if (isPress && mouseEvent?.captured == true) {
                        capturedByNode = node
                        mouseEvent.captured = false
                    }
                }
            }
        }
    }

    private fun <E : Event> bubbleEvent(event: E, bottomNode: Node) {
        bubbleDownwards(event, bottomNode)
        bubbleUpwards(event, bottomNode)
    }
    // endregion bubble

    // region == StageActions ==
    /**
     * These actions are used for their KeyCombination only.
     * The tests are performed after the normal onKeyPressed bubbling, and therefore if a Node consumes the
     * event, the action won't be performed.
     * So if you change the key combination, you should also make sure that none of your application controls
     * use, and consume it.
     */
    object StageActions : Actions(null) {
        /**
         * ENTER key.
         *
         * Look for a button with `defaultButton == true`, and if found, perform its action.
         */
        val DEFAULT_BUTTON = define("activate_default_button", "Activate Default Button", Key.ENTER.noMods())

        /**
         * ESCAPE key
         *
         * Close any popups.
         * If there were none, then look for a button with `cancelButton == true`, and if found, perform its action.
         */
        val CANCEL = define("escape", "ESCAPE", Key.ESCAPE.noMods())

        /**
         * TAB key (with or without control)
         *
         * TAB and Ctrl+TAB both match, so that in a TextArea, the user can still use Ctrl+TAB, but elsewhere,
         * can use just TAB.
         */
        val FOCUS_NEXT = define("focus_next", "Focus Next", Key.TAB.noMods().maybeControl())

        /**
         * Shift+TAB key (with or without control)
         *
         * Shift+TAB and Ctrl+Shift+TAB both match, so that in a TextArea, the user can use Ctrl+Shift+TAB, but elsewhere,
         * can use just Shift+TAB.
         */
        val FOCUS_PREVIOUS = define("focus_previous", "Focus Previous", Key.TAB.shift().maybeControl())

        /**
         * Similar to [FOCUS_NEXT], but instead of moving one Node at a time, we jump to the next "section".
         * A section is a large group of nodes, such as a `ToolBar`, `MenuBar`, `Dock`, `Harbour Side` etc.
         */
        val NEXT_SECTION = define("next_section", "Next Section", Key.F6.noMods())

        /**
         * The opposite of [NEXT_SECTION].
         */
        val PREVIOUS_SECTION = define("previous_section", "Previous Section", Key.F6.shift())

    }
    // endregion
}

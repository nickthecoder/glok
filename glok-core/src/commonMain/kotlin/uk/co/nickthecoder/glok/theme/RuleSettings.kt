package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.SpinnerArrowPositions
import uk.co.nickthecoder.glok.property.StylableProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.text.Font
import kotlin.reflect.KClass

expect fun findStylableProperty(
    node: Any,
    valueType: KClass<*>,
    propertyName: String,
    selector: Selector
): StylableProperty<*>?
/**
 * Looks for a method with this signature : get{PropertyName}Property() : StylableProperty<*>
 * If one is found, invokes the method, and returns the value.
 * Otherwise, return null.
 *
 * Note, [node] is of type [Any], because it _could_ also be used to style other objects
 * e.g. For [Scene], to set [Scene.backgroundColor] (which is a [StylableProperty]).
 */


class RuleSettings {

    private val settingsByType = mutableMapOf<KClass<*>, MutableMap<String, Any>>()

    /**
     * Note, [node] is of type [Styled], because it is almost _always_ a [Node], but for the [RootSelector],
     * it will apply to a [Scene].
     */
    internal fun createSettersForNode(
        node: Styled,
        selector: Selector,
        importance: Int,
        order: Int
    ): Map<StylableProperty<*>, ValueAndImportance> {

        val result = mutableMapOf<StylableProperty<*>, ValueAndImportance>()
        for ((type, map) in settingsByType) {

            for ((key, value) in map) {
                val property = findStylableProperty(node, type, key, selector)
                if (property != null) {
                    result[property] = ValueAndImportance(value, importance, order)
                }
            }

        }
        return result
    }

    fun <T : Any> add(type: KClass<*>, name: String, value: T) {
        var typedSettings = settingsByType[type]
        if (typedSettings == null) {
            typedSettings = mutableMapOf()
            settingsByType[type] = typedSettings
        }
        typedSettings[name] = value
    }

    fun addFloat(name: String, value: Float) = add(Float::class, name, value)
    fun addOptionalFloat(name: String, value: Float) =
        add(Float::class, name, value)

    fun addInt(name: String, value: Int) = add(Int::class, name, value)
    fun addBoolean(name: String, value: Boolean) = add(Boolean::class, name, value)
    fun addString(name: String, value: String) = add(String::class, name, value)
    fun addColor(name: String, value: Color) = add(Color::class, name, value)
    fun addEdges(name: String, value: Edges) = add(Edges::class, name, value)
    fun addPos(name: String, value: Alignment) = add(Alignment::class, name, value)
    fun addOrientation(name: String, value: Orientation) = add(Orientation::class, name, value)
    fun addImage(name: String, value: Image) = add(Image::class, name, value)
    fun addContentDisplay(name: String, value: ContentDisplay) = add(ContentDisplay::class, name, value)
    fun addSpinnerArrowPositions(name: String, value: SpinnerArrowPositions) =
        add(SpinnerArrowPositions::class, name, value)

    fun addBackground(name: String, value: Background) = add(Background::class, name, value)
    fun addBorder(name: String, value: Border) = add(Border::class, name, value)
    fun addFont(name: String, value: Font) = add(Font::class, name, value)


    override fun toString() = StringBuilder().apply {

        var first = true
        for (typedSettings in settingsByType.values) {
            for ((key, value) in typedSettings) {
                if (first) {
                    append("\n")
                    first = false
                }
                append("    ")
                if (key in standardProperties) {
                    append(key)
                } else {
                    append("\"$key\")")
                }
                append("( ")
                if (value is Color) {
                    append("\"$value\"")
                } else {
                    append(value)
                }
                append(" )")
                append("\n")
            }
        }

    }.toString()

    companion object {

        /**
         * When we use [Theme.dump], standard properties should be displayed as simple method calls.
         * Without this, we would have lots of unnecessary double quotes e.g.
         *
         *     "label" { "width"( 32.0 ) }
         *
         * instead of :
         *
         *     "label" { width( 32.0 ) }
         */
        val standardProperties =
            listOf(
                // Node
                "background", "growPriority", "shrinkPriority",
                "forceMinWidth", "forceMinHeight",
                "forcePrefWidth", "forcePrefHeight",
                "forceMaxWidth", "forceMaxHeight",
                // HBox, VBox
                "spacing", "margin", "", "textColor",
                // Labelled
                "font", "textColor"
            )
    }

}

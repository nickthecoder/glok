package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.scene.dsl.checkAlreadySet

interface NodeParent {
    operator fun Node.unaryPlus()
}

/**
 * Implemented by [Node] subclasses whose [children] are public, and mutable.
 */
interface WithChildren : NodeParent {
    val children: MutableObservableList<Node>

    override operator fun Node.unaryPlus() {
        this@WithChildren.children.add(this)
    }
}

/**
 * Implemented by [Node] subclasses with a list of Nodes called [items], which are mutable.
 */
interface WithItems : NodeParent {
    val items: MutableObservableList<Node>

    override operator fun Node.unaryPlus() {
        this@WithItems.items.add(this)
    }
}

/**
 * Implemented by [Node] subclasses with a settable child called `content`.
 */
interface WithContent {
    var content: Node?

    operator fun Node.unaryPlus() {
        checkAlreadySet(content, "content")
        content = this
    }
}

package uk.co.nickthecoder.glok.scene

object NoBackground : Background {
    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {}

    override fun toString() = "NoBackground"

}

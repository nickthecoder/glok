/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.collections.syncWith
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.lrToVertical
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.box
import uk.co.nickthecoder.glok.scene.dsl.scrollPaneWithButtons
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.TABS_CONTAINER
import uk.co.nickthecoder.glok.theme.styles.TAB_BAR

/**
 * A list of [Tab]s displayed in a row or column.
 *
 * Clicking on a [Tab] sets [currentContent] to that tab's [Tab.content] and nothing more.
 * The content is NOT part of the [TabBar].
 * To see the tab's contents, use a [SingleContainer], and bind its [SingleContainer.contentProperty] to
 * [TabBar.currentContent]. The pair will then act as a traditional tab-pane.
 *
 * [TabPane] combines a [TabBar] and a [SingleContainer], but greater flexibility can be had by keeping them separate.
 * (and not using a [TabPane]).
 *
 * For example
 *
 *     lateinit var tabBar: TabBar
 *     borderPane {
 *         top {
 *             toolBar {
 *                 items {
 *                     tabBar {
 *                         tabs {
 *                             tab("Hello") {
 *                                 content { label("Greetings") }
 *                             }
 *                             tab("World") {
 *                                 content { label("World's content") }
 *                             }
 *                         }
 *                     }
 *                     // This is impossible with a regular TabPane.
 *                     button( "Add Tab" )
 *                     button( "Tabs Settings" )
 *                 }
 *             }
 *         }
 *         center {
 *             singleContainer(tabBar.currentContentProperty)
 *         }
 *     }
 *
 */
class TabBar : Region() {

    //region ==== Properties ====

    val sideProperty by sideProperty(Side.TOP)
    var side by sideProperty

    val alignmentProperty by stylableAlignmentProperty(Alignment.CENTER_CENTER)
    var alignment by alignmentProperty

    val fillProperty by stylableBooleanProperty(true)
    var fill by fillProperty

    val orientationProperty = sideProperty.lrToVertical()
    val orientation by orientationProperty

    val tabClosingPolicyProperty by tabClosingPolicyProperty(TabClosingPolicy.UNAVAILABLE)
    var tabClosingPolicy by tabClosingPolicyProperty

    private val mutableCurrentContentProperty by optionalNodeProperty(null)
    val currentContentProperty: ObservableOptionalNode = mutableCurrentContentProperty.asReadOnly()
    val currentContent by currentContentProperty

    //endregion


    //region ==== Field ====

    /**
     * Nodes which appear in the ToolBar. Most commonly [Button]s, but can be any [Node]s.
     */
    val tabs = mutableListOf<Tab>().asMutableObservableList()

    val selection = SingleSelectionModel(tabs)

    private val tabsBox = box(side.lrToVertical()) {
        style(TABS_CONTAINER)
        orientationProperty.bindTo(sideProperty.lrToVertical())
        alignmentProperty.bindTo(this@TabBar.alignmentProperty)
        fillProperty.bindTo(this@TabBar.fillProperty)
    }

    private val tabsScrollPaneWithButtons = scrollPaneWithButtons {
        content = tabsBox
        orientationProperty.bindTo(this@TabBar.orientationProperty)
    }

    @Suppress("unused")
    private val syncTabs = tabsBox.children.syncWith(tabs)

    override val children: ObservableList<Node> = listOf(tabsScrollPaneWithButtons).asObservableList()

    //endregion

    init {
        style(TAB_BAR)
        section = true
        pseudoStyles.add(side.pseudoStyle)

        sideProperty.addChangeListener { _, old, new ->
            pseudoStyles.remove(old.pseudoStyle)
            pseudoStyles.add(new.pseudoStyle)
        }

        selection.selectedItemProperty.addChangeListener { _, _, tab ->
            mutableCurrentContentProperty.unbind()
            if (tab == null) {
                mutableCurrentContentProperty.value = null
            } else {
                mutableCurrentContentProperty.bindTo(tab.contentProperty)

                Platform.runLater {
                    // Using runLater, because the tabs may change size/position when tabClosingPolicy
                    // is SELECTED_TAB.
                    tabsScrollPaneWithButtons.scrollTo(tab)
                }
            }

        }

        tabs.addChangeListener { _, change ->
            for (tab in change.removed) {
                val closed = ActionEvent()
                tab.onClosed?.handle(closed)
                tab.mutableTabBarProperty.value = null
            }
            for (tab in change.added) {
                tab.mutableTabBarProperty.value = this
            }
            if (selection.selectedItem == null) {
                selection.selectedItem = tabs.firstOrNull { ! it.disabled }
            }
        }

        claimChildren()
        children.addChangeListener(childrenListener)

    }

    /**
     * Syntactic sugar for adding a [Tab]
     */
    operator fun Tab.unaryPlus() {
        tabs.add(this)
    }

    //region ==== Layout ====
    override fun nodeMinWidth() = surroundX() + tabsScrollPaneWithButtons.evalMinWidth()
    override fun nodeMinHeight() = surroundY() + tabsScrollPaneWithButtons.evalMinHeight()
    override fun nodePrefWidth() = surroundX() + tabsScrollPaneWithButtons.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + tabsScrollPaneWithButtons.evalPrefHeight()
    override fun nodeMaxWidth() = NO_MAXIMUM
    override fun nodeMaxHeight() = NO_MAXIMUM

    override fun layoutChildren() {
        setChildBounds(tabsScrollPaneWithButtons, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
    //endRegion
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.action.Action
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.VBox
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.scene.dsl.vBox
import uk.co.nickthecoder.glok.theme.styles.TOOLTIP

/**
 * When the mouse remains still for a short period of time over a [Node] with a `tooltip`,
 * the `tooltip` is displayed as a `POPUP`.
 *
 * Unlike JavaFX, [Tooltip] is _not_ a GUI element, it holds a _description_ of the `tooltip`,
 * and must build the `POPUP` when required.
 *
 * [TextTooltip] is a simple implementation (and currently the only implementation).
 * However, you are free to create your own implementation(s).
 */
interface Tooltip {
    fun show(node: Node, mouseX: Float, mouseY: Float): Stage?
}


/**
 * A [Tooltip] which takes [text], and splits it by line breaks, placing each line as a [Label] in a [VBox].
 * The `Tantalum` theme renders it with a rounded corners, and padding.
 */
class TextTooltip(val text: String) : Tooltip {

    override fun show(node: Node, mouseX: Float, mouseY: Float): Stage? {
        return node.scene?.stage?.let { parentStage ->
            overlayStage(parentStage, StageType.POPUP) {
                scene {
                    root = vBox {
                        style(TOOLTIP)
                        for (line in text.split("\n")) {
                            + Label(line)
                        }
                    }
                }
                show(mouseX, mouseY + 16f)
            }
        }
    }
}

class ActionTooltip(val action: Action) : Tooltip {

    override fun show(node: Node, mouseX: Float, mouseY: Float): Stage? {
        return node.scene?.stage?.let { parentStage ->
            val text = if (action.tooltip.isNullOrBlank()) {
                action.text
            } else {
                action.tooltip
            }
            overlayStage(parentStage, StageType.POPUP) {
                scene {
                    root = vBox {
                        style(TOOLTIP)
                        text?.let { text ->
                            for (line in text.split("\n")) {
                                + Label(line)
                            }
                        }
                        action.keyCombination?.let { keyCombo ->
                            + label("Shortcut : ${keyCombo.displayText}") {
                                style(".key_combination")
                            }
                        }
                    }
                }
                show(mouseX, mouseY + 16f)
            }
        }
    }

}

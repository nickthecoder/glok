package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.text.TextDocument
import uk.co.nickthecoder.glok.theme.styles.TEXT_AREA
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

/**
 * A multi-line text editor.
 *
 * See [TextField] for a single line text editor.
 *
 * JavaFX's TextArea, stores the `document` as a simple String, and the caret position as a simple Int.
 * This make editing longish documents very expensive.
 *
 * So Glok stores the [document] as an array of [lines] (each line is a simple String).
 * The [caretPosition] and [anchorPosition] are [TextPosition]s, which are composed of a `row` and `column`.
 *
 * The same [TextDocument] can be shared by more than one [TextArea], giving multiple views of the same document.
 * (Changes to one will be seen in the other).
 *
 * TextArea has been optimised to handle long documents.
 * It has _not_ been optimised to handle extremely long lines.
 *
 * ### Theme DSL
 *
 *     "text_area" {
 *
 *         lineHeight( number )
 *         prefColumnCount( int )
 *         prefRowColumn( int )
 *
 *         ":focus" { ... }
 *
 *         ":read_only" { ... }
 *
 *         child( "scroll_pane" ) {
 *             child( ".viewport" ) {
 *                 child( ".container" ) { // Or use "descendant" rather than 3 levels of nesting.
 *                     // The text is drawn here
 *                 }
 *             }
 *         }
 *     }
 *
 * TextArea inherits all the features of [TextInputControl].
 */
/* PERFORMANCE
   This implementation has been optimised for large documents.
   Most operations, such as typing, moving the carat etc. are O(1)
   with respect to the size of the document. O(n) with respect to LINE length.
   I've tested a 500,000+ line document : no loss in responsiveness.

   AFAIK (without actually testing), the biggest bottlenecks are :
   1. Altering the widest line of text (measured in pixels).
      This causes maxLineWidth to be recalculated, which is O(lines.size).
   2. Hmm, I can't think of another one!
   Other 'slow' operations, which I believe are O(n) with respect to text.length
   1. Getting/setting text.
   2. Any text changes when textProperty is being used. (Never use this with large documents!)
   3. Changing font (all line widths need recalculating)
*/
class TextArea(

    document: TextDocument

) : TextAreaBase<TextDocument>(document) {

    constructor(text: String = "") : this(TextDocument(text))
    constructor(stringProperty: StringProperty) : this(TextDocument("")) {
        textProperty.bindTo(stringProperty)
    }

    override val container: Container = TextAreaContainer()

    init {
        style(TEXT_AREA)
        scrolled.children.add(1, container) // After the left gutter
    }

    // region == inner class TextAreaContainer ==

    /**
     * Note, unlike [StyledTextArea], this Node has no children, and the text is drawn
     * in [draw]. There are no [Text] objects.
     */
    private inner class TextAreaContainer : Container() {

        // Bad code warning! This looked so neat before I optimised it, now it's a mess.
        // In hindsight, I should have rewritten it from scratch, with optimisation in mind.
        // Along these lines...
        //    Position to the first visible row.
        //    Is the current row in the selection.
        //        Is it the same row as the end of the selection
        //    Where is the next boundary
        override fun draw() {
            super.draw()

            val x = sceneX + surroundLeft()
            val top = sceneY + surroundTop()
            var y = top

            val fullLineHeight = font.height * lineHeight

            // Draw selection rectangles.
            if (textSelected) {
                val from = selectionStart
                val to = selectionEnd
                val fromX = x + widthOf(document.lines[from.row].substring(0, from.column))
                val fromY = y + from.row * fullLineHeight
                if (to.row == from.row) {
                    // Only a single line of selection
                    val toX = x + widthOf(document.lines[from.row].substring(0, to.column))
                    backend.fillRect(
                        fromX, fromY, toX, fromY + fullLineHeight, highlightColor
                    )
                } else {
                    // More than 1 line of selection
                    // First line, from selection start to right edge of the container
                    backend.fillRect(
                        fromX, fromY, sceneX + width - surroundRight(), fromY + fullLineHeight, highlightColor
                    )
                    // Middle lines of the selection (the whole width of the container)
                    val startY = y + (from.row + 1) * fullLineHeight
                    val endY = y + (to.row) * fullLineHeight
                    backend.fillRect(
                        x, startY, sceneX + width - surroundRight(), endY, highlightColor
                    )
                    // Last line of selection
                    val endX = x + widthOf(document.lines[to.row].substring(0, to.column))
                    val rectTop = y + to.row * fullLineHeight
                    backend.fillRect(
                        x, rectTop, endX, rectTop + fullLineHeight, highlightColor
                    )
                }
            }

            // Add half of the extra "lineHeight" to the top.
            y = top + (lineHeight - 1) * font.height / 2

            // Draw text

            // We only need to draw the text that's within the viewport
            val visibleBounds = scrollPane.visibleContentBounds()
            val firstVisibleRow = max(0, (visibleBounds.top / fullLineHeight - 1).toInt())
            val lastVisibleRow = min(document.lines.size, (visibleBounds.bottom / fullLineHeight + 1).toInt())

            // Do we need special handling to change the color of text due to a selection?
            if (textSelected && textColor != highlightTextColor) {
                // Yes, we need to draw the lines before and after the selection in one color,
                // and the lines of the selection in a different color.
                // We also need to treat the first and last lines of the selection carefully,
                // because they contain 2 colors.

                val selectionStart = selectionStart
                val selectionEnd = selectionEnd

                // Lines before the selection
                val selectionStartRow = selectionStart.row
                val skipTo = min(selectionStartRow, firstVisibleRow)
                y += skipTo * fullLineHeight
                for (i in firstVisibleRow until selectionStartRow) {
                    if (i < lastVisibleRow) { // Could optimise better, by changing the for loop
                        font.drawTopLeft(document.lines[i], textColor, x, y, indentation.columns)
                    }
                    y += fullLineHeight
                }
                // Lines of the selection
                val startLine = document.lines[selectionStartRow]

                if (selectionStartRow == selectionEnd.row) {
                    if (selectionStartRow >= firstVisibleRow) {
                        drawLineText(x, y, startLine, selectionStart.column, selectionEnd.column)
                    }
                    y += fullLineHeight
                } else {
                    // Line at the start of the selection
                    if (selectionStartRow >= firstVisibleRow) {
                        drawLineText(x, y, startLine, selectionStart.column, startLine.length)
                    }
                    y += fullLineHeight
                    // Complete lines in the middle of the selection
                    val endRow = min(selectionEnd.row, lastVisibleRow)
                    for (i in selectionStartRow + 1 until endRow) {
                        if (i > firstVisibleRow) {
                            drawText(document.lines[i], x, y, highlightTextColor)
                        }
                        y += fullLineHeight
                    }
                    // Line at the end of the selection
                    drawLineText(x, y, document.lines[selectionEnd.row], 0, selectionEnd.column)
                    y += fullLineHeight
                }
                // Lines after the selection
                for (i in selectionEnd.row + 1 until lastVisibleRow) {
                    if (i >= firstVisibleRow) {
                        font.drawTopLeft(document.lines[i], textColor, x, y, indentation.columns)
                    }
                    y += fullLineHeight
                }

            } else {
                y += fullLineHeight * firstVisibleRow
                for (i in firstVisibleRow until lastVisibleRow) {
                    val line = document.lines[i]
                    font.drawTopLeft(line, textColor, x, y, indentation.columns)
                    y += fullLineHeight
                }
            }

            if (this@TextArea.focused && caretVisible) {
                val caretX = sceneX + caretX()
                val caretY = sceneY + caretY()
                caretImage?.let { image ->
                    val scale = fullLineHeight / image.imageHeight
                    val width = image.imageWidth * scale
                    val height = image.imageHeight * scale
                    image.drawTo(caretX - width / 2, caretY, width, height, caretColor)
                }
            }
        }

    }

    // endregion Container

}

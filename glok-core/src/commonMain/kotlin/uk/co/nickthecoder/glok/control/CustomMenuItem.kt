/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.WithContent
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.CUSTOM_MENU_ITEM

/**
 * Allows any [Node] to be added to a [Menu] or [SubMenu], and for it to still behave like a menu item. i.e.
 *
 * 1. It will have a `:hover` pseudo class applied when the mouse moves over it
 * 2. [onAction] is run when clicked.
 *
 */
class CustomMenuItem : MenuItemBase(), WithContent {

    private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children = mutableChildren.asReadOnly()

    val contentProperty by optionalNodeProperty(null)
    override var content by contentProperty

    init {
        style(CUSTOM_MENU_ITEM)
        mutableChildren.addChangeListener(childrenListener)
        contentProperty.addChangeListener { _, old, new ->
            old?.let { mutableChildren.remove(it) }
            new?.let { mutableChildren.add(it) }
        }
    }

    override fun nodeMinWidth() = surroundX() + (content?.evalMinWidth() ?: 0f)
    override fun nodeMinHeight() = surroundX() + (content?.evalMinHeight() ?: 0f)
    override fun nodePrefWidth() = surroundX() + (content?.evalPrefWidth() ?: 0f)
    override fun nodePrefHeight() = surroundX() + (content?.evalPrefHeight() ?: 0f)
    override fun nodeMaxWidth() = surroundX() + (content?.evalMaxWidth() ?: 0f)
    override fun nodeMaxHeight() = surroundX() + (content?.evalMaxHeight() ?: 0f)

    override fun layoutChildren() {
        content?.let {
            setChildBounds(it, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
        }
    }
}

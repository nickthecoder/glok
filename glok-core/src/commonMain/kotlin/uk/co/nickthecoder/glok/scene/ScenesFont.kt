/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.property.LazyObservableValue
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalFont
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalScene
import uk.co.nickthecoder.glok.property.changeListener
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.Theme

/**
 * An [ObservableOptionalFont], whose value is the [Font] defined in the scene's theme ([Theme.defaultFont]).
 * [Node]s which do not have a font of their own use this to convert `ems` to [LogicalPixels].
 * See [Node.em].
 *
 * This is a little smelly. Maybe if/when `Theme`s support `em` units, this can be deprecated, and later deleted?
 */
internal class ScenesFont(sceneProperty: ObservableOptionalScene) : ObservableOptionalFont,
    LazyObservableValue<Font?>() {

    private var theme: Theme? = null

    private val themeListener = changeListener<Theme, ObservableValue<Theme>> { _, _, new ->
        theme = new
        invalidate()
    }
    private val sceneListener = sceneProperty.addChangeListener { _, old, new ->
        old?.themeProperty?.removeChangeListener(themeListener)
        new?.themeProperty?.addChangeListener(themeListener)
        invalidate()
    }

    init {
        theme = sceneProperty.value?.theme
    }

    override fun eval() = theme?.defaultFont

}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.RADIO_BUTTON

/**
 * A specialised form of [ToggleButtonBase], which must belong to a [ToggleGroup].
 * Unlike a [ToggleButton], when you press a selected [RadioButton], it remains selected.
 *
 * This class is similar in design to JavaFX's `RadioButton`, and was one of the first Glok controls.
 * I never use it. IMHO, the design is bad.
 * [PropertyRadioButton] is better (which has no JavaFX equivalent).
 * [PropertyRadioButton] doesn't use a [ToggleGroup], and instead uses a [Property]
 * to synchronise its state.
 *
 * FYI, for the youngsters out there, [RadioButton] is named after clunky, old-fashioned radios,
 * which had three buttons : `LW` (Long Wave), `MW` (Medium Wave) and `FM` (Frequency Modulated).
 * There were no modern electronics. When you press a button, a physical mechanism released the
 * previously selected button (usually with a loud mechanical click).
 * As a young lad, I loved playing with these buttons, till one day, I managed to get all three
 * buttons simultaneously depressed, and had to head sheepishly to my Dad, who, after berating me, took the radio
 * apart, and released the buttons by levering them with a screwdriver.
 * Decades later, we are stuck with this anachronism.
 *
 * ### Theme DSL
 *
 * RadioButton has no unique features, and inherits all the features of [ToggleButtonBase].
 *
 * See [ButtonBase] for a class diagram of Glok's button hierarchy.
 */
open class RadioButton(toggleGroup: ToggleGroup, text: String, graphic: Node? = null) :
    ToggleButtonBase(text, graphic, toggleGroup) {

    constructor(toggleGroup: ToggleGroup, text: String) : this(toggleGroup, text, null)

    /**
     * Unlike a [ToggleButton], we cannot toggle a [RadioButton] to its unselected state.
     * So this sets [selected] to `true` regardless of its current state.
     */
    override fun toggle() {
        selected = true
    }

    init {
        style(RADIO_BUTTON)
    }
}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.property.boilerplate.stylableBackgroundProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableBorderProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableColorProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableEdgesProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.REGION
import uk.co.nickthecoder.glok.theme.styles.TOOL_BAR

/**
 * A [Region] adds a [Border], and padding compared to a regular [Node].
 * Most controls use [Region] as a base class.
 *
 *          ◀─────────── width ───────────▶
 *
 *    ▲     ┌─────────────────────────────┐  ▲
 *    │     │           border            │  │ borderSize.top
 *    │     │   ┌─────────────────────┐   │  ▼
 *    │     │   │       padding       │   │
 *    │     │   │    ┌───────────┐    │   │
 *  height  │   │    │ <content> │    │   │
 *    │     │   │    └───────────┘    │   │  ▲
 *    │     │   │                     │   │  │ padding.bottom
 *    │     │   └─────────────────────┘   │  ▼
 *    │     │                             │
 *    ▼     └─────────────────────────────┘
 *
 * In this diagram the size of the border and padding are roughly the same on all sides,
 * but that isn't a requirement. Both [border] and [padding] use [Edges] to specify the
 * thickness of each edge.
 *
 * The size of the border is unrelated to the padding.
 *
 * Borders (and backgrounds) are rendered by the usual [draw] method.
 * Subclasses should call super first, so that the content appears on top of the background.
 *
 * There is nothing preventing a subclass drawing into the padding or border region
 * (either intentionally, or by mistake).
 *
 * ## Theme DSL
 *
 *         background( color : String )
 *
 *         borderSize( value : Edges )
 *         borderSize( size : Number )
 *         borderSize( topBottom : Number, leftRight : Number )
 *         borderSize( top : Number, right : Number, bottom : Number, left : Number )
 *
 *         border( value : Border )
 *
 *         plainBorder( color : Color )
 *         plainBorder( color : String )
 *
 *         ninePatchBorder( value : NinePatch )
 *
 *         padding( value : Edges )
 *         padding( size : Number )
 *         padding( topBottom : Number, leftRight : Number )
 *         padding( top : Number, right : Number, bottom : Number, left : Number )
 *     }
 *
 * Region inherits all the features of [Node].
 *
 */
open class Region : Node() {

    // ==== Properties ====

    val focusBorderColorProperty by stylableColorProperty(Color.TRANSPARENT)
    var focusBorderColor by focusBorderColorProperty

    /**
     * The focus border is only ever drawn when the focus is set using keyboard shortcuts.
     * So, when you use TAB or Shift+TAB, the extra [focusBorder] is drawn, but otherwise, it isn't drawn.
     * This gives an uncluttered most of the time (when TAB isn't used).
     */
    val focusBorderProperty by stylableBorderProperty(NoBorder)
    var focusBorder by focusBorderProperty

    /**
     * The border is drawn OUTSIDE the bounds given by [sceneX], [sceneY] -> [width] x [height],
     * and therefore doesn't add to the size of the Node.
     * Nodes which are at the edge of the scene will have one (or two) edges missing.
     */
    val focusBorderSizeProperty by stylableEdgesProperty(Edges.NONE)
    var focusBorderSize by focusBorderSizeProperty

    val focusHighlightColorProperty by stylableColorProperty(Color.TRANSPARENT)
    var focusHighlightColor by focusHighlightColorProperty

    /**
     * A [Background], which is drawn ON TOP of the node to highlight it.
     * Used to highlight a [section], when using the `F6` and `Shift+F6` keyboard shortcuts.
     *
     * The color should be semi-transparent, so that the contents of the node is still visible.
     */
    val focusHighlightProperty by stylableBackgroundProperty(NoBackground)
    var focusHighlight by focusHighlightProperty


    val borderSizeProperty by stylableEdgesProperty(Edges.NONE)
    var borderSize by borderSizeProperty

    val borderColorProperty by stylableColorProperty(Color.TRANSPARENT)
    var borderColor by borderColorProperty

    val borderProperty by stylableBorderProperty(NoBorder)
    var border by borderProperty

    val backgroundColorProperty by stylableColorProperty(Color.TRANSPARENT)
    var backgroundColor by backgroundColorProperty

    val backgroundProperty by stylableBackgroundProperty(NoBackground)
    var background by backgroundProperty

    val paddingProperty by stylableEdgesProperty(Edges())
    var padding by paddingProperty

    // ==== End of Properties ====


    // ==== End of fields ====

    init {
        style(REGION)

        for (prop in arrayOf(
            borderProperty, backgroundProperty, backgroundColorProperty, borderColorProperty,
            focusHighlightColorProperty, focusBorderColorProperty, focusHighlightColorProperty,
            focusHighlightProperty, focusBorderSizeProperty, focusedProperty
        )) {
            prop.addListener(requestRedrawListener)
        }
        for (prop in arrayOf(borderSizeProperty, paddingProperty)) {
            prop.addListener(requestLayoutListener)
        }

    }

    // ==== End of init ====

    override fun nodePrefWidth() = surroundX()

    override fun nodePrefHeight() = surroundY()

    /**
     * @return [padding].left + [border].left
     */
    fun surroundLeft() = padding.left + borderSize.left

    /**
     * @return [padding].left + [border].left
     */
    fun surroundRight() = padding.right + borderSize.right

    /**
     * @return [padding].top + [border].top
     */
    fun surroundTop() = padding.top + borderSize.top

    /**
     * @return [padding].bottom + [border].bottom
     */
    fun surroundBottom() = padding.bottom + borderSize.bottom

    /**
     * @return [padding].x + [border].x
     */
    fun surroundX() = padding.x + borderSize.x

    /**
     * @return [padding].y + [border].y
     */
    fun surroundY() = padding.y + borderSize.y

    override fun draw() {
        background.draw(sceneX, sceneY, width, height, backgroundColor, borderSize)
        border.draw(sceneX, sceneY, width, height, borderColor, borderSize)
    }

}

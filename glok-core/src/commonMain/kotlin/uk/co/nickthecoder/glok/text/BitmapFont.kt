/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.util.Matrix
import uk.co.nickthecoder.glok.util.clamp
import kotlin.math.abs
import kotlin.math.roundToInt

class BitmapFont(

    override val identifier: FontIdentifier,
    val texture: Texture,
    val glyphDataMap: Map<Char, BitmapGlyphData?>,
    override val height: Float,
    override val ascent: Float,
    override val descent: Float,
    override val top: Float,
    override val bottom: Float,
    override val italicInverseSlope: Float

) : Font {

    internal var throwGlyphMissing: Boolean = false

    private val averageWidth: Float = widthOfOrZero("o", 1)

    override val maxWidthOfDigit: Float by lazy {
        listOf("0", "1", "2", "3,", "4", "5", "6", "7", "8", "9", "-", ".").maxOf { widthOfOrZero(it, 0) }
    }

    override val fixedWidth: Float

    init {
        val w = widthOfOrZero("W", 0)
        fixedWidth = if (w == widthOfOrZero(".", 0)) w else 0f
    }

    override fun image() = texture

    /**
     * Calculate the carat position (`0`..`text.length` _inclusive_) for a given [x] in pixels
     * for a given [text].
     *
     * The current implementation is very inefficient when [text] is long - O(n²).
     * But for simple text fields, this should be sufficient.
     */
    override fun caretPosition(text: CharSequence, x: Float, indentationColumns: Int): Int {
        /*
         * We guess a position, based on an `average` width character, and calculate
         * the X position for that guess.
         * If the required position is to the right of our guess, then keep adding 1
         * to the guess until we hit the end of the text, or the previous guess is better than the
         * corrected guess.
         *
         * Ditto for the other direction.
         *
         * For long lines of text, this will be inefficient. e.g. if we have 1000 letter `i`,
         * then our guess may be off by 100s of characters (which is O(n), where n is the number of characters)
         * But widthOf() is O(n), where n is the length of the text.
         * So overall, it is O(n²). Eek - not good!
         */
        val textLength = text.length
        var guess = (x / averageWidth).roundToInt().clamp(0, textLength)
        val guessX = widthOfOrZero(text.subSequence(0, guess), indentationColumns)
        var diff = x - guessX
        if (diff > 0) {
            while (true) {
                // The required position is to the right of uur guess.
                val correctedGuess = guess + 1
                if (correctedGuess > textLength) {
                    return guess
                }
                val correctedGuessX = widthOfOrZero(text.subSequence(0, correctedGuess), indentationColumns)
                val correctedDiff = abs(correctedGuessX - x)
                if (diff <= correctedDiff) {
                    return guess
                }
                guess = correctedGuess
                diff = correctedDiff
            }
        } else if (diff < 0) {
            // The required position is to the left of Our guess.
            diff = -diff
            while (true) {
                val correctedGuess = guess - 1
                if (correctedGuess <= 0) {
                    return guess
                }
                val correctedGuessX = widthOfOrZero(text.subSequence(0, correctedGuess), indentationColumns)
                val diff2 = abs(correctedGuessX - x)
                if (diff <= diff2) {
                    return guess
                }
                guess = correctedGuess
                diff = diff2
            }
        } else {
            // Wow, spot on, what are the odds of that?
            return guess
        }
    }

    override fun widthOf(text: CharSequence, indentationColumns: Int, startColumn: Int): Float {
        if (text.isEmpty()) return 0f

        // Hopefully, this is an optimisation for TextArea when using a fixed width font.
        // Most other text will be virtually unaffected, because fixedWidth == 0
        if (fixedWidth != 0f && ! text.contains('\t')) {
            return text.length * fixedWidth
        }

        var total = glyphDataMap[text[0]]?.extraBoundsLeft ?: 0f
        var column = startColumn
        for (c in text) {
            if (c == '\t') {
                val extraColumns = indentationColumns - (column % indentationColumns)
                glyphDataMap[' ']?.let { total += it.advance * extraColumns }
                column = 0 // We only care about modulo. We are at an indentation boundary.
            } else {
                if (throwGlyphMissing && ! glyphDataMap.containsKey(c)) {
                    throw GlyphMissingException(c)
                }
                val glyphData = glyphDataMap[c] ?: glyphDataMap['W']
                if (glyphData != null) {
                    total += glyphData.advance
                }
                column ++
            }
        }
        return total
    }

    override fun hasGlyph(c: Char) = glyphDataMap.containsKey(c)

    override fun drawTopLeft(
        text: CharSequence, color: Color, x: Float, y: Float,
        indentationColumns: Int, startColumn: Int,
        modelMatrix: Matrix?
    ) {
        if (text.isEmpty()) return
        // Ensure that the glyphs are aligned with pixel boundaries, to prevent blurriness.
        // For this to work, it assumes that the advance of each bitmap glyph is a whole number.
        val alignedX = x.roundToInt().toFloat() + (glyphDataMap[text[0]]?.extraBoundsLeft ?: 0f)
        val alignedY = y.roundToInt().toFloat()

        backend.batch(texture, color, modelMatrix) {
            var column = startColumn
            var currentX = alignedX
            val height = maxHeight
            for (c in text) {
                if (c == '\t') {
                    val extraColumns = indentationColumns - (column % indentationColumns)
                    glyphDataMap[' ']?.let { currentX += it.advance * extraColumns }
                    column = 0 // We only care about modulo. We are at an indentation boundary.
                } else {
                    val glyphData = glyphDataMap[c]
                    if (glyphData == null) {
                        if (throwGlyphMissing && !glyphDataMap.containsKey(c)) {
                            throw GlyphMissingException(c)
                        }
                    } else {
                        draw(
                            glyphData.left,
                            glyphData.baseline - top,
                            glyphData.width,
                            height,
                            currentX - glyphData.extraBoundsLeft,
                            alignedY
                        )
                        currentX += glyphData.advance
                    }
                    column ++
                }
            }
        }
    }

    companion object {

        /**
         * A Set of characters which are pre-rendered by all [BitmapFont]s.
         */
        internal val requiredGlyphs = mutableSetOf<Char>()

        init {
            requireGlyphs(32..255)
            requireGlyphs("¢£¥₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₶₷₸₹₺") // Currency symbols
            requireGlyphs("…¡¤¦§¨©«¬®¯°±²³µ¶¹»¼½¾¿×÷") // Other symbols (some of these may be in 32..255?)
        }

        /**
         * All [BitmapFont]s created will pre-render glyphs for these characters.
         * The default is the range 32..255, plus some currency symbols and other characters outside this range.
         *
         * If characters outside the default range are used, then font-metrics may be wrong and the characters
         * will not be rendered.
         * However, Glok will rebuild the font, so that subsequent use of the font _will_ work correctly.
         *
         * If you _know_ your application will use glyphs outside the default range, then adding them early,
         * will improve performance (as the fonts won't need to be rebuilt on-the-fly).
         */
        fun requireGlyphs(charRange: CharRange) {
            for (c in charRange) {
                requiredGlyphs.add(c)
            }
        }

        fun requireGlyphs(intRange: IntRange) {
            for (i in intRange) {
                requiredGlyphs.add(i.toChar())
            }
        }

        fun requireGlyphs(str: String) {
            for (c in str) {
                requiredGlyphs.add(c)
            }
        }

        fun requireGlyph(c: Char) {
            requiredGlyphs.add(c)
        }

    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.property

import uk.co.nickthecoder.glok.scene.Image
import uk.co.nickthecoder.glok.scene.ImageView

/**
 * A [SimpleProperty], which is also a [StylableProperty].
 * This is the only concrete implementation of [StylableProperty] used within Glok, but you are free
 * to create other implementations if you need to.
 */
abstract class SimpleStylableProperty<V>(initialValue: V, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<V>(initialValue, bean, beanName), StylableProperty<V> {

    private var manuallySet = false

    private var unstyledValue: V = initialValue

    override var value: V
        get() = super.value
        set(newValue) {
            manuallySet = true
            unstyledValue = newValue
            super.value = newValue
        }

    /**
     * A type UNSAFE way to set the value by te Theme.
     * If [newValue] is the wrong type, an [ClassCastException] will be thrown.
     */
    override fun style(newValue: Any) {
        if (! manuallySet && ! isBound()) {
            @Suppress("UNCHECKED_CAST")
            if (newValue is Image && beanName == "graphic") {
                // A special case for "graphic" property of Labelled.
                // The theme's value is an Image, but the property is a Node
                val oldImageView = (value as? ImageView)
                if (oldImageView == null) {
                    super.value = ImageView(newValue) as V
                } else {
                    oldImageView.image = newValue
                }
            } else {
                // May throw a ClassCastException
                super.value = newValue as V
            }
        }
    }

    override fun revert() {
        if (! manuallySet && ! isBound()) {
            super.value = unstyledValue
        }
    }
}


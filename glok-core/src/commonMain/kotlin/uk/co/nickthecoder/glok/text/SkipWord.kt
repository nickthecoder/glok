/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

/**
 * When you hold Ctrl and press the Left or Right arrow keys, it jumps by 1 `word`.
 * However, what is considered a `word` isn't clear, and can depend on the context.
 *
 * For example, to a programmer, a `word` is an _identifier_, which can have
 * numbers as well as digits. So `foo42` is a word.
 *
 * In a natural language context, a `word` may be anything which doesn't contain whitespace.
 *
 * Each of my implementations have an option `preSkipWhitespace`, which defaults to `true`.
 * Pretty much every text editor I've used pre-skips whitespace, and I hate it!
 * IMHO, skipping forwards should be symmetrical with skipping backwards,
 * with the _downside_, that `whitespace` is considered a word!
 * Therefore, it takes an extra key-press to skip it. But I can type faster than I can think.
 * I don't want to think about where the editor is going to jump to next, I want it to be
 * simple and symmetric (i.e. the same caret positions are used going forwards and backwards).
 * If I overshoot, then hitting the other arrow key should correct it.
 *
 * This is NOT the case when `preSkipWhitespace` = true. When using the left arrow,
 * the caret only visits the start of each `word`, and when using the right arrow, it only visits
 * the end of each word. IMHO, this sucks, but I assume I'm an outlier, because that's the default
 * (or only option) in *every* text editor I've seen.
 *
 * I'm going to stick with convention - Glok does it the way I hate!
 *
 * The default algorithm is [forProgrammers] (preSkipWhitespace=true)
 *
 * To change TextField's behaviour, set [instance] to [byWhiteSpaceOnly], [letterDigitOther] or [forProgrammers].
 *
 */
interface SkipWord {
    /**
     * @param position The _start_ position, which must be in the range 0 .. [text].length (inclusive)
     * @return The new caret position when jumping a word to the right.
     * This must be in the range `0 until text.length`
     */
    fun forwards(position: Int, text: CharSequence): Int

    /**
     * @param position The _start_ position, which must be in the range 0 .. [text].length (inclusive)
     * @return The new caret position when jumping a word to the left.
     * This must be in the range `0 until text.length`
     */
    fun backwards(position: Int, text: CharSequence): Int

    /**
     * @param position The _start_ position, which must be in the range 0 .. [text].length (inclusive)
     * @return the left and right caret position to select the word at the given caret position
     */
    fun selectWord(position: Int, text: CharSequence): Pair<Int, Int>

    companion object {

        val instance = forProgrammers(false)

        fun byWhiteSpaceOnly(preSkipWhitespace: Boolean = false) =
            SkipWordByCategory(preSkipWhitespace, listOf(
                { it.isWhitespace() },
                { !it.isWhitespace() }
            ))

        fun letterDigitOther(preSkipWhitespace: Boolean = false) =
            SkipWordByCategory(preSkipWhitespace, listOf(
                { it.isWhitespace() },
                { it.isLetter() },
                { it.isDigit() },
                { !(it.isLetterOrDigit() || it.isWhitespace()) }
            ))

        fun forProgrammers(preSkipWhitespace: Boolean = false) =
            SkipWordByCategory(preSkipWhitespace, listOf(
                { it.isWhitespace() },
                { it.isLetterOrDigit() || it == '_' },
                { !(it.isLetterOrDigit() || it == '_' || it.isWhitespace()) }
            ))
    }
}

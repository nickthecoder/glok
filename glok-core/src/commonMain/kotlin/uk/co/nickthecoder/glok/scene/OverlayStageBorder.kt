package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.property.boilerplate.ObservableColor

/**
 * Similar to [RoundedBorder], but instead of filling the whole of the edges, only fill a thin line
 * (given by [thickness]).
 * Everything outside the thin line remains un-drawn.
 *
 * Used by `WindowDecoration` of `OverlayStage`
 *
 * The dashed outer rectangle is the Node's bounds.
 * We draw the region between the two solid lines
 * Therefore the region between the dashed lines and the solid lines remain un-drawn.
 *
 *      (x,y)
 *        ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐   ▲  ▲
 *        ┊   ╭━━━━━━━━━━━━━╮ ┊   ┊  ┆ size.top  ▲ thickness
 *        ┊   ┃ ╭━━━━━━━━━╮ ┃ ┊   ┊  ▼           ▼
 *        ┊   ┃ ┃         ┃ ┃ ┊   ┊
 *        ┊   ┃ ┃         ┃ ┃ ┊   ┊height
 *        ┊   ┃ ┃         ┃ ┃ ┊   ┊
 *        ┊   ┃ ╰━━━━━━━━━╯ ┃ ┊   ┊  ▲
 *        ┊   ╰━━━━━━━━━━━━━╯ ┊   ┊  │ size.bottom
 *        └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘   ▼  ▼
 *
 *        ◀┄┄┄┄┄┄┄┄width┄┄┄┄┄┄▶
 *        ◀┄┄┄┄▶           ◀┄╌▶
 *        size.left         size.right

 *
 * The distance between the dotted outer rectangle and the inner-most solid rectangle are given by the Edges (named "size").
 * These are often equal (i.e. top == left == right == bottom ). In this diagram, left is larger than the others.
 * By comparison, the distance between the two solid rectangles are always the same (as they are a single float [thickness]).
 */
class OverlayStageBorder(radius: Float, val thickness: Float) : Border {

    private val actualBorder = RoundedBorder(radius)

    private val actualEdges = Edges(thickness)

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        actualBorder.draw(
            x + size.left - thickness,
            y + size.top - thickness,
            width - size.left - size.right + thickness * 2,
            height - size.top - size.bottom + thickness * 2,
            color,
            actualEdges
        )
    }

    override fun toString() = "OverlayStageBorder( ${actualBorder.radius}, $thickness )"
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.orientationProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.WithChildren

/**
 * Like a [VBox] or [HBox], but the [orientation] can be changed from horizontal to vertical.
 */
class Box(orientation: Orientation) : BoxBase(), WithChildren {

    // ==== Properties ====

    val orientationProperty by orientationProperty(orientation)
    public override val orientation by orientationProperty

    val fillProperty by booleanProperty(false)
    public override var fill by fillProperty

    // ==== End of Properties ====

    override val children = mutableListOf<Node>().asMutableObservableList().apply {
        addChangeListener(childrenListener)
    }

    // ==== End of Fields ====

    init {
        orientationProperty.addListener(requestLayoutListener)
        fillProperty.addListener(requestLayoutListener)
    }

    // ==== End of init ====


    // ==== Object overrides ====

    override fun toString() = super.toString() + " $orientation spacing($spacing)"

}

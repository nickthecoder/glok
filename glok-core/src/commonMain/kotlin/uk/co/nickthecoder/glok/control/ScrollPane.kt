package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.backend.GlokRect
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableScrollBarPolicyProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.WithContent
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.CORNER
import uk.co.nickthecoder.glok.theme.styles.SCROLL_PANE
import uk.co.nickthecoder.glok.theme.styles.VIEWPORT
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.max

/**
 * Displays a portion of a [content] node, with scrollbars to pan.
 * Some Glok controls have a built-in ScrollPanes (e.g. [TextArea], [ListView]).
 *
 */
class ScrollPane(content: Node? = null) : Region(), WithContent, Scrollable {

    // region ==== Properties ====

    val contentProperty by optionalNodeProperty(null)
    override var content by contentProperty

    private val hScroll = ScrollBar(Orientation.HORIZONTAL)
    private val vScroll = ScrollBar(Orientation.VERTICAL)
    private val viewport = Viewport()
    private val corner = Region().apply {
        styles.add(CORNER)
    }
    val hScrollMaxProperty = hScroll.maxProperty.asReadOnly()
    val hScrollMax by hScrollMaxProperty

    val vScrollMaxProperty = vScroll.maxProperty.asReadOnly()
    val vScrollMax by vScrollMaxProperty

    val hScrollValueProperty = hScroll.valueProperty
    var hScrollValue by hScrollValueProperty

    val vScrollValueProperty = vScroll.valueProperty
    var vScrollValue by vScrollValueProperty

    /**
     * If [content] is narrower than the viewport, should it be expanded to fill the whole width of the viewport?
     */
    val fitToWidthProperty by booleanProperty(true)
    var fitToWidth by fitToWidthProperty

    /**
     * If [content] is taller than the viewport, should it be expanded to fill the whole height of the viewport?
     */
    val fitToHeightProperty by booleanProperty(true)
    var fitToHeight by fitToHeightProperty


    val hPolicyProperty by stylableScrollBarPolicyProperty(ScrollBarPolicy.AS_NEEDED)
    var hPolicy by hPolicyProperty

    val vPolicyProperty by stylableScrollBarPolicyProperty(ScrollBarPolicy.AS_NEEDED)
    var vPolicy by vPolicyProperty

    // endregion properties

    // region ==== Fields ====
    override val children = listOf(viewport, hScroll, vScroll, corner).asObservableList()

    // endregion fields

    // region ==== Commands ====
    val commands = Commands().apply {
        with(ScrollPaneActions) {
            PAGE_UP { vScroll.scrollBy(viewport.height * - 0.8f) }
            PAGE_DOWN { vScroll.scrollBy(viewport.height * 0.8f) }
            HOME { vScroll.value = vScroll.min }
            END { vScroll.value = vScroll.max }
        }
        attachTo(this@ScrollPane)
    }
    // endregion commands

    // region ==== init ====

    init {
        style(SCROLL_PANE)
        claimChildren()
        this.content = content

        for (prop in listOf(
            hScroll.valueProperty, vScroll.valueProperty,
            fitToWidthProperty, fitToHeightProperty, hPolicyProperty, vPolicyProperty
        )) {
            prop.addListener(requestLayoutListener)
        }

        onScrolled { event ->
            hScroll.scrolledBy(event.deltaX)
            vScroll.scrolledBy(event.deltaY)
        }

        growPriority = 1f
    }
    // endregion init

    // region ==== Public Methods ====

    /**
     * The width the ScrollPane would have to be, so that the viewport could display the full width of the content.
     */
    fun fullWidth() = (content?.evalPrefWidth() ?: 0f) + surroundX() + viewport.surroundX() +
        if (vPolicy == ScrollBarPolicy.NEVER) 0f else vScroll.evalPrefWidth()

    /**
     * The height the ScrollPane would have to be, so that the viewport could display the full height of the content.
     */
    fun fullHeight() = (content?.evalPrefHeight() ?: 0f) + surroundY() + viewport.surroundY() +
        if (hPolicy == ScrollBarPolicy.NEVER) 0f else hScroll.evalPrefHeight()

    /**
     * Ensures that [descendant] is fully visible.
     * [descendant] must be a descendant of [content].
     *
     * Note. This currently does NOT work correctly with descendants of transformed nodes, such as [Rotation].
     * Sorry, it is on my to-do list, but is low priority.
     */
    override fun scrollTo(descendant: Node) {
        content?.let { content ->
            scrollToX(descendant.sceneX + descendant.width - content.sceneX)
            scrollToX(descendant.sceneX - content.sceneX)

            scrollToY(descendant.sceneY + descendant.height - content.sceneY)
            scrollToY(descendant.sceneY - content.sceneY)
        }
    }

    /**
     * @param contentX An x position local to [content]. i.e. 0 is the left of the content.
     */
    fun scrollToX(contentX: Float) {
        val bounds = visibleContentBounds()
        val left = bounds.left - contentX
        if (left > 0) {
            hScrollValue = (hScrollValue - left).clamp(0f, hScrollMax)
        } else {
            val right = contentX - bounds.right
            if (right > 0) {
                hScrollValue = (hScrollValue + right).clamp(0f, hScrollMax)
            }
        }
    }

    /**
     * @param contentY A y position local to [content]. i.e. 0 is the top of the content.
     */
    fun scrollToY(contentY: Float) {
        val bounds = visibleContentBounds()
        val top = bounds.top - contentY
        if (top > 0) {
            vScrollValue = (vScrollValue - top).clamp(0f, vScrollMax)
        } else {
            val bottom = contentY - bounds.bottom
            if (bottom > 0) {
                vScrollValue = (vScrollValue + bottom).clamp(0f, vScrollMax)
            }
        }
    }

    /**
     * The bounds of the visible region of [content], relative to content's local coordinates.
     * i.e. a value of (0,0) would be the top left of the content.
     */
    fun visibleContentBounds(): GlokRect {
        content?.let { content ->
            val left = - content.localX
            val top = - content.localY
            return GlokRect(left, top, left + viewport.width, top + viewport.height)
        }
        return GlokRect()
    }
    // endregion public methods

    // region ==== Layout ====

    override fun nodePrefWidth() = surroundX() + viewport.evalPrefWidth() +
        if (vPolicy == ScrollBarPolicy.NEVER) 0f else vScroll.evalPrefWidth()

    override fun nodePrefHeight() = surroundY() + viewport.evalPrefHeight() +
        if (hPolicy == ScrollBarPolicy.NEVER) 0f else hScroll.evalPrefHeight()

    override fun layoutChildren() {
        var scrollWidth = vScroll.evalPrefWidth()
        var scrollHeight = hScroll.evalPrefHeight()
        val contentWidth = content?.evalPrefWidth() ?: 0f
        val contentHeight = content?.evalPrefHeight() ?: 0f

        hScroll.visible = when (hPolicy) {
            ScrollBarPolicy.NEVER -> false
            ScrollBarPolicy.ALWAYS -> true
            ScrollBarPolicy.AS_NEEDED -> {
                // Let's assume the vertical scroll bar WILL be visible for now.
                val availableWidth = width - surroundX() - viewport.surroundX() - scrollWidth
                contentWidth > availableWidth
            }
        }
        vScroll.visible = when (vPolicy) {
            ScrollBarPolicy.NEVER -> false
            ScrollBarPolicy.ALWAYS -> true
            ScrollBarPolicy.AS_NEEDED -> {
                var availableHeight = height - surroundY() - viewport.surroundY()
                if (hScroll.visible) availableHeight -= scrollHeight
                contentHeight > availableHeight
            }
        }
        // Now let's check the hScroll again if the vScroll is not visible, there may be just enough room now.
        if (!vScroll.visible && hScroll.visible && hPolicy == ScrollBarPolicy.AS_NEEDED) {
            val availableWidth = width - surroundX() - viewport.surroundX()
            hScroll.visible = (contentHeight <= availableWidth)
        }

        if (!hScroll.visible) {
            scrollHeight = 0f
            // We used to shrink the viewport, so that it didn't overlap the scroll bars
            // Now we don't because the scroll bars can be semi-transparent (or invisible)
            //viewportHeight += scrollHeight
        }
        if (!vScroll.visible) {
            scrollWidth = 0f
            //viewportWidth += scrollWidth
        }
        // For old-fashioned scrollbars, which are opaque, set the corner's size.
        // For new-style semi-transparent scrollbars, leave it as zero
        val cornerWidth = corner.evalPrefWidth()
        val cornerHeight = corner.evalPrefHeight()

        val viewportWidth = width - surroundX() - cornerWidth
        val viewportHeight = height - surroundY() - cornerHeight

        setChildBounds(vScroll, width - surroundRight() - scrollWidth, surroundTop(), scrollWidth, viewportHeight)
        setChildBounds(hScroll, surroundLeft(), height - surroundBottom() - scrollHeight, viewportWidth, scrollHeight)
        setChildBounds(
            corner,
            width - surroundRight() - cornerWidth, height - surroundBottom() - cornerHeight,
            cornerWidth, cornerHeight
        )
        setChildBounds(viewport, surroundLeft(), surroundTop(), viewportWidth, viewportHeight)

        viewport.layoutViewport()
    }
    // endregion layout

    // region ==== Object Methods ====

    override fun toString() = super.toString() + " (${hScroll.value} , ${vScroll.value})"
    // endregion

    // region == Viewport ==
    private inner class Viewport : Region() {

        /**
         * Will only ever contain zero or 1 nodes ([content])
         */
        private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
        override val children = mutableChildren.asReadOnly()

        init {
            styles.add(VIEWPORT)

            contentProperty.addChangeListener { _, old, new ->
                old?.let {
                    it.parent = null
                    it.scene = null
                    mutableChildren.remove(it)
                }
                new?.let {
                    mutableChildren.add(it)
                    claimChildren()
                }
                viewport.requestLayout()
            }
        }

        fun layoutViewport() {
            content?.let { content ->
                val availableWidth = width - surroundX() - if (vPolicy == ScrollBarPolicy.NEVER) 0f else vScroll.evalPrefWidth()
                val availableHeight = height - surroundY() - if (hPolicy == ScrollBarPolicy.NEVER) 0f else hScroll.evalPrefHeight()

                val contentPrefWidth = content.evalPrefWidth()
                val contentPrefHeight = content.evalPrefHeight()

                with(hScroll) {
                    min = 0f
                    max = max(0f, contentPrefWidth - availableWidth)
                    visibleAmount = (availableWidth / contentPrefWidth) * (max - min)
                    blockIncrement = availableWidth
                    unitIncrement = blockIncrement / 10f
                    value = value.clamp(min, max)
                }
                with(vScroll) {
                    min = 0f
                    max = max(0f, contentPrefHeight - availableHeight)
                    visibleAmount = (availableHeight / contentPrefHeight) * (max - min)
                    blockIncrement = availableHeight
                    unitIncrement = blockIncrement / 10f
                    value = value.clamp(min, max)
                }

                val newWidth = if (fitToWidth && contentPrefWidth < availableWidth) {
                    availableWidth
                } else {
                    contentPrefWidth
                }
                val newHeight = if (fitToHeight && contentPrefHeight < availableHeight) {
                    availableHeight
                } else {
                    contentPrefHeight
                }
                setChildBounds(
                    content, surroundLeft() - hScroll.value, surroundTop() - vScroll.value,
                    newWidth, newHeight
                )

            }
        }

        override fun nodePrefWidth() = surroundX() + (content?.evalPrefWidth() ?: 0f)
        override fun nodePrefHeight() = surroundY() + (content?.evalPrefHeight() ?: 0f)

        override fun layoutChildren() {
        }

        override fun drawChildren() {
            backend.clip(sceneX, sceneY, width, height) {
                super.drawChildren()
            }
        }

    }
    // endregion viewport

    // region == ScrollPaneActions==
    object ScrollPaneActions : Actions(null) {

        val HOME = define("home", "Home", Key.HOME.maybeControl())
        val PAGE_UP = define("page_up", "Page Up", Key.PAGE_UP.noMods())
        val PAGE_DOWN = define("page_down", "Page Down", Key.PAGE_DOWN.noMods())
        val END = define("end", "End", Key.END.maybeControl())

    }
    // endregion ScrollPaneActions
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.scene.Node

/**
 * An interface for any Nodes which can scroll, or hide nodes from view by other means.
 *
 * Implementations :
 * * [ScrollPane] is the most obvious implementation, and adjusts its scrollbars, so that the node is viewable
 * * [TabBar] selects the tab which contains the node.
 * * [TitledPane] will expand its content (if the node is part of the content).
 * * `Dock` will make themselves visible.
 */
interface Scrollable {

    /**
     * Ensure that [descendant] is visible on screen.
     *
     * @param descendant Must be a descendant node of `this` Node.
     */
    fun scrollTo(descendant: Node)
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.action

import uk.co.nickthecoder.glok.event.KeyCombination
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.property.boilerplate.optionalKeyCombinationProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty

/**
 * [Action] defines an action that the user can make, such a saving the current document.
 * It does NOT define _HOW_ that action is performed, that is the job of [Command].
 *
 * An [Action] defines the text, icon and keyCombination (AKA keyboard shortcut).
 * Actions are grouped using the [Actions] class.
 */
class Action(
    val actions : Actions,
    val name: String,
    text: String,
    keyCombination: KeyCombination? = null,
    var tooltip: String? = null
) {

    /**
     * The text that will appear in buttons / menu items
     */
    val textProperty by stringProperty(text)
    var text by textProperty

    val keyCombinationProperty by optionalKeyCombinationProperty(keyCombination)
    var keyCombination by keyCombinationProperty

    var iconName: String = name

    /**
     * For monochrome icons, which must be tinted with the fontColor.
     */
    var tinted: Boolean = false
    var alternateIconName: String? = null

    /**
     * Additional key combinations which are _not_ displayed in tooltips, nor in menu items.
     */
    val additionalKeyCombinations = mutableListOf<KeyCombination>()

    fun add(vararg keyCombinations: KeyCombination) {
        for (kc in keyCombinations) {
            additionalKeyCombinations.add(kc)
        }
    }

    fun matches(event: KeyEvent) = keyCombination?.let { event.matches(it) } == true ||
        additionalKeyCombinations.firstOrNull { event.matches(it) } != null

    override fun toString() = "Action $name : $text ${keyCombination?.displayText}"

}

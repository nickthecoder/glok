package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.util.log

/**
 * Finds the next/previous focusable [Node] in the scene graph.
 *
 * This was ported from FXEssentials (helper utilities for JavaFX),
 * with only minor tweaks to make it work in Glok.
 *
 * The original only went forwards (next).
 */
internal object FocusTraversableWalker {

    /**
     * Assuming the parent/child relationship is valid, then we should always get back to
     * the [startNode] (assuming the scene has no focusable nodes).
     * But this is a safety measure, just in case there's a bug, it will prevent an infinite loop.
     * If we navigate through many nodes, without getting back to the [startNode],
     * then we abort.
     */
    var maxIterations = 1000

    var debug: Boolean = false

    /**
     * Starts at [maxIterations], and when it hits 0, we abort.
     */
    private var iterationCountdown: Int = 0

    private lateinit var startNode: Node

    private fun findNextInsideParent(parent: Node, usingSections: Boolean): Node? {
        if (debug) log.trace("findNextInsideParent : $parent")
        if (-- iterationCountdown == 0) {
            if (debug) {
                log.warn("FocusTraversableWalker : Max iterations exceeded.")
            }
            return startNode
        }

        for (child in parent.children) {
            if (child.visible) {
                findNextFocusNode(child, usingSections)?.let { return it }
            }
        }

        if (debug) log.trace("findNextInsideParent found nothing for $parent")
        return null
    }

    private fun findPrevInsideParent(parent: Node, usingSections: Boolean): Node? {

        if (-- iterationCountdown == 0) {
            if (debug) {
                log.warn("FocusTraversableWalker : Max iterations exceeded.")
            }
            return startNode
        }

        for (child in parent.children.asReversed()) {
            if (child.visible) {
                findPrevFocusNode(child, usingSections)?.let { return it }
            }
        }

        return null
    }

    private fun findNextFocusNode(node: Node, usingSections: Boolean): Node? {
        if (debug) log.trace("findNextFocusNode : $node")
        if (-- iterationCountdown <= 0) return startNode

        if (node === startNode) {
            return startNode
        }

        if (! node.visible) return null

        if (usingSections) {
            if (node.section) {
                if (debug) log.trace("Success! $node")
                return node // Success!
            }
        } else {
            if (node.focusTraversable) {
                if (debug) log.trace("Success! $node")
                return node // Success!
            }
        }
        if (debug) log.trace("Going inside parent node $node")
        findNextInsideParent(node, usingSections)?.let { return it }
        return null
    }

    private fun findPrevFocusNode(node: Node, usingSections: Boolean): Node? {
        if (debug) log.trace("Trying node $node")
        if (-- iterationCountdown <= 0) return startNode

        if (node === startNode) {
            return startNode
        }

        if (! node.visible) return null

        if (usingSections) {
            if (node.section) {
                if (debug) log.trace("Success! $node")
                return node // Success!
            }
        } else {
            if (node.focusTraversable) {
                if (debug) log.trace("Success! $node")
                return node // Success!
            }
        }
        if (debug) log.trace("Going inside parent node $node")
        findPrevInsideParent(node, usingSections)?.let { return it }
        return null
    }


    private fun findNextFromNode(node: Node, usingSections: Boolean): Node? {
        val parent = node.parent ?: return null
        if (! parent.visible) return null

        val children = parent.children
        val idx = children.indexOf(node)
        if (idx >= 0) {
            if (debug) log.trace("Trying all siblings after $node")
            for (i in idx + 1 until children.size) {
                findNextFocusNode(children[i], usingSections)?.let { return it }
            }
            if (debug) log.trace("Done all siblings after $node")
        }
        // We have looked at all the later siblings and their descendants

        // Now look at PARENT's later siblings
        if (debug) log.trace("Trying from parent node $parent")
        findNextFromNode(parent, usingSections)?.let { return it }

        if (parent.parent == null) {
            // We've looked at all the LATER nodes, lets look at the earlier nodes
            if (debug) log.trace("Trying ealier nodes")
            for (i in 0 until idx) {
                findNextFocusNode(children[i], usingSections)?.let { return it }
            }
            if (debug) log.trace("Not in earlier nodes either. Damn!")
        }

        return null
    }

    private fun findPrevFromNode(node: Node, usingSections: Boolean): Node? {
        val parent = node.parent ?: return null
        if (! parent.visible) return null

        val children = parent.children
        val idx = children.indexOf(node)
        if (idx >= 0) {
            if (debug) log.trace("Trying all siblings before $node")
            for (i in idx - 1 downTo 0) {
                findPrevFocusNode(children[i], usingSections)?.let { return it }
            }
            if (debug) log.trace("Done all siblings before $node")
        }
        // We have looked at all the earlier siblings and their descendants

        // Now look at PARENT's earlier siblings
        if (debug) log.trace("Trying from parent node $parent")
        findPrevFromNode(parent, usingSections)?.let { return it }

        if (parent.parent == null) {
            // We've looked at all the EARLIER nodes, lets look at the later nodes
            if (debug) log.trace("Trying earlier nodes")
            for (i in children.size - 1 downTo idx + 1) {
                findPrevFocusNode(children[i], usingSections)?.let { return it }
            }
            if (debug) log.trace("Not in later nodes either. Damn!")
        }

        return null
    }

    fun next(start: Node, usingSections: Boolean = false): Node? {
        if (debug) log.trace("Start at $start")
        iterationCountdown = maxIterations

        // It is vital that we don't start from an invisible node, otherwise the cycle won't
        // loop back to the [startNode], and there would be a danger of infinite looping.
        val visibleStart = start.firstToRoot { it.visible } ?: return null
        if (debug) log.trace("visibleStart = $visibleStart")

        startNode = visibleStart
        return findNextInsideParent(visibleStart, usingSections) ?: findNextFromNode(visibleStart, usingSections)
    }

    fun previous(start: Node, usingSections: Boolean = false): Node? {
        if (debug) log.trace("Start at $start")
        iterationCountdown = maxIterations

        // It is vital that we don't start from an invisible node, otherwise the cycle won't
        // loop back to the [startNode], and there would be a danger of infinite looping.
        val visibleStart = start.firstToRoot { it.visible } ?: return null
        if (debug) log.trace("visibleStart = $visibleStart")

        startNode = start
        return findPrevInsideParent(visibleStart, usingSections) ?: findPrevFromNode(visibleStart, usingSections)
    }

}

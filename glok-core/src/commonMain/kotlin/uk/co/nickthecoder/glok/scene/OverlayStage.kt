/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.tryCatchHandle
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.util.clamp

/**
 * A [Stage] which is not backed by a native `Window`, but instead lives within a [RegularStage].
 *
 * Using OverlayStages have pros and cons.
 *
 * Pros
 * * Doesn't open a second OpenGL backed window.
 * * If the main stage is full-screen, there's no issues creating OverlayStages.
 * * Consistent behaviour across all platforms. For example, you are free to place the window wherever you want.
 *   But using a [RegularStage] (and a native window), the operating system may constrain the placement.
 *
 * Cons
 * * The window decorations are drawn by Glok, and therefore won't match native windows.
 * * OverlayStages are always within the bounds of the parent [RegularStage].
 * * Cannot be minimised/maximised.
 * * Do not appear in your operating system's task bar (though that could be considered a pro in some cases).
 *
 * The coordinate system of the [OverlayStage] is the same as the [RegularStage] that contains it.
 * i.e. (0,0) is the top left of the [RegularStage] (not _this_ stage).
 *
 * ## Beware Scene.root is weird
 *
 * Because the window decoration of [OverlayStage]s are drawn by Glok (and not by the operating system),
 * the window decorations are part of the scene.
 * Let's support we create a scene containing just a `Label`. As soon as the scene is placed within
 * an [OverlayStage], a `WindowDecoration` will be inserted into the scene graph. So now we have :
 *
 *     Scene
 *         root = WindowDecoration
 *             content = Label
 *
 * Suppose we now set the root to be a `TextArea`. The window decoration will reinsert itself giving :
 *
 *     Scene
 *         root = WindowDecoration
 *             content = TextArea
 *
 * This is only an issue if you _read_ the value of the `Scene.root`, and expect it to be your content
 * (i.e. the Label or TextArea etc).
 *
 * ## Beware Scene's size
 *
 * Because the `WindowDecoration` is part of the scene graph, the space available to the content is smaller
 * than the scene's size.
 * For example, if you create a scene with size 600x400, then the space for the actual content
 * (such as the Label or TextArea) will be smaller.
 * In the X direction by the WindowDecoration's border and padding (which aren't very big).
 * But in the Y direction the title bar will also take up space (which is much bigger).
 *
 * So, if you wish to give a set amount of space for a scene, don't set the scene's size directly, and
 * instead ensure the content node is the size you require. The scene will be larger by the appropriate
 * amount to accommodate the title bar and edges.
 */
class OverlayStage(

    val parent: Stage,
    val stageType: StageType

) : StageBase() {

    // region ==== Properties ====

    override val focusedProperty: ObservableBoolean =
        parent.regularStage.windowFocusedProperty and parent.regularStage.mutableFocusedStageProperty.sameInstance(this)
    override val focused: Boolean by focusedProperty

    // endregion

    // region ==== Fields ====

    override val regularStage: RegularStage = parent.regularStage

    internal var visible = false

    fun moveTo(x: Float, y: Float) {
        regularStage.scene?.let { regularScene ->
            scene?.let { scene ->
                // Ensure that POPUP and POPUP_MENUs are fully visible.
                // Other stage types may overlay left, bottom and right edges, so that at least
                // marginX or marginBottom of the overlay is visible.
                val marginX = 50f
                val bottomMargin = 30f
                val width = scene.width
                val height = scene.height

                val clampedX = if (stageType.allowOverlap) {
                    x.clamp(-width + marginX, regularScene.width - marginX)
                } else {
                    x.clamp(0f, regularScene.width - width)
                }
                val clampedY = if (stageType.allowOverlap) {
                    y.clamp(0f, regularScene.height - bottomMargin)
                } else {
                    y.clamp(0f, regularScene.height - height)
                }
                scene.root.sceneX = clampedX
                scene.root.sceneY = clampedY
                scene.root.requestLayout()
            }
        }
    }

    internal val windowDecoration =
        if (stageType == StageType.POPUP || stageType == StageType.POPUP_MENU) null else WindowDecoration(this)
    // endregion fields

    // Brings this overlay to the front when it gains focus.
    private val focusListener = focusedProperty.addChangeListener { _, _, isFocused ->
        if (isFocused) {
            val reg = regularStage
            if (reg.overlayStages.contains(this) && reg.overlayStages.last() !== this) {
                reg.mutableOverlayStages.remove(this)
                reg.mutableOverlayStages.add(this)
                reg.scene?.requestRedraw
            }
        }
    }

    /**
     * Binds/unbinds [mousePointerProperty] with [RegularStage.actualMousePointerProperty] as the mouse enters/exits
     * this stage.
     */
    override fun onMouseEnterExit(entered: Boolean) {
        regularStage.actualMousePointerProperty.unbind()
        if (entered) {
            regularStage.actualMousePointerProperty.bindTo(mousePointerProperty)
        } else {
            regularStage.actualMousePointerProperty.bindTo(regularStage.mousePointerProperty)
        }
        super.onMouseEnterExit(entered)
    }

    override fun show() {
        val scene = scene ?: return
        if (regularStage.overlayStages.contains(this)) {
            visible = true
            return
        }
        preShow()
        regularStage.scene?.let { regularScene ->
            postShow((regularScene.width - scene.width) / 2, (regularScene.height - scene.height) / 2)
        }

    }

    fun show(nearNode: Node) {
        val regularScene = nearNode.scene?.stage?.regularStage?.scene

        val side = if (regularScene == null) {
            Side.BOTTOM
        } else {
            val above = nearNode.sceneY
            val below = regularScene.root.height - (nearNode.sceneY + nearNode.height)
            if (above > below) {
                Side.TOP
            } else {
                Side.BOTTOM
            }
        }
        show(nearNode, side)
    }

    fun show(nearNode: Node, side: Side) {
        val position = when (side) {
            Side.BOTTOM -> Pair(nearNode.sceneX, (nearNode.sceneY + nearNode.height))
            Side.TOP -> Pair(nearNode.sceneX, (nearNode.sceneY - scene !!.root.evalPrefHeight()))
            Side.RIGHT -> Pair((nearNode.sceneX + nearNode.width), nearNode.sceneY)
            Side.LEFT -> Pair((nearNode.sceneX - scene !!.root.evalPrefWidth()), nearNode.sceneY)
        }
        show(position.first, position.second)
    }

    /**
     * Shows this [OverlayStage] with the top left of the stage at [x],[y] relative to
     * the [RegularStage] (in [LogicalPixels]).
     */
    fun show(x: Float, y: Float) {
        if (regularStage.overlayStages.contains(this)) {
            visible = true
            return
        }
        preShow()
        postShow(x, y)
    }

    private fun postShow(x: Float, y: Float) {
        val scene = scene ?: return

        regularStage.mutableOverlayStages.add(this)

        moveTo(x, y)
        // We don't want tooltips, and similar ephemeral things to have focus by default.
        if (stageType != StageType.POPUP) {
            if (scene.focusOwner == null) {
                scene.root.requestFocus(true)
            }
        }
        visible = true
        regularStage.focusOnTopmostStage()
    }

    override fun hide() {
        onMouseEnterExit(entered = false)
        visible = false
        regularStage.focusOnTopmostStage()
    }

    override fun setIcon(images: List<Image?>, tint: Color?) {
        // Not supported yet.
    }

    internal fun closeNow() {
        onMouseEnterExit(entered = false)

        regularStage.mutableOverlayStages.remove(this)
        regularStage.focusOnTopmostStage()
        onClosed?.tryCatchHandle(ActionEvent())
        scene = null
    }

    override fun resize(width: Float, height: Float) {
        scene?.let { scene ->
            scene.width = width
            scene.height = height
            regularStage.scene?.requestRedraw = true
            scene.requestLayout = true
        }
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedFloatProperty
import uk.co.nickthecoder.glok.property.functions.div
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.property.functions.toDouble
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.SLIDER
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.max2DPs

/**
 * Edits a [FloatProperty] by typing digits into [TextArea], via up/down buttons, up/down keys
 * or by scrolling the mouse/trackpad.
 *
 * The range of values are bounded ([min] and [max]).
 *
 * For more details, see the base classes [SliderBase].
 */
class FloatSlider : SliderBase() {

    // region ==== Properties ====

    val valueProperty by validatedFloatProperty(0f) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    var value by valueProperty

    val minProperty by floatProperty(0f)
    var min by minProperty

    val maxProperty by floatProperty(100f)
    var max by maxProperty

    val smallStepProperty by floatProperty(1f)
    var smallStep by smallStepProperty

    val largeStepProperty by floatProperty(10f)
    var largeStep by largeStepProperty

    /**
     * When null, the slider will transition smoothly across the range. However, you can
     * set this, so that the value will snap to particular values.
     */
    val snapProperty: Property<Snap<Float>?> = SimpleProperty(null)
    var snap by snapProperty

    val markers: MutableObservableList<Float> = mutableListOf<Float>().asMutableObservableList()
    val minorMarkers: MutableObservableList<Float> = mutableListOf<Float>().asMutableObservableList()

    // endregion

    // region ==== Fields ====

    override var ratio: Double
        get() = ratio(value)
        set(v) {
            val convertedValue = if (max <= min) min else min + ((max - min) * v).toFloat()
            value = snap?.snap(convertedValue) ?: convertedValue
        }

    // endregion fields

    // region ==== init ====

    init {
        style(SLIDER)

        // When these properties change, we need to redraw this node.
        for (prop in listOf(minProperty, maxProperty, valueProperty)) {
            // NOTE. We are setting the thumb's position in drawChildren,
            // so we do NOT need to requestLayout, only requestRedraw.
            prop.addListener(requestRedrawListener)
        }
        markers.addListener(requestRedrawListener)
        minorMarkers.addListener(requestRedrawListener)

        smallRatioStepProperty.bindTo((smallStepProperty / (maxProperty - minProperty)).toDouble())
        largeRatioStepProperty.bindTo((largeStepProperty / (maxProperty - minProperty)).toDouble())
    }

    // endregion init

    override fun drawChildren() {
        super.drawChildren()

        // Draw markers
        // Draw markers
        if (markers.isNotEmpty()) {
            drawMarkers(markers.map { ratio(it) }, markerLength)
        }
        if (minorMarkers.isNotEmpty()) {
            drawMarkers(minorMarkers.map { ratio(it) }, minorMarkerLength)
        }
        super.drawChildren()
    }

    // ==== Object Methods ====

    private fun ratio(value: Float) = if (max <= min) 0.0 else ((value - min) / (max - min)).toDouble()

    override fun toString() = super.toString() + " { ${min.max2DPs()} .. ${max.max2DPs()} } = ${value.max2DPs()}"

}

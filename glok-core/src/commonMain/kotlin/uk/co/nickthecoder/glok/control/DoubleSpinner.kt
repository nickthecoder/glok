package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.DoubleProperty
import uk.co.nickthecoder.glok.property.boilerplate.doubleProperty
import uk.co.nickthecoder.glok.property.boilerplate.fixedFormatProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedDoubleProperty
import uk.co.nickthecoder.glok.theme.styles.DOUBLE_SPINNER
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.max2DecimalPlaces

/**
 * Edits a [DoubleProperty] by typing digits into [TextArea], via up/down buttons, up/down keys
 * or by scrolling the mouse/trackpad.
 *
 * The range of values are bounded ([min] and [max]).
 *
 * For more details, see the base classes [ComparableSpinner] and [SpinnerBase].
 */
class DoubleSpinner(initialValue : Double = 0.0) : ComparableSpinner<Double, DoubleProperty>() {

    // ==== Properties ====

    override val valueProperty by validatedDoubleProperty(0.0) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    override var value by valueProperty

    override val smallStepProperty by doubleProperty(1.0)
    override var smallStep by smallStepProperty

    override val largeStepProperty by doubleProperty(10.0)
    override var largeStep by largeStepProperty

    override val minProperty by doubleProperty(- Double.MAX_VALUE)
    override var min by minProperty

    override val maxProperty by doubleProperty(Double.MAX_VALUE)
    override var max by maxProperty

    val formatProperty by fixedFormatProperty(max2DecimalPlaces)
    var format by formatProperty

    // ==== End of properties ====

    override var converter = object : Converter<Double, String> {
        override fun forwards(value: Double) = format.format(value)
        override fun backwards(value: String) = value.toDouble()
    }

    // ==== init ====

    init {
        styles.add(DOUBLE_SPINNER)
        initialise()
        value = initialValue
    }

    // ==== End of init ====

    override fun adjustment(direction: Int, byLargeStep: Boolean)
        = value + direction * (if (byLargeStep) largeStep else smallStep)

}

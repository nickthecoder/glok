/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.isNullOrBlank
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Tooltip
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.CHOICE_BOX
import uk.co.nickthecoder.glok.theme.styles.DISABLED

/**
 * A ChoiceBox, has a list of [items] (of type [V]), one of which can be selected.
 * ([selection] is a [SingleSelectionModel]).
 *
 * When the ChoiceBox is pressed a [PopupMenu] appears, which contains all [items] as [ToggleMenuItem]s.
 * The selected item will have a tick-mark.
 *
 * The text within the ChoiceBox and the [ToggleMenuItem]s are generated from [items] using the [converter].
 * The default behaviour simply uses `.toString()`, unless the selected item is `null`, in which case
 * [nullString] is used (which defaults to "<None>").
 *
 * ### Theme DSL
 *
 *     "choice_box" {
 *
 *         "nullString"( String ) // What to display for `null` items. Defaults to "<None>"
 *
 *         child( "button" ) {
 *             ... // The default theme places a `down` arrow graphic on the right hand side.
 *         }
 *     }
 */
class ChoiceBox<V>(

    val items: MutableObservableList<V>

) : Region(), HasDisabled {

    constructor() : this(mutableListOf<V>().asMutableObservableList())

    // region ==== Fields ====

    val selection = SingleSelectionModel(items)

    var converter: (V?) -> String = { it?.toString() ?: nullString }

    // endregion fields

    // region ==== Properties ====
    /**
     * A [Property] whose value is of type [V]`?` (i.e. its value may be null).
     *
     * NOTE. This is a generic type, such as `Property<String?>` when [V] is `String`.
     * NOT an [OptionalStringProperty]. This is annoying, because the JVM's generic type system is limited.
     * For example, we can't use [ObservableOptionalString.isNullOrBlank] with `Property<String?>`.
     * (The JVM's method's signature uses `Property<*>`, so there is no way to differentiate based on the value's type).
     * FYI, this is part of the reason why we need the ungodly mess in glok's auto-generated `boilerplate` package!
     *
     * To get around this, you could create a [SimpleOptionalStringProperty] and bind it to [valueProperty].
     * [SimpleOptionalStringProperty] isn't a generic type, so the JVM's limitations don't apply.
     */
    val valueProperty = selection.selectedItemProperty
    var value by selection.selectedItemProperty

    /**
     * The string returned by the default [converter] for `null` items.
     * At present Glok has no I18N, and this is the English text "<None>".
     * Do not rely on "<None>" being the default value, because a future version of Glok
     * may provide translations based on the user's language preference.
     */
    val nullStringProperty by stylableStringProperty("<None>")
    var nullString by nullStringProperty

    /**
     * The text displayed in the [ChoiceBox].
     * This uses [converter], not `toString()`.
     */
    val valueAsStringProperty: ObservableString =
        StringBinaryFunction(valueProperty, nullStringProperty) { value, _ ->
            converter(value)
        }
    val valueAsString by valueAsStringProperty

    override val disabledProperty by booleanProperty(false)
    override var disabled by disabledProperty

    val alignmentProperty by stylableAlignmentProperty(Alignment.CENTER_LEFT)
    var alignment by alignmentProperty

    // endregion properties

    private val button = Button("").apply {
        textProperty.bindTo(valueAsStringProperty)
        disabledProperty.addChangeListener { _, _, value -> pseudoStyleIf(value, DISABLED) }
        alignmentProperty.bindTo(this@ChoiceBox.alignmentProperty)

        onAction { showChoices() }
    }

    override val children = listOf(button).asObservableList()

    /**
     * Create nodes for the [PopupMenu] when the ChoiceBox is pressed.
     * The default simply calls [createMenuItem].
     * In the vast majority of cases, [ToggleMenuItem] should be returned, and usually you
     * will also use [createMenuItem], but alter it in some way (for example, by setting a [Tooltip])
     */
    var menuItemFactory: (V) -> Node = { createMenuItem(it) }

    // region ==== init ====
    init {
        style(CHOICE_BOX)
        claimChildren()
        button.disabledProperty.bindTo(disabledProperty)

        disabledProperty.addChangeListener { _, _, isDisabled -> pseudoStyleIf(isDisabled, DISABLED) }
    }
    // endregion

    private fun showChoices() {
        val menu = PopupMenu()
        for (item in items) {
            menu.items.add(menuItemFactory(item))
        }
        menu.show(this)
    }

    fun createMenuItem(item: V): Node = ToggleMenuItem(converter(item)).apply {
        selected = item === selection.selectedItem
        onAction {
            selection.selectedItem = item
        }
    }
    // region ==== Layout =====

    override fun nodePrefWidth() = surroundX() + button.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + button.evalPrefHeight()

    override fun layoutChildren() {
        setChildBounds(button, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    // endregion layout

    // region ==== Object methods ====
    override fun toString() = super.toString() + " ${items.size} items." +
        if (selection.selectedIndex >= 0) " #${selection.selectedIndex} selected" else ""
    // endregion
}

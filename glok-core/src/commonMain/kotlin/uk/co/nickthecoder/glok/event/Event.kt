package uk.co.nickthecoder.glok.event

import uk.co.nickthecoder.glok.control.ButtonBase
import uk.co.nickthecoder.glok.scene.Node


class EventType<E : Event>(val name: String) {

    override fun toString() = "EventType $name"

    companion object {

        val WINDOW_RESIZED = EventType<ResizeEvent>("WINDOW_RESIZED")

        val ACTION = EventType<ActionEvent>("ACTION")

        val MOUSE_PRESSED = EventType<MouseEvent>("MOUSE_PRESSED")
        val MOUSE_RELEASED = EventType<MouseEvent>("MOUSE_RELEASED")
        val MOUSE_CLICKED = EventType<MouseEvent>("MOUSE_CLICKED")
        val MOUSE_MOVED = EventType<MouseEvent>("MOUSE_MOVED")
        val MOUSE_EXITED = EventType<MouseEvent>("MOUSE_EXITED")
        val MOUSE_ENTERED = EventType<MouseEvent>("MOUSE_ENTERED")

        /**
         * NOTE. In order to receive these events, you must call [MouseEvent.capture] with the
         * [MOUSE_PRESSED] event.
         *
         * Mouse movements will be delivered to the capturing node as [EventType.MOUSE_DRAGGED],
         * (not as [EventType.MOUSE_MOVED])
         * and will be sent even when the mouse is outside the Node's bounds.
         * No other nodes will receive mouse event while the mouse is captured.
         *
         * Capture ends when a mouse button is released ([MOUSE_RELEASED]).
         */
        val MOUSE_DRAGGED = EventType<MouseEvent>("MOUSE_DRAGGED")

        val SCROLL = EventType<ScrollEvent>("SCROLL")

        val KEY_PRESSED = EventType<KeyEvent>("KEY_PRESSED")
        val KEY_RELEASED = EventType<KeyEvent>("KEY_RELEASED")

        val KEY_TYPED = EventType<KeyTypedEvent>("KEY_TYPED")

        val FILES_DROPPED = EventType<DroppedFilesEvent>("DROPPED_FILES")
    }
}

abstract class Event {

    abstract val eventType: EventType<*>

    private var consumed = false

    /**
     * Prevents further processing of this event.
     *
     * **Warning** Consuming events should not be done lightly.
     * Are you _sure_ there aren't handlers later in the chain/bubbling process that_need_ to receive the event?
     *
     * Mouse events go through a 'bubbling' phase. We find the deepest node at the mouse position,
     * and work _down_ the scene graph from the root not to the deepest node, calling `event filters`
     * (See [Node.addEventFilter]).
     * We then work our way _up_ the scene graph, from the deepest node to the root, calling `event handlers`
     * (See [Node.addEventHandler]).
     *
     * If an event handler/filter consumes the event, then no other handlers/filters receive the event.
     *
     * Other event types do not have the `bubbling` concept. e.g. [ButtonBase.onAction], as they delivered to one Node.
     * However, you may still use [consume], as [ButtonBase.onAction] may be a chain of handlers.
     * If one handler in the chain consumes the event, later handlers in the chain will not receive the event.
     *
     * [consume] may also have additional semantics for specific events.
     * For example, `Stage` and `Tab` check [isConsumed] after a `close requested` event, and will
     * refuse to close if the event was consumed.
     */
    fun consume() {
        consumed = true
    }

    fun isConsumed() = consumed

    companion object {
        fun isShiftDown(mods: Int) = mods and MOD_SHIFT != 0
        fun isControlDown(mods: Int) = mods and MOD_CONTROL != 0
        fun isAltDown(mods: Int) = mods and MOD_ALT != 0
        fun isSuperDown(mods: Int) = mods and MOD_SUPER != 0

        const val MOD_SHIFT = 1
        const val MOD_CONTROL = 2
        const val MOD_ALT = 4
        const val MOD_SUPER = 8

    }
}


package uk.co.nickthecoder.glok.theme

import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.max

private const val ROOT_IMPORTANCE = 1
private const val STYLE_IMPORTANCE = 1
private const val PSEUDO_IMPORTANCE = 1
private const val CHILD_IMPORTANCE = 10
private const val DESCENDANT_IMPORTANCE = 10
private const val ID_IMPORTANCE = 1000

/**
 * A rule which considers one node only, ancestors are not considered.
 */
interface Selector {

    fun matches(node: Node): Boolean

    /**
     * Could [node] match, if pseudoStyles matched?
     */
    fun couldMatch(node: Node): Boolean

    /**
     * Does [node] match, regardless of pseudoStyles?
     */
    fun alwaysMatches(node: Node): Boolean

    /**
     * When two or more rules match, and define the same property,
     * which rule should win? Highest wins. In the event of a tie, the later rule wins.
     */
    fun importance(): Int
}

/**
 * A special selector, so we can change the scene's background color.
 */
object RootSelector : Selector {

    override fun matches(node: Node) = false
    override fun couldMatch(node: Node) = false
    override fun alwaysMatches(node: Node) = false

    override fun importance() = ROOT_IMPORTANCE
}

class IDSelector(val id: String) : Selector {

    override fun matches(node: Node) = node.id == id
    override fun couldMatch(node: Node) = matches(node)
    override fun alwaysMatches(node: Node) = matches(node)

    override fun importance() = ID_IMPORTANCE

    override fun toString() = "id(\"$id\")"
}


/**
 * A [Node] matches, if [Node.styles] contains [style].
 */
class StyleSelector(val style: String) : Selector {

    override fun matches(node: Node) = node.styles.contains(style)
    override fun couldMatch(node: Node) = matches(node)
    override fun alwaysMatches(node: Node) = matches(node)

    override fun importance() = STYLE_IMPORTANCE

    override fun toString() = "\"$style\""
}

class PseudoStyleSelector(val pseudoStyle: String) : Selector {

    override fun matches(node: Node) = node.pseudoStyles.contains(pseudoStyle)
    override fun couldMatch(node: Node) = true
    override fun alwaysMatches(node: Node) = false

    override fun importance() = PSEUDO_IMPORTANCE

    override fun toString() = "\"$pseudoStyle\""
}


/**
 * A [Selector](ThemeSelector), which matches, when either [a] or [b] match.
 */
class OrSelector(val a: Selector, val b: Selector) : Selector {

    override fun matches(node: Node) = a.matches(node) || b.matches(node)
    override fun couldMatch(node: Node) = a.couldMatch(node) || b.couldMatch(node)
    override fun alwaysMatches(node: Node) = a.alwaysMatches(node) || b.alwaysMatches(node)

    override fun importance() = max(a.importance(), b.importance())

    override fun toString() = "( $a or $b )"
}

/**
 * A [Selector](ThemeSelector), which matches, only when either [a] and [b] match.
 */
class AndSelector(val a: Selector, val b: Selector) : Selector {

    override fun matches(node: Node) = a.matches(node) && b.matches(node)
    override fun couldMatch(node: Node) = a.couldMatch(node) && b.couldMatch(node)
    override fun alwaysMatches(node: Node) = a.alwaysMatches(node) && b.alwaysMatches(node)

    override fun importance() = a.importance() + b.importance()

    override fun toString() = "( $a and $b )"
}

/**
 * Equivalent to the CSS selector :
 *
 *     .parent > .child
 */
class ChildSelector(val parentSelector: Selector, val childSelector: Selector) : Selector {

    override fun matches(node: Node): Boolean {
        val parent = node.parent ?: return false
        return childSelector.matches(node) && this.parentSelector.matches(parent)
    }

    override fun couldMatch(node: Node): Boolean {
        val parent = node.parent ?: return false
        return childSelector.couldMatch(node) && this.parentSelector.couldMatch(parent)
    }

    override fun alwaysMatches(node: Node): Boolean {
        val parent = node.parent ?: return false
        return childSelector.alwaysMatches(node) && this.parentSelector.alwaysMatches(parent)
    }

    override fun importance() = parentSelector.importance() + childSelector.importance() + CHILD_IMPORTANCE

    override fun toString() = "$parentSelector.child( $childSelector )"
}

/**
 * Equivalent to the CSS selector :
 *
 *     .ancestor .child
 */
class DescendantSelector(val ancestorSelector: Selector, val childSelector: Selector) : Selector {

    override fun matches(node: Node): Boolean {
        if (childSelector.matches(node)) {
            val result = node.parent?.firstToRoot { ancestor ->
                ancestorSelector.matches(ancestor)
            }
            return result != null
        }

        return false
    }

    override fun couldMatch(node: Node): Boolean {
        if (childSelector.couldMatch(node)) {
            val result = node.parent?.firstToRoot { ancestor ->
                ancestorSelector.couldMatch(ancestor)
            }
            return result != null
        }
        return false
    }

    override fun alwaysMatches(node: Node): Boolean {
        if (childSelector.alwaysMatches(node)) {
            val result = node.parent?.firstToRoot { ancestor ->
                ancestorSelector.alwaysMatches(ancestor)
            }
            return result != null
        }
        return false
    }

    override fun importance() = ancestorSelector.importance() + childSelector.importance() + DESCENDANT_IMPORTANCE

    override fun toString() = "$ancestorSelector.descendant( $childSelector )"

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.Batch
import uk.co.nickthecoder.glok.history.Change

/**
 * A [Change] for [TextDocument]s (used by [TextArea]s and [StyledTextArea]s).
 *
 * There most common are [DeleteText] and [InsertText].
 * Replacing text is achieved by a combination of [DeleteText] and [InsertText] within a single [Batch].
 */
interface TextChange : Change {
    override val document: TextDocument

    fun caretPosition(isUndo: Boolean): TextPosition
}

interface DeleteText : TextChange {
    /**
     * The start of the text to be deleted.
     * This must NOT be after [to].
     */
    val from: TextPosition

    /**
     * The end of the text to be deleted.
     */
    val to: TextPosition

    /**
     * This is a calculated when the [DeleteText] is created using
     * `document.subset(from, to)`
     */
    val textList: List<String>
}

interface InsertText : TextChange {
    val from: TextPosition

    /**
     * A calculated field, based on [from] and [textList].
     * This is the position of the end of the inserted text after it has been inserted.
     */
    val to: TextPosition

    /**
     * This list must NOT be empty. To insert nothing, [textList] = `listOf("")`.
     */
    val textList: List<String>
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.CustomColorPicker
import uk.co.nickthecoder.glok.property.boilerplate.ObservableColor

/**
 * A [Background], where each corner uses a different [ObservableColor].
 * This is used for the `color field` in the [CustomColorPicker].
 */
class ColorFieldBackground(
    val topLeftColor: ObservableColor,
    val topRightColor: ObservableColor,
    val bottomLeftColor: ObservableColor,
    val bottomRightColor: ObservableColor
) : Background {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        val topLeftColor = topLeftColor.value
        val topRightColor = topRightColor.value
        val bottomLeftColor = bottomLeftColor.value
        val bottomRightColor = bottomRightColor.value

        val x2 = x + size.left
        val y2 = y + size.top
        val width2 = width - size.left - size.right
        val height2 = height - size.top - size.bottom

        backend.gradient(
            floatArrayOf(
                x2, y2, x2 + width2, y2, x2 + width2, y2 + height2,
                x2, y2, x2, y2 + height2, x2 + width2, y2 + height2
            ),
            arrayOf(
                topLeftColor, topRightColor, bottomRightColor,
                topLeftColor, bottomLeftColor, bottomRightColor
            )
        )
    }

    override fun toString() = "ColorFieldBackground( ${topLeftColor.value}, ${topRightColor.value}, ${bottomLeftColor.value}, ${bottomRightColor.value} )"
}

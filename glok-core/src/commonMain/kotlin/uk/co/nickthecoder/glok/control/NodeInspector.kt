/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.INFORMATION
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.glok.util.findProperties
import uk.co.nickthecoder.glok.util.simpleName

/**
 * A panel with a TreeView at the top, showing the hierarchical scene graph starting at [rootNode].
 * At the bottom is details of the currently selected TreeItem.
 *
 * NOTE. Data in the TreeView can become stale
 * (no attempt is made to rebuild `children` as the scene graph changes).
 *
 * [rootNode] can be set to any [Node], but is often set to [Scene.root].
 *
 * This may be helpful for debugging.
 * It is also a fairly simple example of using a [TreeView] with custom [TreeCell]s, and dynamically
 * building [TreeItem.children] when a [TreeItem] is expanded.
 *
 * See `NodeInspectorDock` in the `glok-dock` subproject for a `Dock` containing a NodeInspector.
 */
class NodeInspector : SingleContainer() {

    val rootNodeProperty by optionalNodeProperty(null)
    var rootNode by rootNodeProperty

    /**
     * Determines which nodes to show in the `TreeView`.
     * The default shows all nodes.
     */
    val nodeFilterProperty = SimpleProperty<(Node) -> Boolean>({ true }, Any::class)
    var nodeFilter: (Node) -> Boolean by nodeFilterProperty

    /**
     * The color to highlight the node when the "eye" icon is clicked.
     */
    val highlightColorProperty by colorProperty(Color.RED.opacity(0.1f))
    var highlightColor by highlightColorProperty

    val iconSizeProperty by intProperty(32)
    var iconSize by iconSizeProperty
    val icons = Tantalum.scaledIcons.resizableNamedImages(iconSizeProperty)

    /**
     * Incremented to cause [detailsPanel] to be reevaluated.
     */
    private val refreshProperty = SimpleIntProperty(0)
    private var refresh by refreshProperty

    private lateinit var nodeTreeView: TreeView<Node>

    // region ==== init ====
    init {
        rootNodeProperty.addListener {
            content = buildContent()
        }
    }
    // endregion init

    fun refresh() {
        content = buildContent()
    }

    private fun buildContent(): Node {
        nodeTreeView = TreeView()

        // Used in two places in our scene-graph, so we declare it ahead of time.

        return SplitPane(Orientation.VERTICAL).apply {
            + nodeTreeView.apply {
                rootNode?.let { root = NodeTreeItem(it) }
                cellFactory = { treeView, item ->
                    TextTreeCell(treeView, item, item.value.toShortString()).apply {
                        node.graphic = Button("#").apply {
                            styles.add(TINTED)
                            noBackground()
                            padding(0)
                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                            graphic = ImageView(icons.getResizable("visible"))
                            disabledProperty.bindTo(! item.value.visibleProperty)

                            disabledProperty.addChangeListener { _, _, disabled ->
                                node.styleIf(disabled, INFORMATION)
                            }
                            node.styleIf(! item.value.visible, INFORMATION)

                            onAction {
                                treeView.selection.selectedItem = item
                                highlightNode(item.value)
                            }
                        }
                    }
                }
            }

            // Displays the details panel for the currently selected Node in the TreeView.
            + singleContainer(detailsPanel(nodeTreeView.selectedValueProperty)) {
                bindVisible = true
            }
        }
    }

    /**
     * Used to bind the selected value of the TreeView to the content of a [SingleContainer].
     */
    private fun detailsPanel(nodeProperty: ObservableValue<Node?>) =
        OptionalNodeBinaryFunction(nodeProperty, refreshProperty) { node, _ ->
            if (node == null) {
                null
            } else {
                borderPane() {
                    top = toolBar {
                        style(".secondary")
                        + Label("Node Details")
                        + spacer()
                        + button("Refresh") {
                            styles.add(TINTED)
                            graphic = Tantalum.icon("refresh")
                            onAction { refresh ++ }
                        }
                    }


                    center = scrollPane {
                        + formGrid {
                            detailsForm(node)
                        }
                    }
                }
            }
        }

    private fun FormGrid.detailsForm(node: Node) {

        lateinit var xTitledPane: TitledPane

        + row("Class", node::class.simpleName ?: "?")
        if (node.id.isNotBlank()) {
            + row("ID", node.id)
        }
        + row("Local X,Y", "${node.localX} , ${node.localY}")
        + row("Scene X,Y", "${node.sceneX} , ${node.sceneY}")
        + row("Styles", "${node.styles}")
        + row("Pseudo Styles", "${node.pseudoStyles}")

        // Sizes
        + row {
            above = hBox {
                spacing = 10f
                + titledPane("X") {
                    xTitledPane = this
                    content = formGrid {
                        padding(0)
                        + row("Actual", node.width.toString())
                        + row("Min", node.evalMinWidth().toString())
                        + row("Pref", node.evalPrefWidth().toString())
                        + row("Max", node.evalMaxWidth().toString())
                    }
                }
                + titledPane("Y") {
                    expandedProperty.bidirectionalBind(xTitledPane.expandedProperty)
                    content = formGrid {
                        padding(0)
                        + row("Actual", node.height.toString())
                        + row("Min", node.evalMinHeight().toString())
                        + row("Pref", node.evalPrefHeight().toString())
                        + row("Max", node.evalMaxHeight().toString())
                    }
                }
            }
        }
        // Properties
        + row {
            above = titledPane("Properties") {
                content = formGrid {
                    padding(0)
                    for (prop in findProperties(node)) {
                        val value = prop.value
                        when (value) {
                            is Node -> {
                                + row(prop.beanName ?: "<anon>") {
                                    right = button(value.simpleName() ?: "") {
                                        onAction { selectNode(value) }
                                    }
                                }
                            }

                            is Scene -> {
                                + row(prop.beanName ?: "<anon>") {
                                    right = button(value.simpleName() ?: "") {
                                        onAction { selectNode(value.root) }
                                    }
                                }
                            }

                            else -> {
                                + row(prop.beanName ?: "<anon>", value?.toString() ?: "<null>")
                            }
                        }
                    }
                }
            }
        }

        // Finish with `Node.toString()`
        + row("toString()", node.toString())
    }

    /**
     * Any node properties are displayed as a button, which when pressed calls this.
     * Select that node in the [nodeTreeView]
     */
    private fun selectNode(node: Node) {
        // Collect all node's ancestors.
        val ancestors = mutableListOf<Node>()
        node.forEachFromRoot { ancestors.add(it) }

        // Find the index of treeView's root.
        var item = nodeTreeView.root ?: return
        var index = ancestors.indexOf(item.value)
        if (index < 0) return

        // Iterate over the rest of the ancestors, finding the corresponding TreeItem.
        index ++
        while (index < ancestors.size) {
            item.expanded = true
            item = item.children.firstOrNull { it.value === ancestors[index] } ?: return
            index ++
        }
        // `item` is now the TreeItem corresponding whose value is `node`.

        nodeTreeView.selection.selectedItem = item
        nodeTreeView.scrollTo(item)
    }

    private fun highlightNode(node: Node) {

        fun highlightNow() {
            node.scene?.stage?.let { nodesStage ->
                highlightNodeStage = overlayStage(nodesStage, StageType.POPUP) {
                    scene {
                        // Clicking outside the overlay stage will dismiss it,
                        // but we also want to dismiss it when we click INSIDE it,
                        // so we make it a button.
                        root = pane {
                            plainBackground(highlightColor)
                            overridePrefWidth = node.width
                            overridePrefHeight = node.height
                            onMouseClicked { scene?.stage?.close() }
                        }
                    }

                    onClosed { highlightNodeStage = null }
                    show(node.sceneX, node.sceneY)
                }
            }
        }

        val existingStage = highlightNodeStage
        if (existingStage == null) {
            highlightNow()
        } else {
            // If there's already a highlight open, then close it, and open the new one AFTER the old one has closed.
            // NOTE, The highlight would normally close itself, but if the NodeInspector is within a different Stage
            // to the node being inspected, then clicking in the NodeInspector won't close the highlightNodeStage.
            existingStage.onClosed { highlightNow() }
            existingStage.close()
        }

    }

    /**
     * Dynamically sets [children] when a [TreeItem] is expanded, and
     * clears [children] when the [TreeItem] is contracted.
     */
    private inner class NodeTreeItem(value: Node) : TreeItem<Node>(value) {

        private val filterListener = nodeFilterProperty.addWeakListener {
            if (expanded) refresh(false)
        }

        init {
            onExpanding { refresh(true) }
        }

        private fun refresh(recursive: Boolean) {
            val oldNodes = children.map { it.value }
            val newNodes = value.children.filter(nodeFilter)
            // If oldNodes is the same as newNodes, then we don't need to refresh.
            if (oldNodes != newNodes) {
                // Build into a new list, because we may reuse some children
                val newChildren = mutableListOf<TreeItem<Node>>()
                for (node in newNodes) {
                    // Only create a new TreeItem if we need to, otherwise reuse the old one.
                    // FYI, If we always create new TreeItems, then we would lose the `expanded` state of the subtree.
                    val newItem = children.firstOrNull { it.value === node } ?: NodeTreeItem(node)
                    newChildren.add(newItem)
                }
                children.clear()
                children.addAll(newChildren)
            }
            if (recursive) {
                for (child in children) {
                    (child as? NodeTreeItem)?.refresh(true)
                }
            }
        }
    }

    companion object {

        private var highlightNodeStage: Stage? = null

        fun showDialog(nodeToInspect: Node?, parentStage: Stage?, width: Float = 400f, height: Float = 700f): Dialog {
            val inspector = NodeInspector().apply {
                overridePrefWidth = width
                overridePrefHeight = height
                rootNode = nodeToInspect
            }

            return Dialog().apply {
                title = "Node Inspector"
                content = inspector

                parentStage?.let { createStage(it).show() }
            }
        }
    }
}

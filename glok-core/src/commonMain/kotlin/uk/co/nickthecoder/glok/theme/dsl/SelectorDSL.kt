package uk.co.nickthecoder.glok.theme.dsl

import uk.co.nickthecoder.glok.theme.*

fun String.toSelector(): Selector = if (this.startsWith(':')) {
    PseudoStyleSelector(this)
} else if (this.startsWith('#')) {
    IDSelector(this.substring(1))
} else {
    StyleSelector(this)
}

infix fun String.or(other: String) = OrSelector(this.toSelector(), other.toSelector())
infix fun Selector.or(other: String) = OrSelector(this, other.toSelector())
infix fun Selector.or(other: Selector) = OrSelector(this, other)
infix fun String.or(other: Selector) = OrSelector(this.toSelector(), other)


infix fun String.and(other: String) = AndSelector(this.toSelector(), other.toSelector())
infix fun Selector.and(other: String) = AndSelector(this, other.toSelector())
infix fun Selector.and(other: Selector) = AndSelector(this, other)
infix fun String.and(other: Selector) = AndSelector(this.toSelector(), other)

fun String.child(other: String) = ChildSelector(this.toSelector(), StyleSelector(other))
fun Selector.child(other: String) = ChildSelector(this, other.toSelector())
fun Selector.child(other: Selector) = ChildSelector(this, other)
fun String.child(other: Selector) = ChildSelector(this.toSelector(), other)

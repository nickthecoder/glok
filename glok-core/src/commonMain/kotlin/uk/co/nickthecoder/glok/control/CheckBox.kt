package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.CHECK_BOX
import uk.co.nickthecoder.glok.theme.styles.INDETERMINATE
import uk.co.nickthecoder.glok.theme.styles.SELECTED

/**
 * A [CheckBox] can have either two states : [selected] == true , [selected] == false.
 * Or, if you set [allowIndeterminate], then it can have three states :
 *
 *     state          |  indeterminate value |  selected value
 *     -------------------------------------------------------
 *     unselected     |  false               |  false
 *     selected       |  false               |  true
 *     indeterminate  |  true                |  n/a
 *
 * It is displayed using one of two/three graphics. Clicking will cycle through the two/three states.
 *
 *
 * ### Theme DSL
 *     "check_box" {
 *
 *          graphic( Image )
 *          graphic( images : NamedImages, imageName: String )
 *
 *          ":selected" { graphic( ... ) }
 *          ":intermediate" { graphic( ... ) }
 *     }
 *
 * CheckBox inherits all the features of [ButtonBase].
 *
 */
class CheckBox(text: String) : SelectButtonBase(text, null) {

    constructor() : this("")

    // ==== Properties ====

    override val selectedProperty by booleanProperty(false)
    override var selected by selectedProperty

    val indeterminateProperty by booleanProperty(false)
    var indeterminate by indeterminateProperty

    /**
     * When set to true, this has three states, rather than the usual [selected] / `not-selected`.
     * The third state is when [indeterminate] is true (and when [indeterminate] is true, [selected] has no meaning).
     *
     * NOTE, This only affects the behaviour when clicked. It does NOT prevent you from setting the
     * [indeterminate] property manually.
     */
    val allowIndeterminateProperty by booleanProperty(false)
    var allowIndeterminate by allowIndeterminateProperty

    // ==== End of properties ====

    init {
        style(CHECK_BOX)

        // Change pseudo styles, and the image in imageView whenever a state change occurs.
        selectedProperty.addListener {
            pseudoStyleIf(indeterminate, INDETERMINATE)
            pseudoStyleIf(selected && ! indeterminate, SELECTED)
        }.also {
            indeterminateProperty.addListener(it)
        }
    }

    // ==== End of init ====

    /**
     * Cycles through not-selected, selected, indeterminate.
     * Missing out indeterminate if [allowIndeterminate] == false.
     */
    override fun toggle() {
        if (indeterminate) {
            // From indeterminate to not-selected
            indeterminate = false
            selected = false
        } else {
            if (selected) {
                // From selected to indeterminate or not-selected.
                if (allowIndeterminate) {
                    indeterminate = true
                } else {
                    selected = false
                }
            } else {
                // From not-selected to selected
                selected = true
            }
        }
    }

    // ==== Object methods ====

    override fun toString() = super.toString() + " selected=$selected" +
        if (allowIndeterminate) " indeterminate=$indeterminate" else ""

}

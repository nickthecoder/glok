/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.event.combineActionEventHandlers
import uk.co.nickthecoder.glok.property.asReadOnly
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.property.functions.or
import uk.co.nickthecoder.glok.property.simpleProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.WithContent
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.*

/**
 * Part of a [TabBar] / [TabPane].
 */
open class Tab(

    text: String,
    graphic: Node? = null

) : BoxBase(), HasDisabled, WithContent {

    //region ==== Pre Properties ====

    /**
     * The orientation of the Box containing the tab's [text]/[graphic] pair (as a [Labelled]), and
     * the close button (if present).
     * Pretty much always horizontal???
     */
    override val orientation = Orientation.HORIZONTAL

    private val tabLabel = Label(text, graphic).apply {
        style(TAB_LABEL)
    }

    private val closeButton = Button("x").apply {
        style(CLOSE_BUTTON)
        onAction { requestClose() }
    }
    //endregion

    //region ==== Properties ====

    val textProperty = tabLabel.textProperty
    var text by tabLabel.textProperty

    val graphicProperty = tabLabel.graphicProperty
    var graphic by tabLabel.graphicProperty

    val contentProperty by optionalNodeProperty(null)
    final override var content by contentProperty

    val fillProperty by stylableBooleanProperty(false)
    public final override var fill by fillProperty

    // NOTE, We are using SimpleProperty<TabBar>, as there is no non-generic version for value type TabBar.
    internal val mutableTabBarProperty by simpleProperty(null as TabBar?)
    val tabBarProperty = mutableTabBarProperty.asReadOnly()
    val tabBar by mutableTabBarProperty

    private val mutableSelectedProperty by booleanProperty(false)
    val selectedProperty = mutableSelectedProperty.asReadOnly()
    val selected by selectedProperty

    final override val disabledProperty by booleanProperty(false)
    final override var disabled by disabledProperty


    val onCloseRequestedProperty by optionalActionEventHandlerProperty(null)
    var onCloseRequested by onCloseRequestedProperty

    val onClosedProperty by optionalActionEventHandlerProperty(null)
    var onClosed by onClosedProperty

    //endregion

    //region ==== Fields ====

    private val mutableChildren = mutableListOf<Node>().asMutableObservableList()
    override val children = mutableChildren.asReadOnly()

    //endregion

    //region === init ====

    init {
        style(TAB)
        fill = true
        mutableChildren.addChangeListener(childrenListener)
        mutableChildren.add(tabLabel)
        mutableChildren.add(closeButton)
        closeButton.disabledProperty.bindTo(disabledProperty)

        tabBarProperty.addChangeListener { _, _, tabBar ->
            mutableSelectedProperty.unbind()
            closeButton.visibleProperty.unbind()
            if (tabBar == null) {
                mutableSelectedProperty.value = false
                closeButton.visibleProperty.value = false
            } else {
                mutableSelectedProperty.bindTo((tabBar.selection.selectedItemProperty).equalTo(this))
                closeButton.visibleProperty.bindTo(
                    tabBar.tabClosingPolicyProperty.equalTo(TabClosingPolicy.ALL_TABS) or
                        (selectedProperty and tabBar.tabClosingPolicyProperty.equalTo(TabClosingPolicy.SELECTED_TAB))
                )
            }
        }

        selectedProperty.addChangeListener { _, _, isSelected -> pseudoStyleIf(isSelected, SELECTED) }
        disabledProperty.addChangeListener { _, _, isDisabled -> pseudoStyleIf(isDisabled, DISABLED) }

        onMousePressed { onMousePressed() }
        onKeyPressed { onKeyPressed(it) }
    }

    //endregion

    //region ==== Events ====

    private fun onMousePressed() {
        if (disabled) return
        if (focusTraversable) requestFocus()
        selectTab()
    }


    private fun onKeyPressed(event: KeyEvent) {
        if (disabled) return
        if (focused && ButtonBase.ButtonActions.ACTIVATE.matches(event)) {
            selectTab()
        }
    }

    private fun selectTab() {
        tabBar?.selection?.selectedItem = this
    }

    /**
     * Request that the tab is closed. This fires the [onCloseRequested] event and if the event is consumed,
     * then the tab will NOT be closed.
     *
     * If you want to _force_ a tab to be closed, just remove it from [TabBar.tabs] / [TabPane.tabs].
     */
    fun requestClose() {
        tabBar?.let { tabPane ->
            val closeRequest = ActionEvent()
            onCloseRequested?.handle(closeRequest)
            if (! closeRequest.isConsumed()) {
                tabPane.tabs.remove(this)
            }
        }
    }

    /**
     * This event is fired when [requestClose] is called or the tab's close button is pressed.
     * If the event is consumed, then the tab will NOT close.
     */
    fun onCloseRequested(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onCloseRequestedProperty, handlerCombination, block)
    }

    /**
     * The event is fired when this tab is removed from [TabBar.tabs] / [TabPane.tabs], regardless of how that happens
     * (close button pressed, [requestClose] called or the tab removed from [TabBar.tabs] / [TabPane.tabs]
     * programmatically).
     */
    fun onClosed(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onClosedProperty, handlerCombination, block)
    }

    //endregion

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.history.History
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.OverlayStage
import uk.co.nickthecoder.glok.scene.Scene
import uk.co.nickthecoder.glok.text.Indentation
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Theme

/**
 * Various settings.
 *
 * If you allow the user to customise these settings, then it is recommended that you
 * load and save them using `GlokSettings.load()` and `GlokSettings.save()`.
 *
 * Load and save is only supported for JVM application (i.e. not web applications).
 *
 */
object GlokSettings {

    /**
     * When using high DPI devices (dots per inch), the scene needs to be scaled, otherwise all the
     * controls will be too small to be usable. In this case, coordinates used in [Node] and [Scene] aren't pixels.
     * Instead, 1 `logical unit` is 1 physical pixel on a low DPI device, but is [globalScale] physical pixels on a
     * high DPI device.
     *
     * This property can be set manually, allowing your entire application to be scaled by any amount
     * (which may be useful for the visually impaired). When an application is started,
     * [globalScale] is based on the first of these :
     *
     * 1. The value of the environment variable `GDK_SCALE`
     * 2. The value of the environment variable `QT_SCALE_FACTOR`
     * 3. A guess is made based on the size in pixels of the display.
     *    Anything over 2000 pixels wide, [globalScale] is set to : round(SCREEN_WIDTH / 2000)
     *
     * Unless stated otherwise, glok uses [LogicalPixels]. If the documentation uses the word `pixels` only,
     * that's just a short-cut  [LogicalPixels].
     *
     * NOTE. When using high DPI monitors, expect mouse events with non-integer values!
     */
    val globalScaleProperty by floatProperty(1f)
    var globalScale by globalScaleProperty

    /**
     * This property is bound to [Tantalum.themeProperty], so if you wish to change the default theme,
     * unbind it, and then either bind it to your own themeProperty, or set its value to your own theme.
     * (the former is preferred, as it allows scenes to be restyled automatically when your
     * themeProperty changes).
     *
     * New scenes' `themeProperty` are initially bound to [defaultThemeProperty].
     *
     * If you want to use a different theme for just one scene, you must first unbind [Scene.themeProperty],
     * and then set it to the required theme.
     *
     * See `DemoCombineThemes` in the `glok-demos` subproject.
     */
    val defaultThemeProperty by themeProperty(Theme())
    var defaultTheme by defaultThemeProperty

    /**
     * Should secondary stages use native windows (useOverlayStages=false), or
     * should [OverlayStage]s be used instead?
     *
     * An [OverlayStage] does not use native windows, and instead they exist as `fake` windows
     * with the primary stage's native window.
     * If your application intends to use `full-screen mode`, then set this to `true`
     * (because in full-screen mode, only one native window is allowed).
     */
    val useOverlayStagesProperty by booleanProperty(false)
    var useOverlayStages by useOverlayStagesProperty

    /**
     * If you find the scroll-wheel / mouse-pad is always the wrong way round, toggle this value!
     * However, if `Scrollbars` work correctly, but adjusting `Spinners` doesn't (or vice versa),
     * then this property won't help.
     */
    val reverseScrollProperty by booleanProperty(false)
    var reverseScroll by reverseScrollProperty

    /**
     * The default indentation used by [TextArea] and [StyledTextArea].
     * Changing this property will also change [indentationTabs], [indentationColumns] and [indentationBehaveLikeTabs]
     */
    val indentationProperty by indentationProperty(Indentation.tabIndentation(4))
    var indentation by indentationProperty

    val indentationTabsProperty by booleanProperty(indentation.indentation == "\t")
    var indentationTabs by indentationTabsProperty

    val indentationColumnsProperty by intProperty(indentation.columns)
    var indentationColumns by indentationColumnsProperty

    val indentationBehaveLikeTabsProperty by booleanProperty(indentation.behaveLikeTabs)
    var indentationBehaveLikeTabs by indentationBehaveLikeTabsProperty

    val terminalCommandProperty by stringProperty("")
    var terminalCommand by terminalCommandProperty

    init {
        for (prop in listOf(indentationTabsProperty, indentationColumnsProperty, indentationBehaveLikeTabsProperty)) {
            prop.addListener {
                indentation = if (indentationTabs) {
                    Indentation.tabIndentation(indentationColumns)
                } else {
                    Indentation.spacesIndentation(indentationColumns, indentationBehaveLikeTabs)
                }
            }
        }
        indentationProperty.addListener {
            indentationTabs = indentation.indentation == "\t"
            indentationColumns = indentation.columns
            indentationBehaveLikeTabs = indentation.behaveLikeTabs
        }
    }

    /**
     * See `FileDialog`.
     */
    val nativeFileDialogsProperty by booleanProperty(false)
    var nativeFileDialogs by nativeFileDialogsProperty


    /**
     * The time between changes in milliseconds, where changes can be merged.
     * The default is 1 second (1000 milliseconds).
     * i.e. if we type with less than 1 second between keystrokes, the changes will be merged to form
     * a single batch, so undo/redo will not operate 1 character at a time.
     *
     * TextArea's [History.mergeTimeThresholdProperty] is initially bound to this property, so if you change this
     * property, it will affect all TextDocuments.
     */
    val historyMergeTimeThresholdProperty by doubleProperty(10_000.0)
    var historyMergeTimeThreshold by historyMergeTimeThresholdProperty


    /**
     * The maximum time in milliseconds that two successive presses of a mouse button are considered to be a `click`.
     */
    val clickTimeThresholdProperty by doubleProperty(500.0)
    var clickTimeThreshold by clickTimeThresholdProperty

    /**
     * The maximum distance (in [LogicalPixels]) between two successive presses of a mouse are considered to be a `click`.
     * i.e. if the mouse has moved further than this, then it isn't a click (just two separate mouse presses).
     */
    val clickDistanceThresholdProperty by floatProperty(10f)
    var clickDistanceThreshold by clickDistanceThresholdProperty

    /**
     * The distance (in [LogicalPixels]) that the mouse must move before a drag event is produced.
     * i.e. if the user presses a mouse button, then moves a tiny distance (less than this), no drag event is fired.
     */
    val dragDistanceThresholdProperty by floatProperty(6f)
    var dragDistanceThreshold by dragDistanceThresholdProperty

    /**
     * The time (in milliseconds) the mouse has to hover, unmoving, before a tooltip appears.
     */
    val tooltipTimeThresholdProperty by doubleProperty(1_000.0)
    var tooltipTimeThreshold by tooltipTimeThresholdProperty


    /**
     * See [Restart] for details.
     *
     * Note, this isn't a `setting`, so it doesn't really belong here,
     * but for backwards compatibility, it is staying.
     */
    val restarts = mutableListOf<Restart>().asMutableObservableList()

    /**
     * Resets the settings to their default values.
     *
     * NOTE, this does NOT change [theme].
     */
    fun reset() {
        globalScale = Application.defaultGlobalScale()
        useOverlayStages = false
        reverseScroll = false
        indentation = Indentation.tabIndentation(4)
        nativeFileDialogs = false
        historyMergeTimeThreshold = 10_000.0
        clickTimeThreshold = 500.0
        clickDistanceThreshold = 10f
        dragDistanceThreshold = 6f
        tooltipTimeThreshold = 1_000.0
    }
}

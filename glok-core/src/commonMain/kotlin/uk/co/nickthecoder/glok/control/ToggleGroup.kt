package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.optionalToggleProperty

/**
 * When a [ToggleButton] or [RadioButton] is added to a `ToggleGroup`, then only one button in the group
 * can be selected at a time.
 *
 * Also used by [ToggleMenuItem] and [RadioMenuItem].
 */
class ToggleGroup {

    val selectedToggleProperty by optionalToggleProperty(null)
    var selectedToggle by selectedToggleProperty

    /**
     * @return The user data of the selected [Toggle].
     */
    fun userData(): Any? = selectedToggle?.userData
}

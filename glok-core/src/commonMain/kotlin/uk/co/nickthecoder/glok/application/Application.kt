package uk.co.nickthecoder.glok.application

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.tryCatchHandle
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.util.currentTimeMillis
import uk.co.nickthecoder.glok.util.log
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.newInstance
import kotlin.math.roundToInt
import kotlin.reflect.KClass

abstract class Application {

    lateinit var rawArguments: Array<out String>

    internal val mutableStages = mutableListOf<RegularStage>().asMutableObservableList()

    /**
     * Set this to [ApplicationStatus.FORCE_QUIT] or [ApplicationStatus.REQUEST_QUIT],
     * and all stages will be closed, which will then cause the application to end.
     *
     * When using [ApplicationStatus.REQUEST_QUIT], each stage will have a chance to prevent closure as normal,
     * by consuming the event sent to [Stage.onCloseRequested].
     */
    val statusProperty by applicationStatusProperty(ApplicationStatus.NOT_STARTED)
    var status by statusProperty

    /**
     * A list of all open [RegularStage]s.
     * i.e. This does NOT include [OverlayStage]s, which are used for `PopupMenu`s, `Tooltips` etc.
     * and may optionally also be used for dialogs and other 'windows'.
     *
     * See [RegularStage.overlayStages].
     */
    val stages = mutableStages.asReadOnly()

    /**
     * Do not create [Application] instances directly.
     * Instead, call [launch], which will initialise `Glok`, create the [Application] instance, and then call this method.
     */
    protected abstract fun start(primaryStage: Stage)

    /**
     * When there are no more open [Stage]s, `Glok` performs its own tidy, and also calls this
     * method, which gives your application a chance to tidy up.
     *
     * The default implementation does nothing.
     *
     * Note, you should not call this method directly.
     * If you want to stop the application, then set [status] to [ApplicationStatus.REQUEST_QUIT],
     * or [ApplicationStatus.FORCE_QUIT].
     */
    protected open fun stop() {}

    companion object {

        // ==== region Companion Properties ====

        // ==== endregion companion properties ====

        /**
         * The running application. Set just before [Application.start] is called.
         */
        lateinit var instance: Application

        internal val stages: ObservableList<RegularStage>
            get() = instance.stages

        internal var running = false

        // region ==== Methods ====

        internal fun defaultGlobalScale(): Float {
            val scale = Platform.getEnv("GDK_SCALE")?.toFloatOrNull()
                ?: Platform.getEnv("QT_SCALE_FACTOR")?.toFloatOrNull()
            if (scale != null) {
                return scale
            }
            backend.monitorSize()?.let { (width, _) ->
                return max(1f, (width / 2000f).roundToInt().toFloat())
            }
            return 1f
        }

        private fun updateGlobalScale() {
            GlokSettings.globalScale = defaultGlobalScale()
        }


        /**
         * Called by [launch], after it has initialised Glok.
         * This method does not end until the application ends (and no new [Restart]s exist).
         *
         * NOTE. [launch] has platform specific (jvm/js) implementation, but this does not.
         */
        internal fun launchPart2(restart: Restart) {
            updateGlobalScale()
            val application = restart.applicationClass.newInstance()

            application.rawArguments = restart.rawArgs

            GlokSettings.defaultThemeProperty.unbind()
            GlokSettings.defaultThemeProperty.bindTo(Tantalum.themeProperty)
            GlokTimer.clearTimers()

            instance = application

            val stage = RegularStage()
            application.start(stage)
            application.status = ApplicationStatus.RUNNING

            loop(application)

            GlokTimer.clearTimers()

            application.stop()
            application.status = ApplicationStatus.ENDED

            GlokSettings.restarts.removeFirstOrNull()?.let {
                launchPart2(it)
            }
        }

        private fun loop(application: Application) {
            while (application.stages.isNotEmpty()) {

                // Timers
                GlokTimer.processAll()

                // REQUEST_QUIT
                if (application.status == ApplicationStatus.REQUEST_QUIT) {
                    for (stage in application.mutableStages.toList()) {
                        stage.closing = true
                        requestStageClose(application, stage)
                    }
                    if (stages.isNotEmpty()) application.status = ApplicationStatus.RUNNING
                }
                if (application.status == ApplicationStatus.FORCE_QUIT) {
                    for (stage in application.mutableStages) {
                        stage.closeNow()
                    }
                    application.mutableStages.clear()
                }

                // Check if the stage, has been requested to close.
                // If so, we call `onClosing`, call `onClosing` and check `onClosing` again.
                // This lets `onClosing` prevent the stage from closing.
                // This is done for the stage's overlays too.
                if (stages.firstOrNull { it.closing } != null) {
                    for (stage in stages.toList()) {
                        if (stage.closing) {
                            requestStageClose(application, stage)
                        }
                    }
                }

                // Check if overlays have been requested to close (similar to above)
                for (stage in application.stages) {
                    if (stage.overlayStages.firstOrNull { it.closing } != null) {
                        for (overlay in stage.overlayStages.toList()) {

                            // Check if an individual overlay has been requested to close.
                            if (overlay.closing) {
                                requestOverlayClose(stage, overlay)
                            }
                        }
                    }
                }

                // Redraw stages (if required)
                for (stage in application.stages) {
                    stage.scene?.let { scene ->
                        var requiresRedraw = scene.requestRestyling || scene.requestLayout || scene.requestRedraw

                        for (overlay in stage.mutableOverlayStages) {
                            overlay.scene?.let { overlayScene ->
                                if (overlayScene.requestRestyling || overlayScene.requestLayout || overlayScene.requestRedraw) {
                                    requiresRedraw = true
                                }
                            }
                        }
                        if (requiresRedraw) {
                            stage.draw()
                        }
                    }
                }

                // Check if it time to display a tooltip
                if (currentTimeMillis() > RegularStage.lastMouseMovementTime + GlokSettings.tooltipTimeThreshold) {
                    val focusedRegularStage = instance.stages.firstOrNull { it.windowFocused }
                    if (focusedRegularStage != null) {
                        focusedRegularStage.mouseInStage?.processTooltip()
                    }
                    // Prevents checking for tooltips again for 10s. Could be arbitrarily large.
                    RegularStage.lastMouseMovementTime = currentTimeMillis() + 10_000.0
                }

                // Run later...
                Platform.performPendingRunnable()

                // For now, the timeout is hard-coded.
                // Later, we will give the application the option to specify the frame-rate,
                // and set the timeout to try to accomplish it. (which won't be a simple 1/frame-rate)!
                backend.processEvents(1.0 / 60)

            }
        }

        private fun requestStageClose(application: Application, stage: RegularStage) {
            val stageCloseRequestEvent = ActionEvent()
            stage.onCloseRequested?.tryCatchHandle(stageCloseRequestEvent)
            if (stageCloseRequestEvent.isConsumed() || ! stage.closing) {
                stage.closing = false
                return
            }

            // Stage itself can close, how about all of its OverlayStages?
            for (overlay in stage.overlayStages) {
                overlay.closing = true
                val overlayCloseRequestedEvent = ActionEvent()
                overlay.onCloseRequested?.tryCatchHandle(overlayCloseRequestedEvent)
                if (overlayCloseRequestedEvent.isConsumed() || ! overlay.closing) {
                    // This overlay rejected to request to close, so let's not close.
                    stage.closing = false
                    for (o in stage.overlayStages) {
                        o.closing = false
                    }
                    return
                }
            }

            // Nothing prevented us from closing.
            // So close the overlays, and the regular stage.
            for (overlay in stage.overlayStages.toList()) {
                overlay.closeNow()
            }
            stage.closeNow()
            application.mutableStages.remove(stage)

        }

        private fun requestOverlayClose(regularStage: RegularStage, overlay: OverlayStage) {
            val overlayCloseRequestedEvent = ActionEvent()
            overlay.onCloseRequested?.tryCatchHandle(overlayCloseRequestedEvent)
            if (overlayCloseRequestedEvent.isConsumed() || ! overlay.closing) {
                overlay.closing = false
            } else {
                overlay.closeNow()
                regularStage.scene?.requestRedraw = true
            }
        }

        // endregion ==== Companion methods ====
    }
}

/**
 * Called from the program's `main` entry point to start a `Glok` [Application].
 *
 * This must be called only once.
 * If you wish to start another application after the first application ends, then use [Application.restart].
 *
 * In general, you should make no assumptions about when (or if) this method returns.
 * If you wish to perform clean-up when your application ends, override [Application.stop].
 *
 * FYI, Glok uses the current thread as glok's event loop, and this method returns when the application
 * ends. However, this may change in later versions of Glok.
 */
expect fun launch(klass: KClass<out Application>, rawArgs: Array<out String> = emptyArray())

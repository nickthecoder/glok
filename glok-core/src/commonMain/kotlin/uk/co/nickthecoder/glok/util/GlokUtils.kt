package uk.co.nickthecoder.glok.util

import uk.co.nickthecoder.glok.property.ReadOnlyProperty
import uk.co.nickthecoder.glok.scene.Node
import kotlin.reflect.KClass

internal fun ignoreErrors(action: () -> Any?) {
    try {
        action()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

internal inline fun <T> Iterable<T>.sumOf(selector: (T) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

internal fun <T> Iterable<T>.defaultMaxOf(default: Float, selector: (T) -> Float) = maxOfOrNull(selector) ?: default
internal fun <T> Iterable<T>.defaultMinOf(default: Float, selector: (T) -> Float) = minOfOrNull(selector) ?: default

internal fun Iterable<Node>.totalMinWidth() = sumOf { it.evalMinWidth() }
internal fun Iterable<Node>.totalPrefWidth() = sumOf { it.evalPrefWidth() }
internal fun Iterable<Node>.totalMaxWidth() = sumOf { it.evalMaxWidth() }

internal fun Iterable<Node>.totalMinHeight() = sumOf { it.evalMinHeight() }
internal fun Iterable<Node>.totalPrefHeight() = sumOf { it.evalPrefHeight() }
internal fun Iterable<Node>.totalMaxHeight() = sumOf { it.evalMaxHeight() }

internal fun Iterable<Node>.maxMinWidth() = defaultMaxOf(0f) { it.evalMinWidth() }
internal fun Iterable<Node>.maxPrefWidth() = defaultMaxOf(0f) { it.evalPrefWidth() }

internal fun Iterable<Node>.maxMinHeight() = defaultMaxOf(0f) { it.evalMinHeight() }
internal fun Iterable<Node>.maxPrefHeight() = defaultMaxOf(0f) { it.evalPrefHeight() }

internal fun Iterable<Node>.minMaxWidth() = defaultMinOf(0f) { it.evalMaxWidth() }
internal fun Iterable<Node>.minMaxHeight() = defaultMinOf(0f) { it.evalMaxHeight() }

// Format numbers with a maximum of 2 decimal places
internal val noDecimalPlaces = FixedFormat.places(0)

// Format numbers with a maximum of 2 decimal places
internal val max1DecimalPlace = FixedFormat.places(1)
internal val max2DecimalPlaces = FixedFormat.places(2)
internal fun Double.max2DPs(): String = max2DecimalPlaces.format(this.toFloat())
internal fun Float.max2DPs(): String = max2DecimalPlaces.format(this)
internal fun Matrix.max2DPs() = """[ ${m00.max2DPs()} , ${m10.max2DPs()} , ${m20.max2DPs()}
${m01.max2DPs()} , ${m11.max2DPs()} , ${m21.max2DPs()} ]"""


internal fun Double.clamp(min: Double, max: Double) = if (this < min) min else if (this > max && max >=min) max else this
internal fun Float.clamp(min: Float, max: Float) = if (this < min) min else if (this > max && max >= min) max else this
internal fun Double.clamp() = if (this < 0.0) 0.0 else if (this > 1.0) 1.0 else this
internal fun Float.clamp() = if (this < 0f) 0f else if (this > 1f) 1f else this
internal fun Int.clamp(min: Int, max: Int) = if (this < min) min else if (this > max && max >=min) max else this
internal fun Long.clamp(min: Long, max: Long) = if (this < min) min else if (this > max && max >=min) max else this

internal fun listOfVisible(vararg elements: Node?): List<Node> = elements.filterNotNull().filter { it.visible }
internal fun Node?.ifVisible(): Node? = if (this?.visible == true) this else null
internal fun List<Node>.ifVisible() = this.filter { it.visible }

internal fun Boolean.toString(str: String) = if (this) str else ""

internal fun commonAncestor(a: Node?, b: Node?): Node? {
    if (a == null || b == null) return null
    val aAncestors = mutableListOf(a)
    a.parent?.forEachToRoot { aAncestors.add(it) }
    return b.firstToRoot { aAncestors.contains(it) }
}

internal fun min(a: Float, b: Float) = if (a <= b) a else b
internal fun max(a: Float, b: Float) = if (a <= b) b else a

internal fun <T : Comparable<T>> min(a: T, b: T) = if (a <= b) a else b
internal fun <T : Comparable<T>> max(a: T, b: T) = if (a <= b) b else a

internal fun Matrix.times(x: Float, y: Float): Pair<Float, Float> {
    return Pair(
        m00 * x + m10 * y + m20,
        m01 * x + m11 * y + m21
    )
}

internal fun Any.simpleName() = this::class.simpleName

expect fun <T : Any> KClass<T>.newInstance(): T

internal expect fun findProperties(obj: Any): List<ReadOnlyProperty<*>>

private val memorySizes = listOf(
    "TB" to 1_000_000_000_000L,
    "GB" to 1_000_000_000L,
    "MB" to 1_000_000L,
    "kB" to 1_000L,
    "bytes" to 1L
)

internal fun Long.toMemorySize(): String {
    for ((label, size) in memorySizes) {
        if (this >= size) {
            val whole = this / size
            val remainder = this - whole * size
            val fraction = (remainder * 10) / size
            return if (fraction == 0L) "$whole $label" else "$whole.$fraction $label"
        }
    }
    return "Too large"
}

internal expect fun dumpStackTrace()

package uk.co.nickthecoder.glok.event


class ResizeEvent(
    val oldWidth: Int,
    val oldHeight: Int,
    val width: Int,
    val height: Int

) : Event() {

    override val eventType: EventType<*> get() = EventType.WINDOW_RESIZED

}

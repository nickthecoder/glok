/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.control.PopupMenu.PopupMenuActions
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.scrollPaneWithButtons
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.scene.dsl.vBox
import uk.co.nickthecoder.glok.theme.styles.POPUP_MENU
import uk.co.nickthecoder.glok.util.GlokException
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

/**
 * The root node for a pop-up menu. The visible items for the pop-up menu are taken from [Menu.items] or
 * [SubMenu.items].
 * A PopupMenu is displayed as a new scene in a new [OverlayStage] ([StageType.POPUP]), and is therefore
 * NOT part of the scene graph of the [Menu] or [SubMenu] from which it originated.
 *
 * Keyboard shortcuts are taken from [PopupMenuActions] as well as [MenuItemActions].
 */
class PopupMenu(val openedBy: Node? = null) : Region(), WithItems {

    // region ==== Properties ====

    // endregion

    // region ==== Fields ====

    private val container = vBox { fillWidth = true }

    private val scrollPaneWithButtons = scrollPaneWithButtons {
        orientation = Orientation.VERTICAL
        content = container
    }

    override val items = container.children

    override val children = listOf(scrollPaneWithButtons).asObservableList()

    // endregion

    init {
        style(POPUP_MENU)
        claimChildren()

        onKeyPressed { keyPressed(it) }
        deselectAll()
    }

    fun show(nearNode: Node): Stage {
        val regularScene = nearNode.scene?.stage?.regularStage?.scene

        val side = if (regularScene == null) {
            Side.BOTTOM
        } else {
            val above = nearNode.sceneY
            val below = regularScene.root.height - (nearNode.sceneY + nearNode.height)
            if (above > below) {
                Side.TOP
            } else {
                Side.BOTTOM
            }
        }
        return show(nearNode, side)
    }
    fun show(nearNode: Node, side: Side): Stage {

        val parentScene = nearNode.scene ?: throw GlokException("Node is not within a Scene")
        val parentStage = parentScene.stage ?: throw GlokException("Node is not on a Stage")

        return createStage(parentStage).apply {
            val position = when (side) {
                Side.BOTTOM -> Pair(nearNode.sceneX, (nearNode.sceneY + nearNode.height))
                Side.TOP -> Pair(nearNode.sceneX, (nearNode.sceneY - scene !!.root.evalPrefHeight()))
                Side.RIGHT -> Pair((nearNode.sceneX + nearNode.width), nearNode.sceneY)
                Side.LEFT -> Pair((nearNode.sceneX - scene !!.root.evalPrefWidth()), nearNode.sceneY)
            }
            val belowBottom = position.second + nodePrefHeight() - regularStage.scene !!.height
            if (belowBottom > 0) {
                show(position.first, max(0f, position.second - belowBottom))
            } else {
                show(position.first, position.second)
            }
        }
    }

    fun show(x: Float, y: Float, parentStage: Stage): Stage {
        return createStage(parentStage).apply {
            show(x, y)
        }
    }

    private fun createStage(parentStage: Stage): OverlayStage {
        parentStage.regularStage.closePopups()
        return overlayStage(parentStage, StageType.POPUP_MENU) {
            scene {
                root = this@PopupMenu
            }
            scene?.restyle()
        }
    }

    private fun keyPressed(event: KeyEvent) {

        if (PopupMenuActions.NEXT.matches(event)) {
            // See if an item has focus. If so, move the focus to the next child.
            val index = items.indexOf(scene?.focusOwner)
            deselectAll()
            if (index >= 0) {
                if (index < items.size - 1) {
                    select(items[index + 1])
                }
            } else {
                items.firstOrNull()?.let { select(it) }
            }
            event.consume()
        }

        if (PopupMenuActions.PREV.matches(event)) {
            val index = items.indexOf(scene?.focusOwner)
            deselectAll()
            if (index > 1) {
                select(items[index - 1])
            } else {
                items.firstOrNull()?.let { select(it) }
            }
            event.consume()
        }

        if (MenuItemActions.BACKWARDS.matches(event)) {
            if (openedBy is Menu) {
                openedBy.forwardsBackwards(- 1)
            } else {
                scene?.stage?.close()
            }
            event.consume()
        }
        if (MenuItemActions.FORWARDS.matches(event)) {
            if (openedBy is Menu) {
                openedBy.forwardsBackwards(1)
                event.consume()
            }
        }
    }

    internal fun forwardsBackwards(delta: Int) {
        if (openedBy is Menu) {
            openedBy.forwardsBackwards(delta)
        } else if (openedBy is SubMenu) {
            openedBy.forwardsBackwards(delta)
        }
    }

    private fun select(menuItem: Node) {
        if (menuItem is MenuItemBase) {
            menuItem.select()
        } else {
            menuItem.requestFocus()
        }
    }

    internal fun deselectAll() {
        for (item in items) {
            if (item is MenuItemBase) {
                item.deselect()
            }
        }
        requestFocus()
    }

    // region ==== Layout ====

    override fun nodeMinWidth() = nodePrefWidth()
    override fun nodeMinHeight() = nodePrefHeight()

    override fun nodePrefWidth() = scrollPaneWithButtons.evalPrefWidth() + surroundX()
    override fun nodePrefHeight(): Float {
        val maxHeight = scene !!.stage !!.regularStage.scene !!.height
        val prefHeight = scrollPaneWithButtons.evalPrefHeight() + surroundY()
        return min(prefHeight, maxHeight)
    }

    override fun layoutChildren() {
        setChildBounds(scrollPaneWithButtons, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    /**
     * Shortcuts for [PopupMenu]s.
     */
    object PopupMenuActions : Actions(null) {
        /**
         * Move to the next submenu. Key Down
         */
        val NEXT = define("next", "Next", Key.DOWN.noMods())

        /**
         * Move to the previous subMenu. Key Up
         */
        val PREV = define("prev", "Prev", Key.UP.noMods())

    }

    // endregion layout

    companion object {
        /**
         * Given an item in a [PopupMenu], this finds the [PopupMenu].
         *
         * There is no direct relationship from items in a [PopupMenu] to the [PopupMenu],
         * but finding it is easy, because [PopupMenu]s are the root node of the scene.
         *
         * Returns null, if [menuItem] isn't currently shown (or is part of a scene
         * which isn't a popup menu).
         *
         * [menuItem] can be any node, but is almost always a [MenuItemBase].
         */
        fun findPopupMenu(menuItem: Node): PopupMenu? {
            return menuItem.scene?.root as? PopupMenu
        }
    }

}

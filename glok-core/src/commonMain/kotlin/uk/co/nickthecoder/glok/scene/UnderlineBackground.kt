/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.Text
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty

/**
 * A [Background] specifically designed for underlining [Text] Nodes,
 * where it isn't possible to use a [Border].
 *
 * [ratio] is the position of the underline as a proportion of the height.
 * The default is 0.9f, because 1.0f is too far down.
 */
class UnderlineBackground(val thickness: Float = 1f, val ratio: Float = 0.9f) : Background {

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        backend.fillRect(x, y + height * ratio - thickness, x + width, y + height * ratio, color)
    }

    override fun toString() = "UnderlineBackground( $thickness )"
}

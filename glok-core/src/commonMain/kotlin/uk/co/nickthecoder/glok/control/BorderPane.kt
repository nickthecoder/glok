package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.MutableObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.*

/**
 * Ascii-art showing how the children are laid out :
 *
 *     ┌───────────────────────────┐
 *     │           top             │
 *     ├──────┬────────────┬───────┤
 *     │      │            │       │
 *     │      │            │       │
 *     │ left │   center   │ right │
 *     │      │            │       │
 *     │      │            │       │
 *     ├──────┴────────────┴───────┤
 *     │          bottom           │
 *     └───────────────────────────┘
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
 */

class BorderPane : Node() {

    override val children: MutableObservableList<Node> = mutableListOf<Node>().asMutableObservableList().apply {
        addChangeListener(childrenListener)
    }

    // region ==== Properties ====

    /**
     * One of the five possible child Nodes.
     */
    val centerProperty by optionalNodeProperty(null)
    var center by centerProperty

    /**
     * One of the five possible child Nodes.
     */
    val topProperty by optionalNodeProperty(null)
    var top by topProperty

    /**
     * One of the five possible child Nodes.
     */
    val rightProperty by optionalNodeProperty(null)
    var right by rightProperty

    /**
     * One of the five possible child Nodes.
     */
    val bottomProperty by optionalNodeProperty(null)
    var bottom by bottomProperty

    /**
     * One of the five possible child Nodes.
     */
    val leftProperty by optionalNodeProperty(null)
    var left by leftProperty

    // endregion ==== End of Properties ====

    init {
        /*
         * When we change any of [center], [top], [right], [bottom] or [left],
         * ensure that [children] list is updated.
         */
        centerProperty.addChangeListener { _, old, new ->
            old?.let { children.remove(it) }
            new?.let { children.add(it) }
        }.also {
            topProperty.addChangeListener(it)
            rightProperty.addChangeListener(it)
            bottomProperty.addChangeListener(it)
            leftProperty.addChangeListener(it)
        }
    }

    private fun middleRow() = listOfVisible(left, center, right)
    private fun topAndBottom() = listOfVisible(top, bottom)

    // region ==== Layout ====

    override fun nodeMinWidth(): Float {
        return max(middleRow().totalMinWidth(), topAndBottom().maxMinWidth())
    }

    override fun nodeMinHeight(): Float {
        return middleRow().maxMinHeight() + topAndBottom().totalMinHeight()
    }

    override fun nodePrefWidth(): Float {
        return max(middleRow().totalPrefWidth(), topAndBottom().maxPrefWidth())
    }

    override fun nodePrefHeight(): Float {
        return middleRow().maxPrefHeight() + topAndBottom().totalPrefHeight()
    }

    override fun layoutChildren() {
        val width = width
        val height = height

        val left = left.ifVisible()
        val right = right.ifVisible()
        val top = top.ifVisible()
        val bottom = bottom.ifVisible()

        val centerWidth = width - (left?.evalPrefWidth() ?: 0f) - (right?.evalPrefWidth() ?: 0f)
        val centerHeight = height - (top?.evalPrefHeight() ?: 0f) - (bottom?.evalPrefHeight() ?: 0f)

        var y = 0f
        top?.let { child ->
            val childPrefHeight = child.evalPrefHeight()
            setChildBounds(child, 0f, y, width, childPrefHeight)
            y += childPrefHeight
        }

        var x = 0f
        left?.let { child ->
            val childPrefWidth = child.evalPrefWidth()
            setChildBounds(child, 0f, y, childPrefWidth, centerHeight)
            x += childPrefWidth
        }
        center?.let { child ->
            setChildBounds(child, x, y, centerWidth, centerHeight)
            x += centerWidth
        }
        right?.let { child ->
            setChildBounds(child, x, y, child.evalPrefWidth(), centerHeight)
        }

        y += centerHeight
        bottom?.let { child ->
            setChildBounds(child, 0f, y, width, child.evalPrefHeight())
        }
    }

    // endregion ==== Layout ====

    override fun draw() {}

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.util.Matrix
import uk.co.nickthecoder.glok.util.log
import kotlin.math.PI
import kotlin.math.abs

/**
 * Rotates the [child] node by multiples of 90°
 */
class Rotation(child: Node, angle: Int = 0) : Transformation(child) {

    /**
     * The angle of rotation (clockwise in degrees). Valid values are -270, -180, -90, 0, 90, 180, 270
     */
    val angleProperty by intProperty(angle)
    var angle by angleProperty

    init {
        angleProperty.addChangeListener { _, _, value ->
            if (! validAngles.contains(angle)) log.error("Rotation. Invalid angle $value")
            requestLayout()
        }
    }


    override fun nodeMinWidth() = if ((abs(angle) / 90) % 2 == 1) child.nodeMinHeight() else child.nodeMinWidth()
    override fun nodeMinHeight() = if ((abs(angle) / 90) % 2 == 1) child.nodeMinWidth() else child.nodeMinHeight()
    override fun nodePrefWidth() = if ((abs(angle) / 90) % 2 == 1) child.nodePrefHeight() else child.nodePrefWidth()
    override fun nodePrefHeight() = if ((abs(angle) / 90) % 2 == 1) child.nodePrefWidth() else child.nodePrefHeight()
    override fun nodeMaxWidth() = if ((abs(angle) / 90) % 2 == 1) child.nodeMaxHeight() else child.nodeMaxWidth()
    override fun nodeMaxHeight() = if ((abs(angle) / 90) % 2 == 1) child.nodeMaxWidth() else child.nodeMaxHeight()

    override fun layoutChildren() {
        val offsetX = when (angle) {
            90, 180, - 180, - 270 -> width
            else -> 0f
        }
        val offsetY = when (angle) {
            180, - 180, - 90, 270 -> height
            else -> 0f
        }
        //println("pos $sceneX , $sceneY size $width x $height offset $offsetX , $offsetY ")
        matrix = Matrix.translation(sceneX + offsetX, sceneY + offsetY).rotate(angle * TO_RADIANS)
            .translate(- sceneX, - sceneY)
        inverse = Matrix.translation(sceneX, sceneY).rotate(- angle.toFloat() * TO_RADIANS)
            .translate(- sceneX - offsetX, - sceneY - offsetY)

        if ((abs(angle) / 90) % 2 == 1) {
            setChildBounds(child, 0f, 0f, height, width)
        } else {
            setChildBounds(child, 0f, 0f, width, height)
        }
    }

    private companion object {
        const val TO_RADIANS = PI / 180.0
        val validAngles = listOf(- 270, - 180, - 90, 0, 90, 180, 270)
    }
}

package uk.co.nickthecoder.glok.scene

/**
 * The default border for a [Region]. Takes no space, and draws nothing.
 */
object NoBorder : Border {
    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {}

    override fun toString() = "NoBorder"
}

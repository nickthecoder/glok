/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.history.Change
import uk.co.nickthecoder.glok.history.DocumentListener
import uk.co.nickthecoder.glok.history.HistoryDocument
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.padding
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.HighlightRange
import uk.co.nickthecoder.glok.text.StyledTextDocument
import uk.co.nickthecoder.glok.theme.styles.STYLED_TEXT_AREA
import uk.co.nickthecoder.glok.util.max
import uk.co.nickthecoder.glok.util.min

/**
 * A [TextArea] with the ability to style the text (in a limited way).
 * This is designed as a code editor, where keywords, constants, Strings etc. can be displayed using different
 * colors.
 *
 * It also allows portions of the text to be highlighted (e.g. for text which matches a search string).
 * The highlighting is limited - the line height is assumed to be the same for ALL text styles.
 *
 * NOTE. Theming this is a bit weird (you may even consider it buggy). The [font] is stylable in the same way
 * as [TextArea], however, this is only used to calculate caret positions, line heights etc., and is NOT
 * used when drawing the text. Drawing the text uses the styles of the [Text] nodes (which may be styled differently).
 * So if they don't match, the caret position won't match the rendered text. Ooops!
 *
 * So if the styled [Text] use a different font to [StyledTextArea]'s [font], they must all be the same size
 * (i.e. a fixed width font at the same point size).
 *
 * At present StyledTextArea is not suitable for editing rich text for two reasons :
 *
 * 1. All text must be the same size
 * 2. The [StyledTextDocument.ranges] are not part of the history mechanism, so there is no way to add/remove
 *    ranges which is compatible with undo/redo.
 *
 */
class StyledTextArea(document: StyledTextDocument) : TextAreaBase<StyledTextDocument>(document) {

    constructor(initialText: String) : this(StyledTextDocument(initialText))
    constructor(stringProperty: StringProperty) : this(StyledTextDocument("")) {
        textProperty.bindTo(stringProperty)
    }

    override val container: Container = StyledTextAreaContainer()

    init {
        style(STYLED_TEXT_AREA)
        scrolled.children.add(1, container) // After the left gutter.
    }

    /**
     * Parses [str], looking for style markings, which are single characters from the keys of [styles].
     * The style markings are removed and the remainder is used as the document's [text].
     *
     * All existing [HighlightRange]s in the document are replaced.
     *
     * [styles] specifies which [Highlight] to add to text enclosed in a particular character.
     * For example,
     *
     *     mapOf( '_' to ThemeHighlight(".underline") )
     *
     * Would apply [Node.style] `.underline` to all text that enclosed with underscores.
     *
     * If you wish to use a different symbol as the terminator, then use [terminators].
     * For example,
     *
     *     terminators = mapOf( ']' to '[' )
     *
     * Allows text within `[` ... `]` to be styled (you must also add `[` to [styles]).
     *
     * Backslash can be used to `escape` any character (which may clash with a style marking). e.g.
     *
     *     str = "No \_underlined\_ text"
     *
     * Would set the document text to `No _underlined_ text`, with no [HighlightRange]s.
     */
    fun setSimpleStyledText(str: String, styles: Map<Char, Highlight>, terminators: Map<Char, Char> = emptyMap()) {

        val builder = StringBuilder()
        val newRanges = mutableListOf<HighlightRange>()

        val startedTags = mutableMapOf<Char, TextPosition>()

        var row = 0
        var column = 0
        var escape = false

        for (c in str) {

            val styleTag = terminators[c] ?: c
            val style = styles[styleTag]

            if (! escape && style != null) {
                val startedTag = startedTags[styleTag]
                if (startedTag == null) {
                    startedTags[styleTag] = TextPosition(row, column)
                } else {
                    newRanges.add(
                        HighlightRange(startedTag, TextPosition(row, column), style)
                    )
                    startedTags.remove(styleTag)
                }
            } else {
                if (! escape && c == '\\') {
                    escape = true
                } else {
                    if (c == '\n') {
                        row ++
                        column = 0
                    } else {
                        column ++
                    }
                    builder.append(c)
                    escape = false
                }
            }
        }
        text = builder.toString()
        document.ranges.clear()
        document.ranges.addAll(newRanges)

    }

    // region == inner class StyledTextAreaContainer ==

    /**
     * Draws the text by having [Text] children for each line of text that is visible.
     * If the document has [HighlightRange]s, then each line may be made up of multiple
     */
    private inner class StyledTextAreaContainer : Container(), DocumentListener {

        private var rebuild: Boolean = true
        private var hadSelection: Boolean = false

        override val children = mutableListOf<Node>().asMutableObservableList()

        private val listener = invalidationListener {
            rebuild = true
            requestLayout()
        }

        val selectionListener = invalidationListener {
            val hasSelection = caretPosition != anchorPosition
            if (hasSelection || hasSelection != hadSelection) {
                rebuild = true
                requestLayout()
            }
        }

        init {
            children.addChangeListener(childrenListener)

            caretPositionProperty.addListener(selectionListener)
            anchorPositionProperty.addListener(selectionListener)

            document.ranges.addChangeListener { _, _ ->
                rebuild = true
                requestLayout()
            }

            // Note. When scrolling, we are currently rebuilding the entire set of Text children.
            // This could be optimised
            for (prop in listOf(
                scrollPane.vScrollValueProperty,
                fontProperty,
                lineHeightProperty
            )) {
                prop.addListener(listener)
            }
        }

        // Note. When the document changes in any way, we are rebuilding the entire set of Text children.
        // This could be optimised.
        override fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean) {
            rebuild = true
            requestLayout()
        }

        override fun layoutChildren() {
            val fullLineHeight = font.height * lineHeight
            val visibleBounds = scrollPane.visibleContentBounds()
            val firstVisibleRow = max(0, (visibleBounds.top / fullLineHeight - 1).toInt())
            val lastVisibleRow = min(document.lines.size, (visibleBounds.bottom / fullLineHeight + 1).toInt())
            val halfLineHeightPadding = (lineHeight - 1) * font.height / 2
            if (rebuild) {
                children.clear()
                val visibleRanges = document.ranges.filter {
                    it.to.row >= firstVisibleRow && it.from.row < lastVisibleRow
                }

                var y = surroundTop() + firstVisibleRow * fullLineHeight

                for (row in firstVisibleRow until lastVisibleRow) {
                    val line = document.lines[row]
                    val lineLength = line.length

                    // Prepare a map of all transitions that affect this line of text.
                    // A transition is a TextPosition where a HighlightRange starts or ends.
                    val transitions = mutableListOf(
                        Transition(0),
                        Transition(lineLength)
                    )

                    fun findTransition(pos: TextPosition): Transition? {
                        val column = if (pos.row < row) 0 else if (pos.row > row) return null else pos.column
                        return transitions.firstOrNull { it.column == column }
                            ?: Transition(column).also { transitions.add(it) }
                    }
                    for (hr in visibleRanges) {
                        if (hr.to.row >= row && hr.from.row <= row) {
                            val from = findTransition(hr.from)
                            // Edge Case. Do NOT include the highlights if the highlight ends on column 0 of this row.
                            if (from != null && ! (hr.to.row == row && hr.to.column == 0)) {
                                from.addHighlights.add(hr.highlight)
                            }
                            findTransition(hr.to)?.removeHighlights?.add(hr.highlight)
                        }
                    }
                    // Now we know the columns where the style of the text needs to change.
                    // So we sort them by column, and create a [Text] node for each fragment.
                    val sortedTransitions = transitions.sorted()

                    val highlights = mutableListOf<Highlight>()
                    var x = surroundLeft()
                    for (i in 1 until sortedTransitions.size) {
                        val fromTransition = sortedTransitions[i - 1]
                        val toTransition = sortedTransitions[i]
                        val fromColumn = min(fromTransition.column, lineLength)
                        val toColumn = min(toTransition.column, lineLength)
                        highlights.addAll(fromTransition.addHighlights)
                        val fragment = line.substring(fromColumn, toColumn)

                        val textNode = Text(fragment).apply {
                            padding(halfLineHeightPadding, 0)
                        }

                        textNode.indentationColumns = indentation.columns
                        val fragmentWidth = font.widthOf(fragment, indentation.columns, fromColumn)
                        setChildBounds(textNode, x, y, fragmentWidth, fullLineHeight)
                        for (h in highlights) {
                            h.style(textNode)
                        }
                        // Note. Cannot use removeAll, because that will remove multi copies of the same highlight.
                        // e.g. If we have TWO sets of bold, at the end of each transition, we only remove 1 of
                        // the bold highlights.
                        for (h in toTransition.removeHighlights) {
                            highlights.remove(h)
                        }

                        // Note, this must be AFTER the rect (for z-order).
                        children.add(textNode)
                        x += fragmentWidth
                    }

                    y += fullLineHeight
                }
            }

        }

        /*
         * Unlike TextArea, the text isn't drawn by this node directly,
         * instead, there are many [Text] nodes which draw themselves.
         * There are only [Text] nodes within the _visible_ part of the viewport.
         */
        override fun draw() {
            super.draw()
            val fullLineHeight = font.height * lineHeight

            val selectionStart = selectionStart
            val selectionEnd = selectionEnd

            if (selectionStart != selectionEnd) {
                // Draw the selection background in the same way as TextArea.
                // The background color is determined by [highlightColor].
                val x = sceneX + surroundLeft()
                val y = sceneY + surroundTop()

                val fromX = sceneX + surroundLeft() + widthOf(
                    document.lines[selectionStart.row].substring(
                        0,
                        selectionStart.column
                    )
                )
                val fromY = sceneY + surroundTop() + selectionStart.row * fullLineHeight
                if (selectionEnd.row == selectionStart.row) {
                    // Only a single line of selection
                    val toX = x + widthOf(document.lines[selectionStart.row].substring(0, selectionEnd.column))
                    backend.fillRect(
                        fromX, fromY, toX, fromY + fullLineHeight, highlightColor
                    )
                } else {
                    // More than 1 line of selection
                    // First line, from selection start to right edge of the container
                    backend.fillRect(
                        fromX, fromY, sceneX + width - surroundRight(), fromY + fullLineHeight, highlightColor
                    )
                    // Middle lines of the selection (the whole width of the container)
                    val startY = y + (selectionStart.row + 1) * fullLineHeight
                    val endY = y + (selectionEnd.row) * fullLineHeight
                    backend.fillRect(
                        x, startY, sceneX + width - surroundRight(), endY, highlightColor
                    )
                    // Last line of selection
                    val endX = x + widthOf(document.lines[selectionEnd.row].substring(0, selectionEnd.column))
                    val rectTop = y + selectionEnd.row * fullLineHeight
                    backend.fillRect(
                        x, rectTop, endX, rectTop + fullLineHeight, highlightColor
                    )

                }
            }

            if (this@StyledTextArea.focused && caretVisible) {
                val caretX = sceneX + caretX()
                val caretY = sceneY + caretY()
                caretImage?.let { image ->
                    val scale = fullLineHeight / image.imageHeight
                    val width = image.imageWidth * scale
                    val height = image.imageHeight * scale
                    image.drawTo(caretX - width / 2, caretY, width, height, caretColor)
                }
            }
        }
    }

    /**
     * Used while drawing - it marks the start/end of a HighlightRange.
     * [addHighlights] are all of the highlights that start at this transition, and
     * [removeHighlights] are all of the highlights that finish at this transition.
     */
    private class Transition(val column: Int) : Comparable<Transition> {

        val addHighlights = mutableListOf<Highlight>()
        val removeHighlights = mutableListOf<Highlight>()

        override fun compareTo(other: Transition): Int {
            return column.compareTo(other.column)
        }

        override fun toString() = "$column +$addHighlights -$removeHighlights"
    }

    // endregion StyledTextContainer

}

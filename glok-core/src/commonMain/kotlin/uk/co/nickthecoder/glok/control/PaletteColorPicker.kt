/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.PlainBackground
import uk.co.nickthecoder.glok.scene.dsl.radioButton2

/**
 * A control (not a dialog), for choosing a color from a palette.
 *
 * Clicking one of the palette swatches changes the [colorProperty].
 *
 * The [palette] defaults to a 'rainbow' of colors in a 12x11 grid.
 * If you change the [palette], you may well want to change the number of [columns] and [rows] too.
 *
 * See [PaletteColorPickerDialog] for a complete dialog, which includes this control.
 */
class PaletteColorPicker(initialColor: Color = Color.WHITE) : Region() {

    // region ==== Properties ====

    val colorProperty by colorProperty(initialColor)
    var color by colorProperty

    val columnsProperty by intProperty(12)
    val columns by columnsProperty

    /**
     * Only used by [defaultPalette]. If you create your own [palette],
     * then `rows` is ignored.
     */
    val rowsProperty by intProperty(11)
    val rows by rowsProperty

    val swatchSizeProperty by stylableFloatProperty(20f)
    var swatchSize by swatchSizeProperty

    // endregion properties

    // region ==== Fields ====

    private val grid = VBox().apply { styles.add(".grid") }

    /**
     * NOTE, altering the palette rebuilds the GUI, which can be quite sluggish.
     * So if you make _lots_ of changes to the palette, consider clearing it,
     * and then use `palette.addAll( newColors )`,
     * rather than adding colors one by one.
     * Without this trick, rebuilding a palette might take a couple of seconds.
     */
    val palette = mutableListOf<Color>().asMutableObservableList()

    override val children = listOf(grid).asObservableList()

    private val toggleGroup = ToggleGroup()

    // endregion

    // region ==== init ====

    init {
        styles.add("palette_color_picker")
        claimChildren()

        defaultPalette()
        columnsProperty.addListener { build() }.also {
            palette.addListener(it)
        }
        build()
    }

    // endregion

    fun defaultPalette() {
        val newPalette = mutableListOf<Color>()
        for (x in columns - 1 downTo 0) {
            val grey = x.toFloat() / (columns - 1)
            newPalette.add(Color(grey, grey, grey))
        }
        val rows = rows - 1
        for (y in 0 until rows) {
            val saturation = 0.5f
            val lightness = (y + 2).toFloat() / (rows + 2)
            for (hueIndex in 0 until columns) {
                val hue = hueIndex.toFloat() / columns
                val color = Color.hsl(hue, saturation, lightness)
                newPalette.add(color)
            }
        }
        palette.clear()
        palette.addAll(newPalette)
    }

    private fun build() {
        grid.children.clear()

        var row = HBox().apply {
            styles.add(".row")
        }
        grid.children.add(row)
        var column = 0
        for (col in palette) {
            if (column == columns) {
                row = HBox().apply {
                    styles.add(".row")
                }
                grid.children.add(row)
                column = 0
            }

            row.children.add(radioButton2(toggleGroup, "") {
                overridePrefWidthProperty.bindCastTo(swatchSizeProperty)
                overridePrefHeightProperty.bindCastTo(swatchSizeProperty)
                overrideMinWidth = 0f
                overrideMinHeight = 0f
                background = PlainBackground
                backgroundColor = col
                selected = col == color
                onAction { color = col }
            })

            column++
        }
    }

    // region ==== Layout ====
    override fun nodePrefWidth() = surroundX() + grid.evalPrefWidth()
    override fun nodePrefHeight() = surroundY() + grid.evalPrefHeight()
    override fun nodeMinWidth() = surroundX() + grid.evalMinWidth()
    override fun nodeMinHeight() = surroundY() + grid.evalMinHeight()
    override fun nodeMaxWidth() = nodePrefWidth()
    override fun nodeMaxHeight() = nodePrefHeight()

    override fun layoutChildren() {
        setChildBounds(grid, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }
    // endregion ==== Layout ====

    override fun toString() = super.toString() + " columns=$columns color=$color"

}

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.control.Region
import uk.co.nickthecoder.glok.util.max2DPs

/**
 * Empty space surrounding the content of a [Region].
 */
data class Edges(
    val top: Float,
    val right: Float,
    val bottom: Float,
    val left: Float
) {

    constructor(y: Float, x: Float) : this(y, x, y, x)
    constructor(size: Float) : this(size, size, size, size)
    constructor() : this(0f)

    val x get() = left + right
    val y get() = top + bottom

    override fun equals(other: Any?): Boolean {
        return other is Edges &&
                top == other.top && other.right == right && bottom == other.bottom && left == other.left
    }

    override fun hashCode(): Int {
        return top.toInt() + right.toInt() * 17 + bottom.toInt() * 51 + left.toInt() * 1577
    }

    override fun toString() = if (left == right && top == bottom) {
        if (left == top) {
            "( ${top.max2DPs()} )"
        } else {
            "( ${top.max2DPs()}, ${right.max2DPs()} )"
        }
    } else {
        "( ${top.max2DPs()}, ${right.max2DPs()}, ${bottom.max2DPs()}, ${left.max2DPs()} )"
    }

    companion object {
        val NONE = Edges(0f, 0f, 0f, 0f)
    }

}

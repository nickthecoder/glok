package uk.co.nickthecoder.glok.scene

data class CalculatedSizes(
    val minWidth: Float,
    val minHeight: Float,

    val prefWidth: Float,
    val prefHeight: Float,

    val maxWidth: Float,
    val maxHeight: Float
)

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.LongProperty
import uk.co.nickthecoder.glok.property.boilerplate.longProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedLongProperty
import uk.co.nickthecoder.glok.property.functions.div
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.property.functions.toDouble
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.SLIDER
import uk.co.nickthecoder.glok.util.clamp
import kotlin.math.roundToInt

/**
 * Edits a [LongProperty] by typing digits into [TextArea], via up/down buttons, up/down keys
 * or by scrolling the mouse/trackpad.
 *
 * The range of values are bounded ([min] and [max]).
 *
 * For more details, see the base classes [SliderBase].
 */
class LongSlider : SliderBase() {

    // region ==== Properties ====

    val valueProperty by validatedLongProperty(0L) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    var value by valueProperty

    val minProperty by longProperty(0L)
    var min by minProperty

    val maxProperty by longProperty(100)
    var max by maxProperty

    val smallStepProperty by longProperty(1)
    var smallStep by smallStepProperty

    val largeStepProperty by longProperty(10)
    var largeStep by largeStepProperty

    // endregion

    // region ==== Fields ====

    /**
     * Allows snapping
     */
    var snap: Snap<Long>? = null

    override var ratio: Double
        get() = if (max <= min) 0.0 else ((value - min) / (max - min)).toDouble()
        set(v) {
            val convertedValue = if (max <= min) min else min + ((max - min) * v).roundToInt()
            value = snap?.snap(convertedValue) ?: convertedValue
        }

    // endregion fields

    // region ==== init ====

    init {
        style(SLIDER)

        for (prop in listOf(minProperty, maxProperty, valueProperty)) {
            // NOTE. We are setting the thumb's position in drawChildren,
            // so we do NOT need to requestLayout, only requestRedraw.
            prop.addListener(requestRedrawListener)
        }
        smallRatioStepProperty.bindTo((smallStepProperty / (maxProperty - minProperty)).toDouble())
        largeRatioStepProperty.bindTo((largeStepProperty / (maxProperty - minProperty)).toDouble())
    }

    // endregion init

    // ==== Object Methods ====

    override fun toString() = super.toString() + " { $min .. $max } = $value"

}

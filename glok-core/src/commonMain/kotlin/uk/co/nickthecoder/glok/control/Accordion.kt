package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.documentation.Demos

/**
 * This class is _NOT USED_, it is only here for Documentation purposes.
 *
 * Glok has no Accordion control, because it is simple to create the behaviour using existing classes.
 *
 * Create a `ToggleGroup`, and a `VBox`.
 * Add `TitledPane`s to `VBox.children`, and also set their `toggleGroup`.
 *
 * See [DemoTitledPane][Demos] for example code.
 */
abstract class Accordion {}

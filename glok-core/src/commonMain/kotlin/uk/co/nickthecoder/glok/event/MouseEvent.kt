package uk.co.nickthecoder.glok.event

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Scene

/**
 * The base class for [MouseEvent] and [ScrollEvent].
 */
abstract class MouseEventBase(

    val scene: Scene,

    /**
     * The X position of the mouse, relative to the left of the scene.
     * For a position relative to a node :
     *
     *     localX = event.sceneX - node.sceneX
     */
    val sceneX: Float,

    /**
     * The X position of the mouse, relative to the top of the scene.
     * For a position relative to a node :
     *
     *     localX = event.sceneY - node.sceneY
     */
    val sceneY: Float,

    val mods: Int

) : Event() {

    val isShiftDown get() = isShiftDown(mods)
    val isControlDown get() = isControlDown(mods)
    val isAltDown get() = isAltDown(mods)
    val isSuperDown get() = isSuperDown(mods)
}

enum class MouseButton {
    PRIMARY, SECONDARY, MIDDLE, NONE
}

/**
 * Used for event types [EventType.MOUSE_PRESSED], [EventType.MOUSE_RELEASED],  [EventType.MOUSE_CLICKED],
 * [EventType.MOUSE_MOVED], [EventType.MOUSE_EXITED], [EventType.MOUSE_ENTERED] and [EventType.MOUSE_DRAGGED].
 *
 */
class MouseEvent(
    override val eventType: EventType<MouseEvent>,

    scene: Scene,
    sceneX: Float,
    sceneY: Float,

    /**
     *
     * -1 = None,  0 = Left,  1 = Right,  2 = Middle.
     */
    val buttonIndex: Int,

    /**
     *
     */
    val clickCount: Int,

    mods: Int

) : MouseEventBase(scene, sceneX, sceneY, mods) {

    val button: MouseButton = when (buttonIndex) {
        0 -> MouseButton.PRIMARY
        1 -> MouseButton.SECONDARY
        2 -> MouseButton.MIDDLE
        else -> MouseButton.NONE
    }

    /**
     * 0 = None,  1 = Left,  2 = Right,  3 = Middle.
     * @return [buttonIndex] + 1
     */
    val buttonNumber: Int get() = buttonIndex + 1

    val isPrimary get() = button == MouseButton.PRIMARY
    val isSecondary get() = button == MouseButton.SECONDARY
    val isMiddle get() = button == MouseButton.MIDDLE

    internal var captured: Boolean = false

    /**
     * Use within [EventType.MOUSE_PRESSED] events only to indicate that this node wants to recieve
     * [EventType.MOUSE_DRAGGED] events until the mouse button is released.
     * If called on other event types, it will be ignored.
     *
     * While the mouse is captured, no other nodes will receive mouse events.
     * Mouse movements will be delivered to the capturing node as [EventType.MOUSE_DRAGGED],
     * (not as [EventType.MOUSE_MOVED])
     * and will be sent even when the mouse is outside the Node's bounds.
     */
    // If/when alternate drag/drop gestures are implemented, this would be a good place
    // to specify which gesture type the node requires. e.g. fun capture( type : DragType=DragType.SIMPLE )
    fun capture() {
        captured = true
    }

    /**
     * On Windows, a popup trigger is when the secondary mouse button is PRESSED,
     * and on all other platforms, it is when the secondary mouse button is RELEASED.
     *
     * To make use of this, you would need to add this test to `MOUSE_PRESSED` and `MOUSE_RELEASED`
     * event handlers.
     * This is a PITA, it is much easier to use [Node.onPopupTrigger] instead.
     */
    fun isPopupTrigger(): Boolean = isSecondary && (
        if (Platform.isWindows()) {
            eventType === EventType.MOUSE_PRESSED
        } else {
            eventType === EventType.MOUSE_RELEASED
        })

    override fun toString() = "MouseEvent @ $sceneX , $sceneY $button"
}

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.BUTTON
import uk.co.nickthecoder.glok.theme.styles.DEFAULT_BUTTON

/**
 *
 * ### Theme DSL
 *     "button" {
 *         // Button has no additional stylable properties.
 *     }
 *
 * Button inherits all the features of [ButtonBase].
 *
 */
class Button(text: String, graphic: Node? = null) : ButtonBase(text, graphic) {

    val defaultButtonProperty by booleanProperty(false)
    var defaultButton by defaultButtonProperty

    val cancelButtonProperty by booleanProperty(false)
    var cancelButton by cancelButtonProperty

    constructor(text: String) : this(text, null)

    init {
        style(BUTTON)
        defaultButtonProperty.addChangeListener { _, _, value -> pseudoStyleIf(value, DEFAULT_BUTTON) }
    }

}

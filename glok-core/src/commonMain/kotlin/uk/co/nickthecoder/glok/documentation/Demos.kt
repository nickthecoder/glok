package uk.co.nickthecoder.glok.documentation

/**
 * This interface is _NOT USED_, it is only here for [Documentation].
 *
 * Glok includes a number of demo applications.
 * They are in a separate subproject.
 *
 * The latest source code can be found on
 * [Gitlab.com](https://gitlab.com/nickthecoder/glok/-/tree/main/glok-demos/src/main/kotlin/uk/co/nickthecoder/glok/demos?ref_type=heads)
 *
 * I strongly recommend downloading the Glok source code, and playing around with them.
 *
 */
interface Demos

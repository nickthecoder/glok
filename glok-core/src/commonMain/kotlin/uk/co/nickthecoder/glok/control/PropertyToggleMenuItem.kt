/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyleIf
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.CHECK_MENU_ITEM
import uk.co.nickthecoder.glok.theme.styles.SELECTED

/**
 * Similar to a [ToggleMenuItem], but instead of using a [ToggleGroup], it uses a [property]
 * to determine which item is selected.
 */
class PropertyToggleMenuItem<V, P : Property<V?>>(

    val property: P,
    /**
     * This menu item is selected when [property].value == [value]
     */
    val value: V,
    text: String

) : SelectMenuItemBase(text) {

    override val selectedProperty by booleanProperty(false)
    override var selected by selectedProperty

    init {
        style(CHECK_MENU_ITEM)
        pseudoStyleIf(selected, SELECTED)

        selectedProperty.addChangeListener { _, _, selected ->
            pseudoStyleIf(selected, SELECTED)
            if (selected) {
                property.value = value
            } else if (property.value == value) {
                property.value = null
            }
        }
        property.addChangeListener { _, _, propValue ->
            selected = propValue == value
        }
    }

    override fun toggle() {
        selected = property.value == value
        property.value = if (property.value == value) null else value
    }

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.text

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * [Actions] used by [FindAndReplace]
 */
object FindAndReplaceActions : Actions(Tantalum.icons) {

    val CLOSE = define("close_find_replace", "Close Find/Replace", Key.ESCAPE.noMods()) {
        iconName = "close"
        tinted = true
    }

    val SHOW_FIND = define("find", "Find...", Key.F.control())
    val SHOW_REPLACE = define("replace", "Replace...", Key.R.control())
    val SHOW_GOTO_DIALOG = define("goto", "Go To...", Key.G.control())

    val REPLACE_ONE = define("replace_one", "Replace")
    val REPLACE_ALL = define("replace_all", "Replace All")

    val FIND_NEXT = define("find_next", "Next Match", Key.F3.noMods()) {
        this.iconName = "down"
        tinted = true
    }
    val FIND_PREV = define("find_prev", "Previous Match", Key.F3.noMods().shift()) {
        this.iconName = "up"
        tinted = true
    }
    val MATCH_CASE = define("match_case", "Cc", tooltip = "Match Case")
    val MATCH_REGEX = define("match_regex", ".*", tooltip = "Regular Expression")
    val MATCH_WORDS = define("match_word", "W", tooltip = "Match Words")
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.event.*
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalActionEventHandlerProperty
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.WithItems
import uk.co.nickthecoder.glok.scene.dsl.pseudoStyle
import uk.co.nickthecoder.glok.theme.styles.ARROW
import uk.co.nickthecoder.glok.theme.styles.HOVER
import uk.co.nickthecoder.glok.theme.styles.SUB_MENU
import uk.co.nickthecoder.glok.util.max

/**
 * A type of [MenuItem], which displays a [PopupMenu] when it is hovered over.
 * The [PopupMenu] is populated from [items].
 *
 * The user does not need to click a SubMenu to open it, in fact clicking does nothing special,
 * and [onAction] is ignored.
 */
class SubMenu(text: String, graphic: Node? = null) : LabelledMenuItem(text, graphic), WithItems {

    // region ==== Properties ====

    /**
     * An event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    val onShowingProperty by optionalActionEventHandlerProperty(null)
    var onShowing by onShowingProperty

    private val mutableShowingProperty by booleanProperty(false)

    /**
     * Set to true, after the button is clicked, and before the [PopupMenu] is displayed.
     * It is reset to false when the [PopupMenu] is closed.
     *
     * Note, it is often easier to use [onShowing] rather than listening to this property.
     *
     * Applications may listen to this, and adjust [items] as required.
     *
     * History. [onShowing] was added later than [showingProperty], but we keep this for backwards compatibility.
     * Also, [showingProperty] could be used to listen for when the menu is closed.
     */
    val showingProperty = mutableShowingProperty.asReadOnly()
    var showing by mutableShowingProperty
        private set

    // endregion Properties

    // region ==== Fields ====

    override val items = mutableListOf<Node>().asMutableObservableList()

    private val arrowImageView = ImageView(null)

    private var popupMenu: PopupMenu? = null

    // endregion Fields

    init {
        styles.add(SUB_MENU)
        arrowImageView.styles.add(ARROW)
        mutableChildren.add(arrowImageView)
    }

    override fun keyPressed(event: KeyEvent) {
        if (MenuItemActions.FORWARDS.matches(event)) {
            pseudoStyle(HOVER)
            openSubMenu(focusFirst = true)
            event.consume()
        } else {
            super.keyPressed(event)
        }
    }

    internal fun forwardsBackwards(delta: Int) {
        if (delta == - 1 && popupMenu != null) {
            popupMenu?.scene?.stage?.close()
        } else {
            PopupMenu.findPopupMenu(this)?.forwardsBackwards(delta)
        }
    }

    /**
     * Add an event handler which is performed after the button is clicked, but before the
     * popup menu is built.
     *
     * Use this to update [items] 'just-in-time'.
     */
    fun onShowing(
        handlerCombination: HandlerCombination = HandlerCombination.AFTER,
        block: (event: ActionEvent) -> Unit
    ) {
        combineActionEventHandlers(onShowingProperty, handlerCombination, block)
    }

    override fun mouseEntered() {
        super.mouseEntered()
        openSubMenu()
    }

    /**
     * Note. [Menu.openMenu] is very similar.
     */
    private fun openSubMenu(focusFirst: Boolean = false) {
        // Is the popupMenu already open?
        if (popupMenu != null) return

        onShowing?.tryCatchHandle(ActionEvent())
        showing = true
        val popupMenu = PopupMenu(this)
        this.popupMenu = popupMenu
        popupMenu.items.addAll(items)
        popupMenu.show(this, Side.RIGHT).apply {
            onClosed {
                popupMenu.deselectAll()
                popupMenu.items.clear()
                this@SubMenu.popupMenu = null
                showing = false
            }
        }
        if (focusFirst) {
            popupMenu.items.firstOrNull()?.requestFocus()
        }
    }

    override fun deselect() {
        pseudoStyles.remove(HOVER)
        popupMenu?.let { it.scene?.stage?.close() }
    }

    override fun performAction() {
    }

    // region ==== Layout ====
    override fun nodePrefWidth() = super.nodePrefWidth() + arrowImageView.evalPrefWidth()
    override fun nodePrefHeight() = max(super.nodePrefHeight(), surroundY() + arrowImageView.evalPrefHeight())

    override fun layoutChildren() {
        val arrowWidth = arrowImageView.evalPrefWidth()
        val arrowHeight = arrowImageView.evalPrefHeight()

        layoutMenuButtBase(width - arrowWidth)
        setChildBounds(arrowImageView,
            width - surroundRight() - arrowWidth,
            surroundTop() + (height - surroundY() - arrowHeight) / 2,
            arrowWidth,
            arrowHeight)
    }
    // endregion
}

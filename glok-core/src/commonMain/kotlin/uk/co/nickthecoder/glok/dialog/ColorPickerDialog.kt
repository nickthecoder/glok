/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dialog

import uk.co.nickthecoder.glok.control.ColorButton
import uk.co.nickthecoder.glok.control.PaletteColorPickerDialog
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.scene.Color

/**
 * A base class for [CustomColorPickerDialog] and [PaletteColorPickerDialog], so that either can be
 * use by [ColorButton].
 *
 * See [ColorButton.dialogFactory]
 */
abstract class ColorPickerDialog : Dialog() {

    val colorProperty by colorProperty(Color.WHITE)
    var color by colorProperty

    val chooseAlphaProperty by booleanProperty(false)
    var chooseAlpha by chooseAlphaProperty

}

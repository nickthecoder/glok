package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.util.Log
import uk.co.nickthecoder.glok.util.Matrix
import uk.co.nickthecoder.glok.util.max2DPs

/**
 * Logs calls to the [Backend], and the forwards the calls to an [inner] [Backend].
 *
 */
class LoggingBackend(val inner: Backend, val log: Log) : Backend {

    // ===== Log =====

    private fun log(text: String) = log.add(Log.TRACE, text)

    // ===== View =====

    override fun drawFrame(window: Window, block: () -> Unit) {
        log("==drawFrame==")
        try {
            inner.drawFrame(window, block)
        } finally {
            log("==endFrame==")
        }
    }

    override fun drawView(width: Float, height: Float, scale: Float, block: () -> Unit) {
        log("=beginView( $width x $height scale=$scale )")
        inner.drawView(width, height, scale, block)
        log("=endView()")
    }

    override fun processEvents(timeoutSeconds: Double) {
        // Do not log poll events, as it clogs up the log with irrelevance.
        // log("pollEvent()")
        inner.processEvents(timeoutSeconds)
    }

    override fun setMousePointer(window: Window, cursor: MousePointer) {
        log("setMousePointer( ${cursor.name} )")
        inner.setMousePointer(window, cursor)
    }

    // ===== Draw =====

    override fun clear(color: Color) {
        log("clear( $color )")
        inner.clear(color)
    }

    override fun beginClipping(left: Float, top: Float, width: Float, height: Float): Boolean {
        log("beginClipping( $left, $top, $width, $height )")
        return inner.beginClipping(left, top, width, height)
    }

    override fun endClipping() {
        log("endClipping()")
        inner.endClipping()
    }

    override fun noClipping(block: () -> Unit) {
        log("noClipping()")
        inner.noClipping(block)
        log("end noClipping")
    }

    override fun transform(transformation: Matrix, block: () -> Unit) {
        log("transform( $transformation )")
        inner.transform(transformation, block)
        log("end transform")
    }

    override fun transform(transformation: Matrix) {
        log("transform( $transformation )")
        inner.transform(transformation)
        log("end transform")
    }

    override fun clearTransform() {
        log("clearTransform")
        inner.clearTransform()
    }

    override fun fillRect(rect: GlokRect, color: Color) {
        log("fillRect( $rect, $color )")
        inner.fillRect(rect, color)
    }

    override fun fillRect(left: Float, top: Float, right: Float, bottom: Float, color: Color) {
        log("fillRect( $left $top -> $right $bottom , $color )")
        inner.fillRect(left, top, right, bottom, color)
    }

    override fun fillQuarterCircle(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, radius: Float) {
        log("fillQuarterCircle( $x1 $y1 -> $x2 $y2 , $color r=$radius)")
        inner.fillQuarterCircle(x1, y1, x2, y2, color, radius)
    }

    override fun strokeQuarterCircle(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, radius: Float, innerRadius: Float) {
        log("strokeQuarterCircle( $x1 $y1 -> $x2 $y2 , $color r=$radius,$innerRadius)")
        inner.strokeQuarterCircle(x1, y1, x2, y2, color, radius, innerRadius)
    }


    override fun strokeInsideRect(rect: GlokRect, thickness: Float, color: Color) {
        log("strokeInsideRect( $rect, $thickness $color )")
        inner.strokeInsideRect(rect, thickness, color)
    }

    override fun strokeInsideRect(
        left: Float, top: Float, right: Float, bottom: Float,
        thickness: Float, color: Color
    ) {
        log("strokeInsideRect( $left, $top -> $right $bottom , $thickness, $color )")
        inner.strokeInsideRect(left, top, right, bottom, thickness, color)
    }

    override fun drawTexture(
        texture: Texture,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float, destHeight: Float,
        modelMatrix : Matrix?
    ) {
        log("drawTexture( ${texture.name} from ${srcX.max2DPs()} , ${srcY.max2DPs()} size ${width.max2DPs()} , ${height.max2DPs()} to ${destX.max2DPs()} , ${destY.max2DPs()} ) size ${destWidth.max2DPs()} x ${destHeight.max2DPs()}")
        inner.drawTexture(texture, srcX, srcY, width, height, destX, destY, destWidth, destHeight, modelMatrix)
    }

    override fun drawTintedTexture(
        texture: Texture,
        tint: Color,
        srcX: Float, srcY: Float, width: Float, height: Float,
        destX: Float, destY: Float, destWidth: Float, destHeight: Float,
        modelMatrix: Matrix?
    ) {
        log("drawPartialTexture( ${texture.name} $tint from ${srcX.max2DPs()} , ${srcY.max2DPs()} size ${width.max2DPs()} , ${height.max2DPs()} to ${destX.max2DPs()} , ${destY.max2DPs()} )")
        inner.drawTintedTexture(texture, tint, srcX, srcY, width, height, destX, destY, destWidth, destHeight, modelMatrix)
    }

    override fun gradient(triangles: FloatArray, colors: Array<Color>) {
        log("gradient( $triangles $colors )")
        inner.gradient(triangles, colors)
    }

    override fun hsvGradient(triangles: FloatArray, colors: Array<FloatArray>) {
        log("gradient( $triangles $colors )")
        inner.hsvGradient(triangles, colors)
    }

    override fun batch(texture: Texture, tint: Color?, modelMatrix: Matrix?, block: TextureBatch.() -> Unit) {
        log("using ${texture.name} tint $tint")
        inner.batch(texture, tint, modelMatrix) {
            val innerThis = this
            object : TextureBatch {
                override fun draw(srcX: Float, srcY: Float, width: Float, height: Float, destX: Float, destY: Float, destWidth: Float, destHeight: Float) {
                    log("draw( ${srcX.max2DPs()} , ${srcY.max2DPs()} size ${width.max2DPs()} , ${height.max2DPs()} to ${destX.max2DPs()} , ${destY.max2DPs()} )")
                    innerThis.draw(srcX, srcY, width, height, destX, destY, destWidth, destHeight)
                }
            }.block()
        }
    }

    // ===== Factory =====

    override fun createWindow(width: Int, height: Int): Window {
        log("createWindow $width x $height")
        return inner.createWindow(width, height)
    }

    override fun createTexture(name: String, width: Int, height: Int, pixels: IntArray): Texture {
        log("createTexture( $name $width x $height )")
        return inner.createTexture(name, width, height, pixels)
    }

    // ===== Other =====

    override fun monitorSize(): Pair<Int, Int>? {
        val result = inner.monitorSize()
        log("monitorSize() = $result")
        return result
    }

    override fun resources(path: String): Resources {
        log("jarResources( $path )")
        return inner.resources(path)
    }

    override fun fileResources(path: String): Resources {
        log("fileResources( $path )")
        return inner.fileResources(path)
    }
}

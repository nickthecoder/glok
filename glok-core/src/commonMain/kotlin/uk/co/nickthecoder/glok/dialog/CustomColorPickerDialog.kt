/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.dialog

import uk.co.nickthecoder.glok.control.ButtonMeaning
import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.control.CustomColorPicker
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableFloatProperty
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.meaning

class CustomColorPickerDialog : ColorPickerDialog() {

    /**
     * The size of the width and height of the square area, and the height of the color slider.
     */
    val colorFieldSizeProperty by stylableFloatProperty(256f)
    var colorFieldSize by colorFieldSizeProperty

    /**
     * The size of the width and height of the square area, and the height of the color slider.
     */
    val swatchSizeProperty by stylableFloatProperty(50f)
    var swatchSize by swatchSizeProperty

    val showLabelsProperty by stylableBooleanProperty(false)
    var showLabels by showLabelsProperty

    val showCurrentColorProperty by stylableBooleanProperty(true)
    var showCurrentColor by showCurrentColorProperty

    private lateinit var colorPicker: CustomColorPicker

    private var originalColor = color

    init {

        onCreate {
            originalColor = color
            stage.resizable = false
            colorPicker = CustomColorPicker(color, chooseAlpha)
            colorProperty.bidirectionalBind(colorPicker.colorProperty)

            content = colorPicker
            colorPicker.colorFieldSize = colorFieldSize
            colorPicker.swatchSize = swatchSize
            colorPicker.showLabels = showLabels
            colorPicker.showCurrentColor = showCurrentColor
        }

        buttonTypes(ButtonType.CANCEL) {
            colorProperty.bidirectionalUnbind(colorPicker.colorProperty)
            color = originalColor
        }

        buttonBar.apply {
            meaning(ButtonMeaning.OK) {
                + button("Select") {
                    defaultButton = true
                    onAction {
                        colorProperty.bidirectionalUnbind(colorPicker.colorProperty)
                        scene?.stage?.close()
                        color = colorPicker.color
                    }
                }
            }
        }
    }
}


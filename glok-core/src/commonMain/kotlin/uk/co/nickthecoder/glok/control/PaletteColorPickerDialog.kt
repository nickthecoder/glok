/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.dialog.ColorPickerDialog
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stylableBooleanProperty
import uk.co.nickthecoder.glok.property.changeListener
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.meaning

/**
 * Shows a set of colors in a grid. Click a color to select it (the dialog is closed).
 *
 * Clicking the `Custom Color` button, replaces the [PaletteColorPicker] control with [CustomColorPicker] control.
 * This feature can be disabled by setting [allowCustomColors] to `false`.
 *
 */
class PaletteColorPickerDialog : ColorPickerDialog() {

    /**
     * When true, a "Custom Color" button is visible,
     * Click it to switch from using a [PaletteColorPicker] to a [CustomColorPicker].
     */
    val allowCustomColorsProperty by booleanProperty(true)
    var allowCustomColors by allowCustomColorsProperty

    /**
     * Only used after switching to a [CustomColorPicker]
     */
    val showLabelsProperty by stylableBooleanProperty(false)
    var showLabels by showLabelsProperty

    val paletteColorPicker = PaletteColorPicker()

    /**
     * [PaletteColorPicker] doesn't have a "Select" button, so instead, we listen for when its color changes,
     * and update [color].
     */
    private val paletteColorListener = changeListener { _, _, newColor: Color ->
        color = newColor
        stage.close()
    }

    init {
        content = paletteColorPicker

        onCreate {
            stage.resizable = false
            paletteColorPicker.color = color
            paletteColorPicker.colorProperty.addChangeListener{ _, _, newColor: Color ->
                color = newColor
                stage.close()
            }
        }

        buttonBar.apply {

            meaning(ButtonMeaning.LEFT) {

                // When this button is clicked, the SAME dialog is reused, replacing the content
                // with a CustomColorPicker, and changing the button in the Dialog's ButtonBar.

                + button("Custom Color") {
                    visibleProperty.bindTo(allowCustomColorsProperty)
                    onAction {
                        paletteColorPicker.colorProperty.removeChangeListener(paletteColorListener)
                        visibleProperty.unbind()
                        val customColorPicker = CustomColorPicker(color, showLabels).apply {
                            showLabels = this@PaletteColorPickerDialog.showLabels
                        }

                        content = customColorPicker
                        stage.resizeToFit()

                        // Make this button invisible, and add two other ones.
                        visible = false
                        meaning(ButtonMeaning.OK) {
                            button("Select") {
                                defaultButton = true
                                onAction {
                                    color = customColorPicker.color
                                    stage.close()
                                }
                            }
                        }
                        meaning(ButtonMeaning.CANCEL) {
                            button("Cancel") {
                                cancelButton = true
                                onAction {
                                    stage.close()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

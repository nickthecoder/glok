package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.fixedFormatProperty
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.property.boilerplate.validatedFloatProperty
import uk.co.nickthecoder.glok.theme.styles.FLOAT_SPINNER
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.max2DecimalPlaces

class FloatSpinner(initialValue : Float = 0f) : ComparableSpinner<Float, FloatProperty>() {

    // ==== Properties ====

    override val valueProperty by validatedFloatProperty(0f) { _, _, newValue ->
        newValue.clamp(min, max)
    }
    override var value by valueProperty

    override val smallStepProperty by floatProperty(1f)
    override var smallStep by smallStepProperty

    override val largeStepProperty by floatProperty(10f)
    override var largeStep by largeStepProperty

    override val minProperty by floatProperty(- Float.MAX_VALUE)
    override var min by minProperty

    override val maxProperty by floatProperty(Float.MAX_VALUE)
    override var max by maxProperty

    val formatProperty by fixedFormatProperty(max2DecimalPlaces)
    var format by formatProperty

    // ==== End of properties ====

    override var converter = object : Converter<Float, String> {
        override fun forwards(value: Float) = format.format(value.toDouble())
        override fun backwards(value: String) = value.toFloat()
    }

    // ==== init ====

    init {
        styles.add(FLOAT_SPINNER)
        initialise()
        value = initialValue
    }

    // ==== End of init ====

    override fun adjustment(direction: Int, byLargeStep: Boolean)
        = value + direction * (if (byLargeStep) largeStep else smallStep)


}

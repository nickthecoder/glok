package uk.co.nickthecoder.glok.backend

import kotlin.math.*

/**
 * Holds an [x], [y] coordinate.
 *
 * Used to pass `point` data from the core of Glok to the [Backend].
 * Note, that we don't use [Vector2f] from `joml`, because I don't want the core of Glok
 * to be dependent on `joml`. At a future date, glok may have an additional backend
 * (e.g. for use within a web browser), where `joml` isn't used.
 */
class Vector2(val x: Float, val y: Float) {

    constructor() : this(0f, 0f)
    constructor(x: Double, y: Double) : this(x.toFloat(), y.toFloat())

    operator fun unaryMinus() = Vector2(- x, - y)

    operator fun plus(other: Vector2) = Vector2(x + other.x, y + other.y)
    operator fun minus(other: Vector2) = Vector2(x - other.x, y - other.y)

    fun plus(dx: Float, dy: Float) = Vector2(x + dx, y + dy)
    fun minus(dx: Float, dy: Float) = Vector2(x - dx, y - dy)

    operator fun times(scale: Float) = Vector2(x * scale, y * scale)
    operator fun times(other: Vector2) = Vector2(x * other.x, y * other.y)

    operator fun div(scale: Float) = Vector2(x / scale, y / scale)
    operator fun div(other: Vector2) = Vector2(x / other.x, y / other.y)

    operator fun rem(other: Float) = Vector2(x % other, y % other)
    operator fun rem(other: Vector2) = Vector2(x % other.x, y % other.y)

    fun translate(dx: Float, dy: Float) = Vector2(x + dx, y + dy)

    fun rotateRadians(radians: Double): Vector2 {
        val sin = sin(radians)
        val cos = cos(radians)
        return Vector2(
            (cos * x - sin * y).toFloat(),
            (sin * x + cos * y).toFloat()
        )
    }


    fun unit() = this / magnitude()

    fun magnitude() = sqrt(x * x + y * y)
    fun magnitudeSquared() = x * x + y * y

    /**
     * @return x + y
     */
    fun hamiltonian() = x + y

    fun distanceTo(to: Vector2) = sqrt(distanceSquaredTo(to))

    fun distanceSquaredTo(to: Vector2): Float {
        val dx = x - to.x
        val dy = y - to.y
        return dx * dx + dy * dy
    }

    fun dot(other: Vector2) = x * other.x + y * other.y
    fun cross(other: Vector2) = x * other.y - y * other.x
    fun perpendicular() = Vector2(- y, x)

    fun angleRadians(other: Vector2) = acos((this.dot(other) / this.magnitude() / other.magnitude()).toDouble())

    override fun equals(other: Any?): Boolean {
        return other is Vector2 && other.x == x && other.y == y
    }

    override fun toString() = "[$x , $y]"
}

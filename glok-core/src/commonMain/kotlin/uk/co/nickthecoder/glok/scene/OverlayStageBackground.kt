/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.property.boilerplate.ColorProperty

/**
 * Very similar to [OverlayStageBorder].
 * Everything within the solid rectangle is filled.
 *
 *      (x,y)
 *        ┌┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐   ▲  ▲
 *        ┊   ╭━━━━━━━━━━━━━╮ ┊   ┊  ┆ size.top  ▲ thickness
 *        ┊   ┃ ╭┄┄┄┄┄┄┄┄┄╮ ┃ ┊   ┊  ▼           ▼
 *        ┊   ┃ ┊         ┊ ┃ ┊   ┊
 *        ┊   ┃ ┊         ┊ ┃ ┊   ┊height
 *        ┊   ┃ ┊         ┊ ┃ ┊   ┊
 *        ┊   ┃ ╰┄┄┄┄┄┄┄┄┄╯ ┃ ┊   ┊  ▲
 *        ┊   ╰━━━━━━━━━━━━━╯ ┊   ┊  │ size.bottom
 *        └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┘   ▼  ▼
 *
 *        ◀┄┄┄┄┄┄┄┄width┄┄┄┄┄┄▶
 *        ◀┄┄┄┄┄▶         ◀┄┄╌▶
 *        size.left         size.right
 *
 * The widths of the transparent margin are given by : `size.top - thickness`, `size.right - thickness` etc.
 *
 * Used by `WindowDecoration` of an `OverlayStage`, which also uses [OverlayStageBorder] with exactly the
 * same geometry. The net results is a node with transparent pixels around the edge, then a thin line,
 * then the content of the node (depicted as the inner dashed rectangle) filled with [color].
 *
 */
class OverlayStageBackground(radius: Float, val thickness: Float) : Background {

    private val actualBackground = RoundedBackground(radius, null)

    override fun draw(x: Float, y: Float, width: Float, height: Float, color: Color, size: Edges) {
        actualBackground.draw(
            x + size.left - thickness,
            y + size.top - thickness,
            width - size.left - size.right + thickness * 2,
            height - size.top - size.bottom + thickness * 2,
            color,
            size // (size is ignored by actualBackground)
        )
    }

    override fun toString() = "OverlayStageBackground( ${actualBackground.radius}, $thickness )"

}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.internal

import uk.co.nickthecoder.glok.scene.Scene
import uk.co.nickthecoder.glok.text.FontIdentifier
import uk.co.nickthecoder.glok.text.createBitmapFont

/**
 * This must only be used by Glok's unit tests.
 *
 * There's a problem using `internal` in a multi-platform project - it seems there's no way to
 * use these internal classes/methods from a unit test.
 *
 * As a workaround, the shim functions here are `public`, and call the real `internal` functions
 * (i.e. they break the protection of the `internal` keyword).
 *
 * Note, these methods are deliberately cumbersome to use, so that they aren't accidentally used by
 * application developers.
 */
object DangerousUnitTestShims {

    fun layout(scene: Scene) {
        scene.layout()
    }

    fun restyle(scene: Scene) {
        scene.restyle()
    }

    fun createBitmapFont(
        identifier: FontIdentifier,
        paddingX: Int,
        paddingY: Int,
        maxTextureWidth: Int
    ) = createBitmapFont(identifier) {
        padding(paddingX, paddingY)
        maxTextureWidth(maxTextureWidth)
    }


}

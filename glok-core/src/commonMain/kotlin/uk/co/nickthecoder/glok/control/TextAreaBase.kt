/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.control

import uk.co.nickthecoder.glok.action.Action
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.ListChange
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.collections.sizeProperty
import uk.co.nickthecoder.glok.event.KeyTypedEvent
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.history.Change
import uk.co.nickthecoder.glok.history.DocumentListener
import uk.co.nickthecoder.glok.history.History
import uk.co.nickthecoder.glok.history.HistoryDocument
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.*
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.hBox
import uk.co.nickthecoder.glok.text.*
import uk.co.nickthecoder.glok.theme.styles.CONTAINER
import uk.co.nickthecoder.glok.theme.styles.LINE_NUMBER_GUTTER
import uk.co.nickthecoder.glok.theme.styles.SCROLLED
import uk.co.nickthecoder.glok.util.Clipboard
import uk.co.nickthecoder.glok.util.clamp
import uk.co.nickthecoder.glok.util.log
import kotlin.math.max
import kotlin.math.min

/**
 * The base class for [TextArea] and [StyledTextArea].
 *
 * The only difference between the two implementations :
 *
 * 1. The type of the [document] ([TextDocument] vs [StyledTextDocument]).
 * 2. How the text is rendered.
 * 3. [StyledTextArea] has additional methods to add/remove [HighlightRange]s.
 *
 * Therefore, [TextArea] and [StyledTextArea] classes are quite small; most of the code is
 * in this base class.
 */
abstract class TextAreaBase<D : TextDocument>(val document: D) : TextInputControl() {

    // region ==== Properties ====

    /**
     * TextArea does NOT store the document as a String, it is stored as a list of strings
     * where each item in the list does NOT contain a new line character.
     *
     * Therefore, getting/setting [text] is O(n) where `n` is the number of lines.
     *
     * IMHO, the trade-off, with _everything else_ being _much_ faster is well worth it!
     * e.g. JavaFX's TextArea is painfully slow inserting a single character (for a moderate length document),
     * because it needs to recreate a String containing the entire document.
     *
     * If you bind this property, you should also set [readOnly] to true, as Themes may style a read-only
     * TextArea differently. If this is bound, then the TextArea cannot be altered by the user
     * regardless of [readOnly].
     */
    val textProperty: StringProperty by lazy { document.textProperty }
    var text: String
        get() = document.text
        set(v) {
            document.setText(v)
            document.history.clear()
            caretPosition = TextPosition(0, 0)
            anchorPosition = caretPosition
        }

    private val linesListener = document.lines.addWeakChangeListener { _, changes -> updateLineWidths(changes) }

    /**
     * See [History.undoableProperty].
     */
    val undoableProperty = document.history.undoableProperty
    val undoable by undoableProperty

    /**
     * See [History.redoableProperty].
     */
    val redoableProperty = document.history.redoableProperty
    val redoable by redoableProperty

    /**
     * The caret is the vertical flashing bar between two characters, where text can be typed.
     * When text is selected, [caretPosition] can be either end of the selection.
     * See [selectionStartProperty] and [selectionEndProperty].
     *
     * The caret is invisible when there is a selection.
     */
    val caretPositionProperty by validatedTextPositionProperty(TextPosition(0, 0)) { _, _, newValue ->
        document.validPosition(newValue)
    }
    var caretPosition by caretPositionProperty

    /**
     * Along with [caretPosition], this defines the extent of the selection.
     * If [anchorPosition] == [caretPosition], then nothing is selected.
     *
     * Note that [anchorPosition] can be greater than, equal to or less than [caretPosition].
     * See [selectionStartProperty] and [selectionEndProperty].
     */
    val anchorPositionProperty by validatedTextPositionProperty(TextPosition(0, 0)) { _, _, newValue ->
        document.validPosition(newValue)
    }
    var anchorPosition by anchorPositionProperty

    /**
     * The minimum of [caretPosition] and [anchorPosition]
     */
    val selectionStartProperty = min(caretPositionProperty, anchorPositionProperty)
    val selectionStart by selectionStartProperty

    /**
     * The maximum of [caretPosition] and [anchorPosition]
     */
    val selectionEndProperty = max(caretPositionProperty, anchorPositionProperty)
    val selectionEnd by selectionEndProperty

    /**
     * True iff [caretPosition] != [anchorPosition]
     */
    final override val textSelectedProperty: ObservableBoolean =
        caretPositionProperty.notEqualTo(anchorPositionProperty)
    final override val textSelected by textSelectedProperty
    private val nothingSelectedProperty = ! textSelectedProperty

    /**
     * Used to calculate the preferred width of this Node.
     */
    val prefColumnCountProperty by stylableIntProperty(20)
    var prefColumnCount by prefColumnCountProperty

    /**
     * Used to calculate the preferred height of this Node.
     */
    val prefRowCountProperty by stylableIntProperty(5)
    var prefRowCount by prefRowCountProperty

    /**
     * The distance from the top of one line of text to the top of the next line,
     * as a proportion of the font's height. i.e. 1 for the 'normal' distance.
     * However, 1 looks a little cramped, and therefore 1.2 is often the 'standard' value.
     */
    val lineHeightProperty by stylableFloatProperty(1f)
    var lineHeight by lineHeightProperty

    val showLineNumbersProperty by stylableBooleanProperty(false)
    var showLineNumbers by showLineNumbersProperty

    // endregion properties

    /**
     * Displays line numbers. This is invisible by default. See [showLineNumbers].
     * Future versions of Glok may introduce richer gutters, with the ability to include custom gutters.
     * e.g. show which lines have been changed based on git.
     */
    protected val leftGutter = LineNumbersGutter()

    protected abstract val container: Container

    // region ==== Fields ====

    protected val scrolled = hBox {
        fillHeight = true
        styles.add(SCROLLED)
        + leftGutter
    }

    protected val scrollPane = ScrollPane(scrolled)

    final override val children = listOf<Node>(scrollPane).asObservableList()

    /**
     * Cached widths of each line in logical pixels.
     * Used to set the width of [scrollPane]'s content.
     * (the height is based on the number of lines in the document, the [font]'s height and [lineHeight]).
     */
    private val lineWidths = mutableListOf<Float>()

    /**
     * A cached calculation of : `max( lineWidths )`.
     */
    private var maxLineWidth = - 1f
        get() {
            // Set to a -ve number to indicate this is invalid, and must be recalculated
            return if (field < 0) {
                val v = lineWidths.maxOrNull() ?: 0f
                field = v
                v
            } else {
                field
            }
        }

    // endregion fields

    // region ==== Commands ====

    /**
     * The behaviour for each [Action] in [TextAreaActions].
     */
    val commands = Commands().apply {
        with(TextAreaActions) {

            SELECT_ALL {
                caretPosition = TextPosition(0, 0)
                anchorPosition = TextPosition(document.lines.size - 1, document.lines.last().length)
            }

            SELECT_NONE { anchorPosition = caretPosition }.disable(nothingSelectedProperty)

            LEFT {
                caretLeft(false)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_LEFT {
                caretLeft(true)
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SKIP_LEFT {
                skipWordBackwards()
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_SKIP_LEFT {
                skipWordBackwards()
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }

            RIGHT {
                caretRight(false)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_RIGHT {
                caretRight(true)
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SKIP_RIGHT {
                skipWordForwards()
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_SKIP_RIGHT {
                skipWordForwards()
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }

            UP {
                caretUpDown(- 1, false)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
            }
            SELECT_UP {
                caretUpDown(- 1, true)
                scrollToCaret()
                resetBlink()
            }

            DOWN {
                caretUpDown(1, false)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
            }
            SELECT_DOWN {
                caretUpDown(1, true)
                scrollToCaret()
                resetBlink()
            }

            PAGE_UP {
                caretUpDown(- (scrollPane.visibleContentBounds().height / font.height / lineHeight).toInt() + 1, false)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
            }
            SELECT_PAGE_UP {
                caretUpDown(- (scrollPane.visibleContentBounds().height / font.height / lineHeight).toInt() + 1, true)
                scrollToCaret()
                resetBlink()
            }

            PAGE_DOWN {
                caretUpDown((scrollPane.visibleContentBounds().height / font.height / lineHeight).toInt() - 1, false)
                scrollToCaret()
                anchorPosition = caretPosition
                resetBlink()
            }
            SELECT_PAGE_DOWN {
                caretUpDown((scrollPane.visibleContentBounds().height / font.height / lineHeight).toInt() - 1, true)
                scrollToCaret()
                resetBlink()
            }

            START_OF_LINE {
                caretPosition = TextPosition(caretPosition.row, 0)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_START_OF_LINE {
                caretPosition = TextPosition(caretPosition.row, 0)
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }

            START_OF_DOCUMENT {
                caretPosition = TextPosition(0, 0)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_START_OF_DOCUMENT {
                caretPosition = TextPosition(0, 0)
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }

            END_OF_LINE {
                caretPosition = TextPosition(caretPosition.row, document.lines[caretPosition.row].length)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_END_OF_LINE {
                caretPosition = TextPosition(caretPosition.row, document.lines[caretPosition.row].length)
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }

            END_OF_DOCUMENT {
                caretPosition = TextPosition(document.lines.size - 1, document.lines.last().length)
                anchorPosition = caretPosition
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }
            SELECT_END_OF_DOCUMENT {
                caretPosition = TextPosition(document.lines.size - 1, document.lines.last().length)
                scrollToCaret()
                resetBlink()
                caretUpDownX = - 1f
            }

            COPY { Clipboard.textContents = selection() }.disable(nothingSelectedProperty)

            // commands which alter the document...

            UNDO { undo() }.disable(! undoableProperty)

            REDO { redo() }.disable(! redoableProperty)

            CUT {
                if (canChange() && textSelected) {
                    Clipboard.textContents = selection()
                    document.delete(selectionStart, selectionEnd)
                    scrollToCaret()
                    resetBlink()
                    caretUpDownX = - 1f
                }
            }.disable(nothingSelectedProperty)

            PASTE {
                if (canChange()) {
                    Clipboard.textContents?.let { text ->
                        val from = selectionStart
                        val toPaste = text.split("\n")
                        if (textSelected) {
                            document.replace(selectionStart, selectionEnd, toPaste)
                        } else {
                            document.insert(caretPosition, toPaste)
                        }

                        caretPosition = positionAfter(from, toPaste)
                        anchorPosition = caretPosition
                        scrollToCaret()
                        resetBlink()
                        caretUpDownX = - 1f
                    }
                }
            }.disable(Clipboard.noTextProperty)

            DUPLICATE {
                if (textSelected) {
                    val start = selectionStart
                    val toPaste = document.subset(start, selectionEnd)
                    document.insert(selectionEnd, toPaste)

                    caretPosition = positionAfter(start, toPaste)
                    anchorPosition = start
                    scrollToCaret()
                    resetBlink()
                    caretUpDownX = - 1f
                }
            }.disable(nothingSelectedProperty)

            ENTER {
                if (canChange()) {
                    if (textSelected) {
                        document.replace(selectionStart, selectionEnd, "\n")
                    } else {
                        document.insert(selectionStart, "\n")
                    }
                    caretPosition = TextPosition(caretPosition.row + 1, 0)
                    anchorPosition = caretPosition
                    scrollToCaret()
                    resetBlink()
                    caretUpDownX = - 1f
                }
            }

            INDENT {
                if (canChange()) {
                    indent()
                    scrollToCaret()
                    resetBlink()
                    caretUpDownX = - 1f
                }
            }.disable(readOnlyProperty) // Allows Tab to move to the next node when read-only
            UNINDENT {
                if (canChange()) {
                    unindent()
                    scrollToCaret()
                    resetBlink()
                    caretUpDownX = - 1f
                }
            }.disable(readOnlyProperty) // Allows Shift+TAB to move to the previous node when read-only.

            TAB {
                if (canChange()) {
                    if (caretPosition.row == anchorPosition.row) {
                        if (textSelected) {
                            document.replace(selectionStart, selectionEnd, "\t")
                        } else {
                            document.insert(selectionStart, "\t")
                        }
                    } else {
                        indent()
                    }
                    scrollToCaret()
                    resetBlink()
                    caretUpDownX = - 1f
                }
            }

            DELETE { if (canChange()) delete() }
            BACKSPACE { if (canChange()) backspace() }

        }
        attachTo(this@TextAreaBase)
    }

    // endregion

    /**
     * Update [caretPosition] and [anchorPosition] if they are after, or within a part of the
     * document affected by the Change.
     */
    private val documentListener = document.addListener(object : DocumentListener {
        override fun documentChanged(document: HistoryDocument, change: Change, isUndo: Boolean) {
            if (change is TextChange) updatePositions(change, isUndo)
            requestRedraw()
        }
    })

    // region ==== init ====
    init {

        fontProperty.addListener { recalculateLineWidths() }
        caretPositionProperty.addListener { checkPosition(caretPositionProperty) }
        anchorPositionProperty.addListener { checkPosition(anchorPositionProperty) }

        indentationProperty.bindTo(GlokSettings.indentationProperty)
        claimChildren()

        for (prop in listOf(
            textColorProperty, highlightColorProperty, caretImageProperty,
            caretPositionProperty, anchorPositionProperty, lineHeightProperty
        )) {
            prop.addListener(requestRedrawListener)
        }
        for (prop in listOf(prefColumnCountProperty, prefRowCountProperty, showLineNumbersProperty)) {
            prop.addListener(requestLayoutListener)
        }

        // NOTE. [commands] adds onKeyPressed for keyboard actions.
        onKeyTyped { keyTyped(it) }
        onPopupTrigger { showContextMenu(it, commands, canChange()) }

        scrollPane.commands.attachTo(this)
        recalculateLineWidths()
    }

    // endregion init


    // region ==== Public Methods ====

    fun endPosition() = document.endPosition()

    /**
     * Sets the [caretPosition] and [anchorPosition].
     */
    fun moveCaretTo(position: TextPosition) {
        caretPosition = position
        anchorPosition = position
    }

    /**
     * Replaces the text between [anchorPosition] and [caretPosition] with [replacement] text.
     * The caret is advanced to the end of the replaced text.
     */
    fun replaceSelection(replacement: String) {
        caretPosition = document.replace(selectionStart, selectionEnd, replacement)
        anchorPosition = caretPosition
    }

    fun undo() {
        if (canChange()) {
            val change = document.history.undo(false)
            (change as? TextChange)?.let {
                caretPosition = it.caretPosition(true)
                anchorPosition = caretPosition
            }
            scrollToCaret()
            resetBlink()
            caretUpDownX = - 1f
        }
    }

    fun redo() {
        if (canChange()) {
            val change = document.history.redo(false)
            (change as? TextChange)?.let {
                caretPosition = it.caretPosition(false)
                anchorPosition = caretPosition
            }
            scrollToCaret()
            resetBlink()
            caretUpDownX = - 1f
        }
    }

    fun moveCaretAndAnchor(position: TextPosition?, scrollTo: Boolean = true, focus: Boolean = true) {
        if (position != null) {
            caretPosition = position
            anchorPosition = position
            if (scrollTo) {
                scrollToCaret()
            }
            if (focus) {
                requestFocus()
            }
        }
    }

    fun selection() = document.subset(selectionStart, selectionEnd).joinToString(separator = "\n")

    fun delete(from: TextPosition, to: TextPosition) {
        document.delete(from, to)
    }

    fun insert(at: TextPosition, text: String) {
        document.insert(at, text)
    }

    fun replace(from: TextPosition, to: TextPosition, text: String) {
        document.replace(from, to, text)
    }

    fun clear() {
        document.clear()
    }

    fun scrollToCaret() {
        // We often want to move the scroll after moving the caret.
        // However, to do this correctly, we need the content to be re-laid out.
        // Therefore, using runLater to allow layout() to occur.
        Platform.runLater {
            val bounds = scrollPane.visibleContentBounds()
            val y = container.caretY()
            val above = bounds.top - y
            if (above > 0) {
                scrollPane.vScrollValue -= above
            } else {
                val below = y - bounds.bottom + (font.height * lineHeight * 2)
                if (below > 0) {
                    scrollPane.vScrollValue += below
                }
            }

            val x = container.caretX()
            val left = bounds.left - x + container.surroundLeft()
            if (left > 0) {
                scrollPane.hScrollValue -= left
            } else {
                val right = x - bounds.right + font.maxWidthOfDigit
                if (right > 0) {
                    scrollPane.hScrollValue += right
                }
            }
        }
    }

    // endregion public methods

    // region ==== Private Methods ====

    private fun positionAfter(start: TextPosition, insertion: List<String>): TextPosition {
        if (insertion.isEmpty()) throw IllegalStateException()
        return if (insertion.size == 1) {
            TextPosition(start.row, start.column + insertion.first().length)
        } else {
            TextPosition(start.row + insertion.size - 1, insertion.last().length)
        }
    }

    private fun canChange() = ! (readOnly || textProperty.isBound())

    /**
     * When the document changes, ensure that [caretPosition] and [anchorPosition] are adjusted accordingly.
     */
    private fun updatePositions(change: TextChange, isUndo: Boolean) {

        fun adjustDelete(pos: TextPosition, from: TextPosition, to: TextPosition): TextPosition =
            if (pos <= from) {
                // Before the deletion. No change
                pos
            } else if (pos < to) {
                // Inside the deletion.
                from
            } else {
                // Beyond the deletion.
                if (pos.row == to.row) {
                    TextPosition(pos.row - (to.row - from.row), pos.column - (to.column - from.column))
                } else {
                    TextPosition(pos.row - (to.row - from.row), pos.column)
                }
            }


        fun adjustInsert(pos: TextPosition, from: TextPosition, inserted: List<String>): TextPosition =
            if (pos <= from) {
                // Before the insertion. No change
                pos
            } else {
                if (pos.row == from.row) {
                    // Same row. Column and row affected.
                    TextPosition(pos.row + inserted.size - 1, pos.column + inserted.last().length)
                } else {
                    // Only the row is changed.
                    TextPosition(pos.row + inserted.size - 1, pos.column)
                }
            }

        when (change) {
            is DeleteText -> {
                if (isUndo) {
                    caretPosition = adjustInsert(caretPosition, change.from, change.textList)
                    anchorPosition = adjustInsert(anchorPosition, change.from, change.textList)
                } else {
                    caretPosition = adjustDelete(caretPosition, change.from, change.to)
                    anchorPosition = adjustDelete(anchorPosition, change.from, change.to)
                }
            }

            is InsertText -> {
                if (isUndo) {
                    caretPosition = adjustDelete(caretPosition, change.from, change.to)
                    anchorPosition = adjustDelete(anchorPosition, change.from, change.to)
                } else {
                    caretPosition = adjustInsert(caretPosition, change.from, change.textList)
                    anchorPosition = adjustInsert(anchorPosition, change.from, change.textList)
                }
            }

            else -> {}
        }
    }

    /**
     * Calculates the width of a single line of text.
     */
    private fun lineWidth(line: String) = font.widthOfOrZero(line, indentation.columns)

    private fun recalculateLineWidths() {
        maxLineWidth = - 1f
        lineWidths.clear()
        for (line in document.lines) {
            val width = lineWidth(line)
            if (width > maxLineWidth) {
                maxLineWidth = width
            }
            lineWidths.add(width)
        }
    }

    /**
     * In order for the horizontal scroll bar to be correct, we need to know the lengths of the lines.
     */
    private fun updateLineWidths(change: ListChange<String>) {
        val oldMax = maxLineWidth

        if (change.isReplacement()) {
            for (i in change.added.indices) {
                val index = change.from + i
                val width = lineWidth(document.lines[index])
                if (width > maxLineWidth) {
                    maxLineWidth = width
                } else {
                    if (lineWidths[index] == maxLineWidth) {
                        maxLineWidth = - 1f
                    }
                }
                lineWidths[index] = width
            }
        } else {
            for (i in change.added.indices) {
                val index = change.from + i
                val width = lineWidth(document.lines[index])
                lineWidths.add(index, width)
                if (width > maxLineWidth) maxLineWidth = width
            }
            if (change.isRemoval()) {
                for (i in change.removed.indices) {
                    val old = lineWidths.removeAt(change.from)
                    if (old == maxLineWidth) {
                        maxLineWidth = - 1f
                    }
                }
            }
        }
        if (oldMax != maxLineWidth || maxLineWidth == - 1f) {
            container.requestLayout()
        } else {
            requestRedraw()
        }
    }

    /**
     * Sanity check for [caretPosition] and [anchorPosition].
     * Both properties have listeners which call this, so that setting them to invalid values is reported.
     */
    private fun checkPosition(prop: TextPositionProperty) {
        val pos = prop.value
        if (pos.row < 0 || pos.row >= document.lines.size) {
            log.warn("Invalid ${prop.beanName}.row $pos")
            return
        }

        val line = document.lines[pos.row]
        if (line.length < pos.column) {
            log.warn("Invalid ${prop.beanName}.column $pos")
        }
    }

    private fun backspace() {
        if (textSelected) {
            val from = selectionStart
            val to = selectionEnd
            caretPosition = from
            anchorPosition = from
            document.delete(from, to)
        } else {
            val row = caretPosition.row
            val column = caretPosition.column

            val to = caretPosition
            val from = if (column == 0) {
                if (row > 0) {
                    TextPosition(row - 1, document.lines[row - 1].length)
                } else {
                    caretPosition
                }
            } else {
                var newColumn = column - 1
                if (indentation.behaveLikeTabs && document.lines[row].substring(0, newColumn).isBlank()) {
                    newColumn = (newColumn / indentation.columns) * indentation.columns
                }
                TextPosition(row, newColumn)
            }
            caretPosition = from
            anchorPosition = from
            document.delete(from, to)
        }
        scrollToCaret()
        resetBlink()
        caretUpDownX = - 1f

    }

    private fun delete() {
        if (textSelected) {
            val from = selectionStart
            val to = selectionEnd
            caretPosition = from
            anchorPosition = from
            document.delete(from, to)
        } else {
            val row = caretPosition.row
            val column = caretPosition.column
            val line = document.lines[row]

            if (column < line.length) {
                var newColumn = caretPosition.column + 1
                if (indentation.behaveLikeTabs && line.substring(0, newColumn).isBlank()) {
                    val rem = newColumn % indentation.columns
                    if (rem != 0) {
                        for (i in newColumn until min(line.length, column + indentation.columns)) {
                            if (line[i].isWhitespace()) {
                                newColumn ++
                            } else {
                                break
                            }
                        }
                    }
                }
                document.delete(
                    caretPosition,
                    TextPosition(caretPosition.row, newColumn),
                    true
                )
            } else {
                // Remove the new line after the caret
                document.delete(caretPosition, TextPosition(caretPosition.row + 1, 0))
            }
        }
        scrollToCaret()
        resetBlink()
        caretUpDownX = - 1f
    }

    private fun indent() {
        document.history.batch("indent") {
            var tmpCaretPosition = caretPosition
            var tmpAnchorPosition = anchorPosition
            if (textSelected) {

                for (row in selectionStart.row..selectionEnd.row) {
                    val oldLine = document.lines[row]
                    val toAdd = indentation.indent(oldLine)
                    val from = TextPosition(row, 0)

                    + document.insertChange(from, toAdd, false)

                    if (row == tmpCaretPosition.row) {
                        tmpCaretPosition = TextPosition(tmpCaretPosition.row, tmpCaretPosition.column + toAdd.length)
                    }
                    if (row == tmpAnchorPosition.row) {
                        tmpAnchorPosition = TextPosition(tmpAnchorPosition.row, tmpAnchorPosition.column + toAdd.length)
                    }
                }
                caretPosition = tmpCaretPosition
                anchorPosition = tmpAnchorPosition
            } else {
                val row = tmpCaretPosition.row
                val oldLine = document.lines[row]
                val toAdd = indentation.indent(oldLine)
                val from = TextPosition(row, 0)

                + document.insertChange(from, toAdd, false)

                caretPosition = TextPosition(tmpCaretPosition.row, tmpCaretPosition.column + toAdd.length)
                anchorPosition = caretPosition
            }
        }
    }

    private fun unindent() {
        document.history.batch("unindent") {
            var tmpCaretPosition = caretPosition
            var tmpAnchorPosition = anchorPosition
            if (textSelected) {
                for (row in selectionStart.row..selectionEnd.row) {
                    val oldLine = document.lines[row]
                    val toRemove = indentation.unindent(oldLine)
                    if (toRemove > 0) {
                        val from = TextPosition(row, 0)
                        val to = TextPosition(row, toRemove)

                        + document.deleteChange(from, to, false)
                    }

                    if (row == tmpCaretPosition.row) {
                        tmpCaretPosition = TextPosition(tmpCaretPosition.row, tmpCaretPosition.column - toRemove)
                    }
                    if (row == tmpAnchorPosition.row) {
                        tmpAnchorPosition = TextPosition(tmpAnchorPosition.row, tmpAnchorPosition.column - toRemove)
                    }
                }
                caretPosition = tmpCaretPosition
                anchorPosition = tmpAnchorPosition
            } else {
                val row = tmpCaretPosition.row
                val oldLine = document.lines[row]
                val toRemove = indentation.unindent(oldLine)
                if (toRemove > 0) {
                    val from = TextPosition(row, 0)
                    val to = TextPosition(row, toRemove)

                    + document.deleteChange(from, to, false)

                    caretPosition = TextPosition(tmpCaretPosition.row, max(0, tmpCaretPosition.column - toRemove))
                    anchorPosition = caretPosition
                }
            }
        }
    }

    // endregion

    // region ==== Events ====

    fun positionForMouseEvent(event: MouseEvent): TextPosition {
        if (document.lines.isEmpty()) return TextPosition(0, 0)
        val fullLineHeight = font.height * lineHeight
        val row = ((event.sceneY - container.sceneY - container.surroundTop()) / fullLineHeight).toInt()
        if (row < 0) return TextPosition(0, 0)
        if (row >= document.lines.size) return TextPosition(document.lines.size - 1, document.lines.last().length)

        val line = document.lines[row]
        var column =
            font.caretPosition(line, event.sceneX - container.sceneX - container.surroundLeft(), indentation.columns)
        if (indentation.behaveLikeTabs && line.substring(0, column).isBlank()) {
            val rem = column % indentation.columns
            if (rem != 0) {
                if (rem > indentation.columns / 2) {
                    // move to the right of the indentation
                    for (i in column until min(line.length, column + indentation.columns - rem)) {
                        if (line[i].isWhitespace()) {
                            column ++
                        } else {
                            break
                        }
                    }
                } else {
                    // move to the left of the indentation
                    column -= rem
                }
            }
        }
        return TextPosition(row, column)
    }

    private fun mousePressed(event: MouseEvent) {
        if (event.isPrimary) {
            caretPosition = positionForMouseEvent(event)
            if (! event.isShiftDown) {
                // Holding shift down alters the selection (by keeping the anchorPosition unchanged).
                // Without shift down, the selection is cleared (by setting anchor to caret).
                anchorPosition = caretPosition
            }
            caretUpDownX = - 1f
            event.capture()
        }
        firstToRoot { it.focusAcceptable }?.requestFocus()
    }

    private fun mouseClicked(event: MouseEvent) {
        if (event.clickCount == 2 && event.isPrimary) {
            selectWord()
        } else if (event.clickCount == 3) {
            val row = caretPosition.row
            // Select whole line
            caretPosition = TextPosition(row, 0)
            anchorPosition = TextPosition(row, document.lines[row].length)
        }
    }

    private fun mouseDragged(event: MouseEvent) {
        if (event.isPrimary) {
            caretPosition = positionForMouseEvent(event)
        }
    }

    private fun keyTyped(event: KeyTypedEvent) {
        if (canChange()) {
            val toInsert = listOf(event.char.toString())
            val from = selectionStart
            val to = selectionEnd
            // Ensure that caret/anchor position are valid while the change is in progress
            caretPosition = from
            anchorPosition = from

            if (from != to) {
                document.history.batch("Selection Replaced") {
                    + document.deleteChange(from, to, false)
                    + document.insertChange(from, toInsert, false)
                }
            } else {
                document.insert(caretPosition, toInsert, true)
            }
            caretPosition = TextPosition(from.row, from.column + 1)
            anchorPosition = caretPosition
            resetBlink()
            caretUpDownX = - 1f
        }
    }

    // endregion events

    // region ==== Layout ====

    override fun nodeMinWidth() = font.widthOfOrZero("W", 0) +
        surroundX() + scrollPane.surroundX() + container.surroundX()

    override fun nodeMinHeight() = nodePrefHeight()

    override fun nodePrefWidth() = font.widthOfOrZero("W", 0) * prefColumnCount +
        surroundX() + scrollPane.surroundX() + container.surroundX()

    override fun nodePrefHeight() = font.height * lineHeight * prefRowCount +
        surroundY() + scrollPane.surroundY() + container.surroundY()

    override fun layoutChildren() {
        setChildBounds(scrollPane, surroundLeft(), surroundTop(), width - surroundX(), height - surroundY())
    }

    // endregion layout

    // region ==== Caret movement ====
    private fun caretLeft(extendSelection: Boolean) {
        if (caretPosition == anchorPosition || extendSelection) {

            val row = caretPosition.row
            val column = caretPosition.column

            if (column > 0) {
                // Not at the start of the line.
                var newColumn = column - 1
                if (indentation.behaveLikeTabs && document.lines[row].substring(0, newColumn).isBlank()) {
                    newColumn = (newColumn / indentation.columns) * indentation.columns
                }
                caretPosition = TextPosition(row, newColumn)
            } else {
                if (row > 0) {
                    caretPosition = TextPosition(row - 1, document.lines[row - 1].length)
                }
            }
        } else {
            caretPosition = selectionStart
        }
    }

    private fun caretRight(extendSelection: Boolean) {
        if (caretPosition == anchorPosition || extendSelection) {
            val row = caretPosition.row
            val column = caretPosition.column
            val line = document.lines[row]

            caretPosition = if (column < line.length) {
                // Not at the end of the line
                var newColumn = column + 1
                if (indentation.behaveLikeTabs && line.substring(0, newColumn).isBlank()) {
                    val rem = newColumn % indentation.columns
                    if (rem != 0) {
                        for (i in newColumn until min(line.length, column + indentation.columns)) {
                            if (line[i].isWhitespace()) {
                                newColumn ++
                            } else {
                                break
                            }
                        }
                    }
                }
                TextPosition(row, newColumn)
            } else {
                // At the end of the current line
                if (row < document.lines.size - 1) {
                    TextPosition(row + 1, 0)
                } else {
                    // No change, we are at the end of the document.
                    caretPosition
                }
            }
        } else {
            caretPosition = selectionEnd
        }
    }

    /**
     * The x position of the caret in pixels when the user _started_ moving the caret up/down.
     * Reset to -1 when the caret moves left/right.
     * The net result, as the user moves up/down, the caret remains roughly in the save X position,
     * even as it passes through a line shorter than [caretUpDownX].
     */
    private var caretUpDownX = - 1f

    private fun caretUpDown(delta: Int, extendSelection: Boolean) {
        if (caretPosition == anchorPosition || extendSelection) {
            val row = caretPosition.row
            val newRow = (row + delta).clamp(0, document.lines.size - 1)

            caretPosition = if (row == 0 && newRow == 0) {
                // The start of the document
                TextPosition(0, 0)
            } else if (row == document.lines.size - 1 && newRow == row) {
                // The end of the document
                TextPosition(row, document.lines[row].length)
            } else {
                if (caretUpDownX < 0f) {
                    // No saved column position, so save it now.
                    caretUpDownX = widthOf(document.lines[row].substring(0, caretPosition.column))
                }
                val line = document.lines[newRow]
                var newColumn = font.caretPosition(line, caretUpDownX, indentation.columns)
                // Smart caret?
                if (indentation.behaveLikeTabs && line.substring(0, newColumn).isBlank()) {
                    // We need to ensure the caret doesn't end up min-indentation
                    val rem = newColumn % indentation.columns
                    if (rem != 0) {
                        if (rem > indentation.columns / 2) {
                            // move to the right of the indentation
                            for (i in newColumn until min(line.length, newColumn + indentation.columns - rem)) {
                                if (line[i].isWhitespace()) {
                                    newColumn ++
                                } else {
                                    break
                                }
                            }
                        } else {
                            // move to the left of the indentation
                            newColumn -= rem
                        }
                    }
                }
                TextPosition(newRow, newColumn)
            }
        } else {
            caretPosition = if (delta > 0) selectionEnd else selectionStart
        }
    }

    private fun skipWordForwards() {
        val cp = caretPosition
        val column = cp.column
        val line = document.lines[cp.row]
        if (column < line.length) {
            caretPosition = TextPosition(cp.row, SkipWord.instance.forwards(column, line))
        } else {
            if (cp.row < document.lines.size - 1) {
                caretPosition = TextPosition(cp.row + 1, 0)
            }
        }
    }

    private fun skipWordBackwards() {
        val cp = caretPosition
        val column = cp.column
        if (column > 0) {
            val line = document.lines[cp.row]
            caretPosition = TextPosition(cp.row, SkipWord.instance.backwards(column, line))
        } else {
            if (cp.row > 0) {
                val line = document.lines[cp.row - 1]
                caretPosition = TextPosition(cp.row - 1, line.length)
            }
        }
    }

    private fun selectWord() {
        val cp = caretPosition
        val (left, right) = SkipWord.instance.selectWord(cp.column, document.lines[cp.row])
        caretPosition = TextPosition(cp.row, left)
        anchorPosition = TextPosition(cp.row, right)
    }

    // endregion caret movement

    // region == inner class Container ==
    protected abstract inner class Container : Region() {

        init {
            growPriority = 1f
            fontProperty.addListener {
                requestLayout()
                maxLineWidth = - 1f
                for (i in lineWidths.indices) {
                    val w = font.widthOfOrZero(document.lines[i], indentation.columns)
                    if (w > maxLineWidth) maxLineWidth = w
                    lineWidths[i] = w
                }
            }

            styles.add(CONTAINER)
            document.lines.addListener(requestLayoutListener)
            // Mouse events are in the container, because we only want clicks in the content area, not the scrollbars.
            onMousePressed { mousePressed(it) }
            onMouseClicked { mouseClicked(it) }
            onMouseDragged { mouseDragged(it) }
        }

        fun caretX() = surroundLeft() + widthOf(document.lines[caretPosition.row].substring(0, caretPosition.column))
        fun caretY() = surroundTop() + caretPosition.row * font.height * lineHeight

        override fun nodePrefWidth() = maxLineWidth + surroundX()

        override fun nodePrefHeight() = font.height * lineHeight * document.lines.size + surroundY()

    }
    // endregion Container

    // region == inner class LineNumberGutter ==
    protected inner class LineNumbersGutter : Region() {

        val textColorProperty by stylableOptionalColorProperty(null)
        var textColor by textColorProperty

        private val columnsProperty = (document.lines.sizeProperty() + 1).toObservableString().length()
        private val columns by columnsProperty

        init {
            styles.add(LINE_NUMBER_GUTTER)
            visibleProperty.bindTo(showLineNumbersProperty)

            textColorProperty.addListener(requestRedrawListener)
            columnsProperty.addListener(requestLayoutListener)
        }

        override fun nodePrefWidth() = surroundX() + font.maxWidthOfDigit * columns

        override fun draw() {
            super.draw()

            val textColor = textColor ?: this@TextAreaBase.textColor
            val fullLineHeight = font.height * lineHeight
            val visibleBounds = scrollPane.visibleContentBounds()
            val firstVisibleRow = max(0, (visibleBounds.top / fullLineHeight - 1).toInt())
            val lastVisibleRow = min(
                document.lines.size - 1, (visibleBounds.bottom / fullLineHeight + 1).toInt()
            )
            var y = sceneY + surroundTop() + firstVisibleRow * fullLineHeight + (lineHeight - 1) * font.height / 2
            val x = sceneX + width - surroundRight()
            for (i in firstVisibleRow..lastVisibleRow) {
                font.draw((i + 1).toString(), textColor, x, y, HAlignment.RIGHT, TextVAlignment.TOP)
                y += fullLineHeight
            }
        }
    }
    // endregion
}

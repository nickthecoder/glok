package uk.co.nickthecoder.glok.scene

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.documentation.LogicalPixels
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.event.ActionEventHandler
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.combineActionEventHandlers
import uk.co.nickthecoder.glok.property.boilerplate.*

/**
 * Stage is the `owner` of a [Scene].
 * There are two implementations :
 * [RegularStage], and [OverlayStage].
 *
 * Your application's `primary` stage is a [RegularStage].
 *
 * [OverlayStage] are for transient mini-scenes, which appear inside an existing `Window`.
 * For example, a `Tooltip`, or a `PopupMenu` use a [OverlayStage].
 *
 * Larger scenes may also use an [OverlayStage] if you wish.
 * The operating system knows nothing about [OverlayStage]s, so they won't appear in the task bar.
 * They are constrained to the bounds of the [RegularStage]. If they have `window-decoration`
 * (a title bar and edges), then these are rendered by Glok, and therefore won't match `real` windows.
 *
 * [OverlayStage]s were implemented as a necessary evil, due to limitations on what can be done with `real` windows.
 * For example, I couldn't create an OpenGL  window _without_ window decorations,
 * so popup menus couldn't use a `real` window.
 *
 * See [Scene] for a Class Diagram.
 */
sealed interface Stage {

    val sceneProperty: ReadOnlyOptionalSceneProperty
    var scene: Scene?

    val resizableProperty: BooleanProperty
    var resizable: Boolean

    val maximizedProperty: BooleanProperty
    var maximized: Boolean

    val minimizedProperty: BooleanProperty
    var minimized: Boolean

    /**
     * Does this stage have focus. i.e. will keyboard events be channeled to this Stage?
     * If another window has focus, this will be `false`.
     * Otherwise, this will be `true` for either the [RegularStage], or one of its [OverlayStage]s.
     */
    val focusedProperty: ObservableBoolean
    val focused: Boolean

    val titleProperty: StringProperty
    var title: String

    /**
     * Set when the user pressed the Window's close icon, hits `Alt+F4`, or
     * when [Stage.close] is called.
     * The [Stage] isn't closed immediately.
     * Instead, at the end of the current frame's processing,
     * [onCloseRequested] is called.
     * If [closingProperty] is still true, then the Stage is closed.
     */
    val closingProperty: ObservableBoolean
    val closing: Boolean

    /**
     * A Handler which is called immediately after [close].
     * Note that [onClosed] is NOT called immediately after [close]
     */
    val onCloseRequestedProperty: OptionalActionEventHandlerProperty
    var onCloseRequested: ActionEventHandler?

    /**
     * A Handler which is called just before the stage is destroyed.
     * Note that [onClosed] is NOT called immediately after [close], as [close] is only a _request_
     * for the stage to be closed. The stage is closed some time later
     * (after this `frame's`processing have completed).
     */
    val onClosedProperty: OptionalActionEventHandlerProperty
    var onClosed: ActionEventHandler?

    val mousePointerProperty: MousePointerProperty
    var mousePointer: MousePointer


    /**
     * Shows the stage, centered in the middle of the screen
     * (or in the middle of the [RegularStage], if this is an [OverlayStage]).
     */
    fun show()

    fun hide()

    fun setIcon(vararg images: Image?) = setIcon(null as Color?, *images)

    fun setIcon(tint: Color?, vararg images: Image?) {
        setIcon(images.toList(), tint)
    }

    /**
     * Set the window's icon. This icon on the left of the window's title-bar, and in the desktop's task bar.
     *
     * NOTE. [show] the stage before setting the icon.
     *
     * Not supported on macOS or Wayland.
     * Wayland expects the icon to be set ONLY within the application's .desktop file (macOS is similar, but
     * with different terminology).
     * IMHO, this is wrong, as they assume that a "regular" program (a simple executable file) cannot have a GUI.
     * NOTE, I'm making a distinction between a "program" and an "application", where the latter has additional meta-data,
     * such as file associations, a place in the "Start" menu, and of course, a desktop icon.
     */
    fun setIcon(images: List<Image?>, tint: Color? = null)

    /**
     * A request for the Stage to close. The stage is NOT closed immediately.
     * This merely sets [closing] = `true`
     *
     * At the end of the current frame's processing, Stages which have requested to be closed
     * call the [onCloseRequested] event handler.
     *
     * This handler has the option to set [closing] back to `false`, which prevent the [Stage] being closed.
     * This is most commonly used to show a dialog : "Document Blah is not saved..."
     *
     * Otherwise, the [onClosed] event handler is called, and the [Stage] is destroyed.
     */
    fun close()

    fun resizeToFit() {
        val scene = scene ?: return
        if (scene.requestRestyling) scene.restyle()
        if (scene.requestLayout) scene.layout()
        resize(scene.root.evalPrefWidth(), scene.root.evalPrefHeight())
    }

    /**
     * Resize the stage, [width] and [height] are in [LogicalPixels], which may differ from actual pixels.
     * See [Application.globalScale] and [RegularStage.scale].
     */
    fun resize(width: Float, height: Float)

    /**
     * A convenient way to set [onCloseRequestedProperty] using a lambda.
     * However, if an event handler is already present, then BOTH will be used.
     *
     * Note, unlike other event handlers, there is not an option
     * to _replace_ the onClosing event handler, because doing so is very
     * likely to break something. No component can know if onClosing is
     * relied upon by another component (including Glok itself).
     */
    fun onCloseRequested(handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (event : ActionEvent) -> Unit) {
        combineActionEventHandlers(onCloseRequestedProperty, handlerCombination, block)
    }

    /**
     * A convenient way to set [onClosedProperty] using a lambda.
     * However, if an event handler is already present, then BOTH will be used.
     *
     * Note, unlike other event handlers, there is not an option
     * to _replace_ the onClose event handler, because doing so is very
     * likely to break something. No component can know if onClose is
     * relied upon by another component (including Glok itself).
     */
    fun onClosed(handlerCombination: HandlerCombination = HandlerCombination.AFTER, block: (event: ActionEvent) -> Unit) {
        combineActionEventHandlers(onClosedProperty, handlerCombination, block)
    }

    /**
     * A factory method to create a new stage.
     * Stages are not created directly through their constructor, because we cannot know whether a
     * [RegularStage] or an [OverlayStage] should be created.
     *
     * @return A [RegularStage] or an [OverlayStage] when a [RegularStage] is not possible.
     *
     * Reasons a [RegularStage] is not possible :
     * * If the primary stage is full-screen (not supported yet)
     * * If the application is running in a browser (not supported yet)
     * * When the Glok setting [GlokSettings.useOverlayStages] suggests that [OverlayStage]s are preferred.
     * * A modal window is requested. (it seems that LWJGL does not support creating modal windows),
     *   so an [OverlayStage] is the only option for modal dialogs at present.
     */
    fun createStage(stageType: StageType): Stage {
        return if (GlokSettings.useOverlayStages || stageType != StageType.NORMAL) OverlayStage(this, stageType) else RegularStage()
    }

    val regularStage: RegularStage

    companion object {
        /**
         * The color which greys out a [RegularStage], when a modal [OverlayStage] is present.
         * This should partially obscure the scene, as visual feedback that interaction can only
         * occur with the modal stage, and not the stages/scenes below.
         */
        var modalGrey = Color(0.5f, 0.5f, 0.5f, 0.5f)
    }
}

fun stage(parent: Stage, stageType: StageType = StageType.NORMAL, block: Stage.() -> Unit): Stage {
    val stage = parent.createStage(stageType)
    stage.block()
    return stage
}

fun overlayStage(parent: Stage, stageType: StageType = StageType.NORMAL, block: OverlayStage.() -> Unit): OverlayStage {
    val stage = OverlayStage(parent, stageType)
    stage.block()
    return stage
}

package uk.co.nickthecoder.glok.util

class GlokException(message: String) : Exception(message)

class ConversionException : Exception()

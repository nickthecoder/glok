package uk.co.nickthecoder.glok.backend

import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Image

/**
 * A bitmap graphic.
 *
 * [Texture] extends [Image], so that the whole texture can be drawn to the screen.
 *
 * However, textures often consist of many individual pictures (such as icons), and each small picture is an [Image]
 * using [PartialTexture].
 */
interface Texture : Image {

    val name: String
    val width: Int
    val height: Int

    override val imageWidth: Float get() = width.toFloat()
    override val imageHeight: Float get() = height.toFloat()

    fun bind()

    override fun batch(tint: Color?, block: TextureBatch.() -> Unit) {
        backend.batch(this, tint, null, block)
    }

    override fun draw(x: Float, y: Float, tint: Color?) {
        if (tint == null) {
            backend.drawTexture(this, 0f, 0f, width.toFloat(), height.toFloat(), x, y)
        } else {
            backend.drawTintedTexture(this, tint, 0f, 0f, width.toFloat(), height.toFloat(), x, y)
        }
    }

    override fun drawBatched(batch: TextureBatch, x: Float, y: Float) {
        batch.draw(0f, 0f, width.toFloat(), height.toFloat(), x, y)
    }

    override fun drawTo(x: Float, y: Float, destWidth: Float, destHeight: Float, tint: Color?) {
        if (tint == null) {
            backend.drawTexture(this, 0f, 0f, width.toFloat(), height.toFloat(), x, y, destWidth, destHeight)
        } else {
            backend.drawTintedTexture(
                this, tint, 0f, 0f, width.toFloat(), height.toFloat(),
                x, y, destWidth, destHeight
            )
        }
    }

    override fun drawToBatched(batch: TextureBatch, x: Float, y: Float, destWidth: Float, destHeight: Float) {
        batch.draw(0f, 0f, width.toFloat(), height.toFloat(), x, y, destWidth, destHeight)
    }

    override fun partialImage(fromX: Float, fromY: Float, width: Float, height: Float): Image {
        return PartialTexture(this, fromX, fromY, width, height)
    }

    override fun scaledBy(scaleX: Float, scaleY: Float): Image {
        return partialImage(0f, 0f, imageWidth, imageHeight).scaledBy(scaleX, scaleY)
    }

}


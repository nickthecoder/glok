package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.MenuItem
import uk.co.nickthecoder.glok.demos.DemoMenus.Companion.ABOUT
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import java.util.*

/**
 * See [ABOUT].
 */
class DemoMenus : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "Menus Demo"

            scene(700, 400) {
                root = demoPanel {
                    name = "DemoMenus"
                    about = ABOUT

                    content = vBox {
                        fillWidth = true

                        + menuBar {
                            + menu("File") {
                                + toggleMenuItem("Light Theme") {
                                    selectedProperty.bindTo(Tantalum.darkProperty.not())
                                    onAction { Tantalum.dark = false }
                                }
                                + toggleMenuItem("Dark Theme") {
                                    selectedProperty.bindTo(Tantalum.darkProperty)
                                    onAction { Tantalum.dark = true }
                                }
                                + menuItem("Dump Scene") { onAction { primaryStage.scene?.dump(borders = true) } }
                                + menuItem("Dump PopupMenu's Scene") { onAction { scene?.dump() } }

                                // Very weird! You can put any nodes in a Menu or SubMenu.
                                + button("A Plain Button") {
                                    padding(4, 16)
                                    onAction { println("Clicked the button") }
                                }

                                // This approach is often better
                                + customMenuItem {
                                    content = label("A Custom Menu Item") {
                                        padding(4, 16)
                                    }
                                    onAction { println("Clicked the custom menu item") }
                                }
                            }

                            + menu("Edit") {
                                + menuItem("Say Hi") { onAction { println("Hi") } }
                                + subMenu("Sub-Menu") {
                                    + MenuItem("Foo")
                                    + subMenu("Nested") {
                                        + MenuItem("Hi")
                                    }
                                    + MenuItem("Bar")
                                }
                                + subMenu("Another Sub-Menu") {
                                    + MenuItem("Baz")
                                }
                            }

                            // The contents of this menu are rebuilt just before the PopupMenu is displayed.
                            // The time displayed will change each time you open the menu.
                            // `onShowing` is also used by MenuButton, SplitMenuButton and SubMenu.
                            // Do not use this feature to change the state of MenuItem properties,
                            // such as `visible`, `disabled` and `selected`
                            // Using bound properties is usually must better and easier.
                            // See "Dark Theme" above.
                            + menu("Dynamic") {
                                onShowing {
                                    items.clear()
                                    + menuItem("This time is ${Date()}") {
                                        onAction {
                                            println("Does nothing")
                                        }
                                    }
                                }
                            }

                        }
                        + toolBar {
                            + menuButton("Choose") {
                                + toggleMenuItem("Light Theme") {
                                    selectedProperty.bindTo(Tantalum.darkProperty.not())
                                    onAction { Tantalum.dark = false }
                                }
                                + toggleMenuItem("Dark Theme") {
                                    selectedProperty.bindTo(Tantalum.darkProperty)
                                    onAction { Tantalum.dark = true }
                                }
                                + menuItem("Dump Scene") { onAction { primaryStage.scene?.dump(borders = true) } }
                                + menuItem("Dump PopupMenu's Scene") { onAction { scene?.dump() } }
                            }
                            + splitMenuButton("Split") {
                                onAction { println("You pressed the SplitMenuButton") }
                                + menuItem("Say Hi") { onAction { println("Hi") } }
                                + subMenu("Sub-Menu") {
                                    + MenuItem("Foo")
                                    + MenuItem("Bar")
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            Demonstrates [MenuButton], [SplitMenuButton] and [Menu].
            
            Functionally, they are very similar, but look quite different.
            [Menu]s are designed to live within a [MenuBar],
            whereas [MenuButton]s and [SplitMenuButton]s live elsewhere,
            most commonly in [ToolBar]s.
              
            When a [Menu], a [MenuButton] or a [SplitMenuButton] is clicked, their `items` are
            displayed using the [PopupMenu] class. You may also use [PopupMenu] for context menus.
            i.e. a menu which appears at the mouse position when you right-click.
            
            You can navigate the menus using the mouse, or using the arrow keys.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoMenus::class)
        }
    }

}

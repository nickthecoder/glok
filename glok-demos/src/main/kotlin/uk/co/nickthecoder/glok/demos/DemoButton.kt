package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Button
import uk.co.nickthecoder.glok.demos.DemoButton.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.scene.NodeParent
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.asImageView
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.LIKE_LINK

/**
 * See [ABOUT]
 */
class DemoButton : Application() {

    val iconSizeProperty by intProperty(24)
    var iconSize by iconSizeProperty

    val resizableIcons = demoIcons.resizableIcons(iconSizeProperty)

    val aboutTextArea = styledTextArea("") {
        setSimpleStyledText(ABOUT, DemoPanel.STYLES, DemoPanel.TERMINATORS)
        prefRowCount = 5
        readOnly = true
    }

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoButton"
            scene(800, 600) {
                root = demoPanel {
                    name = "DemoButton"
                    about = ABOUT
                    relatedDemos.addAll(DemoCommands::class)

                    content = borderPane {
                        top = toolBar {
                            createButtons("ToolBar : ")
                        }
                        center = aboutTextArea

                        bottom = hBox {
                            // Keyboard shortcut F6 can jump to this.
                            // (We are using this hBox in a similar way to a toolbar)
                            section = true

                            fillHeight = true
                            padding(6)
                            spacing(6)
                            createButtons("HBox : ")
                        }
                    }
                }
            }
            show()
        }

    }


    /**
     * NOTE. By using [NodeParent], we can add these buttons to any control, which has multiple children,
     * such as `Toolbar` (whose child nodes are in `items` list)
     * and `HBox` (whose child nodes are in `children` list).
     */
    private fun NodeParent.createButtons(text: String) {
        + label(text)
        + button("Hello") { onAction { println("Hello") } }
        + button("Disabled") {
            disabled = true
            onAction { println("Can't click me") }
        }
        + Button("", resizableIcons["document-save"].asImageView())
        + Button("Save As", resizableIcons["document-save-as"].asImageView())
        + button("Properties", resizableIcons["document-properties"].asImageView()) {
            disabled = true
        }
        + button("Link") {
            style(LIKE_LINK)
            tooltip = TextTooltip( "A button which looks like a HTML link" )
        }
    }

    companion object {
        val ABOUT = """
            The exact same icons are shown in the [ToolBar] at the top,
            and the [HBox] at the bottom.
            
            But they look different, because the default theme has special rules
            for buttons in a ToolBar...
            
            1 ) [contentDisplay = GRAPHIC_ONLY], so buttons with a graphic omit the text.
            2 ) Buttons with graphics have no borders in the `normal` state.
            3 ) There's less padding.
             
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoButton::class)
        }
    }
}

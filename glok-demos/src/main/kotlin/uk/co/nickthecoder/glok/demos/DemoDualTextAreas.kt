package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.demos.DemoDualTextAreas.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.TextPositionProperty
import uk.co.nickthecoder.glok.property.functions.column
import uk.co.nickthecoder.glok.property.functions.plus
import uk.co.nickthecoder.glok.property.functions.row
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.TextDocument

/**
 * See [ABOUT].
 */
class DemoDualTextAreas : Application() {
    override fun start(primaryStage: Stage) {

        val textDocument = TextDocument(ABOUT)

        with(primaryStage) {
            title = "DemoDualTextAreas"

            scene(800, 500) {
                root = demoPanel {
                    name = "DemoDualTextAreas"
                    about = ABOUT

                    content = splitPane {
                        + textArea(textDocument) { section = true }.withPositions()
                        + textArea(textDocument) { section = true }.withPositions()
                    }
                }
            }

            show()
        }

    }

    private fun TextArea.withPositions() = borderPane {

        center = this@withPositions
        bottom = toolBar {
            side = Side.BOTTOM
            + Label("Caret")
            + textPosition(caretPositionProperty)
            + Label("Anchor")
            + textPosition(anchorPositionProperty)
        }
    }

    private fun textPosition(position: TextPositionProperty) = hBox {
        spacing(5)
        padding(0, 5)
        alignment = Alignment.BOTTOM_LEFT

        + textField("") {
            readOnly()
            prefDigits(3)
            textProperty.bindTo((position.row() + 1).toObservableString())
        }
        + Label(",")
        + textField("") {
            readOnly()
            prefDigits(3)
            textProperty.bindTo((position.column() + 1).toObservableString())
        }
    }

    companion object {
        val ABOUT = """
            Two [TextArea]s sharing the same [TextDocument].
            Changing the text in one will be reflected in the other.
            
            Below each are two positions.
            Internally, these are zero based,
            but we display them in human-friendly one-based.
            
            `Anchor` is the same as `Caret` unless there is selected text.
            The `Anchor` may be before or after the `Caret`.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoDualTextAreas::class)
        }
    }
}

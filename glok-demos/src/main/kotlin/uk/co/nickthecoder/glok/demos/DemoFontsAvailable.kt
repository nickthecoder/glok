/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Font

class DemoFontsAvailable : Application() {
    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "DemoFontsAvailable"
            scene(800, 600) {
                root = demoPanel {
                    name = "DemoFontsAvailabel"
                    about = ABOUT

                    content = hBox {
                        + scrollPane {
                            content = vBox {
                                + label(" Font Families")
                                + label("")
                                for (family in Font.allFontFamilies()) {
                                    + label(family)
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
        Demonstrates how to interrogate the fonts available on the system.
        """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoFontsAvailable::class)
        }
    }

}


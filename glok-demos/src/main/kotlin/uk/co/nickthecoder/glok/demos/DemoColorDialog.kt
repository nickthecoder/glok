package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.PaletteColorPickerDialog
import uk.co.nickthecoder.glok.demos.DemoColorDialog.Companion.ABOUT
import uk.co.nickthecoder.glok.dialog.CustomColorPickerDialog
import uk.co.nickthecoder.glok.property.boilerplate.ColorProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleColorProperty
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoColorDialog : Application() {

    val chosenColorProperty: ColorProperty = SimpleColorProperty(Color.BLUE)
    var chosenColor by chosenColorProperty

    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "DemoColorDialog"
            scene(700, 600) {
                root = demoPanel {
                    name = "DemoColorDialog"
                    about = ABOUT
                    relatedDemos.addAll(DemoColorPicker::class)

                    content = form {
                        section = true
                        padding(20)

                        + row("ColorButton (default uses CustomColorPickerDialog)") {
                            right = colorButton("") { // A Color Button can have a label, but we set it to blank.
                                title = "Your favourite color?" // Appears in the window's title bar.
                                colorProperty.bidirectionalBind(chosenColorProperty)
                            }
                        }

                        + row("ColorButton (with live-update)") {
                            right = colorButton("") { // A Color Button can have a label, but we set it to blank.
                                title = "Your favourite color?" // Appears in the window's title bar.
                                live = true
                                colorProperty.bidirectionalBind(chosenColorProperty)
                            }
                        }

                        + row("Choose Alpha Too") {
                            right = colorButton("") {
                                title = "Your favourite color?"
                                chooseAlpha = true
                                colorProperty.bidirectionalBind(chosenColorProperty)
                            }
                        }

                        + row("ColorButton (Using a PaletteColorPickerDialog)") {
                            right = colorButton("") {
                                title = "Your favourite color?"
                                dialogFactory = { PaletteColorPickerDialog() }
                                colorProperty.bidirectionalBind(chosenColorProperty)
                            }
                        }

                        + row("Open a CustomColorPickerDialog manually") {
                            right = button("Pick a Color") {
                                onAction {
                                    CustomColorPickerDialog().apply {
                                        createStage(scene?.stage !!).apply {
                                            // We now have a Stage object, but it isn't visible yet...
                                            title = "Your favourite color?"
                                            show()
                                        }
                                        colorProperty.bidirectionalBind(chosenColorProperty)
                                    }
                                }
                            }
                        }

                        + row("Open a PaletteColorPickerDialog manually") {
                            right = button("Pick a Color") {
                                onAction {
                                    PaletteColorPickerDialog().apply {
                                        title = "Your favourite color?"
                                        allowCustomColors = false // If we want the user to ONLY select from the palette.

                                        // Let's remove all the greys.
                                        // Note, it is more efficient to clear, and then addAll, rather than use removeIf{ ... }
                                        // The later would rebuild the palette multiple times (could take a second or more).
                                        // This way, it is only done twice (once on `clear`, and again on `addAll`).
                                        val withoutGreys = paletteColorPicker.palette.filter { it.red != it.green || it.red != it.blue }
                                        paletteColorPicker.palette.clear()
                                        paletteColorPicker.palette.addAll(withoutGreys)

                                        createStage(scene?.stage !!).apply {
                                            title = "Your favourite color?"
                                            show()
                                        }

                                        colorProperty.bidirectionalBind(chosenColorProperty)
                                    }
                                }
                            }
                        }

                        + row("Result") {
                            right = pane {
                                overridePrefWidth = 32f
                                overridePrefHeight = 32f
                                backgroundColorProperty.bindTo(chosenColorProperty)
                            }
                        }
                    }
                }
            }
            show()
        }

    }


    companion object {
        val ABOUT = """
            Shows buttons, which display a [ColorPickerDialog].
            There are two types of [ColorPickerDialog] :
                [CustomColorPickerDialog] and [PaletteColorPickerDialog].
                
            Two of the buttons are specialised [ColorButton]s, and
            the other two are regular buttons, which show the [ColorPickerDialog]s manually.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoColorDialog::class)
        }
    }
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokTimer
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.ProgressBar
import uk.co.nickthecoder.glok.property.boilerplate.OptionalFloatUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.StringUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import kotlin.math.roundToInt

class DemoProgressBar : Application() {

    /**
     * All the ProgressBars' progress properties are bound to this, so they all move in unison.
     */
    val myProgressProperty by floatProperty(0f)
    var myProgress by myProgressProperty

    override fun start(primaryStage: Stage) {

        // This timer increases, myProgress, which eventually exceeds 1f.
        // At this point, all progress bars will show 100% full.
        val timer = GlokTimer(1.0, true) {
            myProgress += 0.01f
        }

        with(primaryStage) {
            title = "ProgressBar Demo"
            scene {
                root = demoPanel {
                    name = "DemoProgressBar"
                    about = ABOUT

                    content = vBox {
                        fillWidth = true

                        + hBox {
                            padding(10)
                            spacing = 50f

                            // A Vertical ProgressBar
                            + progressBar {
                                orientation = Orientation.VERTICAL
                                progressProperty.bindTo(myProgressProperty)
                            }.withPercentageBelow2()

                            + vBox {
                                spacing = 50f

                                // A horizontal ProgressBar decorated with a % label.
                                + progressBar {
                                    progressProperty.bindTo(myProgressProperty)
                                }.withPercentage()

                                // Longer, and the % label is below
                                + progressBar {
                                    progressProperty.bindTo(myProgressProperty)
                                    length = 300f
                                }.withPercentageBelow()

                                // The % label is on top of the ProgressBar
                                + progressBar {
                                    // Tantalum has rules for a fatter progress bar, specifically designed
                                    // for placing text on top of it.
                                    style(".fat")
                                    progressProperty.bindTo(myProgressProperty)
                                }.withPercentageOnTop()

                                // A button to reset progress back to 0.
                                + button("Reset") {
                                    onAction { myProgress = 0f }
                                }
                            }
                        } // End of hBox.

                        // This will grow to fit the size of the scene.
                        + progressBar {
                            growPriority = 1f
                            progressProperty.bindTo(myProgressProperty)
                        }
                    }
                }
            }
            timer.start()
            show()
        }
    }

    companion object {

        fun main(vararg args: String) {
            launch(DemoProgressBar::class, args)
        }

        val ABOUT = """
            Demonstrates [ProgressBars], aligned both horizontally and vertically.
            
            The length of the bar is easy to adjust, using the [length] property.
            However, to adjust the thickness, I recommend creating a new Theme
            (not shown in this demo).
            
            A [ProgressBar] has no text, but it is easy to add a label.
            At the bottom of this Demo's source code are various examples which do this.
            
            Note the [progress] property isn't clamped to 0..1, and in this demo,
            it DOES exceed 0.
            The property [clampedProgress] is clamped to 0..1 (and is read-only)
            """.trimIndent()
    }

}

/**
 * An example of how to add a label which shows the progress as a percentage.
 */
private fun ProgressBar.withPercentage() = hBox {
    spacing(10)

    + this@withPercentage
    + label("") {
        textProperty.bindTo(
            StringUnaryFunction(clampedProgressProperty) { progress ->
                "${(progress * 100).roundToInt()}%"
            }
        )
    }
}

private fun ProgressBar.withPercentageBelow() = vBox {
    spacing(4)
    alignment = Alignment.TOP_CENTER

    + this@withPercentageBelow
    + label("") {
        textProperty.bindTo(
            StringUnaryFunction(clampedProgressProperty) { progress ->
                "${(progress * 100).roundToInt()}%"
            }
        )
    }
}

private fun ProgressBar.withPercentageOnTop() = stackPane {
    alignment = Alignment.CENTER_CENTER

    + this@withPercentageOnTop
    + label("") {
        textProperty.bindTo(
            StringUnaryFunction(clampedProgressProperty) { progress ->
                "${(progress * 100).roundToInt()}%"
            }
        )
    }
}

private fun ProgressBar.withPercentageBelow2() = vBox {
    spacing(4)
    alignment = Alignment.TOP_CENTER

    + this@withPercentageBelow2
    + label("") {
        textProperty.bindTo(
            StringUnaryFunction(clampedProgressProperty) { progress ->
                "${(progress * 100).roundToInt()}%"
            }
        )
        // We need to make the vBox a fixed width, based on the size of this Label's font.
        // If we don't do this, then the vBox will change width (as the label's text changes),
        // and the nodes to the right will move.
        //
        // FYI, the "surroundX()" is just in case the label has padding and borderSize.
        // These are usually 0, so it would look ok without it.
        this@vBox.overridePrefWidthProperty.bindTo(
            OptionalFloatUnaryFunction(fontProperty) { it.widthOf("100%") + surroundX() }
        )
        // Note, if you display the percentage with decimal places, you need to take into account
        // the 100% won't be the widest string. e.g. it may be "88.8%" when using 1 decimal place.
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.demos.DemoSingleContainer.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.SimpleHAlignmentProperty
import uk.co.nickthecoder.glok.property.boilerplate.SimpleVAlignmentProperty
import uk.co.nickthecoder.glok.property.functions.observablePos
import uk.co.nickthecoder.glok.scene.HAlignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.VAlignment
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT]
 */
class DemoSingleContainer : Application() {


    override fun start(primaryStage: Stage) {

        // One of these will become the `content` for the SingleContainer.
        val myTextField = textField("Hello World") { section = true }
        val myTextArea = textArea("Hello\nWorld") { section = true }

        val singleContainer = singleContainer { content = myTextField }

        val vAlignmentProperty = SimpleVAlignmentProperty(VAlignment.TOP)
        val hAlignmentProperty = SimpleHAlignmentProperty(HAlignment.LEFT)
        singleContainer.alignmentProperty.bindTo(observablePos(hAlignmentProperty, vAlignmentProperty))


        with(primaryStage) {
            title = "SingleContainerDemo"
            scene(800, 600) {
                root = demoPanel {
                    name = "SingleContainerDemo"
                    about = ABOUT
                    relatedDemos.add(DemoFunctions::class)

                    content = borderPane {
                        top = vBox {
                            fillWidth = true

                            + toolBar {
                                + Label("Content : ")
                                + button("TextField") { onAction { singleContainer.content = myTextField } }
                                + button("TextArea") { onAction { singleContainer.content = myTextArea } }
                            }

                            + toolBar {
                                + Label("Alignment : ")
                                radioChoices(hAlignmentProperty) {
                                    + propertyRadioButton2(HAlignment.LEFT, "Left")
                                    + propertyRadioButton2(HAlignment.CENTER, "Center")
                                    + propertyRadioButton2(HAlignment.RIGHT, "Right")
                                }
                                + Separator()
                                radioChoices(vAlignmentProperty) {
                                    + propertyRadioButton2(VAlignment.TOP, "Top")
                                    + propertyRadioButton2(VAlignment.CENTER, "Center")
                                    + propertyRadioButton2(VAlignment.BOTTOM, "Bottom")
                                }
                            }

                            + toolBar {
                                + Label("Fill : ")
                                + toggleButton("Width") {
                                    singleContainer.fillWidthProperty.bindTo(selectedProperty)
                                }
                                + toggleButton("Height") {
                                    singleContainer.fillHeightProperty.bindTo(selectedProperty)
                                }
                            }
                        }
                        center = singleContainer
                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            Demonstrates a [SingleContainer].
            
            You can choose the [SingleContainer.content], and set the following properties :
            [SingleContainer.alignment], [SingleContainer.fillWidth], [SingleContainer.fillHeight].
            
            Note, if `fillWidth` and `fillHeight` are both true, then `alignment` does nothing.
            
            This demo does not demonstrate the features inherited from [Region], such as `border`, `background` and `padding`.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoSingleContainer::class)
        }
    }
}

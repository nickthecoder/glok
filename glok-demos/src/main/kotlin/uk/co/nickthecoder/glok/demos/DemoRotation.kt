package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.Restart
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Button
import uk.co.nickthecoder.glok.control.Rotation
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoRotation : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoRotation"

            scene(700, 600) {
                root = demoPanel {
                    name = "DemoRotation"
                    about = ABOUT

                    content = borderPane {

                        top = toolBar {
                            + rotation(- 270) {
                                + Button("-270 °")
                            }
                            + rotation(- 180) {
                                + Button("-180 °")
                            }
                            + rotation(- 90) {
                                + Button("-90 °")
                            }
                            + rotation(0) {
                                + Button("0 °")
                            }
                            + rotation(90) {
                                + Button("90 °")
                            }
                            + rotation(180) {
                                + Button("180 °")
                            }
                            + rotation(270) {
                                + Button("270 °")
                            }
                            + rotation(- 90) {
                                + intSpinner {
                                    max = 99
                                    editor.prefColumnCount = 2
                                }
                            }
                        }

                        bottom = toolBar {
                            + rotation(0) {
                                + button("Spin Me") {
                                    onAction {
                                        val rotation = parent as Rotation
                                        rotation.angle = if (rotation.angle == 270) {
                                            0
                                        } else {
                                            rotation.angle + 90
                                        }
                                    }
                                }
                            }
                        }

                        right = rotation(90) {
                            + toolBar(Side.TOP) {
                                + button("Hello") { onAction { println("Hello") } }
                                + button("World") { onAction { println("World") } }
                                + intSpinner {
                                    max = 99
                                    editor.prefColumnCount = 2
                                }
                            }
                        }

                        center = TextArea(ABOUT)
                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
            The buttons above are individually rotated.
                They do nothing when clicked.
            
            The entire toolbar on the right is rotated.
                Note, we use [Side.TOP] (the default), even though it is on the right
                (because of the rotation).
                
            The button below should rotate when clicked.
            
            I've also added [IntSpinner]s too, as they are more complex controls.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoRotation::class)
        }
    }
}

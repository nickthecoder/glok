package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.scrollPaneWithButtons
import uk.co.nickthecoder.glok.scene.dsl.vBox

class DemoScrollPaneWithButtons : Application() {


    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "ScrollPaneWithButtons Demo"
            scene(600, 300) {
                root = demoPanel {
                    name = "DemoScrollPaneWithButtons"
                    about = ABOUT
                    relatedDemos.addAll(DemoTabBar::class)

                    content = vBox {
                        section = true
                        + label("") { growPriority = 1f }
                        + scrollPaneWithButtons {
                            content = Label(LONG_TEXT)
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        val LONG_TEXT = "Long text, longer than the width of the scene. Repeated!".repeat(3)
        val ABOUT = """
            A [ScrollPaneWithButtons] works similar to a [ScrollPane], but instead of showing scroll bars,
            it has two buttons for scrolling.
            You may also scroll with a drag gesture on a track-pad.
            
            Note, the center part of a [ScrollPaneWithButtons] is a [ScrollPane], with [ScrollBarPolicy].NEVER.
            
            They can be vertical (not demonstrated here). See `orientation` property.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoScrollPaneWithButtons::class)
        }
    }

}

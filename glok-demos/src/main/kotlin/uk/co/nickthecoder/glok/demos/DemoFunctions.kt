package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demos.DemoFunctions.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.ObservableInt
import uk.co.nickthecoder.glok.property.boilerplate.ObservableOptionalNode
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeUnaryFunction
import uk.co.nickthecoder.glok.property.functions.plus
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * See [ABOUT]
 */
class DemoFunctions : Application() {

    override fun start(primaryStage: Stage) {
        lateinit var a: ObservableInt
        lateinit var b: ObservableInt

        with(primaryStage) {
            title = "DemoFunctions"

            scene(800, 400) {

                root = demoPanel {
                    name = "DemoFunctions"
                    about = ABOUT

                    content = formGrid {
                        section = true

                        + row("A") {
                            right = intSpinner {
                                value = 42
                                a = valueProperty
                            }
                        }
                        + row("B") {
                            right = intSpinner {
                                value = 3
                                b = valueProperty
                            }
                        }

                        + row("A + B") {
                            right = label("") {
                                textProperty.bindTo((a + b).toObservableString())
                            }
                        }

                        + row("Split A") {
                            right = singleContainer {
                                contentProperty.bindTo(split(a))
                            }
                        }
                        + row("Split B") {
                            right = singleContainer {
                                contentProperty.bindTo(split(b))
                            }
                        }
                    }
                }
            }
            show()
        }

    }


    /**
     * Converts an ObservableInt into a HBox, containing a Label for each digit.
     */
    private fun split(observable: ObservableInt): ObservableOptionalNode {
        return OptionalNodeUnaryFunction(observable) { value ->
            val text = value.toString()
            hBox {
                spacing(6)
                for (c in text) {
                    + label("$c") {
                        borderSize(1)
                        plainBorder(Tantalum.strokeColor)
                        padding( 4 )
                        plainBackground(Tantalum.background2Color)
                    }
                }
            }
        }
    }


    companion object {
        val ABOUT = """
            A demonstration of property functions.
            
            The third row is a label whose `textProperty` is bound to :
                [(a + b).toObservableString()]
            
            Note that `a` and `b` are ObservableInt, not Int, and the plus operator is not (directly)
            adding two integers.
            Instead, it is listening to two `ObservableInt`s, and whenever either of them change,
            it asks for their values, and then adds them.
            
            The package : [uk.co.nickthecoder.glok.property.functions]
            contains many built-in property functions, such as \+ and `toObservableString()`
            
            (a \+ b) is also an [ObservableInt] (not an Int)
            
            We finally use [toObservableString()], because we want to bind it to a [StringProperty]
            (the `textProperty` of a `Label`.
            
            The 4th line is, IMHO, much more interesting, and is a *really* useful technique.
            The `split` function converts an [ObservableInt] into an [ObservableNode]
            (actually, the value can be null,nso it is actually an [OptionalObservableNode]).
            In this example, I build a [HBox], with a Label for each digit of the input.
            The result is bound to the contents of a [SingleContainer].
            
            The same technique is used in the [NodeInspector] (you can play with it from the
            hamburger menu in the top right).
            The bottom part of the [NodeInspector] is a [SingleContainer], whose content
            is bound to a property-function, whose input is the [TreeView]'s currently selected value.
            So when you click an item in the [TreeView], the bottom panel is rebuilt.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoFunctions::class)
        }
    }
}

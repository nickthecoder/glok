package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.PropertyRadioButton
import uk.co.nickthecoder.glok.control.SingleContainer
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.demos.DemoPropertyRadioButton.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoPropertyRadioButton : Application() {

    // Beware, this pattern is bad! These nodes are being created before the application has started. Eek!
    // In the real world, consider defining a class which is responsible for the overall structure
    // of the 'main window', and move these declarations there.
    // The implementation may well be in 3 different source code files.
    private val homeView = label("Pretend this is a 'Home' page!") {
        alignment = Alignment.CENTER_CENTER
    }

    private val preferencesView = label("Pretend this is the preferences section of your application") {
        alignment = Alignment.CENTER_CENTER
    }

    private val editorView = label("Pretend this is the main GUI of your application") {
        alignment = Alignment.CENTER_CENTER
    }

    /**
     * A `SingleContainer` uses this property for its content.
     * So when this property changes, the content below the toolbar changes.
     *
     * If you are new to Glok, this may seem weird at first. However, I highly recommend using this pattern.
     * Instead of _doing_ stuff (running code) when buttons are pressed, use bound properties instead.
     * Your code will end up _declarative_, rather than _imperative_.
     *
     * In this case we _declare_ that the content below the toolbar is [viewNodeProperty].
     * We then _declare_ 'choices' to use this property, and assign a value for each of the [PropertyRadioButton].
     */
    private val viewNodeProperty by optionalNodeProperty(homeView)

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "PropertyRadioButton Demo"
            scene(700, 500) {
                root = demoPanel {
                    name = "DemoPropertyRadioButton"
                    about = ABOUT

                    content = borderPane {
                        top = threeRow {
                            section = true
                            padding(10)

                            left = Label("Left")

                            center = hBox {
                                spacing(10)
                                padding(4)
                                // We are using the property : viewNodeProperty to determine which radio button
                                // is selected.
                                radioChoices(viewNodeProperty) {
                                    // When viewNodeProperty.value == homeView, then this button is selected.
                                    + propertyRadioButton2(homeView, "Home")
                                    // When viewNodeProperty.value == editorView, then this button is selected.
                                    + propertyRadioButton2(editorView, "Editor")
                                    // When viewNodeProperty.value == preferencesView, then this button is selected.
                                    + propertyRadioButton2(preferencesView, "Preferences")
                                    // Clicking any of the buttons will set viewNodeProperty.value to
                                    // the appropriate value. i.e. the first argument of propertyRadioButton()
                                    // This is automatically make the clicked button selected, and the other unselected.
                                }
                            }

                            right = Label("Right")
                        }
                        center = SingleContainer(viewNodeProperty)

                        bottom = TextArea("""
                            We have mimicked a TabPane.
                            But unlike a TabPane, we can use the space to the
                            left and right of the 'tabs' as we see fit.
                            """.trimIndent()
                        )
                    }
                }
            }
            show()
        }

    }


    companion object {
        val ABOUT = """
            There are three [PropertyRadioButton]s, each has a `value`.
            In this demo, I've chosen to use Nodes, but you could use any type, [String], [Int], [Color]...
            
            The buttons behave much like a [RadioButton] i.e. exactly one is `selected` at a time.
            
            The possible values are : [homeView], [editorView] and [preferencesView].
            
            When you click a button, the property [viewNodeProperty] is updated to one of these values
            (depending on which button you pressed).
            
            The center of the scene has a [SingleContainer], whose [contentProperty] is bound to [viewNodeProperty].
            The net result : the center of the scene displays a different node, depending on which
            [ChoiceRadioButton] you pressed. We've mimicked a TabPane!
            
            Notice that there is only _setup_ code. There is no code to "do stuff" when a button is pressed.
            
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoPropertyRadioButton::class)
        }
    }

}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.demos.DemoMixedTreeView2.Companion.ABOUT
import uk.co.nickthecoder.glok.demos.DemoTreeView.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import java.io.File

/**
 * See [ABOUT].
 */
class DemoMixedTreeView2 : Application() {

    override fun start(primaryStage: Stage) {

        lateinit var treeView: MixedTreeView<FileItem>

        with(primaryStage) {
            title = "MixedTreeView Demo 2"
            scene = scene(700, 800) {
                root = demoPanel {
                    name = "DemoMixedTreeView2"
                    about = ABOUT
                    relatedDemos.addAll(DemoMixedTreeView::class, DemoListView::class, DemoListViewCustomCells::class)

                    content = borderPane {
                        center = mixedTreeView<FileItem> {
                            treeView = this
                            root = FileItem(File("/home/nick/tmp/test")).apply {
                                expanded = true
                            }
                        }

                        top = toolBar {
                            + toggleButton("Show Root") {
                                selectedProperty.bidirectionalBind(treeView.showRootProperty)
                            }
                        }

                    }
                }
            }
            show()
        }
    }


    inner class FileItem(val file: File) : MixedTreeItem<FileItem>() {

        val expandedListener = expandedProperty.addChangeListener { _, _, expanded ->

            if (expanded) {
                file.listFiles()?.sorted()?.let { allFiles ->
                    for (subDir in allFiles.filter { it.isDirectory && ! it.isHidden }) {
                        children.add(FileItem(subDir)).apply { populateChildrenWhenExpanded() }
                    }
                    for (file in allFiles.filter { it.isFile && ! it.isHidden }) {
                        // In this demo, we don't need a special class for
                        children.add(FileItem(file).apply { leaf = true })
                    }
                }

            } else {
                children.clear()
            }
        }

        init {
            /*
            parentProperty.addChangeListener { _, _, parent ->
                if (parent == null) {
                    println("Removed from parent for $this")
                    expandedProperty.removeChangeListener(expandedListener)
                }
            }
            */
        }

        override fun createCell(treeView: MixedTreeView<FileItem>): MixedTreeCell<FileItem> {
            return TextMixedTreeCell(treeView, this, file.name)
        }

        /**
         * Populate [TreeItem.children] on the fly, when the TreeItem is expanded.
         */
        fun populateChildrenWhenExpanded() {

        }

        override fun toString() = file.name
    }


    companion object {

        val ABOUT = """
            A demonstration of how to use a [TreeView].
            It shows the directory tree starting at your home folder.
            
            When a directory is contracted, [children] is empty.
            [children] is populated on the fly, when the directory is expanded.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoMixedTreeView2::class)
        }

    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.demos.DemoListViewCustomCells.Companion.ABOUT
import uk.co.nickthecoder.glok.property.BidirectionalBind
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * The model for the items in our [ListView].
 */
class Person(name: String, age: Int) {
    val nameProperty by stringProperty(name)
    var name by nameProperty

    val ageProperty by intProperty(age)
    var age by ageProperty
}

/**
 * The ListCell for [DemoListViewCustomCells] Application.
 */
class PersonListCell(listView: ListView<Person>, person: Person) : SingleNodeListCell<Person, HBox>(listView, person, HBox()) {

    private val nameLabel = label("") {
        textProperty.bindTo(person.nameProperty)
        // Allow the label to grow/shrink, so that the spinners are aligned to the right.
        growPriority = 1f
        shrinkPriority = 1f
        // The limit, at which the name will stop shrinking (and the cell will no longer fit, when the window is shrunk too much).
        overrideMinWidth = 20f
    }
    private val ageSpinner = intSpinner {
        editor.prefColumnCount = 3
        valueProperty.bidirectionalBind(person.ageProperty)
    }

    init {
        node.apply {
            alignment = Alignment.CENTER_LEFT
            focusAcceptable = true
            padding(2, 4)

            + nameLabel
            + ageSpinner
        }
    }
}

/**
 * See [ABOUT].
 */
class DemoListViewCustomCells : Application() {
    override fun start(primaryStage: Stage) {

        val listView = listView<Person> {
            section = true
            cellFactory = { listView, person -> PersonListCell(listView, person) }
            items.addAll(
                Person("Nick", 53),
                Person("Emma", 45),
                Person("Nalin", 53)
            )
        }

        with(primaryStage) {
            title = "DemoListViewCustomCells"

            scene(600, 500) {
                root = demoPanel {
                    name = "DemoListViewCustomCells"
                    about = ABOUT

                    content = borderPane {

                        top = toolBar {
                            + button("Add") { onAction { listView.items.add(Person("New", 0)) } }
                            + button("Remove") {
                                disabledProperty.bindTo(listView.selection.selectedIndexProperty.equalTo(- 1))
                                onAction { listView.items.remove(listView.selection.selectedItem) }
                            }
                            + Separator()
                            + button("Increment All") {
                                onAction {
                                    for (item in listView.items) {
                                        item.name += "+"
                                        item.age ++
                                    }
                                }
                            }
                            + spacer()
                            + button("Dump") { onAction { scene?.dump() } }
                        }

                        center = listView
                    }
                }
            }

            show()
        }
    }

    companion object {
        val ABOUT = """
            An example custom [ListCell].
            Each cell is made up of the person's name as a [Label] and their age as an [IntSpinner].
            We use a bidirectional bind for the age, so that when the person's age, or the spinner's value
            changes, the other property also changes.
            The [nameLabel] only needs a regular bind, because a [Label] cannot be edited.
            If [Person] wasn't mutable, then the binds wouldn't be needed, and we could use simple
            assignments in [update] instead.
            (If you know that name isn't changed, we wouldn't need a bind for it, and could have just assigned
            the Label's `text`).
            
            BTW, if you add lots of items to the list (without enlarging the window), and then inspect,
            or dump the scene (from the hamburger button in the top right),
            you can see that the scene only has enough [PersonListCell]s for the items that are visible.
            (Not one [PersonListCell] for every Person in the [ListView]).
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoListViewCustomCells::class)
        }
    }
}

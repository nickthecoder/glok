/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.ThemeBuilder
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.BUTTON
import uk.co.nickthecoder.glok.theme.styles.FORM_GRID
import uk.co.nickthecoder.glok.theme.styles.LABEL

private object MyTheme : ThemeBuilder() {

    /**
     * I'm using a property here, just to demonstrate how properties can be used
     * to let the user customise the application.
     * Normally, for something as simple as a heading's padding,
     * you would use a hard-coded value, not a Property.
     */
    val titlePaddingProperty by floatProperty(20f)
    var titlePadding by titlePaddingProperty

    override fun buildTheme() = theme {

        // Note, in this demo, I've used in-line strings, such as ".title".
        // To avoid accidental typos (which cannot be caught at compile time),
        // you may prefer to use `const val` Strings.
        // Note that LABEL, FORM_GRID and BUTTON are `const val` Strings in Styles.kt in the package
        // uk.co.nickthecoder.glok.theme.styles

        LABEL {
            ".title" {
                font(Font.create(30f, FontStyle.PLAIN, "Verdana"))
                alignment(Alignment.CENTER_CENTER)
                padding(titlePadding, 10) // The left and right padding are hard-coded values (10).
                plainBackground()
                backgroundColor(Tantalum.background2Color)
                borderSize(0, 0, 1, 0)
                plainBorder()
                borderColor(Tantalum.strokeColor)
            }
            ".footing" {
                font(Font.create(16f, FontStyle.PLAIN, "Verdana"))
                alignment(Alignment.CENTER_RIGHT)
                padding(2, 5)
            }
        }

        FORM_GRID {
            ".myForm" {
                padding(10)
            }
        }

        // Demonstrates that we can override any rules declared by Tantalum.
        // The new padding can be seen within the "Close" button.
        // NOTE. This only overrides the "normal" padding for buttons.
        // Tantalum sets a DIFFERENT (smaller) padding for buttons in a ToolBar
        // That rule is more specific than this rule, and therefore that existing rule takes priority.
        // The net result : Buttons within ToolBars are unchanged, but other buttons, such as the "Close" button,
        // are affected.
        BUTTON {
            padding(10, 40)
        }

    }

    init {
        theme = buildTheme()

        // Declare all properties that this theme builder depends on.
        // If any of these properties change, the theme will be rebuilt.
        // NOTE. We MUST include Tantalum.background2ColorProperty etc., because we want to rebuild THIS theme
        // when that property changes.
        // If we didn't include it and then changed its value, only Tantalum's theme would be rebuilt,
        // and then combined with MyTheme (which isn't rebuilt), so the heading's background would be "wrong".
        dependsOn(
            titlePaddingProperty,
            Tantalum.background2ColorProperty,
            Tantalum.strokeColorProperty
        )
    }
}

class DemoCombineThemes : Application() {

    override fun start(primaryStage: Stage) {
        // Tantalum is somewhat customisable, without needing to create a new Theme.
        // For example, we can make button square :
        Tantalum.radius = 0f

        // IMPORTANT!!!
        GlokSettings.defaultThemeProperty.unbind()
        GlokSettings.defaultThemeProperty.bindTo(Tantalum combineWith MyTheme)
        // If you need to combine more than two themes, use this slightly more verbose solution :
        // (which combines ThemeProperties rather than ThemeBuilders).
        //
        // GlokSettings.defaultThemeProperty.bindTo(
        //     Tantalum.themeProperty combineWith MyTheme.themeProperty combineWith AnotherTheme.themeProperty
        // )

        with(primaryStage) {
            title = "Combine Themes Demo"
            scene(800, 600) {
                root = demoPanel {
                    name = "DemoCombineThemes"
                    about = ABOUT

                    content = borderPane {
                        top = label("An Example Title") { style(".title") }

                        center = formGrid {
                            style(".myForm")

                            + row("Title Padding") {
                                right = floatSpinner {
                                    valueProperty.bidirectionalBind(MyTheme.titlePaddingProperty)
                                    editor.prefColumnCount = 4
                                }
                            }
                        }

                        bottom = vBox {
                            fillWidth = true
                            + label("An example footing") { style(".footing") }

                            + toolBar {
                                // This button is here to demonstrate that MyTheme's rule for buttons
                                // does NOT affect buttons in ToolBars.
                                // MyTheme only override the "general" rule, not the more specific rule
                                // for Buttons in ToolBars.
                                + button("Button in ToolBar")
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
            Demonstrates how to combine Tantalum (the default theme), with
            a custom theme, specific to your application.
            
            The important parts of this demo are the [MyTheme] object, and the line :
            
                GlokSettings.defaultThemeProperty.bindTo(Tantalum combineWith MyTheme)
                    
            within the application's start() method.
            
            The _.heading_ style uses a Property to define the vertical padding,
            whereas the _.footing_ style only uses hard-coded values.
            
            Therefore, the heading's padding can be easily changed at run-time.
            If you want this property to be customisable by end-users,
            then you need load/save the property using Java Preferences.
            (which is not included in this demo).
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoCombineThemes::class)
        }
    }
}

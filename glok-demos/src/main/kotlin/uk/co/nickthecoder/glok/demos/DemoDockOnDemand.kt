package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demos.DemoDock.Companion.ABOUT
import uk.co.nickthecoder.glok.demos.DemoDockOnDemand.Companion.ABOUT
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.OnDemandDockFactory
import uk.co.nickthecoder.glok.dock.inspector.NodeInspectorDock
import uk.co.nickthecoder.glok.dock.load
import uk.co.nickthecoder.glok.dock.save
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import java.util.prefs.Preferences

/**
 * See [ABOUT].
 *
 * NOTE. This uses the same Docks as [DemoDock], not within this file. See [HelloDock] and [GoodbyeDock].
 */
class DemoDockOnDemand : Application() {

    private val harbour = Harbour()

    private val dockFactory = OnDemandDockFactory(
        harbour, mapOf(
            HelloDock.ID to { HelloDock(it) },
            GoodbyeDock.ID to { GoodbyeDock(it) },
            NodeInspectorDock.ID to { NodeInspectorDock(it) }
        )
    )

    /**
     * The [Preferences] node for this application.
     * [Harbour]'s state will be saved to node : `uk/co/nickthecoder/glok/demos/harbour`
     * (`harbour` is always appended).
     */
    private fun preferences() = Preferences.userNodeForPackage(DemoDockOnDemand::class.java)

    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "Dock on Demand Demo"

            scene(900, 700) {
                root = demoPanel {
                    name = "DemoDockOnDemand"
                    about = ABOUT
                    relatedDemos.add(DemoDock::class)

                    content = borderPane {
                        top = vBox {
                            fillWidth = true
                            + menuBar {
                                + menu("View") {
                                    + toggleMenuItem("Hello Dock") {
                                        selectedProperty.bidirectionalBind(dockFactory.visibleProperty(HelloDock.ID) !!)
                                    }
                                    + toggleMenuItem("Goodbye Dock") {
                                        selectedProperty.bidirectionalBind(dockFactory.visibleProperty(GoodbyeDock.ID) !!)
                                    }
                                    + toggleMenuItem("Inspector Dock") {
                                        selectedProperty.bidirectionalBind(dockFactory.visibleProperty(NodeInspectorDock.ID) !!)
                                    }
                                }
                            }
                            + toolBar {
                                + toggleButton("Hello Dock") {
                                    selectedProperty.bidirectionalBind(dockFactory.visibleProperty(HelloDock.ID) !!)
                                }
                                + toggleButton("Goodbye Dock") {
                                    selectedProperty.bidirectionalBind(dockFactory.visibleProperty(GoodbyeDock.ID) !!)
                                }
                                + toggleButton("Node Inspector Dock") {
                                    selectedProperty.bidirectionalBind(dockFactory.visibleProperty(NodeInspectorDock.ID) !!)
                                }
                            }
                        }
                        center = harbour.apply {
                            content = textArea(CONTENT_TEXT) {
                                section = true
                            }
                        }
                    }
                }
            }

            // Harbour's state is persisted using Java preferences. Adjust the positions/sizes of the
            // Docks, and restart the application to test it.
            harbour.load(preferences())
            onClosed { harbour.save(preferences()) }

            show()
        }
    }

    companion object {
        val ABOUT = """
            Creates docks on-demand, using the built-in [OnDemandDockFactory].
            
            This factory has a `visibleProperty` for each available [Dock]
            (even before the dock is created).
            
            We use these properties to bind with [ToggleButton.selectedProperty].
            
            PS. I recommend using [Actions] and [Commands] rather than
            creating ToggleButtons/ToggleMenuItems directly.
            """.trimIndent()

        val CONTENT_TEXT = """
            Imagine this is the main area for your application.

            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoDockOnDemand::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.demos.DemoHBox.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.alignmentProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoHBox : Application() {

    val mainAlignmentProperty by alignmentProperty(Alignment.TOP_LEFT)
    var mainAlignment by mainAlignmentProperty

    val mainFillHeightProperty by booleanProperty(false)
    var mainFillHeight by mainFillHeightProperty

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoHBox"
            scene(800, 600) {
                root = demoPanel {
                    name = "DemoHBox"
                    about = ABOUT

                    content = vBox {
                        spacing = 10f
                        padding = Edges(10f)
                        fillWidth = true

                        hBoxes() // *** This is where the hBoxes being tested appear!

                        + Label("Alignment")
                        + hBox {
                            section = true
                            spacing = 5f

                            alignmentControls(
                                Alignment.TOP_LEFT,
                                Alignment.CENTER_LEFT,
                                Alignment.BOTTOM_LEFT
                            )
                            alignmentControls(
                                Alignment.TOP_CENTER,
                                Alignment.CENTER_CENTER,
                                Alignment.BOTTOM_CENTER
                            )
                            alignmentControls(
                                Alignment.TOP_RIGHT,
                                Alignment.CENTER_RIGHT,
                                Alignment.BOTTOM_RIGHT
                            )

                        }
                        + separator()

                        + checkBox("Fill Height ?") {
                            selectedProperty.bidirectionalBind(mainFillHeightProperty)
                        }
                    }
                }
            }
            show()
        }

    }

    /**
     * These are the red/white/blue boxes that we are testing.
     */
    private fun NodeParent.hBoxes() {
        + hBox {
            growPriority = 0.5f
            shrinkPriority = 0.5f
            borderSize = Edges(1f)
            plainBorder(Color.GRAY)
            alignmentProperty.bindTo(mainAlignmentProperty)
            fillHeightProperty.bindTo(mainFillHeightProperty)
            + pane {
                plainBackground(Color.RED)
                overridePrefHeight = 50f
                overridePrefWidth = 200f
                shrinkPriority = 1f
            }
            + pane {
                plainBackground(Color.WHITE)
                overridePrefHeight = 150f
                overridePrefWidth = 200f
            }
            + pane {
                plainBackground(Color.BLUE)
                overridePrefHeight = 100f
                overridePrefWidth = 200f
            }
        }

        + hBox {
            growPriority = 0.5f
            shrinkPriority = 0.5f
            borderSize = Edges(1f)
            plainBorder(Color.GRAY)
            alignmentProperty.bindTo(mainAlignmentProperty)
            fillHeightProperty.bindTo(mainFillHeightProperty)
            + pane {
                plainBackground(Color.RED)
                overridePrefHeight = 50f
                overridePrefWidth = 200f
                growPriority = 1f // Twice as much as WHITE
                shrinkPriority = 1f
            }
            + pane {
                plainBackground(Color.WHITE)
                overridePrefHeight = 150f
                overridePrefWidth = 200f
                growPriority = 0.5f // Half as much as RED
            }
            + pane { // Does not grow
                plainBackground(Color.BLUE)
                overridePrefHeight = 100f
                overridePrefWidth = 200f
            }
        }
    }

    private fun NodeParent.alignmentControls(vararg alignment: Alignment) {
        + vBox {
            spacing = 10f
            for (pos in alignment) {
                + propertyRadioButton(mainAlignmentProperty, pos, pos.name.lowercase()) {
                    growPriority = 1f
                    shrinkPriority = 1f
                }
            }
        }
    }

    companion object {
        val ABOUT = """
            This demo isn't a `HOW-TO`, instead it let's you change the important properties
            of a [HBox], so that you can see their effect.
            
            The boxes at the top have `growPriority` of 0, and therefore do not grow,
            and the horizontal alignment has an effect.
            
            The boxes in the middle DO grow (at different rates), and therefore always
            fill the entire horizontal space, so the horizontal alignment has no effect.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoHBox::class)
        }
    }
}

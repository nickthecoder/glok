package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.demos.DemoTabPane.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.sideProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED

/**
 * See [ABOUT]
 */
class DemoTabPane : Application() {

    val tabSideProperty by sideProperty(Side.TOP)

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoTabPane"
            scene(700, 500) {
                root = demoPanel {
                    name = "DemoTabPane"
                    about = ABOUT

                    content = borderPane {
                        // Buttons which change the side of the first TabPane.
                        top = toolBar {
                            radioChoices(tabSideProperty) {
                                + propertyRadioButton2(Side.TOP, "Top")
                                + propertyRadioButton2(Side.BOTTOM, "Bottom")
                                + propertyRadioButton2(Side.LEFT, "Left")
                                + propertyRadioButton2(Side.RIGHT, "Right")
                            }
                        }

                        center = splitPane {

                            + tabPane {
                                // We can align the TabBar.
                                // For top orientation, BOTTOM_CENTER would be better,
                                // But this demo alters TabPane.side, and CENTER_CENTER is a compromise
                                tabBar.alignment = Alignment.CENTER_CENTER

                                // The side is determined by the buttons in the toolBar above...
                                sideProperty.bindTo(tabSideProperty)
                                + tab("Hello") {
                                    graphic = ImageView(Tantalum.scaledIcons["file_tinted"])
                                    style(TINTED)
                                    content = TextArea("Hello Content")
                                }
                                + tab("World") {
                                    content = TextArea("World Content")
                                }
                                + tab("Disabled") {
                                    // Prevents the tab from being selected.
                                    disabled = true
                                    content = Label("You will never see this!")
                                }
                            }

                            + documentTabPane {
                                // If there is only 1 tab, then the TabBar is made invisible to save space
                                hideSingleTab = true

                                + tab("Doc 1") {
                                    graphic = ImageView(Tantalum.scaledIcons["file_tinted"])
                                    style(TINTED)
                                    content = TextArea("The third tab's content")
                                }
                                + tab("Doc 2") {
                                    content = TextArea("The forth tab's content")
                                }
                                + tab("*Doc 3") {

                                    content = TextArea("You can't close this tab.\nonCloseRequest consumes the event.")
                                    onCloseRequested {
                                        // This is where you would typically show a "Document Changed" dialog
                                        // with options : Save / Close Without Saving / Cancel
                                        println("No, I won't let you close me!")
                                        it.consume()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
            In this Demo :
            
            1. Create a "normal" [TabPane] on the left hand side
            2. See what the [TabPane] looks like with different orientations
            3. Create an alternate type of [TabPane], which is designed
               for tabs containing `documents`. See [documentTabPane]
               The class is the same, but it uses different [Theme] rules.
            4. Consume the [onCloseRequest], which prevents a tab from closing.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTabPane::class)
        }
    }
}

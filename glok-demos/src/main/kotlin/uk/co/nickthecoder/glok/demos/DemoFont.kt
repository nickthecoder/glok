package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.property.boilerplate.SimpleFloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.theme.Tantalum

class DemoFont : Application() {

    val fontSizeProperty by floatProperty(Tantalum.fontSize * 2)
    var fontSize by fontSizeProperty

    override fun start(primaryStage: Stage) {
        Tantalum.dark = true

        with(primaryStage) {
            title = "DemoFont"
            scene(1000, 600) {
                root = demoPanel {
                    name = "DemoFont"
                    about = ABOUT

                    content = borderPane {
                        top = toolBar {
                            + button("Default") {
                                onAction { center = imageViewOfFont(Tantalum.font.size(fontSize)) }
                            }
                            + button("Fixed Width") {
                                onAction { center = imageViewOfFont(Tantalum.fixedWidthFont.size(fontSize)) }
                            }
                            + button("Italic") {
                                onAction { center = imageViewOfFont(Tantalum.font.italic().size(fontSize)) }
                            }
                        }
                        bottom = vBox {
                            fillWidth = true
                            padding(4)
                            spacing( 4)
                            section = true

                            + hBox {
                                alignment = Alignment.CENTER_LEFT
                                spacing( 20)

                                + label("Default Font")
                                + textField("") { growPriority = 1f }
                            }
                            + hBox {
                                alignment = Alignment.CENTER_LEFT
                                spacing( 20)

                                + label("Fixed Width")
                                + textField("") {
                                    growPriority = 1f
                                    style("fixed_width")
                                }
                            }
                            // I added this over-sized italic font because I was having issues with font metrics.
                            // This let me check that J's metrics are "correct". That's in quotes, because
                            // AWT returns false data, so I've had to GUESS some of the metrics! Eek!
                            + hBox {
                                alignment = Alignment.CENTER_LEFT
                                spacing(20)

                                + label("Italic Font")
                                + textField("") {
                                    growPriority = 1f
                                    font = Tantalum.font.italic().size(32f)
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    private fun imageViewOfFont(font: Font) = scrollPane {
        fitToWidth = false
        fitToHeight = false
        content = imageView(font.image()?.scaledBy(1f / GlokSettings.globalScale))
    }

    companion object {
        val ABOUT = """
            Shows a bitmap font's texture.
            
            I created this so that I could inspect the texture behind bitmap fonts.
            The fonts are rendered to a texture as a mostly white image,
            which is then `tinted` to the required font-color.
            Therefore, it is tricky to see using the light theme.
                      
            The text field at the bottom lets you type (or paste) any characters.
            If the bitmap font is missing glyphs, the texture will be rebuilt
            with the additional glyphs added.
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoFont::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.*
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.load

/**
 * To build and run the demos from source :
 *
 *    ./gradlew glok-demo:run
 *
 * Each demo has an "About" tab, which explains the purpose of the demo.
 * So to prevent duplication, there are no Dokka comments.
 */
class DemoMenu : Application() {

    override fun start(primaryStage: Stage) {
        GlokSettings.load()
        // The settings are saved when the GlokSettingsDialog is closed (See DemoPanel)

        with(primaryStage) {
            title = "DemoMenu"

            scene(1000, 670) {
                root = demoPanel {
                    name = "DemoMenu"
                    about = ABOUT

                    content = buildContent()
                }
            }
            show()

            // When setting an icon for the window, we supply many different sizes, which the window manager
            // can pick from.
            // demoIcons has separate 'sheets' for icon sizes : 96, 32 and 24.
            // So the 48 pixels version is scaled by the Glok's `Icons` class.
            // If we omitted 48, it would make no difference. If the window manager wants 48 pixels,
            // it would scale the 98 pixel version.
            // The actual Demos don't set the icon, so they will use a default.
            val iconSizes = listOf( 96, 48, 32, 24 )
            primaryStage.setIcon(iconSizes.map { demoIcons.get("document-properties", it) })
            // If you prefer to use individual .png images, create a Texture for each image
            // using Resources.loadTexture(). (Note : Texture implements Image).
        }
    }


    private fun buildContent(): Node {
        return scrollPane {
            section = true

            content = flowPane {
                padding(20, 10, 4, 10)
                xSpacing = 20f
                ySpacing = 10f

                for (simpleName in DEMO_CLASSES.sortedBy { it.uppercase() }) {
                    val displayName = if (simpleName.startsWith("Demo")) {
                        simpleName.substring(4)
                    } else {
                        simpleName
                    }
                    // Make each column a fixed-width, but still allow the button to have its pref width.
                    + singleContainer {
                        fillWidth = false
                        overridePrefWidth = 200f

                        content = button(displayName) {
                            onAction { launch(simpleName) }
                        }
                    }
                }
            }
        }
    }

    private fun launch(simpleName: String) {
        @Suppress("UNCHECKED_CAST")
        val klass = Class.forName("${this.javaClass.packageName}.$simpleName") as Class<Application>
        GlokSettings.restarts.add(Restart(klass.kotlin))
        GlokSettings.restarts.add(Restart(DemoMenu::class))
        status = ApplicationStatus.REQUEST_QUIT
    }

    companion object {

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoMenu::class, args)
        }

        private val ABOUT = """
            These demos were originally my playground, while developing Glok.
            A simple way to try out the controls that I'd just written.
            
            They have changed quite substantially since then,
            and hopefully they are good examples of how to use `Glok`.
            
            They assume you are proficient in `Kotlin`, but are new to `Glok`
            
            """.trimIndent()

        private val DEMO_CLASSES = listOf(
            "DemoAlert",
            "DemoButton",
            "DemoCheckBox", "DemoChoiceBox", "DemoColorDialog", "DemoColorPicker", "DemoCommands", "DemoCombineThemes",
            "DemoDock", "DemoDockOnDemand", "DemoDualTextAreas",
            "DemoExpandBar",
            "DemoFileDialogs", "DemoFileFields",
            "DemoFindReplace", "DemoFont", "DemoFontsAvailable", "DemoFormGrid", "DemoFunctions",
            "DemoGlobalScale",
            "DemoHBox",
            "DemoLabel", "DemoListView", "DemoListViewCustomCells",
            "DemoMenus", "DemoMixedTreeView", "DemoMousePointer",
            "DemoNinePatch",
            "DemoProgressBar", "DemoPromptDialog", "DemoPropertyRadioButton",
            "DemoRadioButton", "DemoRotation", "DemoRuler",
            "DemoScrollBar", "DemoScrollPane", "DemoScrollPaneWithButtons", "DemoSingleContainer", "DemoSlider",
            "DemoSpinner", "DemoSplitPane", "DemoStackPane", "DemoStyledTextArea",
            "DemoTantalum", "DemoTabBar", "DemoTabPane", "DemoTextArea", "DemoThreeRow", "DemoTitledPane",
            "DemoToolBar", "DemoTreeView",
            "DemoVBox",
            "HelloWorld"
        )
    }
}

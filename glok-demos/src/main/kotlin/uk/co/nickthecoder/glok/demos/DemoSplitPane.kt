package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.Restart
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.SplitPane
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*

class DemoSplitPane : Application() {

    override fun start(primaryStage: Stage) {

        lateinit var mySplitPane: SplitPane
        lateinit var leftNode: Node
        lateinit var centerNode: Node
        lateinit var rightNode: Node

        with(primaryStage) {
            title = "SplitPane Demo"

            scene(600, 400) {
                root = demoPanel {
                    name = "DemoSplitPane"
                    about = ABOUT

                    content = borderPane {

                        top = toolBar {

                            + checkBox("Resize Left") {
                                onAction {
                                    leftNode.growPriority = if (selected) 1f else 0f
                                }
                            }
                            + checkBox("Resize Center") {
                                onAction {
                                    centerNode.growPriority = if (selected) 1f else 0f
                                }
                            }
                            + checkBox("Resize Right") {
                                onAction {
                                    rightNode.growPriority = if (selected) 1f else 0f
                                }
                            }

                        }
                        center = splitPane {
                            mySplitPane = this
                            + button("Left") {
                                leftNode = this
                                onAction { visible = false }
                            }
                            + button("Center") {
                                centerNode = this
                                onAction { visible = false }
                            }
                            + button("Right") {
                                rightNode = this
                                onAction { visible = false }
                            }
                            dividers[0].position = 0.25f
                        }

                        bottom = toolBar {
                            + Label("Click the contents to make them invisible")
                            + spacer()
                            + button("Show") {
                                tooltip = TextTooltip("""
                                    If you have hidden items in the SplitPane,
                                    this will make them visible again.
                                    """.trimIndent()
                                )
                                onAction {
                                    for (item in mySplitPane.items) {
                                        item.visible = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
            show()
        }

    }

    companion object {
        val ABOUT = """
            Behind the scenes, the divider positions are stores as a ratio
            (range 0..1). e.g. with 1 divider, a position of 0.5 would give equal
            space to both child nodes.
            
            Resize the window to see how it affects the divider positions.
            With none or all panels set to "Resize", each panel should grow in
            equals proportion.

            With only 1 panel set to resize (the center is a good choice),
            it will grow, and the other two will stay the same size.
            
            Now try dragging a divider. You should see a similar behaviour.
            For example, if only the right panel is set to resize, then dragging the
            left divider should keep the center panel the same size, so the
            right divider also moves.
            
            FYI, `shrinkPriority` is ignored by SplitPane.
            
            In this demo, the contents of the split pane are Buttons.
            If you click one, Button.visible is set to false.
            The SplitPane should hide one of its dividers, and allocate the space to
            other nodes in the SplitPane.
            
            This seems obvious, but JavaFX doesn't do that, it just leaves a blank gap.
            
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoSplitPane::class)
        }
    }

}

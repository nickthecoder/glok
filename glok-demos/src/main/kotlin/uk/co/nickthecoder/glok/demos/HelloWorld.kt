package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.scene

/**
 * The classic Hello World program.
 */
class HelloWorld : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "Hello World"
            scene(300, 100) {
                root = label("Hello World") {
                    alignment = Alignment.CENTER_CENTER
                }
            }
            show()
        }
    }

    companion object {
        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(HelloWorld::class)
        }
    }

}

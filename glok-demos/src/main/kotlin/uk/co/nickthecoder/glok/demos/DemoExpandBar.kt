package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.demos.DemoExpandBar.Companion.ABOUT
import uk.co.nickthecoder.glok.demos.DemoTextArea.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoExpandBar : Application() {

    lateinit var textArea: TextArea

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoExpandBar"
            scene(1000, 600) {
                root = demoPanel {
                    name = "DemoExpandBar"
                    about = ABOUT
                    relatedDemos.addAll(DemoSplitPane::class)

                    content = formGrid {
                        section = true

                        + row {
                            above = label("Drag the bars to change the TextAreas' width / height")
                        }
                        + row("withExpandHeight") {
                            right = textArea("Hello world") {
                                overrideMinHeight = 100f
                                overrideMaxHeight = 300f
                                prefRowCount = 3
                            }.withExpandHeight()
                        }
                        + row("withExpandWidth") {
                            right = textArea("Hello world") {
                                overrideMinWidth = 100f
                                prefRowCount = 3
                            }.withExpandWidth()
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            Any node can have an additional expand bar, which changes the node's preferred height or width.
            
            In this demo, I've used TextAreas, but it can be applied to any node that can be resized.
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoExpandBar::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.IntSpinner
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.SpinnerArrowPositions
import uk.co.nickthecoder.glok.demos.DemoSpinner.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoSpinner : Application() {

    override fun start(primaryStage: Stage) {

        lateinit var spinner: IntSpinner

        with(primaryStage) {
            title = "DemoSpinner"
            scene(700, 500) {
                root = demoPanel {
                    name = "DemoSpinner"
                    about = ABOUT
                    relatedDemos.addAll(DemoSlider::class)

                    content = borderPane {
                        top = vBox {
                            + toolBar {
                                + intSpinner {
                                    min = 0
                                    max = 100
                                    spinner = this
                                    editor.prefColumnCount = 3
                                }
                            }
                            + toolBar {
                                + Label("Button Positions")
                                + Separator()
                                radioChoices(spinner.spinnerArrowPositionsProperty) {
                                    + propertyRadioButton2(SpinnerArrowPositions.SPLIT, "Split")
                                    + propertyRadioButton2(SpinnerArrowPositions.LEFT_HORIZONTAL, "Left H")
                                    + propertyRadioButton2(SpinnerArrowPositions.LEFT_VERTICAL, "Left V")
                                    + propertyRadioButton2(SpinnerArrowPositions.RIGHT_HORIZONTAL, "Right H")
                                    + propertyRadioButton2(SpinnerArrowPositions.RIGHT_VERTICAL, "Right V")
                                }
                            }
                        }

                        center = styledTextArea("") {
                            readOnly = true
                            setSimpleStyledText(ABOUT, DemoPanel.STYLES, DemoPanel.TERMINATORS)
                        }

                        bottom = toolBar {
                            + Label("Value : ")
                            + label("") {
                                textProperty.bindTo(spinner.valueProperty.toObservableString())
                            }
                            + Label("Text : ")
                            + label("") {
                                textProperty.bindTo(spinner.editor.textProperty)
                            }
                            + spacer()

                            + toggleButton("Auto Update") {
                                onAction {
                                    // Try typing valid and invalid text when this is toggle on and off.
                                    // When on, the background will update immediately, rather than waiting until
                                    // ENTER is pressed, or the IntSpinner loses focus.
                                    spinner.autoUpdate = selected
                                }
                            }
                            + spacer()

                            // Programmatically set the value.
                            + button("Set to 5") {
                                onAction { spinner.value = 5 }
                            }
                            + button("Set to -1 (invalid)") {
                                onAction { spinner.value = - 1 }
                            }
                        }
                    }
                }
            }
            show()
        }

    }

    companion object {

        val ABOUT = """
            The spinner's value and the editor's text are show below.
            Notice how the value does NOT change while you type.
            It only updates when the [Spinner] loses focus, or you hit +Enter+.
            If the text is invalid, then the value doesn't change,
            even when you press +Enter+, or change focus.
            
            If you click the _Auto Update_ button, the [IntSpinner] will now update its value
            on every key-press.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoSpinner::class)
        }
    }

}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.demos.DemoGlobalScale.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.borderPane
import uk.co.nickthecoder.glok.scene.dsl.radioChoices
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.toolBar

/**
 * See [ABOUT].
 */
class DemoGlobalScale : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoGlobalScale"
            scene(700, 500) {
                root = demoPanel {
                    name = "DemoGlobalScale"
                    about = ABOUT

                    content = borderPane {
                        top = toolBar {

                            radioChoices(GlokSettings.globalScaleProperty) {

                                + Label("Set Global Scale :")

                                for (a in 1..6) {
                                    val scale = a * 0.5f
                                    + propertyRadioButton2(scale, "$scale")
                                }
                            }
                        }

                        center = TextArea(ABOUT)
                    }
                }
            }
            show()
        }

    }

    companion object {

        val ABOUT = """
            Change [Application.globalScale] with a click of a button.
            
            Glok supports high resolution screens (high DPI), by using 'logical' pixels,
            rather than `physical` pixels. All controls are scaled automatically behind
            the scenes, so the application developer doesn't have to worry about
            the actual resolution.
            
            To make best use of the display's capabilities, Font and Image report their
            sizes in 'logical' pixels, but behind the scenes, the physical size may
            be scaled by [Application.globalScale].
            
            This demo let's you change [Application.globalScale].
            Clicking 1.0 will show you what would happen if Glok wasn't High DPI aware.
            On a low-res device, it will look normal.
            On a high-res device, everything will be unusably small.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoGlobalScale::class)
        }
    }

}

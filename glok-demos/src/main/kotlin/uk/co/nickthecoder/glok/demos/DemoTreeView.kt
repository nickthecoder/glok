package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.TextTreeCell
import uk.co.nickthecoder.glok.control.TreeItem
import uk.co.nickthecoder.glok.control.TreeView
import uk.co.nickthecoder.glok.demos.DemoTreeView.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import java.io.File

/**
 * See [ABOUT].
 */
class DemoTreeView : Application() {

    override fun start(primaryStage: Stage) {

        lateinit var treeView: TreeView<File>

        with(primaryStage) {
            title = "TreeView Demo"
            scene = scene(700, 400) {
                root = demoPanel {
                    name = "DemoTreeView"
                    about = ABOUT
                    relatedDemos.addAll(DemoMixedTreeView::class, DemoListView::class, DemoListViewCustomCells::class)

                    content = borderPane {
                        center = treeView<File> {
                            section = true
                            treeView = this

                            root = createFolderItem(File(System.getProperty("user.home"))).apply {
                                expanded = true
                            }
                            cellFactory = { tree, item ->
                                TextTreeCell(tree, item, item.value.name).apply {
                                    onPopupTrigger { event ->
                                        popupMenu {
                                            + menuItem("Fake Delete") {
                                                onAction {
                                                    // Only removes the TreeItem doesn't actually delete the file ;-)
                                                    item.parent?.children?.remove(item)
                                                }
                                            }
                                        }.show(event.sceneX, event.sceneY, scene !!.stage !!)
                                    }

                                    if (item.value.isDirectory) {
                                        node.graphic = Tantalum.icon("folder")
                                    } else {
                                        // NOTE Glok currently assumes that all TreeCells in a TreeView are the same height.
                                        // So the "folder" and "file" icons MUST be the same size.
                                        // Otherwise, the view would "jump" weirdly when scrolled :-( Sorry.
                                        // If we want some cells without an icon (or a different size),
                                        // then FORCE the TreeCells to be the same size.
                                        // Either by setting TreeView.fixedRowHeight,
                                        // or by setting TreeCell.overridePrefHeight to the same value for all cells.
                                        node.graphic = Tantalum.icon("file")
                                        onMouseClicked { event ->
                                            // Open the file when double-clicked.
                                            if (event.clickCount == 2 && event.isPrimary) {
                                                openFile(item.value)
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        top = toolBar {
                            + toggleButton("Show Root") {
                                selectedProperty.bidirectionalBind(treeView.showRootProperty)
                            }
                        }

                    }
                }
            }
            show()
        }
    }

    private fun openFile(file: File) {
        if (Platform.isWindows()) {
            // From what I've read, I think this *might* work!?!
            println("I don't have a windows machine, and haven't tested this :-(")
            ProcessBuilder(
                "rundll32.exe", "shell32.dll", "ShellExec_RunDLL", file.absolutePath
            )
        } else {
            // I'm not sure if this works on macOS either ???
            ProcessBuilder("xdg-open", file.absolutePath)
        }.apply {

            // Throw away stdout and stderr. Without this, if the command outputs text, then the
            // process will block when the buffer is full.
            val nullFile = if (Platform.isWindows()) {
                File("NUL:")
            } else {
                File("/dev/null")
            }
            redirectError(nullFile)
            redirectOutput(nullFile)

            start()
        }
    }

    /**
     * Create a TreeItems which hold a _folder_. We listen to the `expanded` Property,
     * so that we can add the children _on demand_.
     */
    private fun createFolderItem(folder: File): TreeItem<File> = TreeItem(folder).apply {
        expandedProperty.addChangeListener { _, _, expanded ->

            // See below for an alternate implementation.
            if (expanded && children.isEmpty()) {
                value.listFiles()?.sorted()?.let { allFiles ->
                    // Add the folders before the files.
                    for (subDir in allFiles.filter { it.isDirectory && ! it.isHidden }) {
                        children.add(createFolderItem(subDir))
                    }
                    for (file in allFiles.filter { it.isFile && ! it.isHidden }) {
                        children.add(TreeItem(file).apply { leaf = true })
                    }
                }

            }
        }
    }

    // An alternative implementation, which clears the children when the item is collapsed.
    // PROS :
    //     Refreshes the contents each time it is expanded.
    //     Memory efficient (but not important).
    // CONS :
    //     Looses the "expanded" state of children in collapsed folders.
    //     Time inefficient (especially on slow remote filesystems).
    //
    // NOTE. In a real-world application I would use Java's WatchService to automatically
    // refresh the contents of a folder. So the primary PRO is moot.
    // But that is beyond the scope of this demo.
    /*
    private fun createFolderItem(folder: File): TreeItem<File> = TreeItem(folder).apply {
        expandedProperty.addChangeListener { _, _, expanded ->

            if (expanded) {
                // This part is identical
                value.listFiles()?.sorted()?.let { allFiles ->
                    // Add the folders before the files.
                    for (subDir in allFiles.filter { it.isDirectory && ! it.isHidden }) {
                        children.add(createFolderItem(subDir))
                    }
                    for (file in allFiles.filter { it.isFile && ! it.isHidden }) {
                        children.add(TreeItem(file).apply { leaf = true })
                    }
                }

            } else {
                children.clear()
            }
        }
    }
    */

    companion object {

        val ABOUT = """
            A demonstration of how to use a [TreeView].
            It shows the directory tree starting at your home folder.
            
            When a directory is contracted, [children] is empty.
            [children] is populated on the fly, when the directory is expanded.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTreeView::class)
        }

    }
}

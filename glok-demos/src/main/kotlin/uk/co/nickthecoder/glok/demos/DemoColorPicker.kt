package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.CustomColorPicker
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.PaletteColorPicker
import uk.co.nickthecoder.glok.demos.DemoColorPicker.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoColorPicker : Application() {

    lateinit var opaquePicker: CustomColorPicker
    lateinit var alphaPicker: CustomColorPicker
    lateinit var palettePicker: PaletteColorPicker

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoColorPicker"
            scene(900, 550) {
                root = demoPanel {
                    name = "DemoColorPicker"
                    about = ABOUT
                    relatedDemos.addAll(DemoColorDialog::class)

                    content = borderPane {

                        center = tabPane(Side.LEFT) {
                            tabBar.padding(20, 6)
                            container.padding(20)

                            + tab("Custom Color Picker") {
                                content = customColorPicker(Color.WHITE) {
                                    opaquePicker = this
                                }
                            }
                            + tab("With Alpha") {
                                content = customColorPicker(Color.WHITE.withAlpha(0.9f), true) {
                                    alphaPicker = this
                                }
                            }
                            + tab("Palette Color Picker") {
                                content = paletteColorPicker(Color.WHITE.withAlpha(0.9f)) {
                                    palettePicker = this
                                }
                            }
                        }


                        // Various options to change the look of the ColorPickers.
                        bottom = vBox {
                            fillWidth = true

                            + toolBar(Side.BOTTOM) {

                                + Label("Slider Size")
                                + spinner(256f) {
                                    max = 500f
                                    editor.prefColumnCount = 3
                                    opaquePicker.colorFieldSizeProperty.bindTo(valueProperty)
                                    alphaPicker.colorFieldSizeProperty.bindTo(valueProperty)
                                }

                                + Label("Swatch Size")
                                + spinner(40f) {
                                    min = 20f
                                    max = 150f
                                    editor.prefColumnCount = 3
                                    opaquePicker.swatchSizeProperty.bindTo(valueProperty)
                                    alphaPicker.swatchSizeProperty.bindTo(valueProperty)
                                }

                                + Label("Palette Swatch Size")
                                + spinner(24f) {
                                    min = 10f
                                    max = 50f
                                    editor.prefColumnCount = 3
                                    palettePicker.swatchSizeProperty.bindTo(valueProperty)
                                }
                            }

                            + toolBar(Side.BOTTOM) {
                                + toggleButton("Show Labels") {
                                    opaquePicker.showLabelsProperty.bindTo(selectedProperty)
                                    alphaPicker.showLabelsProperty.bindTo(selectedProperty)
                                }
                                + toggleButton("Show Current Color") {
                                    opaquePicker.showCurrentColorProperty.bindTo(selectedProperty)
                                    alphaPicker.showCurrentColorProperty.bindTo(selectedProperty)
                                }
                            }
                        }
                    }
                }
            }

            show()
        }

    }


    companion object {
        val ABOUT = """
            This demo has three instances of [ColorPicker].
            The first two are [CustomColorPicker] (with and without the alpha chanel)
            The third is a [PaletteColorPicker].
                        
            At the bottom, you can change some of the [ColorPicker]'s properties,
            such as the size of the swatches, if labels are present...
                        
            Often, you don't want to use [ColorPicker] directly. Instead, use
            [ColorPickerDialog], or [ColorButton].
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoColorPicker::class)
        }
    }
}

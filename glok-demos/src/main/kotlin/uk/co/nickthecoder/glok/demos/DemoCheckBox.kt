package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.CheckBox
import uk.co.nickthecoder.glok.demos.DemoCheckBox.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT]
 */
class DemoCheckBox : Application() {

    override fun start(primaryStage: Stage) {
        lateinit var twoState: CheckBox
        lateinit var threeState: CheckBox

        with(primaryStage) {
            title = "CheckBox Demo"
            scene(700, 600) {
                root = demoPanel {
                    name = "DemoCheckBox"
                    about = ABOUT

                    content = vBox {
                        section = true
                        padding( 6, 12 )
                        spacing = 6f

                        + hBox {
                            alignment = Alignment.CENTER_LEFT
                            spacing(12)
                            + checkBox("CheckBox") {
                                twoState = this
                            }

                            + label("Selected") {
                                visibleProperty.bindTo(twoState.selectedProperty)
                            }
                            + label("Indeterminate") {
                                visibleProperty.bindTo(twoState.indeterminateProperty)
                            }
                        }

                        + hBox {
                            alignment = Alignment.CENTER_LEFT
                            spacing(12)
                            + checkBox("CheckBox (with indeterminate state)") {
                                threeState = this
                                allowIndeterminate = true
                            }

                            + label("Selected") {
                                visibleProperty.bindTo(threeState.selectedProperty)
                            }
                            + label("Indeterminate") {
                                visibleProperty.bindTo(threeState.indeterminateProperty)
                            }
                        }

                        + spacer()

                        /*
                            A switch box is a CheckBox, but themed differently.
                            The function `switchBox` creates a CheckBox, removes the "check_box" style,
                            and adds the "switch" style.
                            The Tantalum theme has two different sets of rules for "check_box" and "switch".
                        */
                        + switch("Switch")
                        + switch("Switch (with indeterminate state)") {
                            allowIndeterminate = true
                        }
                        + switch("Switch Disabled") { disabled = true }
                        + switch("Switch Disabled & Selected") {
                            disabled = true
                            selected = true
                        }

                        + spacer()

                        + checkBox("Disabled") { disabled = true }
                        + checkBox("Disabled & Selected") {
                            disabled = true
                            selected = true
                        }
                        + checkBox("Disabled & Indeterminate") {
                            allowIndeterminate = true
                            disabled = true
                            indeterminate = true
                        }

                        + spacer()

                        /*
                            When a CheckBox is in an `indeterminate` state, the `selected` state can be true OR false.
                            They look the same.
                            If you click them, you won't be able to get them both back to their original state,
                            because they only cycle around THREE of the FOUR possible states.
                        */
                        + checkBox("Initially Indeterminate and selected") {
                            allowIndeterminate = true
                            selected = true
                            indeterminate = true
                        }
                        + checkBox("Initially Indeterminate, but not selected") {
                            allowIndeterminate = true
                            selected = false
                            indeterminate = true
                        }

                    }
                }
            }

            show()
        }

    }


    companion object {

        val ABOUT = """
            Displays 2 checkboxes, a bi-state and a tri-state.
            The states of [CheckBox.selected] and [CheckBox.indeterminate] are displayed as labels.
            
            Below that are switches, which are ALSO CheckBoxes, but themed differently.
            
            Below that are more CheckBoxes which are disabled, so you can see what they look like.
        """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoCheckBox::class)
        }
    }

}

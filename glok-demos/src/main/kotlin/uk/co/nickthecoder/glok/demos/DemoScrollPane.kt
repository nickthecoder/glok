package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.ScrollBarPolicy
import uk.co.nickthecoder.glok.control.ScrollPane
import uk.co.nickthecoder.glok.demos.DemoScrollPane.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.PlainBackground
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * See [ABOUT].
 */
class DemoScrollPane : Application() {

    lateinit var scrollPane: ScrollPane

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "ScrollPane Demo"

            scene(700, 500) {
                root = demoPanel {
                    name = "DemoScrollPane"
                    about = ABOUT
                    relatedDemos.addAll(DemoScrollBar::class)

                    content = borderPane {
                        top = toolBar {
                            + toggleButton("Fit To Width") {
                                selected = true
                                onAction { scrollPane.fitToWidth = selected }
                            }
                            + toggleButton("Fit To Height") {
                                selected = true
                                onAction { scrollPane.fitToHeight = selected }
                            }

                        }

                        center = scrollPane {
                            scrollPane = this
                            content = vBox {
                                plainBackground(Tantalum.buttonColor)
                                padding(4)
                                spacing(4)
                                // Create a LOT of labels.
                                // In a real application, we'd just use a TextArea, with readOnly=true.
                                // which has its own built-in ScrollPane.
                                for (word in LOREM_IPSUM.split("\n", " ")) {
                                    + label("$word ".repeat(4))
                                }
                            }
                        }
                        bottom = toolBar(Side.BOTTOM) {
                            + Label("H Scroll : ")
                            toggleGroup {
                                + radioButton2("As Needed") {
                                    selected = true
                                    onAction { scrollPane.hPolicy = ScrollBarPolicy.AS_NEEDED }
                                }
                                + radioButton2("Never") {
                                    onAction { scrollPane.hPolicy = ScrollBarPolicy.NEVER }
                                }
                                + radioButton2("Always") {
                                    onAction { scrollPane.hPolicy = ScrollBarPolicy.ALWAYS }
                                }
                            }
                        }
                    }
                }
            }
            show()
        }

    }


    companion object {

        val ABOUT = """
            A simple demonstration of a [ScrollPane].
            
            Here, we've set the content to a [VBox], containing lots of [Label]s.
            
            You can see the effect of changing `fillWidth` and `fillHeight`.
            As our content is so long, `fillHeight` does nothing,
            whereas you can see the effect of `fillWidth` by the different in background color.
            
            FYI, To see the difference between `As Needed` and `Always`, you may need to
            narrow the window.
            
            BTW. Many Glok controls, such as [TextArea], [ListView] and [TreeView] have built-in
            [ScrollPane]s.
            These are optimised. They do not draw the entire contents, only the content
            within the bounds of the viewport. [ScrollPane] does not provide this optimisation.
            It merely clips the content.
            
            """.trimIndent()

        val LOREM_IPSUM = """
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat. Duis aute irure dolor in reprehenderit
            in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa qui officia deserunt mollit anim id est
            laborum.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoScrollPane::class)
        }
    }

}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.TitledPane
import uk.co.nickthecoder.glok.demos.DemoLabel.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * See [ABOUT].
 */
class DemoLabel : Application() {

    val resources = backend.resources("uk/co/nickthecoder/glok/demos")
    val icons = icons(resources) {
        texture("tango32.png", 32) {
            icon("document-save", 135, 1)
        }
    }

    val mainBorderProperty by borderProperty(NoBorder)
    var mainBorder by mainBorderProperty

    val mainPaddingProperty by edgesProperty(Edges(0f))
    var mainPadding by mainPaddingProperty

    val mainAlignmentProperty by alignmentProperty(Alignment.CENTER_LEFT)
    var mainAlignment by mainAlignmentProperty

    val mainContentDisplayProperty by contentDisplayProperty(ContentDisplay.LEFT)
    var mainContentDisplay by mainContentDisplayProperty

    val mainLabelPaddingProperty by edgesProperty(Edges(0f))
    var mainLabelPadding by mainLabelPaddingProperty

    val mainGraphicTextGapProperty by floatProperty(0f)
    var mainGraphicTextGap by mainGraphicTextGapProperty


    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoLabel"
            scene {
                root = demoPanel {
                    name = "DemoLabel"
                    about = ABOUT

                    content = hBox {
                        padding(20)
                        spacing(20)
                        fillHeight = true

                        labels()

                        + vBox {

                            + section("Alignment") {
                                content = vBox {
                                    spacing(4)

                                    radioChoices(mainAlignmentProperty) {
                                        for (pos in Alignment.values()) {
                                            + propertyRadioButton(pos, pos.name.lowercase())
                                        }
                                    }
                                }
                            }
                        }

                        + vBox {
                            spacing(20)

                            + section("Padding") {
                                content = vBox {
                                    spacing(4)

                                    radioChoices(mainPaddingProperty) {
                                        + propertyRadioButton(Edges(), "No Padding")
                                        + propertyRadioButton(Edges(30f), "30")
                                        + propertyRadioButton(Edges(30f, 5f, 30f, 5f), "30, 5, 30, 5")
                                    }
                                }
                            }

                            + section("Label Padding") {
                                content = vBox {
                                    spacing(4)

                                    radioChoices(mainLabelPaddingProperty) {
                                        + propertyRadioButton(Edges(), "No Padding")
                                        + propertyRadioButton(Edges(10f), "10")
                                        + propertyRadioButton(Edges(30f, 5f, 5f, 30f), "20, 5, 5, 30")
                                    }
                                }
                            }

                            + section("Graphic Text Gap") {
                                content = vBox {
                                    spacing(4)

                                    radioChoices(mainGraphicTextGapProperty) {
                                        + propertyRadioButton(0f, "None")
                                        + propertyRadioButton(10f, "10px")
                                        + propertyRadioButton(30f, "30px")
                                    }
                                }
                            }
                        }

                        + vBox {
                            spacing(20)

                            + section("Content Display") {
                                content = vBox {
                                    spacing(4)

                                    radioChoices(mainContentDisplayProperty) {
                                        for (cd in ContentDisplay.values()) {
                                            + propertyRadioButton(cd, cd.name.lowercase())
                                        }
                                    }
                                }
                            }

                            + section("Border") {
                                content = vBox {
                                    spacing(4)

                                    radioChoices(mainBorderProperty) {
                                        + propertyRadioButton(NoBorder, "No Border")
                                        + propertyRadioButton(PlainBorder, "Plain Border")
                                    }
                                }
                            }

                        }
                    }
                }
            }
            show()
        }

    }

    private fun section(title: String, block: (TitledPane.() -> Unit)? = null) =
        titledPane(title, block).apply {
            collapsable = false
            section = true
        }

    private fun NodeParent.labels() {
        + vBox {
            growPriority = 1f
            fillWidth = true
            spacing = 10f

            + createLabel("Fixed Size Label", 0f)
            + createLabel("Plain Label", 0f)
            + createLabel("Plain Label", 1f)
            + createLabel("With Graphic", 0f, icons.get("document-save", 32))
            + createLabel("With Graphic", 1f, icons.get("document-save", 32))
        }
    }

    private fun createLabel(text: String, growPriority: Float, image: Image? = null) =
        label(text, image.asImageView()) {
            this.growPriority = growPriority

            borderProperty.bindTo(mainBorderProperty)
            borderColorProperty.bindTo(Tantalum.strokeColorProperty)
            borderSize = Edges(2f)

            paddingProperty.bindTo(mainPaddingProperty)
            alignmentProperty.bindTo(mainAlignmentProperty)
            contentDisplayProperty.bindTo(mainContentDisplayProperty)
            graphicTextGapProperty.bindTo(mainGraphicTextGapProperty)
            labelPaddingProperty.bindTo(mainLabelPaddingProperty)
        }


    companion object {

        val ABOUT = """
            This isn't a HOW-TO, instead, it lets you change the main properties of a Label
            (and other classes which extend Labelled) to see the results.
            
            You wouldn't think that there's much to demonstrate about the humble [Label], but you'd be wrong.

            A [Label], just like a [Button] can have a graphic as well as text.
            You can turn the graphic or the text off, and if both are present, you can change the position of the
            graphic with respect to the text. The distance between them can also be changed.
            
            As Labelled is a subclass of [Region], they also have a border and padding.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoLabel::class)
        }
    }
}

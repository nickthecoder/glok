package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demos.DemoRuler.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.optionalFloatProperty
import uk.co.nickthecoder.glok.property.functions.times
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoRuler : Application() {

    // We set this in BorderPane.onMouseMoved.
    // We bind it to Ruler.valueProperty, so that a marker lines up with the mouse pointer.
    val mouseXProperty by optionalFloatProperty(0f)
    var mouseX by mouseXProperty

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoRuler"
            scene(700, 800) {
                root = demoPanel {
                    name = "DemoRuler"
                    about = ABOUT

                    content = borderPane {

                        onMouseMoved { event ->
                            mouseX = event.sceneX - sceneX
                        }
                        onMouseExited { mouseX = null }

                        top = ruler {
                            // The ruler measures pixels.
                            lowerBound = 0f
                            upperBoundProperty.bindTo(widthProperty)
                            markerValueProperty.bindTo(mouseXProperty)
                        }
                        left = ruler(Side.LEFT) {
                            plainBackground(Color.LIGHT_YELLOW)
                            textColor = Color.BLACK

                            lowerBound = 0f
                            upperBound = 100f
                            step = 20f // Heuristic not used

                            extraMarkers.add(15f)
                            extraMarkers.add(85f)
                        }

                        center = textArea(ABOUT) {
                            section = true
                        }

                        right = ruler(Side.RIGHT) {
                            plainBackground(Color.LIGHT_YELLOW)
                            textColor = Color.BLACK

                            lowerBound = 0f
                            upperBound = 100f
                            reversed = true // Y Axis points upwards.
                        }

                        bottom = ruler(Side.BOTTOM) {
                            // Let's pretend that our content has been scaled by 10.
                            lowerBound = 0f
                            upperBoundProperty.bindTo(widthProperty * 10f)
                            markerValueProperty.bindTo(mouseXProperty * 10f)
                        }
                    }
                }

            }

            show()
        }

    }

    companion object {
        val ABOUT =
            """
            Imagine a diagram here.
            
            TOP
                Measures pixels
                Includes a marker, which follows the mouse pointer.

            BOTTOM
                Measures in pixels * 10 (as if we zoomed in on a document ).
                Includes a marker, which follows the mouse pointer.
                
            LEFT
                The bounds are ( 0 .. 100 )
                i.e. measuring percentages.
                [step] = 20
                (the default is 0, and a heuristic chooses the step).

                Includes 2 "extra" markers with values of 15 and 85.

             RIGHT
                 The bounds are ( 0 .. 100 ) again
                 But this time `reverse = true`,
                 so we are measuring from the bottom upwards.
                 [step] isn't set, so the heuristic is choosing it.
                 Note the difference when you resize the window.
                
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoRuler::class)
        }
    }
}

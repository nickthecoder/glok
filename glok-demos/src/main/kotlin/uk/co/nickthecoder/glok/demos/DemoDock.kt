package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.PopupMenu
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.demos.DemoDock.Companion.ABOUT
import uk.co.nickthecoder.glok.dock.*
import uk.co.nickthecoder.glok.dock.inspector.NodeInspectorDock
import uk.co.nickthecoder.glok.dock.places.Places
import uk.co.nickthecoder.glok.dock.places.PlacesDock
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.log
import java.util.prefs.Preferences

/**
 * See [ABOUT].
 */
class DemoDock : Application() {

    private val harbour = Harbour()

    private val helloDock = HelloDock(harbour)
    private val goodbyeDock = GoodbyeDock(harbour)
    private val inspectorDock = NodeInspectorDock(harbour)
    private val placesDock = PlacesDock(harbour)

    init {
        harbour.dockFactory = MapDockFactory(
            helloDock, goodbyeDock, inspectorDock, placesDock
        )
    }

    /**
     * The [Preferences] node for this application.
     * [Harbour]'s state will be saved to node : `uk/co/nickthecoder/glok/demos/harbour`
     * (`harbour` is always appended).
     */
    private fun preferences() = Preferences.userNodeForPackage(DemoDock::class.java)

    override fun start(primaryStage: Stage) {
        log.level = 1

        with(primaryStage) {
            title = "Dock Demo"

            scene(900, 700) {
                root = demoPanel {
                    name = "DemoDock"
                    about = ABOUT
                    relatedDemos.add(DemoDockOnDemand::class)

                    content = borderPane {
                        top = vBox {
                            fillWidth = true
                            + menuBar {
                                + menu("View") {
                                    + toggleMenuItem("Places Dock") {
                                        selectedProperty.bidirectionalBind(placesDock.visibleProperty)
                                    }
                                    + toggleMenuItem("Inspector Dock") {
                                        selectedProperty.bidirectionalBind(inspectorDock.visibleProperty)
                                    }

                                    + Separator()

                                    + toggleMenuItem("Hello Dock") {
                                        selectedProperty.bidirectionalBind(helloDock.visibleProperty)
                                    }
                                    + toggleMenuItem("Goodbye Dock") {
                                        selectedProperty.bidirectionalBind(goodbyeDock.visibleProperty)
                                    }
                                }
                            }
                            + toolBar {
                                + toggleButton("Hello Dock") {
                                    selectedProperty.bidirectionalBind(helloDock.visibleProperty)
                                }
                                + toggleButton("Goodbye Dock") {
                                    selectedProperty.bidirectionalBind(goodbyeDock.visibleProperty)
                                }

                                + spacer()

                                + button("Reset Preferences") {
                                    onAction {
                                        // As the harbour's state will be saved when the stage closes,
                                        // consider killing the application after clicking this!
                                        preferences().apply {
                                            removeNode()
                                            flush()
                                        }
                                    }
                                }

                            }
                        }
                        center = harbour.apply {
                            content = textArea(CENTER_TEXT) {
                                section = true
                            }
                        }
                    }
                }
            }

            // Harbour's state is persisted using Java preferences. Adjust the positions/sizes of the
            // Docks, and restart the application.
            harbour.load(preferences())

            onClosed {
                harbour.save(preferences())
            }

            inspectorDock.rootNode = scene?.root

            show()
        }
    }

    companion object {
        val ABOUT = """
            There are 4 docks in this demo. `Hello` and `Goodbye` are simple.
            `NodeInspector`, (available from the `Edit` menu), is a Glok debugging tool.
            `Places` lists files and folders in a TreeView.
            
            FYI, you can also use the NodeInspector without using docks. See the `hamburger` menu in the top right.
            
            In this demo, we create the 4 docks at start-up. However, it is possible to create them
            on-demand if you implement your own `DockFactory`.
            
            The position and visibility of each dock is saved when the application ends.
            So if you wish to run from a clean slate, click the `Reset Preferences` button,
            and then terminate the application using Task Manager, or Ctrl+C if launched from
            the command line.
            If you close the application normally, the state of the docks are saved.
            
            The `Hello` dock can be dragged to any side of the `Harbour`, but the other two are limited
            to the left/right sides.
            Free floating docks are not implemented (yet).
            """.trimIndent()

        val CENTER_TEXT = """
            Imagine this is the main area for your application.

            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoDock::class)
        }
    }

}

/**
 * An example Dock.
 */
class HelloDock(harbour: Harbour) : Dock(ID, harbour) {

    init {
        title = "Hello"
        content = Label("Bonjour")

        // Add a button to the dock's title-bar.
        titleButtons.addAll(
            button("Hi") {
                onAction { println("Hello") }
            }
        )
    }

    companion object {
        const val ID = "HELLO"
    }
}


/**
 * An example Dock.
 */
class GoodbyeDock(harbour: Harbour) : Dock(ID, harbour) {

    init {
        title = "Goodbye"
        content = Label("Au revoir")

        // By default, this dock will appear on the right.
        defaultSide = Side.RIGHT

        // Prevent the user moving this dock to the top or bottom.
        allowedSides.removeAll(Side.TOP, Side.BOTTOM)
    }

    override fun createContextMenu(): PopupMenu {
        return super.createContextMenu().apply {
            + Separator()
            + menuItem("About") {
                onAction { println("A demo dock. Goodbye!") }
            }
        }
    }

    companion object {
        const val ID = "GOODBYE"
    }
}

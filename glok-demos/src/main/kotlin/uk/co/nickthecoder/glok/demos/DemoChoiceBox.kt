package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.demos.DemoChoiceBox.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.SimpleOptionalStringProperty
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT]
 */
class DemoChoiceBox : Application() {

    override fun start(primaryStage: Stage) {

        val myChoiceProperty = SimpleOptionalStringProperty(null)

        with(primaryStage) {
            title = "DemoChoiceBox"
            scene(600, 400) {
                root = demoPanel {
                    name = "DemoChoiceBox"
                    about = ABOUT

                    content = borderPane {
                        top = toolBar {
                            // Note <String?> is optional, Kotlin is clever enough to work it out ;-)
                            + choiceBox<String?> {
                                myChoiceProperty.bindTo(valueProperty)
                                items.addAll(null, "Hello", "World")

                                // A silly example of how to customise the ToggleMenuItems
                                menuItemFactory = {
                                    createMenuItem(it).apply {
                                        tooltip = TextTooltip("Tooltip for $it")
                                    }
                                }
                            }
                        }
                        center = vBox {
                            padding(10)
                            spacing(10)

                            + Label("You chose :")
                            + label("") {
                                textProperty.bindTo(myChoiceProperty.toObservableString())
                            }
                        }
                    }
                }
            }

            show()
        }

    }

    companion object {
        val ABOUT = """
            Displays a single [ChoiceBox] in a [ToolBar].
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoChoiceBox::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.scene.dsl.sheets
import uk.co.nickthecoder.glok.scene.icons

val demoResources = backend.resources("uk/co/nickthecoder/glok/demos")

val demoIcons = icons(demoResources) {
    sheets {
        sheet("tango24.png", 24)
        sheet("tango.png", 96)
        sheet("tango32.png", 32)

        grid(1, 1, 2) {
            row(
                prefix = "document-",
                "new", "open", "properties", "save-as", "save"
            )
            row(
                prefix = "edit-",
                "clear", "copy", "cut", "delete", "find-replace", "find", "paste", "redo", "select-all", "undo"
            )
        }
    }
}

val icons24 = demoIcons.size(24)

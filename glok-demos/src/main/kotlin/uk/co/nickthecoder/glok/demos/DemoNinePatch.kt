package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.demos.DemoNinePatch.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.dsl.or
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.*

/**
 * See [ABOUT].
 */
class DemoNinePatch : Application() {

    val ninePatches = ninePatchImages(demoResources, "demoButtons.png") {
        ninePatch("normal", 1, 1, 98, 58, Edges(15f))
        ninePatch("hover", 101, 1, 98, 58, Edges(15f))
        ninePatch("armed", 201, 1, 98, 58, Edges(15f))
        ninePatch("selected", 301, 1, 98, 58, Edges(15f))
    }

    /**
     * A quick and dirty way to combine two themes.
     * This isn't recommended for production code though, because if any of Tantalum's properties are
     * changed, the combined theme will NOT reflect those changes.
     *
     * For example, run this demo, and toggle the Dark/Light theme from the `hamburger` icon in the top right.
     * This scene theme will NOT toggle from dark/light.
     * A better solution can be seen in [DemoCombineThemes].
     */
    val myTheme = Tantalum.theme combineWith theme {
        (BUTTON or TOGGLE_BUTTON or RADIO_BUTTON2) {
            noBorder()
            ninePatchBackground(ninePatches["normal"])
            padding(6, 8)
            HOVER {
                noBorder()
                ninePatchBackground(ninePatches["hover"])
            }
            ARMED {
                noBorder()
                ninePatchBackground(ninePatches["armed"])
            }
            SELECTED {
                noBorder()
                ninePatchBackground(ninePatches["selected"])
                textColor(Color.WHITE)
            }
        }
    }

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoNinePatch"
            scene(600, 400) {
                theme(myTheme)

                root = demoPanel {
                    name = "DemoNinePatch"
                    about = ABOUT

                    content = vBox {
                        section = true
                        padding(6)
                        fillWidth = true
                        spacing = 6f

                        + label("These buttons use 'Nine Patches' as the 'background'")

                        + hBox {
                            + button("Button")
                        }
                        + hBox {
                            + toggleButton("Abc")
                            + toggleButton("Xyz")
                        }
                        + hBox {
                            toggleGroup{
                                + toggleButton("Toggle A")
                                + toggleButton("Toggle B")
                            }
                        }
                        + hBox {
                            toggleGroup {
                                + radioButton2("AM")
                                + radioButton2("FM")
                                + radioButton2("Long Wave")
                            }
                        }
                        + Label("Please forgive the images, I'm a coder, not an artist!")
                    }
                }
            }

            show()
        }
    }

    companion object {
        val ABOUT = """
            A demonstration of using [NinePatch]es for [Button] [Background]s.
            
            This is also a simple example of how to extend a [Theme] using [Theme.combineWith()].

            NOTE. This theme is no longer customisable.
            If you alter any of Tantalum's properties, the Tantalum theme will update,
            but `myTheme` will still refer to the old Tantalum theme, not the customised version.            
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoNinePatch::class)
        }
    }

}

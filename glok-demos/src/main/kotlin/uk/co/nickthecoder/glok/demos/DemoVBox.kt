package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.demos.DemoVBox.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.alignmentProperty
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoVBox : Application() {

    val mainAlignmentProperty by alignmentProperty(Alignment.TOP_LEFT)
    var mainAlignment by mainAlignmentProperty

    val mainFillWidthProperty by booleanProperty(false)
    var mainFillWidth by mainFillWidthProperty

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoVBox"
            scene(900, 700) {
                root = demoPanel {
                    name = "DemoHBox"
                    about = ABOUT

                    content = hBox {
                        spacing = 10f
                        padding = Edges(10f)
                        fillHeight = true

                        vBoxes()

                        + vBox {
                            spacing = 10f
                            section = true

                            + Label("Alignment")
                            + hBox {
                                spacing = 10f
                                alignmentControls(
                                    Alignment.TOP_LEFT,
                                    Alignment.CENTER_LEFT,
                                    Alignment.BOTTOM_LEFT
                                )
                                alignmentControls(
                                    Alignment.TOP_CENTER,
                                    Alignment.CENTER_CENTER,
                                    Alignment.BOTTOM_CENTER
                                )
                                alignmentControls(
                                    Alignment.TOP_RIGHT,
                                    Alignment.CENTER_RIGHT,
                                    Alignment.BOTTOM_RIGHT
                                )
                            }

                            + separator()

                            + checkBox("Fill Width ?") {
                                selectedProperty.bidirectionalBind(mainFillWidthProperty)
                            }
                        }

                    }
                }
            }
            show()
        }
    }

    /**
     * These are the red/white/blue boxes that we are testing.
     */
    private fun NodeParent.vBoxes() {
        + vBox {
            growPriority = 0.5f
            shrinkPriority = 0.5f
            borderSize = Edges(1f)
            plainBorder(Color.GRAY)
            alignmentProperty.bindTo(mainAlignmentProperty)
            fillWidthProperty.bindTo(mainFillWidthProperty)
            + pane {
                plainBackground(Color.RED)
                overridePrefWidth = 50f
                overridePrefHeight = 150f
                shrinkPriority = 1f
            }
            + pane {
                plainBackground(Color.WHITE)
                overridePrefWidth = 150f
                overridePrefHeight = 150f
            }
            + pane {
                plainBackground(Color.BLUE)
                overridePrefWidth = 100f
                overridePrefHeight = 150f
            }
        }
        + vBox {
            growPriority = 0.5f
            shrinkPriority = 0.5f
            borderSize = Edges(1f)
            plainBorder(Color.GRAY)
            alignmentProperty.bindTo(mainAlignmentProperty)
            fillWidthProperty.bindTo(mainFillWidthProperty)
            + pane {
                plainBackground(Color.RED)
                overridePrefWidth = 50f
                overridePrefHeight = 150f
                growPriority = 1f // Twice as much as WHITE
                shrinkPriority = 1f
            }
            + pane {
                plainBackground(Color.WHITE)
                overridePrefWidth = 150f
                overridePrefHeight = 150f
                growPriority = 0.5f // Half as much as RED
            }
            + pane { // Does not grow
                plainBackground(Color.BLUE)
                overridePrefWidth = 100f
                overridePrefHeight = 150f
            }
        }
    }

    private fun NodeParent.alignmentControls(vararg alignment: Alignment) {
        + vBox {
            spacing = 10f
            for (pos in alignment) {
                + propertyRadioButton(mainAlignmentProperty, pos, pos.name.lowercase()) {
                    growPriority = 1f
                    shrinkPriority = 1f
                }
            }
        }
    }


    companion object {
        val ABOUT = """
            This Demo isn't a HOW-TO, instead it let's you change the important properties
            of a HBox, so that you can see their effect.
            
            The boxes on the left have `growPriority` of 0, and therefore do not grow,
            and the vertical alignment has an effect.
            
            The boxes in the middle DO grow (at different rates), and therefore always
            fill the entire vertical space, so the vertical alignment has no effect.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoVBox::class)
        }
    }
}

/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.FindAndReplace
import uk.co.nickthecoder.glok.text.FindAndReplaceActions
import uk.co.nickthecoder.glok.text.FindBar
import uk.co.nickthecoder.glok.text.ReplaceBar

class DemoFindReplace : Application() {

    val findAndReplace = FindAndReplace()

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoFindReplace"

            scene = scene(1000, 600) {
                root = demoPanel {
                    name = "DemoFindReplace"
                    about = ABOUT
                    relatedDemos.addAll(DemoTextArea::class, DemoStyledTextArea::class)

                    content = borderPane {
                        findAndReplace.commands.attachTo(this)
                        top = vBox {
                            fillWidth = true

                            + menuBar {
                                + menu("File") {
                                    + menuItem("Save (not implemented)") { }
                                }
                                + menu("Edit") {
                                    findAndReplace.commands.build {
                                        with(FindAndReplaceActions) {
                                            + menuItem(SHOW_FIND)
                                            + menuItem(SHOW_REPLACE)
                                            + menuItem(SHOW_GOTO_DIALOG)
                                        }
                                    }
                                }
                            }
                            + FindBar(findAndReplace)
                            + ReplaceBar(findAndReplace)
                        }
                        center = styledTextArea(TEXT) {
                            section = true
                            findAndReplace.styledTextArea = this
                        }
                    }
                }
            }
            show()
            findAndReplace.styledTextArea?.requestFocus()
        }
    }

    companion object {
        val TEXT = """
            Ctrl+F = Find
            Ctrl+R = Replace
            Ctrl+G = Go To...
            ESCAPE = Close both toolbars.
            
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat. Duis aute irure dolor in reprehenderit
            in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa qui officia deserunt mollit anim id est
            laborum.
            """.trimIndent()

        val ABOUT = """
            Using [FindAndReplace], [FindBar] and [ReplaceBar].
            [FindAndReplace] does all the hard work, while [FindBar] and
            [ReplaceBar] provide the GUI elements.
            
            If you look at the source code for [FindBar] and [ReplaceBar],
            you will see they are tiny.
            So it is easy to replace the GUI, without needing to touch
            [FindAndReplace], which does all the hard work, but has
            no GUI elements of its own.
            
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTextArea::class)
        }
    }
}

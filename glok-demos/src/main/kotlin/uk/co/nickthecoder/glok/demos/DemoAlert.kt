package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.AlertType
import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.demos.DemoAlert.Companion.ABOUT
import uk.co.nickthecoder.glok.dialog.alert
import uk.co.nickthecoder.glok.dialog.alertDialog
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT]
 */
class DemoAlert : Application() {

    private lateinit var primaryStage: Stage

    private val allowResizableProperty by booleanProperty(false)
    private var allowResizable by allowResizableProperty

    override fun start(primaryStage: Stage) {
        this.primaryStage = primaryStage

        with(primaryStage) {
            title = "DemoAlert"
            scene = scene(900, 600) {

                root = demoPanel {
                    name = "DemoAlert"
                    about = ABOUT
                    relatedDemos.addAll(DemoPromptDialog::class)

                    content = hBox {
                        alignment = Alignment.TOP_CENTER
                        padding(20)
                        spacing(60)

                        + titledPane("alertDialog()") {
                            collapsable = false
                            content = vBox {
                                section = true
                                spacing(6)

                                + button("Information") { onAction { information() } }
                                + button("Error") { onAction { error() } }
                                + button("Scrolled") { onAction { scrolled() } }
                                + button("Scrolled2") { onAction { scrolled2() } }
                            }
                        }

                        + titledPane("alert()") {
                            collapsable = false
                            content = vBox {
                                section = true
                                spacing(6)

                                + button("Warning") { onAction { warning() } }
                                + button("None") { onAction { none() } }
                                + button("Confirmation") { onAction { confirmation() } }
                                + button("Confirmation2") { onAction { confirmation2() } }
                            }
                        }

                        + titledPane("Non Modal Alerts") {
                            collapsable = false
                            content = vBox {
                                section = true
                                spacing(6)
                                + checkBox("Use Overlays?") {
                                    tooltip = TextTooltip(
                                        """
                                    Toggle between 'Native' windows and 'OverlayStage'
                                    This is only possible for non-modal dialogs
                                    (Alas, Glok doesn't support modal dialogs using `Native` windows)
                                    """.trimIndent()
                                    )
                                    selectedProperty.bidirectionalBind(GlokSettings.useOverlayStagesProperty)
                                }
                                + checkBox("Resizable") {
                                    selectedProperty.bidirectionalBind(allowResizableProperty)
                                }

                                + button("Non Modal") {
                                    tooltip = TextTooltip(
                                        """
                                    Click more than once to create many
                                    non-modal dialogs.
                                    """.trimIndent()
                                    )
                                    onAction { nonModal() }
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    private fun information() {
        // This is the verbose way to do it (which I prefer)...
        alertDialog(AlertType.INFORMATION) {
            title = "About"
            heading = "About"
            message = "Glok was written by Nick Robinson"
            buttonTypes(ButtonType.CLOSE)
            createStage(primaryStage, StageType.MODAL) {
                textArea.prefRowCount = 1
                show()
            }
        }
    }

    private fun error() {
        // Using a convenience method, which gets the job done in one go.
        // But we cannot change other attributes, such as resizable.
        alert(primaryStage, "Error!", "Error!", "Error message", AlertType.ERROR, listOf(ButtonType.OK)) {
            println("Resulting button type : $it")
        }
    }

    private fun warning() {
        alert(primaryStage, "Warning", "Warning", "Warning message", AlertType.WARNING, listOf(ButtonType.OK)) {
            println("Resulting button type : $it")
        }
    }

    private fun none() {
        // `alert` shows the whole message, without scroll bars! Beware!
        alert(primaryStage, "Hello", "Hello",
            "No icon, but the message is very long, just to show what would happen with long lines of text.",
            AlertType.NONE, listOf(ButtonType.OK)
        ) {
            println("Resulting button type : $it")
        }
    }

    private fun confirmation() {
        alert(primaryStage, "Exit?", "Exit ?", "Are you sure you want to exit?", AlertType.CONFIRMATION,
            listOf(ButtonType.YES, ButtonType.NO)
        ) { reply ->
            // This time we care about the `reply`...
            if (reply == ButtonType.YES) {
                primaryStage.close()
            }
        }
    }

    private fun confirmation2() {
        alert(primaryStage, "Exit ?", "Exit ?", "Are you sure you want to exit?", AlertType.CONFIRMATION,
            listOf(ButtonType.OK, ButtonType.CANCEL)
        ) { reply ->
            // This time we care about the `reply`...
            if (reply == ButtonType.OK) {
                primaryStage.close()
            }
        }
    }

    private fun scrolled() {
        // We NEED to do the verbose way, because the terse way tries to display the WHOLE message.
        alertDialog(AlertType.INFORMATION) {
            title = "Information"
            heading = "Information"
            message = LOREM
            buttonTypes(ButtonType.CLOSE)
            createStage(primaryStage, StageType.MODAL) {
                textArea.prefColumnCount = 32
                textArea.prefRowCount = 5
                show()
            }
        }
    }
    /**
     * Like [scrolled], but using pixels rather than row/columns :
     */
    private fun scrolled2() {

        alertDialog(AlertType.INFORMATION) {
            title = "Information"
            heading = "Information"
            message = LOREM
            buttonTypes(ButtonType.CLOSE)
            createStage(primaryStage, StageType.MODAL) {
                textArea.overridePrefWidth = 300f
                textArea.overridePrefHeight = 200f
                show()
            }
        }
    }

    private fun nonModal() {
        alertDialog(AlertType.INFORMATION) {
            title = "Non Modal"
            heading = "Information"
            message = LOREM
            resizable = allowResizable // You would normally choose simply `true` or `false` (default = false)
            buttonTypes(ButtonType.CLOSE)
            createStage(primaryStage, StageType.NORMAL) {
                textArea.prefColumnCount = 32
                textArea.prefRowCount = 5
                show()
            }
        }
    }

    companion object {
        val ABOUT = """
            Demonstrates various alert dialogs.
            Some of them use the [alert()] function.
            And others use the more verbose [alertDialog()] function.
    
            The latter gives access to the [AlertDialog] instance, so
            you can tinker with its properties.
    
            Note, [AlertDialog] is a subclass of [Dialog],
            with a Label (heading) and a TextArea (message).
            
            The buttons on the right let you toggle between `Native`
            and `Overlay` windows for non-modal stages.
            Click `Non-Modal`, move the popup and click `Non-Modal` again.

            For more complex dialogs, create your own subclass of [Dialog].    
            """.trimIndent()

        val LOREM = """
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat. Duis aute irure dolor in reprehenderit
            in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa qui officia deserunt mollit anim id est
            laborum …
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoAlert::class)
        }
    }
}

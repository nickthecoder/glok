package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demos.DemoTitledPane.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.ALT_BACKGROUND

/**
 * See [ABOUT]
 */
class DemoTitledPane : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "TitledPane Demo"
            scene(800, 500) {
                root = demoPanel {
                    name = "DemoTitledPane"
                    about = ABOUT

                    content = splitPane {
                        + vBox {
                            fillWidth = true
                            spacing = 10f

                            + titledPane("Hello") {
                                content = lines("The TitledPanes on the left\nare independent of each other.")
                            }
                            + titledPane("World") {
                                content = lines("You can open and close them\nindependently.")
                            }
                            + titledPane("Alternate colors") {
                                style(ALT_BACKGROUND)
                                content =
                                    lines("This TitledPane uses style :\n    .alt_background\n\nSo the colors of the title and the content\nare reversed")
                            }
                        }

                        + vBox {
                            fillWidth = true
                            toggleGroup {
                                + accordionPane("A") {
                                    expanded = true
                                    content = lines("When A is expanded,\nB and C are contracted")
                                }

                                + accordionPane("B") {
                                    content = lines("When B is expanded,\nA and C are contracted")
                                }

                                + accordionPane("C") {
                                    content = lines("When C is expanded,\nA and B are contracted")
                                }
                            }
                        }
                    }
                }
            }

            show()
        }
    }

    private fun lines(text: String) = vBox {
        padding(10)
        for (line in text.split("\n")) {
            + label(line)
        }
    }

    companion object {
        val ABOUT = """
            Demonstrates the use of [TitledPane]s.
            
            There are two columns. The first column, each [TitledPane] is independent.
            
            In the second column, the [TitlePane]s are coordinated via a [ToggleGroup].
            When one [TitledPane] is expanded, the other automatically contract.
            
            We use the method [accordionPane] in the second column.
            It does the same as [titledPane] (it creates a [TitledPane]),
            but in addition, it sets to [TitledPane]'s toggle group.
            Also, [expanded = false] by default when using [accordionPane].
            
            FYI, the name [accordionPane] is taken from JavaFX, which has a
            separate `AccordionPane` class. Glok only has one [TitledPane] class, which
            fulfils both roles.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTitledPane::class)
        }
    }

}

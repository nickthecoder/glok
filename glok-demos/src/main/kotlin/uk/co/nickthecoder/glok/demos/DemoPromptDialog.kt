/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demos.DemoAlert.Companion.ABOUT
import uk.co.nickthecoder.glok.demos.DemoPromptDialog.Companion.ABOUT
import uk.co.nickthecoder.glok.dialog.promptDialog
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT]
 */
class DemoPromptDialog : Application() {

    private lateinit var primaryStage: Stage

    override fun start(primaryStage: Stage) {
        this.primaryStage = primaryStage

        with(primaryStage) {
            title = "PromptDialog Demo"
            scene = scene(900, 600) {

                root = demoPanel {
                    name = "DemoPromptDialog"
                    about = ABOUT
                    relatedDemos.addAll(DemoAlert::class)

                    content = vBox {
                        section = true
                        fillWidth = false
                        alignment = Alignment.CENTER_CENTER
                        padding(20)
                        spacing(20)

                        + button("Simple") { onAction { simple() } }
                        + button("With Heading") { onAction { withHeading() } }
                        + button("Heading and Label") { onAction { headingAndLabel() } }
                    }

                }
            }
            show()
        }
    }

    private fun simple() {
        promptDialog {
            title = "Enter some text"
            show(primaryStage) { text ->
                println("You entered : $text ")
            }
        }
    }

    private fun withHeading() {
        promptDialog {
            title = "Enter some text"
            heading = "This is the heading"
            show(primaryStage) { text ->
                println("You entered : $text ")
            }
        }
    }

    private fun headingAndLabel() {
        promptDialog {
            title = "Enter some text"
            heading = "This is the heading"
            above = "Appears above the TextField"
            show(primaryStage) { text ->
                println("You entered : $text ")
            }
        }
    }


    companion object {
        val ABOUT = """
            How to use [PromptDialog] class via the [promptDialog] function.
            
            [heading] and [above] are optional String properties.
            If they are blank, the corresponding node is made invisible.

            NOTE. [show]'s lambda is only called when OK is pressed.
            
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoAlert::class)
        }
    }
}

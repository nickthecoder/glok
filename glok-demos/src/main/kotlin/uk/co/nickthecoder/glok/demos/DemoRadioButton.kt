package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

class DemoRadioButton : Application() {

    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "RadioButton Demo"
            scene = scene(800, 600) {
                root = demoPanel {
                    name = "DemoRadioButton"
                    about = ABOUT
                    relatedDemos.addAll(DemoCheckBox::class)

                    content = scrollPane {

                        content = formGrid {

                            + rowHeading("PropertyRadioButton")
                            radioChoices(Tantalum.darkProperty) {
                                + row {
                                    left = propertyRadioButton(true, "Dark")
                                    right = propertyRadioButton(false, "Light")
                                }
                                + row {
                                    above = information("These cannot get out of sync.")
                                }
                            }

                            + rowSeparator()

                            + rowHeading("Alternate Style")
                            radioChoices(Tantalum.darkProperty) {
                                + row {
                                    left = propertyRadioButton2(true, "Dark")
                                    right = propertyRadioButton2(false, "Light")
                                }
                                + row {
                                    above = information("These are also PropertyRadioButtons, but themed differently.")
                                    below = information("This style is often used in toolbars.")
                                }
                            }

                            + rowSeparator()

                            + rowHeading("Traditional RadioButtons (avoid)")
                            toggleGroup {
                                + row {
                                    left = radioButton("Dark") {
                                        onAction { Tantalum.dark = true }
                                    }
                                    right = radioButton("Light") {
                                        onAction { Tantalum.dark = false }
                                    }
                                }
                                + row {
                                    above = information("These can get out of sync. See About for details.")
                                }
                            }

                        }
                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            Demonstrates [RadioButton] and [PropertyRadioButton].
            
            [RadioButton]s are connected together using a [ToggleGroup],
            and only one button in the group can be selected at any time.
            
            Unlike a [ToggleButton], clicking on a selected [RadioButton] does nothing.
            In this demo, there is only 2 RadioButtons in the ToggleGroup, but you can have
            as many as required.
            
            I tend not to use [RadioButton], I prefer to use [PropertyRadioButton] instead. YMMV.
            They look, and feel identical, but are implemented differently.
            
            [PropertyRadioButton]s are connected together using a [Property], rather than
            a [ToggleGroup]. Each [PropertyRadioButton] has a value, and clicking it
            will set the [Property] value.
            The `selected` property is `true` iff the [PropertyRadioButton]'s property value is the
            same as the [PropertyRadioButton]'s value.
            Therefore, we can change the selected state of the buttons by changing the [Property]'s value.

            NOTE. The (old fashioned) [RadioButton]s in this demo deliberately don't fully work.
            When we change [Tantalum.darkProperty], the [RadioButton]s don't change state.
            To fix this, we would need to add change listeners on every [RadioButton]. A PITA!
            
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoRadioButton::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.DoubleSnapEvery
import uk.co.nickthecoder.glok.control.FloatSnapEvery
import uk.co.nickthecoder.glok.control.IntSnapEvery
import uk.co.nickthecoder.glok.demos.DemoSlider.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.doubleProperty
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.functions.format
import uk.co.nickthecoder.glok.property.functions.plus
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.util.FixedFormat

/**
 * See [ABOUT].
 */
class DemoSlider : Application() {

    val intValueProperty by intProperty(0)
    val floatValueProperty by floatProperty(0f)
    val doubleValueProperty by doubleProperty(0.0)

    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "Slider Demo"
            scene(900, 600) {
                root = demoPanel {
                    name = "DemoSlider"
                    about = ABOUT
                    relatedDemos.addAll(DemoSpinner::class)

                    content = hBox {
                        fillHeight = true

                        + toolBar {
                            side = Side.LEFT
                            + intSlider {
                                orientation = Orientation.VERTICAL
                                min = - 50
                                max = 50
                                snap = IntSnapEvery(25, 5)
                                overridePrefHeight = 300f

                                markers.addAll(0)
                                minorMarkers.addAll(- 25, 25)
                                valueProperty.bidirectionalBind(intValueProperty)
                            }
                        }

                        + toolBar {
                            side = Side.LEFT
                            + floatSlider {
                                orientation = Orientation.VERTICAL
                                min = - 50f
                                max = 50f
                                snap = FloatSnapEvery(25f, 5f)
                                overridePrefHeight = 300f

                                markers.addAll(0f)
                                minorMarkers.addAll(- 25f, 25f)
                                valueProperty.bidirectionalBind(floatValueProperty)
                            }
                        }

                        + toolBar {
                            side = Side.LEFT
                            + doubleSlider {
                                orientation = Orientation.VERTICAL
                                min = - 50.0
                                max = 50.0
                                snap = DoubleSnapEvery(25.0, 5.0)
                                overridePrefHeight = 300f

                                markers.addAll(0.0)
                                minorMarkers.addAll(- 25.0, 25.0)
                                valueProperty.bidirectionalBind(doubleValueProperty)
                            }
                        }


                        + toolBar {
                            side = Side.LEFT
                            + colorSlider {
                                orientation = Orientation.VERTICAL
                                fromColor = Color.RED
                                toColor = Color.BLUE
                                ratio = 0.5
                                overridePrefHeight = 300f
                            }

                        }

                        + vBox {
                            fillWidth = true
                            growPriority = 1f

                            + toolBar {

                                + intSlider {
                                    min = - 50
                                    max = 50
                                    snap = IntSnapEvery(25, 5)

                                    markers.addAll(0)
                                    minorMarkers.addAll(- 25, 25)
                                    valueProperty.bidirectionalBind(intValueProperty)
                                }

                                + intSpinner(0) {
                                    min = - 50
                                    max = 50
                                    editor.prefColumnCount = 3
                                    valueProperty.bidirectionalBind(intValueProperty)
                                }

                                + label("") {
                                    textProperty.bindTo(intValueProperty.toObservableString() + " (int)")
                                }
                            }

                            + toolBar {

                                + floatSlider {
                                    min = - 50f
                                    max = 50f
                                    snap = FloatSnapEvery(25f, 5f)

                                    markers.addAll(0f)
                                    minorMarkers.addAll(- 25f, 25f)
                                    valueProperty.bidirectionalBind(floatValueProperty)
                                }

                                + floatSpinner(0f) {
                                    min = - 50f
                                    max = 50f
                                    valueProperty.bidirectionalBind(floatValueProperty)
                                    editor.prefColumnCount = 5
                                }

                                + label("") {
                                    textProperty.bindTo(floatValueProperty.format(FixedFormat(2)) + " (float)")
                                }
                            }

                            + toolBar {

                                + doubleSlider {
                                    min = - 50.0
                                    max = 50.0
                                    snap = DoubleSnapEvery(25.0, 5.0)

                                    markers.addAll(0.0)
                                    minorMarkers.addAll(- 25.0, 25.0)
                                    valueProperty.bidirectionalBind(doubleValueProperty)
                                }

                                + doubleSpinner(0.0) {
                                    min = - 50.0
                                    max = 50.0
                                    valueProperty.bidirectionalBind(doubleValueProperty)
                                    editor.prefColumnCount = 5
                                }

                                + label("") {
                                    textProperty.bindTo(doubleValueProperty.format(FixedFormat(2)) + " (double)")
                                }
                            }

                            + toolBar {
                                + colorSlider {
                                    fromColor = Color.WHITE
                                    toColor = Color.BLACK
                                    overridePrefWidth = 400f
                                }
                            }
                        }

                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
            Demonstrates [FloatSlider]s, [IntSlider]s and [ColorSlider]s.
            
            Both the [FloatSlider] and [IntSlider] have a range of -50 to 50,
            and have a "snap" every 25 units, with a threshold of 5.
            Meaning values between -5..5 and 20..30 etc. will be snapped to 0, 25 etc.
            You are encouraged to implement your own [Snap] implementations,
            if the built-in implementations aren't sufficient.
            
            3 markers are added, which correspond to the snap points.
            However, the markers and the snap are completely independent.
            
            [ColorSlider]s perform a simple lerp between the `fromColor` and `toColor`.
            Look at [CustomColorPicker] for how [ColorSlider] can be (ab)used to pick
            colors using HSV instead of RGB.
            
            Note the button which sets the first slider's value +outside+ of the valid range.
            There is no validation check. Beware!

            Not demonstrated in this Demo is [Slider2d], created specifically for
            the [CustomColorPicker].
            However, this could be useful in other situations. e.g. place it over an
            ImageView to pick a point on the image???
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoSlider::class)
        }
    }

}

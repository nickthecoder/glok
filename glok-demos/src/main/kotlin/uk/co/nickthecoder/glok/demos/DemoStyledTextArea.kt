package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.StyledTextArea
import uk.co.nickthecoder.glok.control.Text
import uk.co.nickthecoder.glok.property.boilerplate.IndentationTernaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.PlainBackground
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.UnderlineBackground
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.Indentation
import uk.co.nickthecoder.glok.text.ThemedHighlight

class DemoStyledTextArea : Application() {

    lateinit var textArea: StyledTextArea

    val showLineNumbersProperty by booleanProperty(false)

    val indentationColumnsProperty by intProperty(4)
    val indentWithTabsProperty by booleanProperty(false)
    val behaveLikeTabsProperty by booleanProperty(true)

    val indentationProperty =
        IndentationTernaryFunction(indentationColumnsProperty, indentWithTabsProperty, behaveLikeTabsProperty)
        { columns, tabs, clever ->
            if (tabs) Indentation.tabIndentation(columns) else Indentation.spacesIndentation(columns, clever)
        }

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoStyledTextArea"
            scene(1000, 600) {
                root = demoPanel {
                    name = "DemoStyledTextArea"
                    about = ABOUT
                    relatedDemos.addAll(DemoTextArea::class, DemoFindReplace::class)

                    content = borderPane {
                        center = styledTextArea {
                            section = true
                            textArea = this
                            indentationProperty.unbind()
                            indentationProperty.bindTo(this@DemoStyledTextArea.indentationProperty)
                            showLineNumbersProperty.bindTo(this@DemoStyledTextArea.showLineNumbersProperty)
                            setSimpleStyledText(EXAMPLE_TEXT, STYLES)
                        }
                        bottom = toolBar {
                            + button("Clear") {
                                onAction { textArea.clear() }
                            }
                            + label("Indentation : ")
                            + checkBox("Use Tabs?") {
                                selectedProperty.bidirectionalBind(indentWithTabsProperty)
                            }
                            + label("Columns")
                            + intSpinner {
                                min = 1
                                max = 8
                                editor.prefColumnCount = 3
                                valueProperty.bidirectionalBind(indentationColumnsProperty)
                            }
                            + checkBox("Behave Like Tabs?") {
                                visibleProperty.bindTo(! indentWithTabsProperty)
                                selectedProperty.bidirectionalBind(behaveLikeTabsProperty)
                            }
                            + spacer()
                            + toggleButton("Line Numbers") {
                                selectedProperty.bidirectionalBind(showLineNumbersProperty)
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {

        /**
         * A [Highlight] which does not use themes. Here we add a red underline.
         */
        val errorHighlight = object : Highlight {
            override fun style(text: Text) {
                text.background = UnderlineBackground()
                text.backgroundColor = Color["#e33"]
            }
        }

        /**
         * A [Highlight] which changes the background color, but leaves the text unaltered.
         */
        val importantHighlight = object : Highlight {
            override fun style(text: Text) {
                // NOTE, always use semi-transparent colors for the background, otherwise you won't be
                // able to see the selection background color (which is lower in the z-order).
                text.background = PlainBackground
                text.backgroundColor = Color["#e334"]
            }
        }

        val STYLES = mapOf(
            '`' to ThemedHighlight(".italic"),
            '+' to ThemedHighlight(".bold"),
            '_' to ThemedHighlight(".underline"),
            '|' to ThemedHighlight(".strikethrough"),
            '?' to errorHighlight,
            '!' to importantHighlight
        )

        val ABOUT = """            
            Demonstrates the use of [StyledTextArea], which is similar to [TextArea],
            with the ability to style parts of the text using [HighlightRange]s.
            """.trimIndent()

        val EXAMPLE_TEXT = """            
            _+StyledTextArea
            +_This should NOT be styled.
            
            This line contains `italic` text.
            This line contains +bold+ text.
            _Underlined_ text uses the special `UnderlineBackground`.
            UnderlineBackground can also be (ab)used to |strikethrough| text.
            
            See `errorHighlight` for how to style text without using Themes,
            maybe for ?error? highlighting.
            
            We can !change the background! too (with or without using a Theme).
            Styles can be combined, such as _underline +and bold+ together_.
            They _do +not_ have to be nested+ (but I don't recommend it). 
            
            See the bottom of the `Source Code` tab for source of this document.
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoStyledTextArea::class)
        }
    }
}

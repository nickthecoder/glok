package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Button
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.demos.DemoThreeRow.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.VAlignment
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoThreeRow : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoThreeRow"
            scene(800, 500) {
                root = demoPanel {
                    name = "DemoThreeRow"
                    about = ABOUT
                    relatedDemos.addAll(DemoHBox::class)

                    content = splitPane {
                        + vBox {
                            fillWidth = true
                            section = true

                            + threeRow {
                                padding(10)
                                vAlignment = VAlignment.TOP
                                left = Label("Left")
                                center = Button("Center")
                                right = Label("Right")
                            }

                            + textArea("""
                                Notice the different that vAlignment makes.
                                
                                Below, 'left' is not set
                                """.trimIndent()
                            ) {
                                growPriority = 1f
                                readOnly = true
                            }

                            + threeRow {
                                padding(10)
                                vAlignment = VAlignment.BOTTOM

                                center = Button("Center")
                                right = Label("Right")
                            }

                        }
                        + vBox {
                            section = true
                            fillWidth = true

                            + threeRow {
                                padding(10)
                                center = Button("Center Only")
                            }

                            + textArea("""
                                    On this half, we could have
                                    used a HBox instead!
                                    
                                    Center aligned above
                                    
                                    With a spacer() below
                                    
                                    """.trimIndent()
                            ) {
                                readOnly = true
                                growPriority = 1f
                            }

                            + threeRow {
                                padding(10)
                                left = Label("Just left")
                                right = Label("and right")
                            }
                        }
                    }
                }

            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            A [ThreeRow], as the name suggests, is a row of three [Node]s.
            The `center` [Node] is guaranteed to be centered,
            regardless of the sizes of `left` and `right`.
            
            There are four of them on show, with various combinations of
            left/center/right present.
            
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoThreeRow::class)
        }
    }

}

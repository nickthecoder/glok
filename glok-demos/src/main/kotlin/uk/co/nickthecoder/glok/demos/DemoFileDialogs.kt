package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.ExtensionFilter
import uk.co.nickthecoder.glok.control.FileDialog
import uk.co.nickthecoder.glok.demos.DemoFileDialogs.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.borderPane
import uk.co.nickthecoder.glok.scene.dsl.button
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import java.io.File

/**
 * See [ABOUT].
 */
class DemoFileDialogs : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoFileDialogs"

            scene(600, 400) {
                root = demoPanel {
                    name = "DemoFileDialogs"
                    about = ABOUT

                    content = borderPane {
                        top = toolBar {
                            + button("Open") { onAction { open(this) } }
                            + button("Save") { onAction { save(this) } }
                            + button("Pick Folder") { onAction { folderPicker(this) } }
                        }
                    }
                }
            }
            show()
        }
    }

    fun open(from: Node) {
        val parentStage = from.scene?.stage ?: return

        FileDialog().apply {
            title = "Open File"
            extensions.addAll(
                ExtensionFilter("Text Files", "*.txt"),
                ExtensionFilter("Images", "*.png", "*.jpg", "*.jpeg"),
                ExtensionFilter("All Files", "*")
            )
            showOpenDialog(parentStage) { file: File? ->
                println("Result = $file")
            }
        }
    }

    fun save(from: Node) {
        val parentStage = from.scene?.stage ?: return

        FileDialog().apply {
            title = "Save File"
            extensions.addAll(
                ExtensionFilter("Text Files", "*.txt"),
                ExtensionFilter("Images", "*.png", "*.jpg", "*.jpeg"),
                ExtensionFilter("All Files", "*")
            )
            showSaveDialog(parentStage) { file: File? ->
                println("Result = $file")
            }
        }
    }

    fun folderPicker(from: Node) {
        val parentStage = from.scene?.stage ?: return

        FileDialog().apply {
            title = "Select a Folder"
            showFolderPicker(parentStage) { file: File? ->
                println("Result = $file")
            }
        }
    }

    companion object {
        val ABOUT = """
            Demonstrates the use of [FileDialog].
            
            There are three methods :
            
              1. `showSaveDialog`
              2. `showOpenDialog`
              3. `showFolderPicker`
            
            Note. These is the option to use `native` controls, or `Glok` controls.
            
            Initially, only `native` controls were possible, but I added `Glok` controls,
            and made this the default because :
            
              1. The native library seems to have a critical bug when running on Linux.
                 It very occasionally crashes the application :-(
              2. The native dialogs really annoy me - they ignore the [initialDirectory]
                 I've read somewhere that this is deliberate, because users were confused
                 by this feature. WTF? So now I have to navigate to the same folder
                 again and again just to Save-As with a new name? No!
              3. The native library's API is dreadful, and hints that it was written
                 by a very poor programmer. I don't trust it!
              4. Filtering is a little naff. You can only filter by extension names,
                 and not by general wildcard patterns.
            
            The downsides of the Glok controls :
            
              1. Looks and behaves differently from the native controls
              2. Doesn't include `favourite` places.
              
            But has some advantages too :
            
              1. Uses the same Theme as your application.
              2. Allows you to browse a folder using the file-manager (via a right-click menu).
            
            The native controls look different depending on your platform (Window, MacOS, Linux).
            As they are `native`, they are not affected by your [Theme]
            (try switching between dark/light theme using the hamburger button in the top right).
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoFileDialogs::class)
        }
    }
}

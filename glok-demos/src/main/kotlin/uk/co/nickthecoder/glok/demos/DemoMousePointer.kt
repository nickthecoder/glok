package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.splitPane

class DemoMousePointer : Application() {

    override fun start(primaryStage: Stage) {

        with(primaryStage) {
            title = "Mouse Pointer Demo"
            scene(600, 400) {
                root = demoPanel {
                    name = "DemoMousePointer"
                    about = ABOUT

                    content = splitPane {
                        + label("Cross Hair") {
                            onMouseEntered { primaryStage.mousePointer = MousePointer.CROSS_HAIR }
                            onMouseExited { primaryStage.mousePointer = MousePointer.DEFAULT }
                        }
                        + label("Hand") {
                            onMouseEntered { primaryStage.mousePointer = MousePointer.HAND }
                            onMouseExited { primaryStage.mousePointer = MousePointer.DEFAULT }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
        How to change the mouse pointer.
        The [mousePointerProperty] is on [Stage].
        
        We can change the cursor on a per-Node basis
        using [onMouseEntered] for any [Node].
        Any nodes which change the mouse pointer should
        reset it to DEFAULT in [onMouseExited].
        """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoMousePointer::class)
        }
    }

}

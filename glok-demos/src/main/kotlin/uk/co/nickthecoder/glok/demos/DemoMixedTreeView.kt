package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.MixedTreeItem
import uk.co.nickthecoder.glok.control.MixedTreeView
import uk.co.nickthecoder.glok.control.TextMixedTreeCell
import uk.co.nickthecoder.glok.demos.DemoMixedTreeView.Companion.ABOUT
import uk.co.nickthecoder.glok.demos.DemoTreeView.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Font
import uk.co.nickthecoder.glok.text.FontStyle

/**
 * See [ABOUT].
 */
class DemoMixedTreeView : Application() {

    override fun start(primaryStage: Stage) {

        val mixedTreeView = mixedTreeView<BaseItem> {
            section = true
            // In case we wish to use a Theme to style this MixedTreeView
            styles.add("fonts-tree")
            // Glok currently assumes that all rows are the same height.
            // When the cells are simple, this isn't an issue. But in this example, we
            // are using a different font for each cell label. This could make the rows
            // different heights. So let's force a particular height.
            // Note, we could also use a Theme to set this, rather than hard-coding it here.
            fixedRowHeight = 30f

            // See below for the implementation of AllFontFamilies.
            root = AllFontFamilies()
        }

        with(primaryStage) {
            title = "MixedTreeView Demo"
            scene = scene(700, 400) {
                root = demoPanel {
                    name = "DemoMixedTreeView"
                    about = ABOUT
                    relatedDemos.addAll(DemoTreeView::class, DemoListView::class, DemoListViewCustomCells::class)

                    content = borderPane {

                        top = toolBar {
                            + toggleButton("Show Root") {
                                selectedProperty.bidirectionalBind(mixedTreeView.showRootProperty)
                            }
                        }

                        center = mixedTreeView
                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            Demonstrates a _MixedTreeView_.
            
            Understand how a +TreeView+ works before learning about +MixedTreeView+.
            
            Unlike a +TreeView+, a +MixedTreeView+ can have different types
            for each item in the tree.
            In this case, we have 3 implementations :
            
            1. AllFontFamilies (for the root node). It has no data.
            2. FontFamilyItem. Has a String, such as "Arial" for a font-family.
            3. FontItem. Has a font-family as well as a FontSize.
            
            Each +MixedTreeView+ implementation has a +createCell+ method.
            This is in contrast to +TreeView+, where the +TreeView+ controls
            the creation of all cells.
            
            It is possible to get the same effect with a +TreeView+,
            but the implementation would be messier.
            
            In general if items in a tree are all the same basic type, then
            use +TreeView+ (e.g. a directory structure, were each item
            represents a +File+.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoMixedTreeView::class)
        }
    }
}

/**
 * A base class for common data/behavior for all items in our MixedTreeView.
 * In this example, there is nothing in common, so this class is empty.
 */
abstract class BaseItem : MixedTreeItem<BaseItem>()

/**
 * The root node. Its children are [FontFamilyItem]s.
 */
class AllFontFamilies() : BaseItem() {

    init {
        for (family in Font.allFontFamilies()) {
            children.add(FontFamilyItem(family))
        }
        expanded = true
    }

    override fun createCell(treeView: MixedTreeView<BaseItem>) = TextMixedTreeCell(
        treeView, this, "All Font Families"
    )

}

/**
 * Shows a font _family_, such as Arial, Helvetica etc.
 * Its children are [FontItem]s.
 */
class FontFamilyItem(val family: String) : BaseItem() {
    override fun createCell(treeView: MixedTreeView<BaseItem>) = TextMixedTreeCell(
        treeView, this, family
    ).apply {
        // For symbol-only fonts, we can still read the font name using the tooltip
        tooltip = TextTooltip(family)
        try {
            node.font = Font.create(family, 16f)
        } catch (e: Exception) {
            node.text += " (Using default Font)"
        }
        /*
        We could add a graphic, so that each type of item in the tree looks different.
        Either by setting graphic directly...
        */
        // node.graphic = ...
        /*
           Or using a Theme...
        */
        // node.styles.add("font-family-item")
    }

    init {
        for (style in FontStyle.entries) {
            children.add(FontItem(family, style))
        }
    }
}

/**
 * Shows a font family with a particular [FontStyle] (plain / bold / italic / bold-italic).
 * It is a leaf node, meaning it has no children.
 */
class FontItem(val family: String, val style: FontStyle) : BaseItem() {
    init {
        leaf = true
    }

    override fun createCell(treeView: MixedTreeView<BaseItem>) = TextMixedTreeCell(
        treeView, this, style.name
    ).apply {
        // For symbol-only fonts, we can still read the font name using the tooltip
        tooltip = TextTooltip("$family ${style.name}")
        try {
            node.font = Font.create(family, 16f, style)
        } catch (e: Exception) {
            node.text += "(Using default font)"
        }
        /*
        We could add a graphic, so that each type of item has a different icon :
        Either by setting graphic directly...
        */
        // node.graphic = ...
        /*
           Or using a Theme...
        */
        // node.styles.add("font-style-item")
    }
}

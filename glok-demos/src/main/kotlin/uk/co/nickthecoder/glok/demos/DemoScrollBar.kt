package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.ScrollBar
import uk.co.nickthecoder.glok.demos.DemoScrollBar.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Orientation
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT]
 */
class DemoScrollBar : Application() {

    lateinit var vScrollBar: ScrollBar
    lateinit var hScrollBar: ScrollBar


    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "ScrollBar Demo"

            scene(700, 500) {
                root = demoPanel {
                    name = "DemoScrollBar"
                    about = ABOUT
                    relatedDemos.addAll(DemoScrollPane::class)

                    content = borderPane {
                        center = borderPane {
                            center = textArea(ABOUT) {
                                readOnly = true
                            }
                            right = scrollBar {
                                vScrollBar = this
                                orientation = Orientation.VERTICAL
                                min = - 5f
                                max = 10f
                                visibleAmount = 2f
                            }
                            bottom = scrollBar {
                                hScrollBar = this
                                min = - 5f
                                max = 10f
                                visibleAmount = 6f
                            }
                        }
                        bottom = toolBar {
                            + Label("Values : ")
                            + textField("") {
                                readOnly = true
                                prefColumnCount = 3
                                textProperty.bindTo(hScrollBar.valueProperty.toObservableString())
                            }
                            + Label(",")
                            + textField("") {
                                readOnly = true
                                prefColumnCount = 3
                                textProperty.bindTo(vScrollBar.valueProperty.toObservableString())
                            }
                        }
                    }
                }
            }
            show()
        }

    }


    companion object {
        val ABOUT = """
            A simple demonstration of a [ScrollBar].
            These are rarely used directly, as [ScrollPane] is usually a better solution.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoScrollBar::class)
        }
    }

}

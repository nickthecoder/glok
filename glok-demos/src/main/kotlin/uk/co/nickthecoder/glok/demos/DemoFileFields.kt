package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.acceptDroppedFile
import uk.co.nickthecoder.glok.control.withFileButton
import uk.co.nickthecoder.glok.control.addFileCompletionActions
import uk.co.nickthecoder.glok.control.withFolderButton
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*

class DemoFileFields : Application() {

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoFileFields"
            scene(1000, 600) {
                root = demoPanel {
                    name = "DemoFileFields"
                    about = ABOUT

                    content = formGrid {
                        section = true

                        + row {
                            above = textArea(INSTRUCTIONS) {
                                growPriority = 1f
                                prefRowCount = 10
                                readOnly = true
                            }
                        }
                        // **** This is the interesting part ****
                        + row("Choose a file") {
                            right = textField {
                                growPriority = 1f
                                addFileCompletionActions()
                                acceptDroppedFile()
                                tooltip = TextTooltip("Auto-complete key : DOWN")
                            }.withFileButton("Choose a file")
                        }

                        + row("Choose a folder") {
                            right = textField {
                                growPriority = 1f
                                addFileCompletionActions(foldersOnly = true)
                                acceptDroppedFile()
                                tooltip = TextTooltip("Auto-complete key : DOWN")
                            }.withFolderButton("Choose a folder")
                        }

                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            [TextField]s can be given additional features, when the text is a [File]'s path.
            
            1. Accept files dragged onto them
            2. Filename completion using the down and up arrows
            3. Button(s) on the right which opens a [FileDialog] to choose a file/folder.
            
            Extensions Functions used (package uk.co.nickthecoder.glok.control) :
                withFileCompletion, withFileButton, withFolderButton, withFileAndFolderButtons
            
            """.trimIndent()

        val INSTRUCTIONS = """
            Things to try :
            1. Start typing a path, such as /home/n or C:\program fi
               Then press the DOWN ARROW key to auto-complete the filename.
            2. Press the UP ARROW key to choose the parent folder.
            3. Drag a file/folder onto the fields.
            4. Press the buttons on the right to choose a file/folder in the traditional way.
               
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoFileFields::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.action.Action
import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.demos.DemoActions.DOCUMENT_NEW
import uk.co.nickthecoder.glok.demos.DemoCommands.Companion.ABOUT
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.DOCUMENTS_TAB_BAR
import java.util.*

/**
 * See [ABOUT].
 */
class DemoCommands : Application() {

    private lateinit var stage: Stage

    private val tabPane = TabPane().apply {
        tabBar.styles.add(DOCUMENTS_TAB_BAR)
        tabClosingPolicy = TabClosingPolicy.ALL_TABS
    }

    // In a real application, this property would live on a "settings" Object (singleton).
    // All settings would be loaded/saved at startup/shutdown. e.g. using Java's Preferences class.
    val showStatusBarProperty by booleanProperty(true)
    var showStatusBar by showStatusBarProperty

    val greetingProperty by stringProperty("Hello")
    var greeting by greetingProperty
    val greetingListener = greetingProperty.addChangeListener { _, _, newGreeting ->
        println("Greeting : $newGreeting")
    }

    // Here we concern ourselves with what buttons DO, not where they are.
    private val commands = Commands().apply {
        with(DemoActions) {

            FILE_QUIT { stage.close() }
            DOCUMENT_NEW { onFileNew() }
            DOCUMENT_OPEN { println("Typically you would call FileDialog.showOpenDialog") }
            DOCUMENT_SAVE { onFileSave() }.disable(tabPane.selection.selectedItemProperty.isNull())

            TAB_CLOSE { tabPane.selection.selectedItem?.let { tabPane.tabs.remove(it) } }

            toggle(VIEW_TOGGLE_STATUS_BAR, showStatusBarProperty)

            radioChoices(greetingProperty) {
                choice(GREETING_HELLO, "Hello")
                choice(GREETING_HI, "Hi")
                choice(GREETING_SUP, "'Sup")
            }
        }
    }

    override fun start(primaryStage: Stage) {

        stage = primaryStage
        // Here's the overall structure of our application. Each component is defined elsewhere
        // (usually in a different source file).
        with(primaryStage) {
            title = "DemoCommands"

            scene(800, 600) {
                root = demoPanel {
                    // Enables the Actions' shortcuts while the keyboard focus is anywhere within this Node.
                    commands.attachTo(this)

                    name = "DemoCommands"
                    about = ABOUT

                    content = borderPane {

                        top = vBox {
                            fillWidth = true
                            + createMenuBar(commands)
                            + createToolBar(commands)
                        }

                        center = tabPane

                        bottom = createStatusBar().apply {
                            visibleProperty.bindTo(showStatusBarProperty)
                        }
                    }
                }
            }
            show()
        }
    }

    private fun onFileNew() {
        val newTab = Tab("New Document").apply {
            content = TextArea("This is a new document created at ${Date()}")
        }
        tabPane.tabs.add(newTab)
        tabPane.selection.selectedItem = newTab
    }

    private fun onFileSave() {
        println("Not implemented")
    }

    companion object {

        val ABOUT = """
        Demonstrates the use of [Command], [Commands], [Action], and [Actions].
        
        The object [DemoActions] defines the text, icons and tooltips for the buttons and menus etc.
        Each `val` is an [Action].
        Having them all in one place, rather than spread throughout our application is,
        IMHO much nicer. e.g. it is easy to check for keyboard shortcut clashes.
        
        `DemoCommands.commands` includes a lambda for each [Action]s.
        This is the application logic for HOW to perform that action.
        
        For [ToggleCommand]s (such as `VIEW\_TOGGLE\_STATUS\_BAR`), we provide a [BooleanProperty],
        in this case `showStatusBarProperty`.
        This property is the backing for the [ToggleButton]'s `selectedProperty`
        as well as the [CheckMenuItem]'s `selectedProperty`.
        
        We can then build the GUI (in `createMenuBar`, `createToolBar` and `createStatusBar`),
        very simply and cleanly. There's no tricky logic, only layout.
        
        Notice how the `VIEW\_TOGGLE\_STATUS\_BAR` action can be activated in three ways.
        
        1. Via the `CheckMenuItem` in the `MenuBar`
        2. Via a `ToggleButton` in the `ToolBar`
        3. Via the keyboard shortcut `Ctrl\+Shift\+S`.
        
        The status of the [CheckMenuItem] and the [ToggleButton] is kept in sync automatically
        via `showStatusBarProperty`.
        
        The 3 "greetings" buttons in the toolbar and the 3 corresponding items in the `View` menu
        stay in sync with each other.
        We can also set `greetingProperty` using the shortcut keys `Ctrl+1`, `Ctrl+2` and `Ctrl+3`.
        
        """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoCommands::class)
        }
    }
}

// The layout of each part of our application can have its own source file.
// But here, for simplicity, they are in the same file.
fun createMenuBar(commands: Commands) = menuBar {
    commands.build {
        with(DemoActions) {
            + menu(FILE) {
                + menuItem(DOCUMENT_NEW)
                + menuItem(DOCUMENT_OPEN)
                + menuItem(DOCUMENT_SAVE)
                + menuItem(FILE_QUIT)
            }
            + menu(VIEW) {
                + checkMenuItem(VIEW_TOGGLE_STATUS_BAR)
                + Separator()
                + propertyRadioMenuItem(GREETING_HELLO)
                + propertyRadioMenuItem(GREETING_HI)
                + propertyRadioMenuItem(GREETING_SUP)
            }
            // Not demonstrated here : subMenu
        }
    }
}

fun createToolBar(commands: Commands) = toolBar {
    commands.build {
        with(DemoActions) {

            + button(DOCUMENT_NEW)
            + splitMenuButton(DOCUMENT_OPEN) {
                + menuItem("Imagine this lists recently opened files") {
                    onAction { println("Open most recent file") }
                }
                // Demonstrates a dynamic menu.
                // items are rebuilt just before the PopupMenu is displayed.
                // Open the menu twice, and you will see extra items.
                onShowing {
                    val now = Date()
                    + menuItem("$now") {
                        onAction { println("Open file dated $now") }
                    }
                }
            }
            + button(DOCUMENT_SAVE)
            // Other buttons not used here : toggleButton, choiceRadioButton

            + spacer()

            + label("Greeting :")
            + propertyRadioButton2(GREETING_HELLO)
            + propertyRadioButton2(GREETING_HI)
            + propertyRadioButton2(GREETING_SUP)

            + spacer()

            // This looks a bit ugly, as we don't have an icon for it!
            + toggleButton(VIEW_TOGGLE_STATUS_BAR)
        }
    }
}

fun createStatusBar() = toolBar(Side.BOTTOM) {
    + Label("This is the status bar. It is visible!")
}

/**
 * The [Action]s for each button/menu item in our application.
 *
 * Each has a `name`, `text`, and an optional `shortcut` (KeyCombination) and tooltip.
 *
 * The `icons` parameter of [DemoCommands.commands] should contain icons with the same name as
 * the [Action].
 * e.g. [demoIcons] has an icon named `document-new`, which matches the first argument of [DOCUMENT_NEW].
 *
 * If you prefer, you may set [Action.iconName] to something other than [Action.name].
 * e.g. using a padlock icon named `lock` for two actions which lock different kinds of object.
 *
 * An action can be given more than one shortcut using [Action.additionalKeyCombinations]`.add(...)`.
 * The menu item will show the _first_ shortcut only.
 *
 * NOTE, action names can be any String. Here I've used hyphens to separate words.
 * Personally, I prefer to use underscores. YMMV.
 */
object DemoActions : Actions(demoIcons) {
    // This will be used to create the File menu in the MenuBar. It won't have a Command.
    val FILE = define("file", "File")

    // These will be used to create Buttons in a ToolBar as well as MenuItems in the File menu.
    val DOCUMENT_NEW = define("document-new", "New", Key.N.control(), tooltip = "New Document")
    val DOCUMENT_OPEN = define("document-open", "Open", Key.O.control(), tooltip = "Open Document")
    val DOCUMENT_SAVE = define("document-save", "Save", Key.S.control(), tooltip = "Save")
    val FILE_QUIT = define("file-close", "Quit")

    // Also define actions which may only be available via keyboard shortcuts.
    val TAB_CLOSE = define("tab-close", "Close Tab", Key.W.control())

    // Another Menu
    val VIEW = define("view", "View")
    val VIEW_TOGGLE_STATUS_BAR = define("view-toggle-status-bar", "Status Bar", Key.S.control().shift())

    val GREETING_HELLO = define("hello", "Hello", Key.DIGIT_1.control())
    val GREETING_HI = define("hi", "Hi", Key.DIGIT_2.control())
    val GREETING_SUP = define("sup", "Sup", Key.DIGIT_3.control())
}

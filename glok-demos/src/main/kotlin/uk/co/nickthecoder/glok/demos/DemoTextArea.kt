package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.demos.DemoTextArea.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.functions.*
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.FIXED_WIDTH

/**
 * See [ABOUT].
 */
class DemoTextArea : Application() {

    lateinit var textArea: TextArea

    val showLineNumbersProperty by booleanProperty(false)

    val indentationColumnsProperty by intProperty(4)
    val indentWithTabsProperty by booleanProperty(false)
    val behaveLikeTabsProperty by booleanProperty(true)

    val indentationProperty = indentationProperty(indentWithTabsProperty, indentationColumnsProperty, behaveLikeTabsProperty)

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoTextArea"
            scene(1000, 600) {
                root = demoPanel {
                    name = "DemoTextArea"
                    about = ABOUT
                    relatedDemos.addAll(DemoStyledTextArea::class, DemoFindReplace::class)

                    content = borderPane {

                        top = vBox {
                            + toolBar {
                                padding(4)
                                spacing = 4f
                                + button("Clear") {
                                    onAction { textArea.clear() }
                                }
                                + Label("Fill with text :")
                                + button("Indented Code") {
                                    tooltip = TextTooltip("The indentation is is based on your choice below.")
                                    onAction {
                                        textArea.text = EXAMPLE_CODE.replace("    ", textArea.indentation.indentation)
                                    }
                                }
                                + button("Lorem Ipsum") { onAction { textArea.text = LOREM_IPSUM } }
                                + button("x10") { onAction { textArea.text = LOREM_IPSUM.repeat(10) } }
                                + button("x1000") { onAction { textArea.text = LOREM_IPSUM.repeat(1000) } }
                                + spacer()
                                + toggleButton("Line Numbers") {
                                    selectedProperty.bidirectionalBind(showLineNumbersProperty)
                                }
                            }
                        }

                        center = textArea(ABOUT) {
                            section = true
                            // Use a fixed width font, as the demo includes different forms of indentation.
                            style(FIXED_WIDTH)
                            indentationProperty.unbind()
                            indentationProperty.bindTo(this@DemoTextArea.indentationProperty)
                            showLineNumbersProperty.bindTo(this@DemoTextArea.showLineNumbersProperty)
                            textArea = this
                            prefColumnCount = 30
                            prefRowCount = 10
                        }

                        bottom = toolBar {

                            // Display the position of the caret in human-friendly form (i.e. start = 1,1)
                            // Note `+` is an operator on ObservableInt, which returns an ObservableInt.
                            + Label("Caret Position : ")
                            + textField {
                                prefDigits(6)
                                readOnly = true
                                textProperty.bindTo((textArea.caretPositionProperty.row() + 1).toObservableString())
                            }
                            + Label(",")
                            + textField("") {
                                prefDigits(4)
                                readOnly = true
                                textProperty.bindTo((textArea.caretPositionProperty.column() + 1).toObservableString())
                            }
                            + Separator()

                            + label("Indentation : ")
                            + checkBox("Use Tabs?") {
                                selectedProperty.bidirectionalBind(indentWithTabsProperty)
                            }
                            + label("Columns")
                            + intSpinner {
                                min = 1
                                max = 8
                                editor.prefColumnCount = 3
                                valueProperty.bidirectionalBind(indentationColumnsProperty)
                            }
                            + checkBox("Behave Like Tabs?") {
                                visibleProperty.bindTo(! indentWithTabsProperty)
                                selectedProperty.bidirectionalBind(behaveLikeTabsProperty)
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {

        val ABOUT = """
            TextArea
            
            1.  Add / replace text programmatically.
            2.  Use of style(FIXED WIDTH) to change to a fixed-width font.
            3.  Using the row() and column() property functions to convert from a TextPosition
                to two integers.
            4.  Display the row/column in a status bar.
            5.  Shows that [TextArea] remains responsive with a fair amount of text.
                Insert 1,000 copies of lorem ipsum (9,000 lines).
                Then : Select All, Copy, Paste, Paste, Paste... to stress it.
            6.  Let's you change the `indentation` to tabs or spaces, using 4 or 8 columns
                per indentation level.
                This document is indented with 4 spaces, but it _feels_ like it is
                indented with tabs (until you uncheck `Behave Like Tabs?` below).
                See [Indentation.behaveLikeTabs].
            7.  Note, the +TAB+ key always increases the level of indentation,
                regardless of where the caret is.
                It does `NOT` allow you to insert a tab-character mid-line.
                See [TextAreaActions.TAB] for info on how to change this behaviour.
                
            """.trimIndent()

        val LOREM_IPSUM = """
           Lorem ipsum dolor sit amet, consectetur adipiscing elit,
           sed do eiusmod tempor incididunt ut labore et dolore
           magna aliqua. Ut enim ad minim veniam, quis nostrud
           exercitation ullamco laboris nisi ut aliquip ex ea
           commodo consequat. Duis aute irure dolor in reprehenderit
           in voluptate velit esse cillum dolore eu fugiat nulla
           pariatur. Excepteur sint occaecat cupidatat non proident,
           sunt in culpa qui officia deserunt mollit anim id est
           laborum.
           
           """.trimIndent()

        val EXAMPLE_CODE = """
           class HelloWorld {
           
               companion object {
                   :
                   fun main(vararg args: String) {
                       println( "Hello World" )
                   }
               }
           }
           
           """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTextArea::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Slider2d
import uk.co.nickthecoder.glok.property.functions.plus
import uk.co.nickthecoder.glok.property.functions.times
import uk.co.nickthecoder.glok.property.functions.toInt
import uk.co.nickthecoder.glok.property.functions.unaryMinus
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

class DemoStackPane : Application() {

    lateinit var slider2d: Slider2d

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            scene(600, 500) {
                root = demoPanel {
                    name = "DemoStackPane"
                    about = ABOUT
                    content = borderPane {
                        center = vBox {
                            fillWidth = false
                            alignment = Alignment.CENTER_CENTER

                            // Here's our stack pane.
                            + stackPane {
                                // Add a border, just so that we can see its bounds easily.
                                borderSize(1)
                                plainBorder(Tantalum.strokeColor)

                                // We want all children to be allocated their pref sizes.
                                fill = false
                                // And the centers of the children to align.
                                alignment = Alignment.CENTER_CENTER

                                // A Background, the same size as the ImageView.
                                // Being the first child, this will be the furthest BACK in the z-order.
                                + pane {
                                    plainBackground(Color.WHITE)
                                    noBorder()
                                    overridePrefWidth = 96f
                                    overridePrefHeight = 96f

                                }
                                + ImageView(demoIcons.size(96)["document-new"])

                                // Being the last child, this will be above the ImageView and the pane in the z-order.
                                + slider2d {
                                    val radius = 10f
                                    slider2d = this
                                    padding(radius)
                                    track.overridePrefWidth = 96f
                                    track.overridePrefHeight = 96f
                                    track.noBackground()
                                    track.noBorder()
                                    thumb.roundedBorder(Color.BLACK, radius)
                                }
                            }

                            + Label("document-new")
                        }

                        bottom = toolBar {
                            // Display the position of Slider2D's "thumb".
                            + Label("Position")
                            + textField {
                                readOnly = true
                                prefDigits(4)
                                textProperty.bindTo((slider2d.ratioXProperty * 96.0).toInt().toObservableString())
                            }
                            + Label(",")
                            + textField {
                                readOnly = true
                                prefDigits(4)
                                // Slider2d's ratio measures from the bottom, so we have to calculate (1-slider2d.ratio)
                                // as we want to measure from the top.
                                // Then multiply by 96 to get it into pixels.
                                textProperty.bindTo(((- slider2d.ratioYProperty + 1.0) * 96.0).toInt().toObservableString())
                            }
                            + spacer()
                        }
                    }
                }
            }
            show()
        }

    }

    companion object {
        val ABOUT = """
            The [StackPane] has three children :
            
            1. A white Pane (you could also place a checkered light grey/dark grey pattern where instead.
            2. An [ImageView] of an icon.
            3. A [Slider2d], which lets you move a circle to any part of the 2D area.
            
            The pref size of the [StackPane], is the largest prefWidth x prefHeight of its children.
            In this case, the Slider2d is larger in both directions.
            As we are using [fill = false], and [alignment = CENTER\_CENTER],
            the centers of all three children are aligned.
            
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoStackPane::class)
        }
    }
}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.demos.DemoListView.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoListView : Application() {

    override fun start(primaryStage: Stage) {

        val listView = listView<String> {
            canReorder = true
        }

        with(primaryStage) {
            title = "DemoListView"
            scene(600, 500) {
                root = demoPanel {
                    name = "DemoListView"
                    about = ABOUT

                    content = borderPane {

                        top = toolBar {
                            + button("Add") { onAction { listView.items.add("New item ${listView.items.size + 1}") } }
                            + button("Add Lots") {
                                onAction {
                                    for (word in LOREM_IPSUM) {
                                        listView.items.add(word)
                                    }
                                }
                            }
                            + button("Remove") {
                                disabledProperty.bindTo(listView.selection.selectedIndexProperty.equalTo(- 1))
                                onAction { listView.items.remove(listView.selection.selectedItem) }
                            }
                        }

                        center = listView.apply {
                            section = true
                            items.addAll("Hello", "World")
                        }

                    }
                }
            }

            show()
        }
    }

    companion object {

        val ABOUT = """
            A simple demonstration of [ListView].
            This uses the default cell factory, which creates [TextListCell]s (a subclass of [ListCell]).
            The `converter` simply uses [Any.toString], and as the items are Strings, the cells display the strings.
            
            Click the +Add Lots+ buttons to spam the list with data. Now click the +Dump+ button, and
            you will see that the [ListView] only has a few actual cells (nowhere near `items.size`).
            This is because only enough cells to cover the visible area are created.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoListView::class)
        }

        val LOREM_IPSUM = """
           Lorem ipsum dolor sit amet, consectetur adipiscing elit,
           sed do eiusmod tempor incididunt ut labore et dolore
           magna aliqua. Ut enim ad minim veniam, quis nostrud
           exercitation ullamco laboris nisi ut aliquip ex ea
           commodo consequat. Duis aute irure dolor in reprehenderit
           in voluptate velit esse cillum dolore eu fugiat nulla
           pariatur. Excepteur sint occaecat cupidatat non proident,
           sunt in culpa qui officia deserunt mollit anim id est
           laborum.
           """.trimIndent().split("\n", " ")
    }
}

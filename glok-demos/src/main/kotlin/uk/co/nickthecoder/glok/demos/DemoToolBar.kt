package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.BorderPane
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextArea
import uk.co.nickthecoder.glok.control.ToolBar
import uk.co.nickthecoder.glok.demos.DemoToolBar.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.asImageView
import uk.co.nickthecoder.glok.scene.dsl.*

/**
 * See [ABOUT].
 */
class DemoToolBar : Application() {

    lateinit var toolBar: ToolBar
    lateinit var borderPane: BorderPane

    override fun start(primaryStage: Stage) {
        with(primaryStage) {
            title = "DemoToolBar"

            scene(700, 500) {
                root = demoPanel {
                    name = "DemoToolBar"
                    about = ABOUT
                    relatedDemos.addAll(DemoRotation::class)

                    content = borderPane {
                        borderPane = this

                        top = toolBar {
                            toolBar = this

                            // NOTE, The labels do NOT appear
                            + button("New", icons24["document-new"].asImageView()) {
                                onAction { println("document-new : Not implemented!") }
                            }

                            + button("Open", icons24["document-open"].asImageView()) {
                                onAction { println("document-open : Not Implemented!") }
                            }

                            // NOTE, The label does appear, because there is no graphic.
                            + button("Pow!") {
                                onAction { println("Wham!") }
                            }

                        }

                        center = TextArea(ABOUT)

                        bottom = toolBar {
                            toggleGroup {
                                + Label("Orientation :")
                                + radioButton2("Horizontal") {
                                    selected = true
                                    onAction {
                                        borderPane.left = null
                                        borderPane.top = toolBar
                                        toolBar.side = Side.TOP
                                    }
                                }
                                + radioButton2("Vertical") {
                                    onAction {
                                        borderPane.top = null
                                        borderPane.left = toolBar
                                        toolBar.side = Side.LEFT
                                    }
                                }
                            }
                        }
                    }
                }
            }
            show()
        }
    }

    companion object {
        val ABOUT = """
            A simple demonstration of a [ToolBar].
            
            There are two [ToolBar]s, one at the top and another at the bottom.
            The bottom one is often called a `StatusBar`, but in Glok,
            the same class is used for both.
            
            In this demo :
            
            1. Add buttons, [RadioButton]s and [Label]s to a [ToolBar]
            2. Change the position and orientation of a [ToolBar].
            
            Notes :
            
            1. Despite the buttons having labels, they do `NOT` appear,
               only the graphic. This is because [Tantalum] (Glok's default [Theme])
               sets [Button.contentDisplay = ContentDisplay.GRAPHIC\_ONLY]
               for buttons in a [ToolBar].
            
            2. The +Pow!+ button makes the ToolBar ugly when oriented vertically.
               You `could` rotate the button. See the [Rotation] class, or [DemoRotation].
            
            Try resizing the window, making it narrower.
            Items which do not fit on the ToolBar are replaced by a ... button.
            This is called the "overflow button". Click it for a popup containing
            the items which didn't fit.
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoToolBar::class)
        }
    }

}

package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.FloatSpinner
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.demos.DemoTantalum.Companion.ABOUT
import uk.co.nickthecoder.glok.property.boilerplate.EdgesBinaryFunction
import uk.co.nickthecoder.glok.scene.Edges
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum

/**
 * See [ABOUT]
 */
class DemoTantalum : Application() {

    override fun start(primaryStage: Stage) {

        // Combine the result of two FloatSpinners into an ObservableEdges for Tantalum's buttonPadding
        lateinit var leftRightPadding: FloatSpinner
        lateinit var topBottomPadding: FloatSpinner

        with(primaryStage) {
            title = "DemoTantalum"
            scene(700, 500) {
                root = demoPanel {
                    name = "DemoTantalum"
                    about = ABOUT

                    content = scrollPane {
                        section = true
                        content = formGrid {

                            + row("Font Size") {
                                right = hBox {
                                    fillHeight = false
                                    spacing(10)
                                    + spinner(Tantalum.fontSize) {
                                        min = 10f
                                        max = 40f
                                        editor.prefColumnCount = 4
                                        Tantalum.fontSizeProperty.bidirectionalBind(valueProperty)
                                    }
                                    // It's easy to adjust the theme, just by changing Tantalum's properties :
                                    + button("Small") { onAction { Tantalum.fontSize = 12f } }
                                    + button("Medium") { onAction { Tantalum.fontSize = 16f } }
                                    + button("Large") { onAction { Tantalum.fontSize = 24f } }
                                }
                            }

                            + row("Button Padding") {
                                right = hBox {
                                    spacing(10)

                                    + spinner(Tantalum.buttonPadding.left) {
                                        leftRightPadding = this
                                        editor.prefColumnCount = 4
                                    }

                                    + Label(",")

                                    + spinner(Tantalum.buttonPadding.top) {
                                        topBottomPadding = this
                                        editor.prefColumnCount = 4
                                    }
                                }
                            }

                            + row("Dark Theme?") {
                                right = checkBox {
                                    selectedProperty.bidirectionalBind(Tantalum.darkProperty)
                                }
                            }

                            + row("Accent Color") {
                                right = colorButton("") {
                                    title = "Accent Color"
                                    colorProperty.bidirectionalBind(Tantalum.accentColorProperty)
                                }
                            }

                            + row("Font Color") {
                                right = colorButton("") {
                                    title = "Font Color"
                                    colorProperty.bidirectionalBind(Tantalum.fontColorProperty)
                                }
                            }

                            + row("Background Color 1") {
                                right = colorButton("") {
                                    title = "Background Color 1"
                                    colorProperty.bidirectionalBind(Tantalum.background1ColorProperty)
                                }
                            }

                            + row("Background Color 2") {
                                right = colorButton("") {
                                    title = "Background Color 2"
                                    colorProperty.bidirectionalBind(Tantalum.background2ColorProperty)
                                }
                            }

                            + row("Button Color") {
                                right = colorButton("") {
                                    title = "Button Color"
                                    colorProperty.bidirectionalBind(Tantalum.buttonColorProperty)
                                }
                            }

                            + row("Stroke Color") {
                                right = colorButton("") {
                                    title = "Stroke Color"
                                    colorProperty.bidirectionalBind(Tantalum.strokeColorProperty)
                                }
                            }
                        }
                    }
                }
            }

            Tantalum.buttonPaddingProperty.bindTo(EdgesBinaryFunction(topBottomPadding.valueProperty, leftRightPadding.valueProperty) { y, x ->
                Edges(y, x)
            })
            show()
        }
    }

    companion object {
        val ABOUT = """
            [Tantalum] is Glok's default [Theme].
            A Glok [Theme] does _NOT_ use CSS, it is a Class, with many `rules`, that are defined
            using a DSL (Domain Specific Language).
            
            By avoiding CSS, it is easy to generate themes on-the-fly, with parameters.
            This demo shows off some of the parameters that [Tantalum] provides.
            
            Remember, if you don't like [Tantalum], feel free to adjust it (using [Theme.combineWith]),
            or replace it entirely from scratch.
            """.trimIndent()

        /**
         * The main entry point into the program.
         */
        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTantalum::class)
        }
    }
}

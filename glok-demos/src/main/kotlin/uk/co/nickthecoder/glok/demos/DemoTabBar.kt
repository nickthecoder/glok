package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TabBar
import uk.co.nickthecoder.glok.control.TabClosingPolicy
import uk.co.nickthecoder.glok.control.TitledPane
import uk.co.nickthecoder.glok.demos.DemoTabBar.Companion.ABOUT
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import java.util.*

/**
 * See [ABOUT].
 */
class DemoTabBar : Application() {

    override fun start(primaryStage: Stage) {
        lateinit var myTabBar: TabBar

        with(primaryStage) {
            title = "TabBar Demo"
            scene(700, 500) {
                root = demoPanel {
                    name = "DemoTabBar"
                    about = ABOUT
                    relatedDemos.addAll(DemoTabPane::class, DemoScrollPaneWithButtons::class)

                    content = borderPane {
                        top = hBox {
                            fillHeight = false
                            alignment = Alignment.BOTTOM_CENTER

                            // NOTE, This is similar to:    + tabBar { ...
                            // But it changes the style, more suited to "document" tabs.
                            + documentTabBar {
                                // The default is TabClosingPolicy.ALL_TABS
                                tabClosingPolicy = TabClosingPolicy.SELECTED_TAB

                                myTabBar = this
                                + tab("Hello") {
                                    content = Label("This is the content")
                                }
                                + tab("World") {
                                    content = Label("2nd tab contents")
                                }
                            }

                            + toolBar(Side.TOP) {
                                noBorder()
                                // An "add tab" button.
                                + button("+") {
                                    padding(0)
                                    onAction {
                                        myTabBar.tabs.add(
                                            tab("New") {
                                                content = Label("New tab created at ${Date()}")
                                                // Make the newly created tab the selected one.
                                                myTabBar.selection.selectedItem = this
                                            }
                                        )
                                    }
                                }
                            }
                        }
                        center = splitPane {
                            borderSize(1, 0, 0, 0)
                            plainBorder(Tantalum.strokeColor) // Bad pattern - color won't change if we switch dark/light theme.

                            // To mimic a TabPane, all we need to do it bind the TabBar's currentContentProperty
                            // to SingleContainer.contentProperty...
                            + singleContainer(myTabBar.currentContentProperty) {
                                padding(20)
                            }

                            // This panel is fixed, regardless of which tab is selected.
                            + TitledPane("Help")
                            dividers[0].position = 0.8f
                        }
                    }
                }
            }
            show()
        }
    }


    companion object {
        val ABOUT = """
            A [TabPane] is composed of two parts, a [TabBar] and a [SingleContent].
            In this demo, we are _NOT_ using a [TabPane], but recreating the same effect.
            By doing so, we can be more flexible with the layout.
            
            The demo has two structural differences that are impossible if we used a [TabPane]
            
            1. An extra button to the right of the [TabBar] (which creates a new tab).
            2. Below the [TabBar], we have a [SplitPane].
               The [Tab]'s content appears on the left, and the right is the same,
               regardless of which tab is selected.
               
            """.trimIndent()

        @JvmStatic
        fun main(vararg args: String) {
            launch(DemoTabBar::class)
        }
    }
}

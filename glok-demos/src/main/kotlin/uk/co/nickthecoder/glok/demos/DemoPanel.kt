package uk.co.nickthecoder.glok.demos

import uk.co.nickthecoder.glok.application.*
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.optionalNodeProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.property.functions.isNotBlank
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.ThemedHighlight
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.FIXED_WIDTH
import uk.co.nickthecoder.glok.theme.styles.TINTED
import java.io.File
import java.net.URL
import kotlin.reflect.KClass

/**
 * Each Demo Application uses this control as the root of their scene.
 * It gives a consistent look and feel to each Demo.
 * Ideally, this class shouldn't distract you from the core code of the Demo!
 * Though, I suppose this can act as an advanced demo of how to create a custom Node.
 *
 * The Demos' contents are displayed within the first tab of a tab pane,
 * A second (optional) tab labelled "About" explains the purpose of the Demo.
 * The 3rd tab shows the source code of the demo. It assumes that the demo was launched
 * from the base directory of glok source. If the demo source code file isn't found,
 * it displays a button, which downloads the latest version of the Demo's source code from GitLab.
 *
 * FYI, this contains a nice example of how a multithreaded application should use
 * `Platform.runLater{ ... }` to ensure that all interactions with the Glok GUI
 * (including reading or writing [Property] values`) are from Glok's Thread.
 */
class DemoPanel : WrappedNode<VBox>(VBox()) {

    /**
     * The simple name of the demo's class.
     * This is used as the text for the first tab, as well as finding the source code (in the 3rd tab).
     */
    val nameProperty by stringProperty("")
    var name by nameProperty

    /**
     * The node which makes up the demo.
     */
    val contentProperty by optionalNodeProperty(null)
    var content by contentProperty

    /**
     * A String describing the demo.
     */
    val aboutProperty by stringProperty("")
    var about by aboutProperty

    private val aboutTextArea = styledTextArea("") {
        readOnly = true
    }
    private val aboutListener = aboutProperty.addChangeListener { _, _, text ->
        aboutTextArea.setSimpleStyledText(text, STYLES, TERMINATORS)
    }


    /**
     * Note, we are using a TabBar, and a SingleContainer, rather than just a TabPane,
     * so that we can add the _hamburger_ MenuButton on the right.
     */
    private val tabBar = tabBar {
        // The tabBar is part of a ToolBar, and we don't need BOTH of them to be sections.
        section = false

        growPriority = 0f
        padding(10, 0, 0, 0)

        + tab("Demo") {
            textProperty.bindTo(this@DemoPanel.nameProperty)
            content = hBox {
                fillHeight = true
                padding(0, 20)
                alignment = Alignment.CENTER_CENTER
                + singleContainer(this@DemoPanel.contentProperty) {
                    growPriority = 1f
                    borderSize(1)
                    plainBorder(Tantalum.strokeColor)
                }
            }
        }
        + tab("About") {
            // Hide the tab if [about] is blank.
            visibleProperty.bindTo(aboutProperty.isNotBlank())
            content = aboutTextArea
        }
        + tab("Source Code") {
            // When [name] is changed, so does the contents of this tab.
            // `sourceCode()` returns an ObservableOptionalNode, not a Node.n
            contentProperty.bindTo(sourceCode())
        }
    }

    private val relatedDemosButton = menuButton("Related Demos") {
        visible = false
    }

    /**
     * A list of [Application] classes.
     * When this is not empty, a `MenuButton` appears in the bottom left corner, which
     * lets the user jump to one of the related demos.
     */
    val relatedDemos = mutableListOf<KClass<out Application>>().asMutableObservableList()
    private val relatedDemosListener = relatedDemos.addListener {
        relatedDemosButton.items.clear()
        relatedDemosButton.items.addAll(
            relatedDemos.map { demoClass ->
                menuItem(demoClass.simpleName ?: "?") {
                    onAction {
                        GlokSettings.restarts.clear()
                        GlokSettings.restarts.add(Restart(demoClass))
                        GlokSettings.restarts.add(Restart(DemoMenu::class))
                        relatedDemosButton.scene?.stage?.close()
                    }
                }
            }
        )
        relatedDemosButton.visible = relatedDemos.size > 0
    }

    val buttonBar = buttonBar {
        meaning(ButtonMeaning.FINISH) {
            + button("Close") { onAction { scene?.stage?.close() } }
        }
        meaning(ButtonMeaning.LEFT) {
            + relatedDemosButton
        }
    }

    init {
        inner.apply {
            fillWidth = true

            + toolBar {
                growPriority = 0f
                borderSize(0)

                + spacer()
                + tabBar
                + spacer()
                + menuButton("Extras") {
                    style(TINTED)
                    style(".no_arrow")
                    tooltip = TextTooltip("Extra Options")

                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = Tantalum.icon("hamburger")

                    // Toggle between light/dark theme
                    + toggleMenuItem("Dark Theme") {
                        selectedProperty.bidirectionalBind(Tantalum.darkProperty)
                    }
                    // Toggle between light/dark theme
                    + menuItem("Glok Settings") {
                        onAction {
                            stage(scene !!.stage !!, StageType.NORMAL) {
                                scene = scene {
                                    root = GlokSettingsApplication.createGlokSettingsTabPane()
                                }
                            }.show()

                        }
                    }

                    + Separator()

                    // Use the NodeInspector to look at the Demo's node. Useful for debugging.
                    + menuItem("Inspect Demo Content") {
                        onAction {
                            this@DemoPanel.scene?.let {
                                NodeInspector.showDialog(content, it.stage)
                            }
                        }
                    }

                    // Use the NodeInspector to look at the Scene's root. Useful for debugging.
                    + menuItem("Inspect Scene") {
                        onAction {
                            this@DemoPanel.scene?.let {
                                NodeInspector.showDialog(it.root, it.stage)
                            }
                        }
                    }

                    // Print the scene graph to the console (stdout). Useful for debugging.
                    // Sometimes this is easier to use than the NodeInspector
                    // NOTE. this@DemoPanel is required, otherwise it would dump the PopupMenu's scene.
                    + menuItem("Dump Scene") { onAction { this@DemoPanel.scene?.dump() } }
                    // As above, but also includes the borders (for Regions) and min/pref/max sizes of all nodes.
                    + menuItem("Dump Scene (full)") {
                        onAction {
                            this@DemoPanel.scene?.dump(borders = true, size = true)
                        }
                    }

                }
            }

            + singleContainer {
                growPriority = 1f
                contentProperty.bindTo(tabBar.currentContentProperty)
            }
            + buttonBar

        }
    }

    private fun sourceCode() = OptionalNodeUnaryFunction(nameProperty) { name ->

        // When DemoMenu is run from my IDE, the current directory is different to
        // when running it via : ./gradlew glok-demo:run
        // So let's check for both.
        val file1 = File("glok-demos/src/main/kotlin/uk/co/nickthecoder/glok/demos/$name.kt")
        val file2 = File("src/main/kotlin/uk/co/nickthecoder/glok/demos/$name.kt")

        if (name.isBlank()) {
            null
        } else if (file1.exists()) {
            textArea(file1.readText()) {
                style(FIXED_WIDTH)
                readOnly = true
            }
        } else if (file2.exists()) {
            textArea(file2.readText()) {
                style(FIXED_WIDTH)
                readOnly = true
            }
        } else {

            // If the source code wasn't found, then give the option to download the latest version from gitlab.com.

            vBox {
                padding(20)
                spacing(20)

                + Label("File not found :")
                + textField(file1.absolutePath) {
                    readOnly = true
                }
                + threeRow {
                    center = button("Download from GitLab") {
                        defaultButton = true
                        onAction {
                            text = "Loading..."
                            disabled = true
                            downloadSourceCode(this@vBox, name)
                        }
                    }
                }
            }
        }
    }

    private fun downloadSourceCode(box: VBox, name: String) {

        val url = URL("https://gitlab.com/nickthecoder/glok/-/raw/main/glok-demos/src/main/kotlin/uk/co/nickthecoder/glok/demos/$name.kt")

        Thread {
            val text = try {
                url.readText()
            } catch (e: Exception) {
                "Failed to get source code\n$e"
            }
            // Only update Glok controls or properties on the Glok Thread.
            Platform.runLater {
                box.apply {
                    children.clear()
                    + textArea(text) {
                        style(FIXED_WIDTH)
                        growPriority = 1f
                        readOnly = true
                    }
                }
            }
        }.start()
    }

    companion object {
        /**
         * A [Highlight] which does not use themes. Here we add a red underline.
         */
        val nameHighlight = object : Highlight {
            override fun style(text: Text) {
                text.textColor = Color["#181"]
            }
        }

        val STYLES = mapOf(
            '`' to ThemedHighlight(".italic"),
            '+' to ThemedHighlight(".bold"),
            '_' to ThemedHighlight(".underline"),
            '[' to nameHighlight
        )
        val TERMINATORS = mapOf(']' to '[')
    }
}

fun demoPanel(block: DemoPanel.() -> Unit) = DemoPanel().apply(block)
